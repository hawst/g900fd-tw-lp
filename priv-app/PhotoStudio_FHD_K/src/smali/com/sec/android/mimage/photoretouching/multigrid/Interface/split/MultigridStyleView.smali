.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "MultigridStyleView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;,
        Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;,
        Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$ResolverSetAsAdapter;,
        Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$ResolverShareAdapter;
    }
.end annotation


# instance fields
.field private final CONTACTS:Ljava/lang/String;

.field private final SAVE_GOOD_SIZE:I

.field private final SAVE_MAX_SIZE:I

.field private final SAVE_MIN_SIZE:I

.field animationMoveItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

.field private mContext:Landroid/content/Context;

.field private mCurrentBackgroundID:I

.field private mCurrentImgCnt:I

.field private mCurrentLayout:I

.field private mCurrentOptionId:I

.field private mCurrentPileBgId:I

.field private mCurrentPileFrameId:I

.field private mCurrentProportionID:I

.field private mCurrentSaveSize:I

.field private mCurrentTouchDownIndex:I

.field private mCurrentTouchedIndex:I

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

.field private mDrawRoi:Landroid/graphics/Rect;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

.field private mInitSplit:Z

.field private mIsAbleSave:Z

.field private mIsDraging:Z

.field private mIsDrawCollage:Z

.field private mIsMoving:Z

.field private mIsPileMode:Z

.field private mIsPopupTouch:Z

.field private mLongpressButton:Z

.field private mMiddleBtn:Landroid/widget/Button;

.field private mMiddleBtnVisible:Z

.field private mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

.field private mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

.field private mMultiGridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

.field private mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

.field private mOnLayout:Z

.field private mPinchZoomListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;

.field private mSaveButtonHandler:Landroid/os/Handler;

.field private mSaveCanvasHeight:I

.field private mSaveCanvasWidth:I

.field private mSaveDialogListener:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

.field private mSaveFileName:Ljava/lang/String;

.field private mSaveFileUri:Landroid/net/Uri;

.field private mSavePath:Ljava/lang/String;

.field private mSeekbarListenerRoundness:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

.field private mSeekbarListenerSpacing:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

.field private mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

.field private mSubmenuTouchListener:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

.field private mTouchFunction:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;II)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p3, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p4, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p5, "multiGridReloadManager"    # Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;
    .param p6, "drawRectList"    # Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;
    .param p7, "srcItemList"    # Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    .param p8, "layoutStyle"    # I
    .param p9, "proportionId"    # I

    .prologue
    const/16 v5, 0x7d0

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 152
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 3134
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->animationMoveItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 3352
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$1;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveButtonHandler:Landroid/os/Handler;

    .line 3537
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->SAVE_MAX_SIZE:I

    .line 3538
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->SAVE_GOOD_SIZE:I

    .line 3539
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->SAVE_MIN_SIZE:I

    .line 3540
    const-string v0, ".contacts."

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->CONTACTS:Ljava/lang/String;

    .line 3542
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    .line 3543
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 3544
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 3545
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 3546
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    .line 3547
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mOnLayout:Z

    .line 3548
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mInitSplit:Z

    .line 3549
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsMoving:Z

    .line 3550
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDraging:Z

    .line 3551
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDrawCollage:Z

    .line 3552
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsAbleSave:Z

    .line 3553
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRoi:Landroid/graphics/Rect;

    .line 3554
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 3556
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    .line 3557
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .line 3558
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 3559
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSubmenuTouchListener:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    .line 3560
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mPinchZoomListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;

    .line 3561
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;

    .line 3562
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 3564
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    .line 3565
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSeekbarListenerRoundness:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 3566
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSeekbarListenerSpacing:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 3568
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveDialogListener:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 3569
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 3570
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    .line 3572
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    .line 3573
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchDownIndex:I

    .line 3574
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    .line 3575
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    .line 3576
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentBackgroundID:I

    .line 3577
    const v0, 0x7f0601cb

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentSaveSize:I

    .line 3578
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentImgCnt:I

    .line 3580
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasWidth:I

    .line 3581
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasHeight:I

    .line 3583
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    .line 3584
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileName:Ljava/lang/String;

    .line 3586
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    .line 3588
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    .line 3589
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    .line 3590
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileUri:Landroid/net/Uri;

    .line 3591
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I

    .line 3593
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    .line 3594
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentPileFrameId:I

    .line 3595
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentPileBgId:I

    .line 3597
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mLongpressButton:Z

    .line 3598
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPopupTouch:Z

    .line 153
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    .line 154
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 155
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 156
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    .line 157
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 158
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setInterface(Ljava/lang/Object;)V

    .line 159
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRoi:Landroid/graphics/Rect;

    .line 160
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 161
    iput p8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    .line 163
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .line 164
    iput-object p7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 165
    iput p9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    .line 166
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->init()V

    .line 168
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initMiddleButton()V

    .line 169
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p6, "multiGridReloadManager"    # Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;
    .param p7, "layoutStyle"    # I

    .prologue
    const/16 v5, 0x7d0

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 128
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 3134
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->animationMoveItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 3352
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$1;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveButtonHandler:Landroid/os/Handler;

    .line 3537
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->SAVE_MAX_SIZE:I

    .line 3538
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->SAVE_GOOD_SIZE:I

    .line 3539
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->SAVE_MIN_SIZE:I

    .line 3540
    const-string v0, ".contacts."

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->CONTACTS:Ljava/lang/String;

    .line 3542
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    .line 3543
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 3544
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 3545
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 3546
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    .line 3547
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mOnLayout:Z

    .line 3548
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mInitSplit:Z

    .line 3549
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsMoving:Z

    .line 3550
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDraging:Z

    .line 3551
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDrawCollage:Z

    .line 3552
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsAbleSave:Z

    .line 3553
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRoi:Landroid/graphics/Rect;

    .line 3554
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 3556
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    .line 3557
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .line 3558
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 3559
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSubmenuTouchListener:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    .line 3560
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mPinchZoomListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;

    .line 3561
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;

    .line 3562
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 3564
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    .line 3565
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSeekbarListenerRoundness:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 3566
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSeekbarListenerSpacing:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 3568
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveDialogListener:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 3569
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 3570
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    .line 3572
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    .line 3573
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchDownIndex:I

    .line 3574
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    .line 3575
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    .line 3576
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentBackgroundID:I

    .line 3577
    const v0, 0x7f0601cb

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentSaveSize:I

    .line 3578
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentImgCnt:I

    .line 3580
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasWidth:I

    .line 3581
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasHeight:I

    .line 3583
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    .line 3584
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileName:Ljava/lang/String;

    .line 3586
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    .line 3588
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    .line 3589
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    .line 3590
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileUri:Landroid/net/Uri;

    .line 3591
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I

    .line 3593
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    .line 3594
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentPileFrameId:I

    .line 3595
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentPileBgId:I

    .line 3597
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mLongpressButton:Z

    .line 3598
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPopupTouch:Z

    .line 129
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    .line 130
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 131
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 132
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    .line 133
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 134
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setInterface(Ljava/lang/Object;)V

    .line 135
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRoi:Landroid/graphics/Rect;

    .line 136
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 137
    iput p7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    .line 139
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->init()V

    .line 140
    return-void
.end method

.method private EndMoveObject(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, -0x1

    .line 2925
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    if-nez v0, :cond_0

    .line 2936
    :goto_0
    return-void

    .line 2931
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->startSwapAnimation(Landroid/view/MotionEvent;)V

    .line 2932
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    .line 2934
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->setCurrentIndex(I)V

    .line 2935
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    goto :goto_0
.end method

.method private InitMoveObject(Landroid/view/MotionEvent;)V
    .locals 16
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2866
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getTouchedIndex(Landroid/view/MotionEvent;)I

    move-result v11

    .line 2868
    .local v11, "moveIndex":I
    if-gez v11, :cond_0

    .line 2901
    :goto_0
    return-void

    .line 2873
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1, v11}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->setDragFromIndex(I)V

    .line 2874
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    if-eqz v1, :cond_1

    .line 2876
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->EndMoveObject(Landroid/view/MotionEvent;)V

    .line 2879
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1, v11}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 2880
    .local v14, "temp":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    new-instance v12, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-direct {v12, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;-><init>(Landroid/content/Context;)V

    .line 2881
    .local v12, "moveItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    new-instance v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    invoke-direct {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;-><init>()V

    .line 2882
    .local v9, "decodeInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    invoke-virtual {v12, v9}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setDecodeInfo(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;)V

    .line 2883
    invoke-virtual {v14}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v12, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImage(Landroid/graphics/Bitmap;)V

    .line 2884
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v12}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getmDrawRect()Landroid/graphics/Rect;

    move-result-object v2

    new-instance v3, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v4, v11}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->calculateRectSize(Landroid/graphics/Rect;Landroid/graphics/RectF;Z)Landroid/graphics/Rect;

    move-result-object v13

    .line 2885
    .local v13, "newRect":Landroid/graphics/Rect;
    invoke-virtual {v12, v13}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setmDrawRect(Landroid/graphics/Rect;)V

    .line 2886
    invoke-virtual {v12}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getmDrawRect()Landroid/graphics/Rect;

    move-result-object v10

    .line 2888
    .local v10, "drawRect":Landroid/graphics/Rect;
    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v12, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;Z)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    .line 2889
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    .line 2890
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v10}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 2891
    new-instance v3, Landroid/graphics/PointF;

    invoke-virtual {v10}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v10}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    int-to-float v5, v5

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2892
    new-instance v4, Landroid/graphics/PointF;

    iget v5, v10, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    iget v6, v10, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2893
    new-instance v5, Landroid/graphics/PointF;

    iget v6, v10, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    iget v7, v10, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2894
    new-instance v6, Landroid/graphics/PointF;

    iget v7, v10, Landroid/graphics/Rect;->right:I

    int-to-float v7, v7

    iget v8, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v8, v8

    invoke-direct {v6, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2895
    new-instance v7, Landroid/graphics/PointF;

    iget v8, v10, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    iget v15, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v15, v15

    invoke-direct {v7, v8, v15}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2896
    const/4 v8, 0x0

    .line 2889
    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V

    .line 2898
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    invoke-virtual {v1, v11}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->setStartIndex(I)I

    .line 2899
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v4, v11}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->InitMoveObject(FFLandroid/graphics/RectF;)I

    .line 2900
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDraging:Z

    goto/16 :goto_0
.end method

.method private StartMoveObject(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 2906
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v2, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getTouchedIndex(Landroid/view/MotionEvent;)I

    move-result v0

    .line 2908
    .local v0, "newIndex":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    if-eqz v2, :cond_1

    .line 2910
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    if-eq v0, v2, :cond_0

    .line 2912
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    .line 2913
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v2, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->setDragToIndex(I)V

    .line 2914
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->setDrawRect()V

    .line 2917
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->StartMoveObject(FF)V

    .line 2918
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDraging:Z

    .line 2921
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 3543
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Z
    .locals 1

    .prologue
    .line 3552
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsAbleSave:Z

    return v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V
    .locals 0

    .prologue
    .line 349
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setButtonSelected()V

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 218
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->touchMainButton(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 3590
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;I)V
    .locals 0

    .prologue
    .line 3591
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Z)V
    .locals 0

    .prologue
    .line 3597
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mLongpressButton:Z

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 3542
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Z
    .locals 1

    .prologue
    .line 3597
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mLongpressButton:Z

    return v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V
    .locals 0

    .prologue
    .line 2484
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->saveToFile()V

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;I)V
    .locals 0

    .prologue
    .line 3577
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentSaveSize:I

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->clearCollageAnimationView()V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
    .locals 1

    .prologue
    .line 3564
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 2864
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->InitMoveObject(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;
    .locals 1

    .prologue
    .line 3557
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    return-object v0
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Z)Z
    .locals 1

    .prologue
    .line 3319
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setSaveable(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;
    .locals 1

    .prologue
    .line 3556
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    return-object v0
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    .locals 1

    .prologue
    .line 3558
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    return-object v0
.end method

.method static synthetic access$25(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Z
    .locals 1

    .prologue
    .line 3598
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPopupTouch:Z

    return v0
.end method

.method static synthetic access$26(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V
    .locals 0

    .prologue
    .line 2808
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->replaceItem()V

    return-void
.end method

.method static synthetic access$27(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V
    .locals 0

    .prologue
    .line 2788
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->removeItem()V

    return-void
.end method

.method static synthetic access$28(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Z)V
    .locals 0

    .prologue
    .line 3598
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPopupTouch:Z

    return-void
.end method

.method static synthetic access$29(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;I)V
    .locals 0

    .prologue
    .line 2823
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->startEffectRotateMode(I)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Z
    .locals 1

    .prologue
    .line 3593
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    return v0
.end method

.method static synthetic access$30(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;
    .locals 1

    .prologue
    .line 3570
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    return-object v0
.end method

.method static synthetic access$31(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)I
    .locals 1

    .prologue
    .line 3572
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    return v0
.end method

.method static synthetic access$32(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;I)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setViewLayerType(I)V

    return-void
.end method

.method static synthetic access$33(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)I
    .locals 1

    .prologue
    .line 3575
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    return v0
.end method

.method static synthetic access$34(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)I
    .locals 1

    .prologue
    .line 3591
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I

    return v0
.end method

.method static synthetic access$35(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2414
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getSaveFilePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$36(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2524
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->saveCollage(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$37(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 3590
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileUri:Landroid/net/Uri;

    return-void
.end method

.method static synthetic access$38(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Z
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->runningCollageAnimation()Z

    move-result v0

    return v0
.end method

.method static synthetic access$39(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Z)Z
    .locals 1

    .prologue
    .line 1525
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->hideSubMenu(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .locals 1

    .prologue
    .line 3544
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    return-object v0
.end method

.method static synthetic access$40(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V
    .locals 0

    .prologue
    .line 3136
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->startSuffleAnimation()V

    return-void
.end method

.method static synthetic access$41(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V
    .locals 0

    .prologue
    .line 3545
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    return-void
.end method

.method static synthetic access$42(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;
    .locals 1

    .prologue
    .line 3586
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    return-object v0
.end method

.method static synthetic access$43(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;)V
    .locals 0

    .prologue
    .line 3586
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Z)V
    .locals 0

    .prologue
    .line 3589
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 3554
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;I)V
    .locals 0

    .prologue
    .line 2342
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setLayoutStyle(I)V

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Z
    .locals 1

    .prologue
    .line 3384
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setPileMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;ILandroid/view/View;)V
    .locals 0

    .prologue
    .line 2704
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setBackGroundImage(ILandroid/view/View;)V

    return-void
.end method

.method private addImage()V
    .locals 1

    .prologue
    .line 2818
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    .line 2820
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getMultiImageFromGallary()V

    .line 2821
    return-void
.end method

.method private checkHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 3065
    const/4 v0, 0x0

    .line 3066
    .local v0, "isNoShow":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->isDoNotShowPopup(Landroid/content/Context;I)Z

    move-result v0

    .line 3068
    if-nez v0, :cond_0

    .line 3070
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-nez v1, :cond_0

    .line 3072
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initHelpPopup(I)V

    .line 3073
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->showCurrentModeHelpPopup()V

    .line 3076
    :cond_0
    return-void
.end method

.method private getImageFromGallary(Z)V
    .locals 2
    .param p1, "isReplace"    # Z

    .prologue
    .line 2254
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v0

    sget v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MAX_ITEM_CNT:I

    if-lt v0, v1, :cond_0

    if-nez p1, :cond_0

    .line 2256
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    const v1, 0x7f0600ff

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 2288
    :goto_0
    return-void

    .line 2260
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$22;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$22;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->getMultiGridImageFromGallery(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;)V

    goto :goto_0
.end method

.method private getMultiImageFromGallary()V
    .locals 3

    .prologue
    .line 2222
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->getAddImgCnt()I

    move-result v0

    .line 2225
    .local v0, "addImgCnt":I
    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v1

    add-int/2addr v1, v0

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MAX_ITEM_CNT:I

    if-le v1, v2, :cond_1

    .line 2227
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    const v2, 0x7f0600ff

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 2248
    :goto_0
    return-void

    .line 2231
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$21;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$21;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;I)V

    invoke-static {v1, v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->getMultiGridImageFromGallery(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;)V

    goto :goto_0
.end method

.method private getSaveFilePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2416
    const/4 v0, 0x0

    .line 2419
    .local v0, "i":I
    sget-object v1, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    .line 2423
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 2425
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->isPersonalPage()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2427
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getPersonalPagePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage/Private"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2428
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/storage/Private"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Studio"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    .line 2431
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 2432
    const/4 v0, 0x1

    .line 2434
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v1

    if-lt v0, v1, :cond_4

    .line 2451
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 2452
    sget-object v1, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    .line 2455
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2457
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    .line 2459
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->checkOrMakeFileDirectory(Ljava/lang/String;)Ljava/io/File;

    .line 2464
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileName:Ljava/lang/String;

    .line 2467
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    return-object v1

    .line 2436
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->isPersonalPage()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2440
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getPersonalPagePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2444
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Studio"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    goto/16 :goto_1

    .line 2434
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method

.method private hideSubMenu(Z)Z
    .locals 5
    .param p1, "isButtonTouched"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1527
    const/4 v0, 0x0

    .line 1529
    .local v0, "isHided":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v1, :cond_0

    .line 1532
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    move-result v1

    or-int/2addr v0, v1

    .line 1533
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : hideSubMenu : 00 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1537
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubButtonShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1539
    const/4 v0, 0x1

    .line 1540
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->animAndFreeSubViewButtons()V

    .line 1541
    const/high16 v1, 0x1e110000

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 1542
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : hideSubMenu : 01 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1544
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setClearSelectedButton()V

    .line 1548
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1550
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    invoke-virtual {v1, v3, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->show(ZZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1552
    or-int/lit8 v0, v0, 0x1

    .line 1553
    if-eqz p1, :cond_2

    .line 1555
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v1, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setDrawSelectedLine(Z)V

    .line 1556
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->setCurrentIndex(I)V

    .line 1558
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : hideSubMenu : 02 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1562
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->isShown()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1564
    or-int/lit8 v0, v0, 0x1

    .line 1565
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setAssistantLayoutVisible(I)V

    .line 1566
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : hideSubMenu : 03 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1567
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 1568
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    .line 1576
    :cond_4
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1578
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->show(ZZ)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1579
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : hideSubMenu : 04 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1588
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->invalidateViewsWithThread()V

    .line 1590
    return v0

    .line 1570
    :cond_6
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    if-nez v1, :cond_4

    .line 1572
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 1573
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    goto :goto_0
.end method

.method private init()V
    .locals 4

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    if-nez v0, :cond_0

    .line 175
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    .line 177
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initAssistantManager()V

    .line 178
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initPinchZoomCallback()V

    .line 179
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initGesture()V

    .line 180
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initColorPicker()V

    .line 181
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initModifyPopup()V

    .line 182
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setCollageRatio(I)V

    .line 183
    const/high16 v0, 0x1e400000

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setBackGroundImage(ILandroid/view/View;)V

    .line 184
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->loadPileImgs()V

    .line 186
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setViewLayerType(I)V

    .line 187
    return-void
.end method

.method private initAssistantManager()V
    .locals 7

    .prologue
    .line 1154
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    if-nez v0, :cond_0

    .line 1156
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    .line 1159
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->initForCollage()V

    .line 1160
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initSeekbarListener()V

    .line 1161
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getCurrentRoundStep()I

    move-result v1

    .line 1162
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->MAX_ROUND_STEPS:I

    .line 1163
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getCurrentSpacingStep()I

    move-result v3

    .line 1164
    sget v4, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->MAX_SPACING_STEPS:I

    .line 1165
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSeekbarListenerRoundness:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 1166
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSeekbarListenerSpacing:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 1161
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->addCollageBorderSeekbar(IIIILcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V

    .line 1167
    return-void
.end method

.method private initColorPicker()V
    .locals 2

    .prologue
    .line 1289
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-nez v0, :cond_0

    .line 1291
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 1292
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$16;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$16;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setColorPickerCallback(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;)V

    .line 1304
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    sget v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLLAGE_BACKGROUND:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setBackground(I)V

    .line 1306
    :cond_0
    return-void
.end method

.method private initGesture()V
    .locals 3

    .prologue
    .line 1171
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$12;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$12;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    .line 1182
    .local v0, "gestureListener":Landroid/view/GestureDetector$OnGestureListener;
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 1183
    return-void
.end method

.method private initHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 3079
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 3080
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$25;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$25;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->setHelpPopupCallback(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;)V

    .line 3087
    return-void
.end method

.method private initMiddleButton()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2973
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f060198

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2974
    .local v2, "text":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    const v7, 0x7f0900b6

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    .line 2976
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050232

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2977
    .local v5, "viewWitdh":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050235

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2978
    .local v4, "viewHeight":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050234

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2981
    .local v3, "textSize":I
    const-string v6, "sec-roboto-light"

    invoke-static {v6, v8}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 2982
    .local v0, "font":Landroid/graphics/Typeface;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    if-eqz v6, :cond_1

    .line 2985
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-static {v6, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->doNeedUppercaseString(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2986
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->toUppercaseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2989
    :cond_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    const v7, 0x7f020325

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 2990
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2991
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2993
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    int-to-float v7, v3

    invoke-virtual {v6, v8, v7}, Landroid/widget/Button;->setTextSize(IF)V

    .line 2994
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v5}, Landroid/widget/Button;->setWidth(I)V

    .line 2995
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v4}, Landroid/widget/Button;->setHeight(I)V

    .line 2996
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2997
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v8}, Landroid/widget/Button;->setPressed(Z)V

    .line 2998
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/Button;->setSingleLine()V

    .line 2999
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 3001
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 3002
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v6, 0xb

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3003
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f05022e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3004
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f05022f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3005
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v6, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3009
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    new-instance v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$24;

    invoke-direct {v7, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$24;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3021
    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    return-void
.end method

.method private initModifyPopup()V
    .locals 2

    .prologue
    .line 1310
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    if-nez v0, :cond_0

    .line 1312
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPopupTouch:Z

    .line 1313
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    .line 1314
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$17;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$17;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->setModifyPopupCallback(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;)V

    .line 1343
    :cond_0
    return-void
.end method

.method private initPinchZoomCallback()V
    .locals 2

    .prologue
    .line 1254
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;

    .line 1256
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$15;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$15;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;

    .line 1283
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mPinchZoomListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;

    .line 1284
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mPinchZoomListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->init(Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;)V

    .line 1285
    return-void
.end method

.method private initSaveYesNoCancelForFinish()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 780
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 781
    const/16 v2, 0x8

    .line 782
    const v3, 0x7f06019d

    .line 783
    const/4 v4, 0x1

    .line 785
    const v6, 0x103012e

    .line 780
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 787
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 788
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x500

    .line 791
    const v4, 0x7f060192

    move v2, v7

    move v3, v7

    move-object v6, v5

    .line 788
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 795
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    .line 796
    new-instance v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$9;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$9;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    .line 795
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V

    .line 823
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601b2

    .line 824
    new-instance v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$10;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$10;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    .line 823
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 837
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 838
    new-instance v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$11;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$11;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    .line 837
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 848
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 849
    return-void
.end method

.method private initSeekbarListener()V
    .locals 1

    .prologue
    .line 1187
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSeekbarListenerRoundness:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    if-nez v0, :cond_0

    .line 1189
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$13;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$13;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSeekbarListenerRoundness:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 1218
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSeekbarListenerSpacing:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    if-nez v0, :cond_1

    .line 1220
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$14;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$14;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSeekbarListenerSpacing:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 1249
    :cond_1
    return-void
.end method

.method private initSetAsDialog()V
    .locals 7

    .prologue
    .line 2210
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2211
    const v2, 0x7f090158

    .line 2212
    const v3, 0x7f0600a3

    .line 2213
    const/4 v4, 0x0

    .line 2214
    const/4 v5, 0x0

    .line 2215
    const v6, 0x103012e

    .line 2210
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2216
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2217
    return-void
.end method

.method private initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isShareVia"    # Z

    .prologue
    .line 1922
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1923
    .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    const/4 v2, 0x0

    .line 1924
    .local v2, "shareChooser":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 1925
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    const v4, 0x7f06000f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 1929
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/content/pm/LabeledIntent;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/pm/LabeledIntent;

    .line 1930
    .local v0, "extraIntents":[Landroid/content/pm/LabeledIntent;
    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1931
    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1932
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2120
    return-void

    .line 1927
    .end local v0    # "extraIntents":[Landroid/content/pm/LabeledIntent;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    const v4, 0x7f0600a3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method private initShareViaDialog()V
    .locals 7

    .prologue
    const v2, 0x7f090157

    .line 957
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 969
    :goto_0
    return-void

    .line 962
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 964
    const v3, 0x7f06000f

    .line 965
    const/4 v4, 0x0

    .line 966
    const/4 v5, 0x0

    .line 967
    const v6, 0x103012e

    .line 962
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 968
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    goto :goto_0
.end method

.method private initSplitView()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2597
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    if-nez v2, :cond_0

    .line 2599
    new-instance v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;)V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    .line 2602
    :cond_0
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setLayoutStyle(I)V

    .line 2603
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->getIsEditMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2605
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridReloadManager:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;

    invoke-virtual {v2, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->setIsEditMode(Z)V

    .line 2608
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mOnLayout:Z

    if-nez v2, :cond_2

    .line 2609
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getImageEditViewWidth()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getImageEditViewHeight()I

    move-result v2

    if-eqz v2, :cond_3

    .line 2611
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getImageEditViewWidth()I

    move-result v1

    .line 2612
    .local v1, "viewWidth":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getImageEditViewHeight()I

    move-result v0

    .line 2613
    .local v0, "viewHeight":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v2, v6, v6, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 2614
    iput-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mOnLayout:Z

    .line 2616
    .end local v0    # "viewHeight":I
    .end local v1    # "viewWidth":I
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPositionWithBackBtn(I)V

    .line 2617
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mInitSplit:Z

    .line 2618
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->invalidateViews()V

    .line 2619
    return-void
.end method

.method private initSubButtons()V
    .locals 5

    .prologue
    .line 377
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v2, :cond_1

    .line 379
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initSubViewButtons(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V

    .line 380
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getImageEditViewWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->showSubBottomButton(II)V

    .line 382
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSubmenuTouchListener:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    if-nez v2, :cond_0

    .line 384
    new-instance v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$3;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$3;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSubmenuTouchListener:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    .line 443
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 448
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setButtonSelected()V

    .line 450
    .end local v0    # "i":I
    :cond_1
    return-void

    .line 445
    .restart local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 446
    .local v1, "v":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSubmenuTouchListener:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 443
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private loadDrawRects(ILcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;)V
    .locals 9
    .param p1, "layoutStyle"    # I
    .param p2, "drawRectList"    # Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .prologue
    .line 2292
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v0

    .line 2293
    .local v0, "currentItemCnt":I
    const/4 v1, 0x0

    .line 2295
    .local v1, "drawRects":[[F
    sget v4, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MAX_ITEM_CNT:I

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2296
    sget v4, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MIN_ITEM_CNT:I

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2298
    packed-switch v0, :pswitch_data_0

    .line 2320
    :goto_0
    if-nez v1, :cond_1

    .line 2322
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " : setLayoutStyle"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 2335
    :cond_0
    return-void

    .line 2301
    :pswitch_0
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    invoke-static {p1, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->getDrawRects(II)[[F

    move-result-object v1

    .line 2302
    goto :goto_0

    .line 2304
    :pswitch_1
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    invoke-static {p1, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->getDrawRects(II)[[F

    move-result-object v1

    .line 2305
    goto :goto_0

    .line 2307
    :pswitch_2
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    invoke-static {p1, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->getDrawRects(II)[[F

    move-result-object v1

    .line 2308
    goto :goto_0

    .line 2310
    :pswitch_3
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    invoke-static {p1, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->getDrawRects(II)[[F

    move-result-object v1

    .line 2311
    goto :goto_0

    .line 2313
    :pswitch_4
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    invoke-static {p1, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->getDrawRects(II)[[F

    move-result-object v1

    .line 2314
    goto :goto_0

    .line 2325
    :cond_1
    invoke-virtual {p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->clear()V

    .line 2327
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_0

    .line 2329
    new-instance v3, Landroid/graphics/RectF;

    aget-object v4, v1, v2

    sget v5, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->LEFT:I

    aget v4, v4, v5

    .line 2330
    aget-object v5, v1, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->TOP:I

    aget v5, v5, v6

    .line 2331
    aget-object v6, v1, v2

    sget v7, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->RIGHT:I

    aget v6, v6, v7

    .line 2332
    aget-object v7, v1, v2

    sget v8, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->BOTTOM:I

    aget v7, v7, v8

    .line 2329
    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2333
    .local v3, "rect":Landroid/graphics/RectF;
    invoke-virtual {p2, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->add(Ljava/lang/Object;)Z

    .line 2327
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2298
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private loadPileImgs()V
    .locals 8

    .prologue
    const/high16 v7, 0x1e500000

    .line 2623
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v0

    .line 2624
    .local v0, "imgCnt":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getCurrentStyleId()I

    move-result v6

    invoke-static {v6, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getPileStyleId(II)I

    move-result v4

    .line 2625
    .local v4, "pileId":I
    const/4 v2, 0x0

    .line 2626
    .local v2, "pileBgResourceId":I
    const/4 v5, 0x0

    .line 2628
    .local v5, "pileImgResourceId":I
    packed-switch v4, :pswitch_data_0

    .line 2683
    :goto_0
    add-int/lit8 v6, v0, -0x2

    add-int/2addr v5, v6

    .line 2684
    const/4 v3, 0x0

    .line 2685
    .local v3, "pileFrameImg":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .line 2687
    .local v1, "pileBgImg":Landroid/graphics/Bitmap;
    if-eqz v5, :cond_0

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentPileFrameId:I

    if-eq v6, v5, :cond_0

    .line 2689
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v6, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2690
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v6, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setCollagePileFramImg(Landroid/graphics/Bitmap;)V

    .line 2691
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentPileFrameId:I

    .line 2702
    :cond_0
    return-void

    .line 2631
    .end local v1    # "pileBgImg":Landroid/graphics/Bitmap;
    .end local v3    # "pileFrameImg":Landroid/graphics/Bitmap;
    :pswitch_0
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    if-ne v6, v7, :cond_1

    .line 2633
    const v5, 0x7f020126

    .line 2634
    const v2, 0x7f02014e

    .line 2635
    goto :goto_0

    .line 2638
    :cond_1
    const v5, 0x7f02013a

    .line 2639
    const v2, 0x7f020152

    .line 2641
    goto :goto_0

    .line 2644
    :pswitch_1
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    if-ne v6, v7, :cond_2

    .line 2646
    const v5, 0x7f02012b

    .line 2647
    const v2, 0x7f02014f

    .line 2648
    goto :goto_0

    .line 2651
    :cond_2
    const v5, 0x7f02013f

    .line 2652
    const v2, 0x7f020153

    .line 2654
    goto :goto_0

    .line 2657
    :pswitch_2
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    if-ne v6, v7, :cond_3

    .line 2659
    const v5, 0x7f020130

    .line 2660
    const v2, 0x7f020150

    .line 2661
    goto :goto_0

    .line 2664
    :cond_3
    const v5, 0x7f020144

    .line 2665
    const v2, 0x7f020154

    .line 2667
    goto :goto_0

    .line 2670
    :pswitch_3
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    if-ne v6, v7, :cond_4

    .line 2672
    const v5, 0x7f020135

    .line 2673
    const v2, 0x7f020151

    .line 2674
    goto :goto_0

    .line 2677
    :cond_4
    const v5, 0x7f020149

    .line 2678
    const v2, 0x7f020155

    goto :goto_0

    .line 2628
    :pswitch_data_0
    .packed-switch 0x1e200100
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private removeItem()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2790
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setDrawSelectedLine(Z)V

    .line 2791
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v0

    sget v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MIN_ITEM_CNT:I

    if-gt v0, v1, :cond_0

    .line 2794
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0601aa

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 2796
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->invalidateViewsWithThread()V

    .line 2806
    :goto_0
    return-void

    .line 2799
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->destroy(I)V

    .line 2800
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->remove(I)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 2801
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 2802
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setLayoutStyle(I)V

    .line 2804
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    .line 2805
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->setCurrentIndex(I)V

    goto :goto_0
.end method

.method private replaceItem()V
    .locals 2

    .prologue
    .line 2810
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setDrawSelectedLine(Z)V

    .line 2811
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getImageFromGallary(Z)V

    .line 2813
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSavePath:Ljava/lang/String;

    .line 2814
    return-void
.end method

.method private saveCollage(Ljava/lang/String;)Landroid/net/Uri;
    .locals 10
    .param p1, "orgFilePathNName"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 2526
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasWidth:I

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setSaveBitmapSize(I)V

    .line 2528
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->setCurrentIndex(I)V

    .line 2530
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasWidth:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2531
    .local v4, "saveBitmap":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2532
    .local v6, "saveCanvas":Landroid/graphics/Canvas;
    new-instance v7, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-direct {v7}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;-><init>()V

    .line 2533
    .local v7, "saveDrawRectList":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;
    new-instance v8, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-direct {v8, v0, v7, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;)V

    .line 2535
    .local v8, "saveDrawer":Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;
    const/4 v9, 0x0

    .line 2537
    .local v9, "uri":Landroid/net/Uri;
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    invoke-direct {p0, v0, v7}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->loadDrawRects(ILcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;)V

    .line 2538
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasWidth:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasHeight:I

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->setViewWidthHeight(II)V

    .line 2539
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getCurrentSpacingStep()I

    move-result v0

    invoke-virtual {v7, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->setSpacingStep(I)V

    .line 2540
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getCurrentRoundStep()I

    move-result v0

    invoke-virtual {v7, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->setRoundStep(I)V

    .line 2541
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getCurrentStyleId()I

    move-result v0

    invoke-virtual {v7, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->setCurrentStyleId(I)V

    .line 2542
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->isPileMode()Z

    move-result v0

    invoke-virtual {v7, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->setPileMode(Z)Z

    .line 2544
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getViewRatio()Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasWidth:I

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasHeight:I

    invoke-virtual {v8, v0, v1, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setViewSize(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;II)V

    .line 2545
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getViewRatio()Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setViewRatio(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;)V

    .line 2546
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getCollagePileFrameImg()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setCollagePileFramImg(Landroid/graphics/Bitmap;)V

    .line 2549
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getCollageBackGroundColor()I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setCollageBackGroundColor(I)V

    .line 2550
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getCollageBackGroundImg()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2552
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getCollageBackGroundImg()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getCollageBackGroundImgId()I

    move-result v1

    invoke-virtual {v8, v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setCollageBackGroundImg(Landroid/graphics/Bitmap;I)V

    .line 2555
    :cond_0
    invoke-virtual {v8, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->onDraw(Landroid/graphics/Canvas;)V

    .line 2556
    invoke-virtual {v6}, Landroid/graphics/Canvas;->save()I

    .line 2558
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2559
    .local v2, "path":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileName:Ljava/lang/String;

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->saveToImage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;)Landroid/net/Uri;

    move-result-object v9

    .line 2560
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 2561
    const/4 v4, 0x0

    .line 2562
    const/4 v7, 0x0

    .line 2564
    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->destroy()V

    .line 2566
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getViewRatio()Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setViewRatio(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;)V

    .line 2567
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->setCurrentIndex(I)V

    .line 2568
    return-object v9
.end method

.method private saveToFile()V
    .locals 3

    .prologue
    .line 2487
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    .line 2488
    new-instance v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    .line 2487
    invoke-direct {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;)V

    .line 2520
    .local v0, "asyncTask":Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2521
    return-void
.end method

.method private setAssistantLayoutVisible(I)V
    .locals 13
    .param p1, "visibility"    # I

    .prologue
    const/16 v6, 0x8

    const v2, 0x3f666666    # 0.9f

    const/4 v5, 0x1

    const/high16 v1, 0x3f800000    # 1.0f

    .line 290
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cheus, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : setAssistantLayoutVisible : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 291
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const v4, 0x7f0900bb

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    .line 292
    .local v11, "assistantLayout":Landroid/view/ViewGroup;
    if-ne p1, v6, :cond_0

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v3

    if-eq v3, v6, :cond_0

    .line 293
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 294
    .local v10, "animation":Landroid/view/animation/AnimationSet;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getAnimX(Z)F

    move-result v6

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v1

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 295
    .local v0, "scaleAnim":Landroid/view/animation/ScaleAnimation;
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const v2, 0x3f19999a    # 0.6f

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 296
    .local v9, "alphaAnim":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 297
    invoke-virtual {v10, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 298
    new-instance v1, Landroid/view/animation/interpolator/SineEaseInOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/SineEaseInOut;-><init>()V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 299
    const-wide/16 v2, 0xaa

    invoke-virtual {v10, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 300
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const v2, 0x7f0900b5

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 301
    .local v12, "middleButtonLayout":Landroid/view/View;
    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$2;

    invoke-direct {v1, p0, v12}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$2;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Landroid/view/View;)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 339
    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Landroid/view/View;->setAlpha(F)V

    .line 340
    invoke-virtual {v11, v10}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 344
    .end local v0    # "scaleAnim":Landroid/view/animation/ScaleAnimation;
    .end local v9    # "alphaAnim":Landroid/view/animation/AlphaAnimation;
    .end local v10    # "animation":Landroid/view/animation/AnimationSet;
    .end local v12    # "middleButtonLayout":Landroid/view/View;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setVisibilityQuickly(I)V

    .line 345
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setCollageArrowPos()V

    .line 347
    return-void
.end method

.method private setBackGroundImage(ILandroid/view/View;)V
    .locals 8
    .param p1, "bgImgId"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    const v5, 0x7f0200e4

    const/4 v6, 0x1

    .line 2707
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentBackgroundID:I

    if-eq v3, p1, :cond_0

    .line 2709
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentBackgroundID:I

    .line 2710
    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setSaveable(Z)Z

    .line 2714
    :cond_0
    const/4 v0, 0x0

    .line 2715
    .local v0, "backgroundImg":Landroid/graphics/Bitmap;
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentBackgroundID:I

    const/high16 v4, 0x1e400000

    sub-int/2addr v3, v4

    add-int/2addr v3, v5

    add-int/lit8 v1, v3, -0x1

    .line 2717
    .local v1, "bgId":I
    if-ge v1, v5, :cond_1

    if-eqz p2, :cond_1

    .line 2720
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cheus, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : setBackGroundImage : 01-1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2721
    const/4 v3, 0x2

    new-array v2, v3, [I

    .line 2722
    .local v2, "pos":[I
    invoke-virtual {p2, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2723
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    aget v4, v2, v7

    aget v5, v2, v6

    invoke-virtual {v3, v6, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->show(ZIIZ)V

    .line 2724
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->getCurrentColor()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setBackGroundColor(I)V

    .line 2740
    .end local v2    # "pos":[I
    :goto_0
    return-void

    .line 2726
    :cond_1
    if-ge v1, v5, :cond_2

    if-nez p2, :cond_2

    .line 2728
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cheus, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : setBackGroundImage : 01-2"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2729
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->getCurrentColor()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setBackGroundColor(I)V

    goto :goto_0

    .line 2733
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cheus, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : setBackGroundImage : 01-3"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2734
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->backgroundWithAspectRatio(I)I

    move-result v1

    .line 2735
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2737
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setCollageBackGroundImg(Landroid/graphics/Bitmap;I)V

    .line 2738
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v3, v7, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->show(ZZ)Z

    goto :goto_0
.end method

.method private setButtonSelected()V
    .locals 5

    .prologue
    const v4, 0x1e300002

    const/4 v3, 0x1

    .line 351
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 369
    :goto_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    if-eq v0, v4, :cond_0

    .line 371
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->moveArrowToCurrentViewBtn()V

    .line 373
    :cond_0
    return-void

    .line 354
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/high16 v1, 0x1e300000

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IIZ)V

    goto :goto_0

    .line 358
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1e300001

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IIZ)V

    goto :goto_0

    .line 362
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IIZ)V

    goto :goto_0

    .line 366
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1e300003

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentBackgroundID:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IIZ)V

    goto :goto_0

    .line 351
    :pswitch_data_0
    .packed-switch 0x1e300000
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setLayoutStyle(I)V
    .locals 6
    .param p1, "layoutStyle"    # I

    .prologue
    const v5, 0x1e200100

    .line 2345
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v0

    .line 2346
    .local v0, "imgCnt":I
    const/4 v1, -0x1

    .line 2347
    .local v1, "pileId":I
    const/4 v2, 0x5

    .line 2348
    .local v2, "pileOrder":I
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cheus, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : setLayoutStyle 00-0 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentImgCnt:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2350
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentImgCnt:I

    if-eqz v3, :cond_0

    if-nez v0, :cond_2

    .line 2352
    :cond_0
    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getPileStyleId(II)I

    move-result v1

    .line 2353
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cheus, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : setLayoutStyle 00-0 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-int v4, v1, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2368
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 2383
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getMaxGridStyleId(I)I

    move-result v3

    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 2399
    :goto_1
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    if-eq v3, p1, :cond_1

    .line 2401
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setSaveable(Z)Z

    .line 2402
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    .line 2406
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->setCurrentStyleId(I)V

    .line 2407
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-direct {p0, v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->loadDrawRects(ILcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;)V

    .line 2408
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->fitRectSizeRatio(Z)V

    .line 2409
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setPileMode()Z

    .line 2410
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentImgCnt:I

    .line 2411
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->invalidateViewsWithThread()V

    .line 2412
    return-void

    .line 2355
    :cond_2
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentImgCnt:I

    if-eq v3, v0, :cond_3

    .line 2357
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getCurrentStyleId()I

    move-result v3

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentImgCnt:I

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getPileStyleId(II)I

    move-result v1

    .line 2358
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cheus, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : setLayoutStyle 00-1 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-int v4, v1, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 2362
    :cond_3
    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getPileStyleId(II)I

    move-result v1

    .line 2363
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cheus, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : setLayoutStyle 00-2 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-int v4, v1, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2371
    :pswitch_0
    add-int/lit8 v2, v2, -0x1

    .line 2373
    :pswitch_1
    add-int/lit8 v2, v2, -0x1

    .line 2375
    :pswitch_2
    add-int/lit8 v2, v2, -0x1

    .line 2377
    :pswitch_3
    add-int/lit8 v2, v2, -0x1

    .line 2378
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cheus, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : setLayoutStyle 01 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2379
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getMaxGridStyleId(I)I

    move-result v3

    add-int p1, v3, v2

    .line 2380
    goto/16 :goto_1

    .line 2368
    :pswitch_data_0
    .packed-switch 0x1e200100
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setPileMode()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3386
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getCurrentStyleId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->isPileStyle(II)I

    move-result v2

    if-gez v2, :cond_2

    .line 3388
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    .line 3394
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    if-eqz v3, :cond_0

    move v0, v1

    :cond_0
    const v3, 0x1e300002

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setButtonEnable(ZI)V

    .line 3397
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->setPileMode(Z)Z

    .line 3399
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    if-eqz v0, :cond_1

    .line 3402
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    .line 3403
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->loadPileImgs()V

    .line 3411
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    return v0

    .line 3392
    :cond_2
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    goto :goto_0
.end method

.method private setSaveBitmapSize(I)V
    .locals 3
    .param p1, "nWidth"    # I

    .prologue
    .line 2573
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->getMaxSizeIndex()I

    move-result v0

    .line 2575
    .local v0, "maxSizeIndex":I
    if-gez v0, :cond_0

    .line 2588
    :goto_0
    return-void

    .line 2578
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getCurrentStyleId()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->isPileStyle(II)I

    move-result v1

    if-gez v1, :cond_1

    .line 2580
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgWidth()I

    move-result v2

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgHeight()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasWidth:I

    .line 2581
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasWidth:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getCanvasProportionRatio()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasHeight:I

    goto :goto_0

    .line 2585
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getCollagePileFrameImg()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasWidth:I

    .line 2586
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getCollagePileFrameImg()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveCanvasHeight:I

    goto :goto_0
.end method

.method private setSaveFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 2473
    const-string v0, ".jpg"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ".JPG"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2475
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2478
    :cond_0
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileName:Ljava/lang/String;

    .line 2480
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileName:Ljava/lang/String;

    return-object v0
.end method

.method private setSaveable(Z)Z
    .locals 3
    .param p1, "isAbleSave"    # Z

    .prologue
    const/4 v2, 0x0

    .line 3321
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : setSaveable : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 3323
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsAbleSave:Z

    .line 3324
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveButtonHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 3325
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveButtonHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 3326
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setSave(Z)V

    .line 3327
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsAbleSave:Z

    return v0
.end method

.method private showAssistantLayout(I)V
    .locals 12
    .param p1, "assistantType"    # I

    .prologue
    const v1, 0x3f666666    # 0.9f

    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 271
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    if-eqz v3, :cond_0

    .line 273
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getCurrentRoundStep()I

    move-result v4

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getCurrentSpacingStep()I

    move-result v6

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->updateCurrentCollageStep(II)V

    .line 274
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    invoke-virtual {v3, p1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->showAssistantLayout(I)V

    .line 275
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setCollageArrowPos()V

    .line 276
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const v4, 0x7f0900bb

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    .line 277
    .local v11, "assistantLayout":Landroid/view/ViewGroup;
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 278
    .local v10, "animation":Landroid/view/animation/AnimationSet;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getAnimX(Z)F

    move-result v6

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 279
    .local v0, "scaleAnim":Landroid/view/animation/ScaleAnimation;
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const v1, 0x3f19999a    # 0.6f

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 280
    .local v9, "alphaAnim":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 281
    invoke-virtual {v10, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 282
    new-instance v1, Landroid/view/animation/interpolator/SineEaseInOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/SineEaseInOut;-><init>()V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 283
    const-wide/16 v2, 0xaa

    invoke-virtual {v10, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 284
    invoke-virtual {v11, v10}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 286
    .end local v0    # "scaleAnim":Landroid/view/animation/ScaleAnimation;
    .end local v9    # "alphaAnim":Landroid/view/animation/AlphaAnimation;
    .end local v10    # "animation":Landroid/view/animation/AnimationSet;
    .end local v11    # "assistantLayout":Landroid/view/ViewGroup;
    :cond_0
    return-void
.end method

.method private showKeyboard(Landroid/view/View;Z)Z
    .locals 4
    .param p1, "textView"    # Landroid/view/View;
    .param p2, "show"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1076
    if-nez p1, :cond_0

    .line 1090
    :goto_0
    return v1

    .line 1079
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    .line 1080
    const-string v3, "input_method"

    .line 1079
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1082
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz p2, :cond_1

    .line 1084
    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1085
    const/4 v1, 0x1

    goto :goto_0

    .line 1089
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method private startEffectRotateMode(I)V
    .locals 9
    .param p1, "modeToGo"    # I

    .prologue
    .line 2825
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 2827
    .local v4, "time":J
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    invoke-virtual {v6, v7}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->isIndexInList(I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2862
    :cond_0
    :goto_0
    return-void

    .line 2831
    :cond_1
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 2832
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->clearCollageAnimationView()V

    .line 2834
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    invoke-virtual {v6, v7}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 2835
    .local v3, "effectItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 2838
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2839
    .local v1, "effectBitmap":Landroid/graphics/Bitmap;
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;-><init>()V

    .line 2846
    .local v2, "effectImageData":Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getUri()Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x5

    invoke-virtual {v2, v6, v1, v7, v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setDecodedImage(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/net/Uri;I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2858
    :cond_2
    :goto_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v6, :cond_0

    .line 2859
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-virtual {v6, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->setImageData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 2860
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeViewStatus(II)V

    goto :goto_0

    .line 2848
    :catch_0
    move-exception v0

    .line 2850
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 2851
    if-eqz v1, :cond_2

    .line 2853
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " : W/H "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private startSuffleAnimation()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 3138
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 3139
    .local v6, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->size()I

    move-result v8

    if-lt v2, v8, :cond_0

    .line 3153
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v8

    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setShuffleDrawRects(I)V

    .line 3155
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3156
    .local v1, "drawRectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/RectF;>;"
    const/4 v2, 0x0

    :goto_1
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->size()I

    move-result v8

    if-lt v2, v8, :cond_1

    .line 3161
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 3162
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;>;"
    const/4 v2, 0x0

    .line 3163
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_2

    .line 3173
    const/16 v8, 0xfa

    new-instance v9, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;

    invoke-direct {v9, p0, v3, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {p0, v8, v11, v3, v9}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->startCollageAnimation(IZLjava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;)V

    .line 3214
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->invalidateViews()V

    .line 3215
    return-void

    .line 3141
    .end local v1    # "drawRectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/RectF;>;"
    .end local v3    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;>;"
    :cond_0
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v8, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 3142
    .local v7, "temp":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v8, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v0

    .line 3143
    .local v0, "drawRect":Landroid/graphics/RectF;
    new-instance v8, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->animationMoveItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 3144
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->animationMoveItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    new-instance v9, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    invoke-direct {v9}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;-><init>()V

    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setDecodeInfo(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;)V

    .line 3145
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->animationMoveItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImage(Landroid/graphics/Bitmap;)V

    .line 3146
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->animationMoveItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    new-instance v9, Landroid/graphics/Rect;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getmDrawRect()Landroid/graphics/Rect;

    move-result-object v10

    invoke-direct {v9, v10}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setmDrawRect(Landroid/graphics/Rect;)V

    .line 3148
    new-instance v5, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->animationMoveItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-direct {v5, v8, v9, v10, v11}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;Z)V

    .line 3149
    .local v5, "object":Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v9

    invoke-virtual {v5, v8, v9, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->InitMoveObject(FFLandroid/graphics/RectF;)I

    .line 3150
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3139
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 3158
    .end local v0    # "drawRect":Landroid/graphics/RectF;
    .end local v5    # "object":Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;
    .end local v7    # "temp":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    .restart local v1    # "drawRectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/RectF;>;"
    :cond_1
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v8, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3156
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 3163
    .restart local v3    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;>;"
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    .line 3165
    .restart local v5    # "object":Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;
    new-instance v4, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;

    invoke-direct {v4, p0, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;)V

    .line 3166
    .local v4, "oInfo":Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/RectF;

    invoke-virtual {v4, v8}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->setTargetDrawRect(Landroid/graphics/RectF;)V

    .line 3167
    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->setInterpolatedValues()V

    .line 3168
    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->setKeepingSamePosition()V

    .line 3169
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3170
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2
.end method

.method private startSwapAnimation(Landroid/view/MotionEvent;)V
    .locals 26
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 3218
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 3219
    .local v17, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 3220
    .local v4, "drawRectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/RectF;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->getStartIndex()I

    move-result v19

    .line 3221
    .local v19, "startIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getTouchedIndex(Landroid/view/MotionEvent;)I

    move-result v6

    .line 3222
    .local v6, "dstIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v13

    .line 3223
    .local v13, "moveObjectTargetDrawRect":Landroid/graphics/RectF;
    const/16 v23, -0x1

    move/from16 v0, v23

    if-gt v6, v0, :cond_1

    .line 3225
    const/16 v23, -0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    .line 3226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->resetDragIndex()V

    .line 3227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    move-object/from16 v23, v0

    if-eqz v23, :cond_0

    .line 3229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->EndMoveObject()V

    .line 3230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->destroy()V

    .line 3231
    const/16 v23, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    .line 3317
    :cond_0
    :goto_0
    return-void

    .line 3235
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    move-object/from16 v23, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3236
    invoke-virtual {v4, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->getDragFromIndex()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v22

    .line 3241
    .local v22, "upperObjectTargetDrawRect":Landroid/graphics/RectF;
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    .line 3242
    .local v5, "dst":Landroid/graphics/RectF;
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 3243
    .local v10, "m":Landroid/graphics/Matrix;
    const/high16 v18, 0x3f000000    # 0.5f

    .line 3244
    .local v18, "scale":F
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/RectF;->centerX()F

    move-result v23

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/RectF;->centerY()F

    move-result v24

    move/from16 v0, v18

    move/from16 v1, v18

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 3245
    move-object/from16 v0, v22

    invoke-virtual {v10, v5, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 3246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 3247
    .local v20, "temp":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 3248
    .local v11, "moveBitmap":Landroid/graphics/Bitmap;
    new-instance v12, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-direct {v12, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;-><init>(Landroid/content/Context;)V

    .line 3249
    .local v12, "moveItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    new-instance v23, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    invoke-direct/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setDecodeInfo(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;)V

    .line 3250
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImage(Landroid/graphics/Bitmap;)V

    .line 3251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    move-object/from16 v23, v0

    invoke-virtual {v12}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getmDrawRect()Landroid/graphics/Rect;

    move-result-object v24

    const/16 v25, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->calculateRectSize(Landroid/graphics/Rect;Landroid/graphics/RectF;Z)Landroid/graphics/Rect;

    move-result-object v14

    .line 3252
    .local v14, "newRect":Landroid/graphics/Rect;
    invoke-virtual {v12, v14}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setmDrawRect(Landroid/graphics/Rect;)V

    .line 3253
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;-><init>()V

    .line 3255
    .local v8, "iData":Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getUri()Landroid/net/Uri;

    move-result-object v24

    const/16 v25, 0x5

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v8, v0, v11, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setDecodedImage(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/net/Uri;I)V

    .line 3257
    new-instance v21, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v1, v8, v12, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;Z)V

    .line 3258
    .local v21, "upperObject":Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;
    invoke-virtual {v5}, Landroid/graphics/RectF;->centerX()F

    move-result v23

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v24

    move-object/from16 v0, v21

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->InitMoveObject(FFLandroid/graphics/RectF;)I

    .line 3260
    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3261
    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3263
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 3264
    .local v9, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;>;"
    const/4 v7, 0x0

    .line 3265
    .local v7, "i":I
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :goto_1
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-nez v23, :cond_2

    .line 3275
    const/16 v23, 0x15e

    const/16 v24, 0x1

    new-instance v25, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$27;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    move/from16 v2, v19

    invoke-direct {v0, v1, v2, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$27;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;II)V

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v9, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->startCollageAnimation(IZLjava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;)V

    .line 3316
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->invalidateViews()V

    goto/16 :goto_0

    .line 3265
    :cond_2
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    .line 3267
    .local v16, "object":Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;
    new-instance v15, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;)V

    .line 3268
    .local v15, "oInfo":Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/graphics/RectF;

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->setTargetDrawRect(Landroid/graphics/RectF;)V

    .line 3269
    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->setInterpolatedValues()V

    .line 3270
    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->setAlphaAnimation()V

    .line 3271
    invoke-virtual {v9, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3272
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method private touchMainButton(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v4, 0x1e300002

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 220
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 223
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/high16 v1, 0x1e300000

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x1e300001

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x1e300003

    if-eq v0, v1, :cond_0

    .line 224
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setVisibilityQuickly(I)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setCollageArrowPos()V

    .line 230
    :cond_1
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->hideSubMenu(Z)Z

    .line 231
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 232
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 247
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 248
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    .line 249
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setButtonSelected()V

    .line 267
    :goto_1
    return-void

    .line 237
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initSubButtons()V

    goto :goto_0

    .line 242
    :pswitch_1
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->showAssistantLayout(I)V

    .line 243
    invoke-direct {p0, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->checkHelpPopup(I)V

    goto :goto_0

    .line 253
    :cond_2
    const/high16 v0, 0x1e110000

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 255
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->hideSubMenu(Z)Z

    .line 256
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    if-eqz v0, :cond_4

    .line 258
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_3

    .line 259
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 260
    :cond_3
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    goto :goto_1

    .line 262
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 263
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    goto :goto_1

    .line 232
    :pswitch_data_0
    .packed-switch 0x1e300000
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x1

    .line 1368
    const/4 v2, 0x0

    .line 1369
    .local v2, "returnValue":Z
    const/4 v0, 0x0

    .line 1370
    .local v0, "drawAnimation":Z
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->runningCollageAnimation()Z

    move-result v0

    .line 1371
    if-eqz v0, :cond_1

    .line 1476
    :cond_0
    :goto_0
    return v6

    .line 1373
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->isAnimating()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1376
    :cond_2
    const/4 v1, 0x0

    .line 1379
    .local v1, "isModifiable":Z
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v3, v6, :cond_4

    .line 1381
    :cond_3
    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->hideSubMenu(Z)Z

    .line 1395
    :cond_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->isDrawSelectedLine()Z

    move-result v3

    if-eqz v3, :cond_5

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v4, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getTouchedIndex(Landroid/view/MotionEvent;)I

    move-result v4

    if-ne v3, v4, :cond_5

    .line 1396
    const/4 v1, 0x1

    .line 1398
    :cond_5
    if-eqz v1, :cond_7

    .line 1400
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mPinchZoomListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;

    if-eqz v3, :cond_6

    .line 1402
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mPinchZoomListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;

    invoke-virtual {v3, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1405
    :cond_6
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v3, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1412
    :cond_7
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 1415
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v3, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getTouchedIndex(Landroid/view/MotionEvent;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchDownIndex:I

    .line 1416
    if-nez v1, :cond_8

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDraging:Z

    if-eqz v3, :cond_0

    .line 1418
    :cond_8
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    .line 1420
    goto :goto_0

    .line 1423
    :pswitch_1
    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDraging:Z

    if-eqz v3, :cond_9

    .line 1425
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    .line 1426
    goto :goto_0

    .line 1427
    :cond_9
    if-eqz v1, :cond_0

    .line 1428
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchDownIndex:I

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    if-ne v3, v4, :cond_0

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v4, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getTouchedIndex(Landroid/view/MotionEvent;)I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 1430
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    .line 1432
    goto :goto_0

    .line 1435
    :pswitch_2
    if-nez v1, :cond_a

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDraging:Z

    if-eqz v3, :cond_b

    .line 1437
    :cond_a
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    .line 1440
    :cond_b
    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDraging:Z

    if-nez v3, :cond_e

    .line 1442
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v3, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getTouchedIndex(Landroid/view/MotionEvent;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    .line 1443
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->isShown()Z

    move-result v3

    if-nez v3, :cond_c

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    if-eq v3, v7, :cond_c

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchDownIndex:I

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    if-ne v3, v4, :cond_c

    .line 1445
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->setCurrentIndex(I)V

    .line 1446
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setDrawSelectedLine(Z)V

    .line 1447
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->show(ZLandroid/graphics/RectF;)Z

    .line 1466
    :goto_1
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchDownIndex:I

    goto/16 :goto_0

    .line 1452
    :cond_c
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchDownIndex:I

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    if-eq v3, v4, :cond_d

    .line 1453
    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setSaveable(Z)Z

    .line 1454
    :cond_d
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    .line 1455
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->setCurrentIndex(I)V

    .line 1456
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setDrawSelectedLine(Z)V

    goto :goto_1

    .line 1462
    :cond_e
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsMoving:Z

    .line 1463
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDraging:Z

    goto :goto_1

    .line 1471
    :pswitch_3
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchDownIndex:I

    goto/16 :goto_0

    .line 1412
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public backPressed()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1704
    const/4 v0, 0x0

    .line 1705
    .local v0, "isHided":Z
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->hideSubMenu(Z)Z

    move-result v1

    or-int/2addr v0, v1

    .line 1706
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : backPressed : 00 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1707
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    move-result v1

    or-int/2addr v0, v1

    .line 1708
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : backPressed : 01 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1711
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x11100000

    if-eq v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    if-eqz v1, :cond_3

    .line 1713
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_1

    .line 1714
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 1715
    :cond_1
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    .line 1725
    :goto_0
    if-nez v0, :cond_2

    .line 1728
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I

    if-ne v1, v5, :cond_5

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_5

    .line 1730
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const/high16 v2, 0x2e000000

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->changeViewStatus(II)V

    .line 1731
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I

    .line 1756
    :cond_2
    :goto_1
    return-void

    .line 1719
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_4

    .line 1720
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 1721
    :cond_4
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    goto :goto_0

    .line 1735
    :cond_5
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileUri:Landroid/net/Uri;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledSave()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1737
    :cond_6
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I

    .line 1739
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v1, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1741
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v1, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1742
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I

    goto :goto_1
.end method

.method public backgroundWithAspectRatio(I)I
    .locals 2
    .param p1, "bgId"    # I

    .prologue
    .line 2743
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    const/high16 v1, 0x1e500000

    if-eq v0, v1, :cond_1

    .line 2744
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2745
    const-string v1, " : setBackGroundImage : 01-4"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2744
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2746
    packed-switch p1, :pswitch_data_0

    .line 2778
    :cond_0
    :goto_0
    :pswitch_0
    return p1

    .line 2748
    :pswitch_1
    const p1, 0x7f0200ff

    .line 2749
    goto :goto_0

    .line 2751
    :pswitch_2
    const p1, 0x7f020100

    .line 2752
    goto :goto_0

    .line 2754
    :pswitch_3
    const p1, 0x7f020101

    .line 2755
    goto :goto_0

    .line 2757
    :pswitch_4
    const p1, 0x7f020102

    goto :goto_0

    .line 2760
    :cond_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    const v1, 0x1e500001

    if-eq v0, v1, :cond_0

    .line 2761
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2762
    const-string v1, " : setBackGroundImage : 01-4"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2761
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2763
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 2765
    :pswitch_5
    const p1, 0x7f0200f8

    .line 2766
    goto :goto_0

    .line 2768
    :pswitch_6
    const p1, 0x7f0200fa

    .line 2769
    goto :goto_0

    .line 2771
    :pswitch_7
    const p1, 0x7f0200fb

    .line 2772
    goto :goto_0

    .line 2774
    :pswitch_8
    const p1, 0x7f0200fe

    goto :goto_0

    .line 2746
    nop

    :pswitch_data_0
    .packed-switch 0x7f0200f8
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 2763
    :pswitch_data_1
    .packed-switch 0x7f0200ff
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public backupCurrentState()V
    .locals 7

    .prologue
    .line 3112
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentTouchedIndex:I

    .line 3113
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    .line 3114
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    .line 3115
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentBackgroundID:I

    .line 3116
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->getCurrentColor()I

    move-result v5

    .line 3117
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentSaveSize:I

    .line 3112
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->backupCurrentState(IIIIII)V

    .line 3118
    return-void
.end method

.method public changeImage(I)V
    .locals 2
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 1820
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mInitSplit:Z

    if-nez v0, :cond_0

    .line 1821
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initSplitView()V

    .line 1823
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "changeImage view w,h:["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getImageEditViewHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1824
    return-void
.end method

.method public doShuffle()V
    .locals 1

    .prologue
    .line 3026
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->callOnClick()Z

    .line 3027
    return-void
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x0

    return v0
.end method

.method public getStatusHeight()I
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    return v0
.end method

.method public initActionbar()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x1

    .line 559
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 561
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 564
    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$5;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    .line 561
    invoke-virtual {v0, v3, v2, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 593
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 594
    const/4 v1, 0x4

    .line 597
    new-instance v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$6;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    .line 593
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 632
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 633
    const/4 v1, 0x5

    .line 636
    new-instance v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$7;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$7;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    .line 632
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 668
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 669
    const/16 v1, 0x10

    .line 672
    new-instance v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$8;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$8;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    .line 668
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 704
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 705
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 708
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    .line 710
    :cond_0
    return-void
.end method

.method public initButtons()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 507
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_1

    .line 509
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 511
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 539
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x11100000

    if-eq v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    if-eqz v1, :cond_3

    .line 541
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 542
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    .line 551
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setPileMode()Z

    .line 552
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 554
    .end local v0    # "i":I
    :cond_1
    return-void

    .line 513
    .restart local v0    # "i":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/high16 v2, 0x1e300000

    add-int/2addr v2, v0

    .line 514
    new-instance v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$4;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$4;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    .line 513
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 511
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 546
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initMiddleButton()V

    .line 547
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 548
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    goto :goto_1
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 723
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 727
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initShareViaDialog()V

    .line 728
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initSetAsDialog()V

    .line 729
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initSaveYesNoCancelForFinish()V

    .line 732
    return-void
.end method

.method public initEffect()V
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mInitSplit:Z

    if-nez v0, :cond_0

    .line 197
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initSplitView()V

    .line 198
    :cond_0
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 3605
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 1816
    return-void
.end method

.method public initTrayLayout()V
    .locals 0

    .prologue
    .line 1150
    return-void
.end method

.method public initView()V
    .locals 1

    .prologue
    .line 716
    const/high16 v0, 0x2c000000

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->checkHelpPopup(I)V

    .line 717
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1620
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->destroy()V

    .line 1621
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 1622
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    .line 1623
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDrawCollage:Z

    .line 1628
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 1348
    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1349
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1351
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDrawCollage:Z

    if-eqz v0, :cond_1

    .line 1353
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    if-eqz v0, :cond_0

    .line 1355
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1358
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    if-eqz v0, :cond_1

    .line 1360
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->drawObject(Landroid/graphics/Canvas;)V

    .line 1363
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1364
    return-void
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x1

    .line 1761
    const/16 v0, 0x17

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-ne p1, v0, :cond_3

    .line 1763
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 1764
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    .line 1767
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_2

    .line 1768
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 1808
    :cond_2
    :goto_0
    return v1

    .line 1772
    :cond_3
    if-ne p1, v2, :cond_5

    .line 1786
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileUri:Landroid/net/Uri;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledSave()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1787
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I

    .line 1789
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->backPressed()V

    goto :goto_0

    .line 1793
    :cond_5
    const/16 v0, 0x52

    if-ne p1, v0, :cond_2

    .line 1794
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    goto :goto_0
.end method

.method public onLayout()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1829
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cheus, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : onLayout : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1830
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mOnLayout:Z

    .line 1831
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getImageEditViewWidth()I

    move-result v1

    .line 1832
    .local v1, "viewWidth":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getImageEditViewHeight()I

    move-result v0

    .line 1833
    .local v0, "viewHeight":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v2, v4, v4, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 1834
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setViewSize(II)V

    .line 1836
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setButtonSelected()V

    .line 1839
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 1841
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1844
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$20;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$20;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1851
    return-void
.end method

.method public onOptionsItemSelected(I)V
    .locals 3
    .param p1, "optionItemID"    # I

    .prologue
    const/4 v2, 0x1

    .line 1856
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onOptionsItemSelected : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledSave()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1857
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->hideSubMenu(Z)Z

    .line 1858
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I

    .line 1859
    packed-switch p1, :pswitch_data_0

    .line 1907
    :goto_0
    :pswitch_0
    return-void

    .line 1862
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->addImage()V

    goto :goto_0

    .line 1871
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledSave()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1876
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->saveToFile()V

    goto :goto_0

    .line 1880
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    goto :goto_0

    .line 1889
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledSave()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1894
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->saveToFile()V

    goto :goto_0

    .line 1898
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    goto :goto_0

    .line 1859
    nop

    :pswitch_data_0
    .packed-switch 0x7f090155
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 1596
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onResume : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1597
    sget-object v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->DebugTag:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1599
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    if-eqz v0, :cond_0

    .line 1601
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1603
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$18;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$18;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    .line 1608
    const-wide/16 v2, 0xfa

    .line 1603
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1613
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_1

    .line 1614
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 1616
    :cond_1
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 1483
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1522
    :cond_0
    :goto_0
    return v4

    .line 1487
    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsMoving:Z

    .line 1488
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mPinchZoomListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->isZoom()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1489
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getTouchedIndex(Landroid/view/MotionEvent;)I

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->moveItemStart(IFF)Z

    goto :goto_0

    .line 1493
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->StartMoveObject(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mPinchZoomListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->isZoom()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1495
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getTouchedIndex(Landroid/view/MotionEvent;)I

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->moveItem(IFF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1497
    invoke-direct {p0, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setSaveable(Z)Z

    .line 1501
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsMoving:Z

    goto :goto_0

    .line 1509
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsMoving:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mPinchZoomListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->isZoom()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1511
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->moveItemEnd()V

    .line 1514
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsDraging:Z

    if-eqz v0, :cond_0

    .line 1516
    invoke-direct {p0, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->EndMoveObject(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 1483
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public refreshView()V
    .locals 0

    .prologue
    .line 3611
    return-void
.end method

.method public reloadLayoutStyle()V
    .locals 1

    .prologue
    .line 2339
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setLayoutStyle(I)V

    .line 2340
    return-void
.end method

.method public setBackGroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 2783
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setSaveable(Z)Z

    .line 2784
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setCollageBackGroundColor(I)V

    .line 2786
    return-void
.end method

.method protected setCollageRatio(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 455
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    if-eq v3, p1, :cond_0

    .line 457
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    .line 459
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setSaveable(Z)Z

    .line 462
    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    if-eqz v3, :cond_0

    .line 464
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->loadPileImgs()V

    .line 465
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-direct {p0, v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->loadDrawRects(ILcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;)V

    .line 470
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cheus, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : setCollageRatio : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 472
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    packed-switch v3, :pswitch_data_0

    .line 491
    const/high16 v3, 0x1e500000

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentProportionID:I

    .line 492
    sget-object v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 495
    .local v2, "newRatio":Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getCollageBackGroundImgId()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->backgroundWithAspectRatio(I)I

    move-result v1

    .line 496
    .local v1, "bgId":I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getCollageBackGroundImgId()I

    move-result v3

    if-eq v1, v3, :cond_1

    .line 497
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 498
    .local v0, "backgroundImg":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setCollageBackGroundImg(Landroid/graphics/Bitmap;I)V

    .line 500
    .end local v0    # "backgroundImg":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v3, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setViewRatio(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;)V

    .line 501
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->clearCollageAnimationView()V

    .line 502
    return-void

    .line 475
    .end local v1    # "bgId":I
    .end local v2    # "newRatio":Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;
    :pswitch_0
    sget-object v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 476
    .restart local v2    # "newRatio":Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;
    goto :goto_0

    .line 487
    .end local v2    # "newRatio":Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;
    :pswitch_1
    sget-object v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_9_TO_16:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 488
    .restart local v2    # "newRatio":Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;
    goto :goto_0

    .line 472
    nop

    :pswitch_data_0
    .packed-switch 0x1e500000
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setConfigurationChanged()V
    .locals 4

    .prologue
    .line 1632
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : setConfigurationChanged : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1633
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 1634
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 1635
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 1636
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_0

    .line 1637
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 1643
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    if-eqz v0, :cond_1

    .line 1645
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->configurationChangedForCollage()V

    .line 1648
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    const v1, 0x1e300002

    if-ne v0, v1, :cond_1

    .line 1650
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->showAssistantLayout(I)V

    .line 1651
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mAssistantManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setCollageArrowPos()V

    .line 1656
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    if-eqz v0, :cond_2

    .line 1658
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getViewRatio()Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setViewSize(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;II)V

    .line 1659
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->onConfigurationChanged()V

    .line 1662
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    if-eqz v0, :cond_3

    .line 1665
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mModifyPopup:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1667
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$19;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$19;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V

    .line 1672
    const-wide/16 v2, 0xfa

    .line 1667
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1677
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-eqz v0, :cond_4

    .line 1679
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->configurationChanged()V

    .line 1682
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMoveObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    if-eqz v0, :cond_5

    .line 1684
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->EndMoveObject(Landroid/view/MotionEvent;)V

    .line 1687
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initMiddleButton()V

    .line 1689
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMiddleBtnVisible:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mIsPileMode:Z

    if-nez v0, :cond_6

    .line 1691
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 1699
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPopupLayout()V

    .line 1700
    return-void

    .line 1695
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    goto :goto_0
.end method

.method public setCurrentState(IIIIII)V
    .locals 1
    .param p1, "currentTouchedIndex"    # I
    .param p2, "currentProportionID"    # I
    .param p3, "currentLayout"    # I
    .param p4, "currentBackgroundID"    # I
    .param p5, "currentBackgroundColor"    # I
    .param p6, "currentSaveSize"    # I

    .prologue
    .line 3127
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setCollageRatio(I)V

    .line 3128
    invoke-direct {p0, p3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setLayoutStyle(I)V

    .line 3129
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0, p5}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setCurrentColor(I)V

    .line 3130
    const/4 v0, 0x0

    invoke-direct {p0, p4, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setBackGroundImage(ILandroid/view/View;)V

    .line 3131
    iput p6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentSaveSize:I

    .line 3132
    return-void
.end method

.method public setImageMultiGridView(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 3615
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : setImageMultiGridView : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->index:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 3616
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v1

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->MAX_ITEM_CNT:I

    if-gt v1, v2, :cond_0

    .line 3620
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 3623
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentLayout:I

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setLayoutStyle(I)V

    .line 3624
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setSaveable(Z)Z

    .line 3625
    return-void

    .line 3617
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->remove(I)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    goto :goto_0

    .line 3621
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImage()V

    .line 3620
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public setShuffleBackGroundImage()V
    .locals 12

    .prologue
    const/high16 v8, 0x1e400000

    const-wide v10, 0x406fe00000000000L    # 255.0

    .line 3031
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3032
    .local v0, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getBgImgCnt()I

    move-result v6

    .line 3034
    .local v6, "maxBgCnt":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-lt v5, v6, :cond_0

    .line 3039
    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 3042
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 3044
    .local v1, "bgId":I
    if-ne v1, v8, :cond_1

    .line 3046
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    mul-double/2addr v8, v10

    double-to-int v4, v8

    .line 3047
    .local v4, "colorR":I
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    mul-double/2addr v8, v10

    double-to-int v3, v8

    .line 3048
    .local v3, "colorG":I
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    mul-double/2addr v8, v10

    double-to-int v2, v8

    .line 3050
    .local v2, "colorB":I
    invoke-static {v4, v3, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setBackGroundColor(I)V

    .line 3058
    .end local v2    # "colorB":I
    .end local v3    # "colorG":I
    .end local v4    # "colorR":I
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3059
    const/4 v0, 0x0

    .line 3061
    return-void

    .line 3036
    .end local v1    # "bgId":I
    :cond_0
    add-int v7, v8, v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3034
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 3054
    .restart local v1    # "bgId":I
    :cond_1
    const/4 v7, 0x0

    invoke-direct {p0, v1, v7}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setBackGroundImage(ILandroid/view/View;)V

    goto :goto_1
.end method

.method public setShuffleDrawRects(I)V
    .locals 4
    .param p1, "style"    # I

    .prologue
    .line 3092
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3094
    .local v0, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getMaxGridStyleNum(I)I

    move-result v2

    .line 3097
    .local v2, "max":I
    if-nez v2, :cond_0

    .line 3107
    :goto_0
    return-void

    .line 3100
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_1

    .line 3104
    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 3106
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setLayoutStyle(I)V

    goto :goto_0

    .line 3101
    :cond_1
    const/high16 v3, 0x1e200000

    add-int/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3100
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public updateSaveButton()V
    .locals 0

    .prologue
    .line 2593
    return-void
.end method
