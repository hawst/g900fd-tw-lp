.class public Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;
.super Ljava/util/ArrayList;
.source "MultiGridDrawRectList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Landroid/graphics/RectF;",
        ">;"
    }
.end annotation


# static fields
.field public static MAX_ROUND_STEPS:I

.field public static MAX_SPACING_STEPS:I


# instance fields
.field private final MAX_ROUND_WIDTH:F

.field private final MAX_SPACING_WIDTH:F

.field private final MIN_ROUND_WIDTH:F

.field private final MIN_SPACING_WIDTH:F

.field private final ROUND_VALUE_PER_STEP:F

.field private final SPACING_VALUE_PER_STEP:F

.field private mCurrentStyleId:I

.field private mIsPile:Z

.field private mLeftPadding:I

.field private mRoundWidth:F

.field private mSpacingWidth:F

.field private mTopPadding:I

.field private mViewHeight:I

.field private mViewWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/16 v0, 0x1e

    .line 323
    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->MAX_ROUND_STEPS:I

    .line 324
    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->MAX_SPACING_STEPS:I

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const v4, 0x3e4ccccd    # 0.2f

    const v3, 0x3d23d70a    # 0.04f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 11
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 317
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mCurrentStyleId:I

    .line 319
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->MIN_SPACING_WIDTH:F

    .line 320
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->MAX_SPACING_WIDTH:F

    .line 321
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->MIN_ROUND_WIDTH:F

    .line 322
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->MAX_ROUND_WIDTH:F

    .line 326
    sget v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->MAX_SPACING_STEPS:I

    int-to-float v0, v0

    div-float v0, v3, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->SPACING_VALUE_PER_STEP:F

    .line 327
    sget v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->MAX_ROUND_STEPS:I

    int-to-float v0, v0

    div-float v0, v4, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->ROUND_VALUE_PER_STEP:F

    .line 329
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewWidth:I

    .line 330
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewHeight:I

    .line 332
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mTopPadding:I

    .line 333
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mLeftPadding:I

    .line 335
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mRoundWidth:F

    .line 336
    const v0, 0x3c23d70a    # 0.01f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mSpacingWidth:F

    .line 338
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mIsPile:Z

    .line 14
    return-void
.end method

.method private isIndexInList(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 97
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 99
    :cond_0
    const/4 v0, 0x0

    .line 101
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public changeIndex(II)Z
    .locals 6
    .param p1, "index1"    # I
    .param p2, "index2"    # I

    .prologue
    const/4 v4, 0x0

    .line 113
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->isIndexInList(I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 135
    :cond_0
    :goto_0
    return v4

    .line 118
    :cond_1
    invoke-direct {p0, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->isIndexInList(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 123
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 124
    .local v0, "indexH":I
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 126
    .local v1, "indexT":I
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v2

    .line 127
    .local v2, "rectH":Landroid/graphics/RectF;
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v3

    .line 129
    .local v3, "rectT":Landroid/graphics/RectF;
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->remove(I)Ljava/lang/Object;

    .line 130
    invoke-virtual {p0, v0, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->add(ILjava/lang/Object;)V

    .line 132
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->remove(I)Ljava/lang/Object;

    .line 133
    invoke-virtual {p0, v0, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->add(ILjava/lang/Object;)V

    .line 135
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public get(I)Landroid/graphics/RectF;
    .locals 11
    .param p1, "index"    # I

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 19
    const/4 v6, 0x0

    .line 20
    .local v6, "spacingWidth":I
    iget-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mIsPile:Z

    if-nez v7, :cond_0

    .line 22
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getSpacingWidth()I

    move-result v6

    .line 25
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->size()I

    move-result v7

    if-nez v7, :cond_2

    .line 85
    :cond_1
    :goto_0
    return-object v5

    .line 29
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->isIndexInList(I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 34
    const/4 v1, 0x0

    .line 35
    .local v1, "fLeft":F
    const/4 v2, 0x0

    .line 36
    .local v2, "fRight":F
    const/4 v3, 0x0

    .line 37
    .local v3, "fTop":F
    const/4 v0, 0x0

    .line 39
    .local v0, "fBottom":F
    invoke-super {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    .line 41
    .local v4, "rect":Landroid/graphics/RectF;
    if-eqz v4, :cond_1

    .line 45
    iget v7, v4, Landroid/graphics/RectF;->left:F

    cmpl-float v7, v7, v9

    if-nez v7, :cond_3

    .line 47
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewWidth:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/RectF;->left:F

    mul-float/2addr v7, v8

    mul-int/lit8 v8, v6, 0x2

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mLeftPadding:I

    int-to-float v8, v8

    add-float v1, v7, v8

    .line 54
    :goto_1
    iget v7, v4, Landroid/graphics/RectF;->top:F

    cmpl-float v7, v7, v9

    if-nez v7, :cond_4

    .line 56
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewHeight:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/RectF;->top:F

    mul-float/2addr v7, v8

    mul-int/lit8 v8, v6, 0x2

    int-to-float v8, v8

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mTopPadding:I

    int-to-float v8, v8

    add-float v3, v7, v8

    .line 63
    :goto_2
    iget v7, v4, Landroid/graphics/RectF;->right:F

    cmpl-float v7, v7, v10

    if-nez v7, :cond_5

    .line 65
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewWidth:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/RectF;->right:F

    mul-float/2addr v7, v8

    mul-int/lit8 v8, v6, 0x2

    int-to-float v8, v8

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mLeftPadding:I

    int-to-float v8, v8

    add-float v2, v7, v8

    .line 72
    :goto_3
    iget v7, v4, Landroid/graphics/RectF;->bottom:F

    cmpl-float v7, v7, v10

    if-nez v7, :cond_6

    .line 74
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewHeight:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v7, v8

    mul-int/lit8 v8, v6, 0x2

    int-to-float v8, v8

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mTopPadding:I

    int-to-float v8, v8

    add-float v0, v7, v8

    .line 83
    :goto_4
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v1, v3, v2, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 85
    .local v5, "retRect":Landroid/graphics/RectF;
    goto :goto_0

    .line 51
    .end local v5    # "retRect":Landroid/graphics/RectF;
    :cond_3
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewWidth:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/RectF;->left:F

    mul-float/2addr v7, v8

    int-to-float v8, v6

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mLeftPadding:I

    int-to-float v8, v8

    add-float v1, v7, v8

    goto :goto_1

    .line 60
    :cond_4
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewHeight:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/RectF;->top:F

    mul-float/2addr v7, v8

    int-to-float v8, v6

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mTopPadding:I

    int-to-float v8, v8

    add-float v3, v7, v8

    goto :goto_2

    .line 69
    :cond_5
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewWidth:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/RectF;->right:F

    mul-float/2addr v7, v8

    int-to-float v8, v6

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mLeftPadding:I

    int-to-float v8, v8

    add-float v2, v7, v8

    goto :goto_3

    .line 78
    :cond_6
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewHeight:I

    int-to-float v7, v7

    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v7, v8

    int-to-float v8, v6

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mTopPadding:I

    int-to-float v8, v8

    add-float v0, v7, v8

    goto :goto_4
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getBackgroundRoi()Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 247
    new-instance v0, Landroid/graphics/Rect;

    .line 248
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mLeftPadding:I

    add-int/lit8 v1, v1, 0x0

    .line 249
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mTopPadding:I

    add-int/lit8 v2, v2, 0x2

    .line 250
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mLeftPadding:I

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewWidth:I

    add-int/2addr v3, v4

    .line 251
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mTopPadding:I

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewHeight:I

    add-int/2addr v4, v5

    .line 247
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 253
    .local v0, "retRect":Landroid/graphics/Rect;
    return-object v0
.end method

.method public getCanvasRoi()Landroid/graphics/RectF;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 286
    new-instance v0, Landroid/graphics/RectF;

    .line 289
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewWidth:I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mLeftPadding:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    .line 290
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewHeight:I

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mTopPadding:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    .line 286
    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 293
    .local v0, "retRect":Landroid/graphics/RectF;
    return-object v0
.end method

.method public getCurrentRoundStep()I
    .locals 3

    .prologue
    .line 204
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mRoundWidth:F

    const/4 v2, 0x0

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->ROUND_VALUE_PER_STEP:F

    div-float/2addr v1, v2

    float-to-int v0, v1

    .line 205
    .local v0, "retVal":I
    return v0
.end method

.method public getCurrentSpacingStep()I
    .locals 3

    .prologue
    .line 194
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mSpacingWidth:F

    const/4 v2, 0x0

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->SPACING_VALUE_PER_STEP:F

    div-float/2addr v1, v2

    float-to-int v0, v1

    .line 195
    .local v0, "retVal":I
    return v0
.end method

.method public getCurrentStyleId()I
    .locals 1

    .prologue
    .line 298
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mCurrentStyleId:I

    return v0
.end method

.method public getLeftPadding()I
    .locals 1

    .prologue
    .line 233
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mLeftPadding:I

    return v0
.end method

.method public getRoundWidth()I
    .locals 3

    .prologue
    .line 165
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mRoundWidth:F

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewWidth:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 166
    .local v0, "roundWidth":I
    return v0
.end method

.method public getSpacingWidth()I
    .locals 3

    .prologue
    .line 145
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mSpacingWidth:F

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewWidth:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 146
    .local v0, "spacingWidth":I
    return v0
.end method

.method public getTopPadding()I
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mTopPadding:I

    return v0
.end method

.method public getTouchedIndex(FF)I
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 270
    const/4 v1, -0x1

    .line 272
    .local v1, "index":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_0

    .line 281
    :goto_1
    return v1

    .line 274
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 276
    move v1, v0

    .line 277
    goto :goto_1

    .line 272
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public getTouchedIndex(Landroid/view/MotionEvent;)I
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 259
    if-nez p1, :cond_0

    .line 260
    const/4 v0, -0x1

    .line 265
    :goto_0
    return v0

    .line 262
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getTouchedIndex(FF)I

    move-result v0

    .line 265
    .local v0, "index":I
    goto :goto_0
.end method

.method public isPileMode()Z
    .locals 1

    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mIsPile:Z

    return v0
.end method

.method public setCurrentStyleId(I)V
    .locals 0
    .param p1, "currentStyleId"    # I

    .prologue
    .line 302
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mCurrentStyleId:I

    .line 303
    return-void
.end method

.method public setLeftPadding(I)V
    .locals 0
    .param p1, "mLeftPadding"    # I

    .prologue
    .line 241
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mLeftPadding:I

    .line 242
    return-void
.end method

.method public setPileMode(Z)Z
    .locals 1
    .param p1, "isPile"    # Z

    .prologue
    .line 307
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mIsPile:Z

    .line 308
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mIsPile:Z

    return v0
.end method

.method public setRoundStep(I)V
    .locals 2
    .param p1, "step"    # I

    .prologue
    .line 209
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->ROUND_VALUE_PER_STEP:F

    int-to-float v1, p1

    mul-float/2addr v0, v1

    const/4 v1, 0x0

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mRoundWidth:F

    .line 210
    return-void
.end method

.method public setRoundWidth(I)V
    .locals 2
    .param p1, "roundWidthDiff"    # I

    .prologue
    .line 175
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mRoundWidth:F

    int-to-float v1, p1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mRoundWidth:F

    .line 176
    const/4 v0, 0x0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mRoundWidth:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mRoundWidth:F

    .line 177
    const v0, 0x3e4ccccd    # 0.2f

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mRoundWidth:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mRoundWidth:F

    .line 178
    return-void
.end method

.method public setSpacingStep(I)V
    .locals 2
    .param p1, "step"    # I

    .prologue
    .line 199
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->SPACING_VALUE_PER_STEP:F

    int-to-float v1, p1

    mul-float/2addr v0, v1

    const/4 v1, 0x0

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mSpacingWidth:F

    .line 200
    return-void
.end method

.method public setSpacingWidth(I)V
    .locals 2
    .param p1, "spacingWidthDiff"    # I

    .prologue
    .line 155
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mSpacingWidth:F

    int-to-float v1, p1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mSpacingWidth:F

    .line 156
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mSpacingWidth:F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mSpacingWidth:F

    .line 157
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mSpacingWidth:F

    const v1, 0x3d23d70a    # 0.04f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mSpacingWidth:F

    .line 158
    return-void
.end method

.method public setTopPadding(I)V
    .locals 0
    .param p1, "mTopPadding"    # I

    .prologue
    .line 225
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mTopPadding:I

    .line 226
    return-void
.end method

.method public setViewWidthHeight()V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method public setViewWidthHeight(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 182
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewWidth:I

    .line 183
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->mViewHeight:I

    .line 184
    return-void
.end method
