.class public Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;
.super Lcom/sec/android/mimage/photoretouching/Core/EffectInfoInterface;
.source "EffectDefaultMode.java"


# instance fields
.field private final GREAYSCALE_APPLY_STEP:I

.field private mContext:Landroid/content/Context;

.field private mEffectType:I

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mMaskAlpha:I

.field private mStartInitializing:Z

.field private mStep:I

.field private mTexturePreview:[I

.field private mTexturePreview2:[I

.field private mutexOn:Z

.field private myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

.field private myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onCallback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    .param p3, "onActionbarCallback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;
    .param p4, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfoInterface;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 24
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->GREAYSCALE_APPLY_STEP:I

    .line 31
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mMaskAlpha:I

    .line 428
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview:[I

    .line 429
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview2:[I

    .line 431
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mEffectType:I

    .line 432
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mStep:I

    .line 434
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mutexOn:Z

    .line 435
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mStartInitializing:Z

    .line 437
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    .line 438
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .line 439
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .line 441
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 37
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .line 38
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .line 39
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 40
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    .line 42
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mStep:I

    .line 44
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview:[I

    .line 45
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    return-object v0
.end method

.method private makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V
    .locals 8
    .param p1, "src"    # Landroid/graphics/Bitmap;
    .param p2, "outBuffer"    # [I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 54
    const/4 v1, 0x1

    invoke-static {p1, p3, p4, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, "resized":Landroid/graphics/Bitmap;
    move-object v1, p2

    move v3, p3

    move v4, v2

    move v5, v2

    move v6, p3

    move v7, p4

    .line 55
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 56
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 57
    const/4 v0, 0x0

    .line 58
    return-void
.end method


# virtual methods
.method public applyOriginal()[I
    .locals 25

    .prologue
    .line 170
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v3

    .line 171
    .local v3, "mask":[B
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v2

    .line 172
    .local v2, "input":[I
    const/4 v4, 0x0

    .line 173
    .local v4, "output":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v8, :cond_0

    .line 175
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    .line 176
    .local v5, "w":I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v6

    .line 177
    .local v6, "h":I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v7

    .line 179
    .local v7, "r":Landroid/graphics/Rect;
    mul-int v8, v5, v6

    new-array v4, v8, [I

    .line 180
    const/4 v8, 0x0

    const/4 v9, 0x0

    array-length v14, v4

    invoke-static {v2, v8, v4, v9, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 181
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mEffectType:I

    packed-switch v8, :pswitch_data_0

    .line 301
    :goto_0
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v9

    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 302
    .local v12, "ret":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    .line 303
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v19

    .line 302
    invoke-virtual/range {v12 .. v19}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 304
    new-instance v13, Landroid/graphics/Canvas;

    invoke-direct {v13, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 305
    .local v13, "canvas":Landroid/graphics/Canvas;
    new-instance v22, Landroid/graphics/Paint;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Paint;-><init>()V

    .line 306
    .local v22, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mMaskAlpha:I

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 307
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 308
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v20

    const/16 v21, 0x1

    move-object v14, v4

    .line 307
    invoke-virtual/range {v13 .. v22}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 310
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    .line 311
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v21

    move-object v14, v12

    .line 310
    invoke-virtual/range {v14 .. v21}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 313
    const-string v8, "applyOriginal end"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 315
    .end local v5    # "w":I
    .end local v6    # "h":I
    .end local v7    # "r":Landroid/graphics/Rect;
    .end local v12    # "ret":Landroid/graphics/Bitmap;
    .end local v13    # "canvas":Landroid/graphics/Canvas;
    .end local v22    # "paint":Landroid/graphics/Paint;
    :cond_0
    return-object v2

    .line 184
    .restart local v5    # "w":I
    .restart local v6    # "h":I
    .restart local v7    # "r":Landroid/graphics/Rect;
    :pswitch_1
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyVignettePartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 187
    :pswitch_2
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyTurquoisePartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 190
    :pswitch_3
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyVintagePartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 193
    :pswitch_4
    const/16 v13, 0xa

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move v12, v6

    move-object v14, v7

    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlGreyscale([I[B[IIIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 196
    :pswitch_5
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySepiaPartial([I[B[IIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 199
    :pswitch_6
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyDownlightPartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 202
    :pswitch_7
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyBluewashPartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 205
    :pswitch_8
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyNostalgiaPartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 208
    :pswitch_9
    const/16 v13, 0x32

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move v12, v6

    move-object v14, v7

    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlFadedColour([I[B[IIIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 211
    :pswitch_a
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mStep:I

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move v12, v6

    move-object v14, v7

    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySharpenPartial([I[B[IIIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 214
    :pswitch_b
    const/16 v13, 0x32

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move v12, v6

    move-object v14, v7

    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySoftglowPartial([I[B[IIIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 217
    :pswitch_c
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyRainbowPartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 220
    :pswitch_d
    mul-int v8, v5, v6

    new-array v10, v8, [I

    .line 221
    .local v10, "texture":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0201c8

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 222
    .local v23, "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    move-object v8, v2

    move-object v9, v4

    move-object v11, v3

    move v12, v5

    move v13, v6

    move-object v14, v7

    .line 223
    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyStardustPartial([I[I[I[BIILandroid/graphics/Rect;)V

    .line 224
    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 225
    const/4 v10, 0x0

    .line 226
    goto/16 :goto_0

    .line 228
    .end local v10    # "texture":[I
    .end local v23    # "bitmap":Landroid/graphics/Bitmap;
    :pswitch_e
    mul-int v8, v5, v6

    new-array v10, v8, [I

    .line 229
    .restart local v10    # "texture":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0201c7

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 230
    .restart local v23    # "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    move-object v8, v2

    move-object v9, v4

    move-object v11, v3

    move v12, v5

    move v13, v6

    move-object v14, v7

    .line 231
    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyLightFlarePartial([I[I[I[BIILandroid/graphics/Rect;)V

    .line 232
    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 233
    const/4 v10, 0x0

    .line 234
    goto/16 :goto_0

    .line 236
    .end local v10    # "texture":[I
    .end local v23    # "bitmap":Landroid/graphics/Bitmap;
    :pswitch_f
    mul-int v8, v5, v6

    new-array v10, v8, [I

    .line 237
    .restart local v10    # "texture":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0201c9

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 238
    .restart local v23    # "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    move-object v8, v2

    move-object v9, v4

    move-object v11, v3

    move v12, v5

    move v13, v6

    move-object v14, v7

    .line 239
    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyLightStreakPartial([I[I[I[BIILandroid/graphics/Rect;)V

    .line 240
    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 241
    const/4 v10, 0x0

    .line 242
    goto/16 :goto_0

    .line 244
    .end local v10    # "texture":[I
    .end local v23    # "bitmap":Landroid/graphics/Bitmap;
    :pswitch_10
    mul-int v8, v5, v6

    new-array v10, v8, [I

    .line 245
    .restart local v10    # "texture":[I
    mul-int v8, v5, v6

    new-array v11, v8, [I

    .line 246
    .local v11, "texture2":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0200d5

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 247
    .restart local v23    # "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 248
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020555

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v24

    .line 249
    .local v24, "bitmap2":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 250
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v11, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    move-object v8, v2

    move-object v9, v4

    move-object v12, v3

    move v13, v5

    move v14, v6

    move-object v15, v7

    .line 251
    invoke-static/range {v8 .. v15}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyImpressionistPartial([I[I[I[I[BIILandroid/graphics/Rect;)V

    .line 252
    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 253
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 254
    const/4 v10, 0x0

    .line 255
    const/4 v11, 0x0

    .line 256
    goto/16 :goto_0

    .line 258
    .end local v10    # "texture":[I
    .end local v11    # "texture2":[I
    .end local v23    # "bitmap":Landroid/graphics/Bitmap;
    .end local v24    # "bitmap2":Landroid/graphics/Bitmap;
    :pswitch_11
    mul-int v8, v5, v6

    new-array v10, v8, [I

    .line 259
    .restart local v10    # "texture":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f02052b

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 260
    .restart local v23    # "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    move-object v12, v2

    move-object v13, v4

    move-object v14, v10

    move-object v15, v3

    move/from16 v16, v5

    move/from16 v17, v6

    move-object/from16 v18, v7

    .line 261
    invoke-static/range {v12 .. v18}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySketchTexturePartial([I[I[I[BIILandroid/graphics/Rect;)I

    .line 262
    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 263
    const/4 v10, 0x0

    .line 264
    goto/16 :goto_0

    .line 266
    .end local v10    # "texture":[I
    .end local v23    # "bitmap":Landroid/graphics/Bitmap;
    :pswitch_12
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyCartoonizePartial([I[B[IIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 269
    :pswitch_13
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyInvert([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 272
    :pswitch_14
    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyPopArt([I[III)V

    goto/16 :goto_0

    .line 275
    :pswitch_15
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyYellowglowPartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 278
    :pswitch_16
    mul-int v8, v5, v6

    new-array v10, v8, [I

    .line 279
    .restart local v10    # "texture":[I
    mul-int v8, v5, v6

    new-array v11, v8, [I

    .line 280
    .restart local v11    # "texture2":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0201ca

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 281
    .restart local v23    # "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 282
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0201cb

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v24

    .line 283
    .restart local v24    # "bitmap2":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 284
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v11, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    move-object v8, v2

    move-object v9, v4

    move-object v12, v3

    move v13, v5

    move v14, v6

    move-object v15, v7

    .line 285
    invoke-static/range {v8 .. v15}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyDawnCastPartial([I[I[I[I[BIILandroid/graphics/Rect;)V

    .line 286
    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 287
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 288
    const/4 v10, 0x0

    .line 289
    const/4 v11, 0x0

    .line 290
    goto/16 :goto_0

    .line 292
    .end local v10    # "texture":[I
    .end local v11    # "texture2":[I
    .end local v23    # "bitmap":Landroid/graphics/Bitmap;
    .end local v24    # "bitmap2":Landroid/graphics/Bitmap;
    :pswitch_17
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mStep:I

    move/from16 v17, v0

    move-object v12, v2

    move-object v13, v3

    move-object v14, v4

    move v15, v5

    move/from16 v16, v6

    move-object/from16 v18, v7

    invoke-static/range {v12 .. v18}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyPosterizePartial([I[B[IIIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    :pswitch_18
    move-object v12, v2

    move-object v13, v4

    move-object v14, v3

    move v15, v5

    move/from16 v16, v6

    move-object/from16 v17, v7

    .line 295
    invoke-static/range {v12 .. v17}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyGothicNoirPartial([I[I[BIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 298
    :pswitch_19
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyMagicPenPartial([I[B[IIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 181
    nop

    :pswitch_data_0
    .packed-switch 0x16001600
        :pswitch_1
        :pswitch_3
        :pswitch_8
        :pswitch_9
        :pswitch_4
        :pswitch_5
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_2
        :pswitch_10
        :pswitch_12
        :pswitch_13
        :pswitch_11
        :pswitch_14
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method public applyPreview()I
    .locals 15

    .prologue
    const/16 v11, 0x32

    const/4 v7, 0x0

    .line 325
    const/4 v14, -0x1

    .line 326
    .local v14, "ret":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v6, :cond_2

    .line 329
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskBuffer()[B

    move-result-object v1

    .line 330
    .local v1, "previewMask":[B
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 331
    .local v0, "previewInput":[I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v2

    .line 333
    .local v2, "previewOutput":[I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 334
    .local v3, "previewWidth":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    .line 336
    .local v4, "previewHeight":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v5

    .line 337
    .local v5, "r":Landroid/graphics/Rect;
    invoke-virtual {v5}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 338
    invoke-virtual {v5, v3, v4, v7, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 339
    :cond_0
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    .line 341
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mEffectType:I

    packed-switch v6, :pswitch_data_0

    .line 419
    :goto_0
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v6, :cond_1

    .line 420
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    const/4 v7, 0x1

    invoke-interface {v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    .line 421
    :cond_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->afterApplyPreview()V

    .line 422
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "applyPreview preview roi:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 425
    .end local v0    # "previewInput":[I
    .end local v1    # "previewMask":[B
    .end local v2    # "previewOutput":[I
    .end local v3    # "previewWidth":I
    .end local v4    # "previewHeight":I
    .end local v5    # "r":Landroid/graphics/Rect;
    :cond_2
    return v14

    .line 343
    .restart local v0    # "previewInput":[I
    .restart local v1    # "previewMask":[B
    .restart local v2    # "previewOutput":[I
    .restart local v3    # "previewWidth":I
    .restart local v4    # "previewHeight":I
    .restart local v5    # "r":Landroid/graphics/Rect;
    :pswitch_1
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyVignettePartial([I[B[IIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 346
    :pswitch_2
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyTurquoisePartial([I[B[IIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 349
    :pswitch_3
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyVintagePartial([I[B[IIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 352
    :pswitch_4
    const/16 v11, 0xa

    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move v9, v3

    move v10, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlGreyscale([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 355
    :pswitch_5
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySepiaPartial([I[B[IIILandroid/graphics/Rect;)I

    goto :goto_0

    .line 358
    :pswitch_6
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyDownlightPartial([I[B[IIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 361
    :pswitch_7
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyBluewashPartial([I[B[IIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 364
    :pswitch_8
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyNostalgiaPartial([I[B[IIILandroid/graphics/Rect;)V

    goto :goto_0

    :pswitch_9
    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move v9, v3

    move v10, v4

    move-object v12, v5

    .line 367
    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlFadedColour([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 370
    :pswitch_a
    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mStep:I

    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move v9, v3

    move v10, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySharpenPartial([I[B[IIIILandroid/graphics/Rect;)I

    goto :goto_0

    :pswitch_b
    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move v9, v3

    move v10, v4

    move-object v12, v5

    .line 373
    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySoftglowPartial([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 376
    :pswitch_c
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyRainbowPartial([I[B[IIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 379
    :pswitch_d
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview:[I

    move-object v6, v0

    move-object v7, v2

    move-object v9, v1

    move v10, v3

    move v11, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyStardustPartial([I[I[I[BIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 382
    :pswitch_e
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview:[I

    move-object v6, v0

    move-object v7, v2

    move-object v9, v1

    move v10, v3

    move v11, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyLightFlarePartial([I[I[I[BIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 385
    :pswitch_f
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview:[I

    move-object v6, v0

    move-object v7, v2

    move-object v9, v1

    move v10, v3

    move v11, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyLightStreakPartial([I[I[I[BIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 388
    :pswitch_10
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview:[I

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview2:[I

    move-object v6, v0

    move-object v7, v2

    move-object v10, v1

    move v11, v3

    move v12, v4

    move-object v13, v5

    invoke-static/range {v6 .. v13}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyImpressionistPartial([I[I[I[I[BIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 391
    :pswitch_11
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyCartoonizePartial([I[B[IIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 394
    :pswitch_12
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "negative r:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 395
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyInvert([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 398
    :pswitch_13
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview:[I

    move-object v6, v0

    move-object v7, v2

    move-object v9, v1

    move v10, v3

    move v11, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySketchTexturePartial([I[I[I[BIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 401
    :pswitch_14
    invoke-static {v0, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyPopArt([I[III)V

    goto/16 :goto_0

    .line 404
    :pswitch_15
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyYellowglowPartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 407
    :pswitch_16
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview:[I

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview2:[I

    move-object v6, v0

    move-object v7, v2

    move-object v10, v1

    move v11, v3

    move v12, v4

    move-object v13, v5

    invoke-static/range {v6 .. v13}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyDawnCastPartial([I[I[I[I[BIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 410
    :pswitch_17
    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mStep:I

    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move v9, v3

    move v10, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyPosterizePartial([I[B[IIIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    :pswitch_18
    move-object v6, v0

    move-object v7, v2

    move-object v8, v1

    move v9, v3

    move v10, v4

    move-object v11, v5

    .line 413
    invoke-static/range {v6 .. v11}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyGothicNoirPartial([I[I[BIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 416
    :pswitch_19
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyMagicPenPartial([I[B[IIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 341
    :pswitch_data_0
    .packed-switch 0x16001600
        :pswitch_1
        :pswitch_3
        :pswitch_8
        :pswitch_9
        :pswitch_4
        :pswitch_5
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_2
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method public configurationChange()V
    .locals 0

    .prologue
    .line 146
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 151
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 152
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview:[I

    .line 153
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 132
    return-void
.end method

.method public getMutexOn()Z
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mutexOn:Z

    return v0
.end method

.method public init(I)V
    .locals 6
    .param p1, "effectType"    # I

    .prologue
    const/4 v5, 0x0

    .line 63
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mStartInitializing:Z

    .line 64
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview:[I

    invoke-static {v2, v5}, Ljava/util/Arrays;->fill([II)V

    .line 65
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview2:[I

    .line 66
    const/4 v0, 0x0

    .line 67
    .local v0, "texture":Landroid/graphics/Bitmap;
    sparse-switch p1, :sswitch_data_0

    .line 98
    :goto_0
    if-eqz v0, :cond_0

    .line 99
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview:[I

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 100
    :cond_0
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 101
    const/4 v0, 0x0

    .line 102
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 104
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v2, :cond_1

    .line 106
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v2, v5}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    .line 108
    :cond_1
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mEffectType:I

    .line 109
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v2, :cond_2

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->applyPreview()I

    .line 113
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode$1;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 121
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mStartInitializing:Z

    .line 122
    return-void

    .line 70
    :sswitch_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201c8

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 71
    goto :goto_0

    .line 73
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02052b

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 74
    goto :goto_0

    .line 76
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    mul-int/2addr v2, v3

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview2:[I

    .line 77
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200d5

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 78
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020555

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 79
    .local v1, "texture2":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview2:[I

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 80
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 81
    const/4 v1, 0x0

    .line 82
    goto/16 :goto_0

    .line 84
    .end local v1    # "texture2":Landroid/graphics/Bitmap;
    :sswitch_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201c7

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 85
    goto/16 :goto_0

    .line 87
    :sswitch_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201c9

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 88
    goto/16 :goto_0

    .line 90
    :sswitch_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    mul-int/2addr v2, v3

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview2:[I

    .line 91
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201ca

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 92
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201cb

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 93
    .restart local v1    # "texture2":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mTexturePreview2:[I

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 94
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 67
    :sswitch_data_0
    .sparse-switch
        0x16001607 -> :sswitch_0
        0x16001608 -> :sswitch_3
        0x16001609 -> :sswitch_4
        0x1600160f -> :sswitch_2
        0x16001612 -> :sswitch_1
        0x16001616 -> :sswitch_5
    .end sparse-switch
.end method

.method public isStartInitializing()Z
    .locals 1

    .prologue
    .line 320
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mStartInitializing:Z

    return v0
.end method

.method public setMaskAlpha(I)V
    .locals 0
    .param p1, "alpha"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mMaskAlpha:I

    .line 50
    return-void
.end method

.method public setStep(I)V
    .locals 0
    .param p1, "step"    # I

    .prologue
    .line 156
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mStep:I

    .line 157
    return-void
.end method

.method public touch(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectDefaultMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v0, :cond_0

    .line 162
    const/4 v0, 0x0

    .line 164
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
