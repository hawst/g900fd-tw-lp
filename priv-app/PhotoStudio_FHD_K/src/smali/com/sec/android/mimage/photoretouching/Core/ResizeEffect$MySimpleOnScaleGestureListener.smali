.class Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "ResizeEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MySimpleOnScaleGestureListener"
.end annotation


# instance fields
.field private mScale:F

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;)V
    .locals 1

    .prologue
    .line 540
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    .line 541
    const/high16 v0, 0x3f400000    # 0.75f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->mScale:F

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;)V
    .locals 0

    .prologue
    .line 540
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const v2, 0x3dcccccd    # 0.1f

    .line 545
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->mScale:F

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->mScale:F

    .line 546
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->mScale:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    .line 547
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->mScale:F

    .line 548
    :cond_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->mScale:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    .line 549
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->mScale:F

    .line 550
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;)Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->mScale:F

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->setResizeScale(F)V

    .line 551
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 5
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v4, 0x1

    .line 556
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;Z)V

    .line 557
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->access$2(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->access$2(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    mul-int v1, v2, v3

    .line 558
    .local v1, "size":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;)Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->mResizeRect:Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;)Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ResizeRect;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 559
    .local v0, "roiSize":I
    int-to-float v2, v0

    int-to-float v3, v1

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->mScale:F

    .line 560
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "gesture scale:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->mScale:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 563
    return v4
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 568
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect$MySimpleOnScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;Z)V

    .line 569
    return-void
.end method
