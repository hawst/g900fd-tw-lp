.class Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$1;
.super Ljava/lang/Object;
.source "ImageStickerView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public actionBarAbleSave()V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableSave()V

    .line 224
    :cond_0
    return-void
.end method

.method public actionBarUnableSave()V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableSave()V

    .line 229
    :cond_0
    return-void
.end method

.method public setVisibleMenu(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->setVisibleTopBottomMenu(Z)V
    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;Z)V

    .line 219
    return-void
.end method
