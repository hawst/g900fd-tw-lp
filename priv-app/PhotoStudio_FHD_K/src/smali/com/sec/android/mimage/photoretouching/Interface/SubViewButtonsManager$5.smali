.class Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$5;
.super Ljava/lang/Object;
.source "SubViewButtonsManager.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->initHorizontalScrollView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    .line 282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutCallback()V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewWidth:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)I

    move-result v0

    if-lez v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->changeLayoutSize(I)V

    .line 292
    :cond_0
    return-void
.end method

.method public onScrollCallback(I)V
    .locals 3
    .param p1, "scrollX"    # I

    .prologue
    .line 296
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getCurrentModeButtonId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonLeft(I)I

    move-result v0

    .line 297
    .local v0, "left":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->adjustSubmenuBarPosition(II)V

    .line 298
    return-void
.end method
