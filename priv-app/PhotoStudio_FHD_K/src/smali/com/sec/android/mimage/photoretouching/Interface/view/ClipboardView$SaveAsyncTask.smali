.class Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;
.super Landroid/os/AsyncTask;
.source "ClipboardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V
    .locals 0

    .prologue
    .line 1255
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    .line 1254
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1256
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 11
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 1259
    const/4 v2, 0x0

    .line 1260
    .local v2, "fileName":Ljava/lang/String;
    array-length v6, p1

    if-lez v6, :cond_0

    .line 1261
    const/4 v6, 0x0

    aget-object v2, p1, v6

    .line 1263
    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getAvailableExternalMemorySize()J

    move-result-wide v0

    .line 1264
    .local v0, "available_memsize":J
    const-wide/32 v6, 0xa00000

    cmp-long v6, v0, v6

    if-gez v6, :cond_3

    .line 1265
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/app/ProgressDialog;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1266
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/app/ProgressDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1267
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "memory size = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " %"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1269
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f06007b

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 1299
    :cond_2
    :goto_0
    return-object v10

    .line 1272
    :cond_3
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)I

    move-result v6

    if-lt v4, v6, :cond_5

    .line 1277
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v7

    .line 1278
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v8

    .line 1279
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v9

    .line 1277
    invoke-virtual {v6, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([III)V

    .line 1282
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 1284
    const/4 v3, 0x0

    .line 1285
    .local v3, "filePath":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_6

    .line 1287
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 1288
    .local v5, "tempPath":Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1294
    .end local v5    # "tempPath":Ljava/lang/String;
    :goto_2
    if-nez v2, :cond_4

    .line 1295
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    move-result-object v2

    .line 1297
    :cond_4
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentSaveSize:I
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)I

    move-result v7

    invoke-virtual {v6, v3, v2, v7}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->saveCurrentImage(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 1272
    .end local v3    # "filePath":Ljava/lang/String;
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1292
    .restart local v3    # "filePath":Ljava/lang/String;
    :cond_6
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const v4, 0x7f0601df

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1310
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1311
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1312
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1315
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1317
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isPersonalPage()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1319
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Studio"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1320
    .local v1, "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 1335
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 1336
    return-void

    .line 1324
    .end local v1    # "text":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPrivateSaveFolder()Ljava/lang/String;

    move-result-object v0

    .line 1325
    .local v0, "privateFolder":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1326
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 1331
    .end local v0    # "privateFolder":Ljava/lang/String;
    .end local v1    # "text":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Studio"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1332
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 1302
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1303
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;Landroid/app/ProgressDialog;)V

    .line 1304
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06008f

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1305
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1306
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1307
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1308
    return-void
.end method
