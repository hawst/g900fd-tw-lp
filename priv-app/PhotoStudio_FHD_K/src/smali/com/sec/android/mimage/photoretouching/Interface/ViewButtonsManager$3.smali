.class Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;
.super Ljava/lang/Object;
.source "ViewButtonsManager.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->animAndFreeSubViewButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v2, 0x0

    .line 226
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Z)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/high16 v1, 0x437f0000    # 255.0f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsMiddleButtonVisible:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x1e110000

    if-eq v0, v1, :cond_0

    .line 229
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x2c000000

    .line 228
    if-ne v0, v1, :cond_1

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Z)V

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mIsMiddleButtonVisible:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x10000000

    if-eq v0, v1, :cond_2

    .line 236
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x20000000

    if-ne v0, v1, :cond_3

    .line 238
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Z)V

    .line 246
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isSubViewMode()Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 248
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->hideSubBottomButton()Z

    .line 249
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 251
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mSubViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->destroy()V

    .line 252
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    .line 255
    :cond_4
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 222
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 216
    return-void
.end method
