.class public Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;
.super Landroid/widget/BaseAdapter;
.source "GridAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;,
        Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageViewData;
    }
.end annotation


# static fields
.field private static MAX_RECENTLY:I


# instance fields
.field private FCL:Landroid/view/View$OnFocusChangeListener;

.field private MAX_WATERMARK_TYPE1:I

.field private mContext:Landroid/content/Context;

.field private mCurrentFocusIndex:I

.field private mCurrentLinearLayout:Landroid/widget/LinearLayout;

.field private mCurrentRes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

.field private mFrameId:[I

.field private mGridView:Landroid/widget/GridView;

.field private mLabelId:[I

.field private mLabelRes:[I

.field private mNumThumbCount:I

.field private mParent:Landroid/view/ViewGroup;

.field private mPreviousIdx:I

.field private mRecentlyList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mStampId:[I

.field private mStickerId:[I

.field private mStickerRes:[I

.field private mType:I

.field private mWatermarkId:[I

.field private mWatermarkRes:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->MAX_RECENTLY:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/GridView;ILcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;Ljava/util/ArrayList;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "gridview"    # Landroid/widget/GridView;
    .param p3, "type"    # I
    .param p4, "layoutmanager"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/GridView;",
            "I",
            "Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p5, "frameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    const v9, 0xffff

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 252
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 37
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mContext:Landroid/content/Context;

    .line 38
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    .line 40
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mNumThumbCount:I

    .line 41
    const/4 v5, -0x1

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentFocusIndex:I

    .line 44
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    .line 45
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    .line 46
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    .line 48
    const/16 v5, 0xa

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->MAX_WATERMARK_TYPE1:I

    .line 49
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mParent:Landroid/view/ViewGroup;

    .line 50
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerId:[I

    .line 51
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mLabelId:[I

    .line 52
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mWatermarkId:[I

    .line 53
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mFrameId:[I

    .line 54
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStampId:[I

    .line 55
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mPreviousIdx:I

    .line 56
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    .line 58
    const/16 v5, 0x55

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    .line 160
    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerRes:[I

    .line 221
    const/16 v5, 0x18

    new-array v5, v5, [I

    fill-array-data v5, :array_1

    .line 245
    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mLabelRes:[I

    .line 247
    new-array v5, v7, [I

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mWatermarkRes:[I

    .line 560
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->FCL:Landroid/view/View$OnFocusChangeListener;

    .line 253
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mContext:Landroid/content/Context;

    .line 254
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 255
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    .line 256
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    .line 257
    new-instance v5, Ljava/util/ArrayList;

    sget v6, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->MAX_RECENTLY:I

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    .line 258
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    .line 259
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 278
    :goto_0
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int/2addr v5, v6

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerId:[I

    .line 279
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE2:I

    add-int/2addr v5, v6

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mLabelId:[I

    .line 280
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->MAX_WATERMARK_TYPE1:I

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mWatermarkId:[I

    .line 281
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE3:I

    add-int/2addr v5, v6

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mFrameId:[I

    .line 282
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE1:I

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE2:I

    add-int/2addr v5, v6

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStampId:[I

    .line 284
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    .line 285
    const/4 v2, 0x1

    .line 286
    .local v2, "idIndex":I
    packed-switch p3, :pswitch_data_0

    .line 502
    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    .line 503
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mNumThumbCount:I

    .line 505
    :cond_1
    return-void

    .line 262
    .end local v2    # "idIndex":I
    :sswitch_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v8}, Landroid/widget/GridView;->setFocusable(Z)V

    goto :goto_0

    .line 265
    :sswitch_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v7}, Landroid/widget/GridView;->setFocusable(Z)V

    goto :goto_0

    .line 268
    :sswitch_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v8}, Landroid/widget/GridView;->setFocusable(Z)V

    goto :goto_0

    .line 271
    :sswitch_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v7}, Landroid/widget/GridView;->setFocusable(Z)V

    goto :goto_0

    .line 274
    :sswitch_4
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v7}, Landroid/widget/GridView;->setFocusable(Z)V

    goto :goto_0

    .line 289
    .restart local v2    # "idIndex":I
    :pswitch_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v7}, Landroid/widget/GridView;->setFocusable(Z)V

    .line 290
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    if-ge v1, v5, :cond_0

    .line 292
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerRes:[I

    aget v6, v6, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 293
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerId:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idIndex":I
    .local v3, "idIndex":I
    aput v2, v5, v1

    .line 290
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    .end local v3    # "idIndex":I
    .restart local v2    # "idIndex":I
    goto :goto_2

    .line 297
    .end local v1    # "i":I
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v7}, Landroid/widget/GridView;->setFocusable(Z)V

    .line 298
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v2, v5

    .line 299
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    if-ge v1, v5, :cond_0

    .line 301
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerRes:[I

    sget v7, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v7, v1

    aget v6, v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 302
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerId:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idIndex":I
    .restart local v3    # "idIndex":I
    aput v2, v5, v1

    .line 299
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    .end local v3    # "idIndex":I
    .restart local v2    # "idIndex":I
    goto :goto_3

    .line 306
    .end local v1    # "i":I
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v7}, Landroid/widget/GridView;->setFocusable(Z)V

    .line 307
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v5, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int v2, v5, v6

    .line 308
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    if-ge v1, v5, :cond_0

    .line 310
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerRes:[I

    sget v7, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    sget v8, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v7, v8

    add-int/2addr v7, v1

    aget v6, v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerId:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idIndex":I
    .restart local v3    # "idIndex":I
    aput v2, v5, v1

    .line 308
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    .end local v3    # "idIndex":I
    .restart local v2    # "idIndex":I
    goto :goto_4

    .line 315
    .end local v1    # "i":I
    :pswitch_3
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_5
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 317
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_6
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int/2addr v5, v6

    if-lt v1, v5, :cond_2

    .line 315
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 319
    :cond_2
    add-int v5, v1, v2

    and-int v0, v9, v5

    .line 320
    .local v0, "buttonId":I
    const/high16 v5, 0x31100000

    or-int/2addr v0, v5

    .line 321
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v5

    if-ne v5, v0, :cond_3

    .line 323
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerRes:[I

    aget v6, v6, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerId:[I

    aput v0, v5, v4

    .line 317
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 331
    .end local v0    # "buttonId":I
    .end local v1    # "i":I
    .end local v4    # "j":I
    :pswitch_4
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 333
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v8}, Landroid/widget/GridView;->setFocusable(Z)V

    .line 335
    :cond_4
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v5, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int v2, v5, v6

    .line 336
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_7
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    if-ge v1, v5, :cond_0

    .line 338
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mLabelRes:[I

    aget v6, v6, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 339
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mLabelId:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idIndex":I
    .restart local v3    # "idIndex":I
    aput v2, v5, v1

    .line 336
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    .end local v3    # "idIndex":I
    .restart local v2    # "idIndex":I
    goto :goto_7

    .line 343
    .end local v1    # "i":I
    :pswitch_5
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v8}, Landroid/widget/GridView;->setFocusable(Z)V

    .line 344
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v5, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int/2addr v5, v6

    .line 345
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    .line 344
    add-int v2, v5, v6

    .line 346
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_8
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE2:I

    if-lt v1, v5, :cond_5

    .line 351
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sj, GA - init() - mCurrentRes.size : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 348
    :cond_5
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mLabelRes:[I

    sget v7, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    add-int/2addr v7, v1

    aget v6, v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 349
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mLabelId:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idIndex":I
    .restart local v3    # "idIndex":I
    aput v2, v5, v1

    .line 346
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    .end local v3    # "idIndex":I
    .restart local v2    # "idIndex":I
    goto :goto_8

    .line 354
    .end local v1    # "i":I
    :pswitch_6
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v5, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int v2, v5, v6

    .line 355
    const/4 v4, 0x0

    .restart local v4    # "j":I
    :goto_9
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 357
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_a
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE2:I

    add-int/2addr v5, v6

    if-lt v1, v5, :cond_6

    .line 355
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 359
    :cond_6
    add-int v5, v1, v2

    and-int v0, v9, v5

    .line 360
    .restart local v0    # "buttonId":I
    const/high16 v5, 0x31300000

    or-int/2addr v0, v5

    .line 361
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v5

    if-ne v5, v0, :cond_7

    .line 363
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mLabelRes:[I

    aget v6, v6, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mLabelId:[I

    aput v0, v5, v4

    .line 357
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 370
    .end local v0    # "buttonId":I
    .end local v1    # "i":I
    .end local v4    # "j":I
    :pswitch_7
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v5, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int/2addr v5, v6

    .line 371
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    .line 370
    add-int/2addr v5, v6

    .line 371
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE2:I

    .line 370
    add-int v2, v5, v6

    .line 372
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_b
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    if-ge v1, v5, :cond_0

    .line 374
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getStampsOrFrames()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 375
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mFrameId:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idIndex":I
    .restart local v3    # "idIndex":I
    aput v2, v5, v1

    .line 372
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    .end local v3    # "idIndex":I
    .restart local v2    # "idIndex":I
    goto :goto_b

    .line 379
    .end local v1    # "i":I
    :pswitch_8
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v5, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int/2addr v5, v6

    .line 380
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    .line 379
    add-int/2addr v5, v6

    .line 380
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE2:I

    .line 379
    add-int/2addr v5, v6

    .line 381
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    .line 379
    add-int v2, v5, v6

    .line 383
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_c
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE2:I

    if-ge v1, v5, :cond_0

    .line 385
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getStampsOrFrames()Ljava/util/ArrayList;

    move-result-object v6

    sget v7, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    add-int/2addr v7, v1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mFrameId:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idIndex":I
    .restart local v3    # "idIndex":I
    aput v2, v5, v1

    .line 383
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    .end local v3    # "idIndex":I
    .restart local v2    # "idIndex":I
    goto :goto_c

    .line 390
    .end local v1    # "i":I
    :pswitch_9
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v5, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int/2addr v5, v6

    .line 391
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    .line 390
    add-int/2addr v5, v6

    .line 391
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE2:I

    .line 390
    add-int/2addr v5, v6

    .line 392
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    .line 390
    add-int/2addr v5, v6

    .line 392
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE2:I

    .line 390
    add-int v2, v5, v6

    .line 394
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_d
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE3:I

    if-ge v1, v5, :cond_0

    .line 396
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getStampsOrFrames()Ljava/util/ArrayList;

    move-result-object v6

    sget v7, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    sget v8, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE2:I

    add-int/2addr v7, v8

    add-int/2addr v7, v1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 397
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mFrameId:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idIndex":I
    .restart local v3    # "idIndex":I
    aput v2, v5, v1

    .line 394
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    .end local v3    # "idIndex":I
    .restart local v2    # "idIndex":I
    goto :goto_d

    .line 401
    .end local v1    # "i":I
    :pswitch_a
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v5, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int/2addr v5, v6

    .line 402
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    .line 401
    add-int/2addr v5, v6

    .line 402
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE2:I

    .line 401
    add-int v2, v5, v6

    .line 403
    const/4 v4, 0x0

    .restart local v4    # "j":I
    :goto_e
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 405
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_f
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE3:I

    add-int/2addr v5, v6

    if-lt v1, v5, :cond_8

    .line 403
    add-int/lit8 v4, v4, 0x1

    goto :goto_e

    .line 407
    :cond_8
    add-int v5, v1, v2

    and-int v0, v9, v5

    .line 408
    .restart local v0    # "buttonId":I
    const/high16 v5, 0x31200000

    or-int/2addr v0, v5

    .line 409
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v5

    if-ne v5, v0, :cond_9

    .line 411
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getStampsOrFrames()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 412
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mFrameId:[I

    aput v0, v5, v4

    .line 405
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_f

    .line 419
    .end local v0    # "buttonId":I
    .end local v1    # "i":I
    .end local v4    # "j":I
    :pswitch_b
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 421
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v8}, Landroid/widget/GridView;->setFocusable(Z)V

    .line 424
    :cond_a
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v5, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int/2addr v5, v6

    .line 425
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    .line 424
    add-int/2addr v5, v6

    .line 425
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE2:I

    .line 424
    add-int/2addr v5, v6

    .line 426
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    .line 424
    add-int/2addr v5, v6

    .line 426
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE2:I

    .line 424
    add-int/2addr v5, v6

    .line 426
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE3:I

    .line 424
    add-int v2, v5, v6

    .line 427
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_10
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE1:I

    if-ge v1, v5, :cond_0

    .line 430
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getStampsOrFrames()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 431
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStampId:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idIndex":I
    .restart local v3    # "idIndex":I
    aput v2, v5, v1

    .line 427
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    .end local v3    # "idIndex":I
    .restart local v2    # "idIndex":I
    goto :goto_10

    .line 436
    .end local v1    # "i":I
    :pswitch_c
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v8}, Landroid/widget/GridView;->setFocusable(Z)V

    .line 437
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v5, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int/2addr v5, v6

    .line 438
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    .line 437
    add-int/2addr v5, v6

    .line 438
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE2:I

    .line 437
    add-int/2addr v5, v6

    .line 439
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    .line 437
    add-int/2addr v5, v6

    .line 439
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE2:I

    .line 437
    add-int/2addr v5, v6

    .line 439
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE3:I

    .line 437
    add-int/2addr v5, v6

    .line 440
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE1:I

    .line 437
    add-int v2, v5, v6

    .line 441
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_11
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE2:I

    if-ge v1, v5, :cond_0

    .line 444
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getStampsOrFrames()Ljava/util/ArrayList;

    move-result-object v6

    sget v7, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE1:I

    add-int/2addr v7, v1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStampId:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idIndex":I
    .restart local v3    # "idIndex":I
    aput v2, v5, v1

    .line 441
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    .end local v3    # "idIndex":I
    .restart local v2    # "idIndex":I
    goto :goto_11

    .line 449
    .end local v1    # "i":I
    :pswitch_d
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v5, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int/2addr v5, v6

    .line 450
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    .line 449
    add-int/2addr v5, v6

    .line 450
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE2:I

    .line 449
    add-int/2addr v5, v6

    .line 451
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    .line 449
    add-int/2addr v5, v6

    .line 451
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE2:I

    .line 449
    add-int/2addr v5, v6

    .line 451
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE3:I

    .line 449
    add-int v2, v5, v6

    .line 452
    const/4 v4, 0x0

    .restart local v4    # "j":I
    :goto_12
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 454
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_13
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE1:I

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE2:I

    add-int/2addr v5, v6

    if-lt v1, v5, :cond_b

    .line 452
    add-int/lit8 v4, v4, 0x1

    goto :goto_12

    .line 456
    :cond_b
    add-int v5, v1, v2

    and-int v0, v9, v5

    .line 457
    .restart local v0    # "buttonId":I
    const/high16 v5, 0x31500000

    or-int/2addr v0, v5

    .line 458
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v5

    if-ne v5, v0, :cond_c

    .line 461
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getStampsOrFrames()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 462
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStampId:[I

    aput v0, v5, v4

    .line 454
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_13

    .line 471
    .end local v0    # "buttonId":I
    .end local v1    # "i":I
    .end local v4    # "j":I
    :pswitch_e
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v5, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int/2addr v5, v6

    .line 472
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    .line 471
    add-int/2addr v5, v6

    .line 472
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE2:I

    .line 471
    add-int/2addr v5, v6

    .line 473
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    .line 471
    add-int/2addr v5, v6

    .line 473
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE2:I

    .line 471
    add-int/2addr v5, v6

    .line 473
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE3:I

    .line 471
    add-int/2addr v5, v6

    .line 474
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE1:I

    .line 471
    add-int/2addr v5, v6

    .line 474
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE2:I

    .line 471
    add-int v2, v5, v6

    .line 475
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_14
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->MAX_WATERMARK_TYPE1:I

    if-ge v1, v5, :cond_0

    .line 479
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mWatermarkId:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idIndex":I
    .restart local v3    # "idIndex":I
    aput v2, v5, v1

    .line 475
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    .end local v3    # "idIndex":I
    .restart local v2    # "idIndex":I
    goto :goto_14

    .line 483
    .end local v1    # "i":I
    :pswitch_f
    sget v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    add-int/2addr v5, v2

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    add-int/2addr v5, v6

    .line 484
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    .line 483
    add-int/2addr v5, v6

    .line 484
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE2:I

    .line 483
    add-int/2addr v5, v6

    .line 485
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE1:I

    .line 483
    add-int/2addr v5, v6

    .line 485
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE2:I

    .line 483
    add-int/2addr v5, v6

    .line 485
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_FRAME_TYPE3:I

    .line 483
    add-int/2addr v5, v6

    .line 486
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE1:I

    .line 483
    add-int/2addr v5, v6

    .line 486
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STAMP_TYPE2:I

    .line 483
    add-int v2, v5, v6

    .line 487
    const/4 v4, 0x0

    .restart local v4    # "j":I
    :goto_15
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 489
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_16
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->MAX_WATERMARK_TYPE1:I

    if-lt v1, v5, :cond_d

    .line 487
    add-int/lit8 v4, v4, 0x1

    goto :goto_15

    .line 491
    :cond_d
    add-int v5, v1, v2

    and-int v0, v9, v5

    .line 492
    .restart local v0    # "buttonId":I
    const/high16 v5, 0x31400000

    or-int/2addr v0, v5

    .line 493
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v5

    if-ne v5, v0, :cond_e

    .line 495
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mWatermarkRes:[I

    aget v6, v6, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 496
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mWatermarkId:[I

    aput v0, v5, v4

    .line 489
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_16

    .line 58
    nop

    :array_0
    .array-data 4
        0x7f02036a
        0x7f02036b
        0x7f020369
        0x7f02036c
        0x7f02036d
        0x7f020368
        0x7f020540
        0x7f020541
        0x7f020542
        0x7f020543
        0x7f020544
        0x7f020534
        0x7f020535
        0x7f020536
        0x7f020537
        0x7f020538
        0x7f020539
        0x7f02053a
        0x7f02053b
        0x7f02053c
        0x7f02053d
        0x7f02053e
        0x7f02053f
        0x7f02052e
        0x7f02052f
        0x7f020530
        0x7f020531
        0x7f020532
        0x7f020533
        0x7f020545
        0x7f020546
        0x7f020547
        0x7f020548
        0x7f020549
        0x7f02054a
        0x7f0201cc
        0x7f0201cd
        0x7f0201ce
        0x7f0201cf
        0x7f0201d0
        0x7f0201d1
        0x7f020513
        0x7f020514
        0x7f020515
        0x7f020516
        0x7f020517
        0x7f020518
        0x7f020519
        0x7f02051a
        0x7f02051b
        0x7f02051c
        0x7f020563
        0x7f020564
        0x7f020565
        0x7f020566
        0x7f020567
        0x7f020568
        0x7f020569
        0x7f02056a
        0x7f020594
        0x7f020595
        0x7f020596
        0x7f020597
        0x7f020598
        0x7f020599
        0x7f020332
        0x7f020333
        0x7f020334
        0x7f020335
        0x7f020336
        0x7f020337
        0x7f020338
        0x7f020339
        0x7f02033a
        0x7f02033b
        0x7f020023
        0x7f020024
        0x7f020025
        0x7f020026
        0x7f020027
        0x7f020028
        0x7f020029
        0x7f02002a
        0x7f02002b
        0x7f02002c
    .end array-data

    .line 221
    :array_1
    .array-data 4
        0x7f02037c
        0x7f02037d
        0x7f02037e
        0x7f02037f
        0x7f020380
        0x7f020381
        0x7f020382
        0x7f020383
        0x7f020384
        0x7f020385
        0x7f020386
        0x7f020387
        0x7f020388
        0x7f020389
        0x7f02038a
        0x7f02038b
        0x7f02038c
        0x7f02038d
        0x7f02038e
        0x7f02038f
        0x7f020390
        0x7f020391
        0x7f020392
        0x7f020393
    .end array-data

    .line 259
    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_2
        0x31200000 -> :sswitch_0
        0x31300000 -> :sswitch_3
        0x31400000 -> :sswitch_4
        0x31500000 -> :sswitch_1
    .end sparse-switch

    .line 286
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_a
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_6
        :pswitch_4
        :pswitch_5
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)Landroid/widget/GridView;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getLabelIndexByViewId(I)I
    .locals 5
    .param p1, "viewId"    # I

    .prologue
    .line 871
    const/4 v1, -0x1

    .line 872
    .local v1, "index":I
    const v2, 0xffff

    and-int/2addr v2, p1

    .line 873
    sget v3, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    sget v4, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v3, v4

    .line 874
    sget v4, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE3:I

    .line 873
    add-int/2addr v3, v4

    .line 872
    sub-int v0, v2, v3

    .line 875
    .local v0, "id":I
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    packed-switch v2, :pswitch_data_0

    .line 887
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, GA - getLabelIndexByViewId() - viewId : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 888
    const-string v3, " / index : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / mType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 887
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 889
    return v1

    .line 878
    :pswitch_0
    add-int/lit8 v1, v0, -0x1

    .line 879
    goto :goto_0

    .line 881
    :pswitch_1
    add-int/lit8 v1, v0, -0x1

    .line 882
    goto :goto_0

    .line 884
    :pswitch_2
    sget v2, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    sub-int v2, v0, v2

    add-int/lit8 v1, v2, -0x1

    goto :goto_0

    .line 875
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getStickerIndexByViewId(I)I
    .locals 4
    .param p1, "viewId"    # I

    .prologue
    .line 847
    const/4 v1, -0x1

    .line 848
    .local v1, "index":I
    const v2, 0xffff

    and-int v0, p1, v2

    .line 850
    .local v0, "id":I
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    packed-switch v2, :pswitch_data_0

    .line 865
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, GA - getStickerIndexByViewId() - viewId : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 866
    const-string v3, " / index : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / mType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 865
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 867
    return v1

    .line 853
    :pswitch_0
    add-int/lit8 v1, v0, -0x1

    .line 854
    goto :goto_0

    .line 856
    :pswitch_1
    add-int/lit8 v1, v0, -0x1

    .line 857
    goto :goto_0

    .line 859
    :pswitch_2
    sget v2, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    sub-int v2, v0, v2

    add-int/lit8 v1, v2, -0x1

    .line 860
    goto :goto_0

    .line 862
    :pswitch_3
    sget v2, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE1:I

    sget v3, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_STICKER_TYPE2:I

    add-int/2addr v2, v3

    sub-int v2, v0, v2

    add-int/lit8 v1, v2, -0x1

    goto :goto_0

    .line 850
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public destroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 525
    const-string v1, "sticker adapter destroy"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 542
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 544
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 549
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 550
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    .line 552
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 554
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 555
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    .line 558
    :cond_2
    return-void

    .line 544
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 545
    .local v0, "obj":Ljava/lang/Object;
    instance-of v2, v0, Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    .line 546
    check-cast v0, Landroid/graphics/Bitmap;

    .end local v0    # "obj":Ljava/lang/Object;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 674
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentFocusIndex()I
    .locals 1

    .prologue
    .line 510
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentFocusIndex:I

    return v0
.end method

.method public getCurrentLinearLayout()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 909
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 914
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "pos"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 920
    const/4 v2, 0x0

    .line 922
    .local v2, "imageView":Landroid/widget/ImageView;
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mContext:Landroid/content/Context;

    const-string v9, "layout_inflater"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 928
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const/4 v0, 0x0

    .line 929
    .local v0, "buttonId":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    if-eqz v8, :cond_2

    .line 931
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 1003
    if-eqz p2, :cond_0

    move-object v1, p2

    .line 1070
    .end local p2    # "convertView":Landroid/view/View;
    .local v1, "convertView":Landroid/view/View;
    :goto_0
    return-object v1

    .line 935
    .end local v1    # "convertView":Landroid/view/View;
    .restart local p2    # "convertView":Landroid/view/View;
    :sswitch_0
    const/4 v7, 0x0

    .line 936
    .local v7, "stickerHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    if-nez p2, :cond_4

    .line 937
    const v8, 0x7f03006e

    const/4 v9, 0x0

    invoke-virtual {v3, v8, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 938
    new-instance v7, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;

    .end local v7    # "stickerHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    invoke-direct {v7, p0}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;-><init>(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)V

    .line 939
    .restart local v7    # "stickerHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    const v8, 0x7f09013c

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "imageView":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 940
    .restart local v2    # "imageView":Landroid/widget/ImageView;
    iput-object v2, v7, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;->holderImageView:Landroid/widget/ImageView;

    .line 941
    invoke-virtual {p2, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 948
    :goto_1
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 949
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1008
    .end local v7    # "stickerHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    :cond_0
    :goto_2
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    const/4 v9, 0x5

    if-eq v8, v9, :cond_1

    .line 1009
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    const/16 v9, 0xe

    if-eq v8, v9, :cond_1

    .line 1010
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    const/16 v9, 0x9

    if-eq v8, v9, :cond_1

    .line 1011
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    const/16 v9, 0xc

    if-eq v8, v9, :cond_1

    .line 1012
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_6

    .line 1013
    :cond_1
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v0

    .line 1057
    :cond_2
    :goto_3
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "JW final buttonId="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1058
    if-eqz p2, :cond_3

    .line 1060
    invoke-virtual {p2, v0}, Landroid/view/View;->setId(I)V

    .line 1061
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1062
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->FCL:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {p2, v8}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1067
    :cond_3
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mParent:Landroid/view/ViewGroup;

    move-object v1, p2

    .line 1070
    .end local p2    # "convertView":Landroid/view/View;
    .restart local v1    # "convertView":Landroid/view/View;
    goto :goto_0

    .line 944
    .end local v1    # "convertView":Landroid/view/View;
    .restart local v7    # "stickerHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "stickerHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    check-cast v7, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;

    .line 945
    .restart local v7    # "stickerHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    iget-object v2, v7, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;->holderImageView:Landroid/widget/ImageView;

    goto :goto_1

    .line 956
    .end local v7    # "stickerHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    :sswitch_1
    const/4 v4, 0x0

    .line 957
    .local v4, "lableHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    if-nez p2, :cond_5

    .line 958
    const v8, 0x7f03006e

    const/4 v9, 0x0

    invoke-virtual {v3, v8, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 959
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;

    .end local v4    # "lableHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;-><init>(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)V

    .line 960
    .restart local v4    # "lableHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    const v8, 0x7f09013d

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "imageView":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 961
    .restart local v2    # "imageView":Landroid/widget/ImageView;
    iput-object v2, v4, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;->holderImageView:Landroid/widget/ImageView;

    .line 962
    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 969
    :goto_4
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 970
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 965
    :cond_5
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "lableHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    check-cast v4, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;

    .line 966
    .restart local v4    # "lableHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    iget-object v2, v4, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;->holderImageView:Landroid/widget/ImageView;

    goto :goto_4

    .line 980
    .end local v4    # "lableHolder":Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$ImageHolder;
    :sswitch_2
    const v8, 0x7f030043

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 981
    const v8, 0x7f090073

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "imageView":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 983
    .restart local v2    # "imageView":Landroid/widget/ImageView;
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 985
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 986
    .local v5, "mArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Bitmap;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    .line 993
    .end local v5    # "mArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    :sswitch_3
    const v8, 0x7f03006c

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 994
    const v8, 0x7f090073

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "imageView":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 996
    .restart local v2    # "imageView":Landroid/widget/ImageView;
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 998
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 999
    .local v6, "mArrayListstamp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Bitmap;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    .line 1016
    .end local v6    # "mArrayListstamp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    :cond_6
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v8

    sparse-switch v8, :sswitch_data_1

    goto/16 :goto_3

    .line 1019
    :sswitch_4
    const v8, 0xffff

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerId:[I

    aget v9, v9, p1

    if-le v8, v9, :cond_2

    .line 1021
    const v8, 0xffff

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerId:[I

    aget v9, v9, p1

    and-int v0, v8, v9

    .line 1022
    const/high16 v8, 0x31100000

    or-int/2addr v0, v8

    .line 1024
    goto/16 :goto_3

    .line 1026
    :sswitch_5
    const v8, 0xffff

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mLabelId:[I

    aget v9, v9, p1

    if-le v8, v9, :cond_2

    .line 1028
    const v8, 0xffff

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mLabelId:[I

    aget v9, v9, p1

    and-int v0, v8, v9

    .line 1029
    const/high16 v8, 0x31300000

    or-int/2addr v0, v8

    .line 1031
    goto/16 :goto_3

    .line 1033
    :sswitch_6
    const v8, 0xffff

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mWatermarkId:[I

    aget v9, v9, p1

    if-le v8, v9, :cond_2

    .line 1035
    const v8, 0xffff

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mWatermarkId:[I

    aget v9, v9, p1

    and-int v0, v8, v9

    .line 1036
    const/high16 v8, 0x31400000

    or-int/2addr v0, v8

    .line 1038
    goto/16 :goto_3

    .line 1040
    :sswitch_7
    const v8, 0xffff

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mFrameId:[I

    aget v9, v9, p1

    if-le v8, v9, :cond_2

    .line 1042
    const v8, 0xffff

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mFrameId:[I

    aget v9, v9, p1

    and-int v0, v8, v9

    .line 1043
    const/high16 v8, 0x31200000

    or-int/2addr v0, v8

    .line 1045
    goto/16 :goto_3

    .line 1047
    :sswitch_8
    const v8, 0xffff

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStampId:[I

    aget v9, v9, p1

    if-le v8, v9, :cond_2

    .line 1049
    const v8, 0xffff

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStampId:[I

    aget v9, v9, p1

    and-int v0, v8, v9

    .line 1050
    const/high16 v8, 0x31500000

    or-int/2addr v0, v8

    goto/16 :goto_3

    .line 931
    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_0
        0x31200000 -> :sswitch_2
        0x31300000 -> :sswitch_1
        0x31400000 -> :sswitch_0
        0x31500000 -> :sswitch_3
    .end sparse-switch

    .line 1016
    :sswitch_data_1
    .sparse-switch
        0x31100000 -> :sswitch_4
        0x31200000 -> :sswitch_7
        0x31300000 -> :sswitch_5
        0x31400000 -> :sswitch_6
        0x31500000 -> :sswitch_8
    .end sparse-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v10, 0xb

    const v9, 0xffff

    .line 678
    const-string v7, "JW gridAdapter onClick"

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 679
    const/4 v4, 0x0

    .line 680
    .local v4, "item":Landroid/view/View;
    const/4 v1, 0x1

    .line 684
    .local v1, "frameApplied":Z
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_0
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v5, v7, :cond_1

    .line 786
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mNumThumbCount:I

    if-lt v2, v7, :cond_9

    .line 844
    .end local v2    # "i":I
    :cond_0
    :goto_2
    return-void

    .line 687
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mRecentlyList:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$ButtonInfo;->getButtonId()I

    move-result v7

    if-ne v8, v7, :cond_8

    .line 689
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    .line 768
    :cond_2
    :goto_3
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v7

    sparse-switch v7, :sswitch_data_1

    .line 776
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/4 v8, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v9

    invoke-virtual {v7, v8, v9, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setRecentClicked(ZII)V

    .line 780
    :goto_4
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v7

    if-eqz v7, :cond_0

    if-eqz v1, :cond_0

    .line 781
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    goto :goto_2

    .line 692
    :sswitch_0
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_5
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_2

    .line 694
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStickerId:[I

    aget v7, v7, v2

    and-int v0, v9, v7

    .line 695
    .local v0, "buttonId":I
    const/high16 v7, 0x31100000

    or-int/2addr v0, v7

    .line 696
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    if-ne v0, v7, :cond_3

    .line 698
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v8, v7}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setStickerMode(I)V

    goto :goto_3

    .line 692
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 704
    .end local v0    # "buttonId":I
    .end local v2    # "i":I
    :sswitch_1
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_6
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_2

    .line 706
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mLabelId:[I

    aget v7, v7, v2

    and-int v0, v9, v7

    .line 707
    .restart local v0    # "buttonId":I
    const/high16 v7, 0x31300000

    or-int/2addr v0, v7

    .line 708
    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->getLabelIndexByViewId(I)I

    move-result v3

    .line 709
    .local v3, "index":I
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "sj, GA - onClick() - i : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / mLabelId["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mLabelId:[I

    aget v8, v8, v2

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / buttonId : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / index : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 710
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    if-ne v0, v7, :cond_5

    .line 713
    move v6, v3

    .line 714
    .local v6, "styleNum":I
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    if-ne v7, v10, :cond_4

    .line 715
    sget v7, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    add-int/2addr v6, v7

    .line 717
    :cond_4
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/lit8 v9, v6, 0x1

    invoke-virtual {v8, v7, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setLabelMode(II)V

    goto/16 :goto_3

    .line 704
    .end local v6    # "styleNum":I
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 724
    .end local v0    # "buttonId":I
    .end local v2    # "i":I
    .end local v3    # "index":I
    :sswitch_2
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_7
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_2

    .line 726
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mWatermarkId:[I

    aget v7, v7, v2

    and-int v0, v9, v7

    .line 727
    .restart local v0    # "buttonId":I
    const/high16 v7, 0x31400000

    or-int/2addr v0, v7

    .line 728
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    if-eq v0, v7, :cond_2

    .line 724
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 737
    .end local v0    # "buttonId":I
    .end local v2    # "i":I
    :sswitch_3
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_8
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_2

    .line 739
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mFrameId:[I

    aget v7, v7, v2

    and-int v0, v9, v7

    .line 740
    .restart local v0    # "buttonId":I
    const/high16 v7, 0x31200000

    or-int/2addr v0, v7

    .line 741
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "JW FRAME_VIEW: buttonId="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 742
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "JW FRAME_VIEW: v.getId()="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 743
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    if-ne v0, v7, :cond_6

    .line 746
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setFrameMode(I)Z

    move-result v1

    .line 747
    goto/16 :goto_3

    .line 737
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 752
    .end local v0    # "buttonId":I
    .end local v2    # "i":I
    :sswitch_4
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_9
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_2

    .line 754
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mStampId:[I

    aget v7, v7, v2

    and-int v0, v9, v7

    .line 755
    .restart local v0    # "buttonId":I
    const/high16 v7, 0x31500000

    or-int/2addr v0, v7

    .line 756
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    if-ne v0, v7, :cond_7

    .line 760
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setStampMode(I)Z

    move-result v1

    .line 761
    goto/16 :goto_3

    .line 752
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 773
    .end local v0    # "buttonId":I
    .end local v2    # "i":I
    :sswitch_5
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    invoke-virtual {v7, v8, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->resetRecentButton(II)V

    goto/16 :goto_4

    .line 684
    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 788
    .restart local v2    # "i":I
    :cond_9
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mParent:Landroid/view/ViewGroup;

    invoke-virtual {v7, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    if-ne p1, v7, :cond_b

    .line 790
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "JW save Id="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 792
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v7

    sparse-switch v7, :sswitch_data_2

    .line 821
    :goto_a
    :sswitch_6
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "sj, GA - onClick() - v.getId() : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 824
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v7

    sparse-switch v7, :sswitch_data_3

    .line 839
    :goto_b
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v7

    if-eqz v7, :cond_0

    if-eqz v1, :cond_0

    .line 840
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    goto/16 :goto_2

    .line 795
    :sswitch_7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->getStickerIndexByViewId(I)I

    move-result v3

    .line 796
    .restart local v3    # "index":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v8, v7}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setStickerMode(I)V

    goto :goto_a

    .line 799
    .end local v3    # "index":I
    :sswitch_8
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->getLabelIndexByViewId(I)I

    move-result v3

    .line 801
    .restart local v3    # "index":I
    move v6, v3

    .line 802
    .restart local v6    # "styleNum":I
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    if-ne v7, v10, :cond_a

    .line 803
    sget v7, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->MAX_LABEL_TYPE1:I

    add-int/2addr v6, v7

    .line 805
    :cond_a
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "sj, GA - onClick() - mType : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mType:I

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / index : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / styleNum : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 806
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mCurrentRes:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/lit8 v9, v6, 0x1

    invoke-virtual {v8, v7, v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setLabelMode(II)V

    goto/16 :goto_a

    .line 814
    .end local v3    # "index":I
    .end local v6    # "styleNum":I
    :sswitch_9
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setFrameMode(I)Z

    move-result v1

    .line 815
    goto/16 :goto_a

    .line 818
    :sswitch_a
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setStampMode(I)Z

    goto/16 :goto_a

    .line 828
    :sswitch_b
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setRecentButton(I)V

    goto/16 :goto_b

    .line 833
    :sswitch_c
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setRecentId(I)V

    goto/16 :goto_b

    .line 786
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 689
    nop

    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_0
        0x31200000 -> :sswitch_3
        0x31300000 -> :sswitch_1
        0x31400000 -> :sswitch_2
        0x31500000 -> :sswitch_4
    .end sparse-switch

    .line 768
    :sswitch_data_1
    .sparse-switch
        0x31100000 -> :sswitch_5
        0x31300000 -> :sswitch_5
    .end sparse-switch

    .line 792
    :sswitch_data_2
    .sparse-switch
        0x31100000 -> :sswitch_7
        0x31200000 -> :sswitch_9
        0x31300000 -> :sswitch_8
        0x31400000 -> :sswitch_6
        0x31500000 -> :sswitch_a
    .end sparse-switch

    .line 824
    :sswitch_data_3
    .sparse-switch
        0x31100000 -> :sswitch_b
        0x31300000 -> :sswitch_b
        0x31400000 -> :sswitch_c
    .end sparse-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 669
    const/4 v0, 0x1

    return v0
.end method
