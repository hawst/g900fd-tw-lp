.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;
.super Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;
.source "Multigrid5StyleData.java"


# static fields
.field private static final mCollagePreset0:[[F

.field private static final mCollagePreset1:[[F

.field private static final mCollagePreset10:[[F

.field private static final mCollagePreset2:[[F

.field private static final mCollagePreset3:[[F

.field private static final mCollagePreset4:[[F

.field private static final mCollagePreset5:[[F

.field private static final mCollagePreset6:[[F

.field private static final mCollagePreset7:[[F

.field private static final mCollagePreset8:[[F

.field private static final mCollagePreset9:[[F

.field private static final mPilePreset0_1to1:[[F

.field private static final mPilePreset0_9to16:[[F

.field private static final mPilePreset1_1to1:[[F

.field private static final mPilePreset1_9to16:[[F

.field private static final mPilePreset2_1to1:[[F

.field private static final mPilePreset2_9to16:[[F

.field private static final mPilePreset3_1to1:[[F

.field private static final mPilePreset3_9to16:[[F


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 9
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 10
    new-array v1, v3, [F

    .line 11
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 13
    new-array v1, v3, [F

    .line 14
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 16
    new-array v1, v3, [F

    .line 17
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 19
    new-array v1, v3, [F

    .line 20
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 22
    new-array v1, v3, [F

    .line 23
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v3

    .line 7
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset10:[[F

    .line 29
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 30
    new-array v1, v3, [F

    .line 31
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 33
    new-array v1, v3, [F

    .line 34
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 36
    new-array v1, v3, [F

    .line 37
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 39
    new-array v1, v3, [F

    .line 40
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 42
    new-array v1, v3, [F

    .line 43
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v3

    .line 27
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset9:[[F

    .line 49
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 50
    new-array v1, v3, [F

    .line 51
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 53
    new-array v1, v3, [F

    .line 54
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 56
    new-array v1, v3, [F

    .line 57
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 59
    new-array v1, v3, [F

    .line 60
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 62
    new-array v1, v3, [F

    .line 63
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v3

    .line 47
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset8:[[F

    .line 69
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 70
    new-array v1, v3, [F

    .line 71
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 73
    new-array v1, v3, [F

    .line 74
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 76
    new-array v1, v3, [F

    .line 77
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 79
    new-array v1, v3, [F

    .line 80
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 82
    new-array v1, v3, [F

    .line 83
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v3

    .line 67
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset7:[[F

    .line 89
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 90
    new-array v1, v3, [F

    .line 91
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 93
    new-array v1, v3, [F

    .line 94
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 96
    new-array v1, v3, [F

    .line 97
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 99
    new-array v1, v3, [F

    .line 100
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 102
    new-array v1, v3, [F

    .line 103
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v3

    .line 87
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset6:[[F

    .line 109
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 110
    new-array v1, v3, [F

    .line 111
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p20:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 113
    new-array v1, v3, [F

    .line 114
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p20:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p40:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 116
    new-array v1, v3, [F

    .line 117
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p40:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p60:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 119
    new-array v1, v3, [F

    .line 120
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p60:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p80:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 122
    new-array v1, v3, [F

    .line 123
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p80:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v3

    .line 107
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset5:[[F

    .line 129
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 130
    new-array v1, v3, [F

    .line 131
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p20:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 133
    new-array v1, v3, [F

    .line 134
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p20:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p40:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 136
    new-array v1, v3, [F

    .line 137
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p40:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p60:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 139
    new-array v1, v3, [F

    .line 140
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p60:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p80:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 142
    new-array v1, v3, [F

    .line 143
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p80:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v3

    .line 127
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset4:[[F

    .line 149
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 150
    new-array v1, v3, [F

    .line 151
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 153
    new-array v1, v3, [F

    .line 154
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 156
    new-array v1, v3, [F

    .line 157
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 159
    new-array v1, v3, [F

    .line 160
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 162
    new-array v1, v3, [F

    .line 163
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v3

    .line 147
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset3:[[F

    .line 169
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 170
    new-array v1, v3, [F

    .line 171
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 173
    new-array v1, v3, [F

    .line 174
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 176
    new-array v1, v3, [F

    .line 177
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 179
    new-array v1, v3, [F

    .line 180
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 182
    new-array v1, v3, [F

    .line 183
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v3

    .line 167
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset2:[[F

    .line 189
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 190
    new-array v1, v3, [F

    .line 191
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 193
    new-array v1, v3, [F

    .line 194
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 196
    new-array v1, v3, [F

    .line 197
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 199
    new-array v1, v3, [F

    .line 200
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 202
    new-array v1, v3, [F

    .line 203
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v3

    .line 187
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset0:[[F

    .line 208
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 209
    new-array v1, v3, [F

    .line 210
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 212
    new-array v1, v3, [F

    .line 213
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 215
    new-array v1, v3, [F

    .line 216
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 218
    new-array v1, v3, [F

    .line 219
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 221
    new-array v1, v3, [F

    .line 222
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v3

    .line 206
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset1:[[F

    .line 227
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 228
    new-array v1, v3, [F

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    .line 229
    new-array v1, v3, [F

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    .line 230
    new-array v1, v3, [F

    fill-array-data v1, :array_2

    aput-object v1, v0, v6

    .line 231
    new-array v1, v3, [F

    fill-array-data v1, :array_3

    aput-object v1, v0, v7

    .line 232
    new-array v1, v3, [F

    fill-array-data v1, :array_4

    aput-object v1, v0, v3

    .line 226
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset0_1to1:[[F

    .line 236
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 237
    new-array v1, v3, [F

    fill-array-data v1, :array_5

    aput-object v1, v0, v4

    .line 238
    new-array v1, v3, [F

    fill-array-data v1, :array_6

    aput-object v1, v0, v5

    .line 239
    new-array v1, v3, [F

    fill-array-data v1, :array_7

    aput-object v1, v0, v6

    .line 240
    new-array v1, v3, [F

    fill-array-data v1, :array_8

    aput-object v1, v0, v7

    .line 241
    new-array v1, v3, [F

    fill-array-data v1, :array_9

    aput-object v1, v0, v3

    .line 235
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset1_1to1:[[F

    .line 245
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 246
    new-array v1, v3, [F

    fill-array-data v1, :array_a

    aput-object v1, v0, v4

    .line 247
    new-array v1, v3, [F

    fill-array-data v1, :array_b

    aput-object v1, v0, v5

    .line 248
    new-array v1, v3, [F

    fill-array-data v1, :array_c

    aput-object v1, v0, v6

    .line 249
    new-array v1, v3, [F

    fill-array-data v1, :array_d

    aput-object v1, v0, v7

    .line 250
    new-array v1, v3, [F

    fill-array-data v1, :array_e

    aput-object v1, v0, v3

    .line 244
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset2_1to1:[[F

    .line 254
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 255
    new-array v1, v3, [F

    fill-array-data v1, :array_f

    aput-object v1, v0, v4

    .line 256
    new-array v1, v3, [F

    fill-array-data v1, :array_10

    aput-object v1, v0, v5

    .line 257
    new-array v1, v3, [F

    fill-array-data v1, :array_11

    aput-object v1, v0, v6

    .line 258
    new-array v1, v3, [F

    fill-array-data v1, :array_12

    aput-object v1, v0, v7

    .line 259
    new-array v1, v3, [F

    fill-array-data v1, :array_13

    aput-object v1, v0, v3

    .line 253
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset3_1to1:[[F

    .line 263
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 264
    new-array v1, v3, [F

    fill-array-data v1, :array_14

    aput-object v1, v0, v4

    .line 265
    new-array v1, v3, [F

    fill-array-data v1, :array_15

    aput-object v1, v0, v5

    .line 266
    new-array v1, v3, [F

    fill-array-data v1, :array_16

    aput-object v1, v0, v6

    .line 267
    new-array v1, v3, [F

    fill-array-data v1, :array_17

    aput-object v1, v0, v7

    .line 268
    new-array v1, v3, [F

    fill-array-data v1, :array_18

    aput-object v1, v0, v3

    .line 262
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset0_9to16:[[F

    .line 271
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 272
    new-array v1, v3, [F

    fill-array-data v1, :array_19

    aput-object v1, v0, v4

    .line 273
    new-array v1, v3, [F

    fill-array-data v1, :array_1a

    aput-object v1, v0, v5

    .line 274
    new-array v1, v3, [F

    fill-array-data v1, :array_1b

    aput-object v1, v0, v6

    .line 275
    new-array v1, v3, [F

    fill-array-data v1, :array_1c

    aput-object v1, v0, v7

    .line 276
    new-array v1, v3, [F

    fill-array-data v1, :array_1d

    aput-object v1, v0, v3

    .line 270
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset1_9to16:[[F

    .line 280
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 282
    new-array v1, v3, [F

    fill-array-data v1, :array_1e

    aput-object v1, v0, v4

    .line 283
    new-array v1, v3, [F

    fill-array-data v1, :array_1f

    aput-object v1, v0, v5

    .line 284
    new-array v1, v3, [F

    fill-array-data v1, :array_20

    aput-object v1, v0, v6

    .line 285
    new-array v1, v3, [F

    fill-array-data v1, :array_21

    aput-object v1, v0, v7

    .line 286
    new-array v1, v3, [F

    fill-array-data v1, :array_22

    aput-object v1, v0, v3

    .line 279
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset2_9to16:[[F

    .line 290
    const/4 v0, 0x5

    new-array v0, v0, [[F

    .line 291
    new-array v1, v3, [F

    fill-array-data v1, :array_23

    aput-object v1, v0, v4

    .line 292
    new-array v1, v3, [F

    fill-array-data v1, :array_24

    aput-object v1, v0, v5

    .line 293
    new-array v1, v3, [F

    fill-array-data v1, :array_25

    aput-object v1, v0, v6

    .line 294
    new-array v1, v3, [F

    fill-array-data v1, :array_26

    aput-object v1, v0, v7

    .line 295
    new-array v1, v3, [F

    fill-array-data v1, :array_27

    aput-object v1, v0, v3

    .line 289
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset3_9to16:[[F

    .line 296
    return-void

    .line 228
    nop

    :array_0
    .array-data 4
        0x3de94467    # 0.1139f
        -0x42b33333    # -0.05f
        0x3f244674    # 0.6417f
        0x3f071de7    # 0.5278f
    .end array-data

    .line 229
    :array_1
    .array-data 4
        0x3f02d773    # 0.5111f
        0x3d086595    # 0.0333f
        0x3f7c710d    # 0.9861f
        0x3f1b089a    # 0.6056f
    .end array-data

    .line 230
    :array_2
    .array-data 4
        0x3d4154ca    # 0.0472f
        0x3ef33333    # 0.475f
        0x3f0b5dcc    # 0.5444f
        0x3f866666    # 1.05f
    .end array-data

    .line 231
    :array_3
    .array-data 4
        0x3f044674    # 0.5167f
        0x3f13eab3    # 0.5778f
        0x3f7f4880    # 0.9972f
        0x3f7779a7    # 0.9667f
    .end array-data

    .line 232
    :array_4
    .array-data 4
        0x3e8ccccd    # 0.275f
        0x3e3e9100    # 0.1861f
        0x3f4d844d    # 0.8028f
        0x3f1de00d    # 0.6167f
    .end array-data

    .line 237
    :array_5
    .array-data 4
        0x3dcccccd    # 0.1f
        0x3f00b780    # 0.5028f
        0x3f00b780    # 0.5028f
        0x3f51c433    # 0.8194f
    .end array-data

    .line 238
    :array_6
    .array-data 4
        0x3f0aacda    # 0.5417f
        0x3efa511a    # 0.4889f
        0x3f62d773    # 0.8861f
        0x3f6ccccd    # 0.925f
    .end array-data

    .line 239
    :array_7
    .array-data 4
        -0x439c432d    # -0.0139f
        0x3e5288ce    # 0.2056f
        0x3ebd21ff    # 0.3694f
        0x3f02d773    # 0.5111f
    .end array-data

    .line 240
    :array_8
    .array-data 4
        0x3f271de7    # 0.6528f
        0x3e778034    # 0.2417f
        0x3f84432d    # 1.0333f
        0x3f0c154d    # 0.5472f
    .end array-data

    .line 241
    :array_9
    .array-data 4
        0x3ea7d567    # 0.3278f
        0x3e24f766    # 0.1611f
        0x3f305bc0    # 0.6889f
        0x3f200000    # 0.625f
    .end array-data

    .line 246
    :array_a
    .array-data 4
        0x3f2d844d    # 0.6778f
        0x3f14a234    # 0.5806f
        0x3f711340    # 0.9417f
        0x3f693dd9    # 0.9111f
    .end array-data

    .line 247
    :array_b
    .array-data 4
        0x3ebe9100    # 0.3722f
        0x3f11c433    # 0.5694f
        0x3f266666    # 0.65f
        0x3f6b5dcc    # 0.9194f
    .end array-data

    .line 248
    :array_c
    .array-data 4
        0x3d1f559b    # 0.0389f
        0x3f16c227    # 0.5889f
        0x3eac154d    # 0.3361f
        0x3f511340    # 0.8167f
    .end array-data

    .line 249
    :array_d
    .array-data 4
        0x3df487fd    # 0.1194f
        0x3e087fcc    # 0.1333f
        0x3edf4880    # 0.4361f
        0x3f07d567    # 0.5306f
    .end array-data

    .line 250
    :array_e
    .array-data 4
        0x3f066666    # 0.525f
        0x3e1c779a    # 0.1528f
        0x3f67d567    # 0.9056f
        0x3ee4f766    # 0.4472f
    .end array-data

    .line 255
    :array_f
    .array-data 4
        0x3dddcc64    # 0.1083f
        0x3e7d21ff    # 0.2472f
        0x3ec71de7    # 0.3889f
        0x3ed6bb99    # 0.4194f
    .end array-data

    .line 256
    :array_10
    .array-data 4
        0x3f1bb98c    # 0.6083f
        0x3e19999a    # 0.15f
        0x3f5bb98c    # 0.8583f
        0x3ecfaace    # 0.4056f
    .end array-data

    .line 257
    :array_11
    .array-data 4
        0x3dfa43fe    # 0.1222f
        0x3f160aa6    # 0.5861f
        0x3e9b089a    # 0.3028f
        0x3f3d288d    # 0.7389f
    .end array-data

    .line 258
    :array_12
    .array-data 4
        0x3eec154d    # 0.4611f
        0x3f155326    # 0.5833f
        0x3f19999a    # 0.6f
        0x3f505bc0    # 0.8139f
    .end array-data

    .line 259
    :array_13
    .array-data 4
        0x3f3d288d    # 0.7389f
        0x3f13eab3    # 0.5778f
        0x3f600000    # 0.875f
        0x3f327bb3    # 0.6972f
    .end array-data

    .line 264
    :array_14
    .array-data 4
        0x3ee7d567    # 0.4528f
        0x3e200d1b    # 0.1563f
        0x3f882de0    # 1.0639f
        0x3f12680a    # 0.5719f
    .end array-data

    .line 265
    :array_15
    .array-data 4
        0x3d086595    # 0.0333f
        0x3f259b3d    # 0.6469f
        0x3f271de7    # 0.6528f
        0x3f879a6b    # 1.0594f
    .end array-data

    .line 266
    :array_16
    .array-data 4
        -0x424432ca    # -0.0917f
        0x3e8d9e84    # 0.2766f
        0x3f155326    # 0.5833f
        0x3f34d014    # 0.7063f
    .end array-data

    .line 267
    :array_17
    .array-data 4
        0x3c9eecc0    # 0.0194f
        0x3bff9724    # 0.0078f
        0x3f2ccccd    # 0.675f
        0x3e9f2e49    # 0.3109f
    .end array-data

    .line 268
    :array_18
    .array-data 4
        0x3ed10cb3    # 0.4083f
        0x3f11ff2e    # 0.5703f
        0x3f89f55a    # 1.0778f
        0x3f659b3d    # 0.8969f
    .end array-data

    .line 272
    :array_19
    .array-data 4
        0x3c9eecc0    # 0.0194f
        0x3f000000    # 0.5f
        0x3f1b089a    # 0.6056f
        0x3f42680a    # 0.7594f
    .end array-data

    .line 273
    :array_1a
    .array-data 4
        0x3ed9999a    # 0.425f
        0x3f35fd8b    # 0.7109f
        0x3f7a511a    # 0.9778f
        0x3f753261    # 0.9578f
    .end array-data

    .line 274
    :array_1b
    .array-data 4
        0x3d1f559b    # 0.0389f
        0x3d797247    # 0.0609f
        0x3f066666    # 0.525f
        0x3ed33333    # 0.4125f
    .end array-data

    .line 275
    :array_1c
    .array-data 4
        0x3eed844d    # 0.4639f
        0x3dd66cf4    # 0.1047f
        0x3f827bb3    # 1.0194f
        0x3eb59b3d    # 0.3547f
    .end array-data

    .line 276
    :array_1d
    .array-data 4
        0x3ea38866    # 0.3194f
        0x3e99999a    # 0.3f
        0x3f51c433    # 0.8194f
        0x3f280347    # 0.6563f
    .end array-data

    .line 282
    :array_1e
    .array-data 4
        0x3f2c154d    # 0.6722f
        0x3e3b2fec    # 0.1828f
        0x3f9a511a    # 1.2056f
        0x3ed80347    # 0.4219f
    .end array-data

    .line 283
    :array_1f
    .array-data 4
        0x3e910cb3    # 0.2833f
        0x3e3e5c92    # 0.1859f
        0x3f42d773    # 0.7611f
        0x3f046738    # 0.5172f
    .end array-data

    .line 284
    :array_20
    .array-data 4
        -0x426c2268    # -0.0722f
        0x3e3e5c92    # 0.1859f
        0x3ead844d    # 0.3389f
        0x3ef66cf4    # 0.4813f
    .end array-data

    .line 285
    :array_21
    .array-data 4
        0x3c88ce70    # 0.0167f
        0x3f113405    # 0.5672f
        0x3f04f766    # 0.5194f
        0x3f4a0275    # 0.7891f
    .end array-data

    .line 286
    :array_22
    .array-data 4
        0x3f13eab3    # 0.5778f
        0x3f10cb29    # 0.5656f
        0x3f733333    # 0.95f
        0x3f54d014    # 0.8313f
    .end array-data

    .line 291
    :array_23
    .array-data 4
        0x3e1f559b    # 0.1556f
        0x3e333333    # 0.175f
        0x3eb61134    # 0.3556f
        0x3e9cd35b    # 0.3063f
    .end array-data

    .line 292
    :array_24
    .array-data 4
        0x3f182a99    # 0.5944f
        0x3dbcd35b    # 0.0922f
        0x3f51c433    # 0.8194f
        0x3e99999a    # 0.3f
    .end array-data

    .line 293
    :array_25
    .array-data 4
        0x3e361134    # 0.1778f
        0x3edf2e49    # 0.4359f
        0x3ee7d567    # 0.4528f
        0x3f0a64c3    # 0.5406f
    .end array-data

    .line 294
    :array_26
    .array-data 4
        0x3f34a234    # 0.7056f
        0x3ee2680a    # 0.4422f
        0x3f582a99    # 0.8444f
        0x3f079a6b    # 0.5297f
    .end array-data

    .line 295
    :array_27
    .array-data 4
        0x3ea38866    # 0.3194f
        0x3f2e00d2    # 0.6797f
        0x3f25aee6    # 0.6472f
        0x3f5f9724    # 0.8734f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;-><init>()V

    return-void
.end method

.method public static getDrawRects(II)[[F
    .locals 4
    .param p0, "style"    # I
    .param p1, "proportion"    # I

    .prologue
    const/high16 v3, 0x1e500000

    .line 302
    const/4 v0, 0x0

    .line 304
    .local v0, "returnPreset":[[F
    const/high16 v1, 0x1e200000

    const/4 v2, 0x5

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getMaxStyleNum(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    move-result p0

    .line 306
    packed-switch p0, :pswitch_data_0

    .line 397
    const/4 v0, 0x0

    .line 401
    :goto_0
    return-object v0

    .line 309
    :pswitch_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset0:[[F

    .line 310
    goto :goto_0

    .line 313
    :pswitch_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset1:[[F

    .line 314
    goto :goto_0

    .line 317
    :pswitch_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset2:[[F

    .line 318
    goto :goto_0

    .line 321
    :pswitch_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset3:[[F

    .line 322
    goto :goto_0

    .line 325
    :pswitch_4
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset4:[[F

    .line 326
    goto :goto_0

    .line 329
    :pswitch_5
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset5:[[F

    .line 330
    goto :goto_0

    .line 333
    :pswitch_6
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset6:[[F

    .line 334
    goto :goto_0

    .line 337
    :pswitch_7
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset7:[[F

    .line 338
    goto :goto_0

    .line 341
    :pswitch_8
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset8:[[F

    .line 342
    goto :goto_0

    .line 345
    :pswitch_9
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset9:[[F

    .line 346
    goto :goto_0

    .line 349
    :pswitch_a
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mCollagePreset10:[[F

    .line 350
    goto :goto_0

    .line 354
    :pswitch_b
    if-ne p1, v3, :cond_0

    .line 356
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset0_1to1:[[F

    .line 357
    goto :goto_0

    .line 360
    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset0_9to16:[[F

    .line 362
    goto :goto_0

    .line 365
    :pswitch_c
    if-ne p1, v3, :cond_1

    .line 367
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset1_1to1:[[F

    .line 368
    goto :goto_0

    .line 371
    :cond_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset1_9to16:[[F

    .line 373
    goto :goto_0

    .line 376
    :pswitch_d
    if-ne p1, v3, :cond_2

    .line 378
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset2_1to1:[[F

    .line 379
    goto :goto_0

    .line 382
    :cond_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset2_9to16:[[F

    .line 384
    goto :goto_0

    .line 387
    :pswitch_e
    if-ne p1, v3, :cond_3

    .line 389
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset3_1to1:[[F

    .line 390
    goto :goto_0

    .line 393
    :cond_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid5StyleData;->mPilePreset3_9to16:[[F

    .line 395
    goto :goto_0

    .line 306
    nop

    :pswitch_data_0
    .packed-switch 0x1e200000
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method


# virtual methods
.method public getRotate(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 408
    const/4 v0, 0x0

    return v0
.end method
