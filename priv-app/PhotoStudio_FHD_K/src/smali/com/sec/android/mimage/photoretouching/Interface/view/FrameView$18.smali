.class Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$18;
.super Ljava/lang/Object;
.source "FrameView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

.field private final synthetic val$activities:Ljava/util/List;

.field private final synthetic val$intent:Landroid/content/Intent;

.field private final synthetic val$setAsGridView:Landroid/widget/GridView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;Landroid/widget/GridView;Landroid/content/Intent;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$18;->val$setAsGridView:Landroid/widget/GridView;

    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$18;->val$intent:Landroid/content/Intent;

    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$18;->val$activities:Ljava/util/List;

    .line 2126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2130
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$18;->val$setAsGridView:Landroid/widget/GridView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2132
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$18;->val$intent:Landroid/content/Intent;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 2133
    .local v1, "resolvedIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$18;->val$activities:Ljava/util/List;

    invoke-interface {v2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    iget-object v0, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 2135
    .local v0, "ai":Landroid/content/pm/ActivityInfo;
    new-instance v2, Landroid/content/ComponentName;

    .line 2136
    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2135
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2137
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v2, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->startActivity(Landroid/content/Intent;)V

    .line 2148
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 2149
    return-void
.end method
