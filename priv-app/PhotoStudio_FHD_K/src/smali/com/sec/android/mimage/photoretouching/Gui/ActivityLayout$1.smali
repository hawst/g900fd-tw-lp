.class Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;
.super Ljava/lang/Object;
.source "ActivityLayout.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 106
    const/4 v0, 0x0

    .line 107
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mIsIntercept:Z
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;F)V

    .line 110
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;F)V

    .line 111
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 124
    :cond_0
    :goto_0
    return v0

    .line 115
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosX:F
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->access$5(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 116
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mImageView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosY:F
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->access$6(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mThumbnailHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->access$7(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 117
    const/4 v0, 0x1

    .line 118
    goto :goto_0

    .line 120
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosX:F
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->mPosY:F
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->access$6(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;)F

    move-result v3

    # invokes: Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->setDisableIntercept(FF)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->access$8(Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;FF)V

    goto :goto_0

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
