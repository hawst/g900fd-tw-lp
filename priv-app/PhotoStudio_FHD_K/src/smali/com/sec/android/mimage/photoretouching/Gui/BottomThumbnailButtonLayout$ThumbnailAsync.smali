.class Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;
.super Landroid/os/AsyncTask;
.source "BottomThumbnailButtonLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ThumbnailAsync"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field asyncCallback:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$AsyncCallback;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$AsyncCallback;)V
    .locals 1
    .param p2, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$AsyncCallback;

    .prologue
    .line 458
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .line 457
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 456
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;->asyncCallback:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$AsyncCallback;

    .line 459
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;->asyncCallback:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$AsyncCallback;

    .line 460
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 463
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;->asyncCallback:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$AsyncCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$AsyncCallback;->donInBackground()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 471
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;->asyncCallback:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$AsyncCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$AsyncCallback;->onPostExcute(Landroid/graphics/Bitmap;)V

    .line 472
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 473
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$ThumbnailAsync;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 467
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 468
    return-void
.end method
