.class Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;
.super Ljava/lang/Object;
.source "NormalView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionbarNBottomButtonAnimation"
.end annotation


# instance fields
.field private mIsShowing:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 3596
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    .line 3595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3594
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 3597
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 3598
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    .locals 1

    .prologue
    .line 3592
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized hide()V
    .locals 4

    .prologue
    .line 3644
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 3646
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isAnimation()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3648
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 3649
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3651
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->hide()V

    .line 3653
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3655
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->hide(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3661
    :cond_1
    const-wide/16 v2, 0x1f4

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3665
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3667
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->hide(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3671
    :cond_2
    monitor-exit p0

    return-void

    .line 3662
    :catch_0
    move-exception v0

    .line 3663
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 3644
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized isShowing()Z
    .locals 1

    .prologue
    .line 3601
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->mIsShowing:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized show()V
    .locals 4

    .prologue
    .line 3605
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3607
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->isAnimation()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3609
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 3610
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3612
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->show(Z)V

    .line 3614
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ActionbarNBottomButtonAnimation;)V

    .line 3621
    const-wide/16 v2, 0xc8

    .line 3614
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3641
    :cond_1
    monitor-exit p0

    return-void

    .line 3605
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
