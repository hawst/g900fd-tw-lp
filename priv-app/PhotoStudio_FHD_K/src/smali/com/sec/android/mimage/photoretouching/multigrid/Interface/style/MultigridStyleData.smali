.class public abstract Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;
.super Ljava/lang/Object;
.source "MultigridStyleData.java"


# static fields
.field public static BOTTOM:I

.field protected static GRID_CNT:[I

.field public static LEFT:I

.field protected static MAX_BG_CNT:I

.field protected static MAX_SPLIT_CNT:I

.field protected static MIN_SPLIT_CNT:I

.field public static RIGHT:I

.field protected static START_ICON_ID:[I

.field protected static STYLE_CNT:[I

.field public static TOP:I

.field protected static p0:F

.field protected static p100:F

.field protected static p20:F

.field protected static p25:F

.field protected static p33:F

.field protected static p375:F

.field protected static p40:F

.field protected static p50:F

.field protected static p60:F

.field protected static p625:F

.field protected static p66:F

.field protected static p75:F

.field protected static p80:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x5

    .line 15
    sput v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MIN_SPLIT_CNT:I

    .line 16
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MAX_SPLIT_CNT:I

    .line 18
    const/16 v0, 0x1c

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MAX_BG_CNT:I

    .line 20
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->GRID_CNT:[I

    .line 28
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->STYLE_CNT:[I

    .line 50
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->START_ICON_ID:[I

    .line 58
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->p0:F

    .line 59
    const v0, 0x3e4ccccd    # 0.2f

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->p20:F

    .line 60
    const/high16 v0, 0x3e800000    # 0.25f

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->p25:F

    .line 61
    const v0, 0x3ea8f5c3    # 0.33f

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->p33:F

    .line 62
    const/high16 v0, 0x3ec00000    # 0.375f

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->p375:F

    .line 63
    const v0, 0x3ecccccd    # 0.4f

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->p40:F

    .line 64
    const/high16 v0, 0x3f000000    # 0.5f

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->p50:F

    .line 65
    const v0, 0x3f19999a    # 0.6f

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->p60:F

    .line 66
    const/high16 v0, 0x3f200000    # 0.625f

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->p625:F

    .line 67
    const v0, 0x3f28f5c3    # 0.66f

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->p66:F

    .line 68
    const/high16 v0, 0x3f400000    # 0.75f

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->p75:F

    .line 69
    const v0, 0x3f4ccccd    # 0.8f

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->p80:F

    .line 70
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->p100:F

    .line 72
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->LEFT:I

    .line 73
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->TOP:I

    .line 74
    sput v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->RIGHT:I

    .line 75
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->BOTTOM:I

    return-void

    .line 20
    nop

    :array_0
    .array-data 4
        0x6
        0xa
        0xb
        0xb
        0x8
    .end array-data

    .line 28
    :array_1
    .array-data 4
        0xa
        0xe
        0xf
        0xf
        0xc
    .end array-data

    .line 50
    :array_2
    .array-data 4
        0x7f020047
        0x7f020051
        0x7f02005f
        0x7f02006e
        0x7f02007d
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method public static getBgImgCnt()I
    .locals 1

    .prologue
    .line 169
    sget v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MAX_BG_CNT:I

    return v0
.end method

.method public static getDrawRects(I)[[F
    .locals 1
    .param p0, "style"    # I

    .prologue
    .line 164
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getLayoutIcon(I)I
    .locals 2
    .param p0, "split"    # I

    .prologue
    .line 98
    sget v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MIN_SPLIT_CNT:I

    if-lt p0, v0, :cond_0

    sget v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MAX_SPLIT_CNT:I

    if-le p0, v0, :cond_1

    .line 100
    :cond_0
    const/4 v0, 0x0

    .line 102
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->START_ICON_ID:[I

    sget v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MIN_SPLIT_CNT:I

    sub-int v1, p0, v1

    aget v0, v0, v1

    goto :goto_0
.end method

.method public static getMaxGridStyleId(I)I
    .locals 2
    .param p0, "split"    # I

    .prologue
    const/high16 v1, 0x1e200000

    .line 158
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getMaxGridStyleNum(I)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static getMaxGridStyleNum(I)I
    .locals 2
    .param p0, "split"    # I

    .prologue
    .line 81
    sget v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MIN_SPLIT_CNT:I

    if-lt p0, v0, :cond_0

    sget v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MAX_SPLIT_CNT:I

    if-le p0, v0, :cond_1

    .line 83
    :cond_0
    const/4 v0, 0x0

    .line 85
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->GRID_CNT:[I

    sget v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MIN_SPLIT_CNT:I

    sub-int v1, p0, v1

    aget v0, v0, v1

    goto :goto_0
.end method

.method public static getMaxStyleId(I)I
    .locals 2
    .param p0, "split"    # I

    .prologue
    const/high16 v1, 0x1e200000

    .line 153
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getMaxStyleNum(I)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static getMaxStyleNum(I)I
    .locals 2
    .param p0, "split"    # I

    .prologue
    .line 89
    sget v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MIN_SPLIT_CNT:I

    if-lt p0, v0, :cond_0

    sget v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MAX_SPLIT_CNT:I

    if-le p0, v0, :cond_1

    .line 91
    :cond_0
    const/4 v0, 0x0

    .line 93
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->STYLE_CNT:[I

    sget v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MIN_SPLIT_CNT:I

    sub-int v1, p0, v1

    aget v0, v0, v1

    goto :goto_0
.end method

.method public static getPileStyle(I)I
    .locals 4
    .param p0, "split"    # I

    .prologue
    .line 107
    sget-object v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->STYLE_CNT:[I

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MIN_SPLIT_CNT:I

    sub-int v2, p0, v2

    aget v1, v1, v2

    sget-object v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->GRID_CNT:[I

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MIN_SPLIT_CNT:I

    sub-int v3, p0, v3

    aget v2, v2, v3

    sub-int v0, v1, v2

    .line 109
    .local v0, "style":I
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 110
    return v0
.end method

.method public static getPileStyleId(II)I
    .locals 2
    .param p0, "index"    # I
    .param p1, "split"    # I

    .prologue
    .line 133
    const/4 v0, 0x0

    .line 134
    .local v0, "pileStyleID":I
    invoke-static {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->isPileStyle(II)I

    move-result v0

    .line 136
    if-ltz v0, :cond_0

    .line 138
    const v1, 0x1e200100

    add-int/2addr v0, v1

    .line 141
    :cond_0
    return v0
.end method

.method public static getStyleId(I)I
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 147
    const/high16 v0, 0x1e200000

    add-int/2addr v0, p0

    return v0
.end method

.method public static isPileStyle(II)I
    .locals 5
    .param p0, "index"    # I
    .param p1, "split"    # I

    .prologue
    const/4 v1, -0x1

    .line 115
    if-ltz p0, :cond_0

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MIN_SPLIT_CNT:I

    if-lt p1, v2, :cond_0

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MAX_SPLIT_CNT:I

    if-le p1, v2, :cond_2

    :cond_0
    move v0, v1

    .line 128
    :cond_1
    :goto_0
    return v0

    .line 120
    :cond_2
    const/4 v0, -0x1

    .line 122
    .local v0, "pileSytleNum":I
    const/high16 v2, 0x1e200000

    sub-int v2, p0, v2

    sget-object v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->GRID_CNT:[I

    sget v4, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->MIN_SPLIT_CNT:I

    sub-int v4, p1, v4

    aget v3, v3, v4

    sub-int v0, v2, v3

    .line 124
    if-ltz v0, :cond_3

    const/4 v2, 0x3

    if-le v0, v2, :cond_1

    :cond_3
    move v0, v1

    .line 126
    goto :goto_0
.end method


# virtual methods
.method public abstract getRotate(I)I
.end method
