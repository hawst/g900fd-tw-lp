.class Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$1;
.super Ljava/lang/Object;
.source "LabelView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public actionBarAbleSave()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableSave()V

    .line 127
    :cond_0
    return-void
.end method

.method public actionBarUnableSave()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableSave()V

    .line 132
    :cond_0
    return-void
.end method

.method public closeDecorationDrawer()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    .line 140
    return-void
.end method

.method public openDecorationDrawer()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->openDrawer()V

    .line 136
    return-void
.end method

.method public set2DepthActionBar()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->set2depth()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 144
    return-void
.end method

.method public setVisibleMenu(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->setVisibleTopBottomMenu(Z)V
    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;Z)V

    .line 122
    return-void
.end method
