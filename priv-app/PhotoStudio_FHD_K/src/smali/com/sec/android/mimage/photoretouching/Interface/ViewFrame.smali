.class public abstract Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.super Ljava/lang/Object;
.source "ViewFrame.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$CropLassoOnLayoutCallback;,
        Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;,
        Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;
    }
.end annotation


# instance fields
.field private mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

.field private mContext:Landroid/content/Context;

.field private mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

.field private mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

.field private mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 598
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    .line 600
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    .line 601
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    .line 603
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    .line 604
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    .line 88
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    .line 90
    const-string v0, "ViewFrame C\'tor"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;)Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;
    .locals 1

    .prologue
    .line 600
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    return-object v0
.end method

.method private initCollageAnimationView(Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 3
    .param p1, "mainView"    # Landroid/view/ViewGroup;
    .param p2, "frontView"    # Landroid/view/View;

    .prologue
    .line 453
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    if-eqz v1, :cond_0

    .line 455
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 456
    .local v0, "parent":Landroid/view/ViewGroup;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 457
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->destroy()V

    .line 458
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    .line 460
    .end local v0    # "parent":Landroid/view/ViewGroup;
    :cond_0
    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    .line 461
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 462
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->bringChildToFront(Landroid/view/View;)V

    .line 463
    return-void
.end method

.method private initMainView()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const v5, 0x7f0900be

    .line 466
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 547
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v2, :cond_0

    .line 548
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->destroy()V

    .line 549
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    .line 550
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v2, :cond_1

    .line 552
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$9;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setOnDrawCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;)V

    .line 566
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$10;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 579
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$11;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setOnLayoutCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnLayoutCallback;)V

    .line 597
    :cond_1
    :goto_0
    :sswitch_0
    return-void

    .line 474
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v2, :cond_2

    .line 475
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->destroy()V

    .line 476
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0900af

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 478
    .local v1, "mainViewLayout":Landroid/view/ViewGroup;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03003f

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 479
    .local v0, "mainView":Landroid/view/ViewGroup;
    if-eqz v0, :cond_1

    .line 481
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 482
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 483
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    .line 484
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v2, :cond_3

    .line 486
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$6;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setOnDrawCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;)V

    .line 498
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$7;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 511
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$8;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setOnLayoutCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnLayoutCallback;)V

    .line 521
    :cond_3
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x20000000

    if-ne v2, v3, :cond_4

    .line 523
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    goto :goto_0

    .line 536
    :cond_4
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x2c000000

    if-eq v2, v3, :cond_5

    .line 537
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x1e110000

    if-ne v2, v3, :cond_1

    .line 539
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->initCollageAnimationView(Landroid/view/ViewGroup;Landroid/view/View;)V

    goto/16 :goto_0

    .line 466
    :sswitch_data_0
    .sparse-switch
        0x1e110000 -> :sswitch_1
        0x20000000 -> :sswitch_1
        0x2b000000 -> :sswitch_1
        0x2c000000 -> :sswitch_1
        0x2e000000 -> :sswitch_0
        0x2f000000 -> :sswitch_1
    .end sparse-switch
.end method

.method private setViewPosition()V
    .locals 4

    .prologue
    .line 443
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0900be

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    .line 445
    .local v1, "iView":Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;
    :try_start_0
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->requestLayout()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    :goto_0
    return-void

    .line 446
    :catch_0
    move-exception v0

    .line 448
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected clearCollageAnimationView()V
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->clearCanvas()V

    .line 331
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 350
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    if-eqz v1, :cond_0

    .line 351
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->onDestroy()V

    .line 353
    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x2e000000

    if-ne v1, v2, :cond_1

    .line 355
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->destroy()V

    .line 356
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    if-eqz v1, :cond_1

    .line 358
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 359
    .local v0, "parent":Landroid/view/ViewGroup;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 360
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->destroy()V

    .line 361
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    .line 365
    .end local v0    # "parent":Landroid/view/ViewGroup;
    :cond_1
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    .line 366
    return-void
.end method

.method protected getImageEditViewHeight()I
    .locals 2

    .prologue
    .line 429
    const/4 v0, 0x0

    .line 430
    .local v0, "ret":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v1, :cond_0

    .line 432
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->getViewBufferHeight()I

    move-result v0

    .line 434
    :cond_0
    return v0
.end method

.method protected getImageEditViewWidth()I
    .locals 2

    .prologue
    .line 420
    const/4 v0, 0x0

    .line 421
    .local v0, "ret":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v1, :cond_0

    .line 423
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->getViewBufferWidth()I

    move-result v0

    .line 425
    :cond_0
    return v0
.end method

.method public invalidateViews()V
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->invalidate()V

    .line 383
    :cond_0
    return-void
.end method

.method public invalidateViews(I)V
    .locals 4
    .param p1, "time"    # I

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->postInvalidateDelayed(J)V

    .line 417
    :cond_0
    return-void
.end method

.method public invalidateViews(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "area"    # Landroid/graphics/Rect;

    .prologue
    .line 386
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->invalidate(Landroid/graphics/Rect;)V

    .line 388
    :cond_0
    return-void
.end method

.method public invalidateViewsWithThread()V
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$4;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 399
    :cond_0
    return-void
.end method

.method public invalidateViewsWithThread(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "area"    # Landroid/graphics/Rect;

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$5;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;Landroid/graphics/Rect;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 410
    :cond_0
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    .line 160
    :cond_0
    return-void
.end method

.method public onConfigurationChanged()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 226
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v2, :cond_0

    .line 227
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->destroy()V

    .line 229
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0900af

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 230
    .local v1, "mainViewLayout":Landroid/view/ViewGroup;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03003f

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 232
    .local v0, "mainView":Landroid/view/ViewGroup;
    const v2, 0x7f0900be

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    .line 233
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v2, :cond_1

    .line 235
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$1;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setOnDrawCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnDrawCallback;)V

    .line 247
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$2;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 259
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$3;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setOnLayoutCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$OnLayoutCallback;)V

    .line 267
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setInitViewCallback(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;)V

    .line 269
    if-eqz v0, :cond_1

    .line 271
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 272
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 275
    :cond_1
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x2c000000

    if-eq v2, v3, :cond_2

    .line 276
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x1e110000

    if-ne v2, v3, :cond_3

    .line 278
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->initCollageAnimationView(Landroid/view/ViewGroup;Landroid/view/View;)V

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->clearCollageAnimationView()V

    .line 296
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    if-eqz v2, :cond_4

    .line 298
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->setConfigurationChanged()V

    .line 300
    :cond_4
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->onFrameKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 165
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 176
    const/4 v0, 0x0

    .line 177
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    if-eqz v1, :cond_0

    .line 179
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v1, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 181
    :cond_0
    return v0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 118
    const-string v0, "ViewFrame pause"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 119
    return-void
.end method

.method public postInvalidateViews()V
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->postInvalidate()V

    .line 372
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 3

    .prologue
    .line 94
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x2c000000

    if-eq v1, v2, :cond_0

    .line 95
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x1e110000

    if-ne v1, v2, :cond_1

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    const v2, 0x7f0900bd

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 108
    .local v0, "vGroup":Landroid/view/ViewGroup;
    if-eqz v0, :cond_1

    .line 110
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->initCollageAnimationView(Landroid/view/ViewGroup;Landroid/view/View;)V

    .line 113
    .end local v0    # "vGroup":Landroid/view/ViewGroup;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    if-eqz v1, :cond_2

    .line 114
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->onResume()V

    .line 115
    :cond_2
    return-void
.end method

.method protected runningCollageAnimation()Z
    .locals 2

    .prologue
    .line 341
    const/4 v0, 0x0

    .line 342
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    if-eqz v1, :cond_0

    .line 344
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->runningAnimation()Z

    move-result v0

    .line 346
    :cond_0
    return v0
.end method

.method protected setActivityLayoutThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "viewTransformMatrix"    # Landroid/graphics/Matrix;
    .param p3, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;

    .prologue
    .line 211
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    const v2, 0x7f0900ac

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;

    .line 212
    .local v0, "vGroup":Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;
    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout;->setEnableInterceptForThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;)V

    .line 213
    return-void
.end method

.method protected setEditViewImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 218
    :cond_0
    return-void
.end method

.method protected setEditViewImageMatrix(Landroid/graphics/Matrix;)V
    .locals 1
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 223
    :cond_0
    return-void
.end method

.method protected setImageDataToImageEditView(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;)V
    .locals 1
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .prologue
    .line 438
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->changeImageData(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;)V

    .line 440
    :cond_0
    return-void
.end method

.method protected setImageEditViewPinchZoomCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setPinchZoomCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;)V

    .line 305
    :cond_0
    return-void
.end method

.method protected setInterface(Ljava/lang/Object;)V
    .locals 3
    .param p1, "myInterface"    # Ljava/lang/Object;

    .prologue
    .line 185
    move-object v1, p1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    .line 186
    check-cast p1, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    .end local p1    # "myInterface":Ljava/lang/Object;
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    .line 188
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->initMainView()V

    .line 189
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    const v2, 0x7f0900be

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    .line 190
    .local v0, "iView":Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;
    if-eqz v0, :cond_0

    .line 191
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mInitViewCallback:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setInitViewCallback(Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;)V

    .line 192
    :cond_0
    return-void
.end method

.method public setSubMenuView()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initSubView()V

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->invalidateViewsWithThread()V

    .line 173
    :cond_0
    return-void
.end method

.method public setView()V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 124
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initDialog()V

    .line 144
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initTrayLayout()V

    .line 145
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initEffect()V

    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initActionbar()V

    .line 147
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initButtons()V

    .line 148
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initView()V

    .line 151
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->setViewPosition()V

    .line 152
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->invalidateViews()V

    .line 153
    return-void

    .line 131
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initDialog()V

    .line 133
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initTrayLayout()V

    .line 134
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initActionbar()V

    .line 135
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initEffect()V

    .line 136
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initButtons()V

    .line 137
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mTestInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initView()V

    goto :goto_0

    .line 124
    nop

    :sswitch_data_0
    .sparse-switch
        0x1e110000 -> :sswitch_0
        0x2c000000 -> :sswitch_0
    .end sparse-switch
.end method

.method protected setViewLayerType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v0, :cond_0

    .line 197
    packed-switch p1, :pswitch_data_0

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 202
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0

    .line 197
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected startCollageAnimation(IZLjava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;)V
    .locals 7
    .param p1, "duration"    # I
    .param p2, "alphaAnimation"    # Z
    .param p4, "callback"    # Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;",
            ">;",
            "Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 318
    .local p3, "objectInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;>;"
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 321
    .local v6, "vGroup":Landroid/view/ViewGroup;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->bringChildToFront(Landroid/view/View;)V

    .line 322
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mCollageAnimationView:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->runAnimation(IZLjava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;)V

    .line 324
    .end local v6    # "vGroup":Landroid/view/ViewGroup;
    :cond_0
    return-void
.end method

.method protected startImageEditViewAnimation(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    if-eqz v0, :cond_0

    .line 310
    if-eqz p1, :cond_1

    .line 311
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 315
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method protected startMirrorAnimation(ILandroid/graphics/Bitmap;ILandroid/graphics/RectF;Landroid/graphics/RectF;Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;)V
    .locals 0
    .param p1, "duration"    # I
    .param p2, "mirrorBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "mirrorSide"    # I
    .param p4, "mirrorRoi"    # Landroid/graphics/RectF;
    .param p5, "drawRoiForPreviewImage"    # Landroid/graphics/RectF;
    .param p6, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;

    .prologue
    .line 338
    return-void
.end method
