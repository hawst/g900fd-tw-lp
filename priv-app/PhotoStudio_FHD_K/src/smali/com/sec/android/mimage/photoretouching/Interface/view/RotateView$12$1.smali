.class Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;
.super Ljava/lang/Object;
.source "RotateView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->onShow(Landroid/content/DialogInterface;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

.field private final synthetic val$arg0:Landroid/content/DialogInterface;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->val$arg0:Landroid/content/DialogInterface;

    .line 1332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v9, 0x7f0600e5

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1336
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->val$arg0:Landroid/content/DialogInterface;

    check-cast v5, Landroid/app/Dialog;

    const v6, 0x7f09001f

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;

    .line 1337
    .local v0, "eView":Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v5

    .line 1338
    const-string v6, "input_method"

    .line 1337
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 1339
    .local v2, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    invoke-virtual {v2, v5, v8}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1340
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/InputFileName;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1341
    .local v1, "fileName":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    sget-object v5, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-direct {v3, v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342
    .local v3, "saveFile":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 1344
    if-nez v1, :cond_1

    .line 1345
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 1346
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1348
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 1349
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 1350
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    invoke-static {v5, v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    .line 1378
    :cond_0
    :goto_0
    return-void

    .line 1353
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_2

    .line 1354
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 1355
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1357
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 1358
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 1359
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    invoke-static {v5, v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    goto :goto_0

    .line 1362
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1363
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0600da

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 1364
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1366
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 1367
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 1368
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    invoke-static {v5, v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    goto/16 :goto_0

    .line 1373
    :cond_3
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)V

    .line 1374
    .local v4, "task":Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object v1, v5, v8

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$SaveAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1375
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$12$1;->val$arg0:Landroid/content/DialogInterface;

    invoke-interface {v5}, Landroid/content/DialogInterface;->cancel()V

    goto/16 :goto_0
.end method
