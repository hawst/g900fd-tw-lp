.class public Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;
.super Ljava/lang/Object;
.source "SeekbarThreadManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;,
        Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;
    }
.end annotation


# instance fields
.field private isDoingThread:Z

.field private mApplyEffectThread:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;

.field private mStep:I

.field private myCallback:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->mStep:I

    .line 63
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->mApplyEffectThread:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->isDoingThread:Z

    .line 67
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;

    .line 5
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;Z)V
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->isDoingThread:Z

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;)I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->mStep:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;)Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;

    return-object v0
.end method


# virtual methods
.method public init()V
    .locals 0

    .prologue
    .line 18
    return-void
.end method

.method public setOnCallback(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;

    .line 41
    return-void
.end method

.method public setStep(I)V
    .locals 2
    .param p1, "step"    # I

    .prologue
    const/4 v1, 0x0

    .line 22
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->mStep:I

    .line 24
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->isDoingThread:Z

    if-nez v0, :cond_0

    .line 26
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->mApplyEffectThread:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;

    .line 28
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->mApplyEffectThread:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;

    .line 29
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->mApplyEffectThread:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;->start()V

    .line 31
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method
