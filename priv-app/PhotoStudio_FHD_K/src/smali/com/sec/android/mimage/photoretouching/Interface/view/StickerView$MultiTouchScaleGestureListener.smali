.class public Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$MultiTouchScaleGestureListener;
.super Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;
.source "StickerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MultiTouchScaleGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V
    .locals 0

    .prologue
    .line 1663
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1694
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isZoomMinRatio()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1696
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->setZoom(FF)V

    .line 1702
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1700
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->resetZoom()V

    goto :goto_0
.end method

.method public onMultiTouchDown()Z
    .locals 1

    .prologue
    .line 1668
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->resetHandlerIconPressed()V

    .line 1669
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;->onMultiTouchDown()Z

    move-result v0

    return v0
.end method

.method public onMultiTouchScale(FFF)Z
    .locals 1
    .param p1, "scalefactor"    # F
    .param p2, "focusX"    # F
    .param p3, "focusY"    # F

    .prologue
    .line 1681
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->setZoom(FFF)V

    .line 1682
    const/4 v0, 0x1

    return v0
.end method

.method public onMultiTouchScroll(FF)Z
    .locals 1
    .param p1, "distanceX"    # F
    .param p2, "distanceY"    # F

    .prologue
    .line 1675
    const/4 v0, 0x1

    return v0
.end method

.method public onMultiTouchUp()Z
    .locals 1

    .prologue
    .line 1687
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->endZoom()V

    .line 1688
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1757
    const/4 v0, 0x1

    return v0
.end method
