.class Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$2;
.super Landroid/os/Handler;
.source "FrameView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    .line 580
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 583
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 604
    :goto_0
    return-void

    .line 585
    :pswitch_0
    const-string v0, "MSG_WAITING_UNDO_REDO_THREAD"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 586
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->isDoingState()Z

    move-result v0

    if-nez v0, :cond_1

    .line 587
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 588
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW initButton: mDecorationMenuLayoutManager.getRecentlyList().size()="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 589
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v2, :cond_0

    .line 591
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    const/4 v1, 0x6

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->showTabLayout(IZZ)V
    invoke-static {v0, v1, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;IZZ)V

    .line 592
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    goto :goto_0

    .line 596
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    const/4 v1, 0x5

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->showTabLayout(IZZ)V
    invoke-static {v0, v1, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;IZZ)V

    .line 597
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_RECENTLY:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    goto :goto_0

    .line 599
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 583
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
