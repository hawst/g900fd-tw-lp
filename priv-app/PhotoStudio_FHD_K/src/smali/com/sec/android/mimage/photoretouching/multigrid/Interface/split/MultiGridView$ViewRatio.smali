.class public final enum Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;
.super Ljava/lang/Enum;
.source "MultiGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ViewRatio"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

.field public static final enum PROPORTION_16_TO_9:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

.field public static final enum PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

.field public static final enum PROPORTION_2_TO_3:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

.field public static final enum PROPORTION_3_TO_2:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

.field public static final enum PROPORTION_3_TO_4:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

.field public static final enum PROPORTION_4_TO_3:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

.field public static final enum PROPORTION_9_TO_16:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 963
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    const-string v1, "PROPORTION_1_TO_1"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 964
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    const-string v1, "PROPORTION_2_TO_3"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_2_TO_3:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 965
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    const-string v1, "PROPORTION_3_TO_4"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_3_TO_4:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 966
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    const-string v1, "PROPORTION_9_TO_16"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_9_TO_16:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 967
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    const-string v1, "PROPORTION_16_TO_9"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_16_TO_9:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 968
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    const-string v1, "PROPORTION_4_TO_3"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_4_TO_3:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 969
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    const-string v1, "PROPORTION_3_TO_2"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_3_TO_2:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 962
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    sget-object v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_2_TO_3:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_3_TO_4:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_9_TO_16:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_16_TO_9:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_4_TO_3:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_3_TO_2:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->ENUM$VALUES:[Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 962
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->ENUM$VALUES:[Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
