.class public Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;
.super Ljava/lang/Object;
.source "ActionbarButtonsListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;
    }
.end annotation


# instance fields
.field private isFunction:Z

.field private isLongPressed:Z

.field private mButton:Landroid/view/View;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isLongPress"    # Z
    .param p3, "manager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "touchInterface"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 126
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;

    .line 127
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->isLongPressed:Z

    .line 128
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->isFunction:Z

    .line 129
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    .line 130
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mButton:Landroid/view/View;

    .line 32
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 33
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;

    .line 34
    if-eqz p2, :cond_0

    move-object v0, p1

    .line 36
    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V
    .locals 2
    .param p1, "manager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p2, "touchInterface"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 126
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;

    .line 127
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->isLongPressed:Z

    .line 128
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->isFunction:Z

    .line 129
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    .line 130
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mButton:Landroid/view/View;

    .line 27
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 28
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;

    .line 29
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;Z)V
    .locals 0

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->isLongPressed:Z

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;)Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;)Landroid/view/View;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;Landroid/view/GestureDetector;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 61
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->isDoingState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    :cond_0
    :goto_0
    return v1

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 64
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isDisabledButtonTouchEvent(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_3

    .line 72
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mButton:Landroid/view/View;

    .line 73
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 76
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;

    if-eqz v0, :cond_4

    .line 77
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;->TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 79
    :cond_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 81
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->isLongPressed:Z

    if-nez v0, :cond_0

    .line 83
    invoke-virtual {p1, v1}, Landroid/view/View;->setPressed(Z)V

    .line 84
    invoke-virtual {p1, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ignoreAnotherButtonTouchEvent(Landroid/view/View;)V

    goto :goto_0

    .line 90
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->isLongPressed:Z

    if-nez v0, :cond_0

    .line 92
    invoke-static {p1, p2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isInButton(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 94
    invoke-virtual {p1, v1}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 98
    :cond_5
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 103
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->isFunction:Z

    if-nez v0, :cond_7

    .line 104
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->isFunction:Z

    .line 105
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->isLongPressed:Z

    if-nez v0, :cond_6

    .line 107
    invoke-virtual {p1, v2}, Landroid/view/View;->playSoundEffect(I)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;->TouchFunction(Landroid/view/View;)V

    .line 110
    :cond_6
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    .line 111
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->isFunction:Z

    .line 113
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_8

    .line 114
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->mManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resetAnotherButtonTouchEvent()V

    .line 116
    :cond_8
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->isLongPressed:Z

    goto :goto_0

    .line 119
    :pswitch_3
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener;->isLongPressed:Z

    goto :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
