.class Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2$1;
.super Ljava/lang/Object;
.source "StickerView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->setLongClickSticker(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;

.field private final synthetic val$resId:I


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;

    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2$1;->val$resId:I

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public touchUp(FFII)V
    .locals 17
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "roiWidth"    # I
    .param p4, "roiHeight"    # I

    .prologue
    .line 172
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;)Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    move-result-object v11

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getActionbarHeight()I

    move-result v11

    int-to-float v11, v11

    sub-float p2, p2, v11

    .line 173
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;)Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    move-result-object v11

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v2

    .line 174
    .local v2, "drawRoi":Landroid/graphics/Rect;
    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10}, Landroid/graphics/RectF;-><init>()V

    .line 175
    .local v10, "targetOrgRoi":Landroid/graphics/RectF;
    const/4 v11, 0x0

    iput v11, v10, Landroid/graphics/RectF;->left:F

    .line 176
    const/4 v11, 0x0

    iput v11, v10, Landroid/graphics/RectF;->top:F

    .line 177
    move/from16 v0, p3

    int-to-float v11, v0

    iput v11, v10, Landroid/graphics/RectF;->right:F

    .line 178
    move/from16 v0, p4

    int-to-float v11, v0

    iput v11, v10, Landroid/graphics/RectF;->bottom:F

    .line 180
    move/from16 v0, p1

    float-to-int v11, v0

    move/from16 v0, p2

    float-to-int v12, v0

    invoke-virtual {v2, v11, v12}, Landroid/graphics/Rect;->contains(II)Z

    move-result v11

    if-nez v11, :cond_3

    .line 182
    iget v11, v2, Landroid/graphics/Rect;->left:I

    int-to-float v11, v11

    cmpg-float v11, p1, v11

    if-gez v11, :cond_0

    .line 183
    iget v11, v2, Landroid/graphics/Rect;->left:I

    add-int/lit8 v11, v11, 0x1

    int-to-float v0, v11

    move/from16 p1, v0

    .line 184
    :cond_0
    iget v11, v2, Landroid/graphics/Rect;->right:I

    int-to-float v11, v11

    cmpl-float v11, p1, v11

    if-lez v11, :cond_1

    .line 185
    iget v11, v2, Landroid/graphics/Rect;->right:I

    add-int/lit8 v11, v11, -0x1

    int-to-float v0, v11

    move/from16 p1, v0

    .line 186
    :cond_1
    iget v11, v2, Landroid/graphics/Rect;->top:I

    int-to-float v11, v11

    cmpg-float v11, p2, v11

    if-gez v11, :cond_2

    .line 187
    iget v11, v2, Landroid/graphics/Rect;->top:I

    add-int/lit8 v11, v11, 0x1

    int-to-float v0, v11

    move/from16 p2, v0

    .line 188
    :cond_2
    iget v11, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v11, v11

    cmpl-float v11, p2, v11

    if-lez v11, :cond_3

    .line 189
    iget v11, v2, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v11, v11, -0x1

    int-to-float v0, v11

    move/from16 p2, v0

    .line 192
    :cond_3
    div-int/lit8 v11, p3, 0x2

    int-to-float v11, v11

    sub-float v11, p1, v11

    float-to-int v3, v11

    .line 193
    .local v3, "dx":I
    div-int/lit8 v11, p4, 0x2

    int-to-float v11, v11

    sub-float v11, p2, v11

    float-to-int v4, v11

    .line 194
    .local v4, "dy":I
    int-to-float v11, v3

    int-to-float v12, v4

    invoke-virtual {v10, v11, v12}, Landroid/graphics/RectF;->offset(FF)V

    .line 195
    const/4 v11, 0x2

    new-array v1, v11, [F

    .line 196
    .local v1, "center":[F
    const/4 v11, 0x0

    invoke-virtual {v10}, Landroid/graphics/RectF;->centerX()F

    move-result v12

    aput v12, v1, v11

    .line 197
    const/4 v11, 0x1

    invoke-virtual {v10}, Landroid/graphics/RectF;->centerY()F

    move-result v12

    aput v12, v1, v11

    .line 198
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;)Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    move-result-object v11

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v6

    .line 199
    .local v6, "m":Landroid/graphics/Matrix;
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 200
    .local v5, "im":Landroid/graphics/Matrix;
    invoke-virtual {v6, v5}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 201
    invoke-virtual {v5, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 203
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;)Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    move-result-object v11

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    .line 204
    new-instance v5, Landroid/graphics/Matrix;

    .end local v5    # "im":Landroid/graphics/Matrix;
    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 205
    .restart local v5    # "im":Landroid/graphics/Matrix;
    invoke-virtual {v6, v5}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 206
    invoke-virtual {v5, v10}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 207
    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    move-result v11

    float-to-int v8, v11

    .line 208
    .local v8, "oriRoiWidth":I
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v11

    float-to-int v7, v11

    .line 209
    .local v7, "oriRoiHeight":I
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 210
    .local v9, "roi":Landroid/graphics/Rect;
    const/4 v11, 0x0

    aget v11, v1, v11

    int-to-float v12, v8

    const/high16 v13, 0x3f000000    # 0.5f

    mul-float/2addr v12, v13

    sub-float/2addr v11, v12

    float-to-int v11, v11

    .line 211
    const/4 v12, 0x1

    aget v12, v1, v12

    int-to-float v13, v7

    const/high16 v14, 0x3f000000    # 0.5f

    mul-float/2addr v13, v14

    sub-float/2addr v12, v13

    float-to-int v12, v12

    .line 212
    const/4 v13, 0x0

    aget v13, v1, v13

    int-to-float v14, v8

    const/high16 v15, 0x3f000000    # 0.5f

    mul-float/2addr v14, v15

    add-float/2addr v13, v14

    float-to-int v13, v13

    .line 213
    const/4 v14, 0x1

    aget v14, v1, v14

    int-to-float v15, v7

    const/high16 v16, 0x3f000000    # 0.5f

    mul-float v15, v15, v16

    add-float/2addr v14, v15

    float-to-int v14, v14

    .line 210
    invoke-virtual {v9, v11, v12, v13, v14}, Landroid/graphics/Rect;->set(IIII)V

    .line 214
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;)Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    move-result-object v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2$1;->val$resId:I

    invoke-virtual {v11, v12, v9}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->setStickerMode(ILandroid/graphics/Rect;)V

    .line 215
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;)Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {v11, v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;Z)V

    .line 216
    return-void
.end method
