.class Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$14;
.super Ljava/lang/Object;
.source "MultigridStyleView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->initSeekbarListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$14;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    .line 1220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMyProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 1242
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$14;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$21(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->setSpacingStep(I)V

    .line 1243
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$14;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$23(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->fitRectSizeRatio(Z)V

    .line 1244
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$14;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setSaveable(Z)Z
    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$22(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Z)Z

    .line 1245
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$14;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->invalidateViews()V

    .line 1246
    return-void
.end method

.method public onMyStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1235
    return-void
.end method

.method public onMyStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1228
    return-void
.end method
