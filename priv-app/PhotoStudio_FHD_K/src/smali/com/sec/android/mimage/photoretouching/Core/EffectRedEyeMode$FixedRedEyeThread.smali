.class Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;
.super Ljava/lang/Thread;
.source "EffectRedEyeMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FixedRedEyeThread"
.end annotation


# instance fields
.field private posX:F

.field private posY:F

.field private stop:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

.field private threadContinue:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;FF)V
    .locals 1
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    const/4 v0, 0x0

    .line 327
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    .line 326
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 324
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->threadContinue:Z

    .line 325
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->stop:Z

    .line 328
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->posX:F

    .line 329
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->posY:F

    .line 330
    return-void
.end method


# virtual methods
.method public resetPosition(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 337
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->posX:F

    .line 338
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->posY:F

    .line 339
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->threadContinue:Z

    .line 340
    return-void
.end method

.method public run()V
    .locals 5

    .prologue
    .line 345
    const/4 v1, 0x0

    .line 347
    .local v1, "i":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->posX:F

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->posY:F

    # invokes: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->ableDrawFixedRedEyePosition(FF)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;FF)V

    .line 349
    :cond_0
    :goto_0
    const/16 v2, 0x3e8

    if-lt v1, v2, :cond_3

    .line 366
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # invokes: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->disableDrawFrixdRedEyePosition()V
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$1(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)V

    .line 368
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 369
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    move-result-object v2

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mFixedRedEyeThread:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;

    .line 370
    :cond_2
    return-void

    .line 350
    :cond_3
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->stop:Z

    if-nez v2, :cond_1

    .line 353
    const-wide/16 v2, 0x19

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    :goto_1
    add-int/lit8 v1, v1, 0x32

    .line 359
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->threadContinue:Z

    if-eqz v2, :cond_0

    .line 361
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->threadContinue:Z

    .line 362
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->posX:F

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->posY:F

    # invokes: Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->ableDrawFixedRedEyePosition(FF)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;FF)V

    .line 363
    const/4 v1, 0x0

    goto :goto_0

    .line 354
    :catch_0
    move-exception v0

    .line 356
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method public stopThread()V
    .locals 1

    .prologue
    .line 333
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->stop:Z

    .line 334
    return-void
.end method
