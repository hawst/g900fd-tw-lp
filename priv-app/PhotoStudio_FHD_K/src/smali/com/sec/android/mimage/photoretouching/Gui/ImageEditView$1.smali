.class Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;
.super Ljava/lang/Object;
.source "ImageEditView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

.field private final synthetic val$listener:Landroid/view/View$OnTouchListener;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->val$listener:Landroid/view/View$OnTouchListener;

    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 250
    const/4 v1, 0x0

    .line 253
    .local v1, "ret":Z
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->val$listener:Landroid/view/View$OnTouchListener;

    invoke-interface {v3, p1, p2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    .line 255
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-result-object v3

    if-nez v3, :cond_1

    .line 295
    :cond_0
    :goto_0
    return v1

    .line 260
    :cond_1
    const/16 v3, 0x9

    new-array v2, v3, [F

    .line 261
    .local v2, "values":[F
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 263
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 264
    .local v0, "m":Landroid/graphics/Matrix;
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 267
    .end local v0    # "m":Landroid/graphics/Matrix;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    aget v3, v2, v3

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    const/4 v3, 0x4

    aget v3, v2, v3

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 269
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 270
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;->isLeftMax()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 271
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->plusAlphaLighing()V

    .line 275
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 276
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;->isRightMax()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 277
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->plusAlphaLighing()V

    .line 281
    :cond_4
    :goto_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$5(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 282
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;->isTopMax()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 283
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$5(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->plusAlphaLighing()V

    .line 287
    :cond_5
    :goto_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$6(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 288
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;->isBottomMax()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 289
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$6(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->plusAlphaLighing()V

    goto/16 :goto_0

    .line 273
    :cond_6
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingLeft:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->minusAlphaLighting()V

    goto :goto_1

    .line 279
    :cond_7
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingRight:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->minusAlphaLighting()V

    goto :goto_2

    .line 285
    :cond_8
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingTop:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$5(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->minusAlphaLighting()V

    goto :goto_3

    .line 291
    :cond_9
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mLightingBottom:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$6(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->minusAlphaLighting()V

    goto/16 :goto_0
.end method
