.class Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;
.super Ljava/lang/Thread;
.source "SeekbarThreadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ApplyEffectThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 46
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 47
    const/4 v0, -0x1

    .line 48
    .local v0, "applyingStep":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;Z)V

    .line 50
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->mStep:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 57
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;Z)V

    .line 58
    return-void

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->mStep:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;)I

    move-result v0

    .line 53
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;)Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;->applyEffect(I)V

    .line 54
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$ApplyEffectThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;)Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;->invalidate()V

    goto :goto_0
.end method
