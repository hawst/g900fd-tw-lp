.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;
.super Ljava/lang/Object;
.source "MultigridStyleView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AnimationObjectInfo"
.end annotation


# instance fields
.field private mAlphaAnimation:Z

.field private mAnimationInterpolator:Lcom/sec/android/easing/SineIn33;

.field private mDiffCenterX:F

.field private mDiffCenterY:F

.field private mDiffHeight:F

.field private mDiffWidth:F

.field private mDstX:F

.field private mDstY:F

.field private mObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

.field private mPreviousDrawRect:Landroid/graphics/RectF;

.field private mSrcRect:Landroid/graphics/RectF;

.field private mStX:F

.field private mStY:F

.field private mTargetDrawRect:Landroid/graphics/RectF;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;)V
    .locals 1
    .param p2, "object"    # Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    .prologue
    const/4 v0, 0x0

    .line 3428
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    .line 3427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3416
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    .line 3417
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mTargetDrawRect:Landroid/graphics/RectF;

    .line 3418
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mPreviousDrawRect:Landroid/graphics/RectF;

    .line 3419
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mSrcRect:Landroid/graphics/RectF;

    .line 3424
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mAnimationInterpolator:Lcom/sec/android/easing/SineIn33;

    .line 3425
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mAlphaAnimation:Z

    .line 3429
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    .line 3430
    invoke-virtual {p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->getDefaultSrcRect()Landroid/graphics/RectF;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mSrcRect:Landroid/graphics/RectF;

    .line 3431
    invoke-virtual {p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->getDrawRect()Landroid/graphics/RectF;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mPreviousDrawRect:Landroid/graphics/RectF;

    .line 3432
    new-instance v0, Lcom/sec/android/easing/SineIn33;

    invoke-direct {v0}, Lcom/sec/android/easing/SineIn33;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mAnimationInterpolator:Lcom/sec/android/easing/SineIn33;

    .line 3433
    return-void
.end method


# virtual methods
.method public free()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3444
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->destroy()V

    .line 3445
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mTargetDrawRect:Landroid/graphics/RectF;

    .line 3446
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mSrcRect:Landroid/graphics/RectF;

    .line 3447
    return-void
.end method

.method public getAlphaAnimation()Z
    .locals 1

    .prologue
    .line 3533
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mAlphaAnimation:Z

    return v0
.end method

.method public getDiffCenterX()F
    .locals 1

    .prologue
    .line 3488
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDiffCenterX:F

    return v0
.end method

.method public getDiffCenterY()F
    .locals 1

    .prologue
    .line 3493
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDiffCenterY:F

    return v0
.end method

.method public getDiffHeight()F
    .locals 1

    .prologue
    .line 3503
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDiffHeight:F

    return v0
.end method

.method public getDiffWidth()F
    .locals 1

    .prologue
    .line 3498
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDiffWidth:F

    return v0
.end method

.method public getDstX()F
    .locals 1

    .prologue
    .line 3508
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDstX:F

    return v0
.end method

.method public getDstY()F
    .locals 1

    .prologue
    .line 3513
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDstY:F

    return v0
.end method

.method public getInterpolator()Lcom/sec/android/easing/SineIn33;
    .locals 1

    .prologue
    .line 3468
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mAnimationInterpolator:Lcom/sec/android/easing/SineIn33;

    return-object v0
.end method

.method public getObject()Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;
    .locals 1

    .prologue
    .line 3473
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    return-object v0
.end method

.method public getPreviousDrawRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 3483
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mPreviousDrawRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getSrcRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 3528
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mSrcRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getStX()F
    .locals 1

    .prologue
    .line 3518
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mStX:F

    return v0
.end method

.method public getStY()F
    .locals 1

    .prologue
    .line 3523
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mStY:F

    return v0
.end method

.method public getTargetDrawRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 3478
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mTargetDrawRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method public setAlphaAnimation()V
    .locals 1

    .prologue
    .line 3440
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mAlphaAnimation:Z

    .line 3441
    return-void
.end method

.method public setInterpolatedValues()V
    .locals 2

    .prologue
    .line 3456
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mPreviousDrawRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mStX:F

    .line 3457
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mPreviousDrawRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mStY:F

    .line 3458
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mTargetDrawRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDstX:F

    .line 3459
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mTargetDrawRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDstY:F

    .line 3460
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDstX:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mStX:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDiffCenterX:F

    .line 3461
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDstY:F

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mStY:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDiffCenterY:F

    .line 3462
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mTargetDrawRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mPreviousDrawRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDiffWidth:F

    .line 3463
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mTargetDrawRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mPreviousDrawRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mDiffHeight:F

    .line 3464
    return-void
.end method

.method public setKeepingSamePosition()V
    .locals 1

    .prologue
    .line 3436
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mObject:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->getSrcRect()Landroid/graphics/RectF;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mSrcRect:Landroid/graphics/RectF;

    .line 3437
    return-void
.end method

.method public setTargetDrawRect(Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "drawRect"    # Landroid/graphics/RectF;

    .prologue
    .line 3451
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->mTargetDrawRect:Landroid/graphics/RectF;

    .line 3452
    return-void
.end method
