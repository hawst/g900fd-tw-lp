.class public Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
.super Landroid/widget/LinearLayout;
.source "BottomFrameButtonLayout.java"


# instance fields
.field private mButtonId:I

.field private mButtonType:I

.field private mContext:Landroid/content/Context;

.field private mLayout:Landroid/view/ViewGroup;

.field private mSelected:Z

.field private mTempThumb:[I

.field private mThumbnail:Landroid/graphics/Bitmap;

.field private mThumbnailHeight:I

.field private mThumbnailWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v2, 0x7f030029

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 279
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mSelected:Z

    .line 280
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mButtonId:I

    .line 281
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mButtonType:I

    .line 282
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mContext:Landroid/content/Context;

    .line 283
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    .line 284
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mTempThumb:[I

    .line 285
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnailWidth:I

    .line 286
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnailHeight:I

    .line 287
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 21
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mContext:Landroid/content/Context;

    .line 22
    invoke-static {p1, v2, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 23
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mButtonId:I

    .line 24
    return-void
.end method

.method private applyEffectThumbnail([III)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "in"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 108
    const/4 v0, 0x0

    .line 110
    .local v0, "ret":Landroid/graphics/Bitmap;
    mul-int v3, p2, p3

    new-array v1, v3, [I

    .line 112
    .local v1, "out":[I
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 113
    .local v8, "roi":Landroid/graphics/Rect;
    invoke-virtual {v8, p2, p3, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 115
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mButtonType:I

    packed-switch v3, :pswitch_data_0

    .line 131
    :goto_0
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    move v3, p2

    move v4, v2

    move v5, v2

    move v6, p2

    move v7, p3

    .line 132
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 134
    const/4 p1, 0x0

    .line 135
    const/4 v1, 0x0

    .line 136
    const/4 v8, 0x0

    .line 138
    return-object v0

    .line 128
    :pswitch_0
    mul-int v3, p2, p3

    invoke-static {p1, v2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x3120006e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getFrameResId()I
    .locals 2

    .prologue
    .line 143
    const/4 v0, 0x0

    .line 144
    .local v0, "resId":I
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mButtonType:I

    packed-switch v1, :pswitch_data_0

    .line 237
    :goto_0
    :pswitch_0
    return v0

    .line 147
    :pswitch_1
    const v0, 0x7f0201d3

    .line 148
    goto :goto_0

    .line 150
    :pswitch_2
    const v0, 0x7f0201d3

    .line 151
    goto :goto_0

    .line 153
    :pswitch_3
    const v0, 0x7f0201d4

    .line 154
    goto :goto_0

    .line 156
    :pswitch_4
    const v0, 0x7f0201d5

    .line 157
    goto :goto_0

    .line 159
    :pswitch_5
    const v0, 0x7f0201d6

    .line 160
    goto :goto_0

    .line 162
    :pswitch_6
    const v0, 0x7f0201d7

    .line 163
    goto :goto_0

    .line 165
    :pswitch_7
    const v0, 0x7f0201d8

    .line 166
    goto :goto_0

    .line 168
    :pswitch_8
    const v0, 0x7f0201d9

    .line 169
    goto :goto_0

    .line 171
    :pswitch_9
    const v0, 0x7f0201da

    .line 172
    goto :goto_0

    .line 174
    :pswitch_a
    const v0, 0x7f0201db

    .line 175
    goto :goto_0

    .line 177
    :pswitch_b
    const v0, 0x7f0201dc

    .line 178
    goto :goto_0

    .line 180
    :pswitch_c
    const v0, 0x7f0203a8

    .line 181
    goto :goto_0

    .line 183
    :pswitch_d
    const v0, 0x7f0203a7

    .line 184
    goto :goto_0

    .line 186
    :pswitch_e
    const v0, 0x7f0203a4

    .line 187
    goto :goto_0

    .line 189
    :pswitch_f
    const v0, 0x7f0203a5

    .line 190
    goto :goto_0

    .line 192
    :pswitch_10
    const v0, 0x7f0203a6

    .line 193
    goto :goto_0

    .line 195
    :pswitch_11
    const v0, 0x7f0203a3

    .line 196
    goto :goto_0

    .line 198
    :pswitch_12
    const v0, 0x7f0203a2

    .line 199
    goto :goto_0

    .line 201
    :pswitch_13
    const v0, 0x7f0203b0

    .line 202
    goto :goto_0

    .line 204
    :pswitch_14
    const v0, 0x7f0203b2

    .line 205
    goto :goto_0

    .line 207
    :pswitch_15
    const v0, 0x7f0203b4

    .line 208
    goto :goto_0

    .line 210
    :pswitch_16
    const v0, 0x7f0203b6

    .line 211
    goto :goto_0

    .line 213
    :pswitch_17
    const v0, 0x7f0203b8

    .line 214
    goto :goto_0

    .line 216
    :pswitch_18
    const v0, 0x7f0203ba

    .line 217
    goto :goto_0

    .line 219
    :pswitch_19
    const v0, 0x7f0203bc

    .line 220
    goto :goto_0

    .line 222
    :pswitch_1a
    const v0, 0x7f0203be

    .line 223
    goto :goto_0

    .line 225
    :pswitch_1b
    const v0, 0x7f0203c0

    .line 226
    goto :goto_0

    .line 228
    :pswitch_1c
    const v0, 0x7f0203c2

    .line 229
    goto :goto_0

    .line 231
    :pswitch_1d
    const v0, 0x7f0203c4

    .line 232
    goto :goto_0

    .line 234
    :pswitch_1e
    const v0, 0x7f0203c6

    goto :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x3120006c
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
    .end packed-switch
.end method


# virtual methods
.method public configurationChanged()V
    .locals 2

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->removeAllViews()V

    .line 272
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mButtonId:I

    invoke-static {v0, v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 274
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mSelected:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->setSelected(Z)V

    .line 275
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mSelected:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mSelected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 276
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mButtonType:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->initFrame(I)V

    .line 277
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    const v1, 0x7f090004

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 66
    .local v0, "icon":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 72
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 74
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 77
    :cond_1
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    .line 78
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mTempThumb:[I

    .line 79
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 80
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    .line 81
    return-void
.end method

.method public getLayoutHeight()I
    .locals 4

    .prologue
    .line 92
    const/4 v2, 0x0

    .line 93
    .local v2, "ret":I
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 94
    .local v0, "layout":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 95
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 96
    return v2
.end method

.method public getLayoutWidth()I
    .locals 4

    .prologue
    .line 84
    const/4 v2, 0x0

    .line 85
    .local v2, "ret":I
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 86
    .local v0, "layout":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 87
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v2, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 88
    return v2
.end method

.method public getThumbnail()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getThumbnailBuffer()[I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mTempThumb:[I

    return-object v0
.end method

.method public getThumbnailHeight()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnailHeight:I

    return v0
.end method

.method public getThumbnailWidth()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnailWidth:I

    return v0
.end method

.method public initFrame(I)V
    .locals 8
    .param p1, "buttonType"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mButtonType:I

    .line 48
    const v5, 0x7f090073

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 49
    .local v2, "iconFrameThumb":Landroid/widget/ImageView;
    if-eqz v2, :cond_0

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mTempThumb:[I

    if-eqz v5, :cond_0

    .line 51
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->getFrameResId()I

    move-result v3

    .line 52
    .local v3, "resId":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 53
    .local v0, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 55
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v3    # "resId":I
    :cond_0
    const v5, 0x7f090072

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 56
    .local v1, "iconFrame":Landroid/widget/ImageView;
    if-eqz v1, :cond_1

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mTempThumb:[I

    if-eqz v5, :cond_1

    .line 58
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mTempThumb:[I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnailWidth:I

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnailHeight:I

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->applyEffectThumbnail([III)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 59
    .local v4, "temp":Landroid/graphics/Bitmap;
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 61
    .end local v4    # "temp":Landroid/graphics/Bitmap;
    :cond_1
    return-void
.end method

.method public isFocused()Z
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mLayout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    return v0
.end method

.method public setSelected(Z)V
    .locals 1
    .param p1, "selected"    # Z

    .prologue
    .line 261
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mSelected:Z

    .line 262
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setSelected(Z)V

    .line 263
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 264
    return-void
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "thumbnail"    # Landroid/graphics/Bitmap;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 34
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnail:Landroid/graphics/Bitmap;

    .line 35
    return-void
.end method

.method public setThumbnail([III)V
    .locals 0
    .param p1, "thumbnail"    # [I
    .param p2, "thumbnailWidth"    # I
    .param p3, "thumbnailHeight"    # I

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mTempThumb:[I

    .line 28
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnailWidth:I

    .line 29
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->mThumbnailHeight:I

    .line 30
    return-void
.end method
