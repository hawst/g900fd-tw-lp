.class Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
.super Ljava/lang/Object;
.source "DialogsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DialogInfo"
.end annotation


# instance fields
.field buttonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field callerId:I

.field dialog:Landroid/app/Dialog;

.field dialogType:I

.field hasContentView:Z

.field isChild:Z

.field isDivider:Z

.field isModal:Z

.field negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

.field neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

.field position:Landroid/graphics/Point;

.field positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

.field theme:I

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field title:Ljava/lang/String;

.field titleId:I


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1884
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1890
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->title:Ljava/lang/String;

    .line 1893
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->hasContentView:Z

    .line 1894
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isDivider:Z

    .line 1897
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    .line 1899
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    .line 1900
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    .line 1901
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    .line 1903
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    return-void
.end method
