.class public Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "ClipboardView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$SaveAsyncTask;
    }
.end annotation


# instance fields
.field public final CLIPBOARD_MAX_NUM:I

.field private ClipboardHandler:Landroid/os/Handler;

.field private final ICON_HEIGHT:I

.field private final ICON_WIDTH:I

.field private REDOALL_DIALOG:I

.field private UNDOALL_DIALOG:I

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mBm_delete:Landroid/graphics/Bitmap;

.field private mBm_delete_press:Landroid/graphics/Bitmap;

.field private mBm_lrtb:Landroid/graphics/Bitmap;

.field private mBm_lrtb_press:Landroid/graphics/Bitmap;

.field private mBm_rotate:Landroid/graphics/Bitmap;

.field private mBm_rotate_press:Landroid/graphics/Bitmap;

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mClipEx:Landroid/sec/clipboard/ClipboardExManager;

.field private mClipPaste:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;

.field private mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

.field private mContext:Landroid/content/Context;

.field private mCurrentSaveSize:I

.field private mCurrentStickerCount:I

.field private mCurrentStickerIndex:I

.field mDeletePressed:Z

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

.field private mGreyPaint:Landroid/graphics/Paint;

.field private mHandler_B:Landroid/graphics/Bitmap;

.field private mHandler_L:Landroid/graphics/Bitmap;

.field private mHandler_LB:Landroid/graphics/Bitmap;

.field private mHandler_R:Landroid/graphics/Bitmap;

.field private mHandler_RB:Landroid/graphics/Bitmap;

.field private mHandler_T:Landroid/graphics/Bitmap;

.field private mHandler_delete:Landroid/graphics/Bitmap;

.field private mHandler_rotate:Landroid/graphics/Bitmap;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mPaint:Landroid/graphics/Paint;

.field private mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

.field private mPreviewBitmap:Landroid/graphics/Bitmap;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mRectPaint:Landroid/graphics/Paint;

.field private mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

.field mTouchPressed:Z

.field private mTouchType:I

.field private mTouchedClipBoardPosition:I

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    .line 84
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 800
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->ClipboardHandler:Landroid/os/Handler;

    .line 1610
    const/4 v1, 0x5

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->CLIPBOARD_MAX_NUM:I

    .line 1612
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    .line 1613
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1614
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1615
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1616
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1617
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1618
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1620
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1621
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mPaint:Landroid/graphics/Paint;

    .line 1622
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mRectPaint:Landroid/graphics/Paint;

    .line 1623
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mGreyPaint:Landroid/graphics/Paint;

    .line 1625
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    .line 1626
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    .line 1627
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    .line 1628
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    .line 1629
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete:Landroid/graphics/Bitmap;

    .line 1630
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mPreviewBitmap:Landroid/graphics/Bitmap;

    .line 1632
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate_press:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete_press:Landroid/graphics/Bitmap;

    .line 1633
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 1634
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 1635
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    .line 1636
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    .line 1637
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    .line 1638
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    .line 1639
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 1640
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 1642
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDeletePressed:Z

    .line 1643
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchPressed:Z

    .line 1648
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1650
    const/16 v1, 0x17

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->ICON_WIDTH:I

    .line 1651
    const/16 v1, 0x17

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->ICON_HEIGHT:I

    .line 1654
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->UNDOALL_DIALOG:I

    .line 1655
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->REDOALL_DIALOG:I

    .line 1663
    const v1, 0x7a1200

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentSaveSize:I

    .line 1665
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 1666
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 1667
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 1668
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchedClipBoardPosition:I

    .line 86
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    .line 87
    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 88
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 89
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 90
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 91
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->setInterface(Ljava/lang/Object;)V

    .line 93
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mPaint:Landroid/graphics/Paint;

    .line 94
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 96
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mRectPaint:Landroid/graphics/Paint;

    .line 97
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mRectPaint:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mRectPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 100
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mGreyPaint:Landroid/graphics/Paint;

    .line 101
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mGreyPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mGreyPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mGreyPaint:Landroid/graphics/Paint;

    const/16 v2, 0x4b

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 105
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 106
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 108
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    if-nez v1, :cond_0

    .line 109
    const/4 v1, 0x5

    new-array v1, v1, [Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020374

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 112
    .local v10, "bm_lrtb":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020377

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 113
    .local v11, "bm_lrtb_press":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020375

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 114
    .local v9, "bm_delete":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020378

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 115
    .local v12, "bm_rotate":Landroid/graphics/Bitmap;
    const/high16 v1, 0x41b80000    # 23.0f

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-int v14, v1

    .line 116
    .local v14, "dstWidth":I
    const/high16 v1, 0x41b80000    # 23.0f

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-int v13, v1

    .line 118
    .local v13, "dstHeight":I
    const/4 v1, 0x1

    invoke-static {v10, v14, v13, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    .line 119
    const/4 v1, 0x1

    invoke-static {v9, v14, v13, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete:Landroid/graphics/Bitmap;

    .line 120
    const/4 v1, 0x1

    invoke-static {v12, v14, v13, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate:Landroid/graphics/Bitmap;

    .line 123
    const/4 v1, 0x1

    invoke-static {v11, v14, v13, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    .line 124
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020376

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete_press:Landroid/graphics/Bitmap;

    .line 125
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020379

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate_press:Landroid/graphics/Bitmap;

    .line 127
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 128
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 129
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    .line 130
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    .line 131
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    .line 132
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    .line 133
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 134
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 137
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_1

    .line 138
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mPreviewBitmap:Landroid/graphics/Bitmap;

    .line 139
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mPreviewBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    .line 140
    const/4 v3, 0x0

    .line 141
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 142
    const/4 v5, 0x0

    .line 143
    const/4 v6, 0x0

    .line 144
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    .line 145
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v8

    .line 139
    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 148
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->setViewLayerType(I)V

    .line 149
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 1613
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1612
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 1618
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 1614
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)I
    .locals 1

    .prologue
    .line 1663
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentSaveSize:I

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 1620
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->showClipboardDialog()V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V
    .locals 0

    .prologue
    .line 912
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->doDone()V

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 1616
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;I)V
    .locals 0

    .prologue
    .line 1663
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentSaveSize:I

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)I
    .locals 1

    .prologue
    .line 1625
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;
    .locals 1

    .prologue
    .line 1627
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;I)V
    .locals 0

    .prologue
    .line 1626
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;I)V
    .locals 0

    .prologue
    .line 1625
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 1615
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 773
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->setClipboardBitmap(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->closeClipboardDialog()V

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 1620
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private applyPreview()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 927
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    if-eqz v1, :cond_0

    .line 929
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    .line 930
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 931
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 929
    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 932
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 934
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 937
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 938
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 932
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 939
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    array-length v1, v1

    if-lt v8, v1, :cond_1

    .line 951
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBuffer(Landroid/graphics/Bitmap;)V

    .line 952
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 953
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mPreviewBitmap:Landroid/graphics/Bitmap;

    .line 955
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v8    # "i":I
    :cond_0
    return-void

    .line 941
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v8    # "i":I
    :cond_1
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    if-lt v9, v1, :cond_2

    .line 939
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 943
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v1, v1, v9

    if-eqz v1, :cond_3

    .line 944
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v1, v1, v9

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getZOrder()I

    move-result v1

    if-ne v1, v8, :cond_3

    .line 946
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v1, v1, v9

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->applyPreview(Landroid/graphics/Bitmap;)V

    .line 941
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method private closeClipboardDialog()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipEx:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipEx:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 186
    :cond_0
    return-void
.end method

.method private deleteSticker()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 1183
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    if-eq v2, v5, :cond_0

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    if-lez v2, :cond_0

    .line 1185
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    .line 1187
    .local v0, "curStckIdx":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->destory()V

    .line 1188
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    aput-object v6, v2, v3

    .line 1190
    const/4 v1, 0x0

    .line 1191
    .local v1, "i":I
    move v1, v0

    :goto_0
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_1

    .line 1200
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    .line 1201
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    .line 1203
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    if-lez v2, :cond_3

    .line 1204
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 1210
    .end local v0    # "curStckIdx":I
    .end local v1    # "i":I
    :cond_0
    :goto_1
    return-void

    .line 1193
    .restart local v0    # "curStckIdx":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v1

    if-nez v2, :cond_2

    .line 1195
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    add-int/lit8 v4, v1, 0x1

    aget-object v3, v3, v4

    aput-object v3, v2, v1

    .line 1196
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    add-int/lit8 v3, v1, 0x1

    aput-object v6, v2, v3

    .line 1191
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1207
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    goto :goto_1
.end method

.method private doDone()V
    .locals 7

    .prologue
    .line 914
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->applyPreview()V

    .line 916
    new-instance v6, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;I)V

    .line 917
    .local v6, "clipboardEffect":Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    invoke-direct {v4, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ClipboardEffect;)V

    .line 919
    .local v4, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 920
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 921
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 922
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getEnhance()Z

    move-result v5

    .line 919
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V

    .line 923
    return-void
.end method

.method private drawStickerBdry(Landroid/graphics/Canvas;)V
    .locals 24
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 958
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    if-nez v2, :cond_1

    .line 1180
    :cond_0
    :goto_0
    return-void

    .line 961
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v22

    .line 962
    .local v22, "viewTransform":Landroid/graphics/Matrix;
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    .line 963
    .local v8, "drawRoi":Landroid/graphics/RectF;
    new-instance v20, Landroid/graphics/RectF;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/RectF;-><init>()V

    .line 966
    .local v20, "src":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v21

    .line 967
    .local v21, "supportViewtransMatrix":Landroid/graphics/Matrix;
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 968
    .local v15, "previewRect":Landroid/graphics/RectF;
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 969
    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 972
    const/4 v2, 0x2

    new-array v14, v2, [F

    .line 974
    .local v14, "point":[F
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    array-length v2, v2

    if-lt v11, v2, :cond_a

    .line 1057
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    .line 1059
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    aput v3, v14, v2

    .line 1060
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->y:F

    aput v3, v14, v2

    .line 1061
    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1063
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getDrawBdry()Landroid/graphics/RectF;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1064
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1066
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 1067
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getAngle()I

    move-result v2

    int-to-float v2, v2

    .line 1068
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 1069
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 1067
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1071
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    .line 1072
    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 1073
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    .line 1074
    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    .line 1075
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 1071
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1076
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    .line 1077
    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 1078
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    .line 1079
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    .line 1080
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 1076
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1081
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    .line 1082
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 1083
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    .line 1084
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    .line 1085
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 1081
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1086
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    .line 1087
    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 1088
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    .line 1089
    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    .line 1090
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 1086
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1093
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    .line 1094
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 1095
    iget v3, v8, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 1096
    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 1097
    const/4 v5, 0x0

    .line 1094
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1099
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 1100
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 1101
    iget v3, v8, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 1102
    iget v4, v8, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 1103
    const/4 v5, 0x0

    .line 1100
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1105
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_4

    .line 1106
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 1107
    iget v3, v8, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 1108
    iget v4, v8, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 1109
    const/4 v5, 0x0

    .line 1106
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1111
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_5

    .line 1112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    .line 1113
    iget v3, v8, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 1114
    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iget v5, v8, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 1115
    const/4 v5, 0x0

    .line 1112
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1117
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_6

    .line 1118
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    .line 1119
    iget v3, v8, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 1120
    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iget v5, v8, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 1121
    const/4 v5, 0x0

    .line 1118
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1123
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_7

    .line 1124
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    .line 1125
    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iget v4, v8, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 1126
    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 1127
    const/4 v5, 0x0

    .line 1124
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1129
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_8

    .line 1130
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    .line 1131
    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iget v4, v8, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 1132
    iget v4, v8, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 1133
    const/4 v5, 0x0

    .line 1130
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1135
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_9

    .line 1136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 1137
    iget v3, v8, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 1138
    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 1139
    const/4 v5, 0x0

    .line 1136
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1178
    :cond_9
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0

    .line 976
    :cond_a
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    if-lt v12, v2, :cond_b

    .line 974
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 978
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v12

    if-eqz v2, :cond_c

    .line 979
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getZOrder()I

    move-result v2

    if-ne v2, v11, :cond_c

    .line 981
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "clipboard j:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", i:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 982
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 984
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v3, v3, v12

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    aput v3, v14, v2

    .line 985
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v3, v3, v12

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    aput v3, v14, v2

    .line 986
    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 988
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getDrawBdry()Landroid/graphics/RectF;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 990
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 992
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 993
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getAngle()I

    move-result v2

    int-to-float v2, v2

    .line 994
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 995
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 993
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 997
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v23

    .line 998
    .local v23, "width":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    .line 999
    .local v10, "height":I
    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v0, v2

    move/from16 v17, v0

    .line 1000
    .local v17, "resizeWidth":I
    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v0, v2

    move/from16 v16, v0

    .line 1001
    .local v16, "resizeHeight":I
    move/from16 v0, v17

    int-to-float v2, v0

    move/from16 v0, v23

    int-to-float v3, v0

    div-float v19, v2, v3

    .line 1002
    .local v19, "scaleWidth":F
    move/from16 v0, v16

    int-to-float v2, v0

    int-to-float v3, v10

    div-float v18, v2, v3

    .line 1004
    .local v18, "scaleHeight":F
    new-instance v13, Landroid/graphics/Matrix;

    invoke-direct {v13}, Landroid/graphics/Matrix;-><init>()V

    .line 1005
    .local v13, "matrix":Landroid/graphics/Matrix;
    move/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1006
    iget v2, v8, Landroid/graphics/RectF;->left:F

    iget v3, v8, Landroid/graphics/RectF;->top:F

    invoke-virtual {v13, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1009
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v19, v2

    if-eqz v2, :cond_d

    .line 1012
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mGreyPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1025
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 1026
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getAngle()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    .line 1027
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 1028
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 1026
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1030
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 1031
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getAngle()I

    move-result v2

    int-to-float v2, v2

    .line 1032
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 1033
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 1031
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1034
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v19, v2

    if-eqz v2, :cond_e

    .line 1037
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1047
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 1050
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 976
    .end local v10    # "height":I
    .end local v13    # "matrix":Landroid/graphics/Matrix;
    .end local v16    # "resizeHeight":I
    .end local v17    # "resizeWidth":I
    .end local v18    # "scaleHeight":F
    .end local v19    # "scaleWidth":F
    .end local v23    # "width":I
    :cond_c
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2

    .line 1014
    .restart local v10    # "height":I
    .restart local v13    # "matrix":Landroid/graphics/Matrix;
    .restart local v16    # "resizeHeight":I
    .restart local v17    # "resizeWidth":I
    .restart local v18    # "scaleHeight":F
    .restart local v19    # "scaleWidth":F
    .restart local v23    # "width":I
    :catch_0
    move-exception v9

    .line 1015
    .local v9, "e":Ljava/lang/Exception;
    goto/16 :goto_0

    .line 1020
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v3, v3, v12

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getDrawBdry()Landroid/graphics/RectF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v4, v4, v12

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getDrawBdry()Landroid/graphics/RectF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mGreyPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 1039
    :catch_1
    move-exception v9

    .line 1040
    .restart local v9    # "e":Ljava/lang/Exception;
    goto/16 :goto_0

    .line 1045
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iget v3, v8, Landroid/graphics/RectF;->left:F

    iget v4, v8, Landroid/graphics/RectF;->top:F

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_4
.end method

.method private initSaveOptionDialog()V
    .locals 11

    .prologue
    const v10, 0x7f0601f0

    const v9, 0x7f0601cb

    const/16 v2, 0x9

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 1526
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1580
    :goto_0
    return-void

    .line 1531
    :cond_0
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$13;

    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    .line 1545
    .local v8, "saveListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1547
    const v3, 0x7f0601cd

    .line 1549
    const/4 v5, 0x1

    .line 1551
    const v7, 0x103012e

    .line 1545
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZZLandroid/graphics/Point;I)V

    .line 1553
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1555
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x810

    move v3, v9

    move v5, v9

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1559
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x820

    move v3, v10

    move v5, v10

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1561
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$14;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1568
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$15;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1579
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    goto :goto_0
.end method

.method private initUndoRedoAllDialog()V
    .locals 8

    .prologue
    const v6, 0x103012e

    const/16 v1, 0x1000

    const/16 v7, 0x500

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1342
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1343
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->UNDOALL_DIALOG:I

    .line 1344
    const v3, 0x7f0600a6

    .line 1342
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1349
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1350
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1351
    const v2, 0x7f06009c

    .line 1353
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$7;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    .line 1350
    invoke-virtual {v0, v7, v2, v5, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1360
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060007

    .line 1361
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$8;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    .line 1360
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1368
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060009

    .line 1369
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$9;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    .line 1368
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1377
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1379
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1380
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->REDOALL_DIALOG:I

    .line 1381
    const v3, 0x7f0600a1

    .line 1379
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1386
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1387
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1388
    const v1, 0x7f0601ce

    .line 1390
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$10;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    .line 1387
    invoke-virtual {v0, v7, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1397
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 1398
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$11;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    .line 1397
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1405
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1406
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$12;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    .line 1405
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1414
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1415
    return-void
.end method

.method private resetHandlerIconPressed()V
    .locals 1

    .prologue
    .line 521
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchPressed:Z

    .line 523
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 524
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 525
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 526
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 527
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    .line 528
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    .line 529
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    .line 530
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    .line 531
    return-void
.end method

.method private setClipboardBitmap(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 775
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 776
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 777
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->ClipboardHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 778
    return-void
.end method

.method private setHandlerIconPressed(I)V
    .locals 1
    .param p1, "touchType"    # I

    .prologue
    .line 387
    packed-switch p1, :pswitch_data_0

    .line 519
    :goto_0
    return-void

    .line 389
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchPressed:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDeletePressed:Z

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 393
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 394
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 395
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 396
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    .line 397
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    .line 398
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    .line 399
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 402
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 405
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchPressed:Z

    if-eqz v0, :cond_1

    .line 407
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 409
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 410
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 411
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 412
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    .line 413
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    .line 414
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    .line 415
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 418
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 421
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchPressed:Z

    if-eqz v0, :cond_2

    .line 423
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 425
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 426
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 427
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 428
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    .line 429
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    .line 430
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    .line 431
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 434
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 437
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchPressed:Z

    if-eqz v0, :cond_3

    .line 439
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 441
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 442
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 443
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 444
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    .line 445
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    .line 446
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    .line 447
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 450
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 453
    :pswitch_4
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchPressed:Z

    if-eqz v0, :cond_4

    .line 455
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    .line 457
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 458
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 459
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 460
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 461
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    .line 462
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    .line 463
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 466
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 469
    :pswitch_5
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchPressed:Z

    if-eqz v0, :cond_5

    .line 471
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    .line 473
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 474
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 475
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 476
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 477
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    .line 478
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    .line 479
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 482
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 485
    :pswitch_6
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchPressed:Z

    if-eqz v0, :cond_6

    .line 487
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    .line 489
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 490
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 491
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 492
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 493
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    .line 494
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    .line 495
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 498
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 501
    :pswitch_7
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchPressed:Z

    if-eqz v0, :cond_7

    .line 503
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    .line 505
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 506
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 507
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 508
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 509
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    .line 510
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    .line 511
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 514
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 387
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private showClipboardDialog()V
    .locals 4

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    .line 169
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipPaste:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;

    if-nez v0, :cond_0

    .line 170
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipPaste:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipEx:Landroid/sec/clipboard/ClipboardExManager;

    if-nez v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    const-string v1, "clipboardEx"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/sec/clipboard/ClipboardExManager;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipEx:Landroid/sec/clipboard/ClipboardExManager;

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipEx:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 175
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipEx:Landroid/sec/clipboard/ClipboardExManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipPaste:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;

    invoke-virtual {v0, v1, v2, v3}, Landroid/sec/clipboard/ClipboardExManager;->getData(Landroid/content/Context;ILandroid/sec/clipboard/IClipboardDataPasteEvent;)Z

    .line 177
    :cond_2
    return-void
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v11, -0x1

    const/4 v10, 0x6

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 535
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    invoke-static {p2, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->transformVirtualPreviewCoordinate(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)V

    .line 537
    const/4 v2, 0x1

    .line 538
    .local v2, "ret":Z
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v7

    int-to-float v7, v7

    sub-float v4, v6, v7

    .line 539
    .local v4, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v7

    int-to-float v7, v7

    sub-float v5, v6, v7

    .line 540
    .local v5, "y":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 630
    :goto_0
    return v2

    .line 543
    :pswitch_0
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchPressed:Z

    .line 545
    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    .line 546
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    if-eq v6, v11, :cond_0

    .line 548
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    aget-object v6, v6, v7

    invoke-virtual {v6, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->InitMoveObject(FF)I

    move-result v6

    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    .line 551
    :cond_0
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    if-ge v6, v9, :cond_1

    .line 553
    const/4 v1, 0x0

    .line 554
    .local v1, "isTouchOneRect":Z
    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchedClipBoardPosition:I

    .line 555
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    add-int/lit8 v0, v6, -0x1

    .local v0, "i":I
    :goto_1
    if-gez v0, :cond_3

    .line 563
    if-eqz v1, :cond_1

    .line 565
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    add-int/lit8 v0, v6, -0x1

    :goto_2
    if-gez v0, :cond_5

    .line 583
    .end local v0    # "i":I
    .end local v1    # "isTouchOneRect":Z
    :cond_1
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    if-ne v6, v10, :cond_2

    .line 584
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDeletePressed:Z

    .line 588
    :cond_2
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->setHandlerIconPressed(I)V

    goto :goto_0

    .line 556
    .restart local v0    # "i":I
    .restart local v1    # "isTouchOneRect":Z
    :cond_3
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v6, v6, v0

    invoke-virtual {v6, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->InitMoveObject(FF)I

    move-result v6

    if-lez v6, :cond_4

    .line 558
    const/4 v1, 0x1

    .line 559
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getZOrder()I

    move-result v6

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchedClipBoardPosition:I

    if-le v6, v7, :cond_4

    .line 560
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getZOrder()I

    move-result v6

    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchedClipBoardPosition:I

    .line 555
    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 566
    :cond_5
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getZOrder()I

    move-result v6

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchedClipBoardPosition:I

    if-lt v6, v7, :cond_6

    .line 567
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v6, v6, v0

    invoke-virtual {v6, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->InitMoveObject(FF)I

    move-result v6

    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    .line 568
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "touch type:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 569
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    if-lt v6, v9, :cond_7

    .line 570
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v6, v6, v0

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->setZOrder(I)V

    .line 571
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    .line 565
    :cond_6
    :goto_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 575
    :cond_7
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getZOrder()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ltz v6, :cond_6

    .line 576
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v6, v6, v0

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v7, v7, v0

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->getZOrder()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->setZOrder(I)V

    goto :goto_3

    .line 593
    .end local v0    # "i":I
    .end local v1    # "isTouchOneRect":Z
    :pswitch_1
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchPressed:Z

    .line 595
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    if-eq v6, v11, :cond_8

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    if-eq v6, v10, :cond_8

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    aget-object v6, v6, v7

    if-eqz v6, :cond_8

    .line 596
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    aget-object v6, v6, v7

    invoke-virtual {v6, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->StartMoveObject(FF)V

    .line 599
    :cond_8
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    .line 600
    .local v3, "touchType":I
    iget-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDeletePressed:Z

    if-eqz v6, :cond_9

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    if-ltz v6, :cond_9

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    array-length v7, v7

    if-ge v6, v7, :cond_9

    .line 602
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    aget-object v6, v6, v7

    invoke-virtual {v6, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->InitMoveObject(FF)I

    move-result v3

    .line 603
    if-eq v3, v10, :cond_9

    .line 604
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDeletePressed:Z

    .line 608
    :cond_9
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->setHandlerIconPressed(I)V

    goto/16 :goto_0

    .line 613
    .end local v3    # "touchType":I
    :pswitch_2
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchPressed:Z

    .line 615
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    if-eq v6, v11, :cond_a

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    if-eq v6, v10, :cond_a

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    aget-object v6, v6, v7

    if-eqz v6, :cond_a

    .line 616
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerIndex:I

    aget-object v6, v6, v7

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->EndMoveObject()V

    .line 619
    :cond_a
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    if-ne v6, v10, :cond_b

    iget-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDeletePressed:Z

    if-eqz v6, :cond_b

    .line 621
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->deleteSticker()V

    .line 623
    :cond_b
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDeletePressed:Z

    .line 624
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTouchType:I

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->setHandlerIconPressed(I)V

    goto/16 :goto_0

    .line 540
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public backPressed()V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 229
    return-void
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 745
    return-void
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 760
    const/4 v0, 0x0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 764
    const/4 v0, 0x0

    return v0
.end method

.method public getStatusHeight()I
    .locals 3

    .prologue
    .line 768
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 769
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 770
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 771
    iget v2, v0, Landroid/graphics/Rect;->top:I

    return v2
.end method

.method public initActionbar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 232
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 237
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$4;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    .line 234
    invoke-virtual {v0, v4, v2, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 300
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$5;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    .line 297
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 320
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 323
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    .line 320
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 344
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 347
    return-void
.end method

.method public initButtons()V
    .locals 3

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/high16 v1, 0x17000000

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$3;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 216
    :cond_0
    return-void
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 355
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->initSaveOptionDialog()V

    .line 357
    return-void
.end method

.method public initEffect()V
    .locals 0

    .prologue
    .line 152
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 375
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 732
    return-void
.end method

.method public initTrayLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 735
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 737
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 738
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->setVisibleDeleteButton(I)V

    .line 740
    :cond_0
    return-void
.end method

.method public initView()V
    .locals 4

    .prologue
    .line 156
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    .line 161
    const-wide/16 v2, 0x12c

    .line 156
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 162
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 674
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 675
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledSave()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 678
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentSaveSize:I

    .line 679
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 691
    :goto_0
    return-void

    .line 683
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_1

    .line 685
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 686
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 687
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 689
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 635
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mContext:Landroid/content/Context;

    .line 636
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mPaint:Landroid/graphics/Paint;

    .line 637
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mGreyPaint:Landroid/graphics/Paint;

    .line 639
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 641
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 642
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 643
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeCancelPressState(Z)V

    .line 644
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 645
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->free()V

    .line 647
    :cond_0
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 649
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 650
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 651
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 653
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 654
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_rotate_press:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 655
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mBm_delete_press:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 657
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 658
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 659
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_L:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 660
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_T:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 661
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_R:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 662
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_B:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 663
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_delete:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 664
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 666
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipEx:Landroid/sec/clipboard/ClipboardExManager;

    if-eqz v0, :cond_1

    .line 667
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipEx:Landroid/sec/clipboard/ClipboardExManager;

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 668
    :cond_1
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipEx:Landroid/sec/clipboard/ClipboardExManager;

    .line 669
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mPreviewBitmap:Landroid/graphics/Bitmap;

    .line 670
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mPreviewBitmap:Landroid/graphics/Bitmap;

    .line 365
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 366
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mPaint:Landroid/graphics/Paint;

    .line 363
    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 367
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->drawStickerBdry(Landroid/graphics/Canvas;)V

    .line 369
    :cond_0
    return-void
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 694
    const/16 v0, 0x17

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-ne p1, v0, :cond_3

    .line 696
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 697
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    .line 700
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_2

    .line 701
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 723
    :cond_2
    :goto_0
    return v1

    .line 705
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 707
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->backPressed()V

    goto :goto_0
.end method

.method public onLayout()V
    .locals 5

    .prologue
    .line 1585
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_0

    .line 1586
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 1587
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_1

    .line 1588
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1589
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 1590
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->getImageEditViewWidth()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->getImageEditViewHeight()I

    move-result v1

    if-eqz v1, :cond_2

    .line 1592
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_2

    .line 1594
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->getImageEditViewHeight()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(IILcom/sec/android/mimage/photoretouching/Core/ImageData$setViewSizeMatrixCallback;)V

    .line 1598
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    if-eqz v1, :cond_3

    .line 1600
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mCurrentStickerCount:I

    if-lt v0, v1, :cond_4

    .line 1608
    .end local v0    # "i":I
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->invalidateViews()V

    .line 1609
    return-void

    .line 1602
    .restart local v0    # "i":I
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v1, v1, v0

    if-eqz v1, :cond_5

    .line 1604
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mClipboard:[Lcom/sec/android/mimage/photoretouching/Core/Clipboard;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Clipboard;->configurationChanged()V

    .line 1600
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 757
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 748
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 750
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->resume()V

    .line 752
    :cond_0
    return-void
.end method

.method public refreshView()V
    .locals 0

    .prologue
    .line 1661
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 381
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 382
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 383
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 384
    return-void
.end method
