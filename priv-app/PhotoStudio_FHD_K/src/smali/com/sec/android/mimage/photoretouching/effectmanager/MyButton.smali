.class public Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;
.super Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;
.source "MyButton.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;


# instance fields
.field private mAnimation:Landroid/view/animation/Animation;

.field private mBtnThumnail:Landroid/widget/FrameLayout;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mContext:Landroid/content/Context;

.field private mCurrentPos:Landroid/graphics/Rect;

.field private mDoneBtn:Landroid/view/View;

.field private mImageView:Landroid/widget/ImageView;

.field private mIsFocus:Z

.field private mIsSelected:Z

.field private mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

.field private mMoveToPos:Landroid/graphics/Rect;

.field private mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

.field private mRunningAnimation:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;-><init>(Landroid/content/Context;)V

    .line 246
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mContext:Landroid/content/Context;

    .line 248
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mDoneBtn:Landroid/view/View;

    .line 250
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 251
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 252
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mAnimation:Landroid/view/animation/Animation;

    .line 254
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mRunningAnimation:Z

    .line 256
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;

    .line 257
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;

    .line 259
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mImageView:Landroid/widget/ImageView;

    .line 260
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mBtnThumnail:Landroid/widget/FrameLayout;

    .line 261
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    .line 262
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mIsSelected:Z

    .line 263
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mIsFocus:Z

    .line 30
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mContext:Landroid/content/Context;

    .line 31
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;Z)V
    .locals 0

    .prologue
    .line 262
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mIsSelected:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mBtnThumnail:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Z
    .locals 1

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mIsSelected:Z

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/view/View;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mDoneBtn:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;Z)V
    .locals 0

    .prologue
    .line 254
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mRunningAnimation:Z

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;

    return-object v0
.end method

.method private onkey()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mBtnThumnail:Landroid/widget/FrameLayout;

    const v1, 0x7f0200a9

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mBtnThumnail:Landroid/widget/FrameLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mBtnThumnail:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$1;-><init>(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 107
    return-void
.end method


# virtual methods
.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 40
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 65
    :goto_0
    return-void

    .line 42
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isSelected()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mIsSelected:Z

    .line 43
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setPressed(Z)V

    goto :goto_0

    .line 46
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setPressed(Z)V

    goto :goto_0

    .line 49
    :pswitch_2
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v6

    int-to-float v6, v6

    invoke-direct {v0, v1, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 50
    .local v0, "r":Landroid/graphics/RectF;
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    add-float/2addr v1, v4

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual {v0, v1, v4}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mIsSelected:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setSelected(Z)V

    .line 53
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mIsSelected:Z

    if-eqz v4, :cond_2

    :goto_2
    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 54
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mDoneBtn:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 55
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mDoneBtn:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 57
    :cond_0
    const/4 v0, 0x0

    .line 58
    goto :goto_0

    :cond_1
    move v1, v3

    .line 52
    goto :goto_1

    :cond_2
    move v2, v3

    .line 53
    goto :goto_2

    .line 60
    .end local v0    # "r":Landroid/graphics/RectF;
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setPressed(Z)V

    goto :goto_0

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public configurationChanged()V
    .locals 0

    .prologue
    .line 119
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->configurationChanged()V

    .line 120
    return-void
.end method

.method public copyButtonsNPosition(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V
    .locals 1
    .param p1, "button"    # Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .prologue
    .line 140
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getLeftButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 141
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getRightButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 142
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setCurrentPosition(Landroid/graphics/Rect;)V

    .line 143
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 110
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->destroy()V

    .line 111
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;

    .line 112
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;

    .line 113
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 114
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 115
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    .line 116
    return-void
.end method

.method public getCurrentPosition()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 234
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method public getLeftButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    return-object v0
.end method

.method public getRightButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    return-object v0
.end method

.method public initButton(ILandroid/graphics/Bitmap;I)V
    .locals 1
    .param p1, "buttonId"    # I
    .param p2, "icon"    # Landroid/graphics/Bitmap;
    .param p3, "textId"    # I

    .prologue
    .line 69
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->initButton(ILandroid/graphics/Bitmap;I)V

    .line 71
    const v0, 0x7f09001d

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    .line 72
    const v0, 0x7f0900c1

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mBtnThumnail:Landroid/widget/FrameLayout;

    .line 74
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->onkey()V

    .line 76
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;

    .line 77
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;

    .line 78
    return-void
.end method

.method public isChecked()Z
    .locals 2

    .prologue
    .line 192
    const/4 v0, 0x0

    .line 193
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz v1, :cond_0

    .line 194
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isSelected()Z

    move-result v0

    .line 195
    :cond_0
    return v0
.end method

.method public moveToLeft()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    invoke-interface {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 182
    :cond_0
    return-void
.end method

.method public moveToRight()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    invoke-interface {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 189
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 128
    if-eqz p1, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setLeft(I)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setTop(I)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setRight(I)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setBottom(I)V

    .line 136
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->onLayout(ZIIII)V

    .line 137
    return-void
.end method

.method public reloadLayout()V
    .locals 0

    .prologue
    .line 35
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->init()V

    .line 36
    return-void
.end method

.method public runningAnimation()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mRunningAnimation:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1, "check"    # Z

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setSelected(Z)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 241
    :cond_0
    return-void
.end method

.method public setCurrentPosition(Landroid/graphics/Rect;)V
    .locals 5
    .param p1, "pos"    # Landroid/graphics/Rect;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 156
    return-void
.end method

.method public setDoneBtn(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mDoneBtn:Landroid/view/View;

    .line 244
    return-void
.end method

.method public setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V
    .locals 0
    .param p1, "left"    # Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mLeftButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 171
    return-void
.end method

.method public setMovePosition(Landroid/graphics/Rect;)V
    .locals 5
    .param p1, "pos"    # Landroid/graphics/Rect;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 167
    return-void
.end method

.method public setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V
    .locals 0
    .param p1, "right"    # Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mRightButton:Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 175
    return-void
.end method

.method public setTouchCallback(Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "touchInterface"    # Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123
    .local p2, "buttonList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;>;"
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->setTouchCallback(Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;Ljava/util/ArrayList;)V

    .line 124
    return-void
.end method

.method public startAnimation()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 199
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mRunningAnimation:Z

    .line 200
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    .line 201
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mMoveToPos:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mCurrentPos:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-direct {v0, v4, v1, v4, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 200
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mAnimation:Landroid/view/animation/Animation;

    .line 203
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mAnimation:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton$2;-><init>(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setFillEnabled(Z)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mAnimation:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->mAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->startAnimation(Landroid/view/animation/Animation;)V

    .line 232
    return-void
.end method
