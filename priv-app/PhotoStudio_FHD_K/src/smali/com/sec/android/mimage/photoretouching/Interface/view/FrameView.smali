.class public Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "FrameView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$ResolverSetAsAdapter;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$SaveAsyncTask;
    }
.end annotation


# static fields
.field public static final FRAME_TYPE1:I = 0x2001

.field public static final FRAME_TYPE2:I = 0x2002

.field public static final FRAME_TYPE3:I = 0x2003

.field private static final MSG_WAITING_UNDO_REDO_THREAD:I

.field private static instance:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;


# instance fields
.field public final FRAME_TYPE1_MAX_NUM:I

.field public final FRAME_TYPE2_MAX_NUM:I

.field public final FRAME_TYPE3_MAX_NUM:I

.field private MAX_RECENTLY:I

.field private REDOALL_DIALOG:I

.field private UNDOALL_DIALOG:I

.field private curThumbnailView:Landroid/view/View;

.field private currDepth:Ljava/lang/String;

.field private isDone:Z

.field isFrameApplied:Z

.field isFrameFirstpage:Z

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field mBitmap:Landroid/graphics/Bitmap;

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mColor:I

.field private mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

.field private mContext:Landroid/content/Context;

.field private mCurentType:I

.field private mCurrentEffectType:I

.field private mCurrentLinearLayout:Landroid/widget/LinearLayout;

.field private mCurrentSaveSize:I

.field private mCurrentlayout:Landroid/view/View;

.field private mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mDrawOriginal:Z

.field private mFlipperLinearLayout:Landroid/widget/LinearLayout;

.field mFrameAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

.field public mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

.field private mFrameLayer:Landroid/widget/FrameLayout;

.field mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field mHandler:Landroid/os/Handler;

.field private mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

.field public mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mImageView:Landroid/widget/ImageView;

.field private mInitialPreviewHeight:I

.field private mInitialPreviewWidth:I

.field private mIsMinimum:Z

.field private mIsshowTabLayout:Z

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mOptionItemId:I

.field public mPageImage:[Landroid/widget/ImageView;

.field private mPageImage1:Landroid/widget/LinearLayout;

.field private mPageImage2:Landroid/widget/LinearLayout;

.field private mPageImage3:Landroid/widget/LinearLayout;

.field private mPageImage4:Landroid/widget/LinearLayout;

.field private mPageImage5:Landroid/widget/LinearLayout;

.field public mPageLayout:[Landroid/widget/LinearLayout;

.field mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

.field private mPaint:Landroid/graphics/Paint;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mResId:I

.field private mTime:J

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;

.field private mViewPagerBottomContainer:Landroid/widget/LinearLayout;

.field onFrameCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnFrameCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p6, "decoManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .prologue
    const/4 v6, 0x2

    const/high16 v5, 0x42c80000    # 100.0f

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 98
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 163
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->isFrameApplied:Z

    .line 164
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->onFrameCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnFrameCallback;

    .line 580
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHandler:Landroid/os/Handler;

    .line 2346
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 2347
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    .line 2348
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 2349
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 2350
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 2351
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2352
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 2353
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 2354
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPaint:Landroid/graphics/Paint;

    .line 2355
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    .line 2356
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 2357
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mCurrentEffectType:I

    .line 2358
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->curThumbnailView:Landroid/view/View;

    .line 2360
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mColor:I

    .line 2361
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2362
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 2364
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->UNDOALL_DIALOG:I

    .line 2365
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->REDOALL_DIALOG:I

    .line 2366
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mIsMinimum:Z

    .line 2367
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mIsshowTabLayout:Z

    .line 2368
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 2369
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPageImage:[Landroid/widget/ImageView;

    .line 2370
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPageLayout:[Landroid/widget/LinearLayout;

    .line 2371
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mCurrentlayout:Landroid/view/View;

    .line 2372
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFlipperLinearLayout:Landroid/widget/LinearLayout;

    .line 2373
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPageImage1:Landroid/widget/LinearLayout;

    .line 2374
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPageImage2:Landroid/widget/LinearLayout;

    .line 2375
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPageImage3:Landroid/widget/LinearLayout;

    .line 2376
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPageImage4:Landroid/widget/LinearLayout;

    .line 2377
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPageImage5:Landroid/widget/LinearLayout;

    .line 2379
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    .line 2381
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 2383
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    .line 2384
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    .line 2386
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPagerAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 2387
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    .line 2388
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mCurentType:I

    .line 2392
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->FRAME_TYPE1_MAX_NUM:I

    .line 2393
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->FRAME_TYPE2_MAX_NUM:I

    .line 2394
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->FRAME_TYPE3_MAX_NUM:I

    .line 2395
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageView:Landroid/widget/ImageView;

    .line 2396
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->isFrameFirstpage:Z

    .line 2397
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mOptionItemId:I

    .line 2398
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDrawOriginal:Z

    .line 2399
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 2401
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mCurrentSaveSize:I

    .line 2402
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mTime:J

    .line 2403
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->MAX_RECENTLY:I

    .line 2404
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mBitmap:Landroid/graphics/Bitmap;

    .line 2405
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 2406
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->isDone:Z

    .line 99
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    .line 100
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 101
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 102
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 103
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 104
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 105
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->setInterface(Ljava/lang/Object;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setmTestInterface(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;)V

    .line 107
    sput-object p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->instance:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    .line 108
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->initStampsOrFrames()V

    .line 109
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPaint:Landroid/graphics/Paint;

    .line 110
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mInitialPreviewWidth:I

    .line 114
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mInitialPreviewHeight:I

    .line 115
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 117
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->isDone:Z

    .line 118
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->initTabLayout()V

    .line 121
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    .line 122
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->setOnCallback(Lcom/sec/android/mimage/photoretouching/Core/FrameEffect$OnCallback;)V

    .line 144
    invoke-virtual {p0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->setViewLayerType(I)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v5

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v5

    if-gez v0, :cond_1

    .line 147
    :cond_0
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mIsMinimum:Z

    .line 150
    :goto_0
    return-void

    .line 149
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mIsMinimum:Z

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)J
    .locals 2

    .prologue
    .line 2402
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mTime:J

    return-wide v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 2350
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 2346
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 2362
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 2362
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)I
    .locals 1

    .prologue
    .line 2397
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mOptionItemId:I

    return v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;I)Z
    .locals 1

    .prologue
    .line 1900
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->runOptionItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2361
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 2354
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V
    .locals 0

    .prologue
    .line 2353
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V
    .locals 0

    .prologue
    .line 926
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->doCancel()V

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)I
    .locals 1

    .prologue
    .line 2403
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->MAX_RECENTLY:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;J)V
    .locals 1

    .prologue
    .line 2402
    iput-wide p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mTime:J

    return-void
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;I)I
    .locals 1

    .prologue
    .line 815
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getRscId(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V
    .locals 0

    .prologue
    .line 988
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->doDone()V

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 2351
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;I)V
    .locals 0

    .prologue
    .line 2401
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mCurrentSaveSize:I

    return-void
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;Z)V
    .locals 0

    .prologue
    .line 2398
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDrawOriginal:Z

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .locals 1

    .prologue
    .line 2368
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;IZZ)V
    .locals 0

    .prologue
    .line 1861
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->showTabLayout(IZZ)V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 2346
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 2347
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 2349
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 2348
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)I
    .locals 1

    .prologue
    .line 2401
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mCurrentSaveSize:I

    return v0
.end method

.method private checkHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 194
    const/4 v0, 0x0

    .line 195
    .local v0, "isNoShow":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->isDoNotShowPopup(Landroid/content/Context;I)Z

    move-result v0

    .line 196
    if-nez v0, :cond_0

    .line 198
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-nez v1, :cond_0

    .line 200
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->initHelpPopup(I)V

    .line 201
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->showCurrentModeHelpPopup()V

    .line 204
    :cond_0
    return-void
.end method

.method private doCancel()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 928
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->isFrameApplied:Z

    .line 929
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    if-eqz v3, :cond_0

    .line 931
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->getColorPicker()Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 932
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->Release()V

    .line 934
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 935
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    .line 936
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageView:Landroid/widget/ImageView;

    .line 937
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->isFrameFirstpage:Z

    .line 938
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 940
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 941
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 944
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 947
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 948
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 949
    const/4 v8, 0x1

    .line 950
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 942
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 951
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->set2depth()V

    .line 952
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 953
    return-void
.end method

.method private doDone()V
    .locals 39

    .prologue
    .line 990
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->isFrameApplied:Z

    .line 991
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v7, v7, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    iget-object v7, v7, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v7}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v25

    .line 993
    .local v25, "position":I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v7, v7, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 994
    .local v15, "bmp":Landroid/graphics/Bitmap;
    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v0, v7

    move/from16 v19, v0

    .line 995
    .local v19, "frameWidth":F
    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v0, v7

    move/from16 v18, v0

    .line 996
    .local v18, "frameHeight":F
    const/16 v21, 0x1

    .line 997
    .local v21, "isPreviewSameOriginal":Z
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    const v8, 0x31200078

    if-lt v7, v8, :cond_0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    const v8, 0x3120008a

    if-gt v7, v8, :cond_0

    .line 998
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->shapeFramesize()[I

    move-result-object v34

    .line 999
    .local v34, "size":[I
    const/4 v7, 0x0

    aget v7, v34, v7

    int-to-float v0, v7

    move/from16 v19, v0

    .line 1000
    const/4 v7, 0x1

    aget v7, v34, v7

    int-to-float v0, v7

    move/from16 v18, v0

    .line 1002
    .end local v34    # "size":[I
    :cond_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    int-to-float v0, v7

    move/from16 v28, v0

    .line 1003
    .local v28, "previewWidth":F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    int-to-float v0, v7

    move/from16 v27, v0

    .line 1005
    .local v27, "previewHeight":F
    div-float v7, v19, v18

    div-float v8, v28, v27

    cmpg-float v7, v7, v8

    if-gez v7, :cond_6

    .line 1006
    div-float v7, v19, v18

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mInitialPreviewHeight:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    float-to-int v0, v7

    move/from16 v37, v0

    .line 1007
    .local v37, "width":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mInitialPreviewHeight:I

    move/from16 v20, v0

    .line 1013
    .local v20, "height":I
    :goto_0
    const/16 v31, 0x0

    .line 1014
    .local v31, "ret":Landroid/graphics/Bitmap;
    const/16 v30, 0x0

    .line 1015
    .local v30, "resizedWidth":I
    const/16 v29, 0x0

    .line 1016
    .local v29, "resizedHeight":I
    mul-float v7, v19, v18

    const v8, 0x49f42400    # 2000000.0f

    cmpl-float v7, v7, v8

    if-lez v7, :cond_1

    .line 1017
    const/16 v21, 0x0

    .line 1018
    div-float v36, v19, v18

    .line 1019
    .local v36, "viewRatio":F
    const/16 v38, 0x1

    .line 1020
    .local v38, "y":I
    :goto_1
    move/from16 v0, v38

    int-to-float v7, v0

    mul-float v7, v7, v36

    move/from16 v0, v38

    int-to-float v8, v0

    mul-float/2addr v7, v8

    const v8, 0x49f42400    # 2000000.0f

    cmpg-float v7, v7, v8

    if-ltz v7, :cond_7

    .line 1023
    move/from16 v0, v38

    int-to-float v7, v0

    mul-float v7, v7, v36

    float-to-int v0, v7

    move/from16 v30, v0

    .line 1024
    move/from16 v29, v38

    .line 1030
    .end local v36    # "viewRatio":F
    .end local v38    # "y":I
    :cond_1
    move/from16 v0, v19

    float-to-int v7, v0

    move/from16 v0, v18

    float-to-int v8, v0

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v31

    .line 1032
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v24

    .line 1033
    .local v24, "oriRetScale":F
    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    mul-int/2addr v7, v8

    int-to-float v7, v7

    mul-float v7, v7, v24

    mul-float v7, v7, v24

    float-to-int v7, v7

    const v8, 0x7a1200

    if-le v7, v8, :cond_2

    .line 1034
    const v7, 0x7a1200

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    mul-int/2addr v8, v9

    div-int/2addr v7, v8

    int-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v0, v8

    move/from16 v24, v0

    .line 1036
    :cond_2
    new-instance v17, Landroid/graphics/Canvas;

    move-object/from16 v0, v17

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1038
    .local v17, "can":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    sub-int v7, v7, v37

    int-to-float v7, v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    div-float v22, v7, v8

    .line 1039
    .local v22, "left":F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sub-int v7, v7, v20

    int-to-float v7, v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    div-float v35, v7, v8

    .line 1040
    .local v35, "top":F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int v7, v7, v37

    int-to-float v7, v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    div-float v33, v7, v8

    .line 1041
    .local v33, "right":F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    add-int v7, v7, v20

    int-to-float v7, v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    div-float v16, v7, v8

    .line 1043
    .local v16, "bottom":F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    .line 1044
    new-instance v8, Landroid/graphics/Rect;

    .line 1045
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    sub-int v9, v9, v37

    div-int/lit8 v9, v9, 0x2

    .line 1046
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    sub-int v10, v10, v20

    div-int/lit8 v10, v10, 0x2

    .line 1047
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v12}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    add-int v12, v12, v37

    div-int/lit8 v12, v12, 0x2

    .line 1048
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    add-int v13, v13, v20

    div-int/lit8 v13, v13, 0x2

    .line 1044
    invoke-direct {v8, v9, v10, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1049
    new-instance v9, Landroid/graphics/Rect;

    const/4 v10, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    invoke-direct {v9, v10, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v10, 0x0

    .line 1042
    move-object/from16 v0, v17

    invoke-virtual {v0, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1050
    new-instance v7, Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    invoke-direct {v7, v8, v9, v10, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v8, Landroid/graphics/Rect;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    invoke-direct {v8, v9, v10, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v9, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1053
    if-eqz v31, :cond_3

    .line 1054
    if-nez v21, :cond_8

    .line 1055
    const/4 v7, 0x1

    move-object/from16 v0, v31

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-static {v0, v1, v2, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v26

    .line 1056
    .local v26, "preview":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBuffer(Landroid/graphics/Bitmap;)V

    .line 1062
    .end local v26    # "preview":Landroid/graphics/Bitmap;
    :goto_2
    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float v7, v7, v24

    float-to-int v7, v7

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    mul-float v8, v8, v24

    float-to-int v8, v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v32

    .line 1063
    .local v32, "retOri":Landroid/graphics/Bitmap;
    new-instance v23, Landroid/graphics/Canvas;

    move-object/from16 v0, v23

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1064
    .local v23, "oriCanvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1065
    .local v4, "oriBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v12}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v11

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1068
    new-instance v7, Landroid/graphics/Rect;

    .line 1069
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float v8, v8, v22

    float-to-int v8, v8

    .line 1070
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v9

    int-to-float v9, v9

    mul-float v9, v9, v35

    float-to-int v9, v9

    .line 1071
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v10

    int-to-float v10, v10

    mul-float v10, v10, v33

    float-to-int v10, v10

    .line 1072
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v12}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v12

    int-to-float v12, v12

    mul-float v12, v12, v16

    float-to-int v12, v12

    .line 1068
    invoke-direct {v7, v8, v9, v10, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1073
    new-instance v8, Landroid/graphics/Rect;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v32 .. v32}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    invoke-virtual/range {v32 .. v32}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    invoke-direct {v8, v9, v10, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v9, 0x0

    .line 1066
    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1074
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 1075
    new-instance v7, Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    invoke-direct {v7, v8, v9, v10, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v8, Landroid/graphics/Rect;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v32 .. v32}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    invoke-virtual/range {v32 .. v32}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    invoke-direct {v8, v9, v10, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v9, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v15, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1076
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v0, v32

    invoke-virtual {v7, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer(Landroid/graphics/Bitmap;)V

    .line 1082
    .end local v4    # "oriBitmap":Landroid/graphics/Bitmap;
    .end local v23    # "oriCanvas":Landroid/graphics/Canvas;
    .end local v32    # "retOri":Landroid/graphics/Bitmap;
    :cond_3
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->Release()V

    .line 1083
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    .line 1084
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v6

    .line 1085
    .local v6, "output":[I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1086
    new-instance v5, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v5, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1088
    .local v5, "canvas":Landroid/graphics/Canvas;
    if-eqz v5, :cond_4

    .line 1090
    const/4 v7, 0x0

    .line 1091
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v8

    .line 1092
    const/4 v9, 0x0

    .line 1093
    const/4 v10, 0x0

    .line 1094
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v12}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v11

    .line 1095
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v12}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v12

    .line 1096
    const/4 v13, 0x1

    .line 1097
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPaint:Landroid/graphics/Paint;

    .line 1089
    invoke-virtual/range {v5 .. v14}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1099
    :cond_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->getResId()I

    move-result v7

    move-object/from16 v0, p0

    iput v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    .line 1101
    new-instance v11, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-direct {v11, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;)V

    .line 1103
    .local v11, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v8

    .line 1104
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v9

    .line 1105
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v10

    .line 1106
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v12}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getEnhance()Z

    move-result v12

    .line 1103
    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V

    .line 1107
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v7, :cond_5

    .line 1108
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->loadTextures(Landroid/content/Context;)V

    .line 1111
    :cond_5
    return-void

    .line 1010
    .end local v5    # "canvas":Landroid/graphics/Canvas;
    .end local v6    # "output":[I
    .end local v11    # "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    .end local v16    # "bottom":F
    .end local v17    # "can":Landroid/graphics/Canvas;
    .end local v20    # "height":I
    .end local v22    # "left":F
    .end local v24    # "oriRetScale":F
    .end local v29    # "resizedHeight":I
    .end local v30    # "resizedWidth":I
    .end local v31    # "ret":Landroid/graphics/Bitmap;
    .end local v33    # "right":F
    .end local v35    # "top":F
    .end local v37    # "width":I
    :cond_6
    div-float v7, v18, v19

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mInitialPreviewWidth:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    float-to-int v0, v7

    move/from16 v20, v0

    .line 1011
    .restart local v20    # "height":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mInitialPreviewWidth:I

    move/from16 v37, v0

    .restart local v37    # "width":I
    goto/16 :goto_0

    .line 1021
    .restart local v29    # "resizedHeight":I
    .restart local v30    # "resizedWidth":I
    .restart local v31    # "ret":Landroid/graphics/Bitmap;
    .restart local v36    # "viewRatio":F
    .restart local v38    # "y":I
    :cond_7
    add-int/lit8 v38, v38, 0x1

    goto/16 :goto_1

    .line 1059
    .end local v36    # "viewRatio":F
    .end local v38    # "y":I
    .restart local v16    # "bottom":F
    .restart local v17    # "can":Landroid/graphics/Canvas;
    .restart local v22    # "left":F
    .restart local v24    # "oriRetScale":F
    .restart local v33    # "right":F
    .restart local v35    # "top":F
    :cond_8
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1060
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v9, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBuffer(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2
.end method

.method public static getInstance()Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;
    .locals 1

    .prologue
    .line 2341
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->instance:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    return-object v0
.end method

.method private getRscId(I)I
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 817
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    const v1, 0x3120006e

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    const v1, 0x31200077

    if-gt v0, v1, :cond_0

    .line 819
    packed-switch p1, :pswitch_data_0

    .line 922
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW getRscId: type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 923
    return p1

    .line 822
    :pswitch_0
    const p1, 0x3120006e

    .line 823
    goto :goto_0

    .line 825
    :pswitch_1
    const p1, 0x3120006f

    .line 826
    goto :goto_0

    .line 828
    :pswitch_2
    const p1, 0x31200070

    .line 829
    goto :goto_0

    .line 831
    :pswitch_3
    const p1, 0x31200071

    .line 832
    goto :goto_0

    .line 834
    :pswitch_4
    const p1, 0x31200072

    .line 835
    goto :goto_0

    .line 837
    :pswitch_5
    const p1, 0x31200073

    .line 838
    goto :goto_0

    .line 840
    :pswitch_6
    const p1, 0x31200074

    .line 841
    goto :goto_0

    .line 843
    :pswitch_7
    const p1, 0x31200075

    .line 844
    goto :goto_0

    .line 846
    :pswitch_8
    const p1, 0x31200076

    .line 847
    goto :goto_0

    .line 849
    :pswitch_9
    const p1, 0x31200077

    goto :goto_0

    .line 853
    :cond_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    const v1, 0x31200078

    if-lt v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    const v1, 0x3120007e

    if-gt v0, v1, :cond_1

    .line 855
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 858
    :pswitch_a
    const p1, 0x31200078

    .line 859
    goto :goto_0

    .line 861
    :pswitch_b
    const p1, 0x31200079

    .line 862
    goto :goto_0

    .line 864
    :pswitch_c
    const p1, 0x3120007a

    .line 865
    goto :goto_0

    .line 867
    :pswitch_d
    const p1, 0x3120007b

    .line 868
    goto :goto_0

    .line 870
    :pswitch_e
    const p1, 0x3120007c

    .line 871
    goto :goto_0

    .line 873
    :pswitch_f
    const p1, 0x3120007d

    .line 874
    goto :goto_0

    .line 876
    :pswitch_10
    const p1, 0x3120007e

    goto :goto_0

    .line 882
    :cond_1
    packed-switch p1, :pswitch_data_2

    goto :goto_0

    .line 885
    :pswitch_11
    const p1, 0x3120007f

    .line 886
    goto :goto_0

    .line 888
    :pswitch_12
    const p1, 0x31200080

    .line 889
    goto :goto_0

    .line 891
    :pswitch_13
    const p1, 0x31200081

    .line 892
    goto :goto_0

    .line 894
    :pswitch_14
    const p1, 0x31200082

    .line 895
    goto :goto_0

    .line 897
    :pswitch_15
    const p1, 0x31200083

    .line 898
    goto :goto_0

    .line 900
    :pswitch_16
    const p1, 0x31200084

    .line 901
    goto/16 :goto_0

    .line 903
    :pswitch_17
    const p1, 0x31200085

    .line 904
    goto/16 :goto_0

    .line 906
    :pswitch_18
    const p1, 0x31200086

    .line 907
    goto/16 :goto_0

    .line 909
    :pswitch_19
    const p1, 0x31200087

    .line 910
    goto/16 :goto_0

    .line 912
    :pswitch_1a
    const p1, 0x31200088

    .line 913
    goto/16 :goto_0

    .line 915
    :pswitch_1b
    const p1, 0x31200089

    .line 916
    goto/16 :goto_0

    .line 918
    :pswitch_1c
    const p1, 0x3120008a

    goto/16 :goto_0

    .line 819
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 855
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 882
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method

.method private init2DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 637
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 639
    const-string v0, "2D"

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->currDepth:Ljava/lang/String;

    .line 640
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v4, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 641
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 642
    const/4 v2, 0x1

    .line 644
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$5;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 641
    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 671
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 674
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 671
    invoke-virtual {v0, v1, v4, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 700
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 702
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 703
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 704
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableCancel()V

    .line 705
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 706
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 709
    :cond_0
    return-void
.end method

.method private init3DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 713
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 715
    const-string v0, "3D"

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->currDepth:Ljava/lang/String;

    .line 716
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 717
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 720
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$7;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 717
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 746
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 749
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 746
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 808
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 809
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableCancel()V

    .line 811
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 813
    :cond_0
    return-void
.end method

.method private initGesture()V
    .locals 3

    .prologue
    .line 2270
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$20;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$20;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 2315
    .local v0, "gesture":Landroid/view/GestureDetector$OnGestureListener;
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 2316
    return-void
.end method

.method private initHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 207
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 208
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$4;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->setHelpPopupCallback(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;)V

    .line 214
    return-void
.end method

.method private initSaveOptionDialog()V
    .locals 11

    .prologue
    const v10, 0x7f0601f0

    const v9, 0x7f0601cb

    const/16 v2, 0x9

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 1737
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1790
    :goto_0
    return-void

    .line 1742
    :cond_0
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$15;

    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 1756
    .local v8, "saveListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1758
    const v3, 0x7f0601cd

    .line 1760
    const/4 v5, 0x1

    .line 1762
    const v7, 0x103012e

    .line 1756
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZZLandroid/graphics/Point;I)V

    .line 1764
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1765
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x810

    move v3, v9

    move v5, v9

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1769
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x820

    move v3, v10

    move v5, v10

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1771
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$16;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1778
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$17;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$17;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1789
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    goto :goto_0
.end method

.method private initSetAsDialog()V
    .locals 7

    .prologue
    .line 2257
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 2259
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2260
    const v2, 0x7f090158

    .line 2261
    const v3, 0x7f0600a3

    .line 2262
    const/4 v4, 0x0

    .line 2263
    const/4 v5, 0x0

    .line 2264
    const v6, 0x103012e

    .line 2259
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2265
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2267
    :cond_0
    return-void
.end method

.method private initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
    .locals 19
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isShareVia"    # Z

    .prologue
    .line 2023
    const/16 v16, 0x0

    .line 2024
    .local v16, "shareVialayout":Landroid/widget/LinearLayout;
    const/4 v14, 0x0

    .line 2026
    .local v14, "setAsLayout":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    .line 2028
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    .line 2030
    .local v12, "pm":Landroid/content/pm/PackageManager;
    const/16 v17, 0x0

    .line 2029
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v12, v0, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 2034
    .local v3, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v10, -0x1

    .line 2035
    .local v10, "loc":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    .line 2036
    .local v11, "package_name":Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-lt v7, v0, :cond_2

    .line 2043
    :goto_1
    if-ltz v10, :cond_0

    .line 2044
    invoke-interface {v3, v10}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2047
    :cond_0
    if-eqz p2, :cond_5

    .line 2048
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2049
    .local v9, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    const/4 v15, 0x0

    .line 2050
    .local v15, "shareChooser":Landroid/content/Intent;
    if-eqz p2, :cond_4

    .line 2051
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f06000f

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v15

    .line 2055
    :goto_2
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    new-array v0, v0, [Landroid/content/pm/LabeledIntent;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v9, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/content/pm/LabeledIntent;

    .line 2056
    .local v5, "extraIntents":[Landroid/content/pm/LabeledIntent;
    const-string v17, "android.intent.extra.INITIAL_INTENTS"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2057
    const/high16 v17, 0x24000000

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2058
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2179
    .end local v5    # "extraIntents":[Landroid/content/pm/LabeledIntent;
    .end local v9    # "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    .end local v15    # "shareChooser":Landroid/content/Intent;
    :goto_3
    if-nez p2, :cond_1

    .line 2183
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-object/from16 v17, v0

    const v18, 0x7f090158

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v14}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setDialogView(ILandroid/view/View;)V

    .line 2198
    .end local v3    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v7    # "i":I
    .end local v10    # "loc":I
    .end local v11    # "package_name":Ljava/lang/String;
    .end local v12    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    return-void

    .line 2037
    .restart local v3    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v7    # "i":I
    .restart local v10    # "loc":I
    .restart local v11    # "package_name":Ljava/lang/String;
    .restart local v12    # "pm":Landroid/content/pm/PackageManager;
    :cond_2
    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/ResolveInfo;

    .line 2038
    .local v8, "info":Landroid/content/pm/ResolveInfo;
    iget-object v0, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 2039
    move v10, v7

    .line 2040
    goto :goto_1

    .line 2036
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 2053
    .end local v8    # "info":Landroid/content/pm/ResolveInfo;
    .restart local v9    # "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    .restart local v15    # "shareChooser":Landroid/content/Intent;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0600a3

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v15

    goto :goto_2

    .line 2118
    .end local v9    # "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    .end local v15    # "shareChooser":Landroid/content/Intent;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    check-cast v17, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const-string v18, "layout_inflater"

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 2119
    .local v6, "factory":Landroid/view/LayoutInflater;
    const v17, 0x7f030068

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v6, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    .end local v14    # "setAsLayout":Landroid/widget/LinearLayout;
    check-cast v14, Landroid/widget/LinearLayout;

    .line 2121
    .restart local v14    # "setAsLayout":Landroid/widget/LinearLayout;
    const v17, 0x7f090133

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/GridView;

    .line 2123
    .local v13, "setAsGridView":Landroid/widget/GridView;
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$ResolverSetAsAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v4, v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$ResolverSetAsAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 2124
    .local v4, "adapter":Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$ResolverSetAsAdapter;
    invoke-virtual {v13, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2126
    new-instance v17, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$18;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v13, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$18;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;Landroid/widget/GridView;Landroid/content/Intent;Ljava/util/List;)V

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2152
    new-instance v17, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$19;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$19;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;Ljava/util/List;)V

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    goto/16 :goto_3
.end method

.method private initShareViaDialog()V
    .locals 7

    .prologue
    .line 2245
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 2247
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2248
    const v2, 0x7f090157

    .line 2249
    const v3, 0x7f06000f

    .line 2250
    const/4 v4, 0x0

    .line 2251
    const/4 v5, 0x0

    .line 2252
    const v6, 0x103012e

    .line 2247
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2253
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2255
    :cond_0
    return-void
.end method

.method private initTabLayout()V
    .locals 2

    .prologue
    .line 1855
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->initDataBase(Landroid/content/Context;)V

    .line 1856
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setDrawerHandleVisibility(I)V

    .line 1857
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTab()V

    .line 1858
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->onFrameCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnFrameCallback;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->addFrame(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnFrameCallback;)V

    .line 1859
    return-void
.end method

.method private initUndoRedoAllDialog()V
    .locals 8

    .prologue
    const v6, 0x1030132

    const/16 v1, 0x1000

    const/16 v7, 0x500

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1553
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1554
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->UNDOALL_DIALOG:I

    .line 1555
    const v3, 0x7f0600a6

    .line 1553
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1560
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1561
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1562
    const v2, 0x7f06009c

    .line 1564
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$9;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 1561
    invoke-virtual {v0, v7, v2, v5, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1571
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060007

    .line 1572
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$10;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 1571
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1579
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060009

    .line 1580
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$11;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 1579
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1588
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1590
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1591
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->REDOALL_DIALOG:I

    .line 1592
    const v3, 0x7f0600a1

    .line 1590
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1597
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1598
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1599
    const v1, 0x7f0601ce

    .line 1601
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$12;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 1598
    invoke-virtual {v0, v7, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1608
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 1609
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$13;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 1608
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1616
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1617
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$14;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    .line 1616
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1625
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1626
    return-void
.end method

.method private runOptionItem(I)Z
    .locals 9
    .param p1, "optionItemId"    # I

    .prologue
    const v8, 0x7f090156

    const v7, 0x7a1200

    const/4 v6, 0x7

    const/4 v5, 0x0

    const/16 v4, 0x9

    .line 1902
    const/4 v1, 0x0

    .line 1903
    .local v1, "ret":Z
    const/4 v2, 0x0

    .line 1904
    .local v2, "uri":Landroid/net/Uri;
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mOptionItemId:I

    .line 1905
    sparse-switch p1, :sswitch_data_0

    .line 2018
    :cond_0
    :goto_0
    return v1

    .line 1908
    :sswitch_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1910
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1917
    :goto_1
    const/4 v1, 0x1

    .line 1918
    goto :goto_0

    .line 1915
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->openCamera(Landroid/content/Context;)V

    goto :goto_1

    .line 1920
    :sswitch_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1933
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mOptionItemId:I

    .line 1938
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mCurrentSaveSize:I

    .line 1939
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1940
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1951
    :cond_2
    :goto_2
    const/4 v1, 0x1

    .line 1952
    goto :goto_0

    .line 1942
    :cond_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090157

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1943
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 1944
    if-nez v2, :cond_4

    .line 1946
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1948
    :cond_4
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    goto :goto_2

    .line 1954
    :sswitch_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1956
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1958
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1960
    .local v0, "fileName":Ljava/lang/String;
    if-nez v0, :cond_6

    .line 1961
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    .line 1965
    :goto_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v0, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTextToDialog(Ljava/lang/String;I)V

    .line 1967
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v4, 0x2e000000

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 1968
    const/4 v1, 0x1

    .line 1969
    goto/16 :goto_0

    .line 1963
    .restart local v0    # "fileName":Ljava/lang/String;
    :cond_6
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1972
    .end local v0    # "fileName":Ljava/lang/String;
    :sswitch_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1989
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mCurrentSaveSize:I

    .line 1990
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1991
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2004
    :cond_7
    :goto_4
    const/4 v1, 0x1

    .line 2005
    goto/16 :goto_0

    .line 1993
    :cond_8
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090158

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1996
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 1997
    if-nez v2, :cond_9

    .line 1999
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 2001
    :cond_9
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    .line 2002
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090158

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    goto :goto_4

    .line 2008
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2009
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    .line 2010
    :cond_a
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setVisibilityDirectly(I)V

    .line 2012
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 2013
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v4, 0x17000000

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 1905
    :sswitch_data_0
    .sparse-switch
        0x7f090007 -> :sswitch_2
        0x7f090157 -> :sswitch_1
        0x7f090158 -> :sswitch_3
        0x7f09015a -> :sswitch_0
        0x7f09015b -> :sswitch_4
    .end sparse-switch
.end method

.method private set2depth()V
    .locals 0

    .prologue
    .line 1114
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->init2DepthActionBar()V

    .line 1133
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->invalidateViews()V

    .line 1134
    return-void
.end method

.method private setThumbnailSelection(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1541
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->curThumbnailView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1542
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->curThumbnailView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1544
    :cond_0
    if-eqz p1, :cond_1

    .line 1545
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 1547
    :cond_1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->curThumbnailView:Landroid/view/View;

    .line 1548
    return-void
.end method

.method private showTabLayout(IZZ)V
    .locals 7
    .param p1, "assistantType"    # I
    .param p2, "initByconfig"    # Z
    .param p3, "doOpen"    # Z

    .prologue
    .line 1863
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_0

    .line 1865
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 1866
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getImageEditViewWidth()I

    move-result v2

    .line 1867
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v3

    .line 1868
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v4

    .line 1869
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v5

    move v1, p1

    move v6, p3

    .line 1865
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->showTabView(II[IIIZ)V

    .line 1871
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mIsshowTabLayout:Z

    .line 1873
    :cond_0
    return-void
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1198
    const/4 v0, 0x1

    return v0
.end method

.method public applyImage(IZ)Landroid/graphics/Bitmap;
    .locals 21
    .param p1, "type"    # I
    .param p2, "isPageNum"    # Z

    .prologue
    .line 326
    const/4 v3, 0x0

    .local v3, "w":I
    const/4 v4, 0x0

    .line 327
    .local v4, "h":I
    const/16 v18, 0x0

    .line 329
    .local v18, "frameBitMap":Landroid/graphics/Bitmap;
    const/4 v7, 0x0

    .line 330
    .local v7, "frameToApply":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v2, :cond_0

    .line 331
    const/16 v20, 0x0

    .line 557
    :goto_0
    return-object v20

    .line 332
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    .line 336
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    if-nez v2, :cond_1

    .line 337
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v2, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    .line 339
    :cond_1
    if-eqz p2, :cond_4

    .line 341
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    const v5, 0x3120006e

    if-lt v2, v5, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    const v5, 0x31200077

    if-gt v2, v5, :cond_2

    .line 343
    packed-switch p1, :pswitch_data_0

    .line 452
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    packed-switch v2, :pswitch_data_1

    .line 543
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->ApplyPreview(IIIIIZ)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 546
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v9

    .line 547
    .local v9, "input":[I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v19

    .line 548
    .local v19, "output":[I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 549
    .local v20, "ret":Landroid/graphics/Bitmap;
    new-instance v8, Landroid/graphics/Canvas;

    move-object/from16 v0, v20

    invoke-direct {v8, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 550
    .local v8, "canvas":Landroid/graphics/Canvas;
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v15

    const/16 v16, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v17, v0

    invoke-virtual/range {v8 .. v17}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 551
    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v18

    invoke-virtual {v8, v0, v2, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 555
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 346
    .end local v8    # "canvas":Landroid/graphics/Canvas;
    .end local v9    # "input":[I
    .end local v19    # "output":[I
    .end local v20    # "ret":Landroid/graphics/Bitmap;
    :pswitch_0
    const v2, 0x3120006e

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 349
    :pswitch_1
    const v2, 0x3120006f

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 352
    :pswitch_2
    const v2, 0x31200070

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 355
    :pswitch_3
    const v2, 0x31200071

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 358
    :pswitch_4
    const v2, 0x31200072

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 361
    :pswitch_5
    const v2, 0x31200073

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 364
    :pswitch_6
    const v2, 0x31200074

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 367
    :pswitch_7
    const v2, 0x31200075

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 370
    :pswitch_8
    const v2, 0x31200076

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 373
    :pswitch_9
    const v2, 0x31200077

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 377
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    const v5, 0x31200078

    if-lt v2, v5, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    const v5, 0x3120007e

    if-gt v2, v5, :cond_3

    .line 379
    packed-switch p1, :pswitch_data_2

    goto/16 :goto_1

    .line 382
    :pswitch_a
    const v2, 0x31200078

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 385
    :pswitch_b
    const v2, 0x31200079

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 388
    :pswitch_c
    const v2, 0x3120007a

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 391
    :pswitch_d
    const v2, 0x3120007b

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 394
    :pswitch_e
    const v2, 0x3120007c

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 397
    :pswitch_f
    const v2, 0x3120007d

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 400
    :pswitch_10
    const v2, 0x3120007e

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 406
    :cond_3
    packed-switch p1, :pswitch_data_3

    goto/16 :goto_1

    .line 409
    :pswitch_11
    const v2, 0x3120007f

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 412
    :pswitch_12
    const v2, 0x31200080

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 415
    :pswitch_13
    const v2, 0x31200081

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 418
    :pswitch_14
    const v2, 0x31200082

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 421
    :pswitch_15
    const v2, 0x31200083

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 424
    :pswitch_16
    const v2, 0x31200084

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 427
    :pswitch_17
    const v2, 0x31200085

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 430
    :pswitch_18
    const v2, 0x31200086

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 433
    :pswitch_19
    const v2, 0x31200087

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 436
    :pswitch_1a
    const v2, 0x31200088

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 439
    :pswitch_1b
    const v2, 0x31200089

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 442
    :pswitch_1c
    const v2, 0x3120008a

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 449
    :cond_4
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    goto/16 :goto_1

    .line 455
    :pswitch_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x0

    .line 456
    goto/16 :goto_2

    .line 458
    :pswitch_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x1

    .line 459
    goto/16 :goto_2

    .line 461
    :pswitch_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x2

    .line 462
    goto/16 :goto_2

    .line 464
    :pswitch_20
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x3

    .line 465
    goto/16 :goto_2

    .line 467
    :pswitch_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x4

    .line 468
    goto/16 :goto_2

    .line 470
    :pswitch_22
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x5

    .line 471
    goto/16 :goto_2

    .line 473
    :pswitch_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x6

    .line 474
    goto/16 :goto_2

    .line 476
    :pswitch_24
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x7

    .line 477
    goto/16 :goto_2

    .line 479
    :pswitch_25
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x8

    .line 480
    goto/16 :goto_2

    .line 482
    :pswitch_26
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x9

    .line 483
    goto/16 :goto_2

    .line 485
    :pswitch_27
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0xa

    .line 486
    goto/16 :goto_2

    .line 488
    :pswitch_28
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0xb

    .line 489
    goto/16 :goto_2

    .line 491
    :pswitch_29
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0xc

    .line 492
    goto/16 :goto_2

    .line 494
    :pswitch_2a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0xd

    .line 495
    goto/16 :goto_2

    .line 497
    :pswitch_2b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0xe

    .line 498
    goto/16 :goto_2

    .line 500
    :pswitch_2c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0xf

    .line 501
    goto/16 :goto_2

    .line 503
    :pswitch_2d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x10

    .line 504
    goto/16 :goto_2

    .line 506
    :pswitch_2e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x11

    .line 507
    goto/16 :goto_2

    .line 509
    :pswitch_2f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x12

    .line 510
    goto/16 :goto_2

    .line 512
    :pswitch_30
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x13

    .line 513
    goto/16 :goto_2

    .line 515
    :pswitch_31
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x14

    .line 516
    goto/16 :goto_2

    .line 518
    :pswitch_32
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x15

    .line 519
    goto/16 :goto_2

    .line 521
    :pswitch_33
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x16

    .line 522
    goto/16 :goto_2

    .line 524
    :pswitch_34
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x17

    .line 525
    goto/16 :goto_2

    .line 527
    :pswitch_35
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x18

    .line 528
    goto/16 :goto_2

    .line 530
    :pswitch_36
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x19

    .line 531
    goto/16 :goto_2

    .line 533
    :pswitch_37
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x1a

    .line 534
    goto/16 :goto_2

    .line 536
    :pswitch_38
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x1b

    .line 537
    goto/16 :goto_2

    .line 539
    :pswitch_39
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x1c

    goto/16 :goto_2

    .line 343
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 452
    :pswitch_data_1
    .packed-switch 0x3120006e
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
    .end packed-switch

    .line 379
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 406
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method

.method public backPressed()V
    .locals 2

    .prologue
    .line 612
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledDone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 614
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->doCancel()V

    .line 615
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->show()V

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    if-nez v0, :cond_1

    .line 618
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->openDrawer()V

    .line 628
    :cond_1
    :goto_0
    return-void

    .line 620
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledCancel()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 622
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 623
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    .line 624
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->saveToDB()V

    .line 625
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x31000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 1374
    return-void
.end method

.method public drawerCloseListner()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->currDepth:Ljava/lang/String;

    const-string v1, "3D"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableCancel()V

    .line 159
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 161
    :cond_0
    return-void
.end method

.method public drawerOpenListner()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->currDepth:Ljava/lang/String;

    const-string v1, "2D"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableCancel()V

    .line 155
    :cond_0
    return-void
.end method

.method public getActionBar()Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 1204
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 1388
    const/4 v0, 0x0

    return v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1885
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 1392
    const/4 v0, 0x0

    return v0
.end method

.method public getFrameVectorColor()I
    .locals 1

    .prologue
    .line 1897
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->getFrameVectorColor()I

    move-result v0

    return v0
.end method

.method public getImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 1889
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method public getResId()I
    .locals 1

    .prologue
    .line 1881
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    return v0
.end method

.method public getStatusHeight()I
    .locals 1

    .prologue
    .line 1399
    const/4 v0, 0x0

    return v0
.end method

.method public initActionbar()V
    .locals 0

    .prologue
    .line 632
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->init2DepthActionBar()V

    .line 635
    return-void
.end method

.method public initButtons()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 563
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->isDoingState()Z

    move-result v0

    if-nez v0, :cond_1

    .line 564
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW initButton: mDecorationMenuLayoutManager.getRecentlyList().size()="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 566
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v2, :cond_0

    .line 568
    const/4 v0, 0x6

    invoke-direct {p0, v0, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->showTabLayout(IZZ)V

    .line 569
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_TYPE1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    .line 578
    :goto_0
    return-void

    .line 573
    :cond_0
    const/4 v0, 0x5

    invoke-direct {p0, v0, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->showTabLayout(IZZ)V

    .line 574
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_FRAME_RECENTLY:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    goto :goto_0

    .line 576
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 1140
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 1141
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->initUndoRedoAllDialog()V

    .line 1143
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->initSaveOptionDialog()V

    .line 1144
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->initShareViaDialog()V

    .line 1145
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->initSetAsDialog()V

    .line 1146
    return-void
.end method

.method public initEffect()V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 1158
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 1361
    return-void
.end method

.method public initTrayLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1365
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 1367
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 1369
    :cond_0
    return-void
.end method

.method public initView()V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const v1, 0x7f0900b0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    .line 188
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 189
    const/high16 v0, 0x31200000

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->checkHelpPopup(I)V

    .line 190
    return-void
.end method

.method public isContainedViewPagerBottomArea(FF)Z
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 218
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 219
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewPagerBottomContainer:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 220
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewPagerBottomContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 222
    :cond_0
    float-to-int v1, p1

    float-to-int v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    return v1
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 1264
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1265
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1267
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1279
    :goto_0
    return-void

    .line 1271
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_1

    .line 1273
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 1274
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 1275
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1277
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1212
    const-string v2, "JW frameView onDestroy"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1213
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    .line 1214
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1215
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPaint:Landroid/graphics/Paint;

    .line 1216
    sput-object v3, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->instance:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    .line 1217
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 1218
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1220
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getStampsOrFrames()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_5

    .line 1227
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->destroyStampsOrFrames()V

    .line 1229
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeCancelPressState(Z)V

    .line 1231
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1232
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1233
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v2, :cond_0

    .line 1234
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->free()V

    .line 1235
    :cond_0
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1236
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->destroy()V

    .line 1240
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->Destroy()V

    .line 1241
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    .line 1243
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameAdapter:Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter;

    .line 1244
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    if-eqz v2, :cond_1

    .line 1245
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->removeAllViews()V

    .line 1246
    :cond_1
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    .line 1247
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_2

    .line 1248
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1249
    :cond_2
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    .line 1252
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 1253
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1255
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-eqz v2, :cond_4

    .line 1256
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->destroy()V

    .line 1257
    :cond_4
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    .line 1259
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mBitmap:Landroid/graphics/Bitmap;

    .line 1260
    return-void

    .line 1222
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getStampsOrFrames()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 1223
    .local v1, "mArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1220
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1150
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->isFrameFirstpage:Z

    if-nez v0, :cond_0

    .line 1151
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPaint:Landroid/graphics/Paint;

    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 1152
    :cond_0
    return-void
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1283
    const/16 v3, 0x17

    if-eq p1, v3, :cond_0

    const/16 v3, 0x42

    if-ne p1, v3, :cond_4

    .line 1285
    :cond_0
    const/4 v0, 0x0

    .line 1286
    .local v0, "isFocused":Z
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v2, :cond_1

    .line 1287
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    move-result v0

    .line 1290
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getGridView()Landroid/widget/GridView;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getGridView()Landroid/widget/GridView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/GridView;->getFocusedChild()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1291
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getGridView()Landroid/widget/GridView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/GridView;->getFocusedChild()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->performClick()Z

    .line 1294
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v2, :cond_3

    .line 1295
    if-nez v0, :cond_3

    .line 1296
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 1353
    .end local v0    # "isFocused":Z
    :cond_3
    :goto_0
    return v1

    .line 1300
    :cond_4
    const/4 v3, 0x4

    if-ne p1, v3, :cond_5

    .line 1302
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->backPressed()V

    goto :goto_0

    .line 1307
    :cond_5
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1320
    :pswitch_0
    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->isFrameApplied:Z

    if-eqz v3, :cond_3

    .line 1321
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->isFocusViewPager()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1322
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1323
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->focusCancelAndDone(I)V

    goto :goto_0

    .line 1311
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isCancelFocused()Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isDoneFocused()Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_6
    move v1, v2

    .line 1312
    goto :goto_0

    .line 1314
    :cond_7
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->isFocusViewPager()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1315
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->focusViewPager()V

    goto :goto_0

    :cond_8
    move v1, v2

    .line 1325
    goto :goto_0

    .line 1328
    :cond_9
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isCancelFocused()Z

    move-result v3

    if-nez v3, :cond_b

    :cond_a
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isDoneFocused()Z

    move-result v3

    if-eqz v3, :cond_c

    :cond_b
    move v1, v2

    .line 1329
    goto :goto_0

    .line 1333
    :cond_c
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->focusViewPager()V

    goto/16 :goto_0

    .line 1339
    :pswitch_2
    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->isFrameApplied:Z

    if-eqz v3, :cond_3

    .line 1340
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isCancelFocused()Z

    move-result v3

    if-nez v3, :cond_d

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isDoneFocused()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1341
    :cond_d
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->focusViewPager()V

    goto/16 :goto_0

    .line 1342
    :cond_e
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->isFocusViewPager()Z

    move-result v3

    if-eqz v3, :cond_f

    move v1, v2

    .line 1343
    goto/16 :goto_0

    .line 1347
    :cond_f
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->focusCancelAndDone(I)V

    goto/16 :goto_0

    .line 1307
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onLayout()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1833
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 1834
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 1835
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1842
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 1843
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1844
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1846
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1849
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1850
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 1844
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1851
    return-void
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 1383
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->runOptionItem(I)Z

    .line 1384
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 1379
    return-void
.end method

.method public refreshView()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2321
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getImageEditViewWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getImageEditViewHeight()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 2323
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2325
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 2326
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2329
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 2332
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 2333
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 2334
    const/4 v8, 0x1

    .line 2335
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 2327
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 2338
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->invalidateViews()V

    .line 2339
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 3

    .prologue
    .line 1163
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 1164
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 1165
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 1166
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 1167
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    if-eqz v0, :cond_0

    .line 1168
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->configurationChanged()V

    .line 1172
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_2

    .line 1174
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1175
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const v1, 0x7f0900b0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    .line 1176
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    if-eqz v0, :cond_2

    .line 1177
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->onConfigurationChanged()V

    .line 1178
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 1179
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1180
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1185
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_3

    .line 1186
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 1187
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->configurationChanged()V

    .line 1188
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setDrawerHandleVisibility(I)V

    .line 1189
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getCurrnetType()I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->showTabLayout(IZZ)V

    .line 1190
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getCurrnetType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    .line 1191
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->invalidateViews()V

    .line 1192
    return-void
.end method

.method public setFrameMode(ILandroid/graphics/Rect;)V
    .locals 5
    .param p1, "resId"    # I
    .param p2, "targetOrgRoi"    # Landroid/graphics/Rect;

    .prologue
    const/4 v4, 0x1

    .line 1794
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mCurrentEffectType:I

    .line 1795
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->setResId(I)V

    .line 1796
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->setResId(I)V

    .line 1797
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    .line 1798
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageView:Landroid/widget/ImageView;

    .line 1799
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->isFrameFirstpage:Z

    .line 1800
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1801
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->getCurrentPage(I)I

    move-result v1

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->getCroppedImage(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mBitmap:Landroid/graphics/Bitmap;

    .line 1802
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1822
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1823
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameLayer:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameMenu:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1825
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->init3DepthActionBar()V

    .line 1826
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->invalidateViews()V

    .line 1827
    return-void
.end method

.method public setFrameVectorColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 1893
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->setFrameVectorColor(I)V

    .line 1894
    return-void
.end method

.method public setResId(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 1877
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mResId:I

    .line 1878
    return-void
.end method
