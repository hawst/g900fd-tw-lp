.class Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$1;
.super Ljava/lang/Object;
.source "DialogContextLayout.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setSeekBar(IILcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;

.field private final synthetic val$listener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

.field private final synthetic val$textView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$1;->val$listener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$1;->val$textView:Landroid/widget/TextView;

    .line 276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$1;->val$listener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$1;->val$textView:Landroid/widget/TextView;

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;->onMyProgressChanged(Landroid/widget/SeekBar;IZLandroid/widget/TextView;)V

    .line 295
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$1;->val$listener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;->onMyStartTrackingTouch(Landroid/widget/SeekBar;)V

    .line 288
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$1;->val$listener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;->onMyStopTrackingTouch(Landroid/widget/SeekBar;)V

    .line 282
    return-void
.end method
