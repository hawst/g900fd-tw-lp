.class public Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;
.super Landroid/widget/LinearLayout;
.source "DialogContextLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;
    }
.end annotation


# instance fields
.field private isInit:Z

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 413
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->mContext:Landroid/content/Context;

    .line 414
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    .line 47
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->mContext:Landroid/content/Context;

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 413
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->mContext:Landroid/content/Context;

    .line 414
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    .line 51
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->mContext:Landroid/content/Context;

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 413
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->mContext:Landroid/content/Context;

    .line 414
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    .line 55
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->mContext:Landroid/content/Context;

    .line 56
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->removeAllViewsInLayout()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->mContext:Landroid/content/Context;

    .line 61
    return-void
.end method

.method public init(I)V
    .locals 1
    .param p1, "layoutId"    # I

    .prologue
    .line 389
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setGravity(I)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 391
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    .line 392
    return-void
.end method

.method public isCheckBoxFocused()Z
    .locals 4

    .prologue
    .line 84
    const/4 v1, 0x0

    .line 85
    .local v1, "ret":Z
    invoke-super {p0}, Landroid/widget/LinearLayout;->isFocused()Z

    .line 87
    const v2, 0x7f09001d

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 88
    .local v0, "checkBox":Landroid/widget/CheckBox;
    if-eqz v0, :cond_0

    .line 90
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, DCL - isCheckBoxFocused() - checkBox.isFocused() : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isFocused()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 91
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isFocused()Z

    move-result v1

    .line 93
    :cond_0
    return v1
.end method

.method public isFocused()Z
    .locals 3

    .prologue
    .line 64
    const/4 v1, 0x0

    .line 65
    .local v1, "ret":Z
    invoke-super {p0}, Landroid/widget/LinearLayout;->isFocused()Z

    .line 66
    const v2, 0x7f090019

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 67
    .local v0, "layout":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v1

    .line 69
    :cond_0
    return v1
.end method

.method public isLastBtnFocused()Z
    .locals 3

    .prologue
    .line 97
    const/4 v1, 0x0

    .line 98
    .local v1, "ret":Z
    invoke-super {p0}, Landroid/widget/LinearLayout;->isFocused()Z

    .line 99
    const v2, 0x7f09001a

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 100
    .local v0, "lastLayout":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v1

    .line 102
    :cond_0
    return v1
.end method

.method public isRadioBtnFocused()Z
    .locals 3

    .prologue
    .line 73
    const/4 v1, 0x0

    .line 74
    .local v1, "ret":Z
    invoke-super {p0}, Landroid/widget/LinearLayout;->isFocused()Z

    .line 75
    const v2, 0x7f09001e

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 76
    .local v0, "radioBtn":Landroid/widget/RadioButton;
    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isFocused()Z

    move-result v1

    .line 80
    :cond_0
    return v1
.end method

.method public removeDivider()V
    .locals 2

    .prologue
    .line 395
    const/4 v0, 0x0

    .line 396
    .local v0, "divider":Landroid/view/View;
    const v1, 0x7f090018

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 398
    if-eqz v0, :cond_0

    .line 400
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 402
    :cond_0
    const-string v1, "dialog removeDivider"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 403
    return-void
.end method

.method public setBrushSeekBar(Landroid/app/Dialog;IILcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;)V
    .locals 21
    .param p1, "d"    # Landroid/app/Dialog;
    .param p2, "startPos"    # I
    .param p3, "max"    # I
    .param p4, "listener"    # Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    .prologue
    .line 303
    const-string v2, "ysjeong, setBrushSeekBar, DialogContextLayout"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 305
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v2, :cond_1

    .line 307
    const v2, 0x7f090020

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/LinearLayout;

    .line 308
    .local v16, "seekbarLayout":Landroid/widget/LinearLayout;
    invoke-virtual/range {v16 .. v16}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout$LayoutParams;

    .line 309
    .local v17, "seekbarLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v2, 0x41400000    # 12.0f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, v17

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 310
    invoke-virtual/range {v16 .. v17}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 312
    const v2, 0x7f090024

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/SeekBar;

    .line 313
    .local v15, "seekBar":Landroid/widget/SeekBar;
    const v2, 0x7f090005

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 314
    .local v6, "textView":Landroid/widget/TextView;
    const v2, 0x7f090025

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup;

    .line 315
    .local v14, "rIconLayout":Landroid/view/ViewGroup;
    const/16 v18, 0x0

    .line 316
    .local v18, "temp2":Landroid/widget/LinearLayout;
    if-eqz v14, :cond_0

    .line 318
    const/4 v2, 0x0

    invoke-virtual {v14, v2}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 319
    const v2, 0x7f090027

    invoke-virtual {v14, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v18

    .end local v18    # "temp2":Landroid/widget/LinearLayout;
    check-cast v18, Landroid/widget/LinearLayout;

    .line 320
    .restart local v18    # "temp2":Landroid/widget/LinearLayout;
    const/16 v2, 0xf8

    const/16 v3, 0xf8

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 321
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    new-instance v9, Landroid/graphics/Canvas;

    invoke-direct {v9, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 322
    .local v9, "c":Landroid/graphics/Canvas;
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    .line 323
    .local v13, "p":Landroid/graphics/Paint;
    const/4 v2, 0x1

    invoke-virtual {v13, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 324
    const/4 v2, 0x1

    invoke-virtual {v13, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 325
    const/4 v2, -0x1

    invoke-virtual {v13, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 326
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x4

    int-to-float v4, v4

    invoke-virtual {v9, v2, v3, v4, v13}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 328
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v8}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 331
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    .end local v9    # "c":Landroid/graphics/Canvas;
    .end local v13    # "p":Landroid/graphics/Paint;
    :cond_0
    move-object/from16 v7, v18

    .line 332
    .local v7, "rIcon":Landroid/widget/LinearLayout;
    if-eqz v15, :cond_1

    .line 334
    invoke-virtual {v15}, Landroid/widget/SeekBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout$LayoutParams;

    .line 335
    .local v10, "lp":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v2, 0x43a20000    # 324.0f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v10, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 336
    invoke-virtual {v15, v10}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 337
    move/from16 v0, p3

    invoke-virtual {v15, v0}, Landroid/widget/SeekBar;->setMax(I)V

    .line 339
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;

    move-object/from16 v3, p0

    move-object/from16 v4, p4

    move/from16 v5, p3

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$2;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;ILandroid/widget/TextView;Landroid/widget/LinearLayout;)V

    invoke-virtual {v15, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 373
    move-object/from16 v0, p4

    move/from16 v1, p3

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;->getStartPosition(I)I

    move-result v2

    invoke-virtual {v15, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 375
    const v2, 0x7f0900a9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/view/ViewGroup;

    .line 376
    .local v19, "vGroup":Landroid/view/ViewGroup;
    invoke-virtual/range {v19 .. v19}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/FrameLayout$LayoutParams;

    .line 377
    .local v11, "lp2":Landroid/widget/FrameLayout$LayoutParams;
    const/high16 v2, 0x42dc0000    # 110.0f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v11, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 378
    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 380
    const v2, 0x7f0900ab

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .line 381
    .local v20, "view":Landroid/view/View;
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/widget/FrameLayout$LayoutParams;

    .line 382
    .local v12, "lp3":Landroid/widget/FrameLayout$LayoutParams;
    const/high16 v2, 0x40a00000    # 5.0f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v12, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 383
    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 386
    .end local v6    # "textView":Landroid/widget/TextView;
    .end local v7    # "rIcon":Landroid/widget/LinearLayout;
    .end local v10    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v11    # "lp2":Landroid/widget/FrameLayout$LayoutParams;
    .end local v12    # "lp3":Landroid/widget/FrameLayout$LayoutParams;
    .end local v14    # "rIconLayout":Landroid/view/ViewGroup;
    .end local v15    # "seekBar":Landroid/widget/SeekBar;
    .end local v16    # "seekbarLayout":Landroid/widget/LinearLayout;
    .end local v17    # "seekbarLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v18    # "temp2":Landroid/widget/LinearLayout;
    .end local v19    # "vGroup":Landroid/view/ViewGroup;
    .end local v20    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public setCheckBoxTypeFace(Landroid/graphics/Typeface;)V
    .locals 2
    .param p1, "tf"    # Landroid/graphics/Typeface;

    .prologue
    .line 201
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v1, :cond_0

    .line 203
    const v1, 0x7f09001d

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 204
    .local v0, "checkBox":Landroid/widget/CheckBox;
    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setTypeface(Landroid/graphics/Typeface;)V

    .line 207
    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    :cond_0
    return-void
.end method

.method public setCheckButtonListener(Landroid/view/View$OnTouchListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/view/View$OnTouchListener;

    .prologue
    .line 248
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v1, :cond_0

    .line 250
    const v1, 0x7f09001d

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 251
    .local v0, "cButton":Landroid/widget/CheckBox;
    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 254
    .end local v0    # "cButton":Landroid/widget/CheckBox;
    :cond_0
    return-void
.end method

.method public setDividerBackground(Z)V
    .locals 2
    .param p1, "isLightTheme"    # Z

    .prologue
    .line 405
    const/4 v0, 0x0

    .line 406
    .local v0, "divider":Landroid/view/View;
    const v1, 0x7f090018

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 407
    if-nez v0, :cond_1

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    if-eqz p1, :cond_0

    .line 410
    const v1, 0x7f020589

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setEditText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 144
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v1, :cond_0

    .line 146
    const v1, 0x7f09001f

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 147
    .local v0, "eText":Landroid/widget/EditText;
    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 150
    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 151
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 154
    .end local v0    # "eText":Landroid/widget/EditText;
    :cond_0
    return-void
.end method

.method public setEditTextColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 170
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v1, :cond_0

    .line 172
    const v1, 0x7f09001f

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 173
    .local v0, "textView":Landroid/widget/EditText;
    if-eqz v0, :cond_0

    .line 174
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHorizontalFadingEdgeEnabled(Z)V

    .line 175
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 176
    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 179
    .end local v0    # "textView":Landroid/widget/EditText;
    :cond_0
    return-void
.end method

.method public setIcon(I)V
    .locals 3
    .param p1, "iconId"    # I

    .prologue
    const v2, 0x7f090004

    .line 106
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v1, :cond_0

    .line 108
    if-nez p1, :cond_1

    .line 110
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 111
    .local v0, "icon":Landroid/widget/ImageView;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 120
    .end local v0    # "icon":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 116
    .restart local v0    # "icon":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 124
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v1, :cond_0

    .line 126
    const v1, 0x7f090004

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 127
    .local v0, "icon":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 130
    .end local v0    # "icon":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method

.method public setLeftButton(IILandroid/view/View$OnClickListener;)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "iconId"    # I
    .param p3, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 210
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v2, :cond_1

    .line 212
    const v2, 0x7f090021

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 213
    .local v1, "iconLayout":Landroid/view/ViewGroup;
    if-eqz v1, :cond_1

    .line 215
    if-nez p2, :cond_2

    .line 216
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 223
    :cond_0
    :goto_0
    invoke-virtual {v1, p3}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    .end local v1    # "iconLayout":Landroid/view/ViewGroup;
    :cond_1
    return-void

    .line 219
    .restart local v1    # "iconLayout":Landroid/view/ViewGroup;
    :cond_2
    const v2, 0x7f090022

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 220
    .local v0, "icon":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 221
    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setRightButton(IILandroid/view/View$OnClickListener;)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "iconId"    # I
    .param p3, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 229
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v2, :cond_1

    .line 231
    const v2, 0x7f090025

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 232
    .local v1, "iconLayout":Landroid/view/ViewGroup;
    if-eqz v1, :cond_1

    .line 234
    if-nez p2, :cond_2

    .line 235
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 242
    :cond_0
    :goto_0
    invoke-virtual {v1, p3}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    .end local v1    # "iconLayout":Landroid/view/ViewGroup;
    :cond_1
    return-void

    .line 238
    .restart local v1    # "iconLayout":Landroid/view/ViewGroup;
    :cond_2
    const v2, 0x7f090026

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 239
    .local v0, "icon":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 240
    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setSeekBar(IILcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;)V
    .locals 4
    .param p1, "startPos"    # I
    .param p2, "max"    # I
    .param p3, "listener"    # Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    .prologue
    .line 266
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v2, :cond_0

    .line 268
    const v2, 0x7f090024

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    .line 269
    .local v0, "seekBar":Landroid/widget/SeekBar;
    const v2, 0x7f090005

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 270
    .local v1, "textView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 272
    invoke-virtual {v0, p2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 273
    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 274
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "seekbar start:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", max:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 276
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$1;

    invoke-direct {v2, p0, p3, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;Landroid/widget/TextView;)V

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 299
    .end local v0    # "seekBar":Landroid/widget/SeekBar;
    .end local v1    # "textView":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 133
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v1, :cond_1

    .line 135
    const v1, 0x7f090005

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 136
    .local v0, "textView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 139
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 141
    .end local v0    # "textView":Landroid/widget/TextView;
    :cond_1
    return-void
.end method

.method public setTextColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 157
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v1, :cond_0

    .line 159
    const v1, 0x7f090005

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 160
    .local v0, "textView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 161
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 162
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 163
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 166
    .end local v0    # "textView":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public setTextSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 183
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v1, :cond_0

    .line 185
    const v1, 0x7f090005

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 186
    .local v0, "textView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 187
    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 189
    .end local v0    # "textView":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 257
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v1, :cond_0

    .line 259
    const v1, 0x7f09006e

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 260
    .local v0, "t":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 261
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 263
    .end local v0    # "t":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method

.method public setTypeFace(Landroid/graphics/Typeface;)V
    .locals 2
    .param p1, "tf"    # Landroid/graphics/Typeface;

    .prologue
    .line 192
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->isInit:Z

    if-eqz v1, :cond_0

    .line 194
    const v1, 0x7f090005

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 195
    .local v0, "textView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 198
    .end local v0    # "textView":Landroid/widget/TextView;
    :cond_0
    return-void
.end method
