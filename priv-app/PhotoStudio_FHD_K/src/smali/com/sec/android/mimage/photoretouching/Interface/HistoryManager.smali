.class public Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
.super Ljava/lang/Object;
.source "HistoryManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;,
        Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$RedoAsyncTask;,
        Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$UndoAsyncTask;
    }
.end annotation


# instance fields
.field private final MAX_HISTORY:I

.field private mApplyOriginalThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentSavedIndex:I

.field private mFileName:Ljava/lang/String;

.field private mFirstOriginal:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

.field private mFirstPreview:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

.field private mHistoryManagerCallback:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;

.field private mOriginalCurrentIndex:I

.field private mOriginalHistoryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPreviewCurrentIndex:I

.field private mPreviewHistoryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mHistoryManagerCallback:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;

    .line 662
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mCurrentSavedIndex:I

    .line 663
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    .line 664
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I

    .line 665
    const-string v4, "HistoryInfo"

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFileName:Ljava/lang/String;

    .line 667
    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstPreview:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 668
    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstOriginal:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 670
    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mApplyOriginalThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    .line 671
    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    .line 672
    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;

    .line 673
    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mContext:Landroid/content/Context;

    .line 674
    const/16 v4, 0x1e

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->MAX_HISTORY:I

    .line 27
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mContext:Landroid/content/Context;

    .line 28
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    .line 29
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;

    .line 30
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mHistoryManagerCallback:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;

    invoke-direct {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;)V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mApplyOriginalThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    .line 34
    sget-object v4, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->HISTORY_PATH:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->checkOrMakeFileDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 37
    .local v2, "file":Ljava/io/File;
    if-eqz v2, :cond_0

    .line 39
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 41
    .local v1, "childFileList":[Ljava/io/File;
    if-eqz v1, :cond_0

    .line 42
    array-length v4, v1

    :goto_0
    if-lt v3, v4, :cond_1

    .line 50
    .end local v1    # "childFileList":[Ljava/io/File;
    :cond_0
    return-void

    .line 42
    .restart local v1    # "childFileList":[Ljava/io/File;
    :cond_1
    aget-object v0, v1, v3

    .line 44
    .local v0, "childFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 42
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 456
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)I
    .locals 1

    .prologue
    .line 664
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;I)V
    .locals 0

    .prologue
    .line 664
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .locals 1

    .prologue
    .line 268
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->undoAllOriginal()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .locals 1

    .prologue
    .line 668
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstOriginal:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    return-object v0
.end method

.method private declared-synchronized getFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 458
    monitor-enter p0

    const/4 v1, 0x0

    .line 463
    .local v1, "ret":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 464
    .local v0, "d":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Core/Image;->isPng()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 465
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 469
    :goto_0
    monitor-exit p0

    return-object v1

    .line 467
    :cond_0
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 458
    .end local v0    # "d":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private undoAllOriginal()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .locals 2

    .prologue
    .line 270
    const/4 v0, 0x0

    .line 271
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstOriginal:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    if-eqz v1, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstOriginal:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 273
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I

    .line 274
    return-object v0
.end method

.method private undoAllPreview()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .locals 2

    .prologue
    .line 278
    const/4 v0, 0x0

    .line 279
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstPreview:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    if-eqz v1, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstPreview:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 281
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    .line 282
    return-object v0
.end method


# virtual methods
.method public addHistory([III)V
    .locals 0
    .param p1, "bitmap"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 455
    return-void
.end method

.method public addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V
    .locals 9
    .param p1, "bitmap"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "effectinfo"    # Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    .param p5, "enhanceFlag"    # Z

    .prologue
    const/16 v7, 0x1e

    const/4 v6, 0x0

    .line 378
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mApplyOriginalThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    invoke-virtual {v4, p4, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->setData(Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;I)V

    .line 379
    if-eqz p1, :cond_2

    .line 381
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)V

    .line 382
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iput-object p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->data:[I

    .line 383
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    .line 384
    iput p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    .line 385
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "preview"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    .line 386
    sget-object v4, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->HISTORY_PATH:Ljava/lang/String;

    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    .line 387
    iput-boolean p5, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->autoFlag:Z

    .line 390
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    .line 392
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v7, :cond_0

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    if-le v4, v7, :cond_0

    .line 394
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 395
    .local v1, "hi":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->deleteFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    .line 399
    .end local v1    # "hi":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isPreviewRedo()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 400
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 401
    .local v3, "listSize":I
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_3

    .line 409
    .end local v2    # "i":I
    .end local v3    # "listSize":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 410
    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->data:[I

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    invoke-static {v4, v5, v6, v7, v8}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->saveData(Ljava/lang/String;Ljava/lang/String;[III)Z

    .line 411
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    .line 412
    const/4 v4, 0x0

    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->data:[I

    .line 416
    .end local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    :cond_2
    return-void

    .line 403
    .restart local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .restart local v2    # "i":I
    .restart local v3    # "listSize":I
    :cond_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 404
    .restart local v1    # "hi":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->deleteFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public addMaskBuffer(Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;)V
    .locals 2
    .param p1, "effectinfo"    # Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    .prologue
    .line 420
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mApplyOriginalThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    const/4 v1, 0x5

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->setData(Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;I)V

    .line 421
    return-void
.end method

.method public destroy()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 53
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 55
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v4, v5, :cond_5

    .line 60
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 62
    .end local v4    # "i":I
    :cond_0
    const/4 v5, -0x1

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mCurrentSavedIndex:I

    .line 63
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    .line 65
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v4, v5, :cond_6

    .line 70
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 72
    .end local v4    # "i":I
    :cond_1
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    .line 73
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;

    .line 74
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstPreview:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    if-eqz v5, :cond_2

    .line 75
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstPreview:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstPreview:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->deleteFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_2
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstPreview:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 77
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstOriginal:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    if-eqz v5, :cond_3

    .line 78
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstOriginal:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstOriginal:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->deleteFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_3
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstOriginal:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 83
    sget-object v5, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->HISTORY_PATH:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->checkOrMakeFileDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 85
    .local v2, "file":Ljava/io/File;
    if-eqz v2, :cond_4

    .line 87
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 88
    .local v1, "childFileList":[Ljava/io/File;
    if-eqz v1, :cond_4

    .line 89
    array-length v6, v1

    const/4 v5, 0x0

    :goto_2
    if-lt v5, v6, :cond_7

    .line 97
    .end local v1    # "childFileList":[Ljava/io/File;
    :cond_4
    return-void

    .line 57
    .end local v2    # "file":Ljava/io/File;
    .restart local v4    # "i":I
    :cond_5
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 58
    .local v3, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v5, v3, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v6, v3, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->deleteFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 67
    .end local v3    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    :cond_6
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 68
    .restart local v3    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v5, v3, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v6, v3, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->deleteFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 89
    .end local v3    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .end local v4    # "i":I
    .restart local v1    # "childFileList":[Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    :cond_7
    aget-object v0, v1, v5

    .line 91
    .local v0, "childFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 89
    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method public getCurrentSavedIndex()I
    .locals 1

    .prologue
    .line 648
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mCurrentSavedIndex:I

    return v0
.end method

.method public getEnhance()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 199
    const/4 v0, 0x0

    .line 200
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 201
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 202
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 204
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->undoAllPreview()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    move-result-object v0

    .line 205
    iget-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->autoFlag:Z

    .line 216
    :cond_0
    :goto_0
    return v1

    .line 209
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 210
    .restart local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->autoFlag:Z

    goto :goto_0
.end method

.method public getOriginalHistory()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .locals 3

    .prologue
    .line 329
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I

    if-eq v1, v2, :cond_0

    .line 330
    const/4 v0, 0x0

    .line 336
    :goto_0
    return-object v0

    .line 331
    :cond_0
    const/4 v0, 0x0

    .line 332
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 333
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstOriginal:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    goto :goto_0

    .line 335
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .restart local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    goto :goto_0
.end method

.method public getPreviewCurrentIndex()I
    .locals 1

    .prologue
    .line 658
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    return v0
.end method

.method public initFirstOriginalHistory([III)V
    .locals 7
    .param p1, "bitmap"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v3, 0x0

    .line 359
    if-eqz p1, :cond_0

    .line 361
    mul-int v2, p2, p3

    new-array v1, v2, [I

    .line 362
    .local v1, "pixels":[I
    mul-int v2, p2, p3

    invoke-static {p1, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 364
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)V

    .line 365
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->data:[I

    .line 366
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    .line 367
    iput p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    .line 368
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "original"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    .line 369
    sget-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->HISTORY_PATH:Ljava/lang/String;

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    .line 370
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstOriginal:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 372
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->data:[I

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    invoke-static {v2, v3, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->saveData(Ljava/lang/String;Ljava/lang/String;[III)Z

    .line 373
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->data:[I

    .line 375
    .end local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .end local v1    # "pixels":[I
    :cond_0
    return-void
.end method

.method public initFirstPreviewHistory([III)V
    .locals 7
    .param p1, "bitmap"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v3, 0x0

    .line 340
    if-eqz p1, :cond_0

    .line 342
    mul-int v2, p2, p3

    new-array v1, v2, [I

    .line 343
    .local v1, "pixels":[I
    mul-int v2, p2, p3

    invoke-static {p1, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 345
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)V

    .line 346
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->data:[I

    .line 347
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    .line 348
    iput p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    .line 349
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "preview"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    .line 350
    sget-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->HISTORY_PATH:Ljava/lang/String;

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    .line 351
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstPreview:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 353
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->data:[I

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    invoke-static {v2, v3, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->saveData(Ljava/lang/String;Ljava/lang/String;[III)Z

    .line 354
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->data:[I

    .line 356
    .end local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .end local v1    # "pixels":[I
    :cond_0
    return-void
.end method

.method public isDoingThread()Z
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mApplyOriginalThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->isDoingThread()Z

    move-result v0

    return v0
.end method

.method public isOriginalRedo()Z
    .locals 3

    .prologue
    .line 150
    const/4 v0, 0x0

    .line 151
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 153
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 155
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 157
    const/4 v0, 0x1

    .line 161
    :cond_0
    return v0
.end method

.method public isPreviewRedo()Z
    .locals 3

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 136
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 140
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 142
    const/4 v0, 0x1

    .line 146
    :cond_0
    return v0
.end method

.method public isRedo()Z
    .locals 3

    .prologue
    .line 120
    const/4 v0, 0x0

    .line 121
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 125
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 127
    const/4 v0, 0x1

    .line 131
    :cond_0
    return v0
.end method

.method public isUndo()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 100
    const/4 v0, 0x0

    .line 101
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 105
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    if-ne v1, v2, :cond_1

    .line 107
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstPreview:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    if-eqz v1, :cond_0

    .line 108
    const/4 v0, 0x1

    .line 116
    :cond_0
    :goto_0
    return v0

    .line 110
    :cond_1
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    if-le v1, v2, :cond_0

    .line 112
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public loadOriginalData(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 5
    .param p1, "hinfo"    # Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .param p2, "imagedata"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 318
    if-nez p1, :cond_1

    .line 326
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    iget v1, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    iget v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    mul-int/2addr v1, v2

    new-array v0, v1, [I

    .line 322
    .local v0, "data":[I
    iget-object v1, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    iget v3, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    iget v4, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    invoke-static {v1, v2, v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->loadData(Ljava/lang/String;Ljava/lang/String;[III)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 324
    iget v1, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    iget v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    invoke-virtual {p2, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([III)V

    goto :goto_0
.end method

.method public loadPreviewData(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z
    .locals 5
    .param p1, "hinfo"    # Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .param p2, "imagedata"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p3, "viewwdt"    # I
    .param p4, "viewhgt"    # I

    .prologue
    .line 305
    if-nez p1, :cond_0

    .line 306
    const/4 v1, 0x0

    .line 314
    :goto_0
    return v1

    .line 307
    :cond_0
    iget v1, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    iget v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    mul-int/2addr v1, v2

    new-array v0, v1, [I

    .line 308
    .local v0, "data":[I
    iget-object v1, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    iget v3, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    iget v4, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    invoke-static {v1, v2, v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->loadData(Ljava/lang/String;Ljava/lang/String;[III)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 310
    iget v1, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    iget v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    invoke-virtual {p2, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBuffer([III)V

    .line 311
    invoke-virtual {p2, p3, p4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 314
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public redo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z
    .locals 3
    .param p1, "imagedata"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "viewwdt"    # I
    .param p3, "viewhgt"    # I

    .prologue
    .line 226
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mApplyOriginalThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->isUndoRedoThread()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227
    const/4 v1, 0x0

    .line 252
    :goto_0
    return v1

    .line 228
    :cond_0
    const/4 v0, 0x0

    .line 229
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 231
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 233
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    .line 235
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gt v1, v2, :cond_4

    .line 237
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 246
    .restart local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mApplyOriginalThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    const/4 v2, 0x2

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->setData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;I)V

    .line 247
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->loadPreviewData(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 249
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setSaved()V

    .line 250
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_3

    .line 251
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->loadTextures(Landroid/content/Context;)V

    .line 252
    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 241
    :cond_4
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    .line 242
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I

    goto :goto_1
.end method

.method public redoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .locals 3
    .param p1, "imagedata"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "viewwdt"    # I
    .param p3, "viewhgt"    # I

    .prologue
    .line 286
    const/4 v0, 0x0

    .line 287
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 289
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 291
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 294
    .restart local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    .line 295
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mApplyOriginalThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    const/4 v2, 0x4

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->setData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;I)V

    .line 296
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->loadPreviewData(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 298
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setSaved()V

    .line 299
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_2

    .line 300
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->loadTextures(Landroid/content/Context;)V

    .line 301
    :cond_2
    return-object v0
.end method

.method public setCurrentSavedIndex()V
    .locals 1

    .prologue
    .line 653
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mCurrentSavedIndex:I

    .line 654
    return-void
.end method

.method public undo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z
    .locals 4
    .param p1, "imagedata"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "viewwdt"    # I
    .param p3, "viewhgt"    # I

    .prologue
    const/4 v1, 0x1

    .line 166
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mApplyOriginalThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->isUndoRedoThread()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 167
    const/4 v1, 0x0

    .line 194
    :cond_0
    :goto_0
    return v1

    .line 168
    :cond_1
    const/4 v0, 0x0

    .line 170
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 172
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 174
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    .line 176
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    if-ge v2, v1, :cond_4

    .line 178
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->undoAllPreview()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    move-result-object v0

    .line 188
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mApplyOriginalThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    invoke-virtual {v2, p1, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->setData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;I)V

    .line 189
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->loadPreviewData(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 191
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setSaved()V

    .line 192
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 193
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->loadTextures(Landroid/content/Context;)V

    goto :goto_0

    .line 182
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewHistoryList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .restart local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    goto :goto_1
.end method

.method public undoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .locals 3
    .param p1, "imagedata"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "viewwdt"    # I
    .param p3, "viewhgt"    # I

    .prologue
    .line 256
    const/4 v0, 0x0

    .line 257
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstPreview:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    if-eqz v1, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstPreview:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 259
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mPreviewCurrentIndex:I

    .line 260
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->loadPreviewData(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    .line 261
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 262
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->loadTextures(Landroid/content/Context;)V

    .line 263
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mApplyOriginalThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;

    const/4 v2, 0x3

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->setData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;I)V

    .line 264
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_2

    .line 265
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->loadTextures(Landroid/content/Context;)V

    .line 266
    :cond_2
    return-object v0
.end method
