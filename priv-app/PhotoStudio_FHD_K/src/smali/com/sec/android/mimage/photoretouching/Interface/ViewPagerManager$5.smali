.class Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$5;
.super Ljava/lang/Object;
.source "ViewPagerManager.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->initListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    .line 383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 387
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 397
    :cond_0
    :goto_0
    return v1

    .line 390
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mColorPicker:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->hideColorpicker()V

    .line 393
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$5;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 387
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
