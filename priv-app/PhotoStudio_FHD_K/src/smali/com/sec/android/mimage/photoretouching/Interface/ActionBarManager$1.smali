.class Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;
.super Ljava/lang/Object;
.source "ActionBarManager.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initHorizontalScrollView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method


# virtual methods
.method public onLayoutCallback()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 224
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Landroid/widget/LinearLayout;

    move-result-object v5

    if-nez v5, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 228
    .local v3, "scrollParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mCustomLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 229
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v4, 0x0

    .line 230
    .local v4, "totalWidth":I
    const/4 v1, 0x0

    .line 232
    .local v1, "homeWidth":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mViewWidth:I
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)I

    move-result v5

    if-lez v5, :cond_0

    .line 234
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 245
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mViewWidth:I
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)I

    move-result v5

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 246
    add-int v5, v4, v1

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mViewWidth:I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)I

    move-result v6

    if-le v5, v6, :cond_5

    .line 248
    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 255
    :goto_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    new-instance v6, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1$1;

    invoke-direct {v6, p0, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;Landroid/widget/LinearLayout$LayoutParams;Landroid/widget/LinearLayout$LayoutParams;)V

    invoke-virtual {v5, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 234
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 236
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v6

    if-nez v6, :cond_2

    .line 238
    invoke-virtual {v0, v7, v7}, Landroid/widget/LinearLayout;->measure(II)V

    .line 239
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v6

    if-nez v6, :cond_4

    .line 240
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v1

    goto :goto_1

    .line 242
    :cond_4
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v4, v6

    goto :goto_1

    .line 252
    .end local v0    # "button":Landroid/widget/LinearLayout;
    :cond_5
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->mViewWidth:I
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)I

    move-result v5

    sub-int/2addr v5, v1

    iput v5, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    goto :goto_2
.end method

.method public onScrollCallback(I)V
    .locals 0
    .param p1, "scrollX"    # I

    .prologue
    .line 271
    return-void
.end method
