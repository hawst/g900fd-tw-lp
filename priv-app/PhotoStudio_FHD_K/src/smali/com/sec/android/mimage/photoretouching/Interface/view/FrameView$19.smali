.class Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$19;
.super Ljava/lang/Object;
.source "FrameView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemLongClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

.field private final synthetic val$activities:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$19;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$19;->val$activities:Ljava/util/List;

    .line 2152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 8
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 2158
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$19;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Landroid/content/Context;

    move-result-object v5

    const-string v6, "audio"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 2159
    .local v1, "am":Landroid/media/AudioManager;
    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v5

    if-eqz v5, :cond_0

    .line 2161
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$19;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Landroid/content/Context;

    move-result-object v5

    const-string v6, "vibrator"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Vibrator;

    .line 2162
    .local v4, "v":Landroid/os/Vibrator;
    const-wide/16 v6, 0xa

    invoke-virtual {v4, v6, v7}, Landroid/os/Vibrator;->vibrate(J)V

    .line 2165
    .end local v4    # "v":Landroid/os/Vibrator;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$19;->val$activities:Ljava/util/List;

    invoke-interface {v5, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v0, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 2167
    .local v0, "ai":Landroid/content/pm/ActivityInfo;
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2168
    .local v2, "intent":Landroid/content/Intent;
    const-string v5, "package"

    iget-object v6, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2169
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2171
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$19;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v5, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->startActivity(Landroid/content/Intent;)V

    .line 2173
    const/4 v5, 0x0

    return v5
.end method
