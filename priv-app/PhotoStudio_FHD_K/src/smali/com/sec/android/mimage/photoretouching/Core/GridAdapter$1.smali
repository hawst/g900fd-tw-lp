.class Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;
.super Ljava/lang/Object;
.source "GridAdapter.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    .line 560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 12
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    const v10, 0x7f050221

    .line 566
    const-string v8, "JW onFocusChange"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 568
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mGridView:Landroid/widget/GridView;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)Landroid/widget/GridView;

    move-result-object v1

    .line 569
    .local v1, "gridView":Landroid/widget/GridView;
    if-eqz p2, :cond_0

    .line 571
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 572
    .local v6, "rect":Landroid/graphics/Rect;
    invoke-virtual {p1, v6}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 573
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 574
    .local v0, "contentViewRect":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->access$1(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getTabContentView()Landroid/widget/FrameLayout;

    move-result-object v8

    invoke-virtual {v8, v0}, Landroid/widget/FrameLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 575
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->access$2(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f05021f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 576
    .local v5, "offset":I
    const/4 v4, 0x0

    .line 577
    .local v4, "itemOffsetForScrollUp":I
    const/4 v3, 0x0

    .line 579
    .local v3, "itemOffsetForScrollDown":I
    const/16 v7, 0x32

    .line 581
    .local v7, "scrollDuration":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 594
    :goto_0
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "JW rect="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 595
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "JW contentViewRect="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 597
    iget v8, v6, Landroid/graphics/Rect;->bottom:I

    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    if-gt v8, v9, :cond_1

    iget v8, v6, Landroid/graphics/Rect;->bottom:I

    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v9, v5

    if-lt v8, v9, :cond_1

    .line 599
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int/2addr v8, v4

    invoke-virtual {v1, v8, v7}, Landroid/widget/GridView;->smoothScrollBy(II)V

    .line 600
    const-string v8, "JW scrollUp"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 654
    .end local v0    # "contentViewRect":Landroid/graphics/Rect;
    .end local v3    # "itemOffsetForScrollDown":I
    .end local v4    # "itemOffsetForScrollUp":I
    .end local v5    # "offset":I
    .end local v6    # "rect":Landroid/graphics/Rect;
    .end local v7    # "scrollDuration":I
    :cond_0
    :goto_1
    const-string v8, "JW onFocusChange end"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 655
    return-void

    .line 584
    .restart local v0    # "contentViewRect":Landroid/graphics/Rect;
    .restart local v3    # "itemOffsetForScrollDown":I
    .restart local v4    # "itemOffsetForScrollUp":I
    .restart local v5    # "offset":I
    .restart local v6    # "rect":Landroid/graphics/Rect;
    .restart local v7    # "scrollDuration":I
    :sswitch_0
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->access$2(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050220

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 585
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->access$2(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 587
    goto :goto_0

    .line 589
    :sswitch_1
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->access$2(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f050223

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 590
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->access$2(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    goto/16 :goto_0

    .line 603
    :cond_1
    iget v8, v6, Landroid/graphics/Rect;->top:I

    iget v9, v0, Landroid/graphics/Rect;->top:I

    if-lt v8, v9, :cond_0

    iget v8, v6, Landroid/graphics/Rect;->top:I

    iget v9, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v5

    if-gt v8, v9, :cond_0

    .line 606
    invoke-virtual {v1}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->access$1(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getNumColumn()I

    move-result v9

    if-le v8, v9, :cond_2

    .line 608
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int/2addr v8, v3

    neg-int v8, v8

    invoke-virtual {v1, v8, v7}, Landroid/widget/GridView;->smoothScrollBy(II)V

    .line 609
    const-string v8, "JW scrolldown"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_1

    .line 611
    :cond_2
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v8

    const/high16 v9, 0x31100000

    if-eq v8, v9, :cond_3

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v8

    const/high16 v9, 0x31300000

    if-eq v8, v9, :cond_3

    .line 612
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v8

    const/high16 v9, 0x31500000

    if-ne v8, v9, :cond_4

    .line 614
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v8

    neg-int v8, v8

    invoke-virtual {v1, v8, v7}, Landroid/widget/GridView;->smoothScrollBy(II)V

    .line 616
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1$1;

    invoke-direct {v8, p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;Landroid/view/View;)V

    .line 622
    const-wide/16 v10, 0x19

    .line 616
    invoke-virtual {p1, v8, v10, v11}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 627
    :cond_4
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-virtual {v1}, Landroid/widget/GridView;->getCount()I

    move-result v8

    if-ge v2, v8, :cond_0

    .line 629
    invoke-virtual {v1, v2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->isFocused()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 632
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;->access$1(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getNumColumn()I

    move-result v8

    sub-int v8, v2, v8

    invoke-virtual {v1, v8}, Landroid/widget/GridView;->setSelection(I)V

    .line 633
    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setSelection(I)V

    .line 634
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1$2;

    invoke-direct {v8, p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1$2;-><init>(Lcom/sec/android/mimage/photoretouching/Core/GridAdapter$1;Landroid/view/View;)V

    .line 642
    const-wide/16 v10, 0x0

    .line 634
    invoke-virtual {p1, v8, v10, v11}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 643
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "JW scrolldown to my position="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 627
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 581
    nop

    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_0
        0x31300000 -> :sswitch_1
    .end sparse-switch
.end method
