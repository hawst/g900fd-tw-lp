.class Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$1;
.super Ljava/lang/Object;
.source "LauncherActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->initView(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    const/4 v1, 0x2

    .line 174
    if-ne p1, v1, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCurrentFocusIndex:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    packed-switch v0, :pswitch_data_0

    .line 188
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->access$2(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCurrentFocusIndex:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->getImageData(I)Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->requestFocus()Z

    .line 190
    :cond_0
    return-void

    .line 180
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCurrentFocusIndex:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)I

    move-result v0

    if-lt v0, v1, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCurrentFocusIndex:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->access$1(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;I)V

    goto :goto_0

    .line 186
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCurrentFocusIndex:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->access$1(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;I)V

    goto :goto_0

    .line 176
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 193
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$1;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setIndicator(I)V

    .line 198
    return-void
.end method
