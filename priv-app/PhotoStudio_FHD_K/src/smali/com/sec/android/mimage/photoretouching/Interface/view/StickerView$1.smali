.class Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$1;
.super Ljava/lang/Object;
.source "StickerView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public actionBarAbleSave()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableSave()V

    .line 129
    :cond_0
    return-void
.end method

.method public actionBarUnableSave()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableSave()V

    .line 134
    :cond_0
    return-void
.end method

.method public closeDecorationDrawer()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    .line 142
    return-void
.end method

.method public openDecorationDrawer()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->openDrawer()V

    .line 138
    return-void
.end method

.method public set2DepthActionBar()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->set2depth()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 146
    return-void
.end method

.method public setVisibleMenu(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->setVisibleTopBottomMenu(Z)V
    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;Z)V

    .line 124
    return-void
.end method
