.class public Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "DecorationView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ResolverSetAsAdapter;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ResolverShareAdapter;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$TrayDeleteFunction;
    }
.end annotation


# instance fields
.field public final FINISH_PROGRESS:I

.field private final REDO:I

.field private final REDOALL:I

.field private REDOALL_DIALOG:I

.field private SAVE_YES_NO_CANCEL_DIALOG:I

.field public final SHOW_PROGRESS:I

.field public final SHOW_SAVE_TOAST:I

.field public final TOP_BOTTOM_SHOW_HIDE_AVAILABLE_DIFF:I

.field private final UNDO:I

.field private final UNDOALL:I

.field private UNDOALL_DIALOG:I

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mClearPaint:Landroid/graphics/Paint;

.field private mColor:I

.field private mContext:Landroid/content/Context;

.field private mCurrentSaveSize:I

.field private mDashPathEffectPaint:Landroid/graphics/Paint;

.field private mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mFileName:Ljava/lang/String;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIsBackPressed:Z

.field private mIsDestroy:Z

.field private mIsMinimum:Z

.field private mIsPenTouch:Z

.field private mLinePaint:Landroid/graphics/Paint;

.field private mLongpressButton:Z

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mOptionItemId:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mPushSaveButton:Z

.field private mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

.field private mToChildrenDepth:Z

.field private mTrayDeleteFunction:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$TrayDeleteFunction;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;

.field private msave:Z

.field private msetas:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p6, "decoManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 2378
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    .line 2379
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 2380
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 2381
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 2382
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2383
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 2384
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 2385
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->msetas:Z

    .line 2386
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->msave:Z

    .line 2387
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPaint:Landroid/graphics/Paint;

    .line 2388
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mClearPaint:Landroid/graphics/Paint;

    .line 2389
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mLinePaint:Landroid/graphics/Paint;

    .line 2390
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 2394
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2396
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayDeleteFunction:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$TrayDeleteFunction;

    .line 2397
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 2399
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

    .line 2401
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mFileName:Ljava/lang/String;

    .line 2402
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsBackPressed:Z

    .line 2403
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsMinimum:Z

    .line 2404
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPushSaveButton:Z

    .line 2405
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsDestroy:Z

    .line 2406
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mOptionItemId:I

    .line 2408
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mColor:I

    .line 2409
    const/4 v3, 0x4

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->UNDOALL_DIALOG:I

    .line 2410
    const/4 v3, 0x5

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->REDOALL_DIALOG:I

    .line 2411
    const/4 v3, 0x7

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->SAVE_YES_NO_CANCEL_DIALOG:I

    .line 2413
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->UNDO:I

    const/4 v3, 0x1

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->REDO:I

    const/4 v3, 0x2

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->UNDOALL:I

    const/4 v3, 0x3

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->REDOALL:I

    .line 2414
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 2415
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->SHOW_PROGRESS:I

    .line 2416
    const/4 v3, 0x1

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->FINISH_PROGRESS:I

    .line 2417
    const/4 v3, 0x2

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->SHOW_SAVE_TOAST:I

    .line 2418
    const/16 v3, 0xa

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->TOP_BOTTOM_SHOW_HIDE_AVAILABLE_DIFF:I

    .line 2420
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 2421
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 2422
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mToChildrenDepth:Z

    .line 2423
    const v3, 0x7a1200

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mCurrentSaveSize:I

    .line 2424
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mLongpressButton:Z

    .line 2425
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsPenTouch:Z

    .line 91
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    .line 92
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 93
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 94
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 95
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 96
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 97
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->setInterface(Ljava/lang/Object;)V

    .line 99
    new-instance v3, Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPaint:Landroid/graphics/Paint;

    .line 100
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 102
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mClearPaint:Landroid/graphics/Paint;

    .line 103
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mClearPaint:Landroid/graphics/Paint;

    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 105
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mLinePaint:Landroid/graphics/Paint;

    .line 106
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 107
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 109
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 110
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 111
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 112
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    new-instance v4, Landroid/graphics/DashPathEffect;

    const/4 v5, 0x4

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    const/high16 v6, 0x40000000    # 2.0f

    invoke-direct {v4, v5, v6}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 114
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v3, :cond_0

    .line 116
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 117
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 119
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->init()V

    .line 122
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_1

    .line 124
    const/4 v2, 0x0

    .line 126
    .local v2, "previewBuffer":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 128
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 129
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v2

    .line 132
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 133
    new-instance v1, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 135
    .local v1, "canvas":Landroid/graphics/Canvas;
    const/4 v3, 0x0

    .line 136
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 137
    const/4 v5, 0x0

    .line 138
    const/4 v6, 0x0

    .line 139
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    .line 140
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v8

    .line 141
    const/4 v9, 0x1

    .line 142
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPaint:Landroid/graphics/Paint;

    .line 134
    invoke-virtual/range {v1 .. v10}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 144
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "previewBuffer":[I
    :cond_1
    return-void

    .line 131
    .restart local v2    # "previewBuffer":[I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    goto :goto_0

    .line 112
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;)V
    .locals 0

    .prologue
    .line 2399
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I
    .locals 1

    .prologue
    .line 2408
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mColor:I

    return v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 2420
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 2420
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I
    .locals 1

    .prologue
    .line 2406
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mOptionItemId:I

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;I)Z
    .locals 1

    .prologue
    .line 1622
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->runOptionItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;I)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->setViewLayerType(I)V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2394
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .locals 1

    .prologue
    .line 2383
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Z
    .locals 1

    .prologue
    .line 2425
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsPenTouch:Z

    return v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Z
    .locals 1

    .prologue
    .line 2403
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsMinimum:Z

    return v0
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Z)V
    .locals 0

    .prologue
    .line 2422
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mToChildrenDepth:Z

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;I)V
    .locals 0

    .prologue
    .line 2408
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mColor:I

    return-void
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Z)V
    .locals 0

    .prologue
    .line 2425
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsPenTouch:Z

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;I)V
    .locals 0

    .prologue
    .line 722
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->undoNredo(I)V

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 2382
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;I)V
    .locals 0

    .prologue
    .line 2423
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mCurrentSaveSize:I

    return-void
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 2381
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Z
    .locals 1

    .prologue
    .line 2404
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPushSaveButton:Z

    return v0
.end method

.method static synthetic access$26(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Z)V
    .locals 0

    .prologue
    .line 2404
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPushSaveButton:Z

    return-void
.end method

.method static synthetic access$27(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;I)V
    .locals 0

    .prologue
    .line 2406
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mOptionItemId:I

    return-void
.end method

.method static synthetic access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$29(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 2379
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$30(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V
    .locals 0

    .prologue
    .line 2333
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->refreshImageAndBottomButtons()V

    return-void
.end method

.method static synthetic access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I
    .locals 1

    .prologue
    .line 2409
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->UNDOALL_DIALOG:I

    return v0
.end method

.method static synthetic access$32(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I
    .locals 1

    .prologue
    .line 2410
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->REDOALL_DIALOG:I

    return v0
.end method

.method static synthetic access$33(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 2394
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$34(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Z)V
    .locals 0

    .prologue
    .line 2424
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mLongpressButton:Z

    return-void
.end method

.method static synthetic access$35(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Z
    .locals 1

    .prologue
    .line 2424
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mLongpressButton:Z

    return v0
.end method

.method static synthetic access$36(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Landroid/content/Intent;Z)V
    .locals 0

    .prologue
    .line 802
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    return-void
.end method

.method static synthetic access$37(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V
    .locals 0

    .prologue
    .line 183
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->setMainBtnListener()V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 2414
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 2378
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 2384
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 2380
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I
    .locals 1

    .prologue
    .line 2423
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mCurrentSaveSize:I

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 2414
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method private getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 400
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$9;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    return-object v0
.end method

.method private init2DepthActionBar()V
    .locals 5

    .prologue
    const/4 v2, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1898
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 1900
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isValidBackKey()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1902
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1905
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$26;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$26;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1902
    invoke-virtual {v0, v3, v2, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 1945
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1948
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$27;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1945
    invoke-virtual {v0, v3, v4, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1994
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x2

    .line 1997
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$28;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$28;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1994
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 2049
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 2052
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$29;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$29;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 2049
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 2081
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x5

    .line 2084
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$30;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$30;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 2081
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 2123
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0x10

    .line 2126
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$31;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$31;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 2123
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 2162
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 2164
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 2165
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resetMenu()V

    .line 2168
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_2

    .line 2170
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2171
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isSaved()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 2182
    :cond_2
    :goto_1
    return-void

    .line 1942
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 1943
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v4, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    goto :goto_0

    .line 2174
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isSaved()Z

    move-result v1

    invoke-virtual {v0, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_1
.end method

.method private initSaveOptionDialog()V
    .locals 11

    .prologue
    const v10, 0x7f0601f0

    const v9, 0x7f0601cb

    const/16 v2, 0x9

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 1269
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1323
    :goto_0
    return-void

    .line 1274
    :cond_0
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$16;

    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1288
    .local v8, "saveListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1290
    const v3, 0x7f0601cd

    .line 1292
    const/4 v5, 0x1

    .line 1294
    const v7, 0x103012e

    .line 1288
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZZLandroid/graphics/Point;I)V

    .line 1296
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1298
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x810

    move v3, v9

    move v5, v9

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1302
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x820

    move v3, v10

    move v5, v10

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1304
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$17;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$17;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1311
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$18;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$18;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1322
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    goto :goto_0
.end method

.method private initSaveYesNoCancel()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 1327
    const/4 v7, 0x0

    .line 1328
    .local v7, "fileName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 1329
    .local v8, "path":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 1331
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1332
    if-nez v7, :cond_0

    .line 1334
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1337
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_1

    .line 1339
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1340
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->SAVE_YES_NO_CANCEL_DIALOG:I

    .line 1341
    const v3, 0x7f06000d

    .line 1342
    const/4 v4, 0x1

    .line 1344
    const v6, 0x103012e

    .line 1339
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1346
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1347
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x500

    .line 1350
    const v4, 0x7f06007d

    move v2, v9

    move v3, v9

    move-object v6, v5

    .line 1347
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1354
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000a

    .line 1355
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$19;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$19;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1354
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V

    .line 1386
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000b

    .line 1387
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$20;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$20;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1386
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1402
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1403
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$21;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$21;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1402
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1426
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1428
    :cond_1
    return-void
.end method

.method private initSaveYesNoCancelForFinish()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 1432
    const/4 v7, 0x0

    .line 1433
    .local v7, "fileName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 1434
    .local v8, "path":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 1436
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1437
    if-nez v7, :cond_0

    .line 1439
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1443
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1444
    const/16 v2, 0x8

    .line 1445
    const v3, 0x7f06019d

    .line 1446
    const/4 v4, 0x1

    .line 1448
    const v6, 0x103012e

    .line 1443
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1450
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1451
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x500

    .line 1454
    const v4, 0x7f060192

    move v2, v9

    move v3, v9

    move-object v6, v5

    .line 1451
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1458
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    .line 1459
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$22;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$22;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1458
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V

    .line 1481
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601b2

    .line 1482
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$23;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$23;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1481
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1490
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1491
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$24;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$24;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1490
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1498
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1499
    return-void
.end method

.method private initSetAsDialog()V
    .locals 7

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 1013
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1014
    const v2, 0x7f090158

    .line 1015
    const v3, 0x7f0600a3

    .line 1016
    const/4 v4, 0x0

    .line 1017
    const/4 v5, 0x0

    .line 1018
    const v6, 0x103012e

    .line 1013
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1019
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1021
    :cond_0
    return-void
.end method

.method private initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isShareVia"    # Z

    .prologue
    .line 805
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 806
    .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    const/4 v2, 0x0

    .line 807
    .local v2, "shareChooser":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 808
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    const v4, 0x7f06000f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 812
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/content/pm/LabeledIntent;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/pm/LabeledIntent;

    .line 813
    .local v0, "extraIntents":[Landroid/content/pm/LabeledIntent;
    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 814
    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 815
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 996
    return-void

    .line 810
    .end local v0    # "extraIntents":[Landroid/content/pm/LabeledIntent;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    const v4, 0x7f0600a3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method private initShareViaDialog()V
    .locals 7

    .prologue
    .line 999
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 1001
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1002
    const v2, 0x7f090157

    .line 1003
    const v3, 0x7f06000f

    .line 1004
    const/4 v4, 0x0

    .line 1005
    const/4 v5, 0x0

    .line 1006
    const v6, 0x103012e

    .line 1001
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1007
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1009
    :cond_0
    return-void
.end method

.method private initUndoRedoAllDialog(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V
    .locals 8
    .param p1, "actionBarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .prologue
    const v6, 0x103012e

    const/16 v1, 0x1000

    const/16 v7, 0x500

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1071
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 1073
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1074
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->UNDOALL_DIALOG:I

    .line 1075
    const v3, 0x7f0600a6

    .line 1073
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1080
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1081
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1082
    const v2, 0x7f06009c

    .line 1084
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$10;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1081
    invoke-virtual {v0, v7, v2, v5, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1091
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060007

    .line 1092
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$11;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1091
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1100
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060009

    .line 1101
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$12;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1100
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1109
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1111
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1112
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->REDOALL_DIALOG:I

    .line 1113
    const v3, 0x7f0600a1

    .line 1111
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1118
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1119
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1120
    const v1, 0x7f0601ce

    .line 1122
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$13;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1119
    invoke-virtual {v0, v7, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1129
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 1130
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$14;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1129
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1138
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1139
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$15;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    .line 1138
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1147
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1149
    :cond_0
    return-void
.end method

.method private refreshImageAndBottomButtons()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/high16 v10, 0x42c80000    # 100.0f

    const/4 v2, 0x0

    .line 2335
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_1

    .line 2337
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-ltz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-gez v3, :cond_5

    .line 2338
    :cond_0
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsMinimum:Z

    .line 2342
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2343
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 2344
    .local v1, "output":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 2345
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2348
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 2351
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 2352
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 2354
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 2346
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 2356
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "output":[I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_4

    .line 2358
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_3

    .line 2360
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-ltz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-gez v3, :cond_6

    .line 2361
    :cond_2
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsMinimum:Z

    .line 2365
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$32;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$32;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2374
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->invalidateViews()V

    .line 2375
    return-void

    .line 2340
    :cond_5
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsMinimum:Z

    goto :goto_0

    .line 2363
    :cond_6
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsMinimum:Z

    goto :goto_1
.end method

.method private runOptionItem(I)Z
    .locals 6
    .param p1, "optionItemId"    # I

    .prologue
    const v5, 0x7f090158

    const v4, 0x7a1200

    const/16 v3, 0x9

    .line 1624
    const/4 v0, 0x0

    .line 1625
    .local v0, "ret":Z
    const/4 v1, 0x0

    .line 1626
    .local v1, "uri":Landroid/net/Uri;
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mOptionItemId:I

    .line 1627
    sparse-switch p1, :sswitch_data_0

    .line 1693
    :cond_0
    :goto_0
    return v0

    .line 1630
    :sswitch_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->SAVE_YES_NO_CANCEL_DIALOG:I

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1632
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->SAVE_YES_NO_CANCEL_DIALOG:I

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1639
    :goto_1
    const/4 v0, 0x1

    .line 1640
    goto :goto_0

    .line 1637
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->openCamera(Landroid/content/Context;)V

    goto :goto_1

    .line 1642
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1644
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mCurrentSaveSize:I

    .line 1645
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1646
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1658
    :cond_2
    :goto_2
    const/4 v0, 0x1

    .line 1659
    goto :goto_0

    .line 1649
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f090157

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1650
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 1651
    if-nez v1, :cond_4

    .line 1653
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1655
    :cond_4
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    goto :goto_2

    .line 1661
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x2e000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 1662
    const/4 v0, 0x1

    .line 1663
    goto :goto_0

    .line 1666
    :sswitch_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1668
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mCurrentSaveSize:I

    .line 1669
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1670
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1683
    :cond_5
    :goto_3
    const/4 v0, 0x1

    .line 1684
    goto/16 :goto_0

    .line 1672
    :cond_6
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1675
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 1676
    if-nez v1, :cond_7

    .line 1678
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1680
    :cond_7
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    .line 1681
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    goto :goto_3

    .line 1689
    :sswitch_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 1690
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x17000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 1627
    :sswitch_data_0
    .sparse-switch
        0x7f090007 -> :sswitch_2
        0x7f090157 -> :sswitch_1
        0x7f090158 -> :sswitch_3
        0x7f09015a -> :sswitch_0
        0x7f09015b -> :sswitch_4
    .end sparse-switch
.end method

.method private setMainBtnListener()V
    .locals 7

    .prologue
    const v6, 0x10001005

    const v5, 0x10001004

    const/4 v4, 0x0

    const v3, 0x1000100b

    const/4 v2, 0x0

    .line 185
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEnableDecoration()Z

    move-result v0

    if-nez v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 200
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsMinimum:Z

    if-eqz v0, :cond_2

    .line 202
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    invoke-virtual {v0, v1, v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6, v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001006

    invoke-virtual {v0, v1, v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v3, v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 375
    :goto_1
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getPreviousMode()I

    move-result v0

    const/high16 v1, -0x10000

    and-int/2addr v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 393
    :goto_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    goto :goto_0

    .line 233
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$4;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$6;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001006

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$7;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 350
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$8;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_1

    .line 378
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto :goto_2

    .line 381
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto :goto_2

    .line 384
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto :goto_2

    .line 387
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001006

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto :goto_2

    .line 390
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto :goto_2

    .line 375
    :sswitch_data_0
    .sparse-switch
        0x11000000 -> :sswitch_0
        0x15000000 -> :sswitch_1
        0x16000000 -> :sswitch_2
        0x18000000 -> :sswitch_3
        0x31000000 -> :sswitch_4
    .end sparse-switch
.end method

.method private undoNredo(I)V
    .locals 7
    .param p1, "who"    # I

    .prologue
    const/high16 v6, 0x42c80000    # 100.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 724
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_4

    .line 726
    packed-switch p1, :pswitch_data_0

    .line 741
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->refreshImageAndBottomButtons()V

    .line 743
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 745
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 746
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 759
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 760
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0, v5, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 762
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 764
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 771
    :goto_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_7

    .line 773
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableSave()V

    .line 774
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_2

    .line 775
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setIsEdited(Z)V

    .line 785
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_4

    .line 787
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v6

    if-ltz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v6

    if-gez v0, :cond_8

    .line 788
    :cond_3
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsMinimum:Z

    .line 797
    :cond_4
    :goto_4
    return-void

    .line 729
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->undo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    goto :goto_0

    .line 732
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->redo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    goto/16 :goto_0

    .line 735
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->undoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    goto/16 :goto_0

    .line 738
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->redoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    goto/16 :goto_0

    .line 749
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    .line 750
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 751
    if-nez p1, :cond_0

    .line 753
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableSave()V

    .line 754
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 755
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setIsEdited(Z)V

    goto/16 :goto_1

    .line 768
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto/16 :goto_2

    .line 777
    :cond_7
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 779
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableSave()V

    .line 780
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_2

    .line 781
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setIsEdited(Z)V

    goto/16 :goto_3

    .line 790
    :cond_8
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsMinimum:Z

    goto :goto_4

    .line 726
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 586
    const/4 v0, 0x1

    .line 602
    .local v0, "ret":Z
    return v0
.end method

.method public backPressed()V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 498
    return-void
.end method

.method public changeImage(I)V
    .locals 4
    .param p1, "trayButtonIdx"    # I

    .prologue
    const/high16 v3, 0x42c80000    # 100.0f

    .line 1511
    const-string v1, "changeImage"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 1512
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v1, :cond_0

    .line 1514
    const-string v1, "mTrayManager != null"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 1515
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1516
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1518
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v1, :cond_2

    .line 1520
    const-string v1, "mImageData null"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 1566
    :cond_1
    :goto_0
    return-void

    .line 1526
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v2

    div-float/2addr v1, v2

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v2

    div-float/2addr v1, v2

    cmpg-float v1, v1, v3

    if-gez v1, :cond_6

    .line 1527
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsMinimum:Z

    .line 1530
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 1531
    .local v0, "viewBuffer":[I
    if-eqz v0, :cond_4

    .line 1539
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_4

    .line 1541
    const-string v1, "mViewBitmap = Bitmap.createBitmap"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 1542
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1543
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->setImageDataToImageEditView(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;)V

    .line 1546
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->initEffect()V

    .line 1547
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->invalidateViewsWithThread()V

    .line 1548
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->initDialog()V

    .line 1549
    const-string v1, "invalidateViewsWithThread"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 1550
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_5

    .line 1552
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$25;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$25;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1560
    :cond_5
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

    if-nez v1, :cond_1

    .line 1562
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

    .line 1563
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;->start()V

    goto :goto_0

    .line 1529
    .end local v0    # "viewBuffer":[I
    :cond_6
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsMinimum:Z

    goto :goto_1
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    .line 171
    .local v0, "ret":I
    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    .line 176
    .local v0, "ret":I
    return v0
.end method

.method public getStatusHeight()I
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 181
    .local v0, "ret":I
    return v0
.end method

.method public initActionbar()V
    .locals 1

    .prologue
    .line 501
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->init2DepthActionBar()V

    .line 502
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 506
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 507
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 512
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 513
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 514
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 519
    :cond_2
    :goto_1
    return-void

    .line 509
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 510
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    goto :goto_0

    .line 516
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 517
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1
.end method

.method public initButtons()V
    .locals 5

    .prologue
    .line 459
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v2, :cond_1

    .line 460
    const/4 v0, 0x0

    .line 461
    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v2, :cond_1

    .line 462
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v2, :cond_0

    .line 463
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setVisibilityDirectly(I)V

    .line 464
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBottomButtonDirectly(I)V

    .line 465
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initSubViewButtons(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V

    .line 466
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->showSubBottomButton(I)V

    .line 467
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->setMainBtnListener()V

    .line 469
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 477
    .end local v0    # "i":I
    :cond_1
    return-void

    .line 471
    .restart local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 472
    .local v1, "v":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 469
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 526
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->initShareViaDialog()V

    .line 527
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->initSetAsDialog()V

    .line 528
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->initSaveOptionDialog()V

    .line 529
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->initSaveYesNoCancel()V

    .line 530
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->initSaveYesNoCancelForFinish()V

    .line 531
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->initUndoRedoAllDialog(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V

    .line 533
    :cond_0
    return-void
.end method

.method public initEffect()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

    if-nez v0, :cond_0

    .line 150
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

    .line 151
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;->start()V

    .line 153
    :cond_0
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 2301
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 1504
    return-void
.end method

.method public initTrayLayout()V
    .locals 0

    .prologue
    .line 1508
    return-void
.end method

.method public initView()V
    .locals 2

    .prologue
    .line 158
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->startImageEditViewAnimation(Landroid/view/animation/Animation;)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 167
    :cond_0
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 650
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 651
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 653
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 676
    :cond_0
    :goto_0
    return-void

    .line 658
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 662
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 663
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 664
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 665
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 666
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 669
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 608
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsDestroy:Z

    .line 610
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    .line 611
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPaint:Landroid/graphics/Paint;

    .line 612
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mClearPaint:Landroid/graphics/Paint;

    .line 613
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mLinePaint:Landroid/graphics/Paint;

    .line 614
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 615
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 616
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 617
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 618
    :cond_0
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 620
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeCancelPressState(Z)V

    .line 622
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 623
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 625
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->show()V

    .line 626
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 628
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_2

    .line 630
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->free()V

    .line 631
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 635
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 636
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 640
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

    if-eqz v0, :cond_4

    .line 641
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;->destroy()V

    .line 642
    :cond_4
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;

    .line 643
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 644
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 645
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 646
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 538
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 543
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 544
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 545
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPaint:Landroid/graphics/Paint;

    .line 542
    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 546
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 560
    :cond_0
    :goto_0
    return-void

    .line 551
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 552
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 553
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mColor:I

    .line 554
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPaint:Landroid/graphics/Paint;

    .line 555
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mClearPaint:Landroid/graphics/Paint;

    .line 556
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mLinePaint:Landroid/graphics/Paint;

    .line 557
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    move-object v0, p1

    .line 550
    invoke-static/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawCanvasWithPath(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;ILandroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 679
    const/16 v1, 0x17

    if-eq p1, v1, :cond_0

    const/16 v1, 0x42

    if-ne p1, v1, :cond_3

    .line 681
    :cond_0
    const/4 v0, 0x0

    .line 682
    .local v0, "isFocused":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_1

    .line 683
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    move-result v0

    .line 686
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_2

    .line 687
    if-nez v0, :cond_2

    .line 688
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 714
    .end local v0    # "isFocused":Z
    :cond_2
    :goto_0
    return v2

    .line 692
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_4

    .line 694
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->backPressed()V

    goto :goto_0

    .line 697
    :cond_4
    const/16 v1, 0x52

    if-ne p1, v1, :cond_2

    .line 698
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    goto :goto_0
.end method

.method public onLayout()V
    .locals 3

    .prologue
    .line 1836
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1837
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 1842
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_1

    .line 1843
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 1844
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 1845
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1846
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewHeight()I

    move-result v0

    if-eqz v0, :cond_3

    .line 1848
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_3

    .line 1850
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsDestroy:Z

    if-eqz v0, :cond_4

    .line 1859
    :cond_3
    :goto_0
    return-void

    .line 1852
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 720
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->runOptionItem(I)Z

    .line 721
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1569
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 1571
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->resume()V

    .line 1573
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 1575
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resume()V

    .line 1577
    :cond_1
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsPenTouch:Z

    .line 1583
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->msave:Z

    if-eqz v0, :cond_2

    .line 1584
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1585
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mCurrentSaveSize:I

    const v1, 0x7a1200

    if-ne v0, v1, :cond_4

    .line 1586
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cb

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setSelectedFalseAnotherButtons(I)V

    .line 1589
    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->msave:Z

    .line 1592
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1593
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 1594
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1606
    :cond_3
    return-void

    .line 1588
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601f0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setSelectedFalseAnotherButtons(I)V

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1609
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->pause()V

    .line 1610
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-nez v0, :cond_1

    .line 1621
    :cond_0
    :goto_0
    return-void

    .line 1613
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f090158

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1614
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->msetas:Z

    .line 1615
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 1617
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1618
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->msave:Z

    .line 1619
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    goto :goto_0
.end method

.method public refreshView()V
    .locals 3

    .prologue
    .line 2306
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_4

    .line 2308
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewHeight()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2309
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->getImageEditViewHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 2311
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2312
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 2313
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 2317
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2318
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 2319
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 2325
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_3

    .line 2326
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isSaved()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 2328
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->refreshImageAndBottomButtons()V

    .line 2330
    :cond_4
    return-void

    .line 2315
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    goto :goto_0

    .line 2321
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 2322
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1
.end method

.method public setConfigurationChanged()V
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 565
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 566
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 567
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 568
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_2

    .line 569
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 570
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_3

    .line 572
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 573
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 576
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->setImageEditViewPinchZoomCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;)V

    .line 577
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_4

    .line 578
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->init()V

    .line 580
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPopupLayout()V

    .line 581
    return-void
.end method
