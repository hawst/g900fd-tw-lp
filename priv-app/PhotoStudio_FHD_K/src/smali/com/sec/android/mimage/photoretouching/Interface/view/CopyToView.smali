.class public Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "CopyToView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToDecodeCallback;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$SaveAsyncTask;
    }
.end annotation


# instance fields
.field private final CHOICE_DIALOG:I

.field private final ICON_HEIGHT:I

.field private final ICON_WIDTH:I

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mBm_lrtb:Landroid/graphics/Bitmap;

.field private mBm_rotate:Landroid/graphics/Bitmap;

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mContext:Landroid/content/Context;

.field private mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

.field private mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mCopyToObjectImage:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;

.field private mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private final mKey:Ljava/lang/String;

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mPaint:Landroid/graphics/Paint;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 1162
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1163
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1164
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    .line 1165
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1166
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1167
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1168
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1169
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mPaint:Landroid/graphics/Paint;

    .line 1170
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    .line 1171
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1172
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1173
    const-string v1, "CopyToAnother"

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mKey:Ljava/lang/String;

    .line 1174
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCheckBox:Landroid/widget/CheckBox;

    .line 1176
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_rotate:Landroid/graphics/Bitmap;

    .line 1177
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1178
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToObjectImage:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;

    .line 1180
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1182
    const/16 v1, 0x17

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->ICON_WIDTH:I

    .line 1183
    const/16 v1, 0x17

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->ICON_HEIGHT:I

    .line 1184
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->CHOICE_DIALOG:I

    .line 84
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    .line 85
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 86
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 87
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 88
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 90
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->setInterface(Ljava/lang/Object;)V

    .line 92
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mPaint:Landroid/graphics/Paint;

    .line 93
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 95
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 96
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 98
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0202b6

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 99
    .local v9, "bm_lrtb":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020166

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 100
    .local v10, "bm_rotate":Landroid/graphics/Bitmap;
    const/high16 v1, 0x41b80000    # 23.0f

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-int v12, v1

    .line 101
    .local v12, "dstWidth":I
    const/high16 v1, 0x41b80000    # 23.0f

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-int v11, v1

    .line 102
    .local v11, "dstHeight":I
    const/4 v1, 0x1

    invoke-static {v9, v12, v11, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    .line 103
    const/4 v1, 0x1

    invoke-static {v10, v12, v11, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_rotate:Landroid/graphics/Bitmap;

    .line 104
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 105
    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 108
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 109
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 110
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v2

    .line 111
    const/4 v3, 0x0

    .line 112
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 113
    const/4 v5, 0x0

    .line 114
    const/4 v6, 0x0

    .line 115
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    .line 116
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v8

    .line 110
    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 117
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->setViewLayerType(I)V

    .line 118
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;
    .locals 1

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 1171
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 0

    .prologue
    .line 1165
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 1168
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 1174
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCheckBox:Landroid/widget/CheckBox;

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 1174
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 1167
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 1165
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1164
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 1163
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 1163
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 1166
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 1180
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 1180
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method private drawRectBdry(Landroid/graphics/Canvas;Landroid/graphics/Matrix;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "viewTransformMatrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 819
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    if-nez v0, :cond_0

    .line 905
    :goto_0
    return-void

    .line 821
    :cond_0
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 823
    .local v5, "paint":Landroid/graphics/Paint;
    const v0, -0xeea080

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 824
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 826
    const/4 v0, 0x2

    new-array v8, v0, [F

    .line 827
    .local v8, "point":[F
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->getDrawCenterPt()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->x:F

    aput v1, v8, v0

    .line 828
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->getDrawCenterPt()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->y:F

    aput v1, v8, v0

    .line 829
    invoke-virtual {p2, v8}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 831
    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    .line 832
    .local v6, "drawRoi":Landroid/graphics/RectF;
    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10}, Landroid/graphics/RectF;-><init>()V

    .line 833
    .local v10, "src":Landroid/graphics/RectF;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->getDrawBdry()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 834
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "drawRectBdry src:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 835
    invoke-virtual {p2, v6, v10}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 837
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    if-eqz v0, :cond_2

    .line 838
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 840
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 841
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->getAngle()I

    move-result v0

    int-to-float v0, v0

    const/4 v1, 0x0

    aget v1, v8, v1

    const/4 v2, 0x1

    aget v2, v8, v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 843
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    .line 844
    .local v11, "width":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 845
    .local v7, "height":I
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 846
    .local v9, "s":Landroid/graphics/Rect;
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v9, v0, v1, v11, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 847
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "drawRectBdry drawRoi:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 848
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 849
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v9, v6, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 873
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 876
    .end local v7    # "height":I
    .end local v9    # "s":Landroid/graphics/Rect;
    .end local v11    # "width":I
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 877
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->getAngle()I

    move-result v0

    int-to-float v0, v0

    const/4 v1, 0x0

    aget v1, v8, v1

    const/4 v2, 0x1

    aget v2, v8, v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 879
    iget v0, v6, Landroid/graphics/RectF;->left:F

    const/high16 v1, 0x40000000    # 2.0f

    sub-float v1, v0, v1

    iget v0, v6, Landroid/graphics/RectF;->top:F

    const/high16 v2, 0x40000000    # 2.0f

    sub-float v2, v0, v2

    .line 880
    iget v0, v6, Landroid/graphics/RectF;->right:F

    const/high16 v3, 0x40000000    # 2.0f

    add-float/2addr v3, v0

    iget v0, v6, Landroid/graphics/RectF;->top:F

    const/high16 v4, 0x40000000    # 2.0f

    add-float/2addr v4, v0

    move-object v0, p1

    .line 879
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 881
    iget v0, v6, Landroid/graphics/RectF;->right:F

    const/high16 v1, 0x40000000    # 2.0f

    sub-float v1, v0, v1

    iget v0, v6, Landroid/graphics/RectF;->top:F

    const/high16 v2, 0x40000000    # 2.0f

    sub-float v2, v0, v2

    .line 882
    iget v0, v6, Landroid/graphics/RectF;->right:F

    const/high16 v3, 0x40000000    # 2.0f

    add-float/2addr v3, v0

    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x40000000    # 2.0f

    add-float/2addr v4, v0

    move-object v0, p1

    .line 881
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 883
    iget v0, v6, Landroid/graphics/RectF;->left:F

    const/high16 v1, 0x40000000    # 2.0f

    sub-float v1, v0, v1

    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    const/high16 v2, 0x40000000    # 2.0f

    sub-float v2, v0, v2

    .line 884
    iget v0, v6, Landroid/graphics/RectF;->right:F

    const/high16 v3, 0x40000000    # 2.0f

    add-float/2addr v3, v0

    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x40000000    # 2.0f

    add-float/2addr v4, v0

    move-object v0, p1

    .line 883
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 885
    iget v0, v6, Landroid/graphics/RectF;->left:F

    const/high16 v1, 0x40000000    # 2.0f

    sub-float v1, v0, v1

    iget v0, v6, Landroid/graphics/RectF;->top:F

    const/high16 v2, 0x40000000    # 2.0f

    sub-float v2, v0, v2

    .line 886
    iget v0, v6, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    add-float/2addr v3, v0

    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x40000000    # 2.0f

    add-float/2addr v4, v0

    move-object v0, p1

    .line 885
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 888
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, v6, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 889
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 890
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 891
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_rotate:Landroid/graphics/Bitmap;

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, v6, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 893
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 894
    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget v3, v6, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    .line 893
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 895
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 896
    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget v3, v6, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    .line 895
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 897
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    .line 898
    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v2, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v2, v6, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 899
    iget v2, v6, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    .line 897
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 900
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v2, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget v2, v6, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 901
    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    .line 900
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 903
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method

.method private initChoiceDialog()V
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 729
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 730
    const/4 v2, 0x1

    .line 731
    const v3, 0x7f0600de

    .line 734
    const v6, 0x103012e

    .line 729
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 735
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 736
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v7, 0x110

    .line 737
    const v8, 0x14001407

    .line 739
    const v10, 0x7f06001e

    .line 741
    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$11;

    invoke-direct {v12, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    move v9, v4

    move-object v11, v5

    .line 736
    invoke-virtual/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 764
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v7, 0x120

    .line 767
    const v10, 0x7f06001f

    .line 769
    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$12;

    invoke-direct {v12, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    move v8, v4

    move v9, v4

    move-object v11, v5

    .line 764
    invoke-virtual/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 793
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 794
    return-void
.end method

.method private initCopyToAnotherNotiDialog()V
    .locals 7

    .prologue
    .line 561
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 562
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 563
    const v2, 0x1d001702

    .line 564
    const v3, 0x7f060065

    .line 565
    const/4 v4, 0x1

    .line 566
    const/4 v5, 0x0

    .line 567
    const v6, 0x103012e

    .line 562
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 568
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 570
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x300

    .line 571
    const v2, 0x7f060082

    .line 572
    const-string v3, "CopyToAnother"

    .line 573
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$7;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    .line 570
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 582
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 583
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$8;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    .line 582
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 609
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 611
    return-void
.end method

.method private initSaveAsDialog()V
    .locals 14

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 614
    const/4 v10, 0x0

    .line 615
    .local v10, "fileName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v13

    .line 616
    .local v13, "path":Ljava/lang/String;
    if-eqz v13, :cond_0

    .line 618
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 619
    if-nez v10, :cond_0

    .line 621
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 625
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 626
    const v2, 0x7f090156

    .line 627
    const v3, 0x7f0601cd

    .line 630
    const v6, 0x103012e

    .line 625
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 632
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 633
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v7, 0x600

    move v8, v4

    move v9, v4

    move-object v11, v5

    move-object v12, v5

    invoke-virtual/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIILjava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 640
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000a

    .line 641
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    .line 640
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V

    .line 709
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 710
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$10;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    .line 709
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 724
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 726
    return-void
.end method

.method private makeViewMaskBuff()V
    .locals 10

    .prologue
    .line 908
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 909
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 910
    .local v1, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 911
    const/4 v1, 0x0

    .line 913
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 914
    .local v4, "scaledViewMask":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 916
    .local v5, "scaledViewMaskCanvas":Landroid/graphics/Canvas;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    .line 917
    .local v2, "matrix":Landroid/graphics/Matrix;
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 918
    .local v3, "paint":Landroid/graphics/Paint;
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 919
    invoke-virtual {v5, v0, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 920
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 921
    const/4 v0, 0x0

    .line 923
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskBuffer()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 924
    .local v6, "viewMask":Ljava/nio/ByteBuffer;
    invoke-virtual {v4, v6}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 925
    const/4 v6, 0x0

    .line 926
    return-void
.end method

.method private newCopyToImageData(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;II)V
    .locals 7
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .param p2, "viewWidth"    # I
    .param p3, "viewHeight"    # I

    .prologue
    const/4 v6, 0x1

    .line 797
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 798
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "obj Mask : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 800
    iget-boolean v0, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    if-eqz v0, :cond_0

    .line 802
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    .line 803
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    .line 804
    iget-object v3, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 802
    invoke-virtual {v0, v1, v2, v3, v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setDecodedImage(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/net/Uri;I)V

    .line 817
    :goto_0
    return-void

    .line 809
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    .line 810
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    .line 811
    iget-object v3, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 812
    iget-boolean v4, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isCamera:Z

    .line 813
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->fileName:Ljava/lang/String;

    .line 809
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setDecodedImage(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/String;ZLjava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 347
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->transformVirtualPreviewCoordinate(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)V

    .line 348
    const/4 v0, 0x1

    .line 349
    .local v0, "ret":Z
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 371
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->invalidateViews()V

    .line 372
    return v0

    .line 352
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v1, :cond_0

    .line 353
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 354
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 353
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->InitMoveObject(FF)I

    goto :goto_0

    .line 356
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 357
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 356
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->InitMoveObject(FF)I

    goto :goto_0

    .line 360
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v1, :cond_1

    .line 361
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 362
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 361
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->StartMoveObject(FF)V

    goto :goto_0

    .line 364
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 365
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 364
    invoke-virtual {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->StartMoveObject(FF)V

    goto/16 :goto_0

    .line 368
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->EndMoveObject()V

    goto/16 :goto_0

    .line 349
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public backPressed()V
    .locals 3

    .prologue
    .line 228
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledSave()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 230
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;)V

    .line 231
    .local v0, "task":Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 237
    .end local v0    # "task":Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 534
    return-void
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 546
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getActionbarHeight()I

    move-result v0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonHeight()I

    move-result v0

    return v0
.end method

.method public getStatusHeight()I
    .locals 3

    .prologue
    .line 554
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 555
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 556
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 557
    iget v2, v0, Landroid/graphics/Rect;->top:I

    return v2
.end method

.method public initActionbar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 240
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 245
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$4;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    .line 242
    invoke-virtual {v0, v4, v2, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 267
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$5;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    .line 264
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 286
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x5

    .line 289
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    .line 286
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 310
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableSave()V

    .line 313
    :cond_0
    return-void
.end method

.method public initButtons()V
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1d001703

    .line 140
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$1;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    .line 139
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1d001701

    .line 160
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$2;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    .line 159
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1d001702

    .line 186
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$3;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    .line 185
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 224
    return-void
.end method

.method public initDialog()V
    .locals 0

    .prologue
    .line 317
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->initCopyToAnotherNotiDialog()V

    .line 318
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->initSaveAsDialog()V

    .line 319
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->initChoiceDialog()V

    .line 320
    return-void
.end method

.method public initEffect()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 122
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->makeViewMaskBuff()V

    .line 124
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToObjectImage:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;

    .line 125
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 126
    .local v2, "objROI":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getoriginalToPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 127
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToObjectImage:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;

    const/4 v7, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;-><init>(Landroid/content/Context;Landroid/graphics/Rect;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;IZZ)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    .line 128
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 332
    return-void
.end method

.method public initSubView()V
    .locals 14

    .prologue
    const v1, 0x1d001702

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 467
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->destroy()V

    .line 468
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    .line 469
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 471
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v1, v8}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 475
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 476
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 477
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 479
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 482
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 483
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 477
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 485
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setCopyToDrawCanvasRoi(Landroid/graphics/Rect;)V

    .line 486
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 487
    .local v5, "objROI":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 489
    iget v0, v5, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, v5, Landroid/graphics/Rect;->left:I

    .line 490
    iget v0, v5, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, v5, Landroid/graphics/Rect;->top:I

    .line 491
    iget v0, v5, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, v5, Landroid/graphics/Rect;->right:I

    .line 492
    iget v0, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, v5, Landroid/graphics/Rect;->bottom:I

    .line 494
    iget v0, v5, Landroid/graphics/Rect;->right:I

    iget v1, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->right:I

    .line 495
    iget v0, v5, Landroid/graphics/Rect;->left:I

    iget v1, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->left:I

    .line 496
    iget v0, v5, Landroid/graphics/Rect;->bottom:I

    iget v1, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->bottom:I

    .line 497
    iget v0, v5, Landroid/graphics/Rect;->top:I

    iget v1, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->top:I

    .line 499
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToObjectImage:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;

    move v9, v8

    move v10, v2

    invoke-direct/range {v3 .. v10}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;-><init>(Landroid/content/Context;Landroid/graphics/Rect;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;IZZ)V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    .line 523
    :goto_0
    return-void

    .line 503
    .end local v5    # "objROI":Landroid/graphics/Rect;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1d001701

    invoke-virtual {v0, v1, v8}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 504
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_1

    .line 505
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->destroy()V

    .line 506
    :cond_1
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 507
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setCopyToDrawCanvasRoi(Landroid/graphics/Rect;)V

    .line 509
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 510
    .restart local v5    # "objROI":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 511
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToObjectImage:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;

    move v9, v8

    move v10, v2

    invoke-direct/range {v3 .. v10}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;-><init>(Landroid/content/Context;Landroid/graphics/Rect;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;IZZ)V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    .line 513
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 514
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 515
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v7

    .line 517
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v9

    .line 520
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v12

    .line 521
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v13

    move v8, v2

    move v10, v2

    move v11, v2

    .line 515
    invoke-virtual/range {v6 .. v13}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    goto :goto_0
.end method

.method public initTrayLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 526
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 530
    :cond_0
    return-void
.end method

.method public initView()V
    .locals 0

    .prologue
    .line 133
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 415
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 416
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledSave()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 430
    :goto_0
    return-void

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_1

    .line 424
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 425
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 426
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 428
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 377
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->destroy()V

    .line 379
    :cond_0
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    .line 381
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToObjectImage:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;

    if-eqz v0, :cond_1

    .line 382
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToObjectImage:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->destroy()V

    .line 384
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_2

    .line 385
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->destroy()V

    .line 386
    :cond_2
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 388
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;

    .line 389
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 390
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mPaint:Landroid/graphics/Paint;

    .line 392
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 394
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeCancelPressState(Z)V

    .line 396
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 397
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 398
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 400
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 401
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 402
    :cond_3
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_lrtb:Landroid/graphics/Bitmap;

    .line 404
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_rotate:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 405
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 406
    :cond_4
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mBm_rotate:Landroid/graphics/Bitmap;

    .line 408
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 410
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 411
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mPaint:Landroid/graphics/Paint;

    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->drawRectBdry(Landroid/graphics/Canvas;Landroid/graphics/Matrix;)V

    .line 326
    return-void
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 433
    const/16 v0, 0x17

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-ne p1, v0, :cond_3

    .line 435
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 436
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    .line 439
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_2

    .line 440
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 462
    :cond_2
    :goto_0
    return v1

    .line 444
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 446
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->backPressed()V

    goto :goto_0
.end method

.method public onLayout()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1123
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 1124
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1128
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1129
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_1

    .line 1135
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1136
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1137
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 1139
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1142
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1143
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 1137
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1156
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    if-eqz v0, :cond_0

    .line 1158
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->configurationChanged()V

    .line 1160
    :cond_0
    return-void

    .line 1147
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1148
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 1150
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1153
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1154
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 1148
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 542
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 538
    return-void
.end method

.method public refreshView()V
    .locals 0

    .prologue
    .line 1189
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 338
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 339
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 340
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 342
    return-void
.end method
