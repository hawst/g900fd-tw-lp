.class Lcom/sec/android/mimage/photoretouching/PhotoRetouching$4;
.super Ljava/lang/Object;
.source "PhotoRetouching.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getImage(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$4;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .line 422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$4;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching;
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$4;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    return-object v0
.end method


# virtual methods
.method public changeViewStatus(I)V
    .locals 4
    .param p1, "viewStatus"    # I

    .prologue
    .line 427
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$4$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$4$1;-><init>(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$4;I)V

    .line 436
    const-wide/16 v2, 0x14

    .line 427
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 437
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$4;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    # getter for: Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->access$7(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$4;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    # getter for: Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mDecodeAsync:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->access$7(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->setChangeViewCallback(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;)V

    .line 439
    :cond_0
    return-void
.end method

.method public invalidateViews()V
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$4;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->invalidateViews()V

    .line 444
    return-void
.end method
