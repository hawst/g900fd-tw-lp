.class Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$1;
.super Ljava/lang/Object;
.source "TrayManager.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$1;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 115
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 85
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayTouchFunction:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 87
    const/4 v1, -0x1

    .line 88
    .local v1, "idx":I
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 89
    .local v2, "parent":Landroid/view/ViewGroup;
    if-eqz v2, :cond_0

    .line 91
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 92
    .local v0, "grandParent":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 94
    .end local v0    # "grandParent":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayTouchFunction:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    move-result-object v4

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 95
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$1$1;

    invoke-direct {v5, p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$1$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$1;Landroid/view/View;)V

    .line 94
    invoke-interface {v4, v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;->deleteButtonTouch(Landroid/view/View;Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$TrayDeleteFunction;)Z

    .line 103
    .end local v1    # "idx":I
    .end local v2    # "parent":Landroid/view/ViewGroup;
    :cond_1
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 109
    return-void
.end method
