.class Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;
.super Ljava/lang/Thread;
.source "ImageData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Core/ImageData;->loadTextures(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->val$context:Landroid/content/Context;

    .line 1007
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 23

    .prologue
    .line 1010
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v16, v0

    monitor-enter v16

    .line 1012
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->val$context:Landroid/content/Context;

    if-nez v15, :cond_0

    .line 1013
    monitor-exit v16

    .line 1059
    :goto_0
    return-void

    .line 1015
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 1016
    .local v12, "time":J
    const/4 v9, 0x0

    .local v9, "resizeWidth":I
    const/4 v8, 0x0

    .line 1017
    .local v8, "resizeHeight":I
    const/4 v15, 0x1

    const/high16 v17, 0x42960000    # 75.0f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->val$context:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v18

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v15, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    .line 1018
    .local v7, "px":F
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v14

    .local v14, "width":I
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1019
    .local v2, "height":I
    const/high16 v10, 0x3f800000    # 1.0f

    .line 1020
    .local v10, "scale":F
    int-to-float v15, v14

    int-to-float v0, v2

    move/from16 v17, v0

    div-float v11, v15, v17

    .line 1021
    .local v11, "viewRatio":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    .line 1022
    .local v5, "imageWidth":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    .line 1023
    .local v4, "imageHeight":I
    int-to-float v15, v5

    int-to-float v0, v4

    move/from16 v17, v0

    div-float v6, v15, v17

    .line 1024
    .local v6, "imgRatio":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v15

    int-to-float v15, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v17

    div-float v15, v15, v17

    const/high16 v17, 0x42c80000    # 100.0f

    cmpg-float v15, v15, v17

    if-ltz v15, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v15

    int-to-float v15, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v17

    div-float v15, v15, v17

    const/high16 v17, 0x42c80000    # 100.0f

    cmpg-float v15, v15, v17

    if-gez v15, :cond_2

    .line 1025
    :cond_1
    monitor-exit v16

    goto :goto_0

    .line 1010
    .end local v2    # "height":I
    .end local v4    # "imageHeight":I
    .end local v5    # "imageWidth":I
    .end local v6    # "imgRatio":F
    .end local v7    # "px":F
    .end local v8    # "resizeHeight":I
    .end local v9    # "resizeWidth":I
    .end local v10    # "scale":F
    .end local v11    # "viewRatio":F
    .end local v12    # "time":J
    .end local v14    # "width":I
    :catchall_0
    move-exception v15

    monitor-exit v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v15

    .line 1029
    .restart local v2    # "height":I
    .restart local v4    # "imageHeight":I
    .restart local v5    # "imageWidth":I
    .restart local v6    # "imgRatio":F
    .restart local v7    # "px":F
    .restart local v8    # "resizeHeight":I
    .restart local v9    # "resizeWidth":I
    .restart local v10    # "scale":F
    .restart local v11    # "viewRatio":F
    .restart local v12    # "time":J
    .restart local v14    # "width":I
    :cond_2
    cmpl-float v15, v11, v6

    if-lez v15, :cond_5

    .line 1031
    int-to-float v15, v14

    int-to-float v0, v5

    move/from16 v17, v0

    div-float v10, v15, v17

    .line 1032
    int-to-float v15, v4

    mul-float/2addr v15, v10

    const/high16 v17, 0x3f000000    # 0.5f

    add-float v15, v15, v17

    float-to-int v8, v15

    .line 1033
    move v9, v14

    .line 1041
    :goto_1
    mul-int v15, v9, v8

    const/high16 v17, 0x80000

    move/from16 v0, v17

    if-le v15, v0, :cond_3

    .line 1042
    const/high16 v15, 0x49000000    # 524288.0f

    mul-int v17, v5, v4

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    div-float v15, v15, v17

    float-to-double v0, v15

    move-wide/from16 v18, v0

    :try_start_1
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v10, v0

    .line 1043
    int-to-float v15, v5

    mul-float/2addr v15, v10

    const/high16 v17, 0x3f000000    # 0.5f

    add-float v15, v15, v17

    float-to-int v9, v15

    .line 1044
    int-to-float v15, v4

    mul-float/2addr v15, v10

    const/high16 v17, 0x3f000000    # 0.5f

    add-float v15, v15, v17

    float-to-int v8, v15

    .line 1046
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mTextureData:Ljava/util/HashMap;
    invoke-static {v15}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)Ljava/util/HashMap;

    move-result-object v15

    if-eqz v15, :cond_4

    .line 1047
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mTextureData:Ljava/util/HashMap;
    invoke-static {v15}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)Ljava/util/HashMap;

    move-result-object v15

    invoke-virtual {v15}, Ljava/util/HashMap;->clear()V

    .line 1049
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    new-instance v17, Ljava/util/HashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Ljava/util/HashMap;)V

    .line 1051
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mTextureIds:[I
    invoke-static {v15}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->access$2(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)[I

    move-result-object v17

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    const/4 v15, 0x0

    :goto_2
    move/from16 v0, v18

    if-lt v15, v0, :cond_6

    .line 1010
    monitor-exit v16

    goto/16 :goto_0

    .line 1037
    :cond_5
    int-to-float v15, v2

    int-to-float v0, v4

    move/from16 v17, v0

    div-float v10, v15, v17

    .line 1038
    int-to-float v15, v5

    mul-float/2addr v15, v10

    const/high16 v17, 0x3f000000    # 0.5f

    add-float v15, v15, v17

    float-to-int v9, v15

    .line 1039
    move v8, v2

    goto :goto_1

    .line 1051
    :cond_6
    aget v3, v17, v15

    .line 1052
    .local v3, "id":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mTextureData:Ljava/util/HashMap;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)Ljava/util/HashMap;

    move-result-object v19

    if-eqz v19, :cond_7

    .line 1053
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mTextureData:Ljava/util/HashMap;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)Ljava/util/HashMap;

    move-result-object v19

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->val$context:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v3, v9, v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getTexture(Landroid/content/Context;III)[I

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1051
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 1055
    :cond_7
    monitor-exit v16
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method
