.class Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$14;
.super Ljava/lang/Object;
.source "TrayManager.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initBodyListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private final synthetic val$scroll:Landroid/widget/ScrollView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Landroid/widget/ScrollView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$14;->val$scroll:Landroid/widget/ScrollView;

    .line 1042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1047
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$14;->val$scroll:Landroid/widget/ScrollView;

    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 1048
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1050
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 1061
    .end local v1    # "i":I
    :cond_0
    return v3

    .line 1052
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 1053
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    move-result-object v2

    if-eq v0, v2, :cond_3

    .line 1055
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->isDisabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1056
    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setEnabled(Z)V

    .line 1057
    :cond_2
    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setPressed(Z)V

    .line 1050
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
