.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;
.super Landroid/os/AsyncTask;
.source "PenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DoDoneAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 2152
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 2156
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->doDone()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$14(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 2157
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 2169
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2171
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$16(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2174
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$16(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2181
    :cond_0
    :goto_0
    const/high16 v0, 0x31000000

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentMode(I)V

    .line 2182
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->finish()V

    .line 2183
    return-void

    .line 2176
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 2160
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 2161
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$15(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/app/ProgressDialog;)V

    .line 2162
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$16(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    const v2, 0x7f06008f

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2163
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$16(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 2164
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$16(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 2166
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$16(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 2167
    return-void
.end method
