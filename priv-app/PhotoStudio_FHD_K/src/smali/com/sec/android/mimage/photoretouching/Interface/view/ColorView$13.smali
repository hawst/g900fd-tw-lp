.class Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;
.super Ljava/lang/Object;
.source "ColorView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->init2DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    .line 884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 4
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 937
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 938
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewWidth()I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$37(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewHeight()I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$38(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->redoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 939
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$26(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Landroid/graphics/Bitmap;)V

    .line 940
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v7, 0x15001506

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 887
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewWidth()I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$37(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->getImageEditViewHeight()I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$38(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->redo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    .line 888
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 890
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 891
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 892
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableSave()V

    .line 894
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 895
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1, v5, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 899
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    invoke-static {v1, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$39(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Z)V

    .line 909
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 911
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 912
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 920
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->refreshImageAndBottomButtons()V
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$40(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V

    .line 921
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getEnhance()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 922
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getID(I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 923
    .local v0, "btn":Landroid/widget/LinearLayout;
    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 924
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 933
    :goto_3
    return-void

    .line 897
    .end local v0    # "btn":Landroid/widget/LinearLayout;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_0

    .line 904
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 905
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    .line 906
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 907
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1, v5, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto/16 :goto_1

    .line 916
    :cond_5
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 917
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_2

    .line 928
    :cond_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getID(I)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 929
    .restart local v0    # "btn":Landroid/widget/LinearLayout;
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 930
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_3
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 945
    return-void
.end method
