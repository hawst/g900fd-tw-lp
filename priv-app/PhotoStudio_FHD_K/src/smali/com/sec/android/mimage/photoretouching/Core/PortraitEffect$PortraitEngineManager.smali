.class Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;
.super Ljava/lang/Object;
.source "PortraitEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PortraitEngineManager"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316
    return-void
.end method


# virtual methods
.method public declared-synchronized applyPreview(I)I
    .locals 14
    .param p1, "step"    # I

    .prologue
    .line 381
    monitor-enter p0

    const/4 v10, -0x1

    .line 383
    .local v10, "ret":I
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 385
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-static {v4, p1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$1(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;I)V

    .line 387
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 388
    .local v0, "previewInput":[I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v3

    .line 390
    .local v3, "previewOutput":[I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    .line 391
    .local v1, "previewWidth":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    .line 395
    .local v2, "previewHeight":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mEffectType:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$2(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 426
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-static {v4, v3}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$4(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;[I)V

    .line 427
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$5()Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;->afterApplyPreview()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    .end local v0    # "previewInput":[I
    .end local v1    # "previewWidth":I
    .end local v2    # "previewHeight":I
    .end local v3    # "previewOutput":[I
    :cond_1
    monitor-exit p0

    return v10

    .line 399
    .restart local v0    # "previewInput":[I
    .restart local v1    # "previewWidth":I
    .restart local v2    # "previewHeight":I
    .restart local v3    # "previewOutput":[I
    :pswitch_1
    :try_start_1
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->getportraitstatus()I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_2

    .line 400
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->initPortraitEngine(Z)V

    .line 401
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mStep:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$3(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)I

    move-result v6

    move v4, v1

    move v5, v2

    invoke-static/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runLightenInPortraitPreview([III[IIII)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 381
    .end local v0    # "previewInput":[I
    .end local v1    # "previewWidth":I
    .end local v2    # "previewHeight":I
    .end local v3    # "previewOutput":[I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 404
    .restart local v0    # "previewInput":[I
    .restart local v1    # "previewWidth":I
    .restart local v2    # "previewHeight":I
    .restart local v3    # "previewOutput":[I
    :pswitch_2
    :try_start_2
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->getportraitstatus()I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_3

    .line 405
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->initPortraitEngine(Z)V

    .line 406
    :cond_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mStep:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$3(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)I

    move-result v6

    move v4, v1

    move v5, v2

    invoke-static/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runBeautyInPortraitPreview([III[IIII)I

    goto :goto_0

    .line 409
    :pswitch_3
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->getportraitstatus()I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_4

    .line 410
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->initPortraitEngine(Z)V

    .line 411
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 412
    .local v12, "st":J
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mStep:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$3(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)I

    move-result v6

    move v4, v1

    move v5, v2

    invoke-static/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runOutfocusInPortraitPreview([III[IIII)I

    .line 413
    const-string v4, "hanyoung "

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 414
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v4

    sub-long v8, v4, v12

    .line 415
    .local v8, "elapsedTime":J
    :goto_1
    const-wide/16 v4, 0x32

    cmp-long v4, v8, v4

    if-gez v4, :cond_0

    .line 417
    const-wide/16 v4, 0x32

    :try_start_3
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 422
    :goto_2
    :try_start_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v8, v4, v12

    goto :goto_1

    .line 418
    :catch_0
    move-exception v7

    .line 420
    .local v7, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v7}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 395
    nop

    :pswitch_data_0
    .packed-switch 0x17001700
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public finishPortraitEngine()V
    .locals 1

    .prologue
    .line 434
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->getportraitstatus()I

    move-result v0

    if-lez v0, :cond_0

    .line 435
    const-string v0, "hanyoung finishPortraitEngine main"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 436
    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->finishPortrait()V

    .line 437
    return-void
.end method

.method public initPortraitEngine(Z)V
    .locals 8
    .param p1, "forceInitPortraitEngine"    # Z

    .prologue
    .line 320
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v3

    .line 321
    .local v3, "previewInput":[I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 322
    .local v4, "previewWidth":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    .line 324
    .local v5, "previewHeight":I
    if-eqz p1, :cond_1

    .line 329
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 333
    const/16 v0, 0xa

    const/4 v1, 0x3

    const/4 v2, 0x2

    :try_start_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->getFaceInfo()Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->initPortrait(III[IIILcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 335
    :catch_0
    move-exception v7

    .line 337
    .local v7, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 343
    .end local v7    # "e":Ljava/lang/NullPointerException;
    :cond_1
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->getportraitstatus()I

    move-result v0

    if-nez v0, :cond_0

    .line 346
    const/16 v0, 0xa

    const/4 v1, 0x3

    const/4 v2, 0x2

    :try_start_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->getFaceInfo()Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->initPortrait(III[IIILcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 349
    :catch_1
    move-exception v7

    .line 351
    .restart local v7    # "e":Ljava/lang/NullPointerException;
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
