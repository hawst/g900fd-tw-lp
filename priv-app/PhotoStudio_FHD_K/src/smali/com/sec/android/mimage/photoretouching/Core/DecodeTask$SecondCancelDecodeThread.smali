.class Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;
.super Ljava/lang/Thread;
.source "DecodeTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SecondCancelDecodeThread"
.end annotation


# instance fields
.field private exitThread:Z

.field mQuramOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

.field optsResolution:Landroid/graphics/BitmapFactory$Options;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/BitmapFactory$Options;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V
    .locals 1
    .param p2, "opts"    # Landroid/graphics/BitmapFactory$Options;
    .param p3, "quramoptions"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .prologue
    const/4 v0, 0x0

    .line 746
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    .line 745
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 739
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    .line 740
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->mQuramOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .line 741
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->exitThread:Z

    .line 747
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    .line 748
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->mQuramOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .line 749
    return-void
.end method


# virtual methods
.method public exit()V
    .locals 1

    .prologue
    .line 743
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->exitThread:Z

    .line 744
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    const/16 v5, 0x258

    const/4 v4, 0x0

    .line 751
    const/4 v1, 0x0

    .line 755
    .local v1, "sleepCoount":I
    :cond_0
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 760
    :goto_0
    add-int/lit8 v1, v1, 0x1

    .line 761
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState2:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$19(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-nez v2, :cond_1

    if-lt v1, v5, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$8(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->exitThread:Z

    if-eqz v2, :cond_0

    .line 762
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState2:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$19(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-nez v2, :cond_6

    if-ne v1, v5, :cond_6

    .line 764
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$20(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 765
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mSkiaMode:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$2(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 767
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    invoke-virtual {v2}, Landroid/graphics/BitmapFactory$Options;->requestCancelDecode()V

    .line 773
    :goto_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    .line 787
    :cond_4
    :goto_2
    return-void

    .line 756
    :catch_0
    move-exception v0

    .line 758
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 771
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->mQuramOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-static {v2}, Lcom/quramsoft/qrb/QuramBitmapFactory;->cancelDecode(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V

    goto :goto_1

    .line 775
    :cond_6
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$8(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 777
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$20(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 778
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mSkiaMode:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$2(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 780
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    invoke-virtual {v2}, Landroid/graphics/BitmapFactory$Options;->requestCancelDecode()V

    goto :goto_2

    .line 782
    :cond_7
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->exitThread:Z

    if-nez v2, :cond_4

    .line 784
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->mQuramOptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-static {v2}, Lcom/quramsoft/qrb/QuramBitmapFactory;->cancelDecode(Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V

    goto :goto_2
.end method
