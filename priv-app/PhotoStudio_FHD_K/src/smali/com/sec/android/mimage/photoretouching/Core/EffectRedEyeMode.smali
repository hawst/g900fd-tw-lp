.class public Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;
.super Lcom/sec/android/mimage/photoretouching/Core/EffectInfoInterface;
.source "EffectRedEyeMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;,
        Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEffectType:I

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mRedEyeCircle:Landroid/graphics/Bitmap;

.field private mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

.field private mStartInitializing:Z

.field private mutexOn:Z

.field private myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

.field private myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onCallback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    .param p3, "onActionbarCallback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;
    .param p4, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfoInterface;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 397
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mEffectType:I

    .line 399
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mStartInitializing:Z

    .line 400
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mutexOn:Z

    .line 402
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mContext:Landroid/content/Context;

    .line 403
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .line 404
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .line 406
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeCircle:Landroid/graphics/Bitmap;

    .line 408
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    .line 410
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 28
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .line 29
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .line 30
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 31
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mContext:Landroid/content/Context;

    .line 32
    return-void
.end method

.method private ableDrawFixedRedEyePosition(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 311
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iput p1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mPosX:F

    .line 314
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mPosY:F

    .line 315
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mFixedRedEye:Z

    .line 316
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->invalidateWithThread()V

    .line 319
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;FF)V
    .locals 0

    .prologue
    .line 309
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->ableDrawFixedRedEyePosition(FF)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)V
    .locals 0

    .prologue
    .line 372
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->disableDrawFrixdRedEyePosition()V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method private applyRedEyePreview(Landroid/view/MotionEvent;[I)V
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "output"    # [I

    .prologue
    const/4 v9, 0x1

    .line 160
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 161
    .local v4, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 162
    .local v5, "y":F
    const/4 v3, 0x0

    .line 163
    .local v3, "t":Ljava/lang/Thread;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v6

    float-to-int v7, v4

    float-to-int v8, v5

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 165
    invoke-direct {p0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->getRealPosition(FF)Landroid/graphics/Point;

    move-result-object v1

    .line 166
    .local v1, "point":Landroid/graphics/Point;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    add-int/2addr v6, v7

    shr-int/lit8 v2, v6, 0x5

    .line 168
    .local v2, "radius":I
    const/16 v6, 0x50

    if-le v2, v6, :cond_0

    .line 169
    const/16 v2, 0x50

    .line 170
    :cond_0
    const/16 v6, 0xa

    if-ge v2, v6, :cond_1

    .line 171
    const/16 v2, 0xa

    .line 173
    :cond_1
    move v0, v2

    .line 175
    .local v0, "fradius":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 241
    .end local v0    # "fradius":I
    .end local v1    # "point":Landroid/graphics/Point;
    .end local v2    # "radius":I
    :cond_2
    :goto_0
    return-void

    .line 181
    .restart local v0    # "fradius":I
    .restart local v1    # "point":Landroid/graphics/Point;
    .restart local v2    # "radius":I
    :pswitch_0
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mutexOn:Z

    .line 182
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;

    .end local v3    # "t":Ljava/lang/Thread;
    invoke-direct {v3, p0, v1, v0, p2}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$2;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;Landroid/graphics/Point;I[I)V

    .line 208
    .restart local v3    # "t":Ljava/lang/Thread;
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 212
    :pswitch_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mFixedRedEyeThread:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;

    if-nez v6, :cond_3

    .line 214
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    new-instance v7, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;

    invoke-direct {v7, p0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;FF)V

    iput-object v7, v6, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mFixedRedEyeThread:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;

    .line 215
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mFixedRedEyeThread:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->start()V

    .line 221
    :goto_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v6, :cond_2

    .line 223
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v6, v9}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    goto :goto_0

    .line 219
    :cond_3
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mFixedRedEyeThread:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;

    invoke-virtual {v6, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$FixedRedEyeThread;->resetPosition(FF)V

    goto :goto_1

    .line 230
    .end local v0    # "fradius":I
    .end local v1    # "point":Landroid/graphics/Point;
    .end local v2    # "radius":I
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    :pswitch_2
    goto :goto_0

    .line 234
    :pswitch_3
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v6, :cond_2

    .line 236
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v6, v9}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    goto :goto_0

    .line 175
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 230
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private disableDrawFrixdRedEyePosition()V
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mFixedRedEye:Z

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    if-eqz v0, :cond_1

    .line 377
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->invalidateWithThread()V

    .line 378
    :cond_1
    return-void
.end method

.method private drawFixedRedEye(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeCircle:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mFixedRedEye:Z

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeCircle:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget v1, v1, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mPosX:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeCircle:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget v2, v2, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mPosY:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeCircle:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 95
    :cond_0
    return-void
.end method

.method private getRealPosition(FF)Landroid/graphics/Point;
    .locals 11
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 244
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 245
    .local v4, "p":Landroid/graphics/Point;
    const/4 v0, 0x0

    .line 246
    .local v0, "drawCanvasRoi":Landroid/graphics/Rect;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v6, :cond_0

    .line 247
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v0

    .line 249
    float-to-int v6, p1

    float-to-int v7, p2

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 251
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    .line 252
    .local v3, "m":Landroid/graphics/Matrix;
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 253
    .local v2, "i":Landroid/graphics/Matrix;
    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 254
    new-array v5, v10, [F

    .line 255
    .local v5, "src":[F
    new-array v1, v10, [F

    .line 256
    .local v1, "dst":[F
    aput p1, v5, v8

    aput p2, v5, v9

    .line 257
    invoke-virtual {v2, v1, v5}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 258
    aget v6, v1, v8

    float-to-int v6, v6

    iput v6, v4, Landroid/graphics/Point;->x:I

    aget v6, v1, v9

    float-to-int v6, v6

    iput v6, v4, Landroid/graphics/Point;->y:I

    .line 262
    .end local v1    # "dst":[F
    .end local v2    # "i":Landroid/graphics/Matrix;
    .end local v3    # "m":Landroid/graphics/Matrix;
    .end local v5    # "src":[F
    :cond_0
    return-object v4
.end method

.method private initRedEye()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 36
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;)V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    .line 37
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v5

    mul-int/2addr v4, v5

    new-array v4, v4, [I

    iput-object v4, v3, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempOriginal:[I

    .line 38
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    mul-int/2addr v4, v5

    new-array v4, v4, [I

    iput-object v4, v3, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempPreview:[I

    .line 39
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempOriginal:[I

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempOriginal:[I

    array-length v5, v5

    invoke-static {v3, v6, v4, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 40
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 54
    return-void

    .line 42
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v3

    .line 43
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v4

    add-int/2addr v4, v0

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v5

    mul-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v5

    add-int/2addr v4, v5

    .line 44
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    .line 42
    invoke-static {v3, v4, v5}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v1

    .line 45
    .local v1, "inputBuffer":Ljava/nio/IntBuffer;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempPreview:[I

    .line 46
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    mul-int/2addr v4, v0

    .line 47
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    .line 45
    invoke-static {v3, v4, v5}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v2

    .line 49
    .local v2, "outputBuffer":Ljava/nio/IntBuffer;
    invoke-virtual {v2, v1}, Ljava/nio/IntBuffer;->put(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    .line 51
    const/4 v1, 0x0

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public applyOriginal()[I
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 267
    const/4 v3, 0x0

    .line 268
    .local v3, "output":[I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v6, :cond_2

    .line 270
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v2

    .line 271
    .local v2, "mask":[B
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    .line 272
    .local v1, "input":[I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    .line 273
    .local v5, "w":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 274
    .local v0, "h":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v4

    .line 276
    .local v4, "r":Landroid/graphics/Rect;
    mul-int v6, v5, v0

    new-array v3, v6, [I

    .line 277
    array-length v6, v3

    invoke-static {v1, v8, v3, v8, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 279
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempOriginal:[I

    if-eqz v6, :cond_0

    .line 280
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempOriginal:[I

    mul-int v7, v5, v0

    invoke-static {v6, v8, v3, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 282
    :cond_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v6, :cond_1

    .line 283
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([I)V

    .line 284
    :cond_1
    const-string v6, "applyOriginal end"

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 287
    .end local v0    # "h":I
    .end local v1    # "input":[I
    .end local v2    # "mask":[B
    .end local v4    # "r":Landroid/graphics/Rect;
    .end local v5    # "w":I
    :goto_0
    return-object v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public applyPreview()I
    .locals 1

    .prologue
    .line 307
    const/4 v0, -0x1

    return v0
.end method

.method public configurationChange()V
    .locals 12

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v2

    .line 100
    .local v2, "w":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    .line 102
    .local v3, "h":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 103
    .local v4, "pw":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    .line 105
    .local v5, "ph":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v10

    .line 107
    .local v10, "viewWidth":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v9

    .line 109
    .local v9, "previewOutput":[I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempPreview:[I

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempPreview:[I

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v11

    mul-int/2addr v1, v11

    new-array v1, v1, [I

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempPreview:[I

    .line 115
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempOriginal:[I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempPreview:[I

    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->resizeBuff([I[IIIII)V

    .line 117
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    if-lt v6, v0, :cond_2

    .line 130
    .end local v6    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->afterApplyPreview()V

    .line 131
    return-void

    .line 119
    .restart local v6    # "i":I
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->mTempPreview:[I

    .line 120
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    mul-int/2addr v1, v6

    .line 121
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v11

    .line 119
    invoke-static {v0, v1, v11}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v7

    .line 123
    .local v7, "inputBuffer":Ljava/nio/IntBuffer;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v0

    add-int/2addr v0, v6

    mul-int/2addr v0, v10

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    .line 122
    invoke-static {v9, v0, v1}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v8

    .line 125
    .local v8, "outputBuffer":Ljava/nio/IntBuffer;
    invoke-virtual {v8, v7}, Ljava/nio/IntBuffer;->put(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    .line 126
    const/4 v7, 0x0

    .line 117
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 136
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;->destroy()V

    .line 137
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$RedEyeInfo;

    .line 138
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 139
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->drawFixedRedEye(Landroid/graphics/Canvas;)V

    .line 88
    return-void
.end method

.method public getMutexOn()Z
    .locals 1

    .prologue
    .line 297
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mutexOn:Z

    return v0
.end method

.method public init(I)V
    .locals 4
    .param p1, "effectType"    # I

    .prologue
    const/4 v3, 0x0

    .line 59
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mStartInitializing:Z

    .line 61
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201b6

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mRedEyeCircle:Landroid/graphics/Bitmap;

    .line 63
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    .line 67
    :cond_0
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mEffectType:I

    .line 68
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 69
    .local v0, "viewBuffer":[I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 71
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->initRedEye()V

    .line 73
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$1;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 81
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mStartInitializing:Z

    .line 82
    return-void
.end method

.method public isStartInitializing()Z
    .locals 1

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mStartInitializing:Z

    return v0
.end method

.method public setStep(I)V
    .locals 0
    .param p1, "step"    # I

    .prologue
    .line 293
    return-void
.end method

.method public touch(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 144
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v3, :cond_0

    .line 145
    const/4 v3, 0x0

    .line 156
    :goto_0
    return v3

    .line 147
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v2

    .line 149
    .local v2, "viewBuffer":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 150
    .local v1, "m":Landroid/graphics/Matrix;
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 151
    .local v0, "inversM":Landroid/graphics/Matrix;
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 152
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 154
    invoke-direct {p0, p1, v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectRedEyeMode;->applyRedEyePreview(Landroid/view/MotionEvent;[I)V

    .line 156
    const/4 v3, 0x1

    goto :goto_0
.end method
