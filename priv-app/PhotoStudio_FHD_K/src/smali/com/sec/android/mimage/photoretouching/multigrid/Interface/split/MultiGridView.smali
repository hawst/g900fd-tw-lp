.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;
.super Landroid/view/View;
.source "MultiGridView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$sec$android$mimage$photoretouching$multigrid$Interface$split$MultiGridView$ViewRatio:[I


# instance fields
.field private final DISPLAY_HEIGHT:I

.field private final DISPLAY_WIDTH:I

.field private final OVERAY_COLOR:I

.field private mBGDrawRoi:Landroid/graphics/Rect;

.field private mBGImgRoi:Landroid/graphics/Rect;

.field private mBackGroundColor:I

.field private mBackGroundImg:Landroid/graphics/Bitmap;

.field private mBackGroundImgId:I

.field private mDrawLinePaint:Landroid/graphics/Paint;

.field private mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

.field private mDrawSelectedLine:Z

.field private mHeight:I

.field private mJustDrawBackground:Z

.field private mPileBgImg:Landroid/graphics/Bitmap;

.field private mPileClipPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mPileFrameImg:Landroid/graphics/Bitmap;

.field private mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

.field private mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

.field private mViewRatioChanged:Z

.field private mWidth:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$sec$android$mimage$photoretouching$multigrid$Interface$split$MultiGridView$ViewRatio()[I
    .locals 3

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->$SWITCH_TABLE$com$sec$android$mimage$photoretouching$multigrid$Interface$split$MultiGridView$ViewRatio:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->values()[Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_16_TO_9:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_2_TO_3:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_3_TO_2:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_3_TO_4:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_4_TO_3:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_9_TO_16:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->$SWITCH_TABLE$com$sec$android$mimage$photoretouching$multigrid$Interface$split$MultiGridView$ViewRatio:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 958
    const/16 v0, 0x438

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->DISPLAY_WIDTH:I

    .line 959
    const/16 v0, 0x780

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->DISPLAY_HEIGHT:I

    .line 960
    const v0, -0x66cccccd

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->OVERAY_COLOR:I

    .line 972
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatioChanged:Z

    .line 973
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 975
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    .line 976
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImgId:I

    .line 977
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileBgImg:Landroid/graphics/Bitmap;

    .line 978
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileFrameImg:Landroid/graphics/Bitmap;

    .line 979
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundColor:I

    .line 980
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGImgRoi:Landroid/graphics/Rect;

    .line 981
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    .line 982
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mWidth:I

    .line 983
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mHeight:I

    .line 985
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawSelectedLine:Z

    .line 986
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 988
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .line 989
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 991
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mJustDrawBackground:Z

    .line 992
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    .line 41
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mContext:Landroid/content/Context;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatioChanged:Z

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "drawRectList"    # Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;
    .param p3, "srcItemList"    # Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .prologue
    const/16 v5, 0x780

    const/16 v2, 0x438

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 49
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 958
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->DISPLAY_WIDTH:I

    .line 959
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->DISPLAY_HEIGHT:I

    .line 960
    const v0, -0x66cccccd

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->OVERAY_COLOR:I

    .line 972
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatioChanged:Z

    .line 973
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_1_TO_1:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 975
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    .line 976
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImgId:I

    .line 977
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileBgImg:Landroid/graphics/Bitmap;

    .line 978
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileFrameImg:Landroid/graphics/Bitmap;

    .line 979
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundColor:I

    .line 980
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGImgRoi:Landroid/graphics/Rect;

    .line 981
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    .line 982
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mWidth:I

    .line 983
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mHeight:I

    .line 985
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawSelectedLine:Z

    .line 986
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 988
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .line 989
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 991
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mJustDrawBackground:Z

    .line 992
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    .line 51
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mContext:Landroid/content/Context;

    .line 53
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .line 54
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 56
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatioChanged:Z

    .line 58
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-virtual {p0, v0, v2, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setViewSize(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;II)V

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 61
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawLinePaint:Landroid/graphics/Paint;

    const/16 v1, 0xae

    const/16 v2, 0xcf

    invoke-static {v3, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawLinePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 65
    return-void
.end method

.method private calculateViewSize(FFII)V
    .locals 10
    .param p1, "ratioWidth"    # F
    .param p2, "ratioHeight"    # F
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 707
    int-to-float v5, p3

    .line 708
    .local v5, "viewWidth":F
    int-to-float v3, p4

    .line 710
    .local v3, "viewHeight":F
    const/4 v6, 0x0

    .line 711
    .local v6, "viewWidthRatio":F
    const/4 v4, 0x0

    .line 713
    .local v4, "viewHeightRatio":F
    const/4 v2, 0x0

    .line 714
    .local v2, "topPadding":F
    const/4 v0, 0x0

    .line 716
    .local v0, "leftPadding":F
    div-float v7, p2, p1

    mul-float v3, v5, v7

    .line 718
    div-float v6, v5, v5

    .line 719
    div-float v4, v3, v3

    .line 721
    cmpl-float v7, v6, v4

    if-lez v7, :cond_1

    .line 723
    mul-float/2addr v5, v4

    .line 724
    mul-float/2addr v3, v4

    .line 732
    :goto_0
    int-to-float v7, p3

    cmpl-float v7, v5, v7

    if-lez v7, :cond_2

    .line 734
    int-to-float v7, p3

    div-float/2addr v7, v5

    mul-float/2addr v3, v7

    .line 735
    int-to-float v5, p3

    .line 743
    :cond_0
    :goto_1
    int-to-float v7, p3

    sub-float/2addr v7, v5

    div-float v0, v7, v8

    .line 744
    int-to-float v7, p4

    sub-float/2addr v7, v3

    div-float v2, v7, v8

    .line 746
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    float-to-int v8, v5

    float-to-int v9, v3

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->setViewWidthHeight(II)V

    .line 747
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    float-to-int v8, v2

    invoke-virtual {v7, v8}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->setTopPadding(I)V

    .line 748
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    float-to-int v8, v0

    invoke-virtual {v7, v8}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->setLeftPadding(I)V

    .line 752
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    float-to-int v7, v5

    float-to-int v8, v3

    invoke-direct {v1, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 753
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 760
    return-void

    .line 728
    .end local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    mul-float/2addr v5, v6

    .line 729
    mul-float/2addr v3, v6

    goto :goto_0

    .line 737
    :cond_2
    int-to-float v7, p4

    cmpl-float v7, v3, v7

    if-lez v7, :cond_0

    .line 739
    int-to-float v7, p4

    div-float/2addr v7, v3

    mul-float/2addr v5, v7

    .line 740
    int-to-float v3, p4

    goto :goto_1
.end method

.method private drawBackground(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 512
    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 513
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    sget-object v1, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 517
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 524
    :goto_0
    return-void

    .line 522
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->drawBackgroundPattern(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private drawBackgroundPattern(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bgImag"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v12, 0x0

    .line 529
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 586
    :cond_0
    :goto_0
    return-void

    .line 534
    :cond_1
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 535
    .local v6, "patternImgWidth":I
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 537
    .local v5, "patternImgHeight":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    div-int v8, v9, v6

    .line 538
    .local v8, "widthMax":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    div-int v0, v9, v5

    .line 540
    .local v0, "heightMax":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    iget v3, v9, Landroid/graphics/Rect;->left:I

    .line 541
    .local v3, "paddingLeft":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    iget v4, v9, Landroid/graphics/Rect;->top:I

    .line 543
    .local v4, "paddingTop":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    if-le v9, v6, :cond_5

    .line 544
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    if-le v9, v5, :cond_5

    .line 546
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGImgRoi:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    rem-int/2addr v9, v10

    if-lez v9, :cond_2

    .line 548
    add-int/lit8 v8, v8, 0x1

    .line 551
    :cond_2
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGImgRoi:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    rem-int/2addr v9, v10

    if-lez v9, :cond_3

    .line 553
    add-int/lit8 v0, v0, 0x1

    .line 556
    :cond_3
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    sget-object v10, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v9, v10}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 558
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v8, :cond_0

    .line 560
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    if-lt v2, v0, :cond_4

    .line 558
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 563
    :cond_4
    mul-int v9, v6, v1

    add-int/2addr v9, v3

    int-to-float v9, v9

    .line 564
    mul-int v10, v5, v2

    add-int/2addr v10, v4

    int-to-float v10, v10

    .line 562
    invoke-virtual {p1, p2, v9, v10, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 560
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 569
    .end local v1    # "i":I
    .end local v2    # "j":I
    :cond_5
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    if-ne v9, v6, :cond_6

    .line 570
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    if-ne v9, v5, :cond_6

    .line 572
    int-to-float v9, v3

    .line 573
    int-to-float v10, v4

    .line 571
    invoke-virtual {p1, p2, v9, v10, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 578
    :cond_6
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    const/4 v11, 0x1

    invoke-static {p2, v9, v10, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 580
    .local v7, "temp":Landroid/graphics/Bitmap;
    int-to-float v9, v3

    .line 581
    int-to-float v10, v4

    .line 579
    invoke-virtual {p1, v7, v9, v10, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 583
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0
.end method

.method private drawImgs(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v13, 0x0

    .line 378
    if-nez p1, :cond_1

    .line 380
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " : canvas is null : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 478
    :cond_0
    :goto_0
    return-void

    .line 384
    :cond_1
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    if-nez v10, :cond_2

    .line 386
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " : mSrcItemList is null : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0

    .line 390
    :cond_2
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    if-nez v10, :cond_3

    .line 392
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " : mDrawRectList is null : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0

    .line 396
    :cond_3
    const/4 v7, 0x0

    .line 397
    .local v7, "srcItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    const/4 v4, 0x0

    .line 398
    .local v4, "pathRoundRect":Landroid/graphics/Path;
    const/4 v1, 0x0

    .line 399
    .local v1, "drawRect":Landroid/graphics/RectF;
    const/4 v9, 0x0

    .line 400
    .local v9, "srcRect":Landroid/graphics/Rect;
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v8

    .line 401
    .local v8, "srcNum":I
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->size()I

    move-result v0

    .line 402
    .local v0, "drawNum":I
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getRoundWidth()I

    move-result v5

    .line 404
    .local v5, "roundSize":I
    if-eq v8, v0, :cond_4

    .line 407
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " : srcNum != drawNum : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0

    .line 410
    :cond_4
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 411
    .local v3, "paintObj":Landroid/graphics/Paint;
    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 412
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_1
    if-ge v2, v8, :cond_0

    .line 414
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v10, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "srcItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    check-cast v7, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 415
    .restart local v7    # "srcItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    if-eqz v7, :cond_5

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v10

    if-nez v10, :cond_6

    .line 418
    :cond_5
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " : srcItem is null"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 422
    :cond_6
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v10, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v1

    .line 423
    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getmDrawRect()Landroid/graphics/Rect;

    move-result-object v9

    .line 430
    if-eqz v1, :cond_0

    .line 434
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->getDragFromIndex()I

    move-result v10

    if-ne v2, v10, :cond_a

    .line 437
    const/high16 v10, -0x1000000

    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 438
    int-to-float v10, v5

    int-to-float v11, v5

    invoke-virtual {p1, v1, v10, v11, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 455
    :cond_7
    :goto_2
    iget-boolean v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawSelectedLine:Z

    if-eqz v10, :cond_8

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->getCurrentIndex()I

    move-result v10

    if-ne v2, v10, :cond_8

    .line 461
    int-to-float v10, v5

    int-to-float v11, v5

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v10, v11, v12}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 466
    :cond_8
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->getDragToIndex()I

    move-result v10

    if-ne v2, v10, :cond_9

    .line 469
    const v10, -0x66cccccd

    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 470
    int-to-float v10, v5

    int-to-float v11, v5

    invoke-virtual {p1, v1, v10, v11, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 412
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 443
    :cond_a
    const/high16 v10, 0x55550000

    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 444
    int-to-float v10, v5

    int-to-float v11, v5

    invoke-virtual {p1, v1, v10, v11, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 445
    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v10

    int-to-float v11, v5

    invoke-virtual {p0, v10, v9, v1, v11}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->roundCornerImage(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;F)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 446
    .local v6, "src":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_7

    .line 447
    invoke-virtual {p1, v6, v13, v1, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 448
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_2
.end method

.method private drawPileFrame(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "imgBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x0

    .line 591
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 593
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " drawPileFrame : imgBitmap is null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 603
    :goto_0
    return-void

    .line 597
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    sget-object v1, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 598
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    const/4 v2, 0x0

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private declared-synchronized drawPileImage(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 163
    monitor-enter p0

    if-nez p1, :cond_0

    .line 165
    :try_start_0
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, " : canvas is null : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276
    :goto_0
    monitor-exit p0

    return-void

    .line 169
    :cond_0
    :try_start_1
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    if-nez v11, :cond_1

    .line 171
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, " : mSrcItemList is null : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v11

    monitor-exit p0

    throw v11

    .line 175
    :cond_1
    :try_start_2
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    if-nez v11, :cond_2

    .line 177
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, " : mDrawRectList is null : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0

    .line 181
    :cond_2
    const/4 v8, 0x0

    .line 182
    .local v8, "srcItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    const/4 v0, 0x0

    .line 183
    .local v0, "canvasPath":Landroid/graphics/Path;
    const/4 v3, 0x0

    .line 184
    .local v3, "drawRect":Landroid/graphics/RectF;
    const/4 v10, 0x0

    .line 185
    .local v10, "srcRect":Landroid/graphics/Rect;
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v9

    .line 186
    .local v9, "srcNum":I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->size()I

    move-result v1

    .line 189
    .local v1, "drawNum":I
    new-instance v0, Landroid/graphics/Path;

    .end local v0    # "canvasPath":Landroid/graphics/Path;
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 190
    .restart local v0    # "canvasPath":Landroid/graphics/Path;
    new-instance v11, Landroid/graphics/RectF;

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-direct {v11, v12}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    sget-object v12, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v11, v12}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 192
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getCurrentStyleId()I

    move-result v11

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v12}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v12

    invoke-static {v11, v12}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getPileStyleId(II)I

    move-result v7

    .line 193
    .local v7, "pileStyleId":I
    packed-switch v7, :pswitch_data_0

    .line 211
    :goto_1
    if-eq v9, v1, :cond_3

    .line 214
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, " : srcNum != drawNum : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 196
    :pswitch_0
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-static {v9, v11, v12}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileAPathData;->getPaths(ILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;Landroid/graphics/Rect;)Ljava/util/ArrayList;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    goto :goto_1

    .line 199
    :pswitch_1
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-static {v9, v11, v12}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileBPathData;->getPaths(ILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;Landroid/graphics/Rect;)Ljava/util/ArrayList;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    goto :goto_1

    .line 202
    :pswitch_2
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-static {v9, v11, v12}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileCPathData;->getPaths(ILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;Landroid/graphics/Rect;)Ljava/util/ArrayList;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    goto :goto_1

    .line 205
    :pswitch_3
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    invoke-static {v9, v11, v12}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridPileDPathData;->getPaths(ILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;Landroid/graphics/Rect;)Ljava/util/ArrayList;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    goto :goto_1

    .line 217
    :cond_3
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    if-nez v11, :cond_4

    .line 218
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, " : mPileClipPaths is null "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 221
    :cond_4
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-direct {p0, v11, v7, v9, v12}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->getDrawOrder(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;IILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;)[I

    move-result-object v2

    .line 223
    .local v2, "drawOrder":[I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    array-length v11, v2

    if-lt v5, v11, :cond_5

    .line 275
    sget-object v11, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v11}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    goto/16 :goto_0

    .line 225
    :cond_5
    sget-object v11, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v11}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 226
    aget v6, v2, v5

    .line 227
    .local v6, "index":I
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v11, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "srcItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    check-cast v8, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 228
    .restart local v8    # "srcItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    if-eqz v8, :cond_6

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v11

    if-nez v11, :cond_7

    .line 231
    :cond_6
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, " : srcItem is null"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 235
    :cond_7
    if-ltz v6, :cond_8

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->size()I

    move-result v11

    if-ge v6, v11, :cond_8

    .line 236
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v11, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v3

    .line 238
    :cond_8
    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getmDrawRect()Landroid/graphics/Rect;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v10

    .line 242
    :try_start_3
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    if-eqz v11, :cond_9

    if-ltz v6, :cond_9

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v6, v11, :cond_9

    .line 243
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Path;

    sget-object v12, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v11, v12}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z
    :try_end_3
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 252
    :cond_9
    :goto_3
    :try_start_4
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->getDragFromIndex()I

    move-result v11

    if-ne v6, v11, :cond_b

    .line 254
    const/high16 v11, -0x1000000

    invoke-virtual {p1, v11}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 270
    :goto_4
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v11}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->getDragToIndex()I

    move-result v11

    if-ne v6, v11, :cond_a

    .line 272
    const v11, -0x66cccccd

    invoke-virtual {p1, v11}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 223
    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 245
    :catch_0
    move-exception v4

    .line 246
    .local v4, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v4}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_3

    .line 258
    .end local v4    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_b
    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {p1, v11, v10, v3, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4

    .line 193
    :pswitch_data_0
    .packed-switch 0x1e200100
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private drawSelectedPath(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 151
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawSelectedLine:Z

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->getCurrentIndex()I

    move-result v0

    .line 155
    .local v0, "currentIndex":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 156
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->getCurrentIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 158
    .end local v0    # "currentIndex":I
    :cond_0
    return-void
.end method

.method private drawSelectedRect(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 347
    if-nez p1, :cond_1

    .line 349
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " : canvas is null : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 373
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    if-nez v4, :cond_2

    .line 355
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " : mDrawRectList is null : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0

    .line 359
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->getCurrentIndex()I

    move-result v0

    .line 360
    .local v0, "currentIndex":I
    if-ltz v0, :cond_0

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 364
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getRoundWidth()I

    move-result v3

    .line 365
    .local v3, "roundSize":I
    const/4 v2, 0x0

    .line 366
    .local v2, "pathRoundRect":Landroid/graphics/Path;
    const/4 v1, 0x0

    .line 368
    .local v1, "drawRect":Landroid/graphics/RectF;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v1

    .line 372
    int-to-float v4, v3

    int-to-float v5, v3

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private getDrawOrder(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;IILcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;)[I
    .locals 8
    .param p1, "list"    # Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    .param p2, "pileStyleId"    # I
    .param p3, "srcNum"    # I
    .param p4, "mViewRatio2"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x1

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 282
    const v2, 0x1e200102

    if-ne p2, v2, :cond_2

    .line 283
    sget-object v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_9_TO_16:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    if-ne p4, v2, :cond_2

    .line 284
    const/4 v2, 0x6

    if-ne p3, v2, :cond_1

    .line 285
    const/4 v2, 0x6

    new-array v1, v2, [I

    const/4 v2, 0x0

    aput v3, v1, v2

    aput v6, v1, v3

    aput v7, v1, v4

    aput v4, v1, v5

    aput v5, v1, v7

    .line 302
    :cond_0
    :goto_0
    return-object v1

    .line 286
    :cond_1
    if-ne p3, v7, :cond_2

    .line 287
    new-array v1, v7, [I

    const/4 v2, 0x0

    aput v3, v1, v2

    aput v6, v1, v3

    aput v4, v1, v4

    aput v5, v1, v5

    goto :goto_0

    .line 289
    :cond_2
    const v2, 0x1e200101

    if-ne p2, v2, :cond_4

    .line 290
    sget-object v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->PROPORTION_9_TO_16:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    if-ne p4, v2, :cond_4

    .line 291
    if-ne p3, v3, :cond_3

    .line 292
    new-array v1, v3, [I

    const/4 v2, 0x0

    aput v6, v1, v2

    goto :goto_0

    .line 293
    :cond_3
    if-ne p3, v5, :cond_4

    .line 294
    new-array v1, v5, [I

    aput v6, v1, v6

    aput v4, v1, v3

    aput v3, v1, v4

    goto :goto_0

    .line 298
    :cond_4
    new-array v1, p3, [I

    .line 299
    .local v1, "order":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, p3, :cond_0

    .line 300
    aput v0, v1, v0

    .line 299
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private setBackgroundDrawRoi()V
    .locals 2

    .prologue
    .line 914
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 916
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    .line 919
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getBackgroundRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 920
    return-void
.end method


# virtual methods
.method public calculateRectSize(Landroid/graphics/Rect;Landroid/graphics/RectF;Z)Landroid/graphics/Rect;
    .locals 9
    .param p1, "srcRect"    # Landroid/graphics/Rect;
    .param p2, "drawRect"    # Landroid/graphics/RectF;
    .param p3, "isProportionChanged"    # Z

    .prologue
    const/4 v2, 0x0

    .line 793
    if-nez p1, :cond_0

    .line 795
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " : calculateRectSize : srcRect is null"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 826
    :goto_0
    return-object v2

    .line 798
    :cond_0
    if-nez p2, :cond_1

    .line 800
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " : calculateRectSize : drawRect is null"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0

    .line 804
    :cond_1
    const/4 v2, 0x0

    .line 806
    .local v2, "returnRect":Landroid/graphics/Rect;
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    .line 807
    .local v1, "drawWidth":F
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v0

    .line 809
    .local v0, "drawHeight":F
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v4, v5

    .line 810
    .local v4, "srcWidth":F
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v3, v5

    .line 812
    .local v3, "srcHeight":F
    cmpg-float v5, v4, v3

    if-gez v5, :cond_2

    .line 814
    mul-float v5, v3, v1

    div-float v4, v5, v0

    .line 821
    :goto_1
    new-instance v2, Landroid/graphics/Rect;

    .end local v2    # "returnRect":Landroid/graphics/Rect;
    iget v5, p1, Landroid/graphics/Rect;->left:I

    .line 822
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 823
    iget v7, p1, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    add-float/2addr v7, v4

    float-to-int v7, v7

    .line 824
    iget v8, p1, Landroid/graphics/Rect;->top:I

    int-to-float v8, v8

    add-float/2addr v8, v3

    float-to-int v8, v8

    .line 821
    invoke-direct {v2, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 826
    .restart local v2    # "returnRect":Landroid/graphics/Rect;
    goto :goto_0

    .line 818
    :cond_2
    mul-float v5, v4, v0

    div-float v3, v5, v1

    goto :goto_1
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    .line 83
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileFrameImg:Landroid/graphics/Bitmap;

    .line 84
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileBgImg:Landroid/graphics/Bitmap;

    .line 85
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGImgRoi:Landroid/graphics/Rect;

    .line 86
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    .line 87
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 88
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .line 89
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 91
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 94
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    .line 96
    :cond_0
    return-void
.end method

.method public destroyAll()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 100
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    .line 101
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileFrameImg:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileFrameImg:Landroid/graphics/Bitmap;

    .line 102
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileBgImg:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileBgImg:Landroid/graphics/Bitmap;

    .line 103
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGImgRoi:Landroid/graphics/Rect;

    .line 104
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGDrawRoi:Landroid/graphics/Rect;

    .line 105
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 106
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    .line 107
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 109
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 112
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileClipPaths:Ljava/util/ArrayList;

    .line 115
    :cond_0
    return-void
.end method

.method public fitRectSizeRatio(Z)V
    .locals 7
    .param p1, "isProportionChanged"    # Z

    .prologue
    .line 764
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v2

    .line 765
    .local v2, "itms":I
    const/4 v4, 0x0

    .line 766
    .local v4, "srcItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    const/4 v5, 0x0

    .line 767
    .local v5, "srcRect":Landroid/graphics/Rect;
    const/4 v0, 0x0

    .line 768
    .local v0, "drawRect":Landroid/graphics/RectF;
    const/4 v3, 0x0

    .line 770
    .local v3, "newRect":Landroid/graphics/Rect;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 782
    return-void

    .line 772
    :cond_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v6, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "srcItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    check-cast v4, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 773
    .restart local v4    # "srcItem":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getmDrawRect()Landroid/graphics/Rect;

    move-result-object v5

    .line 774
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v6, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->get(I)Landroid/graphics/RectF;

    move-result-object v0

    .line 775
    invoke-virtual {p0, v5, v0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->calculateRectSize(Landroid/graphics/Rect;Landroid/graphics/RectF;Z)Landroid/graphics/Rect;

    move-result-object v3

    .line 777
    if-eqz v3, :cond_1

    .line 779
    invoke-virtual {v4, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setmDrawRect(Landroid/graphics/Rect;)V

    .line 770
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getCanvasProportionRatio()F
    .locals 4

    .prologue
    .line 664
    const/4 v1, 0x0

    .line 665
    .local v1, "ratioWidth":F
    const/4 v0, 0x0

    .line 667
    .local v0, "ratioHeight":F
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->$SWITCH_TABLE$com$sec$android$mimage$photoretouching$multigrid$Interface$split$MultiGridView$ViewRatio()[I

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 699
    :goto_0
    div-float v2, v0, v1

    return v2

    .line 670
    :pswitch_0
    const/high16 v1, 0x3f800000    # 1.0f

    .line 671
    const/high16 v0, 0x3f800000    # 1.0f

    .line 672
    goto :goto_0

    .line 674
    :pswitch_1
    const/high16 v1, 0x40000000    # 2.0f

    .line 675
    const/high16 v0, 0x40400000    # 3.0f

    .line 676
    goto :goto_0

    .line 678
    :pswitch_2
    const/high16 v1, 0x40400000    # 3.0f

    .line 679
    const/high16 v0, 0x40800000    # 4.0f

    .line 680
    goto :goto_0

    .line 682
    :pswitch_3
    const/high16 v1, 0x41100000    # 9.0f

    .line 683
    const/high16 v0, 0x41800000    # 16.0f

    .line 684
    goto :goto_0

    .line 686
    :pswitch_4
    const/high16 v1, 0x41800000    # 16.0f

    .line 687
    const/high16 v0, 0x41100000    # 9.0f

    .line 688
    goto :goto_0

    .line 690
    :pswitch_5
    const/high16 v1, 0x40800000    # 4.0f

    .line 691
    const/high16 v0, 0x40400000    # 3.0f

    .line 692
    goto :goto_0

    .line 694
    :pswitch_6
    const/high16 v1, 0x40400000    # 3.0f

    .line 695
    const/high16 v0, 0x40000000    # 2.0f

    goto :goto_0

    .line 667
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getCollageBackGroundColor()I
    .locals 1

    .prologue
    .line 927
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundColor:I

    return v0
.end method

.method public getCollageBackGroundImg()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 854
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getCollageBackGroundImgId()I
    .locals 1

    .prologue
    .line 870
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImgId:I

    return v0
.end method

.method public getCollagePileBgImg()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileBgImg:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getCollagePileFrameImg()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 890
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileFrameImg:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getViewRatio()Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;
    .locals 1

    .prologue
    .line 835
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    return-object v0
.end method

.method public isDrawSelectedLine()Z
    .locals 1

    .prologue
    .line 941
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawSelectedLine:Z

    return v0
.end method

.method public isJustDrawBackground()Z
    .locals 1

    .prologue
    .line 946
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mJustDrawBackground:Z

    return v0
.end method

.method public onConfigurationChanged()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 121
    if-nez p1, :cond_0

    .line 145
    :goto_0
    return-void

    .line 126
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawRectList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridDrawRectList;->getCurrentStyleId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->isPileStyle(II)I

    move-result v0

    if-gez v0, :cond_2

    .line 131
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->drawBackground(Landroid/graphics/Canvas;)V

    .line 132
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mJustDrawBackground:Z

    if-nez v0, :cond_1

    .line 133
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->drawImgs(Landroid/graphics/Canvas;)V

    .line 134
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->drawSelectedRect(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 140
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->drawBackground(Landroid/graphics/Canvas;)V

    .line 141
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->drawPileImage(Landroid/graphics/Canvas;)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileFrameImg:Landroid/graphics/Bitmap;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->drawPileFrame(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    .line 143
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->drawSelectedPath(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 72
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 73
    return-void
.end method

.method public roundCornerImage(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;F)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "src"    # Landroid/graphics/Bitmap;
    .param p2, "srcRect"    # Landroid/graphics/Rect;
    .param p3, "dstRect"    # Landroid/graphics/RectF;
    .param p4, "round"    # F

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 483
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 484
    .local v5, "width":I
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 486
    .local v1, "height":I
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v1, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 488
    .local v4, "result":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 489
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, v7, v7, v7, v7}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 492
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 493
    .local v2, "paint":Landroid/graphics/Paint;
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 494
    const/high16 v6, -0x1000000

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 497
    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v6

    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v7

    invoke-direct {v3, v8, v8, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 500
    .local v3, "rectF":Landroid/graphics/RectF;
    invoke-virtual {v0, v3, p4, p4, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 503
    new-instance v6, Landroid/graphics/PorterDuffXfermode;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v7}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 505
    invoke-virtual {v0, p1, p2, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 508
    return-object v4
.end method

.method public setCollageBackGroundColor(I)V
    .locals 2
    .param p1, "mBackGroundColor"    # I

    .prologue
    .line 935
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setCollageBackGroundImg(Landroid/graphics/Bitmap;I)V

    .line 936
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundColor:I

    .line 937
    return-void
.end method

.method public setCollageBackGroundImg(Landroid/graphics/Bitmap;I)V
    .locals 1
    .param p1, "backGroundImg"    # Landroid/graphics/Bitmap;
    .param p2, "id"    # I

    .prologue
    .line 862
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    .line 863
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    .line 864
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImgId:I

    .line 865
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setCollageBackgroundImgRoi()V

    .line 866
    return-void
.end method

.method public setCollageBackgroundImgRoi()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 901
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGImgRoi:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 903
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGImgRoi:Landroid/graphics/Rect;

    .line 906
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 908
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBGImgRoi:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mBackGroundImg:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 910
    :cond_1
    return-void
.end method

.method public setCollagePileBgImg(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "pileBgImg"    # Landroid/graphics/Bitmap;

    .prologue
    .line 883
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileBgImg:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileBgImg:Landroid/graphics/Bitmap;

    .line 884
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileBgImg:Landroid/graphics/Bitmap;

    .line 886
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setCollageBackgroundImgRoi()V

    .line 887
    return-void
.end method

.method public setCollagePileFramImg(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "pileFrameImg"    # Landroid/graphics/Bitmap;

    .prologue
    .line 895
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileFrameImg:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileFrameImg:Landroid/graphics/Bitmap;

    .line 896
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mPileFrameImg:Landroid/graphics/Bitmap;

    .line 897
    return-void
.end method

.method public setDrawSelectedLine(Z)V
    .locals 0
    .param p1, "mIsDrawSelectedLine"    # Z

    .prologue
    .line 950
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mDrawSelectedLine:Z

    .line 951
    return-void
.end method

.method public setJustDrawBackground(Z)V
    .locals 0
    .param p1, "drawBackground"    # Z

    .prologue
    .line 955
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mJustDrawBackground:Z

    .line 956
    return-void
.end method

.method public setViewRatio(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;)V
    .locals 3
    .param p1, "mViewRatio"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .prologue
    const/4 v2, 0x1

    .line 843
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatioChanged:Z

    .line 844
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 845
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mWidth:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mHeight:I

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setViewSize(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;II)V

    .line 846
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->fitRectSizeRatio(Z)V

    .line 847
    return-void
.end method

.method public setViewSize(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 607
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatioChanged:Z

    .line 608
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setViewSize(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;II)V

    .line 609
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->fitRectSizeRatio(Z)V

    .line 610
    return-void
.end method

.method public setViewSize(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;II)V
    .locals 4
    .param p1, "ratio"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 614
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mWidth:I

    .line 615
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mHeight:I

    .line 616
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    .line 618
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatioChanged:Z

    if-nez v2, :cond_0

    .line 660
    :goto_0
    return-void

    .line 621
    :cond_0
    const/4 v1, 0x0

    .line 622
    .local v1, "ratioWidth":F
    const/4 v0, 0x0

    .line 624
    .local v0, "ratioHeight":F
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->$SWITCH_TABLE$com$sec$android$mimage$photoretouching$multigrid$Interface$split$MultiGridView$ViewRatio()[I

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatio:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView$ViewRatio;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 656
    :goto_1
    invoke-direct {p0, v1, v0, p2, p3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->calculateViewSize(FFII)V

    .line 657
    invoke-static {p2, p3}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setDisplaySize(II)V

    .line 658
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setBackgroundDrawRoi()V

    .line 659
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->mViewRatioChanged:Z

    goto :goto_0

    .line 627
    :pswitch_0
    const/high16 v1, 0x3f800000    # 1.0f

    .line 628
    const/high16 v0, 0x3f800000    # 1.0f

    .line 629
    goto :goto_1

    .line 631
    :pswitch_1
    const/high16 v1, 0x40000000    # 2.0f

    .line 632
    const/high16 v0, 0x40400000    # 3.0f

    .line 633
    goto :goto_1

    .line 635
    :pswitch_2
    const/high16 v1, 0x40400000    # 3.0f

    .line 636
    const/high16 v0, 0x40800000    # 4.0f

    .line 637
    goto :goto_1

    .line 639
    :pswitch_3
    const/high16 v1, 0x41100000    # 9.0f

    .line 640
    const/high16 v0, 0x41800000    # 16.0f

    .line 641
    goto :goto_1

    .line 643
    :pswitch_4
    const/high16 v1, 0x41800000    # 16.0f

    .line 644
    const/high16 v0, 0x41100000    # 9.0f

    .line 645
    goto :goto_1

    .line 647
    :pswitch_5
    const/high16 v1, 0x40800000    # 4.0f

    .line 648
    const/high16 v0, 0x40400000    # 3.0f

    .line 649
    goto :goto_1

    .line 651
    :pswitch_6
    const/high16 v1, 0x40400000    # 3.0f

    .line 652
    const/high16 v0, 0x40000000    # 2.0f

    goto :goto_1

    .line 624
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
