.class Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;
.super Ljava/lang/Object;
.source "EffectView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initMiddleButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    .line 698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 701
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 702
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 704
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->clearnSelectionArea()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 706
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->applyPreview()I

    .line 708
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->initMiddleButton()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 709
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->invalidateViews()V

    .line 725
    :cond_0
    :goto_0
    return-void

    .line 713
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 715
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v0, :cond_0

    .line 717
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x14000000

    .line 718
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    const/high16 v3, 0x16000000

    .line 719
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getEffectType()I

    move-result v4

    .line 720
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$14;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->getCurrentStep()F

    move-result v5

    float-to-int v5, v5

    .line 718
    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;-><init>(III)V

    .line 717
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(ILcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;)V

    goto :goto_0
.end method
