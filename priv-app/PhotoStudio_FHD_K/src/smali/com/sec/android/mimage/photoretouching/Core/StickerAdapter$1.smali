.class Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$1;
.super Ljava/lang/Object;
.source "StickerAdapter.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    .line 383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 387
    if-eqz p2, :cond_3

    .line 389
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;)[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    move-result-object v1

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 422
    :cond_0
    :goto_1
    return-void

    .line 391
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;)[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    move-result-object v1

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_2

    .line 393
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;)[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    move-result-object v1

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-ne p1, v1, :cond_2

    .line 395
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    invoke-static {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->access$1(Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;I)V

    goto :goto_1

    .line 389
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 403
    .end local v0    # "i":I
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;)[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 405
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;)[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    move-result-object v1

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_4

    .line 407
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;)[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    move-result-object v1

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-eq p1, v1, :cond_0

    .line 403
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method
