.class Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;
.super Ljava/lang/Object;
.source "PenActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "activityDrawListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 2028
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;)V
    .locals 0

    .prologue
    .line 2028
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;FFFFFLandroid/graphics/RectF;)V
    .locals 17
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "ratio"    # F
    .param p5, "frameStartX"    # F
    .param p6, "frameStartY"    # F
    .param p7, "updateRect"    # Landroid/graphics/RectF;

    .prologue
    .line 2033
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f050256

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 2034
    .local v8, "progressbarWidth":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f050255

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 2035
    .local v11, "topMargine":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f05025a

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 2036
    .local v10, "textTopMagine":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f05025b

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 2037
    .local v7, "progressbarTextSize":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f050259

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 2038
    .local v6, "progressbarLeftMagine":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v13

    sub-int/2addr v13, v8

    div-int/lit8 v13, v13, 0x2

    int-to-float v1, v13

    .line 2040
    .local v1, "ProgressLeftMagine":F
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 2041
    .local v5, "paint":Landroid/graphics/Paint;
    const/4 v13, 0x1

    invoke-virtual {v5, v13}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2042
    const/4 v13, 0x1

    invoke-virtual {v5, v13}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2044
    new-instance v12, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getContext()Landroid/content/Context;
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$1(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/content/Context;

    move-result-object v13

    invoke-direct {v12, v13}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2046
    .local v12, "tv":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mBitmap_bg:Landroid/graphics/Bitmap;
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$2(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/graphics/Bitmap;

    move-result-object v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mBitmap_bg:Landroid/graphics/Bitmap;
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$2(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 2047
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    move-object/from16 v0, p1

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setBitmapBG(Landroid/graphics/Canvas;)V
    invoke-static {v13, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$3(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/graphics/Canvas;)V

    .line 2049
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenMultitouch:Z
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$4(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Z

    move-result v13

    if-eqz v13, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomMultitouch:Z
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$5(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 2050
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mGaugeRatio:F
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$6(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F

    move-result v13

    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 2051
    .local v3, "gaugeRatio":I
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "%"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2052
    const/4 v13, -0x1

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2053
    const/4 v13, 0x0

    int-to-float v14, v7

    invoke-virtual {v12, v13, v14}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2054
    const-string v13, "sec-roboto-light"

    const/4 v14, 0x0

    invoke-static {v13, v14}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    .line 2055
    .local v2, "font":Landroid/graphics/Typeface;
    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2056
    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x0

    const/high16 v15, 0x3f800000    # 1.0f

    const/high16 v16, -0x41000000    # -0.5f

    invoke-virtual/range {v12 .. v16}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 2057
    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    .line 2058
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v13

    const/high16 v14, -0x80000000

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    .line 2059
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v14

    const/high16 v15, -0x80000000

    invoke-static {v14, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    .line 2058
    invoke-virtual {v12, v13, v14}, Landroid/widget/TextView;->measure(II)V

    .line 2061
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v12}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v15

    invoke-virtual {v12}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v16

    invoke-virtual/range {v12 .. v16}, Landroid/widget/TextView;->layout(IIII)V

    .line 2062
    const/16 v13, 0x11

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setGravity(I)V

    .line 2063
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v12}, Landroid/widget/TextView;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$7(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/graphics/Bitmap;)V

    .line 2065
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 2066
    .local v9, "res":Landroid/content/res/Resources;
    const v13, 0x7f0203c8

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    .line 2067
    .local v4, "gauge_bg":Landroid/graphics/drawable/BitmapDrawable;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$8(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/graphics/Bitmap;)V

    .line 2069
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mAngleTextBitmap:Landroid/graphics/Bitmap;
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$9(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/graphics/Bitmap;

    move-result-object v13

    if-eqz v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mBitmap_bg:Landroid/graphics/Bitmap;
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$2(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/graphics/Bitmap;

    move-result-object v13

    if-eqz v13, :cond_2

    .line 2070
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mAngleTextBitmap:Landroid/graphics/Bitmap;
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$9(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v13

    if-nez v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mBitmap_bg:Landroid/graphics/Bitmap;
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$2(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v13

    if-nez v13, :cond_2

    .line 2071
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mBitmap_bg:Landroid/graphics/Bitmap;
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$2(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/graphics/Bitmap;

    move-result-object v13

    int-to-float v14, v11

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v1, v14, v15}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2072
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;->this$0:Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mAngleTextBitmap:Landroid/graphics/Bitmap;
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->access$9(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v14

    div-int/lit8 v14, v14, 0x2

    sub-int/2addr v14, v6

    int-to-float v14, v14

    int-to-float v15, v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14, v15, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2075
    :cond_2
    const/16 p1, 0x0

    .line 2077
    .end local v2    # "font":Landroid/graphics/Typeface;
    .end local v3    # "gaugeRatio":I
    .end local v4    # "gauge_bg":Landroid/graphics/drawable/BitmapDrawable;
    .end local v9    # "res":Landroid/content/res/Resources;
    :cond_3
    return-void
.end method
