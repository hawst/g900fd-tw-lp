.class Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$12;
.super Ljava/lang/Object;
.source "PortraitView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->init2DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    .line 678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 6
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 691
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V

    .line 692
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getButtonPosition(I)Landroid/graphics/Point;

    move-result-object v0

    .line 693
    .local v0, "point":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f060009

    iget v4, v0, Landroid/graphics/Point;->x:I

    iget v5, v0, Landroid/graphics/Point;->y:I

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, 0x2

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastActionbar(Landroid/content/Context;III)V

    .line 695
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "vibrator"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    .line 696
    .local v1, "vibrator":Landroid/os/Vibrator;
    const-wide/16 v2, 0xd

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 697
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V

    .line 698
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 683
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 685
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V

    .line 686
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 704
    return-void
.end method
