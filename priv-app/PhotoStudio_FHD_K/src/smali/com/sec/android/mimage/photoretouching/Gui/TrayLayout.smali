.class public Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;
.super Landroid/widget/LinearLayout;
.source "TrayLayout.java"


# static fields
.field public static final MAX:I = 0x5


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsShow:Z

.field private mLayout:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 322
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mIsShow:Z

    .line 325
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mLayout:Landroid/view/ViewGroup;

    .line 326
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    .line 37
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    .line 38
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    const v1, 0x7f030072

    invoke-static {v0, v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mLayout:Landroid/view/ViewGroup;

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 322
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mIsShow:Z

    .line 325
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mLayout:Landroid/view/ViewGroup;

    .line 326
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    .line 43
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    .line 44
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    const v1, 0x7f030072

    invoke-static {v0, v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mLayout:Landroid/view/ViewGroup;

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 322
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mIsShow:Z

    .line 325
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mLayout:Landroid/view/ViewGroup;

    .line 326
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    .line 49
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    .line 50
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    const v1, 0x7f030072

    invoke-static {v0, v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mLayout:Landroid/view/ViewGroup;

    .line 51
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;Z)V
    .locals 0

    .prologue
    .line 322
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mIsShow:Z

    return-void
.end method

.method private initTrayLayoutButton(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V
    .locals 2
    .param p1, "touchFunction"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    .prologue
    .line 290
    const v1, 0x7f090147

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    .line 291
    .local v0, "slider":Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;
    if-eqz v0, :cond_0

    .line 293
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$3;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->setOnDrawerCloseListener(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerCloseListener;)V

    .line 306
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$4;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->setOnDrawerOpenListener(Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer$OnDrawerOpenListener;)V

    .line 320
    :cond_0
    return-void
.end method


# virtual methods
.method public addButton(Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    .locals 5
    .param p2, "buttonListener"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .param p3, "deleteListener"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;",
            ">;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            ")",
            "Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;>;"
    const/4 v2, 0x0

    .line 122
    .local v2, "ret":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    const v3, 0x7f09014a

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 123
    .local v0, "body":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 125
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;-><init>(Landroid/content/Context;)V

    .line 126
    .local v1, "child":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 128
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    const/4 v3, 0x0

    invoke-virtual {v1, p1, v3, p2, p3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->init(Ljava/util/ArrayList;ZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 131
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    new-instance v4, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$1;

    invoke-direct {v4, p0, v1, v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;Landroid/widget/LinearLayout;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 141
    move-object v2, v1

    .line 144
    .end local v1    # "child":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    :cond_0
    return-object v2
.end method

.method public addButton(Ljava/util/ArrayList;ZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    .locals 6
    .param p2, "longClick"    # Z
    .param p3, "buttonListener"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .param p4, "deleteListener"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;",
            ">;Z",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            ")",
            "Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;>;"
    const/4 v2, 0x0

    .line 152
    .local v2, "ret":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    const v4, 0x7f09014a

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 153
    .local v0, "body":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_1

    .line 155
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;-><init>(Landroid/content/Context;)V

    .line 156
    .local v1, "child":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 158
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->init(Ljava/util/ArrayList;ZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 160
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->invisibleDeleteButton()V

    .line 161
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 162
    .local v3, "vGroup":Landroid/view/ViewGroup;
    if-nez v3, :cond_0

    .line 163
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 164
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "long click tray button child id:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 165
    move-object v2, v1

    .line 168
    .end local v1    # "child":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    .end local v3    # "vGroup":Landroid/view/ViewGroup;
    :cond_1
    return-object v2
.end method

.method public addFirstButton(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;>;"
    const v3, 0x7f09014a

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 191
    .local v0, "body":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 193
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;-><init>(Landroid/content/Context;)V

    .line 195
    .local v2, "child":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout$2;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;Ljava/util/ArrayList;)V

    .line 231
    .local v1, "buttonTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    invoke-virtual {v2, p1, v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->init(Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 232
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 243
    .end local v1    # "buttonTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .end local v2    # "child":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mLayout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 73
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mLayout:Landroid/view/ViewGroup;

    .line 74
    return-void
.end method

.method public disableButtonTouch()V
    .locals 0

    .prologue
    .line 116
    return-void
.end method

.method public dispatchTouchEvent()V
    .locals 0

    .prologue
    .line 254
    return-void
.end method

.method public enableButtonTouch()V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public hideTray()V
    .locals 2

    .prologue
    .line 107
    const v1, 0x7f090147

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    .line 108
    .local v0, "slider":Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->close()V

    .line 109
    return-void
.end method

.method public isFocused()Z
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mLayout:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    return v0
.end method

.method public isShow()Z
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->mIsShow:Z

    return v0
.end method

.method public isTrayButtonFocused()Z
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x0

    .line 262
    .local v0, "ret":Z
    return v0
.end method

.method public onConfigurationChanged(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;>;"
    const v6, 0x7f09014a

    invoke-virtual {p0, v6}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 83
    .local v0, "body":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 84
    if-eqz v0, :cond_0

    .line 86
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v3, v6, :cond_1

    .line 96
    .end local v3    # "i":I
    :cond_0
    const v6, 0x7f090148

    invoke-virtual {p0, v6}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 97
    .local v2, "child":Landroid/widget/LinearLayout;
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 98
    .local v4, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v6, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    neg-int v6, v6

    int-to-float v6, v6

    invoke-virtual {p0, v6}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->setTranslationY(F)V

    .line 99
    return-void

    .line 88
    .end local v2    # "child":Landroid/widget/LinearLayout;
    .end local v4    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .restart local v3    # "i":I
    :cond_1
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 89
    .local v1, "button":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 90
    .local v5, "vGroup":Landroid/view/ViewGroup;
    if-eqz v5, :cond_2

    .line 91
    invoke-virtual {v5, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 92
    :cond_2
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 86
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public removeAllViews()V
    .locals 2

    .prologue
    .line 60
    const v1, 0x7f09014a

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 61
    .local v0, "body":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 63
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public setDisableTrayiconLongClick(Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V
    .locals 3
    .param p2, "buttonTouch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .param p3, "deleteTouch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;",
            ">;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            ")V"
        }
    .end annotation

    .prologue
    .line 279
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;>;"
    if-eqz p1, :cond_0

    .line 281
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 287
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 283
    .restart local v0    # "i":I
    :cond_1
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 284
    .local v1, "tButton":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setDisableLongClick(Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 281
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setShowHideButtonInterface(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V
    .locals 0
    .param p1, "touchFunction"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->initTrayLayoutButton(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 56
    return-void
.end method

.method public setTrayiconLongClick(Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V
    .locals 3
    .param p2, "buttonTouch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .param p3, "deleteTouch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;",
            ">;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            ")V"
        }
    .end annotation

    .prologue
    .line 270
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;>;"
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 276
    return-void

    .line 272
    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 273
    .local v1, "tButton":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setLongClick(Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 274
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->enable()V

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public showTray()V
    .locals 2

    .prologue
    .line 102
    const v1, 0x7f090147

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    .line 103
    .local v0, "slider":Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;->open()V

    .line 104
    return-void
.end method
