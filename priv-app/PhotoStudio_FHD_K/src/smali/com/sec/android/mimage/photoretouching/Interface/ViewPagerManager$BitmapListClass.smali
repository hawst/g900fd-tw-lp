.class public Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;
.super Ljava/lang/Object;
.source "ViewPagerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BitmapListClass"
.end annotation


# instance fields
.field private MAX_NUM:I

.field private mBgColor:[I

.field private mBitmapList:[Landroid/graphics/Bitmap;

.field private mImageViewList:[Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

.field private mIsValid:[Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;I)V
    .locals 1
    .param p2, "count"    # I

    .prologue
    const/4 v0, 0x0

    .line 565
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    .line 564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 558
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    .line 559
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBgColor:[I

    .line 560
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mIsValid:[Z

    .line 561
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mImageViewList:[Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

    .line 562
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->MAX_NUM:I

    .line 566
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 568
    new-array v0, p2, [Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mImageViewList:[Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

    .line 569
    new-array v0, p2, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    .line 570
    new-array v0, p2, [I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBgColor:[I

    .line 571
    new-array v0, p2, [Z

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mIsValid:[Z

    .line 572
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->MAX_NUM:I

    .line 574
    :cond_0
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 644
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 646
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->MAX_NUM:I

    if-lt v0, v1, :cond_1

    .line 651
    .end local v0    # "i":I
    :cond_0
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    .line 652
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mImageViewList:[Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

    .line 653
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBgColor:[I

    .line 654
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mIsValid:[Z

    .line 655
    return-void

    .line 648
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v0

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v0

    .line 646
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 615
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    aget-object v0, v0, p1

    .line 617
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getImageView(I)Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 606
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mImageViewList:[Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getValid(I)Z
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 621
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mIsValid:[Z

    aget-boolean v0, v0, p1

    return v0
.end method

.method public removeBitmap(I)V
    .locals 2
    .param p1, "idx"    # I

    .prologue
    .line 625
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    aget-object v1, v1, p1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, p1

    .line 626
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mIsValid:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    .line 627
    return-void
.end method

.method public declared-synchronized setBitmap(Landroid/graphics/Bitmap;IIZ)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "color"    # I
    .param p3, "idx"    # I
    .param p4, "isValid"    # Z

    .prologue
    .line 582
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 584
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    aget-object v1, v1, p3

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    aget-object v1, v1, p3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 591
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    aget-object v1, v1, p3

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 592
    .local v0, "mCanvas":Landroid/graphics/Canvas;
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 593
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 599
    .end local v0    # "mCanvas":Landroid/graphics/Canvas;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBgColor:[I

    aput p2, v1, p3

    .line 600
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mIsValid:[Z

    aput-boolean p4, v1, p3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 602
    :cond_0
    monitor-exit p0

    return-void

    .line 597
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    aput-object p1, v1, p3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 582
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setBitmap(Landroid/graphics/Bitmap;IZ)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "idx"    # I
    .param p3, "isValid"    # Z

    .prologue
    .line 577
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->setBitmap(Landroid/graphics/Bitmap;IIZ)V

    .line 578
    return-void
.end method

.method public setImageView(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;I)V
    .locals 1
    .param p1, "imageview"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;
    .param p2, "idx"    # I

    .prologue
    .line 610
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mImageViewList:[Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

    aput-object p1, v0, p2

    .line 611
    return-void
.end method

.method public declared-synchronized validBitmap(II)Z
    .locals 1
    .param p1, "idx"    # I
    .param p2, "color"    # I

    .prologue
    .line 631
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    aget-object v0, v0, p1

    .line 636
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBitmapList:[Landroid/graphics/Bitmap;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->mBgColor:[I

    aget v0, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p2, :cond_0

    .line 637
    const/4 v0, 0x1

    .line 639
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 631
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
