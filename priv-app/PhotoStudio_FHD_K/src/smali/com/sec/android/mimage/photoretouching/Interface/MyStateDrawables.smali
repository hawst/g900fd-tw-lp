.class Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;
.super Landroid/graphics/drawable/StateListDrawable;
.source "DecorationMenuLayoutManager.java"


# instance fields
.field background:Landroid/graphics/drawable/NinePatchDrawable;

.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2092
    invoke-direct {p0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 2090
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;->mContext:Landroid/content/Context;

    .line 2093
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;->mContext:Landroid/content/Context;

    .line 2094
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02058d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;->background:Landroid/graphics/drawable/NinePatchDrawable;

    .line 2096
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a1

    aput v2, v0, v1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;->background:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 2097
    return-void
.end method


# virtual methods
.method protected onStateChange([I)Z
    .locals 5
    .param p1, "stateSet"    # [I

    .prologue
    .line 2100
    const/4 v0, 0x0

    .line 2101
    .local v0, "isClicked":Z
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_0

    .line 2107
    if-eqz v0, :cond_2

    .line 2108
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f04003c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2112
    :goto_1
    invoke-super {p0, p1}, Landroid/graphics/drawable/StateListDrawable;->onStateChange([I)Z

    move-result v2

    return v2

    .line 2101
    :cond_0
    aget v1, p1, v2

    .line 2102
    .local v1, "state":I
    const v4, 0x10100a1

    if-ne v1, v4, :cond_1

    .line 2103
    const/4 v0, 0x1

    .line 2101
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2110
    .end local v1    # "state":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/MyStateDrawables;->clearColorFilter()V

    goto :goto_1
.end method
