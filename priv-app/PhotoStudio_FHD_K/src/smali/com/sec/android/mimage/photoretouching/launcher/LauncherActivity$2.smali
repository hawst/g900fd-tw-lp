.class Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;
.super Ljava/lang/Object;
.source "LauncherActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->initView(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

.field private final synthetic val$button:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->val$button:Landroid/widget/LinearLayout;

    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;)Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    .line 213
    new-instance v1, Landroid/widget/ListPopupWindow;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 214
    .local v1, "popup":Landroid/widget/ListPopupWindow;
    new-array v0, v5, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f060026

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 215
    .local v0, "data":[Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->val$button:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 216
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->val$button:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 217
    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x1090003

    invoke-direct {v2, v3, v4, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 218
    invoke-virtual {v1, v5}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 219
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setInputMethodMode(I)V

    .line 220
    new-instance v2, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2$1;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2$1;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;Landroid/widget/ListPopupWindow;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 231
    invoke-virtual {v1}, Landroid/widget/ListPopupWindow;->show()V

    .line 232
    return-void
.end method
