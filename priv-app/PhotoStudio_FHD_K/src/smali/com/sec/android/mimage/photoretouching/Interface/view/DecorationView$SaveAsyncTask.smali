.class Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;
.super Landroid/os/AsyncTask;
.source "DecorationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V
    .locals 0

    .prologue
    .line 2186
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    .line 2185
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2187
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 9
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 2190
    const/4 v3, 0x0

    .line 2191
    .local v3, "fileName":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 2192
    array-length v6, p1

    if-lez v6, :cond_0

    .line 2193
    const/4 v6, 0x0

    aget-object v3, p1, v6

    .line 2195
    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getAvailableExternalMemorySize()J

    move-result-wide v0

    .line 2196
    .local v0, "available_memsize":J
    const-wide/32 v6, 0xa00000

    cmp-long v6, v0, v6

    if-gez v6, :cond_3

    .line 2197
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/app/ProgressDialog;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 2198
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/app/ProgressDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2199
    :cond_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 2200
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f06007b

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 2239
    :cond_2
    :goto_0
    return-object v8

    .line 2203
    :cond_3
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 2205
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 2207
    const/4 v4, 0x0

    .line 2210
    .local v4, "filePath":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_7

    .line 2212
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 2213
    .local v5, "tempPath":Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2219
    .end local v5    # "tempPath":Ljava/lang/String;
    :goto_1
    if-nez v3, :cond_4

    .line 2220
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    move-result-object v3

    .line 2222
    :cond_4
    :goto_2
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isDoingThread()Z

    move-result v6

    if-nez v6, :cond_8

    .line 2232
    :cond_5
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 2233
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mCurrentSaveSize:I
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I

    move-result v7

    invoke-virtual {v6, v4, v3, v7}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->saveCurrentImage(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2234
    :cond_6
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 2235
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->setCurrentSavedIndex()V

    goto :goto_0

    .line 2217
    :cond_7
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 2225
    :cond_8
    const-wide/16 v6, 0x12c

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    .line 2226
    const-string v6, "JW decorationView save sleeping"

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2227
    :catch_0
    move-exception v2

    .line 2229
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 8
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const v7, 0x7f0601df

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2255
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2257
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2259
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2260
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 2261
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 2262
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    .line 2265
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/app/ProgressDialog;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2266
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2273
    :cond_2
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 2275
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isPersonalPage()Z

    move-result v3

    if-nez v3, :cond_4

    .line 2277
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    const-string v5, "Studio"

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2278
    .local v2, "text":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 2293
    :goto_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mOptionItemId:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I

    move-result v3

    const v4, 0x7f090156

    if-eq v3, v4, :cond_3

    .line 2294
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mOptionItemId:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I

    move-result v4

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->runOptionItem(I)Z
    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;I)Z

    .line 2295
    :cond_3
    return-void

    .line 2268
    .end local v2    # "text":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2270
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2282
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPrivateSaveFolder()Ljava/lang/String;

    move-result-object v1

    .line 2283
    .local v1, "privateFolder":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2284
    .restart local v2    # "text":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 2289
    .end local v1    # "privateFolder":Ljava/lang/String;
    .end local v2    # "text":Ljava/lang/String;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    const-string v5, "Studio"

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2290
    .restart local v2    # "text":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 2242
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 2243
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Landroid/app/ProgressDialog;)V

    .line 2245
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/app/ProgressDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f06008f

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2246
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/app/ProgressDialog;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 2247
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/app/ProgressDialog;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 2248
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2253
    :goto_0
    return-void

    .line 2249
    :catch_0
    move-exception v0

    .line 2251
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
