.class Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;
.super Ljava/lang/Object;
.source "ColorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ApplyPreviewHandler"
.end annotation


# instance fields
.field private bWating:Z

.field private mIsRunning:Z

.field private mStep:I

.field private mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2985
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    .line 2984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2978
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->mIsRunning:Z

    .line 2979
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->bWating:Z

    .line 2980
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->mStep:I

    .line 2982
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    .line 2986
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    .line 2987
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;)V
    .locals 0

    .prologue
    .line 3021
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->applyEnd()V

    return-void
.end method

.method private applyEnd()V
    .locals 2

    .prologue
    .line 3023
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->invalidateViewsWithThread()V

    .line 3024
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->enableBtnWithTextColor(I)V

    .line 3025
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->mIsRunning:Z

    .line 3027
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->bWating:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mIsDestroying:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3028
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->mStep:I

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->applyStart(I)V

    .line 3029
    :cond_0
    return-void
.end method


# virtual methods
.method public applyStart(I)V
    .locals 9
    .param p1, "step"    # I

    .prologue
    const/4 v1, 0x1

    .line 2994
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->mStep:I

    .line 2995
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->bWating:Z

    .line 2996
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2998
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->mIsRunning:Z

    .line 2999
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->disableBtnWithTextColor(I)V

    .line 3001
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3003
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mViewBitmapForApplyThread:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 3004
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 3003
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 3006
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler$1;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;)V

    .line 3012
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->mStep:I

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewThreadPool;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ThreadCallback;I)V

    .line 3006
    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;->submit(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;)Lcom/sec/android/mimage/photoretouching/util/Future;

    .line 3013
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->bWating:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3020
    :cond_0
    :goto_0
    return-void

    .line 3014
    :catch_0
    move-exception v8

    .line 3015
    .local v8, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v8}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 3032
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    .line 3033
    return-void
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 2990
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$ApplyPreviewHandler;->mIsRunning:Z

    return v0
.end method
