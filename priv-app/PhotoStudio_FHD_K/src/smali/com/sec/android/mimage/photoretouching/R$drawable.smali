.class public final Lcom/sec/android/mimage/photoretouching/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final action_bar_bg:I = 0x7f020000

.field public static final action_bar_divider:I = 0x7f020001

.field public static final action_bar_icon_back:I = 0x7f020002

.field public static final action_bar_icon_back_press:I = 0x7f020003

.field public static final action_bar_icon_brush:I = 0x7f020004

.field public static final action_bar_icon_cancel:I = 0x7f020005

.field public static final action_bar_icon_cancel_press:I = 0x7f020006

.field public static final action_bar_icon_check:I = 0x7f020007

.field public static final action_bar_icon_check_dim:I = 0x7f020008

.field public static final action_bar_icon_check_focus:I = 0x7f020009

.field public static final action_bar_icon_check_press:I = 0x7f02000a

.field public static final action_bar_icon_drawer:I = 0x7f02000b

.field public static final action_bar_icon_drawer_dim:I = 0x7f02000c

.field public static final action_bar_icon_drawer_focus:I = 0x7f02000d

.field public static final action_bar_icon_drawer_press:I = 0x7f02000e

.field public static final action_bar_icon_menu:I = 0x7f02000f

.field public static final action_bar_icon_menu_press:I = 0x7f020010

.field public static final action_bar_icon_redo:I = 0x7f020011

.field public static final action_bar_icon_redo_press:I = 0x7f020012

.field public static final action_bar_icon_save:I = 0x7f020013

.field public static final action_bar_icon_save_press:I = 0x7f020014

.field public static final action_bar_icon_share:I = 0x7f020015

.field public static final action_bar_icon_share_pressed:I = 0x7f020016

.field public static final action_bar_icon_undo:I = 0x7f020017

.field public static final action_bar_icon_undo_press:I = 0x7f020018

.field public static final actionbar_btn_back:I = 0x7f020019

.field public static final actionbar_cancel:I = 0x7f02001a

.field public static final actionbar_done:I = 0x7f02001b

.field public static final actionbar_home:I = 0x7f02001c

.field public static final actionbar_mirror_horizontal:I = 0x7f02001d

.field public static final actionbar_mirror_vertical:I = 0x7f02001e

.field public static final actionbar_overflow_icon:I = 0x7f02001f

.field public static final actionbar_ripple_background_lmr:I = 0x7f020020

.field public static final actionbar_ripple_color:I = 0x7f02059b

.field public static final add_more_pictures_text:I = 0x7f020021

.field public static final adjustment:I = 0x7f020022

.field public static final badge_01:I = 0x7f020023

.field public static final badge_02:I = 0x7f020024

.field public static final badge_03:I = 0x7f020025

.field public static final badge_04:I = 0x7f020026

.field public static final badge_05:I = 0x7f020027

.field public static final badge_06:I = 0x7f020028

.field public static final badge_07:I = 0x7f020029

.field public static final badge_08:I = 0x7f02002a

.field public static final badge_09:I = 0x7f02002b

.field public static final badge_10:I = 0x7f02002c

.field public static final best_face_normal:I = 0x7f02002d

.field public static final bottom_button_bg:I = 0x7f02002e

.field public static final bt_bg:I = 0x7f02002f

.field public static final bt_bg_2:I = 0x7f020030

.field public static final bt_penmode_pen_brush_icon:I = 0x7f020031

.field public static final bt_penmode_pen_chinesebrush_icon:I = 0x7f020032

.field public static final bt_penmode_pen_fountain_icon:I = 0x7f020033

.field public static final bt_penmode_pen_inkpen_icon:I = 0x7f020034

.field public static final bt_penmode_pen_magicpen_icon:I = 0x7f020035

.field public static final bt_penmode_pen_marker_icon:I = 0x7f020036

.field public static final bt_penmode_pen_obliquepen_icon:I = 0x7f020037

.field public static final bt_penmode_pen_pencil_icon:I = 0x7f020038

.field public static final bt_sticker_character:I = 0x7f020039

.field public static final bt_sticker_emotion:I = 0x7f02003a

.field public static final bt_sticker_object:I = 0x7f02003b

.field public static final bt_sticker_stemp:I = 0x7f02003c

.field public static final btn:I = 0x7f02003d

.field public static final btn_01:I = 0x7f02003e

.field public static final btn_action_bar_cancel:I = 0x7f02003f

.field public static final btn_action_bar_done:I = 0x7f020040

.field public static final btn_bg_2:I = 0x7f020041

.field public static final btn_collage_bottom_01_proportions:I = 0x7f020042

.field public static final btn_collage_bottom_02_layout:I = 0x7f020043

.field public static final btn_collage_bottom_03_border:I = 0x7f020044

.field public static final btn_collage_bottom_04_background:I = 0x7f020045

.field public static final btn_collage_colorpicker_color:I = 0x7f020046

.field public static final btn_collage_layout_02_01:I = 0x7f020047

.field public static final btn_collage_layout_02_02:I = 0x7f020048

.field public static final btn_collage_layout_02_03:I = 0x7f020049

.field public static final btn_collage_layout_02_04:I = 0x7f02004a

.field public static final btn_collage_layout_02_05:I = 0x7f02004b

.field public static final btn_collage_layout_02_06:I = 0x7f02004c

.field public static final btn_collage_layout_02_07:I = 0x7f02004d

.field public static final btn_collage_layout_02_08:I = 0x7f02004e

.field public static final btn_collage_layout_02_09:I = 0x7f02004f

.field public static final btn_collage_layout_02_10:I = 0x7f020050

.field public static final btn_collage_layout_03_01:I = 0x7f020051

.field public static final btn_collage_layout_03_02:I = 0x7f020052

.field public static final btn_collage_layout_03_03:I = 0x7f020053

.field public static final btn_collage_layout_03_04:I = 0x7f020054

.field public static final btn_collage_layout_03_05:I = 0x7f020055

.field public static final btn_collage_layout_03_06:I = 0x7f020056

.field public static final btn_collage_layout_03_07:I = 0x7f020057

.field public static final btn_collage_layout_03_08:I = 0x7f020058

.field public static final btn_collage_layout_03_09:I = 0x7f020059

.field public static final btn_collage_layout_03_10:I = 0x7f02005a

.field public static final btn_collage_layout_03_11:I = 0x7f02005b

.field public static final btn_collage_layout_03_12:I = 0x7f02005c

.field public static final btn_collage_layout_03_13:I = 0x7f02005d

.field public static final btn_collage_layout_03_14:I = 0x7f02005e

.field public static final btn_collage_layout_04_01:I = 0x7f02005f

.field public static final btn_collage_layout_04_02:I = 0x7f020060

.field public static final btn_collage_layout_04_03:I = 0x7f020061

.field public static final btn_collage_layout_04_04:I = 0x7f020062

.field public static final btn_collage_layout_04_05:I = 0x7f020063

.field public static final btn_collage_layout_04_06:I = 0x7f020064

.field public static final btn_collage_layout_04_07:I = 0x7f020065

.field public static final btn_collage_layout_04_08:I = 0x7f020066

.field public static final btn_collage_layout_04_09:I = 0x7f020067

.field public static final btn_collage_layout_04_10:I = 0x7f020068

.field public static final btn_collage_layout_04_11:I = 0x7f020069

.field public static final btn_collage_layout_04_12:I = 0x7f02006a

.field public static final btn_collage_layout_04_13:I = 0x7f02006b

.field public static final btn_collage_layout_04_14:I = 0x7f02006c

.field public static final btn_collage_layout_04_15:I = 0x7f02006d

.field public static final btn_collage_layout_05_01:I = 0x7f02006e

.field public static final btn_collage_layout_05_02:I = 0x7f02006f

.field public static final btn_collage_layout_05_03:I = 0x7f020070

.field public static final btn_collage_layout_05_04:I = 0x7f020071

.field public static final btn_collage_layout_05_05:I = 0x7f020072

.field public static final btn_collage_layout_05_06:I = 0x7f020073

.field public static final btn_collage_layout_05_07:I = 0x7f020074

.field public static final btn_collage_layout_05_08:I = 0x7f020075

.field public static final btn_collage_layout_05_09:I = 0x7f020076

.field public static final btn_collage_layout_05_10:I = 0x7f020077

.field public static final btn_collage_layout_05_11:I = 0x7f020078

.field public static final btn_collage_layout_05_12:I = 0x7f020079

.field public static final btn_collage_layout_05_13:I = 0x7f02007a

.field public static final btn_collage_layout_05_14:I = 0x7f02007b

.field public static final btn_collage_layout_05_15:I = 0x7f02007c

.field public static final btn_collage_layout_06_01:I = 0x7f02007d

.field public static final btn_collage_layout_06_02:I = 0x7f02007e

.field public static final btn_collage_layout_06_03:I = 0x7f02007f

.field public static final btn_collage_layout_06_04:I = 0x7f020080

.field public static final btn_collage_layout_06_05:I = 0x7f020081

.field public static final btn_collage_layout_06_06:I = 0x7f020082

.field public static final btn_collage_layout_06_07:I = 0x7f020083

.field public static final btn_collage_layout_06_08:I = 0x7f020084

.field public static final btn_collage_layout_06_09:I = 0x7f020085

.field public static final btn_collage_layout_06_10:I = 0x7f020086

.field public static final btn_collage_layout_06_11:I = 0x7f020087

.field public static final btn_collage_layout_06_12:I = 0x7f020088

.field public static final btn_collage_modify_01_replace:I = 0x7f020089

.field public static final btn_collage_modify_02_remove:I = 0x7f02008a

.field public static final btn_collage_modify_03_effect:I = 0x7f02008b

.field public static final btn_collage_modify_04_rotate:I = 0x7f02008c

.field public static final btn_collage_proportion_01:I = 0x7f02008d

.field public static final btn_collage_proportion_04:I = 0x7f02008e

.field public static final btn_dim:I = 0x7f02008f

.field public static final btn_dim_01:I = 0x7f020090

.field public static final btn_effectmanager_thumbnail_popup:I = 0x7f020091

.field public static final btn_focus:I = 0x7f020092

.field public static final btn_focus_01:I = 0x7f020093

.field public static final btn_focus_bg:I = 0x7f020094

.field public static final btn_main_effect:I = 0x7f020095

.field public static final btn_menu_brush:I = 0x7f020096

.field public static final btn_menu_cancle:I = 0x7f020097

.field public static final btn_menu_crop_rotate:I = 0x7f020098

.field public static final btn_menu_done:I = 0x7f020099

.field public static final btn_menu_mirror_undo:I = 0x7f02009a

.field public static final btn_menu_redo:I = 0x7f02009b

.field public static final btn_menu_save:I = 0x7f02009c

.field public static final btn_menu_selection_circle:I = 0x7f02009d

.field public static final btn_menu_selection_lasso:I = 0x7f02009e

.field public static final btn_menu_selection_square:I = 0x7f02009f

.field public static final btn_menu_share_via:I = 0x7f0200a0

.field public static final btn_menu_show_previous:I = 0x7f0200a1

.field public static final btn_menu_undo:I = 0x7f0200a2

.field public static final btn_press:I = 0x7f0200a3

.field public static final btn_press_01:I = 0x7f0200a4

.field public static final btn_press_bg:I = 0x7f0200a5

.field public static final btn_submenu_airbrush_face:I = 0x7f0200a6

.field public static final btn_submenu_effect_download:I = 0x7f0200a7

.field public static final btn_submenu_face_brightness:I = 0x7f0200a8

.field public static final btn_submenu_icon_popup:I = 0x7f0200a9

.field public static final btn_submenu_icon_popup_check:I = 0x7f0200aa

.field public static final btn_submenu_middle_btn_text_color:I = 0x7f0200ab

.field public static final btn_submenu_out_of_focus:I = 0x7f0200ac

.field public static final btn_submenu_popup_icon_text_color:I = 0x7f0200ad

.field public static final btn_submenu_red_eye_fix:I = 0x7f0200ae

.field public static final btn_submenu_thumbnail_popup:I = 0x7f0200af

.field public static final btn_submenu_tone_auto_tune:I = 0x7f0200b0

.field public static final btn_submenu_tone_blue_effect:I = 0x7f0200b1

.field public static final btn_submenu_tone_brightness:I = 0x7f0200b2

.field public static final btn_submenu_tone_contrast:I = 0x7f0200b3

.field public static final btn_submenu_tone_green_effect:I = 0x7f0200b4

.field public static final btn_submenu_tone_hue:I = 0x7f0200b5

.field public static final btn_submenu_tone_red_effect:I = 0x7f0200b6

.field public static final btn_submenu_tone_saturation:I = 0x7f0200b7

.field public static final btn_submenu_tone_temperature:I = 0x7f0200b8

.field public static final btn_submenu_tone_yellowglow:I = 0x7f0200b9

.field public static final btn_thumbnail:I = 0x7f0200ba

.field public static final camera_info_casual_fun_01:I = 0x7f0200bb

.field public static final camera_info_casual_fun_01_thumbnail:I = 0x7f0200bc

.field public static final camera_info_casual_fun_02:I = 0x7f0200bd

.field public static final camera_info_casual_fun_02_thumbnail:I = 0x7f0200be

.field public static final camera_info_casual_fun_03:I = 0x7f0200bf

.field public static final camera_info_casual_fun_03_thumbnail:I = 0x7f0200c0

.field public static final camera_info_label_01:I = 0x7f0200c1

.field public static final camera_info_label_01_thumbnail:I = 0x7f0200c2

.field public static final camera_info_label_02:I = 0x7f0200c3

.field public static final camera_info_label_02_thumbnail:I = 0x7f0200c4

.field public static final camera_info_label_03:I = 0x7f0200c5

.field public static final camera_info_label_03_thumbnail:I = 0x7f0200c6

.field public static final camera_info_label_04:I = 0x7f0200c7

.field public static final camera_info_label_04_thumbnail:I = 0x7f0200c8

.field public static final camera_info_label_05:I = 0x7f0200c9

.field public static final camera_info_label_05_thumbnail:I = 0x7f0200ca

.field public static final camera_info_label_06:I = 0x7f0200cb

.field public static final camera_info_label_06_thumbnail:I = 0x7f0200cc

.field public static final camera_info_modern_01:I = 0x7f0200cd

.field public static final camera_info_modern_01_thumbnail:I = 0x7f0200ce

.field public static final camera_info_modern_02:I = 0x7f0200cf

.field public static final camera_info_modern_02_thumbnail:I = 0x7f0200d0

.field public static final camera_info_modern_03:I = 0x7f0200d1

.field public static final camera_info_modern_03_thumbnail:I = 0x7f0200d2

.field public static final camera_info_modern_04:I = 0x7f0200d3

.field public static final camera_info_modern_04_thumbnail:I = 0x7f0200d4

.field public static final canvas_canvas7:I = 0x7f0200d5

.field public static final clipboard_btn_normal:I = 0x7f0200d6

.field public static final clipboard_panel_button:I = 0x7f0200d7

.field public static final clipboard_panel_button_light_port:I = 0x7f0200d8

.field public static final clipboard_panel_button_port:I = 0x7f0200d9

.field public static final clipboard_panel_light:I = 0x7f0200da

.field public static final clipboard_panel_toggle:I = 0x7f0200db

.field public static final clipboard_panel_toggle_01:I = 0x7f0200dc

.field public static final clipboard_panel_toggle_h:I = 0x7f0200dd

.field public static final clipboard_panel_toggle_h_light:I = 0x7f0200de

.field public static final clipboard_panel_toggle_h_press:I = 0x7f0200df

.field public static final clipboard_panel_toggle_h_press_light:I = 0x7f0200e0

.field public static final clipboard_panel_toggle_light:I = 0x7f0200e1

.field public static final clipboard_panel_toggle_press:I = 0x7f0200e2

.field public static final clipboard_panel_toggle_press_light:I = 0x7f0200e3

.field public static final collage_bg_01_line_01:I = 0x7f0200e4

.field public static final collage_bg_01_line_02:I = 0x7f0200e5

.field public static final collage_bg_01_line_03:I = 0x7f0200e6

.field public static final collage_bg_01_line_04:I = 0x7f0200e7

.field public static final collage_bg_01_line_05:I = 0x7f0200e8

.field public static final collage_bg_01_line_06:I = 0x7f0200e9

.field public static final collage_bg_01_line_07:I = 0x7f0200ea

.field public static final collage_bg_01_line_08:I = 0x7f0200eb

.field public static final collage_bg_02_diagonal_01:I = 0x7f0200ec

.field public static final collage_bg_02_diagonal_02:I = 0x7f0200ed

.field public static final collage_bg_02_diagonal_03:I = 0x7f0200ee

.field public static final collage_bg_02_diagonal_04:I = 0x7f0200ef

.field public static final collage_bg_03_texture_01:I = 0x7f0200f0

.field public static final collage_bg_03_texture_02:I = 0x7f0200f1

.field public static final collage_bg_03_texture_03:I = 0x7f0200f2

.field public static final collage_bg_03_texture_04:I = 0x7f0200f3

.field public static final collage_bg_03_texture_05:I = 0x7f0200f4

.field public static final collage_bg_03_texture_06:I = 0x7f0200f5

.field public static final collage_bg_03_texture_07:I = 0x7f0200f6

.field public static final collage_bg_04_casualfun_01:I = 0x7f0200f7

.field public static final collage_bg_04_casualfun_02:I = 0x7f0200f8

.field public static final collage_bg_04_casualfun_03:I = 0x7f0200f9

.field public static final collage_bg_04_casualfun_04:I = 0x7f0200fa

.field public static final collage_bg_04_casualfun_05:I = 0x7f0200fb

.field public static final collage_bg_04_casualfun_06:I = 0x7f0200fc

.field public static final collage_bg_04_casualfun_07:I = 0x7f0200fd

.field public static final collage_bg_04_casualfun_08:I = 0x7f0200fe

.field public static final collage_bg_04_casualfun_16_9_02:I = 0x7f0200ff

.field public static final collage_bg_04_casualfun_16_9_04:I = 0x7f020100

.field public static final collage_bg_04_casualfun_16_9_05:I = 0x7f020101

.field public static final collage_bg_04_casualfun_16_9_08:I = 0x7f020102

.field public static final collage_bg_thumb_00_img_00:I = 0x7f020103

.field public static final collage_bg_thumb_01_line_01:I = 0x7f020104

.field public static final collage_bg_thumb_01_line_02:I = 0x7f020105

.field public static final collage_bg_thumb_01_line_03:I = 0x7f020106

.field public static final collage_bg_thumb_01_line_04:I = 0x7f020107

.field public static final collage_bg_thumb_01_line_05:I = 0x7f020108

.field public static final collage_bg_thumb_01_line_06:I = 0x7f020109

.field public static final collage_bg_thumb_01_line_07:I = 0x7f02010a

.field public static final collage_bg_thumb_01_line_08:I = 0x7f02010b

.field public static final collage_bg_thumb_02_diagonal_01:I = 0x7f02010c

.field public static final collage_bg_thumb_02_diagonal_02:I = 0x7f02010d

.field public static final collage_bg_thumb_02_diagonal_03:I = 0x7f02010e

.field public static final collage_bg_thumb_02_diagonal_04:I = 0x7f02010f

.field public static final collage_bg_thumb_03_texture_01:I = 0x7f020110

.field public static final collage_bg_thumb_03_texture_02:I = 0x7f020111

.field public static final collage_bg_thumb_03_texture_03:I = 0x7f020112

.field public static final collage_bg_thumb_03_texture_04:I = 0x7f020113

.field public static final collage_bg_thumb_03_texture_05:I = 0x7f020114

.field public static final collage_bg_thumb_03_texture_06:I = 0x7f020115

.field public static final collage_bg_thumb_03_texture_07:I = 0x7f020116

.field public static final collage_bg_thumb_04_casualfun_01:I = 0x7f020117

.field public static final collage_bg_thumb_04_casualfun_02:I = 0x7f020118

.field public static final collage_bg_thumb_04_casualfun_03:I = 0x7f020119

.field public static final collage_bg_thumb_04_casualfun_04:I = 0x7f02011a

.field public static final collage_bg_thumb_04_casualfun_05:I = 0x7f02011b

.field public static final collage_bg_thumb_04_casualfun_06:I = 0x7f02011c

.field public static final collage_bg_thumb_04_casualfun_07:I = 0x7f02011d

.field public static final collage_bg_thumb_04_casualfun_08:I = 0x7f02011e

.field public static final collage_border_icon_round:I = 0x7f02011f

.field public static final collage_border_icon_spacing:I = 0x7f020120

.field public static final collage_color_popup:I = 0x7f020121

.field public static final collage_mini_popup_ic_effect:I = 0x7f020122

.field public static final collage_mini_popup_ic_remove:I = 0x7f020123

.field public static final collage_mini_popup_ic_replace:I = 0x7f020124

.field public static final collage_mini_popup_ic_rotate:I = 0x7f020125

.field public static final collage_photo_frame_1to1_001_01:I = 0x7f020126

.field public static final collage_photo_frame_1to1_001_02:I = 0x7f020127

.field public static final collage_photo_frame_1to1_001_03:I = 0x7f020128

.field public static final collage_photo_frame_1to1_001_04:I = 0x7f020129

.field public static final collage_photo_frame_1to1_001_05:I = 0x7f02012a

.field public static final collage_photo_frame_1to1_002_01:I = 0x7f02012b

.field public static final collage_photo_frame_1to1_002_02:I = 0x7f02012c

.field public static final collage_photo_frame_1to1_002_03:I = 0x7f02012d

.field public static final collage_photo_frame_1to1_002_04:I = 0x7f02012e

.field public static final collage_photo_frame_1to1_002_05:I = 0x7f02012f

.field public static final collage_photo_frame_1to1_003_01:I = 0x7f020130

.field public static final collage_photo_frame_1to1_003_02:I = 0x7f020131

.field public static final collage_photo_frame_1to1_003_03:I = 0x7f020132

.field public static final collage_photo_frame_1to1_003_04:I = 0x7f020133

.field public static final collage_photo_frame_1to1_003_05:I = 0x7f020134

.field public static final collage_photo_frame_1to1_004_01:I = 0x7f020135

.field public static final collage_photo_frame_1to1_004_02:I = 0x7f020136

.field public static final collage_photo_frame_1to1_004_03:I = 0x7f020137

.field public static final collage_photo_frame_1to1_004_04:I = 0x7f020138

.field public static final collage_photo_frame_1to1_004_05:I = 0x7f020139

.field public static final collage_photo_frame_9to16_001_01:I = 0x7f02013a

.field public static final collage_photo_frame_9to16_001_02:I = 0x7f02013b

.field public static final collage_photo_frame_9to16_001_03:I = 0x7f02013c

.field public static final collage_photo_frame_9to16_001_04:I = 0x7f02013d

.field public static final collage_photo_frame_9to16_001_05:I = 0x7f02013e

.field public static final collage_photo_frame_9to16_002_01:I = 0x7f02013f

.field public static final collage_photo_frame_9to16_002_02:I = 0x7f020140

.field public static final collage_photo_frame_9to16_002_03:I = 0x7f020141

.field public static final collage_photo_frame_9to16_002_04:I = 0x7f020142

.field public static final collage_photo_frame_9to16_002_05:I = 0x7f020143

.field public static final collage_photo_frame_9to16_003_01:I = 0x7f020144

.field public static final collage_photo_frame_9to16_003_02:I = 0x7f020145

.field public static final collage_photo_frame_9to16_003_03:I = 0x7f020146

.field public static final collage_photo_frame_9to16_003_04:I = 0x7f020147

.field public static final collage_photo_frame_9to16_003_05:I = 0x7f020148

.field public static final collage_photo_frame_9to16_004_01:I = 0x7f020149

.field public static final collage_photo_frame_9to16_004_02:I = 0x7f02014a

.field public static final collage_photo_frame_9to16_004_03:I = 0x7f02014b

.field public static final collage_photo_frame_9to16_004_04:I = 0x7f02014c

.field public static final collage_photo_frame_9to16_004_05:I = 0x7f02014d

.field public static final collage_photo_pattern_bg_1to1_01:I = 0x7f02014e

.field public static final collage_photo_pattern_bg_1to1_02:I = 0x7f02014f

.field public static final collage_photo_pattern_bg_1to1_03:I = 0x7f020150

.field public static final collage_photo_pattern_bg_1to1_04:I = 0x7f020151

.field public static final collage_photo_pattern_bg_9to16_01:I = 0x7f020152

.field public static final collage_photo_pattern_bg_9to16_02:I = 0x7f020153

.field public static final collage_photo_pattern_bg_9to16_03:I = 0x7f020154

.field public static final collage_photo_pattern_bg_9to16_04:I = 0x7f020155

.field public static final collage_pic_focus:I = 0x7f020156

.field public static final collage_popup_proportion_01:I = 0x7f020157

.field public static final collage_popup_proportion_01_press:I = 0x7f020158

.field public static final collage_popup_proportion_02:I = 0x7f020159

.field public static final collage_popup_proportion_02_press:I = 0x7f02015a

.field public static final collage_popup_proportion_03:I = 0x7f02015b

.field public static final collage_popup_proportion_03_press:I = 0x7f02015c

.field public static final collage_popup_proportion_04:I = 0x7f02015d

.field public static final collage_popup_proportion_04_press:I = 0x7f02015e

.field public static final color:I = 0x7f02015f

.field public static final copy_to_another:I = 0x7f020160

.field public static final copy_to_image:I = 0x7f020161

.field public static final copy_to_original:I = 0x7f020162

.field public static final crop:I = 0x7f020163

.field public static final crop_fourbythree:I = 0x7f020164

.field public static final crop_free:I = 0x7f020165

.field public static final crop_handler:I = 0x7f020166

.field public static final crop_handler_delete:I = 0x7f020167

.field public static final crop_ninebysixteen:I = 0x7f020168

.field public static final crop_onebyone:I = 0x7f020169

.field public static final crop_rect:I = 0x7f02016a

.field public static final crop_sixteenbynine:I = 0x7f02016b

.field public static final crop_threebyfour:I = 0x7f02016c

.field public static final day_info_casual_fun_01:I = 0x7f02016d

.field public static final day_info_casual_fun_01_thumbnail:I = 0x7f02016e

.field public static final day_info_casual_fun_02:I = 0x7f02016f

.field public static final day_info_casual_fun_02_thumbnail:I = 0x7f020170

.field public static final day_info_casual_fun_03:I = 0x7f020171

.field public static final day_info_casual_fun_03_thumbnail:I = 0x7f020172

.field public static final day_info_casual_fun_04:I = 0x7f020173

.field public static final day_info_casual_fun_04_thumbnail:I = 0x7f020174

.field public static final day_info_label_01:I = 0x7f020175

.field public static final day_info_label_01_thumbnail:I = 0x7f020176

.field public static final day_info_label_02:I = 0x7f020177

.field public static final day_info_label_02_thumbnail:I = 0x7f020178

.field public static final day_info_label_03:I = 0x7f020179

.field public static final day_info_label_03_thumbnail:I = 0x7f02017a

.field public static final day_info_label_04:I = 0x7f02017b

.field public static final day_info_label_04_thumbnail:I = 0x7f02017c

.field public static final day_info_label_05:I = 0x7f02017d

.field public static final day_info_label_05_thumbnail:I = 0x7f02017e

.field public static final day_info_label_06:I = 0x7f02017f

.field public static final day_info_label_06_thumbnail:I = 0x7f020180

.field public static final day_info_label_07:I = 0x7f020181

.field public static final day_info_label_07_thumbnail:I = 0x7f020182

.field public static final day_info_label_08:I = 0x7f020183

.field public static final day_info_label_08_thumbnail:I = 0x7f020184

.field public static final day_info_label_09:I = 0x7f020185

.field public static final day_info_label_09_thumbnail:I = 0x7f020186

.field public static final day_info_label_10:I = 0x7f020187

.field public static final day_info_label_10_thumbnail:I = 0x7f020188

.field public static final day_info_mordern_01:I = 0x7f020189

.field public static final day_info_mordern_01_thumbnail:I = 0x7f02018a

.field public static final day_info_mordern_04:I = 0x7f02018b

.field public static final day_info_mordern_04_thumbnail:I = 0x7f02018c

.field public static final day_info_mordern_05:I = 0x7f02018d

.field public static final day_info_mordern_05_thumbnail:I = 0x7f02018e

.field public static final day_info_mordern_06:I = 0x7f02018f

.field public static final day_info_mordern_06_thumbnail:I = 0x7f020190

.field public static final deco_frame_graphic:I = 0x7f020191

.field public static final deco_frame_heart:I = 0x7f020192

.field public static final deco_frame_vector:I = 0x7f020193

.field public static final deco_label_tab_image:I = 0x7f020194

.field public static final deco_label_tab_text:I = 0x7f020195

.field public static final deco_recently:I = 0x7f020196

.field public static final deco_stamp_calendar:I = 0x7f020197

.field public static final deco_stamp_camera:I = 0x7f020198

.field public static final deco_tab_background:I = 0x7f020199

.field public static final decoration:I = 0x7f02019a

.field public static final decoration_color_01:I = 0x7f02019b

.field public static final decoration_color_02:I = 0x7f02019c

.field public static final decoration_color_03:I = 0x7f02019d

.field public static final decoration_color_04:I = 0x7f02019e

.field public static final decoration_color_05:I = 0x7f02019f

.field public static final decoration_color_06:I = 0x7f0201a0

.field public static final decoration_color_07:I = 0x7f0201a1

.field public static final decoration_color_08:I = 0x7f0201a2

.field public static final decoration_color_09:I = 0x7f0201a3

.field public static final decoration_color_10:I = 0x7f0201a4

.field public static final decoration_color_11:I = 0x7f0201a5

.field public static final decoration_color_12:I = 0x7f0201a6

.field public static final decoration_color_13:I = 0x7f0201a7

.field public static final decoration_color_14:I = 0x7f0201a8

.field public static final decoration_color_palette:I = 0x7f0201a9

.field public static final decoration_color_palette_picker:I = 0x7f0201aa

.field public static final decoration_color_picker:I = 0x7f0201ab

.field public static final decoration_color_popup:I = 0x7f0201ac

.field public static final decoration_tab_bg:I = 0x7f0201ad

.field public static final decoration_tab_focus:I = 0x7f0201ae

.field public static final decoration_tab_handler_01:I = 0x7f0201af

.field public static final decoration_tab_handler_02:I = 0x7f0201b0

.field public static final drawing_eraser:I = 0x7f0201b1

.field public static final drawing_pen_left:I = 0x7f0201b2

.field public static final drawing_redo_right:I = 0x7f0201b3

.field public static final drawing_undo:I = 0x7f0201b4

.field public static final edit_colour_hue_bg:I = 0x7f0201b5

.field public static final edit_normal_effect_eye:I = 0x7f0201b6

.field public static final edit_popup_bg:I = 0x7f0201b7

.field public static final edit_popup_bg_center:I = 0x7f0201b8

.field public static final edit_popup_focus:I = 0x7f0201b9

.field public static final edit_popup_focus_02:I = 0x7f0201ba

.field public static final edit_popup_press:I = 0x7f0201bb

.field public static final edit_popup_press_02:I = 0x7f0201bc

.field public static final edit_popup_select:I = 0x7f0201bd

.field public static final edit_popup_select_check:I = 0x7f0201be

.field public static final edit_popup_selected:I = 0x7f0201bf

.field public static final edit_popup_selected_02:I = 0x7f0201c0

.field public static final edit_scrubber_control:I = 0x7f0201c1

.field public static final effect:I = 0x7f0201c2

.field public static final effect_manager_actionbar:I = 0x7f0201c3

.field public static final effect_thumbnail_cursor:I = 0x7f0201c4

.field public static final effect_thumbnail_original:I = 0x7f0201c5

.field public static final effect_thumbnail_select_frame:I = 0x7f0201c6

.field public static final filter01_lensflare:I = 0x7f0201c7

.field public static final filter03_stardust:I = 0x7f0201c8

.field public static final filter04_lightstreak:I = 0x7f0201c9

.field public static final filter05_dawncast_ov:I = 0x7f0201ca

.field public static final filter05_dawncast_sc:I = 0x7f0201cb

.field public static final food_01:I = 0x7f0201cc

.field public static final food_02:I = 0x7f0201cd

.field public static final food_03:I = 0x7f0201ce

.field public static final food_04:I = 0x7f0201cf

.field public static final food_05:I = 0x7f0201d0

.field public static final food_06:I = 0x7f0201d1

.field public static final frame:I = 0x7f0201d2

.field public static final frame01_thumbnail:I = 0x7f0201d3

.field public static final frame02_thumbnail:I = 0x7f0201d4

.field public static final frame03_thumbnail:I = 0x7f0201d5

.field public static final frame04_thumbnail:I = 0x7f0201d6

.field public static final frame05_thumbnail:I = 0x7f0201d7

.field public static final frame06_thumbnail:I = 0x7f0201d8

.field public static final frame07_thumbnail:I = 0x7f0201d9

.field public static final frame08_thumbnail:I = 0x7f0201da

.field public static final frame09_thumbnail:I = 0x7f0201db

.field public static final frame10_thumbnail:I = 0x7f0201dc

.field public static final frame_01_bottom:I = 0x7f0201dd

.field public static final frame_01_bottom_9_16:I = 0x7f0201de

.field public static final frame_01_l_bottom:I = 0x7f0201df

.field public static final frame_01_l_left:I = 0x7f0201e0

.field public static final frame_01_l_right:I = 0x7f0201e1

.field public static final frame_01_l_top:I = 0x7f0201e2

.field public static final frame_01_left:I = 0x7f0201e3

.field public static final frame_01_left_9_16:I = 0x7f0201e4

.field public static final frame_01_right:I = 0x7f0201e5

.field public static final frame_01_right_9_16:I = 0x7f0201e6

.field public static final frame_01_top:I = 0x7f0201e7

.field public static final frame_01_top_9_16:I = 0x7f0201e8

.field public static final frame_02_bottom:I = 0x7f0201e9

.field public static final frame_02_bottom_9_16:I = 0x7f0201ea

.field public static final frame_02_l_bottom:I = 0x7f0201eb

.field public static final frame_02_l_left:I = 0x7f0201ec

.field public static final frame_02_l_right:I = 0x7f0201ed

.field public static final frame_02_l_top:I = 0x7f0201ee

.field public static final frame_02_left:I = 0x7f0201ef

.field public static final frame_02_left_9_16:I = 0x7f0201f0

.field public static final frame_02_right:I = 0x7f0201f1

.field public static final frame_02_right_9_16:I = 0x7f0201f2

.field public static final frame_02_top:I = 0x7f0201f3

.field public static final frame_02_top_9_16:I = 0x7f0201f4

.field public static final frame_03_bottom:I = 0x7f0201f5

.field public static final frame_03_bottom_9_16:I = 0x7f0201f6

.field public static final frame_03_l_bottom:I = 0x7f0201f7

.field public static final frame_03_l_left:I = 0x7f0201f8

.field public static final frame_03_l_right:I = 0x7f0201f9

.field public static final frame_03_l_top:I = 0x7f0201fa

.field public static final frame_03_left:I = 0x7f0201fb

.field public static final frame_03_left_9_16:I = 0x7f0201fc

.field public static final frame_03_right:I = 0x7f0201fd

.field public static final frame_03_right_9_16:I = 0x7f0201fe

.field public static final frame_03_top:I = 0x7f0201ff

.field public static final frame_03_top_9_16:I = 0x7f020200

.field public static final frame_04_bottom:I = 0x7f020201

.field public static final frame_04_bottom_9_16:I = 0x7f020202

.field public static final frame_04_l_bottom:I = 0x7f020203

.field public static final frame_04_l_left:I = 0x7f020204

.field public static final frame_04_l_right:I = 0x7f020205

.field public static final frame_04_l_top:I = 0x7f020206

.field public static final frame_04_left:I = 0x7f020207

.field public static final frame_04_left_9_16:I = 0x7f020208

.field public static final frame_04_right:I = 0x7f020209

.field public static final frame_04_right_9_16:I = 0x7f02020a

.field public static final frame_04_top:I = 0x7f02020b

.field public static final frame_04_top_9_16:I = 0x7f02020c

.field public static final frame_05_bottom:I = 0x7f02020d

.field public static final frame_05_bottom_9_16:I = 0x7f02020e

.field public static final frame_05_l_bottom:I = 0x7f02020f

.field public static final frame_05_l_left:I = 0x7f020210

.field public static final frame_05_l_right:I = 0x7f020211

.field public static final frame_05_l_top:I = 0x7f020212

.field public static final frame_05_left:I = 0x7f020213

.field public static final frame_05_left_9_16:I = 0x7f020214

.field public static final frame_05_right:I = 0x7f020215

.field public static final frame_05_right_9_16:I = 0x7f020216

.field public static final frame_05_top:I = 0x7f020217

.field public static final frame_05_top_9_16:I = 0x7f020218

.field public static final frame_06_bottom:I = 0x7f020219

.field public static final frame_06_bottom_9_16:I = 0x7f02021a

.field public static final frame_06_l_bottom:I = 0x7f02021b

.field public static final frame_06_l_left:I = 0x7f02021c

.field public static final frame_06_l_right:I = 0x7f02021d

.field public static final frame_06_l_top:I = 0x7f02021e

.field public static final frame_06_left:I = 0x7f02021f

.field public static final frame_06_left_9_16:I = 0x7f020220

.field public static final frame_06_right:I = 0x7f020221

.field public static final frame_06_right_9_16:I = 0x7f020222

.field public static final frame_06_top:I = 0x7f020223

.field public static final frame_06_top_9_16:I = 0x7f020224

.field public static final frame_07_bottom:I = 0x7f020225

.field public static final frame_07_bottom_9_16:I = 0x7f020226

.field public static final frame_07_l_bottom:I = 0x7f020227

.field public static final frame_07_l_left:I = 0x7f020228

.field public static final frame_07_l_right:I = 0x7f020229

.field public static final frame_07_l_top:I = 0x7f02022a

.field public static final frame_07_left:I = 0x7f02022b

.field public static final frame_07_left_9_16:I = 0x7f02022c

.field public static final frame_07_right:I = 0x7f02022d

.field public static final frame_07_right_9_16:I = 0x7f02022e

.field public static final frame_07_top:I = 0x7f02022f

.field public static final frame_07_top_9_16:I = 0x7f020230

.field public static final frame_08_bottom:I = 0x7f020231

.field public static final frame_08_bottom_9_16:I = 0x7f020232

.field public static final frame_08_effect:I = 0x7f020233

.field public static final frame_08_effect_9_16:I = 0x7f020234

.field public static final frame_08_l_bottom:I = 0x7f020235

.field public static final frame_08_l_effect:I = 0x7f020236

.field public static final frame_08_l_left:I = 0x7f020237

.field public static final frame_08_l_right:I = 0x7f020238

.field public static final frame_08_l_top:I = 0x7f020239

.field public static final frame_08_left:I = 0x7f02023a

.field public static final frame_08_left_9_16:I = 0x7f02023b

.field public static final frame_08_right:I = 0x7f02023c

.field public static final frame_08_right_9_16:I = 0x7f02023d

.field public static final frame_08_top:I = 0x7f02023e

.field public static final frame_08_top_9_16:I = 0x7f02023f

.field public static final frame_09_bottom:I = 0x7f020240

.field public static final frame_09_bottom_9_16:I = 0x7f020241

.field public static final frame_09_l_bottom:I = 0x7f020242

.field public static final frame_09_l_left:I = 0x7f020243

.field public static final frame_09_l_right:I = 0x7f020244

.field public static final frame_09_l_top:I = 0x7f020245

.field public static final frame_09_left:I = 0x7f020246

.field public static final frame_09_left_9_16:I = 0x7f020247

.field public static final frame_09_right:I = 0x7f020248

.field public static final frame_09_right_9_16:I = 0x7f020249

.field public static final frame_09_top:I = 0x7f02024a

.field public static final frame_09_top_9_16:I = 0x7f02024b

.field public static final frame_10_bottom:I = 0x7f02024c

.field public static final frame_10_bottom_9_16:I = 0x7f02024d

.field public static final frame_10_l_bottom:I = 0x7f02024e

.field public static final frame_10_l_left:I = 0x7f02024f

.field public static final frame_10_l_right:I = 0x7f020250

.field public static final frame_10_l_top:I = 0x7f020251

.field public static final frame_10_left:I = 0x7f020252

.field public static final frame_10_left_9_16:I = 0x7f020253

.field public static final frame_10_right:I = 0x7f020254

.field public static final frame_10_right_9_16:I = 0x7f020255

.field public static final frame_10_top:I = 0x7f020256

.field public static final frame_10_top_9_16:I = 0x7f020257

.field public static final frame_pager_color_icon:I = 0x7f020258

.field public static final grid_02_1:I = 0x7f020259

.field public static final grid_02_1_press:I = 0x7f02025a

.field public static final grid_02_2:I = 0x7f02025b

.field public static final grid_02_2_press:I = 0x7f02025c

.field public static final grid_02_3:I = 0x7f02025d

.field public static final grid_02_3_press:I = 0x7f02025e

.field public static final grid_02_4:I = 0x7f02025f

.field public static final grid_02_4_press:I = 0x7f020260

.field public static final grid_02_5:I = 0x7f020261

.field public static final grid_02_5_press:I = 0x7f020262

.field public static final grid_02_6:I = 0x7f020263

.field public static final grid_02_6_press:I = 0x7f020264

.field public static final grid_03_1:I = 0x7f020265

.field public static final grid_03_10:I = 0x7f020266

.field public static final grid_03_10_press:I = 0x7f020267

.field public static final grid_03_1_press:I = 0x7f020268

.field public static final grid_03_2:I = 0x7f020269

.field public static final grid_03_2_press:I = 0x7f02026a

.field public static final grid_03_3:I = 0x7f02026b

.field public static final grid_03_3_press:I = 0x7f02026c

.field public static final grid_03_4:I = 0x7f02026d

.field public static final grid_03_4_press:I = 0x7f02026e

.field public static final grid_03_5:I = 0x7f02026f

.field public static final grid_03_5_press:I = 0x7f020270

.field public static final grid_03_6:I = 0x7f020271

.field public static final grid_03_6_press:I = 0x7f020272

.field public static final grid_03_7:I = 0x7f020273

.field public static final grid_03_7_press:I = 0x7f020274

.field public static final grid_03_8:I = 0x7f020275

.field public static final grid_03_8_press:I = 0x7f020276

.field public static final grid_03_9:I = 0x7f020277

.field public static final grid_03_9_press:I = 0x7f020278

.field public static final grid_04_1:I = 0x7f020279

.field public static final grid_04_10:I = 0x7f02027a

.field public static final grid_04_10_press:I = 0x7f02027b

.field public static final grid_04_11:I = 0x7f02027c

.field public static final grid_04_11_press:I = 0x7f02027d

.field public static final grid_04_1_press:I = 0x7f02027e

.field public static final grid_04_2:I = 0x7f02027f

.field public static final grid_04_2_press:I = 0x7f020280

.field public static final grid_04_3:I = 0x7f020281

.field public static final grid_04_3_press:I = 0x7f020282

.field public static final grid_04_4:I = 0x7f020283

.field public static final grid_04_4_press:I = 0x7f020284

.field public static final grid_04_5:I = 0x7f020285

.field public static final grid_04_5_press:I = 0x7f020286

.field public static final grid_04_6:I = 0x7f020287

.field public static final grid_04_6_press:I = 0x7f020288

.field public static final grid_04_7:I = 0x7f020289

.field public static final grid_04_7_press:I = 0x7f02028a

.field public static final grid_04_8:I = 0x7f02028b

.field public static final grid_04_8_press:I = 0x7f02028c

.field public static final grid_04_9:I = 0x7f02028d

.field public static final grid_04_9_press:I = 0x7f02028e

.field public static final grid_05_1:I = 0x7f02028f

.field public static final grid_05_10:I = 0x7f020290

.field public static final grid_05_10_press:I = 0x7f020291

.field public static final grid_05_11:I = 0x7f020292

.field public static final grid_05_11_press:I = 0x7f020293

.field public static final grid_05_1_press:I = 0x7f020294

.field public static final grid_05_2:I = 0x7f020295

.field public static final grid_05_2_press:I = 0x7f020296

.field public static final grid_05_3:I = 0x7f020297

.field public static final grid_05_3_press:I = 0x7f020298

.field public static final grid_05_4:I = 0x7f020299

.field public static final grid_05_4_press:I = 0x7f02029a

.field public static final grid_05_5:I = 0x7f02029b

.field public static final grid_05_5_press:I = 0x7f02029c

.field public static final grid_05_6:I = 0x7f02029d

.field public static final grid_05_6_press:I = 0x7f02029e

.field public static final grid_05_7:I = 0x7f02029f

.field public static final grid_05_7_press:I = 0x7f0202a0

.field public static final grid_05_8:I = 0x7f0202a1

.field public static final grid_05_8_press:I = 0x7f0202a2

.field public static final grid_05_9:I = 0x7f0202a3

.field public static final grid_05_9_press:I = 0x7f0202a4

.field public static final grid_06_1:I = 0x7f0202a5

.field public static final grid_06_1_press:I = 0x7f0202a6

.field public static final grid_06_2:I = 0x7f0202a7

.field public static final grid_06_2_press:I = 0x7f0202a8

.field public static final grid_06_3:I = 0x7f0202a9

.field public static final grid_06_3_press:I = 0x7f0202aa

.field public static final grid_06_4:I = 0x7f0202ab

.field public static final grid_06_4_press:I = 0x7f0202ac

.field public static final grid_06_5:I = 0x7f0202ad

.field public static final grid_06_5_press:I = 0x7f0202ae

.field public static final grid_06_6:I = 0x7f0202af

.field public static final grid_06_6_press:I = 0x7f0202b0

.field public static final grid_06_7:I = 0x7f0202b1

.field public static final grid_06_7_press:I = 0x7f0202b2

.field public static final grid_06_8:I = 0x7f0202b3

.field public static final grid_06_8_press:I = 0x7f0202b4

.field public static final gridview_item_bg:I = 0x7f0202b5

.field public static final hdpi:I = 0x7f0202b6

.field public static final icon:I = 0x7f0202b7

.field public static final icon_add_to:I = 0x7f0202b8

.field public static final icon_adjustment:I = 0x7f0202b9

.field public static final icon_adjustment_dim:I = 0x7f0202ba

.field public static final icon_adjustment_focus:I = 0x7f0202bb

.field public static final icon_adjustment_press:I = 0x7f0202bc

.field public static final icon_adjustment_selected:I = 0x7f0202bd

.field public static final icon_background:I = 0x7f0202be

.field public static final icon_background_dim:I = 0x7f0202bf

.field public static final icon_background_focus:I = 0x7f0202c0

.field public static final icon_background_press:I = 0x7f0202c1

.field public static final icon_background_selected:I = 0x7f0202c2

.field public static final icon_border:I = 0x7f0202c3

.field public static final icon_border_dim:I = 0x7f0202c4

.field public static final icon_border_focus:I = 0x7f0202c5

.field public static final icon_border_press:I = 0x7f0202c6

.field public static final icon_border_selected:I = 0x7f0202c7

.field public static final icon_brush:I = 0x7f0202c8

.field public static final icon_brush_press:I = 0x7f0202c9

.field public static final icon_decoratoion:I = 0x7f0202ca

.field public static final icon_decoratoion_dim:I = 0x7f0202cb

.field public static final icon_decoratoion_focus:I = 0x7f0202cc

.field public static final icon_decoratoion_press:I = 0x7f0202cd

.field public static final icon_decoratoion_selected:I = 0x7f0202ce

.field public static final icon_effect:I = 0x7f0202cf

.field public static final icon_effect_dim:I = 0x7f0202d0

.field public static final icon_effect_focus:I = 0x7f0202d1

.field public static final icon_effect_press:I = 0x7f0202d2

.field public static final icon_effect_selected:I = 0x7f0202d3

.field public static final icon_inverse:I = 0x7f0202d4

.field public static final icon_lasso:I = 0x7f0202d5

.field public static final icon_lasso_press:I = 0x7f0202d6

.field public static final icon_layout:I = 0x7f0202d7

.field public static final icon_layout_dim:I = 0x7f0202d8

.field public static final icon_layout_focus:I = 0x7f0202d9

.field public static final icon_layout_press:I = 0x7f0202da

.field public static final icon_layout_selected:I = 0x7f0202db

.field public static final icon_magnetic:I = 0x7f0202dc

.field public static final icon_magnetic_press:I = 0x7f0202dd

.field public static final icon_new:I = 0x7f0202de

.field public static final icon_portrait:I = 0x7f0202df

.field public static final icon_portrait_dim:I = 0x7f0202e0

.field public static final icon_portrait_focus:I = 0x7f0202e1

.field public static final icon_portrait_press:I = 0x7f0202e2

.field public static final icon_portrait_selected:I = 0x7f0202e3

.field public static final icon_proportions:I = 0x7f0202e4

.field public static final icon_proportions_dim:I = 0x7f0202e5

.field public static final icon_proportions_focus:I = 0x7f0202e6

.field public static final icon_proportions_press:I = 0x7f0202e7

.field public static final icon_proportions_selected:I = 0x7f0202e8

.field public static final icon_round:I = 0x7f0202e9

.field public static final icon_round_press:I = 0x7f0202ea

.field public static final icon_selection_size_normal:I = 0x7f0202eb

.field public static final icon_selection_size_normal_press:I = 0x7f0202ec

.field public static final icon_square:I = 0x7f0202ed

.field public static final icon_square_press:I = 0x7f0202ee

.field public static final icon_sticker:I = 0x7f0202ef

.field public static final icon_subtract:I = 0x7f0202f0

.field public static final icon_tone:I = 0x7f0202f1

.field public static final icon_tone_dim:I = 0x7f0202f2

.field public static final icon_tone_focus:I = 0x7f0202f3

.field public static final icon_tone_press:I = 0x7f0202f4

.field public static final icon_tone_selected:I = 0x7f0202f5

.field public static final image_sticker:I = 0x7f0202f6

.field public static final label:I = 0x7f0202f7

.field public static final label_01_center:I = 0x7f0202f8

.field public static final label_01_left:I = 0x7f0202f9

.field public static final label_01_right:I = 0x7f0202fa

.field public static final label_02_center:I = 0x7f0202fb

.field public static final label_02_left:I = 0x7f0202fc

.field public static final label_02_right:I = 0x7f0202fd

.field public static final label_03_center:I = 0x7f0202fe

.field public static final label_03_left:I = 0x7f0202ff

.field public static final label_03_right:I = 0x7f020300

.field public static final label_04_center:I = 0x7f020301

.field public static final label_04_left:I = 0x7f020302

.field public static final label_04_right:I = 0x7f020303

.field public static final label_05_center:I = 0x7f020304

.field public static final label_05_left:I = 0x7f020305

.field public static final label_05_right:I = 0x7f020306

.field public static final label_06_center:I = 0x7f020307

.field public static final label_06_left:I = 0x7f020308

.field public static final label_06_right:I = 0x7f020309

.field public static final label_07_center:I = 0x7f02030a

.field public static final label_07_left:I = 0x7f02030b

.field public static final label_07_right:I = 0x7f02030c

.field public static final label_08_center:I = 0x7f02030d

.field public static final label_08_left:I = 0x7f02030e

.field public static final label_08_right:I = 0x7f02030f

.field public static final label_09_center01:I = 0x7f020310

.field public static final label_09_center02:I = 0x7f020311

.field public static final label_09_center03:I = 0x7f020312

.field public static final label_09_left:I = 0x7f020313

.field public static final label_09_right:I = 0x7f020314

.field public static final label_10_center:I = 0x7f020315

.field public static final label_10_left:I = 0x7f020316

.field public static final label_10_right:I = 0x7f020317

.field public static final label_11_center:I = 0x7f020318

.field public static final label_11_left:I = 0x7f020319

.field public static final label_11_right:I = 0x7f02031a

.field public static final label_12_center:I = 0x7f02031b

.field public static final label_12_left:I = 0x7f02031c

.field public static final label_12_right:I = 0x7f02031d

.field public static final launcher_actionbar_multi_select_drop_button:I = 0x7f02031e

.field public static final launcher_thumbnail:I = 0x7f02031f

.field public static final launcher_thumbnail2:I = 0x7f020320

.field public static final list_focused_holo:I = 0x7f020321

.field public static final main_menu_buttons1:I = 0x7f020322

.field public static final main_menu_buttons2:I = 0x7f020323

.field public static final middle_button:I = 0x7f020324

.field public static final middle_button_auto:I = 0x7f020325

.field public static final middle_button_selection:I = 0x7f020326

.field public static final middle_button_text_color:I = 0x7f020327

.field public static final mirror_handler_left:I = 0x7f020328

.field public static final mirror_handler_left_press:I = 0x7f020329

.field public static final mirror_handler_right:I = 0x7f02032a

.field public static final mirror_handler_right_press:I = 0x7f02032b

.field public static final mirror_selection:I = 0x7f02032c

.field public static final mode_downloaded_icon:I = 0x7f02032d

.field public static final no_location_01:I = 0x7f02032e

.field public static final no_location_02:I = 0x7f02032f

.field public static final no_location_03:I = 0x7f020330

.field public static final no_location_04:I = 0x7f020331

.field public static final number_0:I = 0x7f020332

.field public static final number_1:I = 0x7f020333

.field public static final number_2:I = 0x7f020334

.field public static final number_3:I = 0x7f020335

.field public static final number_4:I = 0x7f020336

.field public static final number_5:I = 0x7f020337

.field public static final number_6:I = 0x7f020338

.field public static final number_7:I = 0x7f020339

.field public static final number_8:I = 0x7f02033a

.field public static final number_9:I = 0x7f02033b

.field public static final option_box_bg:I = 0x7f02033c

.field public static final overlay_help_button:I = 0x7f02033d

.field public static final overlay_help_button_focus:I = 0x7f02033e

.field public static final overlay_help_button_press:I = 0x7f02033f

.field public static final overlay_help_point:I = 0x7f020340

.field public static final overlay_help_point_down:I = 0x7f020341

.field public static final overlay_help_point_up:I = 0x7f020342

.field public static final overlay_help_popup_selector_ok:I = 0x7f020343

.field public static final overscroll_edge:I = 0x7f020344

.field public static final overscroll_glow:I = 0x7f020345

.field public static final pe_add_pic:I = 0x7f020346

.field public static final pe_bg_deco_open:I = 0x7f020347

.field public static final pe_color_adjust_b:I = 0x7f020348

.field public static final pe_color_adjust_g:I = 0x7f020349

.field public static final pe_color_adjust_r:I = 0x7f02034a

.field public static final pe_color_select_box:I = 0x7f02034b

.field public static final pe_color_select_kit:I = 0x7f02034c

.field public static final pe_deco_arrow_down_popup:I = 0x7f02034d

.field public static final pe_deco_arrow_up_popup:I = 0x7f02034e

.field public static final pe_deco_bg_popup:I = 0x7f02034f

.field public static final pe_deco_bg_popup_inside:I = 0x7f020350

.field public static final pe_deco_bg_popup_pen_preview:I = 0x7f020351

.field public static final pe_ic_camera_nomal:I = 0x7f020352

.field public static final pe_ic_pointer:I = 0x7f020353

.field public static final pe_ic_pointeron:I = 0x7f020354

.field public static final pe_ic_selection_mode:I = 0x7f020355

.field public static final pe_ic_selectpicture:I = 0x7f020356

.field public static final pe_ic_selectpicture_nomal:I = 0x7f020357

.field public static final pe_ic_takepicture:I = 0x7f020358

.field public static final pe_launcher_btn:I = 0x7f020359

.field public static final pe_launcher_btn_nor:I = 0x7f02035a

.field public static final pe_minus:I = 0x7f02035b

.field public static final pe_multi_grid_focused:I = 0x7f02035c

.field public static final pe_pen_circle_bg:I = 0x7f02035d

.field public static final pe_pen_circle_big_01:I = 0x7f02035e

.field public static final pe_picture_tray:I = 0x7f02035f

.field public static final pe_picture_tray_h:I = 0x7f020360

.field public static final pe_progress_bg:I = 0x7f020361

.field public static final pe_progress_primary:I = 0x7f020362

.field public static final pe_save:I = 0x7f020363

.field public static final pe_selectionsize_circle:I = 0x7f020364

.field public static final pe_tray_bg_l:I = 0x7f020365

.field public static final pe_tray_focus:I = 0x7f020366

.field public static final pen:I = 0x7f020367

.field public static final people_04:I = 0x7f020368

.field public static final people_05:I = 0x7f020369

.field public static final people_06:I = 0x7f02036a

.field public static final people_07:I = 0x7f02036b

.field public static final people_08:I = 0x7f02036c

.field public static final people_10:I = 0x7f02036d

.field public static final photo_adjustment_degree:I = 0x7f02036e

.field public static final photo_adjustment_icon_crop:I = 0x7f02036f

.field public static final photo_adjustment_icon_resize:I = 0x7f020370

.field public static final photo_adjustment_icon_rotate:I = 0x7f020371

.field public static final photo_btn_bg:I = 0x7f020372

.field public static final photo_btn_divider:I = 0x7f020373

.field public static final photo_crop_handler:I = 0x7f020374

.field public static final photo_crop_handler_cancel:I = 0x7f020375

.field public static final photo_crop_handler_cancel_press:I = 0x7f020376

.field public static final photo_crop_handler_press:I = 0x7f020377

.field public static final photo_crop_handler_rotate:I = 0x7f020378

.field public static final photo_crop_handler_rotate_press:I = 0x7f020379

.field public static final photo_deco_icon_color_popup_off:I = 0x7f02037a

.field public static final photo_deco_icon_color_popup_on:I = 0x7f02037b

.field public static final photo_deco_label_01:I = 0x7f02037c

.field public static final photo_deco_label_02:I = 0x7f02037d

.field public static final photo_deco_label_03:I = 0x7f02037e

.field public static final photo_deco_label_04:I = 0x7f02037f

.field public static final photo_deco_label_05:I = 0x7f020380

.field public static final photo_deco_label_06:I = 0x7f020381

.field public static final photo_deco_label_07:I = 0x7f020382

.field public static final photo_deco_label_08:I = 0x7f020383

.field public static final photo_deco_label_09:I = 0x7f020384

.field public static final photo_deco_label_10:I = 0x7f020385

.field public static final photo_deco_label_11:I = 0x7f020386

.field public static final photo_deco_label_12:I = 0x7f020387

.field public static final photo_deco_label_t_01:I = 0x7f020388

.field public static final photo_deco_label_t_02:I = 0x7f020389

.field public static final photo_deco_label_t_03:I = 0x7f02038a

.field public static final photo_deco_label_t_04:I = 0x7f02038b

.field public static final photo_deco_label_t_05:I = 0x7f02038c

.field public static final photo_deco_label_t_06:I = 0x7f02038d

.field public static final photo_deco_label_t_07:I = 0x7f02038e

.field public static final photo_deco_label_t_08:I = 0x7f02038f

.field public static final photo_deco_label_t_09:I = 0x7f020390

.field public static final photo_deco_label_t_10:I = 0x7f020391

.field public static final photo_deco_label_t_11:I = 0x7f020392

.field public static final photo_deco_label_t_12:I = 0x7f020393

.field public static final photo_detail_frame_01:I = 0x7f020394

.field public static final photo_effect_download:I = 0x7f020395

.field public static final photo_effect_download_btn:I = 0x7f020396

.field public static final photo_effect_manager:I = 0x7f020397

.field public static final photo_effect_popup_divider:I = 0x7f020398

.field public static final photo_effect_recent_icon:I = 0x7f020399

.field public static final photo_frame_01:I = 0x7f02039a

.field public static final photo_frame_02:I = 0x7f02039b

.field public static final photo_frame_03:I = 0x7f02039c

.field public static final photo_frame_04:I = 0x7f02039d

.field public static final photo_frame_05:I = 0x7f02039e

.field public static final photo_frame_06:I = 0x7f02039f

.field public static final photo_frame_07:I = 0x7f0203a0

.field public static final photo_frame_08:I = 0x7f0203a1

.field public static final photo_frame_basic_01:I = 0x7f0203a2

.field public static final photo_frame_basic_02:I = 0x7f0203a3

.field public static final photo_frame_basic_03:I = 0x7f0203a4

.field public static final photo_frame_basic_04:I = 0x7f0203a5

.field public static final photo_frame_basic_05:I = 0x7f0203a6

.field public static final photo_frame_basic_06:I = 0x7f0203a7

.field public static final photo_frame_basic_07:I = 0x7f0203a8

.field public static final photo_frame_basic_thumbnail_01:I = 0x7f0203a9

.field public static final photo_frame_basic_thumbnail_02:I = 0x7f0203aa

.field public static final photo_frame_basic_thumbnail_03:I = 0x7f0203ab

.field public static final photo_frame_basic_thumbnail_04:I = 0x7f0203ac

.field public static final photo_frame_basic_thumbnail_05:I = 0x7f0203ad

.field public static final photo_frame_basic_thumbnail_06:I = 0x7f0203ae

.field public static final photo_frame_basic_thumbnail_07:I = 0x7f0203af

.field public static final photo_frame_special_01:I = 0x7f0203b0

.field public static final photo_frame_special_01_thumbnail:I = 0x7f0203b1

.field public static final photo_frame_special_02:I = 0x7f0203b2

.field public static final photo_frame_special_02_thumbnail:I = 0x7f0203b3

.field public static final photo_frame_special_03:I = 0x7f0203b4

.field public static final photo_frame_special_03_thumbnail:I = 0x7f0203b5

.field public static final photo_frame_special_04:I = 0x7f0203b6

.field public static final photo_frame_special_04_thumbnail:I = 0x7f0203b7

.field public static final photo_frame_special_05:I = 0x7f0203b8

.field public static final photo_frame_special_05_thumbnail:I = 0x7f0203b9

.field public static final photo_frame_special_06:I = 0x7f0203ba

.field public static final photo_frame_special_06_thumbnail:I = 0x7f0203bb

.field public static final photo_frame_special_07:I = 0x7f0203bc

.field public static final photo_frame_special_07_thumbnail:I = 0x7f0203bd

.field public static final photo_frame_special_08:I = 0x7f0203be

.field public static final photo_frame_special_08_thumbnail:I = 0x7f0203bf

.field public static final photo_frame_special_09:I = 0x7f0203c0

.field public static final photo_frame_special_09_thumbnail:I = 0x7f0203c1

.field public static final photo_frame_special_10:I = 0x7f0203c2

.field public static final photo_frame_special_10_thumbnail:I = 0x7f0203c3

.field public static final photo_frame_special_11:I = 0x7f0203c4

.field public static final photo_frame_special_11_thumbnail:I = 0x7f0203c5

.field public static final photo_frame_special_12:I = 0x7f0203c6

.field public static final photo_frame_special_12_thumbnail:I = 0x7f0203c7

.field public static final photo_gauge_bg:I = 0x7f0203c8

.field public static final photo_gauge_divider:I = 0x7f0203c9

.field public static final photo_gauge_masking:I = 0x7f0203ca

.field public static final photo_gauge_progress:I = 0x7f0203cb

.field public static final photo_icon_crop_01:I = 0x7f0203cc

.field public static final photo_icon_crop_01_dim:I = 0x7f0203cd

.field public static final photo_icon_crop_01_focus:I = 0x7f0203ce

.field public static final photo_icon_crop_01_press:I = 0x7f0203cf

.field public static final photo_icon_crop_01_selected:I = 0x7f0203d0

.field public static final photo_icon_crop_02:I = 0x7f0203d1

.field public static final photo_icon_crop_02_dim:I = 0x7f0203d2

.field public static final photo_icon_crop_02_focus:I = 0x7f0203d3

.field public static final photo_icon_crop_02_press:I = 0x7f0203d4

.field public static final photo_icon_crop_02_selected:I = 0x7f0203d5

.field public static final photo_icon_crop_03:I = 0x7f0203d6

.field public static final photo_icon_crop_03_a_focus:I = 0x7f0203d7

.field public static final photo_icon_crop_03_a_press:I = 0x7f0203d8

.field public static final photo_icon_crop_03_b_focus:I = 0x7f0203d9

.field public static final photo_icon_crop_03_b_press:I = 0x7f0203da

.field public static final photo_icon_crop_03_dim:I = 0x7f0203db

.field public static final photo_icon_crop_03_selected_a:I = 0x7f0203dc

.field public static final photo_icon_crop_03_selected_b:I = 0x7f0203dd

.field public static final photo_icon_crop_04:I = 0x7f0203de

.field public static final photo_icon_crop_04_a_focus:I = 0x7f0203df

.field public static final photo_icon_crop_04_a_press:I = 0x7f0203e0

.field public static final photo_icon_crop_04_b_focus:I = 0x7f0203e1

.field public static final photo_icon_crop_04_b_press:I = 0x7f0203e2

.field public static final photo_icon_crop_04_dim:I = 0x7f0203e3

.field public static final photo_icon_crop_04_selected_a:I = 0x7f0203e4

.field public static final photo_icon_crop_04_selected_b:I = 0x7f0203e5

.field public static final photo_icon_crop_05:I = 0x7f0203e6

.field public static final photo_icon_crop_05_dim:I = 0x7f0203e7

.field public static final photo_icon_crop_05_focus:I = 0x7f0203e8

.field public static final photo_icon_crop_05_press:I = 0x7f0203e9

.field public static final photo_icon_crop_05_selected:I = 0x7f0203ea

.field public static final photo_icon_deco_01:I = 0x7f0203eb

.field public static final photo_icon_deco_01_dim:I = 0x7f0203ec

.field public static final photo_icon_deco_01_focus:I = 0x7f0203ed

.field public static final photo_icon_deco_01_press:I = 0x7f0203ee

.field public static final photo_icon_deco_01_selected:I = 0x7f0203ef

.field public static final photo_icon_deco_02:I = 0x7f0203f0

.field public static final photo_icon_deco_02_dim:I = 0x7f0203f1

.field public static final photo_icon_deco_02_focus:I = 0x7f0203f2

.field public static final photo_icon_deco_02_press:I = 0x7f0203f3

.field public static final photo_icon_deco_02_selected:I = 0x7f0203f4

.field public static final photo_icon_deco_03:I = 0x7f0203f5

.field public static final photo_icon_deco_03_dim:I = 0x7f0203f6

.field public static final photo_icon_deco_03_focus:I = 0x7f0203f7

.field public static final photo_icon_deco_03_press:I = 0x7f0203f8

.field public static final photo_icon_deco_03_selected:I = 0x7f0203f9

.field public static final photo_icon_deco_04:I = 0x7f0203fa

.field public static final photo_icon_deco_04_dim:I = 0x7f0203fb

.field public static final photo_icon_deco_04_focus:I = 0x7f0203fc

.field public static final photo_icon_deco_04_press:I = 0x7f0203fd

.field public static final photo_icon_deco_04_selected:I = 0x7f0203fe

.field public static final photo_icon_deco_05:I = 0x7f0203ff

.field public static final photo_icon_deco_05_dim:I = 0x7f020400

.field public static final photo_icon_deco_05_focus:I = 0x7f020401

.field public static final photo_icon_deco_05_press:I = 0x7f020402

.field public static final photo_icon_deco_05_selected:I = 0x7f020403

.field public static final photo_icon_deco_06:I = 0x7f020404

.field public static final photo_icon_deco_06_dim:I = 0x7f020405

.field public static final photo_icon_deco_06_focus:I = 0x7f020406

.field public static final photo_icon_deco_06_press:I = 0x7f020407

.field public static final photo_icon_deco_06_selected:I = 0x7f020408

.field public static final photo_icon_deco_07:I = 0x7f020409

.field public static final photo_icon_deco_07_dim:I = 0x7f02040a

.field public static final photo_icon_deco_07_focus:I = 0x7f02040b

.field public static final photo_icon_deco_07_press:I = 0x7f02040c

.field public static final photo_icon_deco_07_selected:I = 0x7f02040d

.field public static final photo_icon_deco_08:I = 0x7f02040e

.field public static final photo_icon_deco_08_dim:I = 0x7f02040f

.field public static final photo_icon_deco_08_focus:I = 0x7f020410

.field public static final photo_icon_deco_08_press:I = 0x7f020411

.field public static final photo_icon_deco_08_selected:I = 0x7f020412

.field public static final photo_icon_deco_09:I = 0x7f020413

.field public static final photo_icon_deco_09_dim:I = 0x7f020414

.field public static final photo_icon_deco_09_focus:I = 0x7f020415

.field public static final photo_icon_deco_09_press:I = 0x7f020416

.field public static final photo_icon_deco_09_selected:I = 0x7f020417

.field public static final photo_icon_deco_10:I = 0x7f020418

.field public static final photo_icon_deco_10_dim:I = 0x7f020419

.field public static final photo_icon_deco_10_focus:I = 0x7f02041a

.field public static final photo_icon_deco_10_press:I = 0x7f02041b

.field public static final photo_icon_deco_10_selected:I = 0x7f02041c

.field public static final photo_icon_deco_eraser:I = 0x7f02041d

.field public static final photo_icon_deco_eraser_dim:I = 0x7f02041e

.field public static final photo_icon_deco_eraser_focus:I = 0x7f02041f

.field public static final photo_icon_deco_eraser_press:I = 0x7f020420

.field public static final photo_icon_deco_eraser_selected:I = 0x7f020421

.field public static final photo_icon_deco_redo:I = 0x7f020422

.field public static final photo_icon_deco_redo_dim:I = 0x7f020423

.field public static final photo_icon_deco_redo_focus:I = 0x7f020424

.field public static final photo_icon_deco_redo_press:I = 0x7f020425

.field public static final photo_icon_deco_redo_selected:I = 0x7f020426

.field public static final photo_icon_deco_undo:I = 0x7f020427

.field public static final photo_icon_deco_undo_dim:I = 0x7f020428

.field public static final photo_icon_deco_undo_focus:I = 0x7f020429

.field public static final photo_icon_deco_undo_press:I = 0x7f02042a

.field public static final photo_icon_deco_undo_selected:I = 0x7f02042b

.field public static final photo_icon_decoration_01:I = 0x7f02042c

.field public static final photo_icon_decoration_02:I = 0x7f02042d

.field public static final photo_icon_decoration_03:I = 0x7f02042e

.field public static final photo_icon_decoration_04:I = 0x7f02042f

.field public static final photo_icon_decoration_05:I = 0x7f020430

.field public static final photo_icon_decoration_06:I = 0x7f020431

.field public static final photo_icon_mode_01:I = 0x7f020432

.field public static final photo_icon_mode_01_dim:I = 0x7f020433

.field public static final photo_icon_mode_01_focus:I = 0x7f020434

.field public static final photo_icon_mode_01_press:I = 0x7f020435

.field public static final photo_icon_mode_01_selected:I = 0x7f020436

.field public static final photo_icon_mode_02:I = 0x7f020437

.field public static final photo_icon_mode_02_dim:I = 0x7f020438

.field public static final photo_icon_mode_02_focus:I = 0x7f020439

.field public static final photo_icon_mode_02_press:I = 0x7f02043a

.field public static final photo_icon_mode_02_selected:I = 0x7f02043b

.field public static final photo_icon_mode_03:I = 0x7f02043c

.field public static final photo_icon_mode_03_dim:I = 0x7f02043d

.field public static final photo_icon_mode_03_focus:I = 0x7f02043e

.field public static final photo_icon_mode_03_press:I = 0x7f02043f

.field public static final photo_icon_mode_03_selected:I = 0x7f020440

.field public static final photo_icon_resize_01:I = 0x7f020441

.field public static final photo_icon_resize_01_dim:I = 0x7f020442

.field public static final photo_icon_resize_01_focus:I = 0x7f020443

.field public static final photo_icon_resize_01_press:I = 0x7f020444

.field public static final photo_icon_resize_01_selected:I = 0x7f020445

.field public static final photo_icon_resize_02:I = 0x7f020446

.field public static final photo_icon_resize_02_dim:I = 0x7f020447

.field public static final photo_icon_resize_02_focus:I = 0x7f020448

.field public static final photo_icon_resize_02_press:I = 0x7f020449

.field public static final photo_icon_resize_02_selected:I = 0x7f02044a

.field public static final photo_icon_resize_03:I = 0x7f02044b

.field public static final photo_icon_resize_03_dim:I = 0x7f02044c

.field public static final photo_icon_resize_03_focus:I = 0x7f02044d

.field public static final photo_icon_resize_03_press:I = 0x7f02044e

.field public static final photo_icon_resize_03_selected:I = 0x7f02044f

.field public static final photo_icon_resize_04:I = 0x7f020450

.field public static final photo_icon_resize_04_dim:I = 0x7f020451

.field public static final photo_icon_resize_04_focus:I = 0x7f020452

.field public static final photo_icon_resize_04_press:I = 0x7f020453

.field public static final photo_icon_resize_04_selected:I = 0x7f020454

.field public static final photo_icon_resize_05:I = 0x7f020455

.field public static final photo_icon_resize_05_dim:I = 0x7f020456

.field public static final photo_icon_resize_05_focus:I = 0x7f020457

.field public static final photo_icon_resize_05_press:I = 0x7f020458

.field public static final photo_icon_resize_05_selected:I = 0x7f020459

.field public static final photo_icon_rotate_01:I = 0x7f02045a

.field public static final photo_icon_rotate_01_dim:I = 0x7f02045b

.field public static final photo_icon_rotate_01_focus:I = 0x7f02045c

.field public static final photo_icon_rotate_01_press:I = 0x7f02045d

.field public static final photo_icon_rotate_01_selected:I = 0x7f02045e

.field public static final photo_icon_rotate_02:I = 0x7f02045f

.field public static final photo_icon_rotate_02_dim:I = 0x7f020460

.field public static final photo_icon_rotate_02_focus:I = 0x7f020461

.field public static final photo_icon_rotate_02_press:I = 0x7f020462

.field public static final photo_icon_rotate_02_selected:I = 0x7f020463

.field public static final photo_icon_rotate_03:I = 0x7f020464

.field public static final photo_icon_rotate_03_dim:I = 0x7f020465

.field public static final photo_icon_rotate_03_focus:I = 0x7f020466

.field public static final photo_icon_rotate_03_press:I = 0x7f020467

.field public static final photo_icon_rotate_03_selected:I = 0x7f020468

.field public static final photo_icon_rotate_04:I = 0x7f020469

.field public static final photo_icon_rotate_04_dim:I = 0x7f02046a

.field public static final photo_icon_rotate_04_focus:I = 0x7f02046b

.field public static final photo_icon_rotate_04_press:I = 0x7f02046c

.field public static final photo_icon_rotate_04_selected:I = 0x7f02046d

.field public static final photo_icon_rotate_mirro_dim:I = 0x7f02046e

.field public static final photo_icon_rotate_mirro_focus:I = 0x7f02046f

.field public static final photo_icon_rotate_mirro_press:I = 0x7f020470

.field public static final photo_icon_rotate_mirror:I = 0x7f020471

.field public static final photo_icon_rotate_mirror_selected:I = 0x7f020472

.field public static final photo_icon_rotate_mirror_undo:I = 0x7f020473

.field public static final photo_icon_rotate_mirror_undo_dim:I = 0x7f020474

.field public static final photo_icon_rotate_mirror_undo_focus:I = 0x7f020475

.field public static final photo_icon_rotate_mirror_undo_press:I = 0x7f020476

.field public static final photo_icon_rotate_mirror_undo_selected:I = 0x7f020477

.field public static final photo_page_navicator:I = 0x7f020478

.field public static final photo_page_navicator_selected:I = 0x7f020479

.field public static final photo_page_navicator_unselected:I = 0x7f02047a

.field public static final photo_portrait_icon_airbrushface:I = 0x7f02047b

.field public static final photo_portrait_icon_facebrightness:I = 0x7f02047c

.field public static final photo_portrait_icon_outoffocus:I = 0x7f02047d

.field public static final photo_portrait_icon_redeyefix:I = 0x7f02047e

.field public static final photo_tab_deco_icon_calender:I = 0x7f02047f

.field public static final photo_tab_deco_icon_calender_focus:I = 0x7f020480

.field public static final photo_tab_deco_icon_camera:I = 0x7f020481

.field public static final photo_tab_deco_icon_camera_focus:I = 0x7f020482

.field public static final photo_tab_deco_icon_graphic:I = 0x7f020483

.field public static final photo_tab_deco_icon_graphic_focus:I = 0x7f020484

.field public static final photo_tab_deco_icon_heart:I = 0x7f020485

.field public static final photo_tab_deco_icon_heart_focus:I = 0x7f020486

.field public static final photo_tab_deco_icon_label:I = 0x7f020487

.field public static final photo_tab_deco_icon_label_focus:I = 0x7f020488

.field public static final photo_tab_deco_icon_recent:I = 0x7f020489

.field public static final photo_tab_deco_icon_recent_focus:I = 0x7f02048a

.field public static final photo_tab_deco_icon_stamp:I = 0x7f02048b

.field public static final photo_tab_deco_icon_stamp_focus:I = 0x7f02048c

.field public static final photo_tab_deco_icon_sticker_deco:I = 0x7f02048d

.field public static final photo_tab_deco_icon_sticker_life:I = 0x7f02048e

.field public static final photo_tab_deco_icon_sticker_people:I = 0x7f02048f

.field public static final photo_tab_deco_icon_text:I = 0x7f020490

.field public static final photo_tab_deco_icon_text_focus:I = 0x7f020491

.field public static final photo_tab_deco_icon_vector:I = 0x7f020492

.field public static final photo_tab_deco_icon_vector_focus:I = 0x7f020493

.field public static final photo_tab_deco_icon_watermark:I = 0x7f020494

.field public static final photo_tab_deco_icon_watermark_focus:I = 0x7f020495

.field public static final photoeditor_header_icon_original_photo_view:I = 0x7f020496

.field public static final photoeditor_main_frame:I = 0x7f020497

.field public static final photoeditor_option_popup_focus:I = 0x7f020498

.field public static final photoeditor_option_popup_focus_1:I = 0x7f020499

.field public static final photoeditor_option_popup_focus_2:I = 0x7f02049a

.field public static final photoeditor_popup_bg:I = 0x7f02049b

.field public static final pile_a_2:I = 0x7f02049c

.field public static final pile_a_2_press:I = 0x7f02049d

.field public static final pile_a_3:I = 0x7f02049e

.field public static final pile_a_3_press:I = 0x7f02049f

.field public static final pile_a_4:I = 0x7f0204a0

.field public static final pile_a_4_press:I = 0x7f0204a1

.field public static final pile_a_5:I = 0x7f0204a2

.field public static final pile_a_5_press:I = 0x7f0204a3

.field public static final pile_a_6:I = 0x7f0204a4

.field public static final pile_a_6_press:I = 0x7f0204a5

.field public static final pile_b_2:I = 0x7f0204a6

.field public static final pile_b_2_press:I = 0x7f0204a7

.field public static final pile_b_3:I = 0x7f0204a8

.field public static final pile_b_3_press:I = 0x7f0204a9

.field public static final pile_b_4:I = 0x7f0204aa

.field public static final pile_b_4_press:I = 0x7f0204ab

.field public static final pile_b_5:I = 0x7f0204ac

.field public static final pile_b_5_press:I = 0x7f0204ad

.field public static final pile_b_6:I = 0x7f0204ae

.field public static final pile_b_6_press:I = 0x7f0204af

.field public static final pile_c_2:I = 0x7f0204b0

.field public static final pile_c_2_press:I = 0x7f0204b1

.field public static final pile_c_3:I = 0x7f0204b2

.field public static final pile_c_3_press:I = 0x7f0204b3

.field public static final pile_c_4:I = 0x7f0204b4

.field public static final pile_c_4_press:I = 0x7f0204b5

.field public static final pile_c_5:I = 0x7f0204b6

.field public static final pile_c_5_press:I = 0x7f0204b7

.field public static final pile_c_6:I = 0x7f0204b8

.field public static final pile_c_6_press:I = 0x7f0204b9

.field public static final pile_d_2:I = 0x7f0204ba

.field public static final pile_d_2_press:I = 0x7f0204bb

.field public static final pile_d_3:I = 0x7f0204bc

.field public static final pile_d_3_press:I = 0x7f0204bd

.field public static final pile_d_4:I = 0x7f0204be

.field public static final pile_d_4_press:I = 0x7f0204bf

.field public static final pile_d_5:I = 0x7f0204c0

.field public static final pile_d_5_press:I = 0x7f0204c1

.field public static final pile_d_6:I = 0x7f0204c2

.field public static final pile_d_6_press:I = 0x7f0204c3

.field public static final popup_checkbox:I = 0x7f0204c4

.field public static final popup_help_btn_cancel:I = 0x7f0204c5

.field public static final popup_icon_brush:I = 0x7f0204c6

.field public static final popup_icon_lasso:I = 0x7f0204c7

.field public static final popup_icon_magnetic:I = 0x7f0204c8

.field public static final popup_icon_round:I = 0x7f0204c9

.field public static final popup_icon_square:I = 0x7f0204ca

.field public static final portrait:I = 0x7f0204cb

.field public static final redeyefix_sequence_30fps_01:I = 0x7f0204cc

.field public static final redeyefix_sequence_30fps_02:I = 0x7f0204cd

.field public static final redeyefix_sequence_30fps_03:I = 0x7f0204ce

.field public static final redeyefix_sequence_30fps_04:I = 0x7f0204cf

.field public static final redeyefix_sequence_30fps_05:I = 0x7f0204d0

.field public static final redeyefix_sequence_30fps_06:I = 0x7f0204d1

.field public static final redeyefix_sequence_30fps_07:I = 0x7f0204d2

.field public static final redeyefix_sequence_30fps_08:I = 0x7f0204d3

.field public static final redeyefix_sequence_30fps_09:I = 0x7f0204d4

.field public static final redeyefix_sequence_30fps_10:I = 0x7f0204d5

.field public static final redeyefix_sequence_30fps_11:I = 0x7f0204d6

.field public static final redeyefix_sequence_30fps_12:I = 0x7f0204d7

.field public static final redeyefix_sequence_30fps_13:I = 0x7f0204d8

.field public static final redeyefix_sequence_30fps_14:I = 0x7f0204d9

.field public static final redeyefix_sequence_30fps_15:I = 0x7f0204da

.field public static final redeyefix_sequence_30fps_16:I = 0x7f0204db

.field public static final redeyefix_sequence_30fps_17:I = 0x7f0204dc

.field public static final redeyefix_sequence_30fps_18:I = 0x7f0204dd

.field public static final redeyefix_sequence_30fps_19:I = 0x7f0204de

.field public static final redeyefix_sequence_30fps_20:I = 0x7f0204df

.field public static final redeyefix_sequence_30fps_21:I = 0x7f0204e0

.field public static final redeyefix_sequence_30fps_22:I = 0x7f0204e1

.field public static final redeyefix_sequence_30fps_23:I = 0x7f0204e2

.field public static final redeyefix_sequence_30fps_24:I = 0x7f0204e3

.field public static final redeyefix_sequence_30fps_25:I = 0x7f0204e4

.field public static final redeyefix_sequence_30fps_26:I = 0x7f0204e5

.field public static final redeyefix_sequence_30fps_27:I = 0x7f0204e6

.field public static final redeyefix_sequence_30fps_28:I = 0x7f0204e7

.field public static final redeyefix_sequence_30fps_29:I = 0x7f0204e8

.field public static final redeyefix_sequence_30fps_30:I = 0x7f0204e9

.field public static final resize:I = 0x7f0204ea

.field public static final resize_fifty:I = 0x7f0204eb

.field public static final resize_free:I = 0x7f0204ec

.field public static final resize_seventyfive:I = 0x7f0204ed

.field public static final resize_ten:I = 0x7f0204ee

.field public static final resize_twentyfive:I = 0x7f0204ef

.field public static final rotate:I = 0x7f0204f0

.field public static final rotate_horizontal:I = 0x7f0204f1

.field public static final rotate_left:I = 0x7f0204f2

.field public static final rotate_mirror:I = 0x7f0204f3

.field public static final rotate_right:I = 0x7f0204f4

.field public static final rotate_vertical:I = 0x7f0204f5

.field public static final s_help_cancel_btn_focus:I = 0x7f0204f6

.field public static final s_help_cancel_btn_nor:I = 0x7f0204f7

.field public static final s_help_cancel_btn_press:I = 0x7f0204f8

.field public static final s_help_img_01:I = 0x7f0204f9

.field public static final s_help_img_01_l:I = 0x7f0204fa

.field public static final s_help_img_02:I = 0x7f0204fb

.field public static final s_help_img_03:I = 0x7f0204fc

.field public static final s_help_img_04:I = 0x7f0204fd

.field public static final s_help_img_04_l:I = 0x7f0204fe

.field public static final s_help_img_05:I = 0x7f0204ff

.field public static final s_help_img_05_l:I = 0x7f020500

.field public static final s_help_img_06:I = 0x7f020501

.field public static final s_help_img_07:I = 0x7f020502

.field public static final s_help_img_07_l:I = 0x7f020503

.field public static final s_help_img_08:I = 0x7f020504

.field public static final s_help_img_08_l:I = 0x7f020505

.field public static final s_help_img_09:I = 0x7f020506

.field public static final s_help_img_10:I = 0x7f020507

.field public static final s_help_img_10_l:I = 0x7f020508

.field public static final s_help_img_11:I = 0x7f020509

.field public static final s_help_img_11_l:I = 0x7f02050a

.field public static final s_help_img_12:I = 0x7f02050b

.field public static final s_help_img_12_l:I = 0x7f02050c

.field public static final s_help_img_13:I = 0x7f02050d

.field public static final s_help_img_13_l:I = 0x7f02050e

.field public static final s_help_img_14_l:I = 0x7f02050f

.field public static final s_help_img_bestface:I = 0x7f020510

.field public static final s_studio_main_icon:I = 0x7f020511

.field public static final scale_popup_item:I = 0x7f020512

.field public static final seasonal_01:I = 0x7f020513

.field public static final seasonal_02:I = 0x7f020514

.field public static final seasonal_03:I = 0x7f020515

.field public static final seasonal_04:I = 0x7f020516

.field public static final seasonal_05:I = 0x7f020517

.field public static final seasonal_06:I = 0x7f020518

.field public static final seasonal_07:I = 0x7f020519

.field public static final seasonal_08:I = 0x7f02051a

.field public static final seasonal_09:I = 0x7f02051b

.field public static final seasonal_10:I = 0x7f02051c

.field public static final seekbar:I = 0x7f02051d

.field public static final seekbar_b:I = 0x7f02051e

.field public static final seekbar_g:I = 0x7f02051f

.field public static final seekbar_hue:I = 0x7f020520

.field public static final seekbar_minus:I = 0x7f020521

.field public static final seekbar_plus:I = 0x7f020522

.field public static final seekbar_r:I = 0x7f020523

.field public static final seekbar_thumb:I = 0x7f020524

.field public static final selection_add:I = 0x7f020525

.field public static final selection_inverse:I = 0x7f020526

.field public static final selection_mode:I = 0x7f020527

.field public static final selection_new:I = 0x7f020528

.field public static final selection_size:I = 0x7f020529

.field public static final selection_sub:I = 0x7f02052a

.field public static final semitransparent_white:I = 0x7f02059a

.field public static final sketch_texture:I = 0x7f02052b

.field public static final stamp:I = 0x7f02052c

.field public static final sticker:I = 0x7f02052d

.field public static final style1_emotion_angry_01:I = 0x7f02052e

.field public static final style1_emotion_angry_02:I = 0x7f02052f

.field public static final style1_emotion_cheek:I = 0x7f020530

.field public static final style1_emotion_heart:I = 0x7f020531

.field public static final style1_emotion_sigh:I = 0x7f020532

.field public static final style1_emotion_tear:I = 0x7f020533

.field public static final style1_faceobject_crown:I = 0x7f020534

.field public static final style1_faceobject_glasses:I = 0x7f020535

.field public static final style1_faceobject_hair_band:I = 0x7f020536

.field public static final style1_faceobject_hat:I = 0x7f020537

.field public static final style1_faceobject_moustache:I = 0x7f020538

.field public static final style1_faceobject_mouth:I = 0x7f020539

.field public static final style1_faceobject_wig:I = 0x7f02053a

.field public static final style1_object_pipe:I = 0x7f02053b

.field public static final style1_object_ribbon:I = 0x7f02053c

.field public static final style3_object_bonny:I = 0x7f02053d

.field public static final style3_object_chefhat:I = 0x7f02053e

.field public static final style3_object_mustache:I = 0x7f02053f

.field public static final style3_object_partyhat:I = 0x7f020540

.field public static final style3_object_question:I = 0x7f020541

.field public static final style3_object_star:I = 0x7f020542

.field public static final style3_object_sunglass_blue:I = 0x7f020543

.field public static final style3_object_sunglass_pink:I = 0x7f020544

.field public static final style4_emotion_idea:I = 0x7f020545

.field public static final style4_emotion_peeve:I = 0x7f020546

.field public static final style4_emotion_shyness:I = 0x7f020547

.field public static final style4_emotion_sigh:I = 0x7f020548

.field public static final style4_emotion_tear:I = 0x7f020549

.field public static final style4_emotion_twinkle:I = 0x7f02054a

.field public static final submenu_bg:I = 0x7f02054b

.field public static final submenu_bg_2:I = 0x7f02054c

.field public static final texture_369104391:I = 0x7f02054d

.field public static final texture_369104392:I = 0x7f02054e

.field public static final texture_369104393:I = 0x7f02054f

.field public static final texture_369104399:I = 0x7f020550

.field public static final texture_369104402:I = 0x7f020551

.field public static final texture_369104404:I = 0x7f020552

.field public static final texture_369104406:I = 0x7f020553

.field public static final texture_369104407:I = 0x7f020554

.field public static final texture_vangoghbrush:I = 0x7f020555

.field public static final thumbnail_focuced:I = 0x7f020556

.field public static final thumbnail_normal:I = 0x7f020557

.field public static final thumbnail_rectangle:I = 0x7f020558

.field public static final tone_icon_auto:I = 0x7f020559

.field public static final tone_icon_blueeffect:I = 0x7f02055a

.field public static final tone_icon_brigtness:I = 0x7f02055b

.field public static final tone_icon_contrast:I = 0x7f02055c

.field public static final tone_icon_greeneffect:I = 0x7f02055d

.field public static final tone_icon_hue:I = 0x7f02055e

.field public static final tone_icon_redeffect:I = 0x7f02055f

.field public static final tone_icon_saturation:I = 0x7f020560

.field public static final tone_icon_temperature:I = 0x7f020561

.field public static final tone_selected_check:I = 0x7f020562

.field public static final travel_01:I = 0x7f020563

.field public static final travel_02:I = 0x7f020564

.field public static final travel_03:I = 0x7f020565

.field public static final travel_04:I = 0x7f020566

.field public static final travel_05:I = 0x7f020567

.field public static final travel_06:I = 0x7f020568

.field public static final travel_07:I = 0x7f020569

.field public static final travel_08:I = 0x7f02056a

.field public static final tray_handler:I = 0x7f02056b

.field public static final tray_handler_background:I = 0x7f02056c

.field public static final tray_handler_icon_closed:I = 0x7f02056d

.field public static final tray_handler_icon_closed_h:I = 0x7f02056e

.field public static final tray_handler_icon_open:I = 0x7f02056f

.field public static final tray_handler_icon_open_h:I = 0x7f020570

.field public static final tray_minus_btn:I = 0x7f020571

.field public static final tray_open_icon_add:I = 0x7f020572

.field public static final tray_open_icon_add_dim:I = 0x7f020573

.field public static final tray_open_icon_add_press:I = 0x7f020574

.field public static final tw_action_bar_tab_bg_holo_light:I = 0x7f020575

.field public static final tw_action_bar_tab_selected_bg_h_holo_dark:I = 0x7f020576

.field public static final tw_action_bar_tab_selected_bg_holo_light:I = 0x7f020577

.field public static final tw_action_item_background_focused_holo_light:I = 0x7f020578

.field public static final tw_action_item_background_pressed_holo_light:I = 0x7f020579

.field public static final tw_btn_check_off_disabled_focused_holo_dark:I = 0x7f02057a

.field public static final tw_btn_check_off_disabled_holo_dark:I = 0x7f02057b

.field public static final tw_btn_check_off_focused_holo_dark:I = 0x7f02057c

.field public static final tw_btn_check_off_holo_dark:I = 0x7f02057d

.field public static final tw_btn_check_off_pressed_holo_dark:I = 0x7f02057e

.field public static final tw_btn_check_on_disabled_focused_holo_dark:I = 0x7f02057f

.field public static final tw_btn_check_on_disabled_holo_dark:I = 0x7f020580

.field public static final tw_btn_check_on_focused_holo_dark:I = 0x7f020581

.field public static final tw_btn_check_on_holo_dark:I = 0x7f020582

.field public static final tw_btn_check_on_pressed_holo_dark:I = 0x7f020583

.field public static final tw_dialog_bottom_shadow_holo_dark:I = 0x7f020584

.field public static final tw_gallery_btn_check_off:I = 0x7f020585

.field public static final tw_gallery_btn_check_on:I = 0x7f020586

.field public static final tw_ic_ab_back_holo_dark:I = 0x7f020587

.field public static final tw_list_divider_holo_dark:I = 0x7f020588

.field public static final tw_list_divider_holo_light:I = 0x7f020589

.field public static final tw_list_focused:I = 0x7f02058a

.field public static final tw_scrollbar_holo_light:I = 0x7f02058b

.field public static final tw_spinner_default_holo_dark:I = 0x7f02058c

.field public static final tw_tab_indicator_mtrl_alpha:I = 0x7f02058d

.field public static final tw_tab_selected_focused_holo_light:I = 0x7f02058e

.field public static final tw_tab_selected_focused_pressed_holo_light:I = 0x7f02058f

.field public static final tw_tab_selected_pressed_holo_light:I = 0x7f020590

.field public static final tw_topmenu_focused:I = 0x7f020591

.field public static final tw_widget_progressbar_holo_dark:I = 0x7f020592

.field public static final watermark:I = 0x7f020593

.field public static final weather_01:I = 0x7f020594

.field public static final weather_02:I = 0x7f020595

.field public static final weather_03:I = 0x7f020596

.field public static final weather_04:I = 0x7f020597

.field public static final weather_05:I = 0x7f020598

.field public static final weather_06:I = 0x7f020599


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
