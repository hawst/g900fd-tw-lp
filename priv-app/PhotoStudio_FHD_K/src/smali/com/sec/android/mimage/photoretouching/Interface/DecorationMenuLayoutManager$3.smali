.class Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$3;
.super Ljava/lang/Object;
.source "DecorationMenuLayoutManager.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 131
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # F
    .param p3, "arg2"    # I

    .prologue
    .line 125
    return-void
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 98
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 120
    :goto_0
    :sswitch_0
    return-void

    .line 100
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)Landroid/widget/TabHost;

    move-result-object v0

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto :goto_0

    .line 103
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)Landroid/widget/TabHost;

    move-result-object v0

    add-int/lit8 v1, p1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto :goto_0

    .line 106
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)Landroid/widget/TabHost;

    move-result-object v0

    add-int/lit8 v1, p1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto :goto_0

    .line 112
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$3;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->mTabHostLayout:Landroid/widget/TabHost;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)Landroid/widget/TabHost;

    move-result-object v0

    add-int/lit8 v1, p1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto :goto_0

    .line 98
    :sswitch_data_0
    .sparse-switch
        0x31100000 -> :sswitch_1
        0x31200000 -> :sswitch_4
        0x31300000 -> :sswitch_3
        0x31400000 -> :sswitch_0
        0x31500000 -> :sswitch_2
    .end sparse-switch
.end method
