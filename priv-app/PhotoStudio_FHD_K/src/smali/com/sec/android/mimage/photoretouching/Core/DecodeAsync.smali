.class public Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;
.super Landroid/os/AsyncTask;
.source "DecodeAsync.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field public static final FROM_ANOTHER_APP:I = 0x2

.field public static final FROM_COPYTOANOTHER:I = 0x1

.field public static final FROM_EDIT_OPEN:I = 0x4

.field public static final FROM_LAUNCHER:I = 0x0

.field public static final FROM_MOTIONPHOTO:I = 0x6

.field public static final FROM_MULTIGRID:I = 0x5

.field public static final FROM_MULTIGRID_ADD:I = 0x7

.field public static final FROM_TRAY:I = 0x3


# instance fields
.field private mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

.field private mContext:Landroid/content/Context;

.field private mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

.field private mDInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDecodeFailRunnable:Ljava/lang/Runnable;

.field private mDecodeFrom:I

.field private mDecodeState:Z

.field private mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

.field private mNumCalled:I

.field private mProgressCancelFlag:Z

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mProgressListener:Landroid/content/DialogInterface$OnCancelListener;

.field private mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "srcItemList"    # Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    .param p4, "multiGridDecodeCallback"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;
    .param p5, "decodeFrom"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;",
            ">;",
            "Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;",
            "I)V"
        }
    .end annotation

    .prologue
    .local p3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;>;"
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 423
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFailRunnable:Ljava/lang/Runnable;

    .line 451
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    .line 453
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 454
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 455
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressCancelFlag:Z

    .line 456
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 457
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    .line 458
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .line 460
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    .line 461
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    .line 462
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mNumCalled:I

    .line 472
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 474
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeState:Z

    .line 108
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    .line 109
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 110
    iput p5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFrom:I

    .line 111
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    .line 112
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    .line 114
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 116
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$5;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$5;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p4, "decodeFrom"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;",
            ">;",
            "Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;",
            "I)V"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;>;"
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 423
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFailRunnable:Ljava/lang/Runnable;

    .line 451
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    .line 453
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 454
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 455
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressCancelFlag:Z

    .line 456
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 457
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    .line 458
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .line 460
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    .line 461
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    .line 462
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mNumCalled:I

    .line 472
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 474
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeState:Z

    .line 35
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    .line 36
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 37
    iput p4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFrom:I

    .line 39
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$2;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "copyToDecodeCallback"    # Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;
    .param p4, "decodeFrom"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;",
            ">;",
            "Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;",
            "I)V"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;>;"
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 423
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFailRunnable:Ljava/lang/Runnable;

    .line 451
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    .line 453
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 454
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 455
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressCancelFlag:Z

    .line 456
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 457
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    .line 458
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .line 460
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    .line 461
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    .line 462
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mNumCalled:I

    .line 472
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 474
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeState:Z

    .line 84
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    .line 85
    iput p4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFrom:I

    .line 86
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .line 87
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    .line 89
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$4;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$4;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "srcItemList"    # Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    .param p4, "decodeFrom"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;",
            ">;",
            "Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;",
            "I)V"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;>;"
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 423
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFailRunnable:Ljava/lang/Runnable;

    .line 451
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    .line 453
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 454
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 455
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressCancelFlag:Z

    .line 456
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 457
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    .line 458
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .line 460
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    .line 461
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    .line 462
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mNumCalled:I

    .line 472
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 474
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeState:Z

    .line 59
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    .line 60
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 61
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    .line 62
    iput p4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFrom:I

    .line 64
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$3;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$3;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 80
    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFailRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Z
    .locals 1

    .prologue
    .line 474
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeState:Z

    return v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;Z)V
    .locals 0

    .prologue
    .line 474
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeState:Z

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/content/DialogInterface$OnCancelListener;
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressListener:Landroid/content/DialogInterface$OnCancelListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)I
    .locals 1

    .prologue
    .line 452
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFrom:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Z
    .locals 1

    .prologue
    .line 455
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressCancelFlag:Z

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 454
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;Z)V
    .locals 0

    .prologue
    .line 455
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressCancelFlag:Z

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public checkTaskFinish(I)V
    .locals 9
    .param p1, "idx"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 315
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mNumCalled:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mNumCalled:I

    .line 316
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eqz v5, :cond_2

    .line 318
    const/4 v4, 0x0

    .line 319
    .local v4, "isFinish":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_3

    .line 330
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mNumCalled:I

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ne v5, v6, :cond_2

    .line 332
    const/4 v3, 0x0

    .line 333
    .local v3, "isCancel":I
    const/4 v2, 0x0

    :goto_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_5

    .line 343
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v3, v5, :cond_7

    .line 345
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFailRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 379
    :cond_0
    :goto_2
    const/4 v2, 0x0

    :goto_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_9

    .line 386
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 387
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v5, :cond_1

    .line 388
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->setDecodingAllFinish()V

    .line 389
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v5, :cond_2

    .line 391
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/ProgressDialog;->dismiss()V

    .line 392
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 396
    .end local v2    # "i":I
    .end local v3    # "isCancel":I
    .end local v4    # "isFinish":I
    :cond_2
    return-void

    .line 321
    .restart local v2    # "i":I
    .restart local v4    # "isFinish":I
    :cond_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 322
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    if-eqz v0, :cond_4

    .line 324
    iget-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isFinish:Z

    if-eqz v5, :cond_4

    .line 326
    add-int/lit8 v4, v4, 0x1

    .line 319
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 335
    .end local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .restart local v3    # "isCancel":I
    :cond_5
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 336
    .restart local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->task:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    .line 337
    .local v1, "dTask":Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;
    if-eqz v1, :cond_6

    .line 339
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->isCancel()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 340
    add-int/lit8 v3, v3, 0x1

    .line 333
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 349
    .end local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .end local v1    # "dTask":Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;
    :cond_7
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFrom:I

    packed-switch v5, :pswitch_data_0

    :pswitch_0
    goto :goto_2

    .line 356
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 357
    .restart local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    if-eqz v5, :cond_0

    .line 358
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    invoke-interface {v5, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;->initCopyToImageData(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;)V

    goto :goto_2

    .line 352
    .end local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 353
    .restart local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v6, 0x4

    invoke-virtual {v5, v0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->newImage(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V

    goto :goto_2

    .line 363
    .end local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    invoke-interface {v5, v7}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;->changeViewStatus(I)V

    goto :goto_2

    .line 369
    :pswitch_4
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 370
    .restart local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    if-eqz v5, :cond_8

    .line 373
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    invoke-interface {v5, v0, v6}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;->setImageMultiGridView(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;Landroid/graphics/Bitmap;)V

    .line 375
    :cond_8
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mMultiGridDecodeCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$CollageDecodeCallback;

    goto/16 :goto_2

    .line 381
    .end local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    :cond_9
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 383
    .restart local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->thumbnail:Landroid/graphics/Bitmap;

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->thumbnail:Landroid/graphics/Bitmap;

    .line 384
    iput-object v8, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->task:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    .line 379
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    .line 349
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public destroy()V
    .locals 3

    .prologue
    .line 138
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 144
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mCopyToDecodeCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;

    .line 145
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 146
    return-void

    .line 140
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 141
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->task:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->task:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->isFinish()Z

    move-result v2

    if-nez v2, :cond_1

    .line 142
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->task:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    .line 138
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 277
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 311
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 312
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 280
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 281
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 284
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 286
    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 287
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    const v2, 0x7f06008f

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 294
    return-void
.end method

.method public setChangeViewCallback(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    .line 135
    return-void
.end method

.method public start()V
    .locals 9

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 152
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFrom:I

    packed-switch v0, :pswitch_data_0

    .line 181
    :cond_0
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v7

    .line 182
    .local v7, "executorService":Ljava/util/concurrent/ExecutorService;
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$6;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;)V

    .line 262
    .local v3, "taskFinish":Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v5, v0, :cond_3

    .line 267
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p0, v7, v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 271
    .end local v3    # "taskFinish":Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;
    .end local v5    # "i":I
    .end local v7    # "executorService":Ljava/util/concurrent/ExecutorService;
    :cond_1
    return-void

    .line 156
    :pswitch_1
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v5, v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 160
    .local v2, "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->size()I

    move-result v0

    iput v0, v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->index:I

    .line 161
    new-instance v8, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    invoke-direct {v8, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;-><init>(Landroid/content/Context;)V

    .line 162
    .local v8, "item":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    invoke-virtual {v8, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setDecodeInfo(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;)V

    .line 163
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    iget-boolean v0, v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    :goto_2
    invoke-virtual {v8, v1, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->determinePersonalPage(Landroid/content/Context;Ljava/lang/Object;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    invoke-virtual {v0, v8}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->add(Ljava/lang/Object;)Z

    .line 165
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 156
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 163
    :cond_2
    iget-object v0, v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    goto :goto_2

    .line 171
    .end local v2    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .end local v5    # "i":I
    .end local v8    # "item":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    :pswitch_2
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v5, v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 174
    .restart local v2    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFrom:I

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->newImage(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V

    .line 171
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 264
    .end local v2    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .restart local v3    # "taskFinish":Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;
    .restart local v7    # "executorService":Ljava/util/concurrent/ExecutorService;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 265
    .restart local v2    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mContext:Landroid/content/Context;

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mDecodeFrom:I

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;IILcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 262
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
