.class Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$12;
.super Ljava/lang/Object;
.source "AdjustmentView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->init2DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    .line 534
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 0
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 562
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v1, 0x8

    .line 541
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 544
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 545
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 556
    :cond_0
    :goto_0
    return-void

    .line 547
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    goto :goto_0

    .line 550
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 552
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x2e000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 568
    return-void
.end method
