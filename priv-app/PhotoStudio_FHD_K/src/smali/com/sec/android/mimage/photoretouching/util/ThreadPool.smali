.class public Lcom/sec/android/mimage/photoretouching/util/ThreadPool;
.super Ljava/lang/Object;
.source "ThreadPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/util/ThreadPool$CancelListener;,
        Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;,
        Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;,
        Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContextStub;,
        Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;,
        Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;
    }
.end annotation


# static fields
.field private static final CORE_POOL_SIZE:I = 0x2

.field public static final JOB_CONTEXT_STUB:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;

.field private static final KEEP_ALIVE_TIME:I = 0xa

.field private static final MAX_POOL_SIZE:I = 0x4

.field public static final MODE_CPU:I = 0x1

.field public static final MODE_NETWORK:I = 0x2

.field public static final MODE_NONE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ThreadPool"


# instance fields
.field mCpuCounter:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field mNetworkCounter:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContextStub;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContextStub;-><init>(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContextStub;)V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;->JOB_CONTEXT_STUB:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$JobContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 10

    .prologue
    const/4 v2, 0x2

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    invoke-direct {v0, v2}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;->mCpuCounter:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    .line 38
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    invoke-direct {v0, v2}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;->mNetworkCounter:Lcom/sec/android/mimage/photoretouching/util/ThreadPool$ResourceCounter;

    .line 81
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    .line 82
    const/4 v3, 0x4

    const-wide/16 v4, 0xa

    .line 83
    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    .line 84
    new-instance v8, Lcom/sec/android/mimage/photoretouching/util/PriorityThreadFactory;

    const-string v0, "thread-pool"

    .line 85
    const/16 v9, 0xa

    .line 84
    invoke-direct {v8, v0, v9}, Lcom/sec/android/mimage/photoretouching/util/PriorityThreadFactory;-><init>(Ljava/lang/String;I)V

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 81
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;->mExecutor:Ljava/util/concurrent/Executor;

    .line 86
    return-void
.end method


# virtual methods
.method public submit(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;)Lcom/sec/android/mimage/photoretouching/util/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job",
            "<TT;>;)",
            "Lcom/sec/android/mimage/photoretouching/util/Future",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 97
    .local p1, "job":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;->submit(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;Lcom/sec/android/mimage/photoretouching/util/FutureListener;)Lcom/sec/android/mimage/photoretouching/util/Future;

    move-result-object v0

    return-object v0
.end method

.method public submit(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;Lcom/sec/android/mimage/photoretouching/util/FutureListener;)Lcom/sec/android/mimage/photoretouching/util/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job",
            "<TT;>;",
            "Lcom/sec/android/mimage/photoretouching/util/FutureListener",
            "<TT;>;)",
            "Lcom/sec/android/mimage/photoretouching/util/Future",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "job":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job<TT;>;"
    .local p2, "listener":Lcom/sec/android/mimage/photoretouching/util/FutureListener;, "Lcom/sec/android/mimage/photoretouching/util/FutureListener<TT;>;"
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;-><init>(Lcom/sec/android/mimage/photoretouching/util/ThreadPool;Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;Lcom/sec/android/mimage/photoretouching/util/FutureListener;)V

    .line 92
    .local v0, "w":Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker;, "Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Worker<TT;>;"
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 93
    return-object v0
.end method
