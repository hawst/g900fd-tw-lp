.class Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;
.super Ljava/lang/Object;
.source "EffectSplashMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SplashInfo"
.end annotation


# instance fields
.field initSplash:Z

.field mRadius:F

.field mTempOriginal:[I

.field mTempPreview:[I

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 357
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 359
    const/high16 v0, 0x42e00000    # 112.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mRadius:F

    .line 361
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempOriginal:[I

    .line 362
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;)V
    .locals 0

    .prologue
    .line 357
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode;)V

    return-void
.end method


# virtual methods
.method destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 365
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->finishSplash()I

    .line 366
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->deleteStoredObject()I

    .line 367
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->initSplash:Z

    .line 368
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempOriginal:[I

    .line 369
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSplashMode$SplashInfo;->mTempPreview:[I

    .line 370
    return-void
.end method
