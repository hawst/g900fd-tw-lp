.class public Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "LabelView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ResolverSetAsAdapter;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$SaveAsyncTask;
    }
.end annotation


# static fields
.field public static final CREATE_LABEL_DIALOG:I = 0x3


# instance fields
.field private final REDOALL_DIALOG:I

.field private final UNDOALL_DIALOG:I

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentSaveSize:I

.field private mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIsResetTabLayout:Z

.field mLabelCallback:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

.field private mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mOptionItemId:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mResId:I

.field private mStyleNum:I

.field private mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;

.field onLabelCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnLabelCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p6, "decoManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 92
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 118
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelCallback:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    .line 147
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->onLabelCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnLabelCallback;

    .line 1811
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1813
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1814
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    .line 1815
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1816
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1817
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1818
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1819
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1820
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1821
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 1822
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;

    .line 1824
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mIsResetTabLayout:Z

    .line 1826
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    .line 1830
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mResId:I

    .line 1831
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mStyleNum:I

    .line 1833
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1834
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mPaint:Landroid/graphics/Paint;

    .line 1838
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->UNDOALL_DIALOG:I

    .line 1839
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->REDOALL_DIALOG:I

    .line 1842
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mOptionItemId:I

    .line 1844
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mCurrentSaveSize:I

    .line 1846
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 1847
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 1848
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 94
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    .line 95
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 96
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 97
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 98
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 99
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 101
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->setInterface(Ljava/lang/Object;)V

    .line 103
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mPaint:Landroid/graphics/Paint;

    .line 104
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 107
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 109
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelCallback:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    .line 111
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->initTabLayout()V

    .line 113
    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->setViewLayerType(I)V

    .line 115
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;

    .line 116
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;Z)V
    .locals 0

    .prologue
    .line 356
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->setVisibleTopBottomMenu(Z)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 1817
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 1818
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 1813
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 1816
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 1820
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)I
    .locals 1

    .prologue
    .line 1844
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mCurrentSaveSize:I

    return v0
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 1813
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 1811
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 1811
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)I
    .locals 1

    .prologue
    .line 1842
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mOptionItemId:I

    return v0
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;I)Z
    .locals 1

    .prologue
    .line 1357
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->runOptionItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .locals 1

    .prologue
    .line 1821
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;I)V
    .locals 0

    .prologue
    .line 1844
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mCurrentSaveSize:I

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)I
    .locals 1

    .prologue
    .line 1830
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mResId:I

    return v0
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)I
    .locals 1

    .prologue
    .line 1831
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mStyleNum:I

    return v0
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Z
    .locals 1

    .prologue
    .line 1824
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mIsResetTabLayout:Z

    return v0
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;IZZ)V
    .locals 0

    .prologue
    .line 500
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->showTabLayout(IZZ)V

    return-void
.end method

.method static synthetic access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;Z)V
    .locals 0

    .prologue
    .line 1824
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mIsResetTabLayout:Z

    return-void
.end method

.method static synthetic access$26(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V
    .locals 0

    .prologue
    .line 1239
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->doCancel()V

    return-void
.end method

.method static synthetic access$27(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V
    .locals 0

    .prologue
    .line 1266
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->doDone()V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V
    .locals 0

    .prologue
    .line 1296
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->set2depth()V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;I)V
    .locals 0

    .prologue
    .line 1830
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mResId:I

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;I)V
    .locals 0

    .prologue
    .line 1831
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mStyleNum:I

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;
    .locals 1

    .prologue
    .line 1826
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1814
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 1815
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;)V
    .locals 0

    .prologue
    .line 1826
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    return-void
.end method

.method private doCancel()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1241
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v3, :cond_0

    .line 1264
    :goto_0
    return-void

    .line 1244
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    if-eqz v3, :cond_1

    .line 1245
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->deleteLabelAll()V

    .line 1247
    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    .line 1249
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 1251
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1252
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1255
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1258
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1259
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 1260
    const/4 v8, 0x1

    .line 1261
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 1253
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1262
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->set2depth()V

    goto :goto_0
.end method

.method private doDone()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1268
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    if-eqz v0, :cond_0

    .line 1269
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->applyPreview()V

    .line 1275
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1276
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1278
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1281
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1282
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 1276
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1284
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->applyPreview()V

    .line 1285
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-direct {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;)V

    .line 1287
    .local v4, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    .line 1289
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1290
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 1291
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 1292
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getEnhance()Z

    move-result v5

    .line 1289
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V

    .line 1295
    return-void
.end method

.method private init2DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1077
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 1079
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v4, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 1080
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 1081
    const/4 v2, 0x1

    .line 1083
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$14;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 1080
    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1112
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 1115
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$15;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 1112
    invoke-virtual {v0, v1, v4, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1141
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 1143
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 1144
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 1146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 1147
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 1151
    :cond_0
    return-void
.end method

.method private init3DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1154
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 1156
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 1157
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 1160
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$16;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 1157
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1184
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 1187
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$17;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$17;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 1184
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1233
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 1236
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 1238
    :cond_0
    return-void
.end method

.method private initLabelTextDialog()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 765
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 766
    const/4 v2, 0x3

    .line 767
    const v3, 0x7f0601bd

    .line 768
    const/4 v4, 0x1

    .line 770
    const v6, 0x103012e

    .line 765
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 772
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 773
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x900

    move v3, v7

    move v4, v7

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIILjava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 780
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 781
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 780
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V

    .line 890
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 891
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 890
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 931
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 932
    return-void
.end method

.method private initSaveOptionDialog()V
    .locals 11

    .prologue
    const v10, 0x7f0601f0

    const v9, 0x7f0601cb

    const/16 v2, 0x9

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 633
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 686
    :goto_0
    return-void

    .line 638
    :cond_0
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$3;

    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 652
    .local v8, "saveListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 654
    const v3, 0x7f0601cd

    .line 656
    const/4 v5, 0x1

    .line 658
    const v7, 0x103012e

    .line 652
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZZLandroid/graphics/Point;I)V

    .line 660
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 662
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x810

    move v3, v9

    move v5, v9

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 666
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x820

    move v3, v10

    move v5, v10

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 667
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$4;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 674
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$5;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 685
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    goto :goto_0
.end method

.method private initSetAsDialog()V
    .locals 7

    .prologue
    .line 1728
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1730
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1731
    const v2, 0x7f090158

    .line 1732
    const v3, 0x7f0600a3

    .line 1733
    const/4 v4, 0x0

    .line 1734
    const/4 v5, 0x0

    .line 1735
    const v6, 0x103012e

    .line 1730
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1736
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1738
    :cond_0
    return-void
.end method

.method private initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isShareVia"    # Z

    .prologue
    .line 1478
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1479
    .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    const/4 v2, 0x0

    .line 1480
    .local v2, "shareChooser":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 1481
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    const v4, 0x7f06000f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 1485
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/content/pm/LabeledIntent;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/pm/LabeledIntent;

    .line 1486
    .local v0, "extraIntents":[Landroid/content/pm/LabeledIntent;
    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1487
    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1488
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1669
    return-void

    .line 1483
    .end local v0    # "extraIntents":[Landroid/content/pm/LabeledIntent;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    const v4, 0x7f0600a3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method private initShareViaDialog()V
    .locals 7

    .prologue
    .line 1716
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1718
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1719
    const v2, 0x7f090157

    .line 1720
    const v3, 0x7f06000f

    .line 1721
    const/4 v4, 0x0

    .line 1722
    const/4 v5, 0x0

    .line 1723
    const v6, 0x103012e

    .line 1718
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1724
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1726
    :cond_0
    return-void
.end method

.method private initTabLayout()V
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->initDataBase(Landroid/content/Context;)V

    .line 511
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setDrawerHandleVisibility(I)V

    .line 512
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTab()V

    .line 513
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->onLabelCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnLabelCallback;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->addLabel(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnLabelCallback;)V

    .line 514
    return-void
.end method

.method private initUndoRedoAllDialog()V
    .locals 15

    .prologue
    const v6, 0x103012e

    const/16 v1, 0x1000

    const/16 v14, 0x500

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 689
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 691
    const v3, 0x7f0600a6

    move v4, v2

    .line 689
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 696
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 697
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 698
    const v3, 0x7f06009c

    .line 700
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$6;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 697
    invoke-virtual {v0, v14, v3, v5, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 707
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060007

    .line 708
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$7;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 707
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 715
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060009

    .line 716
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$8;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 715
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 724
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 726
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 727
    const/4 v9, 0x2

    .line 728
    const v10, 0x7f0600a1

    move v8, v1

    move v11, v2

    move-object v12, v5

    move v13, v6

    .line 726
    invoke-virtual/range {v7 .. v13}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 733
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 734
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 735
    const v1, 0x7f0601ce

    .line 737
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$9;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 734
    invoke-virtual {v0, v14, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 744
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 745
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$10;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 744
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 752
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 753
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$11;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V

    .line 752
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 761
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 762
    return-void
.end method

.method private runOptionItem(I)Z
    .locals 9
    .param p1, "optionItemId"    # I

    .prologue
    const v8, 0x7f090156

    const v7, 0x7a1200

    const/4 v6, 0x7

    const/4 v5, 0x0

    const/16 v4, 0x9

    .line 1359
    const/4 v1, 0x0

    .line 1360
    .local v1, "ret":Z
    const/4 v2, 0x0

    .line 1361
    .local v2, "uri":Landroid/net/Uri;
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mOptionItemId:I

    .line 1362
    sparse-switch p1, :sswitch_data_0

    .line 1474
    :cond_0
    :goto_0
    return v1

    .line 1365
    :sswitch_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1367
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1374
    :goto_1
    const/4 v1, 0x1

    .line 1375
    goto :goto_0

    .line 1372
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->openCamera(Landroid/content/Context;)V

    goto :goto_1

    .line 1377
    :sswitch_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1394
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mCurrentSaveSize:I

    .line 1395
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1396
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1407
    :cond_2
    :goto_2
    const/4 v1, 0x1

    .line 1408
    goto :goto_0

    .line 1398
    :cond_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090157

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1399
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 1400
    if-nez v2, :cond_4

    .line 1402
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1404
    :cond_4
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    goto :goto_2

    .line 1410
    :sswitch_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1412
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1414
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1416
    .local v0, "fileName":Ljava/lang/String;
    if-nez v0, :cond_6

    .line 1417
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    .line 1421
    :goto_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v0, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTextToDialog(Ljava/lang/String;I)V

    .line 1423
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v4, 0x2e000000

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 1424
    const/4 v1, 0x1

    .line 1425
    goto/16 :goto_0

    .line 1419
    .restart local v0    # "fileName":Ljava/lang/String;
    :cond_6
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1428
    .end local v0    # "fileName":Ljava/lang/String;
    :sswitch_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1445
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mCurrentSaveSize:I

    .line 1446
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1447
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1460
    :cond_7
    :goto_4
    const/4 v1, 0x1

    .line 1461
    goto/16 :goto_0

    .line 1449
    :cond_8
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090158

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1452
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 1453
    if-nez v2, :cond_9

    .line 1455
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1457
    :cond_9
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    .line 1458
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090158

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    goto :goto_4

    .line 1464
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1465
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    .line 1466
    :cond_a
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setVisibilityDirectly(I)V

    .line 1468
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 1469
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v4, 0x17000000

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 1362
    :sswitch_data_0
    .sparse-switch
        0x7f090007 -> :sswitch_2
        0x7f090157 -> :sswitch_1
        0x7f090158 -> :sswitch_3
        0x7f09015a -> :sswitch_0
        0x7f09015b -> :sswitch_4
    .end sparse-switch
.end method

.method private set2depth()V
    .locals 0

    .prologue
    .line 1298
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->init2DepthActionBar()V

    .line 1318
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->invalidateViews()V

    .line 1319
    return-void
.end method

.method private setVisibleTopBottomMenu(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 357
    if-eqz p1, :cond_1

    .line 358
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->show()V

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 361
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$ActionbarNBottomButtonAnimation;->hide()V

    goto :goto_0
.end method

.method private showTabLayout(IZZ)V
    .locals 1
    .param p1, "assistantType"    # I
    .param p2, "initByconfig"    # Z
    .param p3, "doOpen"    # Z

    .prologue
    .line 502
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0, p1, p3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->showTabView(IZ)V

    .line 506
    :cond_0
    return-void
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 370
    const/4 v1, 0x1

    .line 372
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    .line 373
    .local v0, "isDrawerOpened":Z
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    if-eqz v2, :cond_0

    .line 374
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-virtual {v2, p2, v0}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->touch(Landroid/view/MotionEvent;Z)V

    .line 376
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->invalidateViews()V

    .line 377
    return v1
.end method

.method public backPressed()V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    if-nez v0, :cond_0

    .line 271
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->doCancel()V

    .line 272
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->openDrawer()V

    .line 281
    :goto_0
    return-void

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->saveToDB()V

    .line 279
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x31000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 474
    return-void
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 486
    const/4 v0, 0x0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 490
    const/4 v0, 0x0

    return v0
.end method

.method public getStatusHeight()I
    .locals 1

    .prologue
    .line 497
    const/4 v0, 0x0

    return v0
.end method

.method public getmDecorationMenuLayoutManager()Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    return-object v0
.end method

.method public initActionbar()V
    .locals 0

    .prologue
    .line 285
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->init2DepthActionBar()V

    .line 288
    return-void
.end method

.method public initButtons()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 253
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW initButton: mDecorationMenuLayoutManager.getRecentlyList().size()="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v2, :cond_0

    .line 256
    const/16 v0, 0xa

    invoke-direct {p0, v0, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->showTabLayout(IZZ)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_TYPE1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    .line 265
    :goto_0
    return-void

    .line 261
    :cond_0
    const/16 v0, 0x9

    invoke-direct {p0, v0, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->showTabLayout(IZZ)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_LABEL_RECENTLY:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    goto :goto_0
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 295
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->initUndoRedoAllDialog()V

    .line 297
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->initSaveOptionDialog()V

    .line 298
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->initLabelTextDialog()V

    .line 299
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->initShareViaDialog()V

    .line 300
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->initSetAsDialog()V

    .line 302
    :cond_0
    return-void
.end method

.method public initEffect()V
    .locals 0

    .prologue
    .line 244
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 322
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 462
    return-void
.end method

.method public initTrayLayout()V
    .locals 0

    .prologue
    .line 469
    return-void
.end method

.method public initView()V
    .locals 0

    .prologue
    .line 249
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 407
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 408
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 422
    :goto_0
    return-void

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_1

    .line 416
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 417
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 418
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 420
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 383
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 384
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mPaint:Landroid/graphics/Paint;

    .line 386
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 387
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 389
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeCancelPressState(Z)V

    .line 391
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 392
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 393
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 394
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->destroy()V

    .line 396
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 399
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 400
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 403
    :cond_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 310
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 311
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mPaint:Landroid/graphics/Paint;

    .line 308
    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->drawLabelBdry(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 316
    :cond_0
    return-void
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 425
    const/16 v1, 0x17

    if-eq p1, v1, :cond_0

    const/16 v1, 0x42

    if-ne p1, v1, :cond_3

    .line 427
    :cond_0
    const/4 v0, 0x0

    .line 428
    .local v0, "isFocused":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_1

    .line 429
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    move-result v0

    .line 432
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_2

    .line 433
    if-nez v0, :cond_2

    .line 434
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 456
    .end local v0    # "isFocused":Z
    :cond_2
    :goto_0
    return v2

    .line 438
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->backPressed()V

    goto :goto_0
.end method

.method public onLayout()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1050
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 1051
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1055
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 1056
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1057
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sj, LV - onLayout() - mImageData.getPreviewBitmap() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1059
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1061
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1064
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1065
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 1059
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1066
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW onLayout: mImageData.getPreviewHeight()="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1068
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    if-eqz v0, :cond_0

    .line 1070
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->configurationChanged()V

    .line 1071
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->getLabel()[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-result-object v8

    .line 1072
    .local v8, "labelList":[Lcom/sec/android/mimage/photoretouching/Core/Label;
    array-length v0, v8

    if-nez v0, :cond_0

    .line 1073
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->set2depth()V

    .line 1075
    .end local v8    # "labelList":[Lcom/sec/android/mimage/photoretouching/Core/Label;
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 482
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->runOptionItem(I)Z

    .line 483
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 478
    return-void
.end method

.method public refreshView()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1792
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->getImageEditViewWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->getImageEditViewHeight()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 1794
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1796
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1797
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1800
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1803
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1804
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 1805
    const/4 v8, 0x1

    .line 1806
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 1798
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1808
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->invalidateViews()V

    .line 1809
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 326
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 327
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 328
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 329
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 330
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->configurationChanged()V

    .line 333
    const-string v0, "JW setConfigurationChanged: mIsshowTabLayout true"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sj, LV - setConfigurationChanged() - mDialogsManager.IsDialogShown(CREATE_LABEL_DIALOG) : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 336
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v1, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 335
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPopupLayout()V

    .line 339
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getCurrnetType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    .line 342
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mIsResetTabLayout:Z

    .line 354
    :goto_0
    return-void

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getCurrnetType()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v1

    invoke-direct {p0, v0, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->showTabLayout(IZZ)V

    .line 350
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mIsResetTabLayout:Z

    goto :goto_0
.end method

.method public setLabelMode(ILandroid/graphics/Rect;Ljava/lang/String;I)V
    .locals 4
    .param p1, "resId"    # I
    .param p2, "targetOrgRoi"    # Landroid/graphics/Rect;
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "styleNum"    # I

    .prologue
    .line 937
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    if-nez v0, :cond_0

    .line 938
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelCallback:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    .line 940
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mLabelEffect:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->setLabelMode(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 942
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->init3DepthActionBar()V

    .line 943
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->invalidateViews()V

    .line 944
    return-void
.end method
