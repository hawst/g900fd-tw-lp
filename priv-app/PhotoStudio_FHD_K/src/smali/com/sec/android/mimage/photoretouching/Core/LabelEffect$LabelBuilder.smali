.class Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;
.super Ljava/lang/Object;
.source "LabelEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LabelBuilder"
.end annotation


# static fields
.field public static final LABEL_STYLE_1:I = 0x1

.field public static final LABEL_STYLE_10:I = 0xa

.field public static final LABEL_STYLE_11:I = 0xb

.field public static final LABEL_STYLE_12:I = 0xc

.field public static final LABEL_STYLE_13:I = 0xd

.field public static final LABEL_STYLE_14:I = 0xe

.field public static final LABEL_STYLE_15:I = 0xf

.field public static final LABEL_STYLE_16:I = 0x10

.field public static final LABEL_STYLE_17:I = 0x11

.field public static final LABEL_STYLE_18:I = 0x12

.field public static final LABEL_STYLE_19:I = 0x13

.field public static final LABEL_STYLE_2:I = 0x2

.field public static final LABEL_STYLE_20:I = 0x14

.field public static final LABEL_STYLE_21:I = 0x15

.field public static final LABEL_STYLE_22:I = 0x16

.field public static final LABEL_STYLE_23:I = 0x17

.field public static final LABEL_STYLE_24:I = 0x18

.field public static final LABEL_STYLE_3:I = 0x3

.field public static final LABEL_STYLE_4:I = 0x4

.field public static final LABEL_STYLE_5:I = 0x5

.field public static final LABEL_STYLE_6:I = 0x6

.field public static final LABEL_STYLE_7:I = 0x7

.field public static final LABEL_STYLE_8:I = 0x8

.field public static final LABEL_STYLE_9:I = 0x9


# instance fields
.field private TEXT_SIZE:I

.field private mContext:Landroid/content/Context;

.field private mLabelLayout:Landroid/widget/LinearLayout;

.field private mLeftImage:Landroid/widget/ImageView;

.field private mMiddleImage:Landroid/widget/LinearLayout;

.field private mRightImage:Landroid/widget/ImageView;

.field private mTextView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;Landroid/content/Context;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 829
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->this$0:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 818
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    .line 820
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->TEXT_SIZE:I

    .line 822
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    .line 823
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLeftImage:Landroid/widget/ImageView;

    .line 824
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mMiddleImage:Landroid/widget/LinearLayout;

    .line 825
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mRightImage:Landroid/widget/ImageView;

    .line 827
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    .line 830
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    .line 832
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f05021c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->TEXT_SIZE:I

    .line 834
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 835
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030047

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    .line 836
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0900e1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    .line 837
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0900de

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLeftImage:Landroid/widget/ImageView;

    .line 838
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0900e0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mMiddleImage:Landroid/widget/LinearLayout;

    .line 839
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0900e2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mRightImage:Landroid/widget/ImageView;

    .line 840
    return-void
.end method

.method private clearViewResource(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 1300
    if-eqz p1, :cond_1

    .line 1301
    instance-of v5, p1, Landroid/widget/ImageView;

    if-eqz v5, :cond_2

    move-object v5, p1

    .line 1302
    check-cast v5, Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1303
    .local v2, "d":Landroid/graphics/drawable/Drawable;
    instance-of v5, v2, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v5, :cond_0

    .line 1304
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1305
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 1306
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1309
    .end local v0    # "b":Landroid/graphics/Bitmap;
    :cond_0
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1327
    :cond_1
    return-void

    .line 1310
    .restart local p1    # "view":Landroid/view/View;
    :cond_2
    instance-of v5, p1, Landroid/widget/LinearLayout;

    if-eqz v5, :cond_1

    move-object v4, p1

    .line 1311
    check-cast v4, Landroid/widget/LinearLayout;

    .line 1312
    .local v4, "layout":Landroid/widget/LinearLayout;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 1313
    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1314
    .local v1, "childView":Landroid/view/View;
    instance-of v5, v1, Landroid/widget/ImageView;

    if-eqz v5, :cond_4

    move-object v5, v1

    .line 1315
    check-cast v5, Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1316
    .restart local v2    # "d":Landroid/graphics/drawable/Drawable;
    instance-of v5, v2, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v5, :cond_3

    .line 1317
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1318
    .restart local v0    # "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_3

    .line 1319
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1322
    .end local v0    # "b":Landroid/graphics/Bitmap;
    :cond_3
    check-cast v1, Landroid/widget/ImageView;

    .end local v1    # "childView":Landroid/view/View;
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1312
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private measureTextRect(Landroid/widget/TextView;)Landroid/graphics/Rect;
    .locals 7
    .param p1, "textView"    # Landroid/widget/TextView;

    .prologue
    const/4 v6, 0x0

    .line 1142
    const/16 v1, 0xa

    .line 1143
    .local v1, "padding":I
    new-instance v2, Landroid/graphics/Rect;

    const/16 v4, 0x12c

    const/16 v5, 0x32

    invoke-direct {v2, v6, v6, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1144
    .local v2, "textRect":Landroid/graphics/Rect;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v3

    .line 1146
    .local v3, "width":F
    invoke-virtual {p1, v6, v6}, Landroid/widget/TextView;->measure(II)V

    .line 1147
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    const/16 v5, 0xa

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v0

    .line 1149
    .local v0, "dpPadding":I
    invoke-virtual {p1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    invoke-virtual {p1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {v2, v6, v6, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 1150
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sj, LE - measureTextRect() - textView.width : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1151
    const-string v5, " / textView.height : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / textRect : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / width : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1150
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1153
    return-object v2
.end method

.method private resetLayoutSize()V
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 843
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 844
    .local v1, "sideParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLeftImage:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    .line 845
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLeftImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 846
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mRightImage:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 847
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mRightImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 849
    :cond_1
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 850
    .local v0, "middleParams":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v2, 0x11

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 851
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mMiddleImage:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_2

    .line 852
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mMiddleImage:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 853
    :cond_2
    return-void
.end method

.method private setLabelSettings(Ljava/lang/String;I)V
    .locals 25
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "style"    # I

    .prologue
    .line 856
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    const v21, 0x7f0900e1

    invoke-virtual/range {v20 .. v21}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    .line 858
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->resetLayoutSize()V

    .line 860
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    sget-object v21, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 862
    const/16 v19, 0x0

    .line 863
    .local v19, "typeface":Landroid/graphics/Typeface;
    const/4 v5, 0x0

    .line 864
    .local v5, "leftImageResId":I
    const/4 v8, 0x0

    .line 865
    .local v8, "middleImageResId":I
    const/4 v15, 0x0

    .line 867
    .local v15, "rightImageResId":I
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "sj, LE - setLabelSettings() - text : "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " / style : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 875
    packed-switch p2, :pswitch_data_0

    .line 1064
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->measureTextRect(Landroid/widget/TextView;)Landroid/graphics/Rect;

    move-result-object v17

    .line 1066
    .local v17, "textRect":Landroid/graphics/Rect;
    const/4 v6, 0x0

    .line 1067
    .local v6, "leftLabelBitmap":Landroid/graphics/Bitmap;
    const/4 v10, 0x0

    .line 1068
    .local v10, "middleLabelBitmap":Landroid/graphics/Bitmap;
    const/16 v16, 0x0

    .line 1070
    .local v16, "rightLabelBitmap":Landroid/graphics/Bitmap;
    const/16 v20, 0x1

    move/from16 v0, p2

    move/from16 v1, v20

    if-lt v0, v1, :cond_0

    const/16 v20, 0xc

    move/from16 v0, p2

    move/from16 v1, v20

    if-gt v0, v1, :cond_0

    .line 1071
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-static {v0, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 1072
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-static {v0, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 1073
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-static {v0, v15}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 1076
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLeftImage:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1079
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mRightImage:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1081
    if-eqz v19, :cond_1

    .line 1082
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1087
    :cond_1
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->width()I

    move-result v18

    .line 1088
    .local v18, "textWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mMiddleImage:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1091
    const/4 v9, 0x0

    .line 1092
    .local v9, "middleImageView":Landroid/widget/ImageView;
    packed-switch p2, :pswitch_data_1

    .line 1139
    :cond_2
    return-void

    .line 879
    .end local v6    # "leftLabelBitmap":Landroid/graphics/Bitmap;
    .end local v9    # "middleImageView":Landroid/widget/ImageView;
    .end local v10    # "middleLabelBitmap":Landroid/graphics/Bitmap;
    .end local v16    # "rightLabelBitmap":Landroid/graphics/Bitmap;
    .end local v17    # "textRect":Landroid/graphics/Rect;
    .end local v18    # "textWidth":I
    :pswitch_0
    const-string v20, "sans-serif-light"

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 880
    const v5, 0x7f0202f9

    .line 881
    const v8, 0x7f0202f8

    .line 882
    const v15, 0x7f0202fa

    .line 883
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070017

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 884
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    const/high16 v22, 0x40000000    # 2.0f

    const/high16 v23, 0x40000000    # 2.0f

    const v24, -0x9f70d3

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 888
    :pswitch_1
    const-string v20, "sans-serif"

    const/16 v21, 0x1

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 889
    const v5, 0x7f0202fc

    .line 890
    const v8, 0x7f0202fb

    .line 891
    const v15, 0x7f0202fd

    .line 892
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070018

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 893
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    const/high16 v22, 0x3f800000    # 1.0f

    const/high16 v23, 0x3f800000    # 1.0f

    const v24, 0x7affffff

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 897
    :pswitch_2
    const-string v20, "sans-serif"

    const/16 v21, 0x1

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 898
    const v5, 0x7f0202ff

    .line 899
    const v8, 0x7f0202fe

    .line 900
    const v15, 0x7f020300

    .line 901
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070019

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 902
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/high16 v21, 0x40400000    # 3.0f

    const/16 v22, 0x0

    const/high16 v23, 0x40400000    # 3.0f

    const v24, -0xf2f1f2

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 906
    :pswitch_3
    const-string v20, "sans-serif-light"

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 907
    const v5, 0x7f020302

    .line 908
    const v8, 0x7f020301

    .line 909
    const v15, 0x7f020303

    .line 910
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f07001a

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 911
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 915
    :pswitch_4
    const-string v20, "sans-serif"

    const/16 v21, 0x1

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 916
    const v5, 0x7f020305

    .line 917
    const v8, 0x7f020304

    .line 918
    const v15, 0x7f020306

    .line 919
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f07001b

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 920
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 924
    :pswitch_5
    const-string v20, "sans-serif"

    const/16 v21, 0x1

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 925
    const v5, 0x7f020308

    .line 926
    const v8, 0x7f020307

    .line 927
    const v15, 0x7f020309

    .line 928
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f07001c

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 929
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 933
    :pswitch_6
    const-string v20, "sans-serif"

    const/16 v21, 0x3

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 934
    const v5, 0x7f02030b

    .line 935
    const v8, 0x7f02030a

    .line 936
    const v15, 0x7f02030c

    .line 937
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f07001d

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 938
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 942
    :pswitch_7
    sget-object v20, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 943
    const v5, 0x7f02030e

    .line 944
    const v8, 0x7f02030d

    .line 945
    const v15, 0x7f02030f

    .line 946
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f07001e

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 947
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 951
    :pswitch_8
    const-string v20, "sans-serif"

    const/16 v21, 0x1

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 952
    const v5, 0x7f020313

    .line 953
    const v8, 0x7f020311

    .line 954
    const v15, 0x7f020314

    .line 955
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f07001f

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 956
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 960
    :pswitch_9
    sget-object v20, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 961
    const v5, 0x7f020316

    .line 962
    const v8, 0x7f020315

    .line 963
    const v15, 0x7f020317

    .line 964
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070020

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 965
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 969
    :pswitch_a
    const-string v20, "sans-serif-condensed"

    const/16 v21, 0x1

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 970
    const v5, 0x7f020319

    .line 971
    const v8, 0x7f020318

    .line 972
    const v15, 0x7f02031a

    .line 973
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070021

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 974
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 978
    :pswitch_b
    const-string v20, "sans-serif-light"

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 979
    const v5, 0x7f02031c

    .line 980
    const v8, 0x7f02031b

    .line 981
    const v15, 0x7f02031d

    .line 982
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070022

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 983
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 988
    :pswitch_c
    const-string v20, "sans-serif-light"

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 989
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070023

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 990
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 994
    :pswitch_d
    const-string v20, "sans-serif-condensed"

    const/16 v21, 0x3

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 995
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070024

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 996
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    const/high16 v22, 0x3f800000    # 1.0f

    const/high16 v23, 0x3f800000    # 1.0f

    const v24, 0x7bffffff

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 1000
    :pswitch_e
    const-string v20, "sans-serif"

    const/16 v21, 0x1

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 1001
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070025

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1002
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    const/16 v22, 0x0

    const/high16 v23, 0x3f800000    # 1.0f

    const/high16 v24, -0x1000000

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 1006
    :pswitch_f
    const-string v20, "sans-serif-light"

    const/16 v21, 0x2

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 1007
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070026

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1008
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 1012
    :pswitch_10
    const-string v20, "sans-serif"

    const/16 v21, 0x1

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 1013
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070027

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1014
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 1018
    :pswitch_11
    const-string v20, "sans-serif"

    const/16 v21, 0x3

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 1019
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070028

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1020
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 1024
    :pswitch_12
    const-string v20, "sans-serif-condensed"

    const/16 v21, 0x1

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 1025
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070029

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1026
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 1030
    :pswitch_13
    const-string v20, "sans-serif-thin"

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 1031
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f07002a

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1032
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 1036
    :pswitch_14
    sget-object v20, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    const/16 v21, 0x1

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 1037
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f07002b

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1038
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    const/16 v22, 0x0

    const/high16 v23, 0x3f800000    # 1.0f

    const/high16 v24, -0x1000000

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 1042
    :pswitch_15
    sget-object v20, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 1043
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f07002c

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1044
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 1048
    :pswitch_16
    sget-object v20, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    const/16 v21, 0x3

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 1049
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f07002d

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1050
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 1054
    :pswitch_17
    sget-object v20, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v19

    .line 1055
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f07002e

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1056
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    goto/16 :goto_0

    .line 1094
    .restart local v6    # "leftLabelBitmap":Landroid/graphics/Bitmap;
    .restart local v9    # "middleImageView":Landroid/widget/ImageView;
    .restart local v10    # "middleLabelBitmap":Landroid/graphics/Bitmap;
    .restart local v16    # "rightLabelBitmap":Landroid/graphics/Bitmap;
    .restart local v17    # "textRect":Landroid/graphics/Rect;
    .restart local v18    # "textWidth":I
    :pswitch_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f020310

    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 1095
    .local v11, "middle_left_bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f020312

    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 1097
    .local v13, "middle_right_bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    div-int v20, v18, v20

    move/from16 v0, v20

    int-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-int v12, v0

    .line 1098
    .local v12, "middle_left_loop":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-le v4, v12, :cond_3

    .line 1103
    new-instance v9, Landroid/widget/ImageView;

    .end local v9    # "middleImageView":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v9, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1104
    .restart local v9    # "middleImageView":Landroid/widget/ImageView;
    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mMiddleImage:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1106
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    div-int v20, v18, v20

    move/from16 v0, v20

    int-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-int v14, v0

    .line 1107
    .local v14, "middle_right_loop":I
    const/4 v4, 0x0

    :goto_2
    if-gt v4, v14, :cond_2

    .line 1108
    new-instance v9, Landroid/widget/ImageView;

    .end local v9    # "middleImageView":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v9, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1109
    .restart local v9    # "middleImageView":Landroid/widget/ImageView;
    invoke-virtual {v9, v13}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mMiddleImage:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1107
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1099
    .end local v14    # "middle_right_loop":I
    :cond_3
    new-instance v9, Landroid/widget/ImageView;

    .end local v9    # "middleImageView":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v9, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1100
    .restart local v9    # "middleImageView":Landroid/widget/ImageView;
    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1101
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mMiddleImage:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1098
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1124
    .end local v4    # "i":I
    .end local v11    # "middle_left_bitmap":Landroid/graphics/Bitmap;
    .end local v12    # "middle_left_loop":I
    .end local v13    # "middle_right_bitmap":Landroid/graphics/Bitmap;
    :pswitch_19
    if-eqz v10, :cond_2

    .line 1126
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v7, v0

    .line 1127
    .local v7, "loop":I
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "sj, LE - setLabelSettings() - textWidth : "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " / middleLabelBitmap.getWidth() : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    .line 1128
    const-string v21, " / loop : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    .line 1127
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1129
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_3
    if-gt v4, v7, :cond_2

    .line 1130
    new-instance v9, Landroid/widget/ImageView;

    .end local v9    # "middleImageView":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v9, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1131
    .restart local v9    # "middleImageView":Landroid/widget/ImageView;
    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1132
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mMiddleImage:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1129
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 875
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch

    .line 1092
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_18
        :pswitch_19
        :pswitch_19
        :pswitch_19
    .end packed-switch
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1331
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    .line 1333
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    .line 1334
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLeftImage:Landroid/widget/ImageView;

    .line 1335
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mMiddleImage:Landroid/widget/LinearLayout;

    .line 1336
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mRightImage:Landroid/widget/ImageView;

    .line 1337
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    .line 1338
    return-void
.end method

.method public getLabelBitmap(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 29
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "style"    # I

    .prologue
    .line 1157
    const/4 v4, 0x0

    .line 1159
    .local v4, "labelBitmap":Landroid/graphics/Bitmap;
    invoke-direct/range {p0 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->setLabelSettings(Ljava/lang/String;I)V

    .line 1163
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->this$0:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v27

    .line 1164
    .local v27, "viewTransform":Landroid/graphics/Matrix;
    new-instance v17, Landroid/graphics/Matrix;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Matrix;-><init>()V

    .line 1165
    .local v17, "invertMatrix":Landroid/graphics/Matrix;
    const/16 v5, 0x9

    new-array v0, v5, [F

    move-object/from16 v26, v0

    .line 1167
    .local v26, "val":[F
    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1168
    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1170
    const/4 v5, 0x0

    aget v20, v26, v5

    .line 1171
    .local v20, "matrixScaleX":F
    const/4 v5, 0x4

    aget v21, v26, v5

    .line 1173
    .local v21, "matrixScaleY":F
    new-instance v14, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mContext:Landroid/content/Context;

    invoke-direct {v14, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1174
    .local v14, "calcText":Landroid/widget/TextView;
    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1175
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->TEXT_SIZE:I

    int-to-float v0, v5

    move/from16 v24, v0

    .line 1176
    .local v24, "scaleTextSize":F
    const/4 v5, 0x0

    move/from16 v0, v24

    invoke-virtual {v14, v5, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1177
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->measureTextRect(Landroid/widget/TextView;)Landroid/graphics/Rect;

    move-result-object v25

    .line 1179
    .local v25, "textRect":Landroid/graphics/Rect;
    const/16 v22, 0x0

    .line 1180
    .local v22, "middleLayoutWidth":I
    const/4 v5, 0x1

    move/from16 v0, p2

    if-lt v0, v5, :cond_7

    const/16 v5, 0xc

    move/from16 v0, p2

    if-gt v0, v5, :cond_7

    .line 1182
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mMiddleImage:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/widget/LinearLayout;->measure(II)V

    .line 1183
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mMiddleImage:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v22

    .line 1190
    :cond_0
    :goto_0
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    move/from16 v0, v22

    int-to-float v6, v0

    div-float v15, v5, v6

    .line 1191
    .local v15, "convertScale":F
    const/4 v5, 0x4

    move/from16 v0, p2

    if-ne v0, v5, :cond_1

    .line 1192
    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float/2addr v15, v5

    .line 1197
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sj, LE - getLabelBitmap() - textRect.width() : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / middleLayoutWidth : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1198
    const-string v6, " / convertScale : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1197
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1203
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_6

    .line 1204
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/widget/LinearLayout;->measure(II)V

    .line 1205
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v11

    invoke-virtual {v5, v6, v7, v10, v11}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 1206
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->buildDrawingCache()V

    .line 1207
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v19

    .line 1213
    .local v19, "layoutBitmap":Landroid/graphics/Bitmap;
    if-eqz v19, :cond_5

    .line 1215
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    .line 1216
    .local v13, "bitmapWidth":I
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    .line 1218
    .local v12, "bitmapHeight":I
    const/4 v5, 0x0

    cmpg-float v5, v15, v5

    if-lez v5, :cond_2

    invoke-static {v15}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1219
    :cond_2
    const/high16 v15, 0x3f800000    # 1.0f

    .line 1221
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->this$0:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->this$0:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    if-lt v5, v6, :cond_8

    const/16 v18, 0x1

    .line 1222
    .local v18, "isImageLandscape":Z
    :goto_1
    const/high16 v23, 0x3f800000    # 1.0f

    .line 1234
    .local v23, "orientationScale":F
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v15

    float-to-int v13, v5

    .line 1235
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v15

    float-to-int v12, v5

    .line 1237
    const/4 v5, 0x1

    move-object/from16 v0, v19

    invoke-static {v0, v13, v12, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1247
    .local v2, "tmpLabelBitmap":Landroid/graphics/Bitmap;
    const/4 v5, 0x1

    move/from16 v0, p2

    if-lt v0, v5, :cond_9

    const/16 v5, 0xc

    move/from16 v0, p2

    if-gt v0, v5, :cond_9

    .line 1248
    move-object v4, v2

    .line 1282
    :cond_4
    :goto_2
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->recycle()V

    .line 1283
    const/16 v19, 0x0

    .line 1285
    .end local v2    # "tmpLabelBitmap":Landroid/graphics/Bitmap;
    .end local v12    # "bitmapHeight":I
    .end local v13    # "bitmapWidth":I
    .end local v18    # "isImageLandscape":Z
    .end local v23    # "orientationScale":F
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLabelLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->destroyDrawingCache()V

    .line 1290
    .end local v19    # "layoutBitmap":Landroid/graphics/Bitmap;
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mLeftImage:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->clearViewResource(Landroid/view/View;)V

    .line 1291
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mMiddleImage:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->clearViewResource(Landroid/view/View;)V

    .line 1292
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mRightImage:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->clearViewResource(Landroid/view/View;)V

    .line 1295
    return-object v4

    .line 1185
    .end local v15    # "convertScale":F
    :cond_7
    const/16 v5, 0xd

    move/from16 v0, p2

    if-lt v0, v5, :cond_0

    const/16 v5, 0x18

    move/from16 v0, p2

    if-gt v0, v5, :cond_0

    .line 1187
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->measure(II)V

    .line 1188
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v22

    goto/16 :goto_0

    .line 1221
    .restart local v12    # "bitmapHeight":I
    .restart local v13    # "bitmapWidth":I
    .restart local v15    # "convertScale":F
    .restart local v19    # "layoutBitmap":Landroid/graphics/Bitmap;
    :cond_8
    const/16 v18, 0x0

    goto :goto_1

    .line 1249
    .restart local v2    # "tmpLabelBitmap":Landroid/graphics/Bitmap;
    .restart local v18    # "isImageLandscape":Z
    .restart local v23    # "orientationScale":F
    :cond_9
    const/16 v5, 0xd

    move/from16 v0, p2

    if-lt v0, v5, :cond_4

    const/16 v5, 0x18

    move/from16 v0, p2

    if-gt v0, v5, :cond_4

    .line 1250
    const/high16 v5, 0x42200000    # 40.0f

    mul-float v5, v5, v20

    float-to-int v0, v5

    move/from16 v28, v0

    .line 1251
    .local v28, "wPadding":I
    const/high16 v5, 0x42200000    # 40.0f

    mul-float v5, v5, v21

    float-to-int v0, v5

    move/from16 v16, v0

    .line 1262
    .local v16, "hPadding":I
    if-eqz v2, :cond_4

    .line 1263
    mul-int v5, v13, v12

    new-array v3, v5, [I

    .line 1264
    .local v3, "pixels":[I
    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    .end local v4    # "labelBitmap":Landroid/graphics/Bitmap;
    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1265
    add-int v5, v13, v28

    add-int v6, v12, v16

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1266
    .restart local v4    # "labelBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    const/4 v6, 0x0

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 1267
    .local v8, "x":I
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    const/4 v6, 0x0

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 1268
    .local v9, "y":I
    const/4 v6, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    move-object v5, v3

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1269
    const/4 v3, 0x0

    .line 1270
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1271
    const/4 v2, 0x0

    goto/16 :goto_2
.end method
