.class public Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "StampView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$AsyncCallback;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ResolverSetAsAdapter;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;
    }
.end annotation


# instance fields
.field private MAX_RECENTLY:I

.field private final REDOALL_DIALOG:I

.field private final UNDOALL_DIALOG:I

.field private currDepth:Ljava/lang/String;

.field isStampApplied:Z

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentSaveSize:I

.field private mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mFrameLayer:Landroid/widget/FrameLayout;

.field private mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mMultiDirectionSlidingDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mOptionItemId:I

.field private mPaint:Landroid/graphics/Paint;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mResId:I

.field private mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

.field private mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

.field private mTime:J

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;

.field onStampCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnStampCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p6, "decoManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 98
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 135
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->onStampCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnStampCallback;

    .line 721
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->isStampApplied:Z

    .line 6089
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 6090
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 6091
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    .line 6092
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 6093
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 6094
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 6095
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 6096
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 6097
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 6098
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 6099
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 6100
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 6101
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mPaint:Landroid/graphics/Paint;

    .line 6102
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->UNDOALL_DIALOG:I

    .line 6103
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->REDOALL_DIALOG:I

    .line 6104
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    .line 6106
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mOptionItemId:I

    .line 6108
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mMultiDirectionSlidingDrawer:Lcom/sec/android/mimage/photoretouching/Gui/MultiDirectionSlidingDrawer;

    .line 6109
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mCurrentSaveSize:I

    .line 6110
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTime:J

    .line 6111
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    .line 6112
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mFrameLayer:Landroid/widget/FrameLayout;

    .line 6113
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mResId:I

    .line 6114
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->MAX_RECENTLY:I

    .line 100
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    .line 101
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 102
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 103
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 104
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 105
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 107
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->setInterface(Ljava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setmTestInterface(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$DrawListners;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->initStampsOrFrames()V

    .line 110
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mPaint:Landroid/graphics/Paint;

    .line 111
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 115
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 117
    :cond_0
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    .line 118
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setOnCallback(Lcom/sec/android/mimage/photoretouching/Core/StampEffect$OnMyStampCallback;)V

    .line 131
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->initTabLayout()V

    .line 132
    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->setViewLayerType(I)V

    .line 133
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)J
    .locals 2

    .prologue
    .line 6110
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTime:J

    return-wide v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 6094
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 6089
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 6089
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)I
    .locals 1

    .prologue
    .line 6106
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mOptionItemId:I

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;I)Z
    .locals 1

    .prologue
    .line 5609
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->runOptionItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V
    .locals 0

    .prologue
    .line 311
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->init3DepthActionBar()V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/StampEffect;
    .locals 1

    .prologue
    .line 6104
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    return-object v0
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .locals 1

    .prologue
    .line 6099
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V
    .locals 0

    .prologue
    .line 439
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->doCancel()V

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)I
    .locals 1

    .prologue
    .line 6114
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->MAX_RECENTLY:I

    return v0
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;
    .locals 1

    .prologue
    .line 6111
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;J)V
    .locals 1

    .prologue
    .line 6110
    iput-wide p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTime:J

    return-void
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;I)I
    .locals 1

    .prologue
    .line 428
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getCurrentResId(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V
    .locals 0

    .prologue
    .line 462
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->doDone()V

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getImageEditViewWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;I)V
    .locals 0

    .prologue
    .line 6109
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mCurrentSaveSize:I

    return-void
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 6095
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V
    .locals 0

    .prologue
    .line 6098
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 6090
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 6091
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 6090
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 6092
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 6093
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 6097
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)I
    .locals 1

    .prologue
    .line 6109
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mCurrentSaveSize:I

    return v0
.end method

.method private checkHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 5587
    const/4 v0, 0x0

    .line 5588
    .local v0, "isNoShow":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->isDoNotShowPopup(Landroid/content/Context;I)Z

    move-result v0

    .line 5590
    if-nez v0, :cond_0

    .line 5591
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-nez v1, :cond_0

    .line 5592
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->initHelpPopup(I)V

    .line 5593
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->showCurrentModeHelpPopup()V

    .line 5596
    :cond_0
    return-void
.end method

.method private doCancel()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 440
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->isStampApplied:Z

    .line 441
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->Release()V

    .line 443
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    .line 444
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mFrameLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 445
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    if-eqz v0, :cond_1

    .line 446
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->resetStampLayer()V

    .line 447
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->releaseViewPager()V

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_2

    .line 450
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 451
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 452
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    .line 453
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 452
    invoke-static {v0, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 454
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 455
    .local v1, "output":[I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 456
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 457
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 455
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 459
    .end local v1    # "output":[I
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->set2depth()V

    .line 460
    return-void
.end method

.method private doDone()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 463
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->isStampApplied:Z

    .line 464
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 465
    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v9

    .line 466
    .local v9, "resId":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v0, v9}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->changeId(I)I

    move-result v8

    .line 467
    .local v8, "currId":I
    invoke-direct {p0, v9}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getCurrentResId(I)I

    .line 468
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->Release()V

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    if-eqz v0, :cond_1

    .line 471
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    const/4 v1, 0x1

    invoke-virtual {v0, v9, v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->applyPreview(IZ)Landroid/graphics/Bitmap;

    .line 472
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_1

    .line 473
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->getCurBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBuffer(Landroid/graphics/Bitmap;)V

    .line 475
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    .line 477
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_2

    .line 478
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 479
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    .line 480
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 479
    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 481
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 482
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 483
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 484
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 485
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 482
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 489
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v0, v8}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setResId(I)V

    .line 490
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-direct {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/StampEffect;)V

    .line 491
    .local v4, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_3

    .line 492
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 493
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 494
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getEnhance()Z

    move-result v5

    .line 492
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V

    .line 497
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    if-eqz v0, :cond_4

    .line 498
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->releaseViewPager()V

    .line 499
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->resetStampLayer()V

    .line 504
    :cond_4
    return-void
.end method

.method private getCurrentResId(I)I
    .locals 4
    .param p1, "pageNum"    # I

    .prologue
    const v3, 0x315000a0

    const v2, 0x3150008b

    .line 429
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mResId:I

    if-lt v0, v2, :cond_1

    .line 430
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mResId:I

    const v1, 0x3150009f

    if-gt v0, v1, :cond_1

    .line 431
    add-int v0, v2, p1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mResId:I

    .line 436
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mResId:I

    return v0

    .line 432
    :cond_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mResId:I

    if-lt v0, v3, :cond_0

    .line 433
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mResId:I

    const v1, 0x315000ac

    if-gt v0, v1, :cond_0

    .line 434
    add-int v0, v3, p1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mResId:I

    goto :goto_0
.end method

.method private init2DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 237
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 238
    const-string v0, "2D"

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->currDepth:Ljava/lang/String;

    .line 239
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v4, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 241
    const/4 v1, 0x4

    const/4 v2, 0x1

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$3;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 240
    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 272
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 273
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$4;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 272
    invoke-virtual {v0, v1, v4, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 301
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 303
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 304
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableCancel()V

    .line 305
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 306
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 309
    :cond_0
    return-void
.end method

.method private init3DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 312
    const-string v0, "JW init3DepthActionBar"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 314
    const-string v0, "3D"

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->currDepth:Ljava/lang/String;

    .line 315
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 317
    const/4 v1, 0x4

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$5;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 316
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 346
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 347
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 346
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 407
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 410
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableCancel()V

    .line 413
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 416
    :cond_0
    return-void
.end method

.method private initHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 5599
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 5600
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$17;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$17;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->setHelpPopupCallback(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;)V

    .line 5607
    return-void
.end method

.method private initSaveOptionDialog()V
    .locals 11

    .prologue
    const v10, 0x7f0601f0

    const v9, 0x7f0601cb

    const/16 v2, 0x9

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 5333
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5394
    :goto_0
    return-void

    .line 5337
    :cond_0
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$8;

    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 5356
    .local v8, "saveListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 5357
    const v3, 0x7f0601cd

    .line 5358
    const/4 v5, 0x1

    const v7, 0x103012e

    .line 5356
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZZLandroid/graphics/Point;I)V

    .line 5360
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 5362
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5363
    const/16 v2, 0x810

    move v3, v9

    move v5, v9

    move-object v7, v8

    .line 5362
    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 5371
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5372
    const/16 v2, 0x820

    move v3, v10

    move v5, v10

    move-object v7, v8

    .line 5371
    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 5374
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 5375
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$9;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 5374
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 5382
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$10;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 5393
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    goto :goto_0
.end method

.method private initSetAsDialog()V
    .locals 7

    .prologue
    .line 6005
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 6006
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 6007
    const v2, 0x7f090158

    const v3, 0x7f0600a3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 6008
    const v6, 0x103012e

    .line 6006
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 6009
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 6011
    :cond_0
    return-void
.end method

.method private initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isShareVia"    # Z

    .prologue
    .line 5752
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 5753
    .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    const/4 v2, 0x0

    .line 5754
    .local v2, "shareChooser":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 5755
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    const v4, 0x7f06000f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 5759
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/content/pm/LabeledIntent;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/pm/LabeledIntent;

    .line 5760
    .local v0, "extraIntents":[Landroid/content/pm/LabeledIntent;
    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 5761
    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 5762
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 5943
    return-void

    .line 5757
    .end local v0    # "extraIntents":[Landroid/content/pm/LabeledIntent;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    const v4, 0x7f0600a3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method private initShareViaDialog()V
    .locals 7

    .prologue
    .line 5996
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 5997
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 5998
    const v2, 0x7f090157

    const v3, 0x7f06000f

    const/4 v4, 0x0

    .line 5999
    const/4 v5, 0x0

    const v6, 0x103012e

    .line 5997
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 6000
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 6002
    :cond_0
    return-void
.end method

.method private initTabLayout()V
    .locals 2

    .prologue
    .line 5322
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_0

    .line 5323
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->initDataBase(Landroid/content/Context;)V

    .line 5324
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setDrawerHandleVisibility(I)V

    .line 5325
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTab()V

    .line 5326
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    if-eqz v0, :cond_0

    .line 5327
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->onStampCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnStampCallback;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->addStamp(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnStampCallback;)V

    .line 5330
    :cond_0
    return-void
.end method

.method private initUndoRedoAllDialog()V
    .locals 15

    .prologue
    const v6, 0x103012e

    const/16 v1, 0x1000

    const/16 v14, 0x500

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 5397
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 5398
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5399
    const v3, 0x7f0600a6

    move v4, v2

    .line 5398
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 5402
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 5403
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5404
    const v3, 0x7f06009c

    .line 5405
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$11;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 5403
    invoke-virtual {v0, v14, v3, v5, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 5412
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060007

    .line 5413
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$12;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 5412
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 5420
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060009

    .line 5421
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$13;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 5420
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 5429
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 5431
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5432
    const/4 v9, 0x2

    const v10, 0x7f0600a1

    move v8, v1

    move v11, v2

    move-object v12, v5

    move v13, v6

    .line 5431
    invoke-virtual/range {v7 .. v13}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 5435
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 5436
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5437
    const v1, 0x7f0601ce

    .line 5438
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$14;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 5436
    invoke-virtual {v0, v14, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 5445
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 5446
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$15;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 5445
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 5453
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 5454
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$16;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    .line 5453
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 5462
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 5465
    :cond_0
    return-void
.end method

.method private runOptionItem(I)Z
    .locals 9
    .param p1, "optionItemId"    # I

    .prologue
    const v8, 0x7f090156

    const v7, 0x7a1200

    const/4 v6, 0x7

    const/4 v5, 0x0

    const/16 v4, 0x9

    .line 5610
    const/4 v1, 0x0

    .line 5611
    .local v1, "ret":Z
    const/4 v2, 0x0

    .line 5612
    .local v2, "uri":Landroid/net/Uri;
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mOptionItemId:I

    .line 5613
    sparse-switch p1, :sswitch_data_0

    .line 5747
    :cond_0
    :goto_0
    return v1

    .line 5615
    :sswitch_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5616
    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 5617
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5618
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5619
    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 5624
    :goto_1
    const/4 v1, 0x1

    .line 5625
    goto :goto_0

    .line 5622
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->openCamera(Landroid/content/Context;)V

    goto :goto_1

    .line 5627
    :sswitch_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 5647
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mCurrentSaveSize:I

    .line 5648
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5649
    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 5650
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5651
    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 5652
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5653
    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 5671
    :cond_2
    :goto_2
    const/4 v1, 0x1

    .line 5672
    goto :goto_0

    .line 5654
    :cond_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5655
    const v4, 0x7f090157

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 5660
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 5661
    if-nez v2, :cond_4

    .line 5662
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 5665
    :cond_4
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x1

    .line 5664
    invoke-direct {p0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    goto :goto_2

    .line 5674
    :sswitch_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 5675
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 5678
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v4

    .line 5677
    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5680
    .local v0, "fileName":Ljava/lang/String;
    if-nez v0, :cond_6

    .line 5681
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    .line 5685
    :goto_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v0, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTextToDialog(Ljava/lang/String;I)V

    .line 5688
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v4, 0x2e000000

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 5689
    const/4 v1, 0x1

    .line 5690
    goto/16 :goto_0

    .line 5683
    .restart local v0    # "fileName":Ljava/lang/String;
    :cond_6
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 5693
    .end local v0    # "fileName":Ljava/lang/String;
    :sswitch_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 5713
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mCurrentSaveSize:I

    .line 5714
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5715
    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 5716
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5717
    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 5718
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5719
    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 5732
    :cond_7
    :goto_4
    const/4 v1, 0x1

    .line 5733
    goto/16 :goto_0

    .line 5720
    :cond_8
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 5721
    const v4, 0x7f090158

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 5723
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 5724
    if-nez v2, :cond_9

    .line 5725
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 5727
    :cond_9
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    .line 5729
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090158

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    goto :goto_4

    .line 5736
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v3, :cond_a

    .line 5737
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 5738
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    .line 5739
    :cond_a
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setVisibilityDirectly(I)V

    .line 5741
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 5742
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .line 5743
    const/high16 v4, 0x17000000

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 5613
    :sswitch_data_0
    .sparse-switch
        0x7f090007 -> :sswitch_2
        0x7f090157 -> :sswitch_1
        0x7f090158 -> :sswitch_3
        0x7f09015a -> :sswitch_0
        0x7f09015b -> :sswitch_4
    .end sparse-switch
.end method

.method private set2depth()V
    .locals 0

    .prologue
    .line 507
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->init2DepthActionBar()V

    .line 521
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->invalidateViews()V

    .line 522
    return-void
.end method

.method private showTabLayout(IZZ)V
    .locals 2
    .param p1, "assistantType"    # I
    .param p2, "initByconfig"    # Z
    .param p3, "doOpen"    # Z

    .prologue
    .line 884
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setCurrentType(I)V

    .line 885
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;IZZ)V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$AsyncCallback;)V

    .line 5317
    .local v0, "async":Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 5319
    return-void
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 629
    const/4 v0, 0x1

    return v0
.end method

.method public backPressed()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 191
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledDone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 193
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->doCancel()V

    .line 194
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->show()V

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    if-nez v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->openDrawer()V

    .line 227
    :cond_1
    :goto_0
    return-void

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledCancel()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    if-eqz v0, :cond_3

    .line 202
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->resetStampLayer()V

    .line 203
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->releaseViewPager()V

    .line 205
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_4

    .line 206
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 208
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 209
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    .line 210
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 209
    invoke-static {v0, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 211
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 212
    .local v1, "output":[I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 213
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 214
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 212
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 217
    .end local v1    # "output":[I
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_5

    .line 218
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->saveToDB()V

    .line 220
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_6

    .line 221
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 222
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    .line 224
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .line 225
    const/high16 v2, 0x31000000

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 809
    return-void
.end method

.method public drawerCloseListner()V
    .locals 2

    .prologue
    .line 423
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->currDepth:Ljava/lang/String;

    const-string v1, "3D"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableCancel()V

    .line 425
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 427
    :cond_0
    return-void
.end method

.method public drawerOpenListner()V
    .locals 2

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->currDepth:Ljava/lang/String;

    const-string v1, "2D"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableCancel()V

    .line 421
    :cond_0
    return-void
.end method

.method public getActionBar()Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 634
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 824
    const/4 v0, 0x0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 829
    const/4 v0, 0x0

    return v0
.end method

.method public getResId()I
    .locals 1

    .prologue
    .line 6087
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mResId:I

    return v0
.end method

.method public getStatusHeight()I
    .locals 1

    .prologue
    .line 837
    const/4 v0, 0x0

    return v0
.end method

.method public initActionbar()V
    .locals 0

    .prologue
    .line 231
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->init2DepthActionBar()V

    .line 234
    return-void
.end method

.method public initButtons()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 177
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v1, :cond_1

    .line 179
    const/16 v0, 0xf

    invoke-direct {p0, v0, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->showTabLayout(IZZ)V

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    const/16 v0, 0xe

    invoke-direct {p0, v0, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->showTabLayout(IZZ)V

    goto :goto_0
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 528
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->initUndoRedoAllDialog()V

    .line 530
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->initSaveOptionDialog()V

    .line 531
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->initShareViaDialog()V

    .line 532
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->initSetAsDialog()V

    .line 534
    :cond_0
    return-void
.end method

.method public initEffect()V
    .locals 0

    .prologue
    .line 164
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 561
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 796
    return-void
.end method

.method public initTrayLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 800
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 801
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 803
    :cond_0
    return-void
.end method

.method public initView()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .line 169
    const v1, 0x7f0900b0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 168
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mFrameLayer:Landroid/widget/FrameLayout;

    .line 170
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mFrameLayer:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 171
    const/high16 v0, 0x31500000

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->checkHelpPopup(I)V

    .line 172
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 704
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 705
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 706
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 707
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 719
    :cond_0
    :goto_0
    return-void

    .line 709
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_2

    .line 710
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 711
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 712
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 714
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .line 715
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 662
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    .line 663
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 664
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mPaint:Landroid/graphics/Paint;

    .line 666
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 667
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 669
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getStampsOrFrames()Ljava/util/ArrayList;

    move-result-object v2

    .line 670
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 669
    if-lt v0, v2, :cond_5

    .line 677
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->destroyStampsOrFrames()V

    .line 679
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 680
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 681
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 682
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v2, :cond_0

    .line 683
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->destroy()V

    .line 684
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    if-eqz v2, :cond_1

    .line 685
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->onDestroy()V

    .line 687
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mFrameLayer:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_2

    .line 688
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mFrameLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 689
    :cond_2
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mFrameLayer:Landroid/widget/FrameLayout;

    .line 690
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    if-eqz v2, :cond_3

    .line 691
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->removeAllViews()V

    .line 692
    :cond_3
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    .line 693
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 696
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_4

    .line 697
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 700
    :cond_4
    return-void

    .line 671
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 672
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getStampsOrFrames()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 673
    .local v1, "mArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 670
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mPaint:Landroid/graphics/Paint;

    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 555
    :cond_0
    return-void
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 725
    const/16 v3, 0x17

    if-eq p1, v3, :cond_0

    .line 726
    const/16 v3, 0x42

    if-ne p1, v3, :cond_4

    .line 727
    :cond_0
    const/4 v0, 0x0

    .line 728
    .local v0, "isFocused":Z
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v2, :cond_1

    .line 729
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    move-result v0

    .line 732
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getGridView()Landroid/widget/GridView;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 733
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getGridView()Landroid/widget/GridView;

    move-result-object v2

    .line 734
    invoke-virtual {v2}, Landroid/widget/GridView;->getFocusedChild()Landroid/view/View;

    move-result-object v2

    .line 733
    if-eqz v2, :cond_2

    .line 735
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getGridView()Landroid/widget/GridView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/GridView;->getFocusedChild()Landroid/view/View;

    move-result-object v2

    .line 736
    invoke-virtual {v2}, Landroid/view/View;->performClick()Z

    .line 738
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v2, :cond_3

    .line 739
    if-nez v0, :cond_3

    .line 740
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 789
    .end local v0    # "isFocused":Z
    :cond_3
    :goto_0
    return v1

    .line 743
    :cond_4
    const/4 v3, 0x4

    if-ne p1, v3, :cond_5

    .line 744
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->backPressed()V

    goto :goto_0

    .line 747
    :cond_5
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 759
    :pswitch_0
    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->isStampApplied:Z

    if-eqz v3, :cond_3

    .line 761
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->isFocusViewPager()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 762
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 763
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->focusCancelAndDone(I)V

    goto :goto_0

    .line 750
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isCancelFocused()Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isDoneFocused()Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_6
    move v1, v2

    .line 751
    goto :goto_0

    .line 753
    :cond_7
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->isFocusViewPager()Z

    move-result v2

    if-nez v2, :cond_3

    .line 754
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->focusViewPager()V

    goto :goto_0

    .line 765
    :cond_8
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isCancelFocused()Z

    move-result v3

    if-nez v3, :cond_a

    :cond_9
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isDoneFocused()Z

    move-result v3

    if-eqz v3, :cond_b

    :cond_a
    move v1, v2

    .line 766
    goto :goto_0

    .line 770
    :cond_b
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->focusViewPager()V

    goto/16 :goto_0

    .line 776
    :pswitch_2
    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->isStampApplied:Z

    if-eqz v3, :cond_3

    .line 777
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isCancelFocused()Z

    move-result v3

    if-nez v3, :cond_c

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isDoneFocused()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 778
    :cond_c
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->focusViewPager()V

    goto/16 :goto_0

    .line 779
    :cond_d
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->isFocusViewPager()Z

    move-result v3

    if-eqz v3, :cond_e

    move v1, v2

    .line 780
    goto/16 :goto_0

    .line 784
    :cond_e
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->focusCancelAndDone(I)V

    goto/16 :goto_0

    .line 747
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onLayout()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 5564
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 5565
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getImageEditViewWidth()I

    move-result v1

    .line 5566
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getImageEditViewHeight()I

    move-result v3

    .line 5565
    invoke-virtual {v0, v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 5567
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 5569
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_1

    .line 5570
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 5571
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 5572
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 5573
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_3

    .line 5575
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    .line 5576
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 5575
    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 5577
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 5578
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 5579
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 5580
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 5581
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 5578
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 5584
    :cond_3
    return-void
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 819
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->runOptionItem(I)Z

    .line 820
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 814
    return-void
.end method

.method public refreshView()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 6068
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getImageEditViewWidth()I

    move-result v4

    .line 6069
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->getImageEditViewHeight()I

    move-result v5

    .line 6068
    invoke-virtual {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 6071
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 6073
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 6074
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 6075
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 6076
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 6077
    const/4 v8, 0x1

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 6075
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 6079
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->invalidateViews()V

    .line 6080
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 3

    .prologue
    .line 592
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 594
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 595
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 598
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mFrameLayer:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 601
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mFrameLayer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 602
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .line 603
    const v1, 0x7f0900b0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 602
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mFrameLayer:Landroid/widget/FrameLayout;

    .line 604
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    if-eqz v0, :cond_1

    .line 611
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->onConfigurationChanged()V

    .line 612
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mFrameLayer:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 616
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->configurationChanged()V

    .line 617
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setDrawerHandleVisibility(I)V

    .line 618
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getCurrnetType()I

    move-result v0

    const/4 v1, 0x1

    .line 619
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v2

    .line 618
    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->showTabLayout(IZZ)V

    .line 623
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->invalidateViews()V

    .line 624
    return-void
.end method

.method public setResId(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 6083
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mResId:I

    .line 6084
    return-void
.end method

.method protected setStampMode(I)V
    .locals 3
    .param p1, "resId"    # I

    .prologue
    .line 153
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->setResId(I)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/StampEffect;->setResId(I)V

    .line 155
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampEffect:Lcom/sec/android/mimage/photoretouching/Core/StampEffect;

    invoke-direct {v0, v1, v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/StampEffect;Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    .line 156
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mFrameLayer:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mStampListView:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 158
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->init3DepthActionBar()V

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->invalidateViews()V

    .line 160
    return-void
.end method
