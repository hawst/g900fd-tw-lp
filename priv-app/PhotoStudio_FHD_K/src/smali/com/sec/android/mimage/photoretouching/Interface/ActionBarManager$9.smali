.class Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;
.super Ljava/lang/Object;
.source "ActionBarManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ActionBarSetEnabled(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private final synthetic val$flag:Z

.field private final synthetic val$icon:Landroid/widget/ImageView;

.field private final synthetic val$l:Landroid/widget/LinearLayout;

.field private final synthetic val$text:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/LinearLayout;Z)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;->val$icon:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;->val$text:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;->val$l:Landroid/widget/LinearLayout;

    iput-boolean p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;->val$flag:Z

    .line 1637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1642
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;->val$icon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1644
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;->val$icon:Landroid/widget/ImageView;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 1646
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;->val$text:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1648
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;->val$text:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1651
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;->val$l:Landroid/widget/LinearLayout;

    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;->val$flag:Z

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1652
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager$9;->val$l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1653
    return-void
.end method
