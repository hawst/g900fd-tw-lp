.class Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;
.super Landroid/view/animation/AnimationSet;
.source "AssistantLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LayoutAnimationSet"
.end annotation


# instance fields
.field private mIsRunning:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 1120
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    .line 1121
    invoke-direct {p0, p2, p3}, Landroid/view/animation/AnimationSet;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1168
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->mIsRunning:Z

    .line 1122
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->init()V

    .line 1123
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;Z)V
    .locals 1
    .param p2, "shareInterpolator"    # Z

    .prologue
    .line 1115
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    .line 1116
    invoke-direct {p0, p2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1168
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->mIsRunning:Z

    .line 1117
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->init()V

    .line 1118
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;Z)V
    .locals 0

    .prologue
    .line 1168
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->mIsRunning:Z

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;)Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
    .locals 1

    .prologue
    .line 1113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    return-object v0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 1130
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1166
    return-void
.end method


# virtual methods
.method public getIsRunning()Z
    .locals 1

    .prologue
    .line 1126
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->mIsRunning:Z

    return v0
.end method
