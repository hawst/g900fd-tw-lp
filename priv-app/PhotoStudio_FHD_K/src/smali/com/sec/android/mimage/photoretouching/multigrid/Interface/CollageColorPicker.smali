.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;
.super Landroid/widget/LinearLayout;
.source "CollageColorPicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;
    }
.end annotation


# static fields
.field public static COLLAGE_BACKGROUND:I

.field public static DECO_BACKGROUND:I


# instance fields
.field private final COLOR_01:I

.field private final COLOR_02:I

.field private final COLOR_03:I

.field private final COLOR_04:I

.field private final COLOR_05:I

.field private final COLOR_06:I

.field private final COLOR_07:I

.field private final COLOR_08:I

.field private final COLOR_09:I

.field private final COLOR_10:I

.field private final COLOR_11:I

.field private final COLOR_12:I

.field private final COLOR_13:I

.field private final COLOR_14:I

.field private final COLOR_BUTTON_NUM:I

.field private focusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private mColorButtonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mColorChipPicker:Landroid/widget/LinearLayout;

.field private mColorPalette:Landroid/widget/RelativeLayout;

.field private mColorPaletteBitmap:Landroid/graphics/Bitmap;

.field private mColorPalettePicker:Landroid/widget/LinearLayout;

.field private mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;

.field private mColorPickerLayout:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mCurrentColor:I

.field private mCurrentColorBtn:Landroid/widget/LinearLayout;

.field private mIsForCollage:Z

.field private mMainLayout:Landroid/widget/LinearLayout;

.field private mOnClickColorButton:Landroid/view/View$OnClickListener;

.field private mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

.field private mPaletteHeight:I

.field private mPaletteWidth:I

.field private mTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576
    const v0, 0x7f020121

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLLAGE_BACKGROUND:I

    .line 577
    const v0, 0x7f0201ac

    sput v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->DECO_BACKGROUND:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 25
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 486
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$1;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->focusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 559
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_BUTTON_NUM:I

    .line 561
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_01:I

    .line 562
    const v0, -0x200d3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_02:I

    .line 563
    const/16 v0, -0x7ca3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_03:I

    .line 564
    const v0, -0xc4a5

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_04:I

    .line 565
    const v0, -0xb637

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_05:I

    .line 566
    const v0, -0x357a01

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_06:I

    .line 567
    const v0, -0x85c822

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_07:I

    .line 568
    const v0, -0xfe6bd2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_08:I

    .line 569
    const v0, -0xc75701

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_09:I

    .line 570
    const v0, -0xcc9803

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_10:I

    .line 571
    const v0, -0x595a5b

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_11:I

    .line 572
    const v0, -0xc9c9ca

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_12:I

    .line 573
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_13:I

    .line 574
    const/16 v0, -0x199e

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_14:I

    .line 579
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    .line 580
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    .line 581
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    .line 582
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalette:Landroid/widget/RelativeLayout;

    .line 583
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    .line 584
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorChipPicker:Landroid/widget/LinearLayout;

    .line 585
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    .line 586
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColorBtn:Landroid/widget/LinearLayout;

    .line 588
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    .line 589
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    .line 590
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    .line 592
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;

    .line 594
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I

    .line 595
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteWidth:I

    .line 596
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteHeight:I

    .line 598
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mIsForCollage:Z

    .line 599
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mTime:J

    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->init(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 486
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$1;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->focusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 559
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_BUTTON_NUM:I

    .line 561
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_01:I

    .line 562
    const v0, -0x200d3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_02:I

    .line 563
    const/16 v0, -0x7ca3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_03:I

    .line 564
    const v0, -0xc4a5

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_04:I

    .line 565
    const v0, -0xb637

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_05:I

    .line 566
    const v0, -0x357a01

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_06:I

    .line 567
    const v0, -0x85c822

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_07:I

    .line 568
    const v0, -0xfe6bd2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_08:I

    .line 569
    const v0, -0xc75701

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_09:I

    .line 570
    const v0, -0xcc9803

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_10:I

    .line 571
    const v0, -0x595a5b

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_11:I

    .line 572
    const v0, -0xc9c9ca

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_12:I

    .line 573
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_13:I

    .line 574
    const/16 v0, -0x199e

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_14:I

    .line 579
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    .line 580
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    .line 581
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    .line 582
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalette:Landroid/widget/RelativeLayout;

    .line 583
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    .line 584
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorChipPicker:Landroid/widget/LinearLayout;

    .line 585
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    .line 586
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColorBtn:Landroid/widget/LinearLayout;

    .line 588
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    .line 589
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    .line 590
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    .line 592
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;

    .line 594
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I

    .line 595
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteWidth:I

    .line 596
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteHeight:I

    .line 598
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mIsForCollage:Z

    .line 599
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mTime:J

    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->init(Landroid/content/Context;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 486
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$1;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->focusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 559
    const/16 v0, 0xe

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_BUTTON_NUM:I

    .line 561
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_01:I

    .line 562
    const v0, -0x200d3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_02:I

    .line 563
    const/16 v0, -0x7ca3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_03:I

    .line 564
    const v0, -0xc4a5

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_04:I

    .line 565
    const v0, -0xb637

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_05:I

    .line 566
    const v0, -0x357a01

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_06:I

    .line 567
    const v0, -0x85c822

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_07:I

    .line 568
    const v0, -0xfe6bd2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_08:I

    .line 569
    const v0, -0xc75701

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_09:I

    .line 570
    const v0, -0xcc9803

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_10:I

    .line 571
    const v0, -0x595a5b

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_11:I

    .line 572
    const v0, -0xc9c9ca

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_12:I

    .line 573
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_13:I

    .line 574
    const/16 v0, -0x199e

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLOR_14:I

    .line 579
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    .line 580
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    .line 581
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    .line 582
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalette:Landroid/widget/RelativeLayout;

    .line 583
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    .line 584
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorChipPicker:Landroid/widget/LinearLayout;

    .line 585
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    .line 586
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColorBtn:Landroid/widget/LinearLayout;

    .line 588
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    .line 589
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    .line 590
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    .line 592
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;

    .line 594
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I

    .line 595
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteWidth:I

    .line 596
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteHeight:I

    .line 598
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mIsForCollage:Z

    .line 599
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mTime:J

    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->init(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;I)V
    .locals 0

    .prologue
    .line 594
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColorBtn:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Landroid/view/MotionEvent;Z)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setPickerPos(Landroid/view/MotionEvent;Z)V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)I
    .locals 1

    .prologue
    .line 594
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 361
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setSelected(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)Z
    .locals 1

    .prologue
    .line 598
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mIsForCollage:Z

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;Landroid/view/MotionEvent;)I
    .locals 1

    .prologue
    .line 275
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->getColor(Landroid/view/MotionEvent;)I

    move-result v0

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)J
    .locals 2

    .prologue
    .line 599
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mTime:J

    return-wide v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;J)V
    .locals 1

    .prologue
    .line 599
    iput-wide p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mTime:J

    return-void
.end method

.method private getColor(Landroid/view/MotionEvent;)I
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 277
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int v0, v2, v3

    .line 278
    .local v0, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int v1, v2, v3

    .line 280
    .local v1, "y":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteWidth:I

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 281
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteHeight:I

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 283
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    return v2
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : init : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 78
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    .line 79
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030036

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    .line 80
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f090089

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalette:Landroid/widget/RelativeLayout;

    .line 81
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f090088

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorChipPicker:Landroid/widget/LinearLayout;

    .line 84
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f09008a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    .line 85
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 88
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->initListener()V

    .line 89
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->initColorButtons()V

    .line 91
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalette:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalette:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 95
    :cond_0
    return-void
.end method

.method private initColorButtons()V
    .locals 4

    .prologue
    .line 323
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 325
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    .line 328
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    .line 339
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f090087

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColorBtn:Landroid/widget/LinearLayout;

    .line 340
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setCurrentColor(I)V

    .line 341
    return-void

    .line 330
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f09007a

    add-int/2addr v3, v1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 331
    .local v0, "button":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_2

    .line 333
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 334
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->focusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 335
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private initListener()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$2;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 134
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$3;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201a9

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    .line 202
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteWidth:I

    .line 203
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteHeight:I

    .line 206
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    if-nez v0, :cond_3

    .line 208
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$4;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    .line 273
    :cond_3
    return-void
.end method

.method private setPickerPos(Landroid/view/MotionEvent;Z)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "isShow"    # Z

    .prologue
    const/4 v4, 0x0

    .line 49
    if-eqz p2, :cond_4

    .line 51
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 52
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 53
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteWidth:I

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 54
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteWidth:I

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 60
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteHeight:I

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 61
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mPaletteHeight:I

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 66
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    .end local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_2
    return-void

    .line 55
    .restart local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 56
    iput v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    goto :goto_0

    .line 58
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    goto :goto_0

    .line 62
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    .line 63
    iput v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto :goto_1

    .line 65
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto :goto_1

    .line 70
    .end local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPalettePicker:Landroid/widget/LinearLayout;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method private setSelected(Landroid/view/View;)V
    .locals 4
    .param p1, "selectedButton"    # Landroid/view/View;

    .prologue
    .line 363
    if-eqz p1, :cond_0

    .line 365
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorChipPicker:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 366
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050019

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v1, v2

    .line 367
    .local v1, "pickerWidth":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorChipPicker:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 368
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    div-int/lit8 v3, v1, 0x2

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 369
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    div-int/lit8 v3, v1, 0x2

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 370
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorChipPicker:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 376
    .end local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "pickerWidth":I
    :goto_0
    return-void

    .line 374
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorChipPicker:Landroid/widget/LinearLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public configurationChanged()V
    .locals 4

    .prologue
    const v3, 0x7f0900b1

    .line 288
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : configurationChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 289
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 291
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 292
    .local v0, "viewGroup":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 294
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 298
    .end local v0    # "viewGroup":Landroid/view/ViewGroup;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v1, :cond_1

    .line 299
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-virtual {v1, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    .line 300
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v1, :cond_2

    .line 301
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v1, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    .line 307
    :cond_2
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 312
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 315
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorButtonList:Ljava/util/ArrayList;

    .line 316
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnTouchColorPalette:Landroid/view/View$OnTouchListener;

    .line 317
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mOnClickColorButton:Landroid/view/View$OnClickListener;

    .line 318
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPaletteBitmap:Landroid/graphics/Bitmap;

    .line 319
    return-void
.end method

.method public getCurrentColor()I
    .locals 1

    .prologue
    .line 380
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 481
    const/4 v0, 0x1

    .line 483
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 114
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cheus, "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onLayout : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method public setBackground(I)V
    .locals 1
    .param p1, "backId"    # I

    .prologue
    .line 99
    sget v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->COLLAGE_BACKGROUND:I

    if-ne p1, v0, :cond_0

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mIsForCollage:Z

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 104
    return-void
.end method

.method public setColorPickerCallback(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;)V
    .locals 0
    .param p1, "colorPickerCallback"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker$ColorPickerCallback;

    .line 109
    return-void
.end method

.method public setCurrentColor(I)V
    .locals 2
    .param p1, "currentColor"    # I

    .prologue
    .line 385
    if-nez p1, :cond_0

    .line 387
    const/4 p1, -0x1

    .line 391
    :cond_0
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I

    .line 392
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColorBtn:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 393
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColorBtn:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mCurrentColor:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 394
    return-void
.end method

.method public setPosition(IIZ)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "isCollage"    # Z

    .prologue
    const/4 v1, -0x2

    .line 398
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 399
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : setPosition : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 400
    if-eqz p3, :cond_0

    .line 402
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 403
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sub-int v1, p2, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 411
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 412
    const/4 v0, 0x0

    .line 413
    return-void

    .line 407
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050263

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sub-int v1, p1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 408
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050264

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v1, p2

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    goto :goto_0
.end method

.method public show(ZIIZ)V
    .locals 0
    .param p1, "isShow"    # Z
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "isCollage"    # Z

    .prologue
    .line 473
    invoke-virtual {p0, p1, p4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->show(ZZ)Z

    .line 474
    invoke-virtual {p0, p2, p3, p4}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->setPosition(IIZ)V

    .line 475
    return-void
.end method

.method public show(ZZ)Z
    .locals 9
    .param p1, "show"    # Z
    .param p2, "isCollage"    # Z

    .prologue
    const v8, 0x7f0900b1

    const/16 v6, 0x8

    const/4 v7, 0x0

    .line 418
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 419
    .local v4, "viewGroup":Landroid/view/ViewGroup;
    const/4 v0, 0x0

    .line 421
    .local v0, "isHided":Z
    if-eqz v4, :cond_0

    .line 423
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 426
    :cond_0
    if-eqz p2, :cond_3

    .line 427
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-virtual {v5, v8}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    .line 431
    :goto_0
    if-eqz p1, :cond_4

    .line 433
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    if-nez v5, :cond_2

    .line 435
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 436
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 437
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 440
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    instance-of v5, v5, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-nez v5, :cond_2

    .line 444
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    const v6, 0x7f090079

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 445
    .local v1, "layout":Landroid/widget/RelativeLayout;
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 446
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f05029a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v3, v5

    .line 447
    .local v3, "topMargin":I
    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 449
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 468
    .end local v1    # "layout":Landroid/widget/RelativeLayout;
    .end local v2    # "params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "topMargin":I
    :cond_2
    :goto_1
    return v0

    .line 429
    :cond_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v5, v8}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    goto :goto_0

    .line 460
    :cond_4
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v5

    if-nez v5, :cond_2

    .line 462
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mMainLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 463
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;->mColorPickerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 464
    const/4 v0, 0x1

    goto :goto_1
.end method
