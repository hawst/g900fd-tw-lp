.class public Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;
.super Ljava/lang/Object;
.source "MultiGridReloadManager.java"


# instance fields
.field private THUMB_DRAW_TOTAL_NUL:I

.field private THUMB_VIEW_TOTAL_NUM:I

.field private mCollage:Lsiso/AutoCollage/Collage;

.field private mContext:Landroid/content/Context;

.field private mDecodeInfoItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDecodeInfoItemsTemp:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDetailedInfo:Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;

.field private mIsCollageEditMode:Z

.field private mIsEditMode:Z

.field private mMultiGridCollage:Lcom/sec/android/collage/MultiGridCollage;

.field private mReloadGridCount:I

.field private mReloadGridSplit:I

.field private mReloadGridStyle:I

.field private mReloadedPathList:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->THUMB_VIEW_TOTAL_NUM:I

    .line 196
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->THUMB_DRAW_TOTAL_NUL:I

    .line 197
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridCount:I

    .line 198
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridSplit:I

    .line 199
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridStyle:I

    .line 201
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mIsCollageEditMode:Z

    .line 202
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mIsEditMode:Z

    .line 204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDecodeInfoItems:Ljava/util/ArrayList;

    .line 205
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDecodeInfoItemsTemp:Ljava/util/ArrayList;

    .line 207
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDetailedInfo:Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;

    .line 208
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mContext:Landroid/content/Context;

    .line 214
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadedPathList:[Ljava/lang/String;

    .line 20
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mContext:Landroid/content/Context;

    .line 21
    return-void
.end method


# virtual methods
.method public LoadMultiGridInfo(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x1

    .line 64
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mIsCollageEditMode:Z

    if-eqz v5, :cond_3

    .line 66
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mCollage:Lsiso/AutoCollage/Collage;

    invoke-virtual {v5}, Lsiso/AutoCollage/Collage;->getAllImageROI()Lsiso/AutoCollage/ImageROIInfo;

    move-result-object v5

    invoke-virtual {v5}, Lsiso/AutoCollage/ImageROIInfo;->getCount()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridCount:I

    .line 67
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridCount:I

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->getReloadGridSplit(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridSplit:I

    .line 68
    const/high16 v5, 0x1e200000

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridStyle:I

    .line 69
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridCount:I

    if-lt v2, v5, :cond_1

    .line 112
    :cond_0
    return-void

    .line 71
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mCollage:Lsiso/AutoCollage/Collage;

    invoke-virtual {v5}, Lsiso/AutoCollage/Collage;->getAllImageROI()Lsiso/AutoCollage/ImageROIInfo;

    move-result-object v5

    invoke-virtual {v5, v2}, Lsiso/AutoCollage/ImageROIInfo;->getImageFilePath(I)Ljava/lang/String;

    move-result-object v3

    .line 72
    .local v3, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 73
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 75
    new-instance v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;-><init>()V

    .line 76
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mContext:Landroid/content/Context;

    invoke-static {v5, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getImageContentUri(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    .line 77
    .local v4, "uri":Landroid/net/Uri;
    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 78
    iput-boolean v7, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 79
    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 81
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDecodeInfoItemsTemp:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadedPathList:[Ljava/lang/String;

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    aput-object v6, v5, v2

    .line 69
    .end local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 89
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "i":I
    .end local v3    # "path":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDetailedInfo:Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->getGridCount()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridCount:I

    .line 90
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridCount:I

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->getReloadGridSplit(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridSplit:I

    .line 91
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDetailedInfo:Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->getStyle()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridStyle:I

    .line 93
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridCount:I

    if-ge v2, v5, :cond_0

    .line 95
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mMultiGridCollage:Lcom/sec/android/collage/MultiGridCollage;

    invoke-virtual {v5, v2}, Lcom/sec/android/collage/MultiGridCollage;->getROIInfo(I)Lcom/sec/android/collage/MultiGridROINode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/collage/MultiGridROINode;->getFilePath()Ljava/lang/String;

    move-result-object v3

    .line 96
    .restart local v3    # "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 97
    .restart local v1    # "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 99
    new-instance v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;-><init>()V

    .line 101
    .restart local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mContext:Landroid/content/Context;

    invoke-static {v5, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getImageContentUri(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    .line 102
    .restart local v4    # "uri":Landroid/net/Uri;
    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    .line 103
    iput-boolean v7, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    .line 104
    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    .line 106
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDecodeInfoItemsTemp:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadedPathList:[Ljava/lang/String;

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    aput-object v6, v5, v2

    .line 93
    .end local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public destroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 177
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mMultiGridCollage:Lcom/sec/android/collage/MultiGridCollage;

    if-eqz v1, :cond_0

    .line 178
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mMultiGridCollage:Lcom/sec/android/collage/MultiGridCollage;

    .line 180
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mCollage:Lsiso/AutoCollage/Collage;

    if-eqz v1, :cond_1

    .line 181
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mCollage:Lsiso/AutoCollage/Collage;

    .line 183
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDetailedInfo:Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;

    if-eqz v1, :cond_2

    .line 184
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDetailedInfo:Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;

    .line 186
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDecodeInfoItemsTemp:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    .line 187
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDecodeInfoItemsTemp:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_4

    .line 191
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDecodeInfoItemsTemp:Ljava/util/ArrayList;

    .line 193
    :cond_3
    return-void

    .line 187
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 188
    .local v0, "item":Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    .line 189
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->thumbnail:Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->thumbnail:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getCollage()Lsiso/AutoCollage/Collage;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mCollage:Lsiso/AutoCollage/Collage;

    return-object v0
.end method

.method public getDecodeInfoList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDecodeInfoItemsTemp:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDetailedInfo()Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDetailedInfo:Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;

    return-object v0
.end method

.method public getIsEditMode()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mIsEditMode:Z

    return v0
.end method

.method public getReloadGridCount()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridCount:I

    return v0
.end method

.method public getReloadGridSplit()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridSplit:I

    return v0
.end method

.method public getReloadGridSplit(I)I
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 59
    const/high16 v0, 0x2c000000

    return v0
.end method

.method public getReloadGridStyle()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadGridStyle:I

    return v0
.end method

.method public getReloadedPathList(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadedPathList:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getReloadedPathList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mReloadedPathList:[Ljava/lang/String;

    return-object v0
.end method

.method public loadCollageData(Ljava/lang/String;)[I
    .locals 5
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 25
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->THUMB_DRAW_TOTAL_NUL:I

    new-array v2, v3, [I

    .line 26
    .local v2, "orderList":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-lt v0, v3, :cond_1

    .line 30
    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mIsCollageEditMode:Z

    if-eqz v3, :cond_2

    .line 32
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mCollage:Lsiso/AutoCollage/Collage;

    if-eqz v3, :cond_0

    .line 34
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mCollage:Lsiso/AutoCollage/Collage;

    .line 36
    :cond_0
    new-instance v3, Lsiso/AutoCollage/Collage;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lsiso/AutoCollage/Collage;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mCollage:Lsiso/AutoCollage/Collage;

    .line 37
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mCollage:Lsiso/AutoCollage/Collage;

    invoke-virtual {v3, p1}, Lsiso/AutoCollage/Collage;->loadCollage(Ljava/lang/String;)Z

    .line 54
    :goto_1
    return-object v2

    .line 27
    :cond_1
    aput v0, v2, v0

    .line 26
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mMultiGridCollage:Lcom/sec/android/collage/MultiGridCollage;

    if-eqz v3, :cond_3

    .line 42
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mMultiGridCollage:Lcom/sec/android/collage/MultiGridCollage;

    .line 44
    :cond_3
    new-instance v3, Lcom/sec/android/collage/MultiGridCollage;

    invoke-direct {v3}, Lcom/sec/android/collage/MultiGridCollage;-><init>()V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mMultiGridCollage:Lcom/sec/android/collage/MultiGridCollage;

    .line 45
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mMultiGridCollage:Lcom/sec/android/collage/MultiGridCollage;

    invoke-virtual {v3, p1}, Lcom/sec/android/collage/MultiGridCollage;->load(Ljava/lang/String;)Z

    .line 47
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mMultiGridCollage:Lcom/sec/android/collage/MultiGridCollage;

    invoke-virtual {v3}, Lcom/sec/android/collage/MultiGridCollage;->getROICount()I

    move-result v3

    if-lt v0, v3, :cond_4

    .line 52
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mDetailedInfo:Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mMultiGridCollage:Lcom/sec/android/collage/MultiGridCollage;

    invoke-virtual {v4}, Lcom/sec/android/collage/MultiGridCollage;->getDetailedInfo()Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultigridDetailedInfo;->setByteBuffer(Ljava/nio/ByteBuffer;)V

    goto :goto_1

    .line 48
    :cond_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mMultiGridCollage:Lcom/sec/android/collage/MultiGridCollage;

    invoke-virtual {v3, v0}, Lcom/sec/android/collage/MultiGridCollage;->getROIInfo(I)Lcom/sec/android/collage/MultiGridROINode;

    move-result-object v1

    .line 49
    .local v1, "imageNodeROI":Lcom/sec/android/collage/MultiGridROINode;
    invoke-virtual {v1}, Lcom/sec/android/collage/MultiGridROINode;->getLevel()I

    move-result v3

    aput v3, v2, v0

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public setCollageEditMode(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mIsCollageEditMode:Z

    .line 142
    return-void
.end method

.method public setIsEditMode(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 137
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mIsEditMode:Z

    .line 138
    return-void
.end method

.method public setIsEditMode(Landroid/content/Intent;)Z
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 116
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mIsEditMode:Z

    .line 117
    const/4 v0, 0x0

    .line 118
    .local v0, "ret":Z
    const-string v2, "EDIT_URI"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 120
    const-string v2, "COLLAGE_TYPE"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 121
    .local v1, "tmpStr":Ljava/lang/String;
    const-string v2, "StyleA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 122
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mIsCollageEditMode:Z

    .line 125
    :goto_0
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mIsEditMode:Z

    .line 126
    const/4 v0, 0x1

    .line 128
    .end local v1    # "tmpStr":Ljava/lang/String;
    :cond_0
    return v0

    .line 124
    .restart local v1    # "tmpStr":Ljava/lang/String;
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridReloadManager;->mIsCollageEditMode:Z

    goto :goto_0
.end method
