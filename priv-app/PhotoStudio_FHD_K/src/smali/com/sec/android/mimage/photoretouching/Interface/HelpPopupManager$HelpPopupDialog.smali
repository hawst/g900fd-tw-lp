.class Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;
.super Landroid/app/Dialog;
.source "HelpPopupManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "HelpPopupDialog"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;Landroid/content/Context;)V
    .locals 6
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, -0x1

    .line 343
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 344
    invoke-direct {p0, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 346
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;->requestWindowFeature(I)Z

    .line 347
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x400

    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    .line 348
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v3, -0x1000000

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 349
    .local v1, "color":Landroid/graphics/drawable/ColorDrawable;
    const/16 v3, 0x8c

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/ColorDrawable;->setAlpha(I)V

    .line 350
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 351
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;->setCancelable(Z)V

    .line 353
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->getCurrentModeHelpPopup()Landroid/view/View;

    move-result-object v2

    .line 354
    .local v2, "currentModePopupLayout":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 356
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v5, v5}, Landroid/view/Window;->setLayout(II)V

    .line 360
    const v3, 0x7f09012b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 362
    .local v0, "closeBtn":Landroid/widget/LinearLayout;
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 363
    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->setHelpPopupCloseBtnListener(Landroid/widget/LinearLayout;)V
    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;Landroid/widget/LinearLayout;)V

    .line 370
    .end local v0    # "closeBtn":Landroid/widget/LinearLayout;
    :goto_0
    return-void

    .line 368
    :cond_0
    const-string v3, "ysjeong, currentModePopupLayout == null, HelpPopupDialog, HelpPopupManager"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupDialog;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->touchCancelButton()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V

    .line 375
    invoke-super {p0}, Landroid/app/Dialog;->onBackPressed()V

    .line 376
    return-void
.end method
