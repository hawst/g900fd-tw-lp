.class public Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;
.super Landroid/view/SurfaceView;
.source "MirrorAnimationView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;,
        Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;
    }
.end annotation


# static fields
.field public static final MIRROR_ANIMATION_VIEW_ID:I = 0x40000002


# instance fields
.field private mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

.field private mClearPaint:Landroid/graphics/Paint;

.field private mFinishDraw:Z

.field private mHolder:Landroid/view/SurfaceHolder;

.field private mIsLoop:Z

.field private mMirrorAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;

.field private mThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 177
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mFinishDraw:Z

    .line 253
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mIsLoop:Z

    .line 254
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mThread:Ljava/lang/Thread;

    .line 255
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    .line 256
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    .line 257
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mMirrorAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;

    .line 258
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mClearPaint:Landroid/graphics/Paint;

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    .line 32
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 33
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->setZOrderOnTop(Z)V

    .line 34
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    const/4 v1, -0x2

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 35
    const v0, 0x40000002    # 2.0000005f

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->setId(I)V

    .line 36
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;)Z
    .locals 1

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mIsLoop:Z

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;)Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->drawCanvas(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private createThread()V
    .locals 2

    .prologue
    .line 179
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mThread:Ljava/lang/Thread;

    .line 199
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 200
    return-void
.end method

.method private drawCanvas(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 124
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    if-nez v11, :cond_1

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 128
    .local v4, "currentTime":J
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mStartTime:J
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)J

    move-result-wide v12

    sub-long v6, v4, v12

    .line 131
    .local v6, "diff":J
    long-to-float v11, v6

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mDuration:J
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)J

    move-result-wide v12

    long-to-float v12, v12

    div-float v8, v11, v12

    .line 132
    .local v8, "input":F
    const/high16 v11, 0x3f800000    # 1.0f

    cmpl-float v11, v8, v11

    if-lez v11, :cond_2

    .line 133
    const/high16 v8, 0x3f800000    # 1.0f

    .line 134
    :cond_2
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mInterpolator:Landroid/view/animation/Interpolator;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)Landroid/view/animation/Interpolator;

    move-result-object v11

    invoke-interface {v11, v8}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v8

    .line 135
    if-eqz p1, :cond_3

    .line 136
    const/4 v11, 0x0

    sget-object v12, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v11, v12}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 141
    :cond_3
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mDrawRoiForPreview:Landroid/graphics/RectF;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)Landroid/graphics/RectF;

    move-result-object v3

    .line 142
    .local v3, "drawRoi":Landroid/graphics/RectF;
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mMirrorRoi:Landroid/graphics/RectF;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->access$5(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)Landroid/graphics/RectF;

    move-result-object v10

    .line 143
    .local v10, "mirrorRoi":Landroid/graphics/RectF;
    const/4 v1, 0x0

    .line 144
    .local v1, "centerX":F
    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float v2, v11, v12

    .line 145
    .local v2, "centerY":F
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 146
    .local v9, "matrix":Landroid/graphics/Matrix;
    new-instance v0, Landroid/graphics/Camera;

    invoke-direct {v0}, Landroid/graphics/Camera;-><init>()V

    .line 147
    .local v0, "camera":Landroid/graphics/Camera;
    invoke-virtual {v0}, Landroid/graphics/Camera;->save()V

    .line 148
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mAngle:F
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->access$6(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)F

    move-result v11

    mul-float/2addr v11, v8

    invoke-virtual {v0, v11}, Landroid/graphics/Camera;->rotateY(F)V

    .line 149
    invoke-virtual {v0, v9}, Landroid/graphics/Camera;->getMatrix(Landroid/graphics/Matrix;)V

    .line 150
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mAngle:F
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->access$6(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)F

    move-result v11

    const/4 v12, 0x0

    cmpl-float v11, v11, v12

    if-lez v11, :cond_4

    .line 151
    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    move-result v1

    .line 152
    :cond_4
    neg-float v11, v1

    neg-float v12, v2

    invoke-virtual {v9, v11, v12}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 153
    invoke-virtual {v9, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 154
    iget v11, v3, Landroid/graphics/RectF;->left:F

    iget v12, v10, Landroid/graphics/RectF;->left:F

    add-float/2addr v11, v12

    iget v12, v3, Landroid/graphics/RectF;->top:F

    iget v13, v10, Landroid/graphics/RectF;->top:F

    add-float/2addr v12, v13

    invoke-virtual {v9, v11, v12}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 155
    invoke-virtual {v0}, Landroid/graphics/Camera;->restore()V

    .line 157
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->access$7(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)Landroid/graphics/Bitmap;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {p1, v11, v9, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 163
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mDuration:J
    invoke-static {v11}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)J

    move-result-wide v12

    cmp-long v11, v6, v12

    if-lez v11, :cond_0

    .line 165
    iget-boolean v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mFinishDraw:Z

    if-eqz v11, :cond_5

    .line 167
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->finishAnimation()V

    .line 168
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mFinishDraw:Z

    goto/16 :goto_0

    .line 172
    :cond_5
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mFinishDraw:Z

    .line 173
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mMirrorAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;

    invoke-interface {v11}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;->endAnimation()V

    goto/16 :goto_0
.end method

.method private finishAnimation()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mIsLoop:Z

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mThread:Ljava/lang/Thread;

    .line 110
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->free()V

    .line 113
    :cond_0
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->post(Ljava/lang/Runnable;)Z

    .line 120
    return-void
.end method


# virtual methods
.method public clearCanvas()V
    .locals 3

    .prologue
    .line 66
    const/4 v0, 0x0

    .line 68
    .local v0, "canvas":Landroid/graphics/Canvas;
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 70
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 71
    if-eqz v0, :cond_0

    .line 73
    const/high16 v1, -0x1000000

    :try_start_1
    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 70
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    if-eqz v0, :cond_1

    .line 78
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 81
    :cond_1
    return-void

    .line 70
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 76
    :catchall_1
    move-exception v1

    .line 77
    if-eqz v0, :cond_2

    .line 78
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 80
    :cond_2
    throw v1
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->finishAnimation()V

    .line 41
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mMirrorAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mMirrorAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;->endAnimation()V

    .line 43
    :cond_0
    return-void
.end method

.method public runAnimation(ILandroid/graphics/Bitmap;ILandroid/graphics/RectF;Landroid/graphics/RectF;Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;)V
    .locals 4
    .param p1, "duration"    # I
    .param p2, "mirrorBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "mirrorSide"    # I
    .param p4, "mirrorRoi"    # Landroid/graphics/RectF;
    .param p5, "drawRoiForPreviewImage"    # Landroid/graphics/RectF;
    .param p6, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;

    .prologue
    const/4 v3, 0x0

    .line 88
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mClearPaint:Landroid/graphics/Paint;

    .line 89
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mClearPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 90
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mMirrorAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;

    .line 91
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    .line 92
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    invoke-virtual {v0, p3}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->setMirrorSide(I)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->setDuration(I)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    invoke-virtual {v0, p2}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    invoke-virtual {v0, p5}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->setDrawRoi(Landroid/graphics/RectF;)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    invoke-virtual {v0, p4}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->setMirrorRoi(Landroid/graphics/RectF;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->setStartTime()V

    .line 98
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->setInterpolator()V

    .line 99
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mMirrorAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;->startAnimation()V

    .line 100
    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->setZOrderOnTop(Z)V

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mIsLoop:Z

    .line 102
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mFinishDraw:Z

    .line 103
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->createThread()V

    .line 104
    return-void
.end method

.method public runningAnimation()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mIsLoop:Z

    return v0
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 47
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 52
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 57
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V

    .line 59
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mIsLoop:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    goto :goto_0
.end method
