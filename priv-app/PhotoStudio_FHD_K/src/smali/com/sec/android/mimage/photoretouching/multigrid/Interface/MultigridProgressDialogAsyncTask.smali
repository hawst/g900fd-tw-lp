.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;
.super Landroid/os/AsyncTask;
.source "MultigridProgressDialogAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDoAsyncTaskCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;

.field private mProgressDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callback"    # Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 79
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mContext:Landroid/content/Context;

    .line 80
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 81
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mDoAsyncTaskCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;

    .line 20
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mContext:Landroid/content/Context;

    .line 21
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mDoAsyncTaskCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;

    .line 22
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mDoAsyncTaskCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mDoAsyncTaskCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;->doInBackground()V

    .line 38
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 8
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f0601df

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 43
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 44
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v2, :cond_0

    .line 45
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 46
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 53
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_1

    .line 55
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v2, :cond_1

    .line 57
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->getPrivateSaveFolder()Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "saveFolder":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, MPDAT - onPostExecute() - saveFolder : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 59
    if-nez v0, :cond_3

    .line 61
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const-string v4, "Studio"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 73
    .end local v0    # "saveFolder":Ljava/lang/String;
    .end local v1    # "text":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mDoAsyncTaskCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;

    if-eqz v2, :cond_2

    .line 75
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mDoAsyncTaskCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;->onPostExecute()V

    .line 77
    :cond_2
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mContext:Landroid/content/Context;

    .line 78
    return-void

    .line 66
    .restart local v0    # "saveFolder":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 67
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 25
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 26
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 27
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mContext:Landroid/content/Context;

    const v2, 0x7f06008f

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 28
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 29
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 30
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 31
    return-void
.end method
