.class Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;
.super Landroid/os/AsyncTask;
.source "WatermarkView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V
    .locals 0

    .prologue
    .line 947
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    .line 946
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 948
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 11
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 951
    const/4 v2, 0x0

    .line 952
    .local v2, "fileName":Ljava/lang/String;
    array-length v6, p1

    if-lez v6, :cond_0

    .line 953
    const/4 v6, 0x0

    aget-object v2, p1, v6

    .line 955
    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getAvailableExternalMemorySize()J

    move-result-wide v0

    .line 956
    .local v0, "available_memsize":J
    const-wide/32 v6, 0xa00000

    cmp-long v6, v0, v6

    if-gez v6, :cond_3

    .line 957
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/app/ProgressDialog;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 958
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/app/ProgressDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->dismiss()V

    .line 959
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "memory size = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " %"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 961
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f06007b

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 996
    :cond_2
    :goto_0
    return-object v10

    .line 964
    :cond_3
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mCurrentStickerCount:I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)I

    move-result v6

    if-lt v4, v6, :cond_5

    .line 970
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v7

    .line 971
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v8

    .line 972
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v9

    .line 970
    invoke-virtual {v6, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([III)V

    .line 975
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 977
    const/4 v3, 0x0

    .line 978
    .local v3, "filePath":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_6

    .line 980
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 981
    .local v5, "tempPath":Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 988
    .end local v5    # "tempPath":Ljava/lang/String;
    :goto_2
    if-nez v2, :cond_4

    .line 989
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    move-result-object v2

    .line 991
    :cond_4
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mCurrentSaveSize:I
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)I

    move-result v7

    invoke-virtual {v6, v3, v2, v7}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->saveCurrentImage(Ljava/lang/String;Ljava/lang/String;I)V

    .line 992
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 993
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->setCurrentSavedIndex()V

    goto/16 :goto_0

    .line 964
    .end local v3    # "filePath":Ljava/lang/String;
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 985
    .restart local v3    # "filePath":Ljava/lang/String;
    :cond_6
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const v4, 0x7f0601df

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1007
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1008
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1009
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1012
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1014
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isPersonalPage()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1016
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Studio"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1017
    .local v1, "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 1032
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1034
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 1035
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 1036
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    .line 1038
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 1039
    return-void

    .line 1021
    .end local v1    # "text":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPrivateSaveFolder()Ljava/lang/String;

    move-result-object v0

    .line 1022
    .local v0, "privateFolder":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1023
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 1028
    .end local v0    # "privateFolder":Ljava/lang/String;
    .end local v1    # "text":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Studio"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1029
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 999
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1000
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;Landroid/app/ProgressDialog;)V

    .line 1001
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06008f

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1002
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1003
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1004
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1005
    return-void
.end method
