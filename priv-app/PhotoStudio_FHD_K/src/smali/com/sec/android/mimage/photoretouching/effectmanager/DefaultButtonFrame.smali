.class public abstract Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;
.super Landroid/widget/LinearLayout;
.source "DefaultButtonFrame.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDefaultButtonListener:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;

.field private mIcon:Landroid/graphics/Bitmap;

.field private mIconView:Landroid/widget/ImageView;

.field private mId:I

.field private mLayout:Landroid/widget/LinearLayout;

.field private mLeftBar:Landroid/widget/LinearLayout;

.field private mTextId:I

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 110
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mContext:Landroid/content/Context;

    .line 111
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mDefaultButtonListener:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;

    .line 113
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mId:I

    .line 114
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIcon:Landroid/graphics/Bitmap;

    .line 115
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextId:I

    .line 117
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIconView:Landroid/widget/ImageView;

    .line 118
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextView:Landroid/widget/TextView;

    .line 120
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mLayout:Landroid/widget/LinearLayout;

    .line 121
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mLeftBar:Landroid/widget/LinearLayout;

    .line 33
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mContext:Landroid/content/Context;

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->init()V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 110
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mContext:Landroid/content/Context;

    .line 111
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mDefaultButtonListener:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;

    .line 113
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mId:I

    .line 114
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIcon:Landroid/graphics/Bitmap;

    .line 115
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextId:I

    .line 117
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIconView:Landroid/widget/ImageView;

    .line 118
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextView:Landroid/widget/TextView;

    .line 120
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mLayout:Landroid/widget/LinearLayout;

    .line 121
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mLeftBar:Landroid/widget/LinearLayout;

    .line 39
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mContext:Landroid/content/Context;

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->init()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 110
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mContext:Landroid/content/Context;

    .line 111
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mDefaultButtonListener:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;

    .line 113
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mId:I

    .line 114
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIcon:Landroid/graphics/Bitmap;

    .line 115
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextId:I

    .line 117
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIconView:Landroid/widget/ImageView;

    .line 118
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextView:Landroid/widget/TextView;

    .line 120
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mLayout:Landroid/widget/LinearLayout;

    .line 121
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mLeftBar:Landroid/widget/LinearLayout;

    .line 45
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mContext:Landroid/content/Context;

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->init()V

    .line 47
    return-void
.end method


# virtual methods
.method public configurationChanged()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 79
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mContext:Landroid/content/Context;

    .line 80
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIconView:Landroid/widget/ImageView;

    .line 81
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextView:Landroid/widget/TextView;

    .line 82
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mDefaultButtonListener:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;

    .line 83
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIcon:Landroid/graphics/Bitmap;

    .line 84
    return-void
.end method

.method public getButtonIcon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIcon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getButtonId()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mId:I

    return v0
.end method

.method public getTextId()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextId:I

    return v0
.end method

.method public init()V
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mContext:Landroid/content/Context;

    const v1, 0x7f030041

    invoke-static {v0, v1, p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mLayout:Landroid/widget/LinearLayout;

    .line 52
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mId:I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIcon:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextId:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->initButton(ILandroid/graphics/Bitmap;I)V

    .line 53
    return-void
.end method

.method public initButton(ILandroid/graphics/Bitmap;I)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "icon"    # Landroid/graphics/Bitmap;
    .param p3, "textId"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mId:I

    .line 65
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIcon:Landroid/graphics/Bitmap;

    .line 66
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextId:I

    .line 67
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0900ca

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIconView:Landroid/widget/ImageView;

    .line 68
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0900cb

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextView:Landroid/widget/TextView;

    .line 70
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIcon:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIconView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 72
    :cond_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextId:I

    if-lez v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 76
    :cond_1
    return-void
.end method

.method protected setCheckBoxVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    .line 56
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 57
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f09001d

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 58
    .local v0, "checkbox":Landroid/widget/CheckBox;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v1

    if-eq v1, p1, :cond_0

    .line 59
    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 61
    .end local v0    # "checkbox":Landroid/widget/CheckBox;
    :cond_0
    return-void
.end method

.method public setTouchCallback(Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;Ljava/util/ArrayList;)V
    .locals 2
    .param p1, "touchInterface"    # Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p2, "buttonList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;>;"
    new-instance v0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p2, p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mDefaultButtonListener:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->mDefaultButtonListener:Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 94
    return-void
.end method
