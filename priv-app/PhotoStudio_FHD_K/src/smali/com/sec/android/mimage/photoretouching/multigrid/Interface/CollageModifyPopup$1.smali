.class Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$1;
.super Ljava/lang/Object;
.source "CollageModifyPopup.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->initTouchListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$1;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 106
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 145
    :cond_0
    :goto_0
    return v3

    .line 109
    :pswitch_0
    invoke-virtual {p1, v3}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 114
    :pswitch_1
    invoke-static {p1, p2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isInButton(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    invoke-virtual {p1, v3}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 120
    :cond_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 125
    :pswitch_2
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 129
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p1, v2}, Landroid/view/View;->playSoundEffect(I)V

    .line 133
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$1;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->mModifyPopupCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$CollageModifyPopupCallback;->onTouch(I)V

    .line 137
    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup$1;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageModifyPopup;->show(ZZ)Z

    goto :goto_0

    .line 106
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
