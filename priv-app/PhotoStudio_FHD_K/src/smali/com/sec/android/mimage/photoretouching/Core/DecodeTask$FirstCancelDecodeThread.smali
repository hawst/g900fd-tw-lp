.class Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;
.super Ljava/lang/Thread;
.source "DecodeTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FirstCancelDecodeThread"
.end annotation


# instance fields
.field private exitThread:Z

.field optsResolution:Landroid/graphics/BitmapFactory$Options;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/BitmapFactory$Options;)V
    .locals 1
    .param p2, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 709
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    .line 708
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 703
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    .line 704
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->exitThread:Z

    .line 710
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    .line 711
    return-void
.end method


# virtual methods
.method public exit()V
    .locals 1

    .prologue
    .line 706
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->exitThread:Z

    .line 707
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    const/16 v5, 0x258

    const/4 v4, 0x0

    .line 713
    const/4 v1, 0x0

    .line 717
    .local v1, "sleepCoount":I
    :cond_0
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 722
    :goto_0
    add-int/lit8 v1, v1, 0x1

    .line 723
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState1:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$17(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-nez v2, :cond_1

    if-lt v1, v5, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$8(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->exitThread:Z

    if-eqz v2, :cond_0

    .line 725
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState1:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$17(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-nez v2, :cond_5

    if-ne v1, v5, :cond_5

    .line 727
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$18(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 728
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    invoke-virtual {v2}, Landroid/graphics/BitmapFactory$Options;->requestCancelDecode()V

    .line 729
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    .line 736
    :cond_4
    :goto_1
    return-void

    .line 718
    :catch_0
    move-exception v0

    .line 720
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 731
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$8(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 733
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$18(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 734
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    invoke-virtual {v2}, Landroid/graphics/BitmapFactory$Options;->requestCancelDecode()V

    goto :goto_1
.end method
