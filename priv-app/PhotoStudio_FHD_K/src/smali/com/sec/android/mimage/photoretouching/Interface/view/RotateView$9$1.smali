.class Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;
.super Ljava/lang/Object;
.source "RotateView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->TouchFunction(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

.field private final synthetic val$mirrorBufferHeight:I

.field private final synthetic val$mirrorBufferWidth:I

.field private final synthetic val$v:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;IILandroid/view/View;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->val$mirrorBufferWidth:I

    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->val$mirrorBufferHeight:I

    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->val$v:Landroid/view/View;

    .line 845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;
    .locals 1

    .prologue
    .line 845
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    return-object v0
.end method


# virtual methods
.method public ableDone()V
    .locals 1

    .prologue
    .line 907
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 908
    return-void
.end method

.method public ableDoneWithThread()V
    .locals 2

    .prologue
    .line 854
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 861
    return-void
.end method

.method public actionBarIsEnableDone()Z
    .locals 1

    .prologue
    .line 902
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledDone()Z

    move-result v0

    return v0
.end method

.method public changeMirrorButtonTOUndoButton()V
    .locals 3

    .prologue
    .line 971
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    const v1, 0x11001108

    const v2, 0x7f02009a

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeSelectionBtnIcon(II)V

    .line 972
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->val$v:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(Landroid/view/View;Z)V

    .line 973
    return-void
.end method

.method public getImageEditViewHeight()I
    .locals 1

    .prologue
    .line 917
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewHeight()I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I

    move-result v0

    return v0
.end method

.method public getImageEditViewWidth()I
    .locals 1

    .prologue
    .line 912
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->getImageEditViewWidth()I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)I

    move-result v0

    return v0
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 892
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->invalidateViews()V

    .line 893
    return-void
.end method

.method public invalidateThread()V
    .locals 1

    .prologue
    .line 897
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->invalidateViewsWithThread()V

    .line 898
    return-void
.end method

.method public setBottomButtonEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 887
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setEnabled(Z)V

    .line 888
    return-void
.end method

.method public setEnabledWithChildren(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 865
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setEnabledWithChildren(Z)V

    .line 867
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 883
    return-void

    .line 868
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 869
    .local v1, "v":Landroid/widget/LinearLayout;
    instance-of v2, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v2, :cond_1

    .line 870
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v2

    const v3, 0x11001108

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEnableDecoration()Z

    move-result v2

    if-nez v2, :cond_1

    .line 871
    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local v1    # "v":Landroid/widget/LinearLayout;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setEnabledWidthChildren(Z)V

    .line 872
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1$2;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 867
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setViewLayerType(I)V
    .locals 1
    .param p1, "viewLayoutType"    # I

    .prologue
    .line 922
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->setViewLayerType(I)V
    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;I)V

    .line 923
    return-void
.end method

.method public startMirrorRotateAnimation(IILandroid/graphics/RectF;[ILcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;)V
    .locals 25
    .param p1, "duration"    # I
    .param p2, "mirrorSide"    # I
    .param p3, "virtualMirrorRoi"    # Landroid/graphics/RectF;
    .param p4, "buffer"    # [I
    .param p5, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;

    .prologue
    .line 929
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->getDrawRoi()Landroid/graphics/RectF;

    move-result-object v23

    .line 930
    .local v23, "virtualDrawRoi":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v15

    .line 931
    .local v15, "m":Landroid/graphics/Matrix;
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    .line 932
    .local v13, "drawRoi":Landroid/graphics/RectF;
    move-object/from16 v0, v23

    invoke-virtual {v15, v13, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 933
    new-instance v22, Landroid/graphics/RectF;

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v7

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v7

    int-to-float v7, v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v8

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v8

    int-to-float v8, v8

    move-object/from16 v0, v22

    invoke-direct {v0, v4, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 934
    .local v22, "viewRoi":Landroid/graphics/RectF;
    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 936
    const/high16 v19, 0x3f800000    # 1.0f

    .line 937
    .local v19, "scale":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->val$mirrorBufferWidth:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->val$mirrorBufferHeight:I

    int-to-float v6, v6

    div-float v18, v4, v6

    .line 938
    .local v18, "previewRatio":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v6

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v6

    int-to-float v6, v6

    div-float v21, v4, v6

    .line 939
    .local v21, "viewRatio":F
    cmpl-float v4, v21, v18

    if-lez v4, :cond_0

    .line 941
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/RectF;->height()F

    move-result v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->val$mirrorBufferHeight:I

    int-to-float v6, v6

    div-float v19, v4, v6

    .line 947
    :goto_0
    new-instance v20, Landroid/graphics/Matrix;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Matrix;-><init>()V

    .line 948
    .local v20, "scaleMatrix":Landroid/graphics/Matrix;
    move-object/from16 v0, v20

    move/from16 v1, v19

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 950
    new-instance v16, Landroid/graphics/RectF;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/RectF;-><init>()V

    .line 951
    .local v16, "mirrorRoi":Landroid/graphics/RectF;
    move-object/from16 v0, v20

    move-object/from16 v1, v16

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 952
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v0, v4

    move/from16 v24, v0

    .line 953
    .local v24, "width":I
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->height()F

    move-result v4

    float-to-int v14, v4

    .line 954
    .local v14, "height":I
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v24

    invoke-static {v0, v14, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 955
    .local v17, "previewBitmap":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 956
    .local v3, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v3}, Landroid/graphics/Canvas;->save()I

    .line 958
    move-object/from16 v0, p3

    iget v4, v0, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->val$mirrorBufferWidth:I

    int-to-float v6, v6

    mul-float/2addr v4, v6

    move-object/from16 v0, p3

    iget v6, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v6

    float-to-int v5, v4

    .line 959
    .local v5, "offset":I
    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 960
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->val$mirrorBufferWidth:I

    .line 961
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v9, v4

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->height()F

    move-result v4

    float-to-int v10, v4

    .line 962
    const/4 v11, 0x1

    const/4 v12, 0x0

    move-object/from16 v4, p4

    .line 960
    invoke-virtual/range {v3 .. v12}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 965
    invoke-virtual {v3}, Landroid/graphics/Canvas;->restore()V

    .line 966
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v6

    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11, v13}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    move/from16 v7, p1

    move-object/from16 v8, v17

    move/from16 v9, p2

    move-object/from16 v10, v16

    move-object/from16 v12, p5

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->startMirrorAnimation(ILandroid/graphics/Bitmap;ILandroid/graphics/RectF;Landroid/graphics/RectF;Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;)V
    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;ILandroid/graphics/Bitmap;ILandroid/graphics/RectF;Landroid/graphics/RectF;Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;)V

    .line 967
    return-void

    .line 945
    .end local v3    # "canvas":Landroid/graphics/Canvas;
    .end local v5    # "offset":I
    .end local v14    # "height":I
    .end local v16    # "mirrorRoi":Landroid/graphics/RectF;
    .end local v17    # "previewBitmap":Landroid/graphics/Bitmap;
    .end local v20    # "scaleMatrix":Landroid/graphics/Matrix;
    .end local v24    # "width":I
    :cond_0
    invoke-virtual/range {v22 .. v22}, Landroid/graphics/RectF;->width()F

    move-result v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->val$mirrorBufferWidth:I

    int-to-float v6, v6

    div-float v19, v4, v6

    goto/16 :goto_0
.end method

.method public unableDone()V
    .locals 1

    .prologue
    .line 849
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$9;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 850
    return-void
.end method
