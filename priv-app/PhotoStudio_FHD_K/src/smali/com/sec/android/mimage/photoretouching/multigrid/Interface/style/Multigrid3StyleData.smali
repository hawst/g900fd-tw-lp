.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;
.super Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;
.source "Multigrid3StyleData.java"


# static fields
.field private static final mCollagePreset0:[[F

.field private static final mCollagePreset1:[[F

.field private static final mCollagePreset2:[[F

.field private static final mCollagePreset3:[[F

.field private static final mCollagePreset4:[[F

.field private static final mCollagePreset5:[[F

.field private static final mCollagePreset6:[[F

.field private static final mCollagePreset7:[[F

.field private static final mCollagePreset8:[[F

.field private static final mCollagePreset9:[[F

.field private static final mPilePreset0_1to1:[[F

.field private static final mPilePreset0_9to16:[[F

.field private static final mPilePreset1_1to1:[[F

.field private static final mPilePreset1_9to16:[[F

.field private static final mPilePreset2_1to1:[[F

.field private static final mPilePreset2_9to16:[[F

.field private static final mPilePreset3_1to1:[[F

.field private static final mPilePreset3_9to16:[[F


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 10
    new-array v0, v7, [[F

    .line 11
    new-array v1, v3, [F

    .line 12
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p375:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 14
    new-array v1, v3, [F

    .line 15
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p375:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 17
    new-array v1, v3, [F

    .line 18
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p375:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 8
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset9:[[F

    .line 24
    new-array v0, v7, [[F

    .line 25
    new-array v1, v3, [F

    .line 26
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p625:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 28
    new-array v1, v3, [F

    .line 29
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p625:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 31
    new-array v1, v3, [F

    .line 32
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p625:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 22
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset8:[[F

    .line 38
    new-array v0, v7, [[F

    .line 39
    new-array v1, v3, [F

    .line 40
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p625:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 42
    new-array v1, v3, [F

    .line 43
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p625:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 45
    new-array v1, v3, [F

    .line 46
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p625:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 36
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset7:[[F

    .line 52
    new-array v0, v7, [[F

    .line 53
    new-array v1, v3, [F

    .line 54
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p375:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 56
    new-array v1, v3, [F

    .line 57
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p375:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 59
    new-array v1, v3, [F

    .line 60
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p375:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 50
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset6:[[F

    .line 66
    new-array v0, v7, [[F

    .line 67
    new-array v1, v3, [F

    .line 68
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 70
    new-array v1, v3, [F

    .line 71
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 73
    new-array v1, v3, [F

    .line 74
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 64
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset0:[[F

    .line 80
    new-array v0, v7, [[F

    .line 81
    new-array v1, v3, [F

    .line 82
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 84
    new-array v1, v3, [F

    .line 85
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 87
    new-array v1, v3, [F

    .line 88
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 78
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset2:[[F

    .line 94
    new-array v0, v7, [[F

    .line 95
    new-array v1, v3, [F

    .line 96
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 98
    new-array v1, v3, [F

    .line 99
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 101
    new-array v1, v3, [F

    .line 102
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 92
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset3:[[F

    .line 108
    new-array v0, v7, [[F

    .line 109
    new-array v1, v3, [F

    .line 110
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 112
    new-array v1, v3, [F

    .line 113
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 115
    new-array v1, v3, [F

    .line 116
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 106
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset1:[[F

    .line 122
    new-array v0, v7, [[F

    .line 123
    new-array v1, v3, [F

    .line 124
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 126
    new-array v1, v3, [F

    .line 127
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 129
    new-array v1, v3, [F

    .line 130
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 120
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset5:[[F

    .line 136
    new-array v0, v7, [[F

    .line 137
    new-array v1, v3, [F

    .line 138
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 140
    new-array v1, v3, [F

    .line 141
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 143
    new-array v1, v3, [F

    .line 144
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 134
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset4:[[F

    .line 149
    new-array v0, v7, [[F

    .line 150
    new-array v1, v3, [F

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    .line 151
    new-array v1, v3, [F

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    .line 152
    new-array v1, v3, [F

    fill-array-data v1, :array_2

    aput-object v1, v0, v6

    .line 148
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset0_1to1:[[F

    .line 156
    new-array v0, v7, [[F

    .line 157
    new-array v1, v3, [F

    fill-array-data v1, :array_3

    aput-object v1, v0, v4

    .line 158
    new-array v1, v3, [F

    fill-array-data v1, :array_4

    aput-object v1, v0, v5

    .line 159
    new-array v1, v3, [F

    fill-array-data v1, :array_5

    aput-object v1, v0, v6

    .line 155
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset1_1to1:[[F

    .line 163
    new-array v0, v7, [[F

    .line 164
    new-array v1, v3, [F

    fill-array-data v1, :array_6

    aput-object v1, v0, v4

    .line 165
    new-array v1, v3, [F

    fill-array-data v1, :array_7

    aput-object v1, v0, v5

    .line 166
    new-array v1, v3, [F

    fill-array-data v1, :array_8

    aput-object v1, v0, v6

    .line 162
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset2_1to1:[[F

    .line 170
    new-array v0, v7, [[F

    .line 171
    new-array v1, v3, [F

    fill-array-data v1, :array_9

    aput-object v1, v0, v4

    .line 172
    new-array v1, v3, [F

    fill-array-data v1, :array_a

    aput-object v1, v0, v5

    .line 173
    new-array v1, v3, [F

    fill-array-data v1, :array_b

    aput-object v1, v0, v6

    .line 169
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset3_1to1:[[F

    .line 177
    new-array v0, v7, [[F

    .line 178
    new-array v1, v3, [F

    fill-array-data v1, :array_c

    aput-object v1, v0, v4

    .line 179
    new-array v1, v3, [F

    fill-array-data v1, :array_d

    aput-object v1, v0, v5

    .line 180
    new-array v1, v3, [F

    fill-array-data v1, :array_e

    aput-object v1, v0, v6

    .line 176
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset0_9to16:[[F

    .line 184
    new-array v0, v7, [[F

    .line 185
    new-array v1, v3, [F

    fill-array-data v1, :array_f

    aput-object v1, v0, v4

    .line 186
    new-array v1, v3, [F

    fill-array-data v1, :array_10

    aput-object v1, v0, v5

    .line 187
    new-array v1, v3, [F

    fill-array-data v1, :array_11

    aput-object v1, v0, v6

    .line 183
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset1_9to16:[[F

    .line 191
    new-array v0, v7, [[F

    .line 192
    new-array v1, v3, [F

    fill-array-data v1, :array_12

    aput-object v1, v0, v4

    .line 193
    new-array v1, v3, [F

    fill-array-data v1, :array_13

    aput-object v1, v0, v5

    .line 194
    new-array v1, v3, [F

    fill-array-data v1, :array_14

    aput-object v1, v0, v6

    .line 190
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset2_9to16:[[F

    .line 200
    new-array v0, v7, [[F

    .line 201
    new-array v1, v3, [F

    fill-array-data v1, :array_15

    aput-object v1, v0, v4

    .line 202
    new-array v1, v3, [F

    fill-array-data v1, :array_16

    aput-object v1, v0, v5

    .line 203
    new-array v1, v3, [F

    fill-array-data v1, :array_17

    aput-object v1, v0, v6

    .line 199
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset3_9to16:[[F

    .line 204
    return-void

    .line 150
    :array_0
    .array-data 4
        0x3edb089a    # 0.4278f
        -0x43333333    # -0.025f
        0x3f688659    # 0.9083f
        0x3f127bb3    # 0.5722f
    .end array-data

    .line 151
    :array_1
    .array-data 4
        -0x44487fcc    # -0.0056f
        0x3d63bcd3    # 0.0556f
        0x3f0fa440    # 0.5611f
        0x3f360aa6    # 0.7111f
    .end array-data

    .line 152
    :array_2
    .array-data 4
        0x3e7a43fe    # 0.2444f
        0x3f04f766    # 0.5194f
        0x3f5bb98c    # 0.8583f
        0x3f827bb3    # 1.0194f
    .end array-data

    .line 157
    :array_3
    .array-data 4
        0x3ef61134    # 0.4806f
        0x3da5119d    # 0.0806f
        0x3f6c154d    # 0.9222f
        0x3edf4880    # 0.4361f
    .end array-data

    .line 158
    :array_4
    .array-data 4
        0x3eb77319    # 0.3583f
        0x3ef77319    # 0.4833f
        0x3f36c227    # 0.7139f
        0x3f705bc0    # 0.9389f
    .end array-data

    .line 159
    :array_5
    .array-data 4
        0x3e000000    # 0.125f
        0x3e24f766    # 0.1611f
        0x3f000000    # 0.5f
        0x3f24f766    # 0.6444f
    .end array-data

    .line 164
    :array_6
    .array-data 4
        0x3f288659    # 0.6583f
        0x3e93eab3    # 0.2889f
        0x3f782a99    # 0.9694f
        0x3f2eecc0    # 0.6833f
    .end array-data

    .line 165
    :array_7
    .array-data 4
        0x3ea38866    # 0.3194f
        0x3ea4f766    # 0.3222f
        0x3f3c710d    # 0.7361f
        0x3f25aee6    # 0.6472f
    .end array-data

    .line 166
    :array_8
    .array-data 4
        0x3c9eecc0    # 0.0194f
        0x3e8faace    # 0.2806f
        0x3ec43fe6    # 0.3833f
        0x3f3d288d    # 0.7389f
    .end array-data

    .line 171
    :array_9
    .array-data 4
        0x3e0b5dcc    # 0.1361f
        0x3e24f766    # 0.1611f
        0x3ec5aee6    # 0.3861f
        0x3efa511a    # 0.4889f
    .end array-data

    .line 172
    :array_a
    .array-data 4
        0x3f1b089a    # 0.6056f
        0x3ec9eecc    # 0.3944f
        0x3f62d773    # 0.8861f
        0x3f105bc0    # 0.5639f
    .end array-data

    .line 173
    :array_b
    .array-data 4
        0x3ea38866    # 0.3194f
        0x3f3c710d    # 0.7361f
        0x3f044674    # 0.5167f
        0x3f5de00d    # 0.8667f
    .end array-data

    .line 178
    :array_c
    .array-data 4
        0x3ec2de01    # 0.3806f
        0x3e84d014    # 0.2594f
        0x3f82d773    # 1.0222f
        0x3f32ca58    # 0.6984f
    .end array-data

    .line 179
    :array_d
    .array-data 4
        -0x423eab36    # -0.0944f
        0x3d06594b    # 0.0328f
        0x3f216f00    # 0.6306f
        0x3f00cb29    # 0.5031f
    .end array-data

    .line 180
    :array_e
    .array-data 4
        0x3e087fcc    # 0.1333f
        0x3f246738    # 0.6422f
        0x3f51c433    # 0.8194f
        0x3f753261    # 0.9578f
    .end array-data

    .line 185
    :array_f
    .array-data 4
        0x3ee7d567    # 0.4528f
        0x3e959b3d    # 0.2922f
        0x3f800000    # 1.0f
        0x3f2ecbfb    # 0.6828f
    .end array-data

    .line 186
    :array_10
    .array-data 4
        0x3e38ef35    # 0.1806f
        0x3f0acd9f    # 0.5422f
        0x3f2ccccd    # 0.675f
        0x3f66cf42    # 0.9016f
    .end array-data

    .line 187
    :array_11
    .array-data 4
        0x0
        0x3e07fcb9    # 0.1328f
        0x3f2ccccd    # 0.675f
        0x3ee0d1b7    # 0.4391f
    .end array-data

    .line 192
    :array_12
    .array-data 4
        0x3e71c433    # 0.2361f
        0x3f18ce70    # 0.5969f
        0x3f360aa6    # 0.7111f
        0x3f4ecbfb    # 0.8078f
    .end array-data

    .line 193
    :array_13
    .array-data 4
        0x3f05aee6    # 0.5222f
        0x3e467382    # 0.1938f
        0x3f74a234    # 0.9556f
        0x3f000000    # 0.5f
    .end array-data

    .line 194
    :array_14
    .array-data 4
        0x3d63bcd3    # 0.0556f
        0x3e47fcb9    # 0.1953f
        0x3ef4a234    # 0.4778f
        0x3f000000    # 0.5f
    .end array-data

    .line 201
    :array_15
    .array-data 4
        0x3e1f559b    # 0.1556f
        0x3e067382    # 0.1313f
        0x3ee7d567    # 0.4528f
        0x3eb404ea    # 0.3516f
    .end array-data

    .line 202
    :array_16
    .array-data 4
        0x3f038ef3    # 0.5139f
        0x3f0acd9f    # 0.5422f
        0x3f59999a    # 0.85f
        0x3f286595    # 0.6578f
    .end array-data

    .line 203
    :array_17
    .array-data 4
        0x3e416f00    # 0.1889f
        0x3f480347    # 0.7813f
        0x3ed9999a    # 0.425f
        0x3f5e69ad    # 0.8688f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;-><init>()V

    return-void
.end method

.method public static getDrawRects(II)[[F
    .locals 4
    .param p0, "style"    # I
    .param p1, "proportion"    # I

    .prologue
    const/high16 v3, 0x1e500000

    .line 210
    const/4 v0, 0x0

    .line 212
    .local v0, "returnPreset":[[F
    const/high16 v1, 0x1e200000

    const/4 v2, 0x3

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getMaxStyleNum(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    move-result p0

    .line 214
    packed-switch p0, :pswitch_data_0

    .line 301
    const/4 v0, 0x0

    .line 305
    :goto_0
    return-object v0

    .line 217
    :pswitch_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset0:[[F

    .line 218
    goto :goto_0

    .line 221
    :pswitch_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset1:[[F

    .line 222
    goto :goto_0

    .line 225
    :pswitch_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset2:[[F

    .line 226
    goto :goto_0

    .line 229
    :pswitch_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset3:[[F

    .line 230
    goto :goto_0

    .line 233
    :pswitch_4
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset4:[[F

    .line 234
    goto :goto_0

    .line 237
    :pswitch_5
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset5:[[F

    .line 238
    goto :goto_0

    .line 241
    :pswitch_6
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset6:[[F

    .line 242
    goto :goto_0

    .line 245
    :pswitch_7
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset7:[[F

    .line 246
    goto :goto_0

    .line 249
    :pswitch_8
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset8:[[F

    .line 250
    goto :goto_0

    .line 253
    :pswitch_9
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mCollagePreset9:[[F

    .line 254
    goto :goto_0

    .line 258
    :pswitch_a
    if-ne p1, v3, :cond_0

    .line 260
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset0_1to1:[[F

    .line 261
    goto :goto_0

    .line 264
    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset0_9to16:[[F

    .line 266
    goto :goto_0

    .line 269
    :pswitch_b
    if-ne p1, v3, :cond_1

    .line 271
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset1_1to1:[[F

    .line 272
    goto :goto_0

    .line 275
    :cond_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset1_9to16:[[F

    .line 277
    goto :goto_0

    .line 280
    :pswitch_c
    if-ne p1, v3, :cond_2

    .line 282
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset2_1to1:[[F

    .line 283
    goto :goto_0

    .line 286
    :cond_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset2_9to16:[[F

    .line 288
    goto :goto_0

    .line 291
    :pswitch_d
    if-ne p1, v3, :cond_3

    .line 293
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset3_1to1:[[F

    .line 294
    goto :goto_0

    .line 297
    :cond_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid3StyleData;->mPilePreset3_9to16:[[F

    .line 299
    goto :goto_0

    .line 214
    :pswitch_data_0
    .packed-switch 0x1e200000
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method


# virtual methods
.method public getRotate(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 312
    const/4 v0, 0x0

    return v0
.end method
