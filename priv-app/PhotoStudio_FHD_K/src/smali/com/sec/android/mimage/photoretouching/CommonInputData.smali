.class public Lcom/sec/android/mimage/photoretouching/CommonInputData;
.super Ljava/lang/Object;
.source "CommonInputData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/CommonInputData$Data;
    }
.end annotation


# static fields
.field public static count:I

.field public static mDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/CommonInputData$Data;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/CommonInputData;->mDataList:Ljava/util/ArrayList;

    .line 8
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/mimage/photoretouching/CommonInputData;->count:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getData(I)[I
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/mimage/photoretouching/CommonInputData;->mDataList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 38
    const/4 v0, 0x0

    .line 41
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/CommonInputData;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/CommonInputData$Data;

    sget-object v0, Lcom/sec/android/mimage/photoretouching/CommonInputData$Data;->data:[I

    goto :goto_0
.end method

.method public static removeData([I)V
    .locals 2
    .param p0, "buff"    # [I

    .prologue
    .line 47
    sget-object v1, Lcom/sec/android/mimage/photoretouching/CommonInputData;->mDataList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 49
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/sec/android/mimage/photoretouching/CommonInputData;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 59
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return-void

    .line 51
    .restart local v0    # "i":I
    :cond_1
    sget-object v1, Lcom/sec/android/mimage/photoretouching/CommonInputData;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/CommonInputData$Data;

    sget-object v1, Lcom/sec/android/mimage/photoretouching/CommonInputData$Data;->data:[I

    if-ne v1, p0, :cond_2

    .line 53
    sget-object v1, Lcom/sec/android/mimage/photoretouching/CommonInputData;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/CommonInputData$Data;

    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/mimage/photoretouching/CommonInputData$Data;->data:[I

    .line 54
    sget-object v1, Lcom/sec/android/mimage/photoretouching/CommonInputData;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 49
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static setData([I)I
    .locals 2
    .param p0, "buff"    # [I

    .prologue
    .line 22
    sget-object v1, Lcom/sec/android/mimage/photoretouching/CommonInputData;->mDataList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 23
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/sec/android/mimage/photoretouching/CommonInputData;->mDataList:Ljava/util/ArrayList;

    .line 25
    :cond_0
    new-instance v0, Lcom/sec/android/mimage/photoretouching/CommonInputData$Data;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/CommonInputData$Data;-><init>([I)V

    .line 27
    .local v0, "newData":Lcom/sec/android/mimage/photoretouching/CommonInputData$Data;
    sget-object v1, Lcom/sec/android/mimage/photoretouching/CommonInputData;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    sget-object v1, Lcom/sec/android/mimage/photoretouching/CommonInputData;->mDataList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    return v1
.end method
