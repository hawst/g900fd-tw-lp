.class public Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;
.super Ljava/lang/Object;
.source "DecodeTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;,
        Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;,
        Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;,
        Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;,
        Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;
    }
.end annotation


# static fields
.field public static final FROM_ANOTHER_APP:I = 0x2

.field public static final FROM_COPYTOANOTHER:I = 0x1

.field public static final FROM_LAUNCHER:I


# instance fields
.field final DECODE_THREAD:I

.field final FIRST_HANDLER:I

.field final INIT_HANDLER:I

.field final PARSING_THREAD:I

.field final SECOND_HANDLER:I

.field private mBitmap:Landroid/graphics/Bitmap;

.field mCancelDecodeThread:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;

.field mCancelDecodeThread1:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;

.field private mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mDecodeFromUri:Z

.field private mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

.field private mDecodeMode:I

.field private mDecodeState1:Z

.field private mDecodeState2:Z

.field private mDecodeSucc1:Z

.field private mDecodeSucc2:Z

.field private mFromDecode:I

.field private mIdx:I

.field private mIsTaskCancel:Z

.field private mOnTaskFinish:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;

.field private mSkiaMode:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;IILcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "decodeInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .param p3, "finishCallback"    # Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;
    .param p4, "fromDecode"    # I
    .param p5, "idx"    # I
    .param p6, "changeViewCallback"    # Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIdx:I

    .line 840
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mOnTaskFinish:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;

    .line 842
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeFromUri:Z

    .line 843
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 846
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState1:Z

    .line 847
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState2:Z

    .line 848
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc1:Z

    .line 849
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc2:Z

    .line 851
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeMode:I

    .line 852
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mFromDecode:I

    .line 854
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z

    .line 856
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContentResolver:Landroid/content/ContentResolver;

    .line 857
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContext:Landroid/content/Context;

    .line 858
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    .line 860
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->INIT_HANDLER:I

    .line 861
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->PARSING_THREAD:I

    .line 862
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->FIRST_HANDLER:I

    .line 863
    const/4 v1, 0x4

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->DECODE_THREAD:I

    .line 864
    const/4 v1, 0x5

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->SECOND_HANDLER:I

    .line 869
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    .line 871
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mSkiaMode:Z

    .line 46
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContext:Landroid/content/Context;

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContentResolver:Landroid/content/ContentResolver;

    .line 48
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .line 49
    iput-object p0, p2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->task:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    .line 50
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mOnTaskFinish:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;

    .line 52
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 54
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeFromUri:Z

    .line 60
    :cond_0
    :goto_0
    iput p5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIdx:I

    .line 61
    iput p4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mFromDecode:I

    .line 63
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    .line 65
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/Image;->getRealPathFromURI(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "realPath":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 67
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mSkiaMode:Z

    .line 76
    :goto_1
    return-void

    .line 56
    .end local v0    # "realPath":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 58
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeFromUri:Z

    goto :goto_0

    .line 70
    .restart local v0    # "realPath":Ljava/lang/String;
    :cond_2
    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ".JPG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 71
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mSkiaMode:Z

    goto :goto_1

    .line 73
    :cond_3
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mSkiaMode:Z

    goto :goto_1
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;I)V
    .locals 0

    .prologue
    .line 851
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeMode:I

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)I
    .locals 1

    .prologue
    .line 851
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeMode:I

    return v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)V
    .locals 0

    .prologue
    .line 333
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->makeImageHandlerAboutStream()V

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V
    .locals 0

    .prologue
    .line 846
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState1:Z

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z
    .locals 1

    .prologue
    .line 848
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc1:Z

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 194
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->preDecodingAboutStream(II)V

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIdx:I

    return v0
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)V
    .locals 0

    .prologue
    .line 456
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->makeImageHandlerAboutLocalPath()V

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;II)V
    .locals 0

    .prologue
    .line 379
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->preDecodingAboutLocalPath(II)V

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z
    .locals 1

    .prologue
    .line 846
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState1:Z

    return v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V
    .locals 0

    .prologue
    .line 848
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc1:Z

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z
    .locals 1

    .prologue
    .line 847
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState2:Z

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z
    .locals 1

    .prologue
    .line 871
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mSkiaMode:Z

    return v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V
    .locals 0

    .prologue
    .line 849
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc2:Z

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;
    .locals 1

    .prologue
    .line 869
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    return-object v0
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;)V
    .locals 0

    .prologue
    .line 869
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mChangeViewCallback:Lcom/sec/android/mimage/photoretouching/Core/DecodeAsync$ChangeViewCallback;

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 858
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 858
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;II)I
    .locals 1

    .prologue
    .line 295
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->getSampleSize(II)I

    move-result v0

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)I
    .locals 1

    .prologue
    .line 852
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mFromDecode:I

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V
    .locals 0

    .prologue
    .line 847
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState2:Z

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z
    .locals 1

    .prologue
    .line 854
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z
    .locals 1

    .prologue
    .line 849
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc2:Z

    return v0
.end method

.method private finishTask()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 152
    const/4 v0, -0x1

    .line 153
    .local v0, "idx":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    iput-object v2, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    .line 154
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    .line 155
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isFinish:Z

    .line 156
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->trayButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    if-eqz v1, :cond_1

    .line 158
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->trayButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mFromDecode:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setDecodeInfo(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V

    .line 159
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->trayButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getIndexFromTrayButtonList()I

    move-result v0

    .line 161
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$1;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 187
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iput-object v4, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->trayButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 188
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mOnTaskFinish:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;

    if-eqz v1, :cond_0

    .line 189
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mOnTaskFinish:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;

    invoke-interface {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;->finishTask(I)V

    .line 190
    :cond_0
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContext:Landroid/content/Context;

    .line 191
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIdx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]finish decode task."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 192
    return-void

    .line 179
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mCancelDecodeThread:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;

    if-eqz v1, :cond_2

    .line 180
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mCancelDecodeThread:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->exit()V

    .line 182
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mCancelDecodeThread1:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;

    if-eqz v1, :cond_3

    .line 183
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mCancelDecodeThread1:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->exit()V

    .line 185
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget v0, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->index:I

    goto :goto_0
.end method

.method private getResizeRatio(FF)F
    .locals 2
    .param p1, "bitmapSize"    # F
    .param p2, "maxSize"    # F

    .prologue
    .line 328
    div-float v0, p1, p2

    .line 329
    .local v0, "ret":F
    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 330
    const/high16 v0, 0x3f800000    # 1.0f

    .line 331
    :cond_0
    return v0
.end method

.method private getSampleSize(II)I
    .locals 3
    .param p1, "bitmapSize"    # I
    .param p2, "maxSize"    # I

    .prologue
    .line 297
    const/4 v1, 0x1

    .line 298
    .local v1, "ret":I
    if-le p1, p2, :cond_0

    .line 300
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    div-int v2, p1, v0

    if-gt v2, p2, :cond_1

    .line 304
    add-int/lit8 v1, v1, 0x1

    .line 308
    .end local v0    # "i":I
    :cond_0
    return v1

    .line 302
    .restart local v0    # "i":I
    :cond_1
    move v1, v0

    .line 300
    mul-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method private makeBitmapMatrix(Landroid/graphics/Matrix;)Z
    .locals 7
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 791
    const/4 v1, 0x0

    .line 792
    .local v1, "ret":Z
    const/4 v2, 0x0

    .line 793
    .local v2, "rotate":I
    const/high16 v3, 0x3f800000    # 1.0f

    .line 794
    .local v3, "scale":F
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    if-eqz v4, :cond_6

    .line 795
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotateDegree(Landroid/content/ContentResolver;Landroid/net/Uri;)I

    move-result v2

    .line 798
    :cond_0
    :goto_0
    if-nez v2, :cond_7

    .line 799
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 814
    :goto_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    mul-int v0, v4, v5

    .line 815
    .local v0, "imageSize":I
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mFromDecode:I

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mFromDecode:I

    const/4 v5, 0x7

    if-ne v4, v5, :cond_3

    .line 817
    :cond_1
    const v4, 0x2dc6c0

    if-le v0, v4, :cond_3

    .line 818
    const v4, 0x4a371b00    # 3000000.0f

    int-to-float v5, v0

    div-float/2addr v4, v5

    invoke-static {v4}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v3

    .line 819
    const v4, 0x3f7fbe77    # 0.999f

    cmpl-float v4, v3, v4

    if-lez v4, :cond_2

    cmpg-float v4, v3, v6

    if-gez v4, :cond_2

    .line 820
    const v3, 0x3f7fbe77    # 0.999f

    .line 821
    :cond_2
    invoke-virtual {p1, v3, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 834
    :cond_3
    if-nez v2, :cond_4

    cmpl-float v4, v3, v6

    if-eqz v4, :cond_5

    .line 835
    :cond_4
    const/4 v1, 0x1

    .line 837
    :cond_5
    return v1

    .line 796
    .end local v0    # "imageSize":I
    :cond_6
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 797
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotateDegree(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 801
    :cond_7
    const/16 v4, 0x5a

    if-ne v2, v4, :cond_8

    .line 802
    const/high16 v4, 0x42b40000    # 90.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_1

    .line 804
    :cond_8
    const/16 v4, 0xb4

    if-ne v2, v4, :cond_9

    .line 805
    const/high16 v4, 0x43340000    # 180.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_1

    .line 807
    :cond_9
    const/16 v4, 0x10e

    if-ne v2, v4, :cond_a

    .line 808
    const/high16 v4, 0x43870000    # 270.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_1

    .line 811
    :cond_a
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private makeImageHandlerAboutLocalPath()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 458
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 460
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeMode:I

    .line 462
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 464
    .local v5, "matrix":Landroid/graphics/Matrix;
    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->makeBitmapMatrix(Landroid/graphics/Matrix;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 466
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 467
    .local v7, "b":Landroid/graphics/Bitmap;
    if-nez v7, :cond_0

    .line 469
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    .line 490
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    .end local v7    # "b":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 472
    .restart local v5    # "matrix":Landroid/graphics/Matrix;
    .restart local v7    # "b":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 473
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    .line 477
    .end local v7    # "b":Landroid/graphics/Bitmap;
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z

    if-eqz v0, :cond_2

    .line 478
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto :goto_0

    .line 482
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->finishTask()V

    goto :goto_0

    .line 486
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    :cond_3
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc2:Z

    .line 487
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto :goto_0
.end method

.method private makeImageHandlerAboutStream()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 335
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 337
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeMode:I

    .line 339
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 341
    .local v5, "matrix":Landroid/graphics/Matrix;
    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->makeBitmapMatrix(Landroid/graphics/Matrix;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 343
    const/4 v7, 0x0

    .line 344
    .local v7, "b":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 348
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 356
    :cond_0
    :goto_0
    if-nez v7, :cond_1

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    .line 378
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    .end local v7    # "b":Landroid/graphics/Bitmap;
    :goto_1
    return-void

    .line 350
    .restart local v5    # "matrix":Landroid/graphics/Matrix;
    .restart local v7    # "b":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v8

    .line 352
    .local v8, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v8}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 353
    const/4 v7, 0x0

    goto :goto_0

    .line 361
    .end local v8    # "e":Ljava/lang/OutOfMemoryError;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 362
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    .line 364
    .end local v7    # "b":Landroid/graphics/Bitmap;
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z

    if-eqz v0, :cond_3

    .line 366
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto :goto_1

    .line 370
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->finishTask()V

    goto :goto_1

    .line 374
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    :cond_4
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc2:Z

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto :goto_1
.end method

.method private preDecodingAboutLocalPath(II)V
    .locals 12
    .param p1, "originalWidth"    # I
    .param p2, "originalHeight"    # I

    .prologue
    const/16 v11, 0x7d0

    const/4 v10, 0x7

    const/4 v9, 0x0

    const/4 v5, 0x1

    .line 381
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeMode:I

    .line 383
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 384
    :cond_0
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc1:Z

    .line 385
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    .line 453
    :goto_0
    return-void

    .line 389
    :cond_1
    mul-int v6, p1, p2

    .line 390
    .local v6, "imageSize":I
    const/4 v8, 0x1

    .line 412
    .local v8, "sampleSize":I
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 413
    .local v7, "mOptions":Landroid/graphics/BitmapFactory$Options;
    iput p2, v7, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 414
    iput p1, v7, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 416
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mFromDecode:I

    const/4 v4, 0x5

    if-eq v1, v4, :cond_2

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mFromDecode:I

    if-ne v1, v10, :cond_3

    .line 418
    :cond_2
    const v1, 0x2dc6c0

    invoke-direct {p0, v6, v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->getSampleSize(II)I

    move-result v8

    .line 429
    :goto_1
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 430
    .local v2, "optsResolution":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v9, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 431
    iput v8, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 432
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 433
    iput-boolean v5, v2, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 435
    new-instance v3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v3}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 436
    .local v3, "quramOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    iput v10, v3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    .line 437
    iput-boolean v5, v3, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inCancelingRequested:Z

    .line 439
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z

    if-eqz v1, :cond_5

    .line 441
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto :goto_0

    .line 420
    .end local v2    # "optsResolution":Landroid/graphics/BitmapFactory$Options;
    .end local v3    # "quramOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    :cond_3
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mFromDecode:I

    if-ne v1, v5, :cond_4

    .line 422
    invoke-virtual {p0, v7, v11, v11}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v8

    .line 423
    goto :goto_1

    .line 426
    :cond_4
    const v1, 0x7a1200

    invoke-direct {p0, v6, v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->getSampleSize(II)I

    move-result v8

    goto :goto_1

    .line 445
    .restart local v2    # "optsResolution":Landroid/graphics/BitmapFactory$Options;
    .restart local v3    # "quramOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    :cond_5
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState2:Z

    .line 446
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc2:Z

    .line 448
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/BitmapFactory$Options;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mCancelDecodeThread:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;

    .line 449
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mCancelDecodeThread:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->start()V

    .line 451
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    const/4 v5, 0x4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/BitmapFactory$Options;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/lang/String;I)V

    .line 452
    .local v0, "decodeThread1":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private preDecodingAboutStream(II)V
    .locals 18
    .param p1, "originalWidth"    # I
    .param p2, "originalHeight"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 196
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeMode:I

    .line 197
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 198
    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc1:Z

    .line 199
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    .line 294
    :goto_0
    return-void

    .line 203
    :cond_1
    mul-int v11, p1, p2

    .line 204
    .local v11, "imageSize":I
    const/4 v14, 0x1

    .line 205
    .local v14, "sampleSize":I
    const/high16 v13, 0x3f800000    # 1.0f

    .line 206
    .local v13, "resizeRatio":F
    new-instance v12, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v12}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 207
    .local v12, "mOptions":Landroid/graphics/BitmapFactory$Options;
    move/from16 v0, p2

    iput v0, v12, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 208
    move/from16 v0, p1

    iput v0, v12, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 209
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mFromDecode:I

    const/4 v7, 0x5

    if-eq v3, v7, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mFromDecode:I

    const/4 v7, 0x7

    if-ne v3, v7, :cond_4

    .line 211
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mSkiaMode:Z

    if-eqz v3, :cond_3

    .line 212
    const v3, 0x2dc6c0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v3}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->getSampleSize(II)I

    move-result v14

    .line 230
    :goto_1
    const/4 v6, 0x0

    .line 232
    .local v6, "stream":Ljava/io/InputStream;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContentResolver:Landroid/content/ContentResolver;

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    if-eqz v3, :cond_7

    .line 233
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v7, v7, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v7}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 246
    if-nez v6, :cond_8

    .line 248
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc1:Z

    .line 249
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto :goto_0

    .line 214
    .end local v6    # "stream":Ljava/io/InputStream;
    :cond_3
    int-to-float v3, v11

    const v7, 0x4a371b00    # 3000000.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v7}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->getResizeRatio(FF)F

    move-result v13

    .line 215
    goto :goto_1

    .line 216
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mFromDecode:I

    const/4 v7, 0x1

    if-ne v3, v7, :cond_5

    .line 219
    const/16 v3, 0x7d0

    const/16 v7, 0x7d0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v3, v7}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v14

    .line 221
    goto :goto_1

    .line 224
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mSkiaMode:Z

    if-eqz v3, :cond_6

    .line 225
    const v3, 0x7a1200

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v3}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->getSampleSize(II)I

    move-result v14

    goto :goto_1

    .line 227
    :cond_6
    int-to-float v3, v11

    const v7, 0x4af42400    # 8000000.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v7}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->getResizeRatio(FF)F

    move-result v13

    goto :goto_1

    .line 235
    .restart local v6    # "stream":Ljava/io/InputStream;
    :cond_7
    const/4 v3, 0x0

    :try_start_1
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc1:Z

    .line 236
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 239
    :catch_0
    move-exception v10

    .line 240
    .local v10, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v10}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 241
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc1:Z

    .line 242
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto/16 :goto_0

    .line 253
    .end local v10    # "e":Ljava/io/FileNotFoundException;
    :cond_8
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 254
    .local v4, "optsResolution":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x0

    iput-boolean v3, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 255
    iput v14, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 256
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v3, v4, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 257
    const/4 v3, 0x1

    iput-boolean v3, v4, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 260
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "bigheadk, Config.ARGB_8888 = "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 261
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "bigheadk, Config.ARGB_8888 = "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v7}, Landroid/graphics/Bitmap$Config;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 263
    const/4 v8, 0x0

    .line 264
    .local v8, "width":I
    const/4 v9, 0x0

    .line 265
    .local v9, "height":I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mSkiaMode:Z

    if-eqz v3, :cond_9

    .line 267
    move/from16 v0, p1

    int-to-float v3, v0

    int-to-double v0, v14

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v7, v0

    div-float/2addr v3, v7

    float-to-int v8, v3

    .line 268
    move/from16 v0, p2

    int-to-float v3, v0

    int-to-double v0, v14

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v7, v0

    div-float/2addr v3, v7

    float-to-int v9, v3

    .line 276
    :goto_2
    new-instance v5, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-direct {v5}, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;-><init>()V

    .line 277
    .local v5, "quramOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    const/4 v3, 0x7

    iput v3, v5, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inPreferredConfig:I

    .line 278
    const/4 v3, 0x1

    iput-boolean v3, v5, Lcom/quramsoft/qrb/QuramBitmapFactory$Options;->inCancelingRequested:Z

    .line 281
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z

    if-eqz v3, :cond_a

    .line 282
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto/16 :goto_0

    .line 272
    .end local v5    # "quramOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    :cond_9
    move/from16 v0, p1

    int-to-float v3, v0

    float-to-double v0, v13

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v7, v0

    div-float/2addr v3, v7

    float-to-int v8, v3

    .line 273
    move/from16 v0, p2

    int-to-float v3, v0

    float-to-double v0, v13

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v7, v0

    div-float/2addr v3, v7

    float-to-int v9, v3

    goto :goto_2

    .line 286
    .restart local v5    # "quramOptions":Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    :cond_a
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState2:Z

    .line 287
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc2:Z

    .line 289
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/BitmapFactory$Options;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mCancelDecodeThread:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;

    .line 290
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mCancelDecodeThread:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$SecondCancelDecodeThread;->start()V

    .line 292
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;

    const/4 v7, 0x4

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/BitmapFactory$Options;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/io/InputStream;III)V

    .line 293
    .local v2, "decodeThread1":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0
.end method


# virtual methods
.method public calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 6
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "reqWidth"    # I
    .param p3, "reqHeight"    # I

    .prologue
    .line 312
    iget v2, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 313
    .local v2, "height":I
    iget v4, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 314
    .local v4, "width":I
    const/4 v3, 0x1

    .line 315
    .local v3, "inSampleSize":I
    if-gt v2, p3, :cond_0

    if-le v4, p2, :cond_1

    .line 316
    :cond_0
    div-int/lit8 v0, v2, 0x2

    .line 317
    .local v0, "halfHeight":I
    div-int/lit8 v1, v4, 0x2

    .line 318
    .local v1, "halfWidth":I
    :goto_0
    div-int v5, v0, v3

    if-gt v5, p3, :cond_2

    .line 319
    div-int v5, v1, v3

    .line 318
    if-gt v5, p2, :cond_2

    .line 323
    .end local v0    # "halfHeight":I
    .end local v1    # "halfWidth":I
    :cond_1
    return v3

    .line 320
    .restart local v0    # "halfHeight":I
    .restart local v1    # "halfWidth":I
    :cond_2
    mul-int/lit8 v3, v3, 0x2

    goto :goto_0
.end method

.method public doTask()V
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 82
    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeMode:I

    .line 84
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeFromUri:Z

    if-eqz v1, :cond_3

    .line 86
    const/4 v4, 0x0

    .line 87
    .local v4, "stream":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 88
    .local v2, "optsResolution":Landroid/graphics/BitmapFactory$Options;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContentResolver:Landroid/content/ContentResolver;

    if-nez v1, :cond_1

    .line 89
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContentResolver:Landroid/content/ContentResolver;

    .line 90
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContentResolver:Landroid/content/ContentResolver;

    if-nez v1, :cond_1

    .line 91
    iput-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc1:Z

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    .line 131
    .end local v4    # "stream":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return-void

    .line 98
    .restart local v4    # "stream":Ljava/io/InputStream;
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContentResolver:Landroid/content/ContentResolver;

    if-eqz v1, :cond_2

    .line 99
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v7, v7, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v7}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 105
    :cond_2
    if-eqz v4, :cond_0

    .line 107
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    .end local v2    # "optsResolution":Landroid/graphics/BitmapFactory$Options;
    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 108
    .restart local v2    # "optsResolution":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v8, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 110
    iput-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState1:Z

    .line 111
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc1:Z

    .line 112
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/BitmapFactory$Options;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mCancelDecodeThread1:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;

    .line 113
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mCancelDecodeThread1:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->start()V

    .line 114
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;

    move-object v1, p0

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/BitmapFactory$Options;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/io/InputStream;III)V

    .line 115
    .local v0, "decodeThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 100
    .end local v0    # "decodeThread":Ljava/lang/Thread;
    :catch_0
    move-exception v12

    .line 101
    .local v12, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v12}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto :goto_0

    .line 120
    .end local v2    # "optsResolution":Landroid/graphics/BitmapFactory$Options;
    .end local v4    # "stream":Ljava/io/InputStream;
    .end local v12    # "e":Ljava/io/FileNotFoundException;
    :cond_3
    const/4 v2, 0x0

    .line 121
    .restart local v2    # "optsResolution":Landroid/graphics/BitmapFactory$Options;
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    .end local v2    # "optsResolution":Landroid/graphics/BitmapFactory$Options;
    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 122
    .restart local v2    # "optsResolution":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v8, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 124
    iput-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeState1:Z

    .line 125
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc1:Z

    .line 126
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/BitmapFactory$Options;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mCancelDecodeThread1:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;

    .line 127
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mCancelDecodeThread1:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$FirstCancelDecodeThread;->start()V

    .line 128
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-object v10, v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    move-object v6, v0

    move-object v7, p0

    move-object v8, v2

    move-object v9, v3

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeLocalPathThread;-><init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/BitmapFactory$Options;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/lang/String;I)V

    .line 129
    .restart local v0    # "decodeThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public isCancel()Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z

    return v0
.end method

.method public isFinish()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isFinish:Z

    return v0
.end method

.method public run()V
    .locals 0

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->doTask()V

    .line 80
    return-void
.end method

.method public taskCancel()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 142
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;

    .line 143
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isFinish:Z

    .line 144
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeInfo:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isCanceled:Z

    .line 145
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z

    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mOnTaskFinish:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mOnTaskFinish:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;

    const/4 v1, -0x3

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$OnTaskFinish;->finishTask(I)V

    .line 148
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mContext:Landroid/content/Context;

    .line 149
    return-void
.end method
