.class public Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;
.super Lcom/sec/android/mimage/photoretouching/Core/RectController;
.source "CopyPaste.java"


# instance fields
.field private Object_avg_b:I

.field private Object_avg_g:I

.field private Object_avg_r:I

.field private OrgObjectBmp:Landroid/graphics/Bitmap;

.field private bSeamless:Z

.field private mBackImgHeight:I

.field private mBackImgWidth:I

.field private mbApply:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Rect;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;IZZ)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "srcroi"    # Landroid/graphics/Rect;
    .param p3, "imgData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p4, "copyToObjectImage"    # Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;
    .param p5, "boundaryType"    # I
    .param p6, "freeRect"    # Z
    .param p7, "bseamless"    # Z

    .prologue
    const/4 v7, 0x0

    .line 30
    invoke-direct {p0, p1, p3, p5, p6}, Lcom/sec/android/mimage/photoretouching/Core/RectController;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;IZ)V

    .line 31
    iput-boolean p7, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->bSeamless:Z

    .line 33
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 34
    iget v5, p2, Landroid/graphics/Rect;->left:I

    if-gez v5, :cond_0

    .line 35
    iput v7, p2, Landroid/graphics/Rect;->left:I

    .line 36
    :cond_0
    iget v5, p2, Landroid/graphics/Rect;->top:I

    if-gez v5, :cond_1

    .line 37
    iput v7, p2, Landroid/graphics/Rect;->top:I

    .line 38
    :cond_1
    iget v5, p2, Landroid/graphics/Rect;->right:I

    invoke-virtual {p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-le v5, v6, :cond_2

    .line 39
    invoke-virtual {p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    iput v5, p2, Landroid/graphics/Rect;->right:I

    .line 40
    :cond_2
    iget v5, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-le v5, v6, :cond_3

    .line 41
    invoke-virtual {p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    iput v5, p2, Landroid/graphics/Rect;->bottom:I

    .line 43
    :cond_3
    iget v5, p2, Landroid/graphics/Rect;->left:I

    iget v6, p2, Landroid/graphics/Rect;->right:I

    if-gt v5, v6, :cond_4

    iget v5, p2, Landroid/graphics/Rect;->top:I

    iget v6, p2, Landroid/graphics/Rect;->bottom:I

    if-le v5, v6, :cond_a

    .line 44
    :cond_4
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mbApply:Z

    .line 48
    :goto_0
    invoke-virtual {p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mBackImgWidth:I

    .line 49
    invoke-virtual {p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mBackImgHeight:I

    .line 51
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mbApply:Z

    if-eqz v5, :cond_9

    .line 53
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->Object_avg_r:I

    .line 54
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->Object_avg_g:I

    .line 55
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->Object_avg_b:I

    .line 57
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mROI:Landroid/graphics/RectF;

    .line 59
    const/4 v3, 0x0

    .line 60
    .local v3, "x_center":I
    const/4 v4, 0x0

    .line 61
    .local v4, "y_center":I
    invoke-virtual {p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    div-int/lit8 v3, v5, 0x2

    .line 62
    invoke-virtual {p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    div-int/lit8 v4, v5, 0x2

    .line 67
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 68
    .local v1, "ObjectWidth":I
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 70
    .local v0, "ObjectHeight":I
    iget-object v5, p4, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectBitmap:Landroid/graphics/Bitmap;

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 71
    iget v5, p4, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgR:I

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->Object_avg_r:I

    .line 72
    iget v5, p4, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgG:I

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->Object_avg_g:I

    .line 73
    iget v5, p4, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$CopyToObjectImage;->objectAvgB:I

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->Object_avg_b:I

    .line 74
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 75
    .local v2, "roi":Landroid/graphics/Rect;
    div-int/lit8 v5, v1, 0x2

    sub-int v5, v3, v5

    iput v5, v2, Landroid/graphics/Rect;->left:I

    .line 76
    iget v5, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v1

    add-int/lit8 v5, v5, -0x1

    iput v5, v2, Landroid/graphics/Rect;->right:I

    .line 77
    div-int/lit8 v5, v0, 0x2

    sub-int v5, v4, v5

    iput v5, v2, Landroid/graphics/Rect;->top:I

    .line 78
    iget v5, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    iput v5, v2, Landroid/graphics/Rect;->bottom:I

    .line 79
    iget v5, v2, Landroid/graphics/Rect;->left:I

    if-gez v5, :cond_5

    iput v7, v2, Landroid/graphics/Rect;->left:I

    .line 80
    :cond_5
    iget v5, v2, Landroid/graphics/Rect;->right:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mBackImgWidth:I

    if-lt v5, v6, :cond_6

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mBackImgWidth:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v2, Landroid/graphics/Rect;->right:I

    .line 81
    :cond_6
    iget v5, v2, Landroid/graphics/Rect;->top:I

    if-gez v5, :cond_7

    iput v7, v2, Landroid/graphics/Rect;->top:I

    .line 82
    :cond_7
    iget v5, v2, Landroid/graphics/Rect;->bottom:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mBackImgHeight:I

    if-lt v5, v6, :cond_8

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mBackImgHeight:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v2, Landroid/graphics/Rect;->bottom:I

    .line 83
    :cond_8
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->setRectRoi(Landroid/graphics/Rect;)V

    .line 89
    .end local v0    # "ObjectHeight":I
    .end local v1    # "ObjectWidth":I
    .end local v2    # "roi":Landroid/graphics/Rect;
    .end local v3    # "x_center":I
    .end local v4    # "y_center":I
    :cond_9
    return-void

    .line 46
    :cond_a
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mbApply:Z

    goto/16 :goto_0
.end method


# virtual methods
.method public EndMoveObject()V
    .locals 0

    .prologue
    .line 271
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->EndMoveObject()V

    .line 272
    return-void
.end method

.method public GetObjectBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->OrgObjectBmp:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public InitMoveObject(FF)I
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 261
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->InitMoveObject(FF)I

    move-result v0

    return v0
.end method

.method public StartMoveObject(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 266
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->StartMoveObject(FF)V

    .line 267
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 92
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->destroy()V

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 94
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mBackImgWidth:I

    .line 95
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mBackImgHeight:I

    .line 96
    return-void
.end method

.method public getAngle()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mAngle:I

    return v0
.end method

.method public getCenterPt()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 112
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public getDrawBdry()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getDrawCenterPt()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 105
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 106
    .local v0, "ret":Landroid/graphics/PointF;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingX()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingY()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 108
    return-object v0
.end method

.method public run_blending_org(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 37
    .param p1, "copyInfo"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "imgInfo"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 126
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mbApply:Z

    if-nez v5, :cond_0

    .line 257
    :goto_0
    return-void

    .line 135
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v23

    .line 136
    .local v23, "orgwidth":I
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v24

    .line 139
    .local v24, "orgheight":I
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v18

    .line 140
    .local v18, "orgimg":[I
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v22

    .line 141
    .local v22, "orgmask":[B
    const/high16 v32, 0x3f800000    # 1.0f

    .line 142
    .local v32, "scale":F
    if-nez p1, :cond_1

    .line 144
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v34

    .line 145
    .local v34, "src_orgwidth":I
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v33

    .line 147
    .local v33, "src_orgheight":I
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v5

    const/high16 v6, 0x40400000    # 3.0f

    mul-float/2addr v5, v6

    float-to-int v0, v5

    move/from16 v35, v0

    .line 148
    .local v35, "step":I
    new-instance v25, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mOriginalROI:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mOriginalROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    float-to-int v6, v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mOriginalROI:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->right:F

    float-to-int v9, v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mOriginalROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    float-to-int v10, v10

    move-object/from16 v0, v25

    invoke-direct {v0, v5, v6, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 149
    .local v25, "roi":Landroid/graphics/Rect;
    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move-object/from16 v3, v25

    move/from16 v4, v35

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->setObjectRegion([BIILandroid/graphics/Rect;I)V

    .line 150
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v6

    div-float v32, v5, v6

    .line 165
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mOriginalROI:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    float-to-int v15, v5

    .line 166
    .local v15, "destWidth":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mOriginalROI:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-int v0, v5

    move/from16 v16, v0

    .line 168
    .local v16, "destHeight":I
    new-instance v25, Landroid/graphics/Rect;

    .end local v25    # "roi":Landroid/graphics/Rect;
    invoke-direct/range {v25 .. v25}, Landroid/graphics/Rect;-><init>()V

    .line 170
    .restart local v25    # "roi":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt1:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt2:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt3:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt4:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    invoke-static {v6, v9}, Ljava/lang/Math;->min(FF)F

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    float-to-int v5, v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->left:I

    .line 171
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt1:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt2:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt3:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt4:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    invoke-static {v6, v9}, Ljava/lang/Math;->max(FF)F

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v5

    float-to-int v5, v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 172
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt1:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt2:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt3:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt4:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    invoke-static {v6, v9}, Ljava/lang/Math;->min(FF)F

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    float-to-int v5, v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 173
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt1:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt2:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt3:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mDestPt4:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    invoke-static {v6, v9}, Ljava/lang/Math;->max(FF)F

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v5

    float-to-int v5, v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 175
    move-object/from16 v0, v25

    iget v5, v0, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    mul-float v5, v5, v32

    float-to-int v5, v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->left:I

    .line 176
    const/4 v5, 0x0

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/Rect;->left:I

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->left:I

    .line 177
    add-int/lit8 v5, v34, -0x1

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/Rect;->left:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->left:I

    .line 179
    move-object/from16 v0, v25

    iget v5, v0, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    mul-float v5, v5, v32

    float-to-int v5, v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 180
    const/4 v5, 0x0

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/Rect;->top:I

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 181
    add-int/lit8 v5, v33, -0x1

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/Rect;->top:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 183
    move-object/from16 v0, v25

    iget v5, v0, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    mul-float v5, v5, v32

    float-to-int v5, v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 184
    const/4 v5, 0x0

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/Rect;->right:I

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 185
    add-int/lit8 v5, v34, -0x1

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/Rect;->right:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 187
    move-object/from16 v0, v25

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    mul-float v5, v5, v32

    float-to-int v5, v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 188
    const/4 v5, 0x0

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 189
    add-int/lit8 v5, v33, -0x1

    move-object/from16 v0, v25

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 191
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->width()I

    move-result v11

    .line 192
    .local v11, "TmpObjectWidth":I
    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Rect;->height()I

    move-result v12

    .line 194
    .local v12, "TmpObjectHeight":I
    mul-int v5, v11, v12

    new-array v7, v5, [I

    .line 195
    .local v7, "TmpObjectData":[I
    mul-int v5, v11, v12

    new-array v8, v5, [B

    .line 196
    .local v8, "TmpObjectMask":[B
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v6

    .line 198
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v9

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v10

    .line 200
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mAngle:I

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v14

    .line 196
    invoke-static/range {v5 .. v16}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runRotateN([I[B[I[BIIIIILandroid/graphics/Rect;II)V

    .line 203
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->bSeamless:Z

    if-nez v5, :cond_2

    move-object/from16 v17, v7

    move-object/from16 v19, v8

    move-object/from16 v20, v18

    move/from16 v21, v11

    move/from16 v22, v12

    .line 204
    invoke-static/range {v17 .. v25}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->pasteObject([I[I[B[IIIIILandroid/graphics/Rect;)V

    .line 211
    .end local v22    # "orgmask":[B
    :goto_2
    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([III)V

    .line 212
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v22

    .line 214
    .restart local v22    # "orgmask":[B
    const/4 v5, 0x0

    move-object/from16 v0, v22

    invoke-static {v0, v5}, Ljava/util/Arrays;->fill([BB)V

    .line 216
    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v36, v0

    .line 217
    .local v36, "top":I
    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v31, v0

    .line 218
    .local v31, "left":I
    move/from16 v0, v23

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 219
    const/4 v5, 0x0

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 220
    move/from16 v0, v24

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 221
    const/4 v5, 0x0

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 223
    const/16 v30, 0x0

    .local v30, "j":I
    :goto_3
    move/from16 v0, v30

    if-lt v0, v12, :cond_3

    .line 238
    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setOriginalMaskRoi(Landroid/graphics/Rect;)V

    .line 239
    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setCopyToDrawCanvasRoi(Landroid/graphics/Rect;)V

    .line 241
    new-instance v21, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;

    invoke-direct/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;-><init>()V

    .line 242
    .local v21, "c":Lcom/sec/android/mimage/photoretouching/Core/ChainContour;
    move-object/from16 v0, v21

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->init(II)V

    .line 247
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawPathList()Landroid/graphics/Path;

    move-result-object v26

    .line 248
    const/16 v27, 0x0

    .line 243
    invoke-virtual/range {v21 .. v27}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->contourTrace([BIILandroid/graphics/Rect;Landroid/graphics/Path;Z)V

    .line 249
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/Core/ChainContour;->destroy()V

    .line 250
    const/16 v21, 0x0

    .line 251
    const/4 v7, 0x0

    .line 252
    const/4 v8, 0x0

    .line 253
    const/16 v25, 0x0

    .line 255
    const/16 v18, 0x0

    .line 256
    const/16 v22, 0x0

    .line 257
    goto/16 :goto_0

    .line 154
    .end local v7    # "TmpObjectData":[I
    .end local v8    # "TmpObjectMask":[B
    .end local v11    # "TmpObjectWidth":I
    .end local v12    # "TmpObjectHeight":I
    .end local v15    # "destWidth":I
    .end local v16    # "destHeight":I
    .end local v21    # "c":Lcom/sec/android/mimage/photoretouching/Core/ChainContour;
    .end local v25    # "roi":Landroid/graphics/Rect;
    .end local v30    # "j":I
    .end local v31    # "left":I
    .end local v33    # "src_orgheight":I
    .end local v34    # "src_orgwidth":I
    .end local v35    # "step":I
    .end local v36    # "top":I
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v18

    .line 155
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v22

    .line 156
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v34

    .restart local v34    # "src_orgwidth":I
    move/from16 v23, v34

    .line 157
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v33

    .restart local v33    # "src_orgheight":I
    move/from16 v24, v33

    .line 159
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v5

    const/high16 v6, 0x40400000    # 3.0f

    mul-float/2addr v5, v6

    float-to-int v0, v5

    move/from16 v35, v0

    .line 160
    .restart local v35    # "step":I
    new-instance v25, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mOriginalROI:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mOriginalROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    float-to-int v6, v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mOriginalROI:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->right:F

    float-to-int v9, v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->mOriginalROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    float-to-int v10, v10

    move-object/from16 v0, v25

    invoke-direct {v0, v5, v6, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 161
    .restart local v25    # "roi":Landroid/graphics/Rect;
    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move-object/from16 v3, v25

    move/from16 v4, v35

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->setObjectRegion([BIILandroid/graphics/Rect;I)V

    .line 162
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v6

    div-float v32, v5, v6

    goto/16 :goto_1

    .line 209
    .restart local v7    # "TmpObjectData":[I
    .restart local v8    # "TmpObjectMask":[B
    .restart local v11    # "TmpObjectWidth":I
    .restart local v12    # "TmpObjectHeight":I
    .restart local v15    # "destWidth":I
    .restart local v16    # "destHeight":I
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->Object_avg_r:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->Object_avg_g:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->Object_avg_b:I

    move/from16 v28, v0

    move-object/from16 v17, v7

    move-object/from16 v19, v8

    move-object/from16 v20, v18

    move/from16 v21, v11

    move/from16 v22, v12

    .line 207
    invoke-static/range {v17 .. v28}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runCopyPaste([I[I[B[IIIIILandroid/graphics/Rect;III)V

    goto/16 :goto_2

    .line 225
    .restart local v30    # "j":I
    .restart local v31    # "left":I
    .restart local v36    # "top":I
    :cond_3
    const/16 v29, 0x0

    .local v29, "i":I
    :goto_4
    move/from16 v0, v29

    if-lt v0, v11, :cond_4

    .line 223
    add-int/lit8 v30, v30, 0x1

    goto/16 :goto_3

    .line 227
    :cond_4
    mul-int v5, v30, v11

    add-int v5, v5, v29

    aget-byte v5, v8, v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    .line 229
    add-int v5, v30, v36

    mul-int v5, v5, v23

    add-int v6, v29, v31

    add-int/2addr v5, v6

    const/4 v6, 0x1

    aput-byte v6, v22, v5

    .line 231
    move-object/from16 v0, v25

    iget v5, v0, Landroid/graphics/Rect;->left:I

    add-int v6, v29, v31

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->left:I

    .line 232
    move-object/from16 v0, v25

    iget v5, v0, Landroid/graphics/Rect;->right:I

    add-int v6, v29, v31

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->right:I

    .line 233
    move-object/from16 v0, v25

    iget v5, v0, Landroid/graphics/Rect;->top:I

    add-int v6, v30, v36

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 234
    move-object/from16 v0, v25

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    add-int v6, v30, v36

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, v25

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 225
    :cond_5
    add-int/lit8 v29, v29, 0x1

    goto :goto_4
.end method
