.class Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$10;
.super Ljava/lang/Object;
.source "NormalView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->initButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    .line 449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 478
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 453
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 455
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mIsMinimum:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$29(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 457
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0600c4

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 467
    :cond_0
    :goto_0
    return-void

    .line 461
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(Landroid/view/View;Z)V

    .line 463
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$10;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x11000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 472
    return-void
.end method
