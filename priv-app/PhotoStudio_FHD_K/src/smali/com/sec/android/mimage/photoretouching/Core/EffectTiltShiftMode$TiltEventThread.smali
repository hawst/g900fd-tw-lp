.class Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;
.super Ljava/lang/Thread;
.source "EffectTiltShiftMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TiltEventThread"
.end annotation


# instance fields
.field isExit:Z

.field isWait:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)V
    .locals 1

    .prologue
    .line 415
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 413
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->isExit:Z

    .line 414
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->isWait:Z

    .line 416
    return-void
.end method

.method private workWait()V
    .locals 4

    .prologue
    .line 462
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->isWait:Z

    if-eqz v1, :cond_0

    .line 465
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 467
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    .line 465
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 473
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->isExit:Z

    if-eqz v1, :cond_1

    .line 487
    :cond_0
    :goto_1
    return-void

    .line 465
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 469
    :catch_0
    move-exception v0

    .line 471
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 481
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    const-wide/16 v2, 0xfa

    :try_start_4
    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 482
    :catch_1
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 419
    const-string v0, "TiltEventThread start"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 420
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isTSWorking:Z

    .line 422
    :cond_0
    const-string v0, "TiltEventThread start sleep"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 424
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->workWait()V

    .line 425
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->isExit:Z

    if-eqz v0, :cond_1

    .line 427
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltEventThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

    .line 445
    :goto_0
    return-void

    .line 432
    :cond_1
    const-string v0, "TiltEventThread end sleep"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 433
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTSTouchState:I

    if-eq v0, v2, :cond_3

    .line 434
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isConfigurationChange:Z

    if-nez v0, :cond_2

    .line 435
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # invokes: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->changeTiltShiftBuffer(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$1(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;Z)V

    .line 436
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->isTSWorking:Z

    .line 437
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->mTiltShiftInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;

    move-result-object v0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltShiftInfo;->mTiltEventThread:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;

    .line 438
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 440
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;->access$2(Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->invalidateWithThread()V

    .line 444
    :cond_3
    const-string v0, "TiltEventThread end"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public workResume()V
    .locals 1

    .prologue
    .line 448
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->isWait:Z

    .line 449
    monitor-enter p0

    .line 450
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 449
    monitor-exit p0

    .line 452
    return-void

    .line 449
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public workStop()V
    .locals 1

    .prologue
    .line 455
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectTiltShiftMode$TiltEventThread;->isExit:Z

    .line 456
    monitor-enter p0

    .line 457
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 456
    monitor-exit p0

    .line 459
    return-void

    .line 456
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
