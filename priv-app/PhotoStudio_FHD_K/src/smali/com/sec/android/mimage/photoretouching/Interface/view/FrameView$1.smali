.class Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$1;
.super Ljava/lang/Object;
.source "FrameView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnFrameCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setFrame(I)Z
    .locals 6
    .param p1, "resId"    # I

    .prologue
    const/4 v0, 0x1

    .line 169
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mTime:J
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x5dc

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 170
    const/4 v0, 0x0

    .line 175
    :goto_0
    return v0

    .line 171
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableCancel()V

    .line 172
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->setFrameMode(ILandroid/graphics/Rect;)V

    .line 173
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->isFrameApplied:Z

    .line 174
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;J)V

    goto :goto_0
.end method
