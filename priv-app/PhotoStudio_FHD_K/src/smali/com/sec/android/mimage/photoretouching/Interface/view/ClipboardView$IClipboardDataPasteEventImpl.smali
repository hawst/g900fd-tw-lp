.class Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;
.super Ljava/lang/Object;
.source "ClipboardView.java"

# interfaces
.implements Landroid/sec/clipboard/IClipboardDataPasteEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IClipboardDataPasteEventImpl"
.end annotation


# instance fields
.field private final mBinder:Landroid/sec/clipboard/IClipboardDataPasteEvent$Stub;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V
    .locals 1

    .prologue
    .line 779
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 780
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;->mBinder:Landroid/sec/clipboard/IClipboardDataPasteEvent$Stub;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;)V
    .locals 0

    .prologue
    .line 779
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 797
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;->mBinder:Landroid/sec/clipboard/IClipboardDataPasteEvent$Stub;

    return-object v0
.end method

.method public onClipboardDataPaste(Landroid/sec/clipboard/data/ClipboardData;)V
    .locals 3
    .param p1, "data"    # Landroid/sec/clipboard/data/ClipboardData;

    .prologue
    .line 788
    move-object v0, p1

    check-cast v0, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;

    .line 789
    .local v0, "bmp":Landroid/sec/clipboard/data/list/ClipboardDataBitmap;
    invoke-virtual {v0}, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;->GetBitmapPath()Ljava/lang/String;

    move-result-object v1

    .line 791
    .local v1, "path":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->setClipboardBitmap(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;Ljava/lang/String;)V

    .line 793
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView$IClipboardDataPasteEventImpl;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->closeClipboardDialog()V
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/ClipboardView;)V

    .line 794
    return-void
.end method
