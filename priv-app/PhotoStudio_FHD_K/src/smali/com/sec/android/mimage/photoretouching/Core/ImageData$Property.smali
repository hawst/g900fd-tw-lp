.class public Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;
.super Ljava/lang/Object;
.source "ImageData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/ImageData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Property"
.end annotation


# instance fields
.field private atLeastSaved:Z

.field private isEdited:Z

.field private isEnableDecoration:Z

.field private isFromPersonalPage:Z

.field private isSaved:Z

.field private mCopyToDrawCanvasRoi:Landroid/graphics/Rect;

.field private mDrawCanvasRoi:Landroid/graphics/Rect;

.field private mDrawPathForSelectedArea:Landroid/graphics/Path;

.field private mFilePath:Ljava/lang/String;

.field private mInitialPreviewInputBuff:[I

.field private mMaxRatio:F

.field private mMinRatio:F

.field private mOrgLandSupportMatrix:Landroid/graphics/Matrix;

.field private mOrgLandViewTransformMatrix:Landroid/graphics/Matrix;

.field private mOrgPortraitSupportMatrix:Landroid/graphics/Matrix;

.field private mOrgPortraitViewTransformMatrix:Landroid/graphics/Matrix;

.field private mOrgPreviewHeight:I

.field private mOrgPreviewWidth:I

.field private mOrigianlMaskBuff:[B

.field private mOriginalBitmap:Landroid/graphics/Bitmap;

.field private mOriginalBuffer:[I

.field private mOriginalHeight:I

.field private mOriginalMaskRoi:Landroid/graphics/Rect;

.field private mOriginalPaddingX:F

.field private mOriginalPaddingY:F

.field private mOriginalToPreviewMatrix:Landroid/graphics/Matrix;

.field private mOriginalToPreviewRatio:F

.field private mOriginalWidth:I

.field private mPathBitmap:Landroid/graphics/Bitmap;

.field private mPersonalPagePath:Ljava/lang/String;

.field private mPreviewBitmap:Landroid/graphics/Bitmap;

.field private mPreviewHeight:I

.field private mPreviewInputBuff:[I

.field private mPreviewMask:[B

.field private mPreviewMaskRoi:Landroid/graphics/Rect;

.field private mPreviewOutBuff:[I

.field private mPreviewWidth:I

.field private mPrivateSaveFolder:Ljava/lang/String;

.field private mSupportMatrix:Landroid/graphics/Matrix;

.field private mUri:Landroid/net/Uri;

.field private mViewHeight:I

.field private mViewTransformMatrix:Landroid/graphics/Matrix;

.field private mViewWidth:I

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2140
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 2139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2075
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mUri:Landroid/net/Uri;

    .line 2076
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mFilePath:Ljava/lang/String;

    .line 2078
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;

    .line 2079
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBuffer:[I

    .line 2080
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrigianlMaskBuff:[B

    .line 2082
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I

    .line 2083
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMask:[B

    .line 2084
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I

    .line 2085
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mInitialPreviewInputBuff:[I

    .line 2087
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPathBitmap:Landroid/graphics/Bitmap;

    .line 2088
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewBitmap:Landroid/graphics/Bitmap;

    .line 2091
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I

    .line 2092
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I

    .line 2094
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewWidth:I

    .line 2095
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewHeight:I

    .line 2097
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I

    .line 2098
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I

    .line 2100
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPreviewWidth:I

    .line 2101
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPreviewHeight:I

    .line 2103
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawCanvasRoi:Landroid/graphics/Rect;

    .line 2104
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mCopyToDrawCanvasRoi:Landroid/graphics/Rect;

    .line 2105
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMaskRoi:Landroid/graphics/Rect;

    .line 2106
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalMaskRoi:Landroid/graphics/Rect;

    .line 2108
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;

    .line 2109
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;

    .line 2110
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mSupportMatrix:Landroid/graphics/Matrix;

    .line 2112
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitViewTransformMatrix:Landroid/graphics/Matrix;

    .line 2113
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandViewTransformMatrix:Landroid/graphics/Matrix;

    .line 2114
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitSupportMatrix:Landroid/graphics/Matrix;

    .line 2115
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandSupportMatrix:Landroid/graphics/Matrix;

    .line 2117
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mMaxRatio:F

    .line 2118
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mMinRatio:F

    .line 2121
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewRatio:F

    .line 2124
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalPaddingX:F

    .line 2125
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalPaddingY:F

    .line 2127
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;

    .line 2129
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isFromPersonalPage:Z

    .line 2130
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isEdited:Z

    .line 2131
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isSaved:Z

    .line 2132
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->atLeastSaved:Z

    .line 2134
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPersonalPagePath:Ljava/lang/String;

    .line 2135
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPrivateSaveFolder:Ljava/lang/String;

    .line 2137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isEnableDecoration:Z

    .line 2141
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawCanvasRoi:Landroid/graphics/Rect;

    .line 2142
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMaskRoi:Landroid/graphics/Rect;

    .line 2143
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalMaskRoi:Landroid/graphics/Rect;

    .line 2144
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;

    .line 2145
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;

    .line 2146
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mSupportMatrix:Landroid/graphics/Matrix;

    .line 2147
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;

    .line 2149
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitViewTransformMatrix:Landroid/graphics/Matrix;

    .line 2150
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitSupportMatrix:Landroid/graphics/Matrix;

    .line 2151
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandViewTransformMatrix:Landroid/graphics/Matrix;

    .line 2152
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandSupportMatrix:Landroid/graphics/Matrix;

    .line 2153
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I
    .locals 1

    .prologue
    .line 2091
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I
    .locals 1

    .prologue
    .line 2092
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I

    return v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2078
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I
    .locals 1

    .prologue
    .line 2082
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2088
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 2088
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V
    .locals 0

    .prologue
    .line 2083
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMask:[B

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V
    .locals 0

    .prologue
    .line 2084
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I
    .locals 1

    .prologue
    .line 2084
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 2078
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 2108
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V
    .locals 0

    .prologue
    .line 2121
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewRatio:F

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V
    .locals 0

    .prologue
    .line 2117
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mMaxRatio:F

    return-void
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2087
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPathBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I
    .locals 1

    .prologue
    .line 2085
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mInitialPreviewInputBuff:[I

    return-object v0
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V
    .locals 0

    .prologue
    .line 2100
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPreviewWidth:I

    return-void
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V
    .locals 0

    .prologue
    .line 2101
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPreviewHeight:I

    return-void
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I
    .locals 1

    .prologue
    .line 2094
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewWidth:I

    return v0
.end method

.method static synthetic access$25(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I
    .locals 1

    .prologue
    .line 2095
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewHeight:I

    return v0
.end method

.method static synthetic access$26(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 2110
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mSupportMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$27(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V
    .locals 0

    .prologue
    .line 2094
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewWidth:I

    return-void
.end method

.method static synthetic access$28(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V
    .locals 0

    .prologue
    .line 2095
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewHeight:I

    return-void
.end method

.method static synthetic access$29(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V
    .locals 0

    .prologue
    .line 2124
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalPaddingX:F

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V
    .locals 0

    .prologue
    .line 2118
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mMinRatio:F

    return-void
.end method

.method static synthetic access$30(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V
    .locals 0

    .prologue
    .line 2125
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalPaddingY:F

    return-void
.end method

.method static synthetic access$31(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 2087
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPathBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$32(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 2109
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$33(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V
    .locals 0

    .prologue
    .line 2137
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isEnableDecoration:Z

    return-void
.end method

.method static synthetic access$34(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 2114
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitSupportMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$35(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 2112
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitViewTransformMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$36(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 2115
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandSupportMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$37(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 2113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandViewTransformMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$38(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I
    .locals 1

    .prologue
    .line 2079
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBuffer:[I

    return-object v0
.end method

.method static synthetic access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V
    .locals 0

    .prologue
    .line 2079
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBuffer:[I

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V
    .locals 0

    .prologue
    .line 2097
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I

    return-void
.end method

.method static synthetic access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V
    .locals 0

    .prologue
    .line 2080
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrigianlMaskBuff:[B

    return-void
.end method

.method static synthetic access$41(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2075
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mUri:Landroid/net/Uri;

    return-void
.end method

.method static synthetic access$42(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Matrix;)V
    .locals 0

    .prologue
    .line 2108
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;

    return-void
.end method

.method static synthetic access$43(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 2127
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;

    return-object v0
.end method

.method static synthetic access$44(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V
    .locals 0

    .prologue
    .line 2129
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isFromPersonalPage:Z

    return-void
.end method

.method static synthetic access$45(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2134
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPersonalPagePath:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$46(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2134
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPersonalPagePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$47(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2135
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPrivateSaveFolder:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$48(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Z
    .locals 1

    .prologue
    .line 2129
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isFromPersonalPage:Z

    return v0
.end method

.method static synthetic access$49(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2135
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPrivateSaveFolder:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V
    .locals 0

    .prologue
    .line 2098
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I

    return-void
.end method

.method static synthetic access$50(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V
    .locals 0

    .prologue
    .line 2130
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isEdited:Z

    return-void
.end method

.method static synthetic access$51(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2076
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mFilePath:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$52(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V
    .locals 0

    .prologue
    .line 2091
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I

    return-void
.end method

.method static synthetic access$53(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V
    .locals 0

    .prologue
    .line 2092
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I

    return-void
.end method

.method static synthetic access$54(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 2106
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalMaskRoi:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$55(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[B
    .locals 1

    .prologue
    .line 2080
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrigianlMaskBuff:[B

    return-object v0
.end method

.method static synthetic access$56(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2075
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$57(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V
    .locals 0

    .prologue
    .line 2131
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isSaved:Z

    return-void
.end method

.method static synthetic access$58(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V
    .locals 0

    .prologue
    .line 2132
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->atLeastSaved:Z

    return-void
.end method

.method static synthetic access$59(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 2105
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMaskRoi:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I
    .locals 1

    .prologue
    .line 2097
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I

    return v0
.end method

.method static synthetic access$60(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 2104
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mCopyToDrawCanvasRoi:Landroid/graphics/Rect;

    return-void
.end method

.method static synthetic access$61(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Z
    .locals 1

    .prologue
    .line 2130
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isEdited:Z

    return v0
.end method

.method static synthetic access$62(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Z
    .locals 1

    .prologue
    .line 2131
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isSaved:Z

    return v0
.end method

.method static synthetic access$63(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Z
    .locals 1

    .prologue
    .line 2132
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->atLeastSaved:Z

    return v0
.end method

.method static synthetic access$64(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2076
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$65(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[B
    .locals 1

    .prologue
    .line 2083
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMask:[B

    return-object v0
.end method

.method static synthetic access$66(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F
    .locals 1

    .prologue
    .line 2117
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mMaxRatio:F

    return v0
.end method

.method static synthetic access$67(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F
    .locals 1

    .prologue
    .line 2118
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mMinRatio:F

    return v0
.end method

.method static synthetic access$68(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F
    .locals 1

    .prologue
    .line 2124
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalPaddingX:F

    return v0
.end method

.method static synthetic access$69(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F
    .locals 1

    .prologue
    .line 2125
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalPaddingY:F

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I
    .locals 1

    .prologue
    .line 2098
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I

    return v0
.end method

.method static synthetic access$70(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 2104
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mCopyToDrawCanvasRoi:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$71(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 2103
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawCanvasRoi:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$72(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F
    .locals 1

    .prologue
    .line 2121
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewRatio:F

    return v0
.end method

.method static synthetic access$73(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I
    .locals 1

    .prologue
    .line 2100
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPreviewWidth:I

    return v0
.end method

.method static synthetic access$74(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I
    .locals 1

    .prologue
    .line 2101
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPreviewHeight:I

    return v0
.end method

.method static synthetic access$75(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Z
    .locals 1

    .prologue
    .line 2137
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isEnableDecoration:Z

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V
    .locals 0

    .prologue
    .line 2082
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V
    .locals 0

    .prologue
    .line 2085
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mInitialPreviewInputBuff:[I

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2156
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawCanvasRoi:Landroid/graphics/Rect;

    .line 2157
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMaskRoi:Landroid/graphics/Rect;

    .line 2158
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalMaskRoi:Landroid/graphics/Rect;

    .line 2159
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I

    .line 2160
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mInitialPreviewInputBuff:[I

    .line 2161
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMask:[B

    .line 2162
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I

    .line 2163
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;

    .line 2164
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;

    .line 2165
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mSupportMatrix:Landroid/graphics/Matrix;

    .line 2166
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitViewTransformMatrix:Landroid/graphics/Matrix;

    .line 2167
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitSupportMatrix:Landroid/graphics/Matrix;

    .line 2168
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandViewTransformMatrix:Landroid/graphics/Matrix;

    .line 2169
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandSupportMatrix:Landroid/graphics/Matrix;

    .line 2170
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Ljava/util/HashMap;)V

    .line 2171
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;

    if-eqz v0, :cond_0

    .line 2173
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 2174
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;

    .line 2176
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;

    .line 2177
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPathBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPathBitmap:Landroid/graphics/Bitmap;

    .line 2178
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewBitmap:Landroid/graphics/Bitmap;

    .line 2179
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->this$0:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setFreeOriginalData()V

    .line 2181
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mFilePath:Ljava/lang/String;

    .line 2182
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPersonalPagePath:Ljava/lang/String;

    .line 2183
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 2184
    return-void
.end method

.method public getOriginalHeight()I
    .locals 1

    .prologue
    .line 2209
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I

    return v0
.end method

.method public getOriginalWidth()I
    .locals 1

    .prologue
    .line 2213
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I

    return v0
.end method

.method public getViewHeight()I
    .locals 1

    .prologue
    .line 2221
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewHeight:I

    return v0
.end method

.method public getViewWidth()I
    .locals 1

    .prologue
    .line 2217
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewWidth:I

    return v0
.end method

.method public setOrigianlBuffer([I)V
    .locals 0
    .param p1, "buffer"    # [I

    .prologue
    .line 2205
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBuffer:[I

    .line 2206
    return-void
.end method

.method public setOriginalSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 2195
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I

    .line 2196
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I

    .line 2197
    return-void
.end method

.method public setOriginalToPreviewMatrix(Landroid/graphics/Matrix;)V
    .locals 1
    .param p1, "src"    # Landroid/graphics/Matrix;

    .prologue
    .line 2191
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 2192
    return-void
.end method

.method public setViewSize(II)V
    .locals 0
    .param p1, "viewWidth"    # I
    .param p2, "viewHeight"    # I

    .prologue
    .line 2200
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewWidth:I

    .line 2201
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewHeight:I

    .line 2202
    return-void
.end method

.method public setViewTransformMatrix(Landroid/graphics/Matrix;)V
    .locals 1
    .param p1, "src"    # Landroid/graphics/Matrix;

    .prologue
    .line 2187
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 2188
    return-void
.end method
