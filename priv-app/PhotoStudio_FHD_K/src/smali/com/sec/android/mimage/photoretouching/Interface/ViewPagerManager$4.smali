.class Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;
.super Ljava/lang/Object;
.source "ViewPagerManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->setEffect(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

.field private final synthetic val$imageview:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

.field private final synthetic val$index:I


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;->val$imageview:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;->val$index:I

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;->val$imageview:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;->val$index:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;->val$imageview:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;->val$index:I

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$4;->val$imageview:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerImageView;->invalidate()V

    .line 209
    :cond_0
    return-void
.end method
