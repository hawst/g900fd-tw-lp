.class Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$6;
.super Ljava/lang/Object;
.source "PortraitView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->setMainBtnListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    .line 294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 323
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isVisibleTitle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mIsMinimum:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$26(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0600c4

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 306
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(Landroid/view/View;Z)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x16000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 317
    return-void
.end method
