.class public Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;
.super Landroid/view/SurfaceView;
.source "CollageAnimationView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;,
        Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;
    }
.end annotation


# static fields
.field public static final COLLAGE_ANIMATION_VIEW_ID:I = 0x40000001


# instance fields
.field private mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

.field private mAnimationObjectInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;

.field private mClearPaint:Landroid/graphics/Paint;

.field private mCollageAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;

.field private mFinishDraw:Z

.field private mHolder:Landroid/view/SurfaceHolder;

.field private mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

.field private mIsLoop:Z

.field private mThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 115
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    .line 358
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mFinishDraw:Z

    .line 359
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mIsLoop:Z

    .line 360
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mThread:Ljava/lang/Thread;

    .line 361
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    .line 362
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    .line 363
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationObjectInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;

    .line 364
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mCollageAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;

    .line 365
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mClearPaint:Landroid/graphics/Paint;

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    .line 36
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    const/4 v1, -0x2

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 39
    const v0, 0x40000001    # 2.0000002f

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->setId(I)V

    .line 40
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)Z
    .locals 1

    .prologue
    .line 359
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mIsLoop:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->drawCanvas(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private createThread()V
    .locals 2

    .prologue
    .line 243
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$2;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mThread:Ljava/lang/Thread;

    .line 265
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 266
    return-void
.end method

.method private drawCanvas(Landroid/graphics/Canvas;)V
    .locals 27
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 145
    const-string v21, "start drawCanvas"

    invoke-static/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mAnimationObjectList:Ljava/util/ArrayList;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)Ljava/util/ArrayList;

    move-result-object v21

    if-nez v21, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 156
    .local v6, "currentTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mStartTime:J
    invoke-static/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)J

    move-result-wide v22

    sub-long v8, v6, v22

    .line 159
    .local v8, "diff":J
    long-to-float v0, v8

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mDuration:J
    invoke-static/range {v22 .. v22}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->access$4(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)J

    move-result-wide v22

    move-wide/from16 v0, v22

    long-to-float v0, v0

    move/from16 v22, v0

    div-float v12, v21, v22

    .line 160
    .local v12, "input":F
    const/high16 v21, 0x3f800000    # 1.0f

    cmpl-float v21, v12, v21

    if-lez v21, :cond_2

    .line 161
    const/high16 v12, 0x3f800000    # 1.0f

    .line 163
    :cond_2
    new-instance v14, Landroid/graphics/RectF;

    invoke-direct {v14}, Landroid/graphics/RectF;-><init>()V

    .line 164
    .local v14, "interpolatedDrawRect":Landroid/graphics/RectF;
    if-eqz p1, :cond_3

    .line 165
    const/16 v21, 0x0

    sget-object v22, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, p1

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 166
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mCollageAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;->drawBackground(Landroid/graphics/Canvas;)V

    .line 167
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mAnimationObjectList:Ljava/util/ArrayList;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)Ljava/util/ArrayList;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v16

    move/from16 v1, v21

    if-lt v0, v1, :cond_8

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mDuration:J
    invoke-static/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->access$4(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)J

    move-result-wide v22

    cmp-long v21, v8, v22

    if-lez v21, :cond_6

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mDuration:J
    invoke-static/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->access$4(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)J

    move-result-wide v22

    const-wide/16 v24, 0x12c

    add-long v22, v22, v24

    cmp-long v21, v8, v22

    if-lez v21, :cond_4

    .line 223
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->finishAnimation()V

    .line 225
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mFinishDraw:Z

    move/from16 v21, v0

    if-nez v21, :cond_6

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mCollageAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;

    move-object/from16 v21, v0

    if-eqz v21, :cond_5

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mCollageAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-interface/range {v21 .. v22}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;->endAnimation(Z)V

    .line 231
    :cond_5
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mFinishDraw:Z

    .line 234
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mStart:Z
    invoke-static/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->access$5(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)Z

    move-result v21

    if-nez v21, :cond_7

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mCollageAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;->startAnimation()V

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-static/range {v21 .. v22}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->access$6(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;Z)V

    .line 239
    :cond_7
    const-string v21, "end drawCanvas"

    invoke-static/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 168
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mAnimationObjectList:Ljava/util/ArrayList;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)Ljava/util/ArrayList;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;

    .line 169
    .local v17, "oInfo":Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->getObject()Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    move-result-object v18

    .line 170
    .local v18, "object":Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->getPreviousDrawRect()Landroid/graphics/RectF;

    move-result-object v19

    .line 172
    .local v19, "previousDrawRect":Landroid/graphics/RectF;
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->getInterpolator()Lcom/sec/android/easing/SineIn33;

    move-result-object v15

    .line 173
    .local v15, "interpolator":Landroid/view/animation/Interpolator;
    if-eqz v15, :cond_0

    .line 177
    const/4 v13, 0x0

    .line 180
    .local v13, "interpolateValue":F
    :try_start_0
    invoke-interface {v15, v12}, Landroid/view/animation/Interpolator;->getInterpolation(F)F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v13

    .line 190
    :goto_2
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->getStX()F

    move-result v21

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->getDiffCenterX()F

    move-result v22

    mul-float v22, v22, v13

    add-float v21, v21, v22

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->getStY()F

    move-result v22

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->getDiffCenterY()F

    move-result v23

    mul-float v23, v23, v13

    add-float v22, v22, v23

    move-object/from16 v0, v18

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->StartMoveObject(FF)V

    .line 192
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->getDiffWidth()F

    move-result v21

    mul-float v21, v21, v13

    const/high16 v22, 0x3f000000    # 0.5f

    mul-float v11, v21, v22

    .line 193
    .local v11, "halfDiffWidth":F
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->getDiffHeight()F

    move-result v21

    mul-float v21, v21, v13

    const/high16 v22, 0x3f000000    # 0.5f

    mul-float v10, v21, v22

    .line 194
    .local v10, "halfDiffHeight":F
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v21, v0

    sub-float v21, v21, v11

    .line 195
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v22, v0

    sub-float v22, v22, v10

    .line 196
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v23, v0

    add-float v23, v23, v11

    .line 197
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v24, v0

    add-float v24, v24, v10

    .line 194
    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mCollageAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;

    move-object/from16 v21, v0

    if-eqz v21, :cond_9

    .line 200
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->getSrcRect()Landroid/graphics/RectF;

    move-result-object v20

    .line 201
    .local v20, "srcRect":Landroid/graphics/RectF;
    if-eqz v20, :cond_9

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mCollageAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;

    move-object/from16 v21, v0

    new-instance v22, Landroid/graphics/Rect;

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v23, v0

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    .line 203
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    .line 204
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v25, v0

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    .line 205
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v26, v0

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v26, v0

    invoke-direct/range {v22 .. v26}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 202
    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-interface {v0, v1, v14}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;->calculateRectSize(Landroid/graphics/Rect;Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v4

    .line 207
    .local v4, "calculatedRect":Landroid/graphics/Rect;
    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v4}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->changeRectSize(Landroid/graphics/RectF;Landroid/graphics/Rect;)V

    .line 214
    .end local v4    # "calculatedRect":Landroid/graphics/Rect;
    .end local v20    # "srcRect":Landroid/graphics/RectF;
    :cond_9
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->getAlphaAnimation()Z

    move-result v21

    if-eqz v21, :cond_a

    .line 215
    const/high16 v21, 0x425c0000    # 55.0f

    mul-float v21, v21, v13

    const/high16 v22, 0x43480000    # 200.0f

    add-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->drawObject(Landroid/graphics/Canvas;I)V

    .line 167
    :goto_3
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 182
    .end local v10    # "halfDiffHeight":F
    .end local v11    # "halfDiffWidth":F
    :catch_0
    move-exception v5

    .line 184
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 217
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v10    # "halfDiffHeight":F
    .restart local v11    # "halfDiffWidth":F
    :cond_a
    const/16 v21, 0xff

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->drawObject(Landroid/graphics/Canvas;I)V

    goto :goto_3
.end method

.method private finishAnimation()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mThread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mIsLoop:Z

    if-eqz v0, :cond_2

    .line 120
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mIsLoop:Z

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mThread:Ljava/lang/Thread;

    .line 123
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->free()V

    .line 126
    :cond_1
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$1;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->post(Ljava/lang/Runnable;)Z

    .line 141
    :cond_2
    return-void
.end method


# virtual methods
.method public clearCanvas()V
    .locals 4

    .prologue
    .line 75
    const/4 v0, 0x0

    .line 77
    .local v0, "canvas":Landroid/graphics/Canvas;
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 79
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 80
    if-eqz v0, :cond_0

    .line 82
    const/high16 v1, -0x1000000

    :try_start_1
    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 79
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86
    if-eqz v0, :cond_1

    .line 87
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    monitor-enter v2

    .line 88
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 87
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 92
    :cond_1
    return-void

    .line 79
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 85
    :catchall_1
    move-exception v1

    .line 86
    if-eqz v0, :cond_2

    .line 87
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    monitor-enter v2

    .line 88
    :try_start_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v3, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 87
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 91
    :cond_2
    throw v1

    .line 87
    :catchall_2
    move-exception v1

    :try_start_6
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v1

    :catchall_3
    move-exception v1

    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v1
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 44
    const-string v0, "destroy"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 45
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->finishAnimation()V

    .line 46
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mCollageAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mCollageAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;->endAnimation(Z)V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mCollageAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;

    .line 51
    :cond_0
    return-void
.end method

.method public runAnimation(IZLjava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;)V
    .locals 3
    .param p1, "duration"    # I
    .param p2, "alphaAnimation"    # Z
    .param p4, "imageView"    # Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;
    .param p5, "callback"    # Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;",
            ">;",
            "Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;",
            "Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 99
    .local p3, "objectInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;>;"
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mIsLoop:Z

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mClearPaint:Landroid/graphics/Paint;

    .line 102
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mClearPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 103
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mImageEditView:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    .line 104
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mCollageAnimationViewCallback:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;

    .line 105
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    .line 106
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->setDuration(I)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->setStartTime()V

    .line 108
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mAnimationInfo:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;

    invoke-static {v0, p3}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->access$1(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;Ljava/util/ArrayList;)V

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mIsLoop:Z

    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mFinishDraw:Z

    .line 112
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->createThread()V

    .line 114
    :cond_0
    return-void
.end method

.method public runningAnimation()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mIsLoop:Z

    return v0
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 54
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 59
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 63
    const-string v1, "surfaceDestroyed"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->destroy()V

    .line 66
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;->mThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :goto_0
    return-void

    .line 69
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    goto :goto_0
.end method
