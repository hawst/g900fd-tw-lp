.class public Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "StickerAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;
    }
.end annotation


# instance fields
.field private FCL:Landroid/view/View$OnFocusChangeListener;

.field private final STICKER_LAND:I

.field private final STICKER_PORT:I

.field private mAssistantLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentFocusIndex:I

.field private mCurrentLinearLayout:Landroid/widget/LinearLayout;

.field private mCurrentRes:[I

.field private mDecoRes:[I

.field private mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

.field private mIconRes:[I

.field private mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

.field private mImageViewRes:[I

.field private mLifeRes:[I

.field private mNumThumbCount:I

.field private mPeopleRes:[I

.field private mStickerFrameLayout:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I
    .param p3, "layoutmanager"    # Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0xc

    const/4 v1, 0x0

    .line 271
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 22
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mContext:Landroid/content/Context;

    .line 23
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    .line 24
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    .line 25
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentFocusIndex:I

    .line 29
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->STICKER_LAND:I

    .line 30
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->STICKER_PORT:I

    .line 32
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    .line 45
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageViewRes:[I

    .line 48
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    .line 60
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mStickerFrameLayout:[I

    .line 63
    const/16 v0, 0x18

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    .line 90
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mPeopleRes:[I

    .line 92
    const/16 v0, 0x1e

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    .line 129
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mLifeRes:[I

    .line 131
    const/16 v0, 0x14

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    .line 154
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mDecoRes:[I

    .line 156
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mIconRes:[I

    .line 269
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    .line 383
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->FCL:Landroid/view/View$OnFocusChangeListener;

    .line 272
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mContext:Landroid/content/Context;

    .line 273
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mAssistantLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;

    .line 274
    packed-switch p2, :pswitch_data_0

    .line 281
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    .line 282
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->initImageData()V

    .line 284
    return-void

    .line 276
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mPeopleRes:[I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    goto :goto_0

    .line 277
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mLifeRes:[I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    goto :goto_0

    .line 278
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mDecoRes:[I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    goto :goto_0

    .line 279
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mIconRes:[I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    goto :goto_0

    .line 32
    :array_0
    .array-data 4
        0x7f090054
        0x7f090056
        0x7f090058
        0x7f09005a
        0x7f09005c
        0x7f09005e
        0x7f090060
        0x7f090062
        0x7f090064
        0x7f090066
        0x7f090068
        0x7f09006a
    .end array-data

    .line 48
    :array_1
    .array-data 4
        0x7f090053
        0x7f090055
        0x7f090057
        0x7f090059
        0x7f09005b
        0x7f09005d
        0x7f09005f
        0x7f090061
        0x7f090063
        0x7f090065
        0x7f090067
        0x7f090069
    .end array-data

    .line 63
    :array_2
    .array-data 4
        0x7f02036a
        0x7f02036b
        0x7f020369
        0x7f02036c
        0x7f02036d
        0x7f020368
        0x7f020368
        0x7f020540
        0x7f020541
        0x7f020542
        0x7f020543
        0x7f020544
        0x7f02052e
        0x7f02052f
        0x7f020530
        0x7f020531
        0x7f020532
        0x7f020533
        0x7f020545
        0x7f020546
        0x7f020547
        0x7f020548
        0x7f020549
        0x7f02054a
    .end array-data

    .line 92
    :array_3
    .array-data 4
        0x7f0201cc
        0x7f0201cd
        0x7f0201ce
        0x7f0201cf
        0x7f0201d0
        0x7f0201d1
        0x7f020513
        0x7f020514
        0x7f020515
        0x7f020516
        0x7f020517
        0x7f020518
        0x7f020519
        0x7f02051a
        0x7f02051b
        0x7f02051c
        0x7f020563
        0x7f020564
        0x7f020565
        0x7f020566
        0x7f020567
        0x7f020568
        0x7f020569
        0x7f02056a
        0x7f020594
        0x7f020595
        0x7f020596
        0x7f020597
        0x7f020598
        0x7f020599
    .end array-data

    .line 131
    :array_4
    .array-data 4
        0x7f020332
        0x7f020333
        0x7f020334
        0x7f020335
        0x7f020336
        0x7f020337
        0x7f020338
        0x7f020339
        0x7f02033a
        0x7f02033b
        0x7f020023
        0x7f020024
        0x7f020025
        0x7f020026
        0x7f020027
        0x7f020028
        0x7f020029
        0x7f02002a
        0x7f02002b
        0x7f02002c
    .end array-data

    .line 274
    :pswitch_data_0
    .packed-switch 0x1001
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I
    .param p3, "layoutmanager"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0xc

    const/4 v1, 0x0

    .line 286
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 22
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mContext:Landroid/content/Context;

    .line 23
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    .line 24
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    .line 25
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentFocusIndex:I

    .line 29
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->STICKER_LAND:I

    .line 30
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->STICKER_PORT:I

    .line 32
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    .line 45
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageViewRes:[I

    .line 48
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    .line 60
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mStickerFrameLayout:[I

    .line 63
    const/16 v0, 0x18

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    .line 90
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mPeopleRes:[I

    .line 92
    const/16 v0, 0x1e

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    .line 129
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mLifeRes:[I

    .line 131
    const/16 v0, 0x14

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    .line 154
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mDecoRes:[I

    .line 156
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mIconRes:[I

    .line 269
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    .line 383
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->FCL:Landroid/view/View$OnFocusChangeListener;

    .line 287
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mContext:Landroid/content/Context;

    .line 288
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 289
    packed-switch p2, :pswitch_data_0

    .line 296
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->initImageData()V

    .line 298
    return-void

    .line 291
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mPeopleRes:[I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    goto :goto_0

    .line 292
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mLifeRes:[I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    goto :goto_0

    .line 293
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mDecoRes:[I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    goto :goto_0

    .line 32
    nop

    :array_0
    .array-data 4
        0x7f090054
        0x7f090056
        0x7f090058
        0x7f09005a
        0x7f09005c
        0x7f09005e
        0x7f090060
        0x7f090062
        0x7f090064
        0x7f090066
        0x7f090068
        0x7f09006a
    .end array-data

    .line 48
    :array_1
    .array-data 4
        0x7f090053
        0x7f090055
        0x7f090057
        0x7f090059
        0x7f09005b
        0x7f09005d
        0x7f09005f
        0x7f090061
        0x7f090063
        0x7f090065
        0x7f090067
        0x7f090069
    .end array-data

    .line 63
    :array_2
    .array-data 4
        0x7f02036a
        0x7f02036b
        0x7f020369
        0x7f02036c
        0x7f02036d
        0x7f020368
        0x7f020368
        0x7f020540
        0x7f020541
        0x7f020542
        0x7f020543
        0x7f020544
        0x7f02052e
        0x7f02052f
        0x7f020530
        0x7f020531
        0x7f020532
        0x7f020533
        0x7f020545
        0x7f020546
        0x7f020547
        0x7f020548
        0x7f020549
        0x7f02054a
    .end array-data

    .line 92
    :array_3
    .array-data 4
        0x7f0201cc
        0x7f0201cd
        0x7f0201ce
        0x7f0201cf
        0x7f0201d0
        0x7f0201d1
        0x7f020513
        0x7f020514
        0x7f020515
        0x7f020516
        0x7f020517
        0x7f020518
        0x7f020519
        0x7f02051a
        0x7f02051b
        0x7f02051c
        0x7f020563
        0x7f020564
        0x7f020565
        0x7f020566
        0x7f020567
        0x7f020568
        0x7f020569
        0x7f02056a
        0x7f020594
        0x7f020595
        0x7f020596
        0x7f020597
        0x7f020598
        0x7f020599
    .end array-data

    .line 131
    :array_4
    .array-data 4
        0x7f020332
        0x7f020333
        0x7f020334
        0x7f020335
        0x7f020336
        0x7f020337
        0x7f020338
        0x7f020339
        0x7f02033a
        0x7f02033b
        0x7f020023
        0x7f020024
        0x7f020025
        0x7f020026
        0x7f020027
        0x7f020028
        0x7f020029
        0x7f02002a
        0x7f02002b
        0x7f02002c
    .end array-data

    .line 289
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;)[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;I)V
    .locals 0

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentFocusIndex:I

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 369
    const-string v1, "sticker adapter destroy"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 370
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 381
    return-void

    .line 372
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->imageView:Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;

    if-eqz v1, :cond_1

    .line 374
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->imageView:Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 375
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->imageView:Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;->destroy()V

    .line 376
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v1, v1, v0

    iput-object v2, v1, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->imageView:Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;

    .line 377
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v1, v1, v0

    iput-object v2, v1, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    .line 378
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aput-object v2, v1, v0

    .line 370
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 0
    .param p1, "collection"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "view"    # Ljava/lang/Object;

    .prologue
    .line 441
    return-void
.end method

.method public finishUpdate(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 485
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 465
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->getNumOfIcon()I

    move-result v1

    rem-int/2addr v0, v1

    if-nez v0, :cond_0

    .line 466
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->getNumOfIcon()I

    move-result v1

    div-int/2addr v0, v1

    .line 468
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->getNumOfIcon()I

    move-result v1

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getCurrentFocusIndex()I
    .locals 1

    .prologue
    .line 310
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentFocusIndex:I

    return v0
.end method

.method public getCurrentLinearLayout()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getImageData(I)Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 318
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    if-nez v0, :cond_0

    .line 319
    const/4 v0, 0x0

    .line 320
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public getNumOfIcon()I
    .locals 1

    .prologue
    .line 520
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    const/16 v0, 0xc

    .line 523
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public getStickerNum()I
    .locals 1

    .prologue
    .line 474
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    return v0
.end method

.method public getThumbnailCount()I
    .locals 1

    .prologue
    .line 515
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    return v0
.end method

.method public initImageData()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 301
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    new-array v1, v1, [Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    .line 302
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    if-lt v0, v1, :cond_0

    .line 306
    return-void

    .line 303
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    invoke-direct {v2, p0, v3, v3}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;-><init>(Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;Landroid/widget/FrameLayout;)V

    aput-object v2, v1, v0

    .line 304
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->isFlagData:Z

    .line 302
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/View;I)Ljava/lang/Object;
    .locals 4
    .param p1, "page"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 428
    const/4 v0, 0x0

    .line 430
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f03001c

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 431
    const v1, 0x7f090051

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    .line 433
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->setPageView(I)V

    move-object v1, p1

    .line 435
    check-cast v1, Landroid/support/v4/view/ViewPager;

    move-object v2, p1

    check-cast v2, Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    if-le v2, p2, :cond_0

    .end local p1    # "page":Landroid/view/View;
    .end local p2    # "position":I
    :goto_0
    invoke-virtual {v1, v0, p2}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;I)V

    .line 436
    return-object v0

    .line 435
    .restart local p1    # "page":Landroid/view/View;
    .restart local p2    # "position":I
    :cond_0
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "page":Landroid/view/View;
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result p2

    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 478
    check-cast p2, Landroid/view/View;

    .end local p2    # "object":Ljava/lang/Object;
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 493
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    if-lt v0, v1, :cond_0

    .line 501
    :goto_1
    return-void

    .line 495
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-ne p1, v1, :cond_1

    .line 496
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setStickerMode(I)V

    .line 497
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setVisibilityDirectly(I)V

    goto :goto_1

    .line 493
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 446
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    if-lt v0, v1, :cond_0

    .line 454
    :goto_1
    const/4 v1, 0x1

    return v1

    .line 448
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-ne p1, v1, :cond_1

    .line 450
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setVisibilityDirectly(I)V

    goto :goto_1

    .line 446
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0
    .param p1, "pc"    # Landroid/os/Parcelable;
    .param p2, "cl"    # Ljava/lang/ClassLoader;

    .prologue
    .line 487
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 482
    const/4 v0, 0x0

    return-object v0
.end method

.method public setPageView(I)V
    .locals 10
    .param p1, "viewPosition"    # I

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x1

    .line 325
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    if-eqz v5, :cond_1

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->getNumOfIcon()I

    move-result v5

    mul-int v4, p1, v5

    .line 328
    .local v4, "startIndex":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->getNumOfIcon()I

    move-result v5

    add-int v0, v4, v5

    .line 330
    .local v0, "endIndex":I
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ysjeong, viewPosition, startIndex, endIndex : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", setPageView, StickerAdapter"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 332
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    if-le v0, v5, :cond_0

    .line 334
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mNumThumbCount:I

    .line 336
    :cond_0
    move v2, v4

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_2

    .line 365
    .end local v0    # "endIndex":I
    .end local v2    # "i":I
    .end local v4    # "startIndex":I
    :cond_1
    return-void

    .line 338
    .restart local v0    # "endIndex":I
    .restart local v2    # "i":I
    .restart local v4    # "startIndex":I
    :cond_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v5, v5, v2

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->imageView:Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;

    if-eqz v5, :cond_3

    .line 339
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v5, v5, v2

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->imageView:Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;

    invoke-virtual {v5, v9}, Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;->setVisibility(I)V

    .line 340
    :cond_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v5, v5, v2

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_4

    .line 341
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v5, v5, v2

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v9}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 344
    :cond_4
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentLinearLayout:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mStickerFrameLayout:[I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->getNumOfIcon()I

    move-result v7

    rem-int v7, v2, v7

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 346
    .local v1, "fl":Landroid/widget/FrameLayout;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageViewRes:[I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->getNumOfIcon()I

    move-result v6

    rem-int v6, v2, v6

    aget v5, v5, v6

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;

    .line 348
    .local v3, "imageView":Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;
    invoke-virtual {v3, v8}, Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;->setEnabled(Z)V

    .line 349
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;->setVisibility(I)V

    .line 350
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    aget v5, v5, v2

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;->setBackgroundResource(I)V

    .line 352
    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 353
    invoke-virtual {v1, p0}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 354
    invoke-virtual {v1, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 355
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->FCL:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 357
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mCurrentRes:[I

    aget v5, v5, v2

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;->setImageResource(I)V

    .line 359
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v5, v5, v2

    iput-object v3, v5, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->imageView:Lcom/sec/android/mimage/photoretouching/Gui/IconImageView;

    .line 360
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v5, v5, v2

    iput-object v1, v5, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    .line 361
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->mImageData:[Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;

    aget-object v5, v5, v2

    iput-boolean v8, v5, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter$ImageViewData;->isFlagData:Z

    .line 336
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public startUpdate(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 489
    return-void
.end method
