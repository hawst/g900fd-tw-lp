.class Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$7;
.super Ljava/lang/Object;
.source "DecorationView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->setMainBtnListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    .line 319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 348
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsPenTouch:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mIsMinimum:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 327
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0600c4

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 337
    :cond_0
    :goto_0
    return-void

    .line 331
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(Landroid/view/View;Z)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$7;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x18000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 342
    return-void
.end method
