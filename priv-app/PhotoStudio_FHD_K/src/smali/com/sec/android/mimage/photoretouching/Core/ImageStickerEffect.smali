.class public Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;
.super Ljava/lang/Object;
.source "ImageStickerEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;
    }
.end annotation


# instance fields
.field private final STICKER_MAX_NUM:I

.field private mBm_delete:Landroid/graphics/Bitmap;

.field private mBm_delete_press:Landroid/graphics/Bitmap;

.field private mBm_lrtb:Landroid/graphics/Bitmap;

.field private mBm_lrtb_press:Landroid/graphics/Bitmap;

.field private mBm_rotate:Landroid/graphics/Bitmap;

.field private mBm_rotate_press:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mCurrentStickerCount:I

.field private mCurrentStickerIndex:I

.field mDeletePressed:Z

.field private mGreyPaint:Landroid/graphics/Paint;

.field private mHandler_LB:Landroid/graphics/Bitmap;

.field private mHandler_LT:Landroid/graphics/Bitmap;

.field private mHandler_RB:Landroid/graphics/Bitmap;

.field private mHandler_delete:Landroid/graphics/Bitmap;

.field private mHandler_rotate:Landroid/graphics/Bitmap;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mRectPaint:Landroid/graphics/Paint;

.field private mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

.field private mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

.field mTouchPressed:Z

.field private mTouchType:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->STICKER_MAX_NUM:I

    .line 27
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    .line 29
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 30
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    .line 32
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 34
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    .line 36
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchType:I

    .line 37
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    .line 40
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mDeletePressed:Z

    .line 41
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchPressed:Z

    .line 43
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    .line 44
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    .line 45
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 46
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 47
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 48
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 49
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 59
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imgInfo"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p3, "stickerCallback"    # Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->STICKER_MAX_NUM:I

    .line 27
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    .line 29
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 30
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    .line 32
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 34
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    .line 36
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchType:I

    .line 37
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    .line 40
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mDeletePressed:Z

    .line 41
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchPressed:Z

    .line 43
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    .line 44
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    .line 45
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 46
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 47
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 48
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 49
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 59
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

    .line 63
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    .line 64
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 66
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020374

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    .line 67
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020375

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    .line 68
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020378

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    .line 70
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020377

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    .line 71
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020376

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    .line 72
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020379

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    .line 74
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 75
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 76
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 77
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 78
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 80
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 81
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mRectPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mRectPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mRectPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 85
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    .line 86
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    const/16 v1, 0x4b

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    if-nez v0, :cond_0

    .line 97
    new-array v0, v3, [Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    .line 99
    :cond_0
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

    .line 100
    return-void
.end method

.method private setHandlerIconPressed(I)V
    .locals 1
    .param p1, "touchType"    # I

    .prologue
    .line 284
    packed-switch p1, :pswitch_data_0

    .line 316
    :goto_0
    return-void

    .line 290
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchPressed:Z

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 296
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchPressed:Z

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 299
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 302
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchPressed:Z

    if-eqz v0, :cond_2

    .line 303
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 305
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 308
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchPressed:Z

    if-eqz v0, :cond_3

    .line 309
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 311
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public Destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 225
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    if-eqz v1, :cond_0

    .line 227
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 230
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    .line 232
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 233
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 234
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 236
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 237
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 238
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 240
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 241
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 242
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 243
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 244
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 246
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 247
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    .line 248
    return-void

    .line 228
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 229
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->destory()V

    .line 227
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public applyOriginal()[I
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 673
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v10

    .line 674
    .local v10, "ret":[I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    .line 675
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    .line 676
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 674
    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 677
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    .line 679
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    .line 682
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    .line 683
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 677
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 685
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    if-lt v8, v1, :cond_0

    .line 694
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v7

    move-object v1, v10

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 696
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 697
    return-object v10

    .line 687
    :cond_0
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->getStickerOrderPosition(I)I

    move-result v9

    .line 688
    .local v9, "idx":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v9

    if-eqz v1, :cond_1

    .line 690
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v9

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->applyOriginal(Landroid/graphics/Bitmap;)V

    .line 685
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public applyPreview()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 648
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    .line 649
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 650
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 648
    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 651
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 653
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 656
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 657
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 651
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 659
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    if-lt v8, v1, :cond_0

    .line 667
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBuffer(Landroid/graphics/Bitmap;)V

    .line 668
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 669
    return-void

    .line 661
    :cond_0
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->getStickerOrderPosition(I)I

    move-result v9

    .line 662
    .local v9, "idx":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v9

    if-eqz v1, :cond_1

    .line 664
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v9

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->applyPreview(Landroid/graphics/Bitmap;)V

    .line 659
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public clearResource()V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 253
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 254
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_delete:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 256
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 257
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 258
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 260
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 261
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 262
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 263
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 264
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 265
    return-void
.end method

.method public configurationChanged()V
    .locals 2

    .prologue
    .line 269
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    if-eqz v1, :cond_0

    .line 271
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    if-lt v0, v1, :cond_1

    .line 280
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->resetHandlerIconPressed()V

    .line 281
    return-void

    .line 273
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 275
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->configurationChanged()V

    .line 271
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;)V
    .locals 1
    .param p1, "imageSticker"    # Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    .prologue
    .line 106
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 107
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    .line 108
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    .line 109
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    .line 110
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    .line 111
    return-void
.end method

.method public deleteSticker()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 175
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    if-eq v2, v5, :cond_0

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    if-lez v2, :cond_0

    .line 177
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    .line 179
    .local v0, "curStckIdx":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->destory()V

    .line 180
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    aput-object v6, v2, v3

    .line 182
    const/4 v1, 0x0

    .line 183
    .local v1, "i":I
    move v1, v0

    :goto_0
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_1

    .line 192
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    .line 193
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    .line 195
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    if-gtz v2, :cond_3

    .line 196
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;->actionBarUnableSave()V

    .line 200
    .end local v0    # "curStckIdx":I
    .end local v1    # "i":I
    :cond_0
    :goto_1
    return-void

    .line 185
    .restart local v0    # "curStckIdx":I
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v1

    if-nez v2, :cond_2

    .line 187
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    add-int/lit8 v4, v1, 0x1

    aget-object v3, v3, v4

    aput-object v3, v2, v1

    .line 188
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    add-int/lit8 v3, v1, 0x1

    aput-object v6, v2, v3

    .line 183
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 198
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;->actionBarAbleSave()V

    goto :goto_1
.end method

.method public deleteStickerAll()V
    .locals 3

    .prologue
    .line 204
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    if-lt v0, v1, :cond_0

    .line 211
    return-void

    .line 205
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->destory()V

    .line 206
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 208
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    .line 209
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    .line 204
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public deleteStickerOrdering(I)V
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 609
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    if-lt v0, v1, :cond_0

    .line 614
    move v0, p1

    :goto_1
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_2

    .line 618
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v0, v1, -0x1

    :goto_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    array-length v1, v1

    if-lt v0, v1, :cond_3

    .line 621
    return-void

    .line 610
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getZOrder()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getZOrder()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 611
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getZOrder()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->setZOrder(I)V

    .line 609
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 615
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    add-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getZOrder()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->setZOrder(I)V

    .line 614
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 619
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->setZOrder(I)V

    .line 618
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public drawStickerBdry(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 24
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 412
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    if-nez v2, :cond_1

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 415
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v22

    .line 417
    .local v22, "viewTransform":Landroid/graphics/Matrix;
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    .line 418
    .local v8, "drawRoi":Landroid/graphics/RectF;
    new-instance v20, Landroid/graphics/RectF;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/RectF;-><init>()V

    .line 421
    .local v20, "src":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v21

    .line 422
    .local v21, "supportViewtransMatrix":Landroid/graphics/Matrix;
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 423
    .local v15, "previewRect":Landroid/graphics/RectF;
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 424
    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 427
    const/4 v2, 0x2

    new-array v14, v2, [F

    .line 429
    .local v14, "point":[F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 431
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    array-length v2, v2

    if-lt v11, v2, :cond_6

    .line 521
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 522
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 524
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    aput v3, v14, v2

    .line 525
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->y:F

    aput v3, v14, v2

    .line 526
    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 528
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getDrawBdry()Landroid/graphics/RectF;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 529
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 531
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 532
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getAngle()I

    move-result v2

    int-to-float v2, v2

    .line 533
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 534
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 532
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 536
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 537
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 536
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 538
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 539
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 538
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 540
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 541
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 540
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 542
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 543
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 542
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 551
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    .line 552
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 553
    iget v3, v8, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 554
    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 552
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 557
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 558
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 559
    iget v3, v8, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 560
    iget v4, v8, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 558
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 563
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_4

    .line 564
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 565
    iget v3, v8, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 566
    iget v4, v8, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 564
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 569
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_5

    .line 570
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 571
    iget v3, v8, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 572
    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 570
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 576
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0

    .line 433
    :cond_6
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    if-lt v12, v2, :cond_7

    .line 431
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 435
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    if-eqz v2, :cond_8

    .line 436
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getZOrder()I

    move-result v2

    if-ne v2, v11, :cond_8

    .line 438
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 440
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v3, v3, v12

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    aput v3, v14, v2

    .line 441
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v3, v3, v12

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->y:F

    aput v3, v14, v2

    .line 442
    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 444
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getDrawBdry()Landroid/graphics/RectF;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 445
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 447
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 448
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getAngle()I

    move-result v2

    int-to-float v2, v2

    .line 449
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 450
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 448
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 456
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v23

    .line 457
    .local v23, "width":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    .line 458
    .local v10, "height":I
    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v0, v2

    move/from16 v17, v0

    .line 459
    .local v17, "resizeWidth":I
    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v0, v2

    move/from16 v16, v0

    .line 460
    .local v16, "resizeHeight":I
    move/from16 v0, v17

    int-to-float v2, v0

    move/from16 v0, v23

    int-to-float v3, v0

    div-float v19, v2, v3

    .line 461
    .local v19, "scaleWidth":F
    move/from16 v0, v16

    int-to-float v2, v0

    int-to-float v3, v10

    div-float v18, v2, v3

    .line 463
    .local v18, "scaleHeight":F
    new-instance v13, Landroid/graphics/Matrix;

    invoke-direct {v13}, Landroid/graphics/Matrix;-><init>()V

    .line 464
    .local v13, "matrix":Landroid/graphics/Matrix;
    move/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 465
    iget v2, v8, Landroid/graphics/RectF;->left:F

    iget v3, v8, Landroid/graphics/RectF;->top:F

    invoke-virtual {v13, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 473
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v19, v2

    if-eqz v2, :cond_9

    .line 476
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 489
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 490
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getAngle()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    .line 491
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 492
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 490
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 494
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 495
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getAngle()I

    move-result v2

    int-to-float v2, v2

    .line 496
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 497
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 495
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 498
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v19, v2

    if-eqz v2, :cond_a

    .line 501
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v13, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 511
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 514
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 433
    .end local v10    # "height":I
    .end local v13    # "matrix":Landroid/graphics/Matrix;
    .end local v16    # "resizeHeight":I
    .end local v17    # "resizeWidth":I
    .end local v18    # "scaleHeight":F
    .end local v19    # "scaleWidth":F
    .end local v23    # "width":I
    :cond_8
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2

    .line 478
    .restart local v10    # "height":I
    .restart local v13    # "matrix":Landroid/graphics/Matrix;
    .restart local v16    # "resizeHeight":I
    .restart local v17    # "resizeWidth":I
    .restart local v18    # "scaleHeight":F
    .restart local v19    # "scaleWidth":F
    .restart local v23    # "width":I
    :catch_0
    move-exception v9

    .line 479
    .local v9, "e":Ljava/lang/Exception;
    goto/16 :goto_0

    .line 484
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iget v3, v8, Landroid/graphics/RectF;->left:F

    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mGreyPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_3

    .line 503
    :catch_1
    move-exception v9

    .line 504
    .restart local v9    # "e":Ljava/lang/Exception;
    goto/16 :goto_0

    .line 509
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iget v3, v8, Landroid/graphics/RectF;->left:F

    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_4
.end method

.method public freeResource()V
    .locals 0

    .prologue
    .line 216
    return-void
.end method

.method public getCurStickerCount()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    return v0
.end method

.method public getCurStickerIdx()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 121
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSticker()[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    return-object v0
.end method

.method public getStickerOrderPosition(I)I
    .locals 2
    .param p1, "orderNum"    # I

    .prologue
    .line 591
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 598
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v0, v1, -0x1

    .end local v0    # "i":I
    :goto_1
    return v0

    .line 592
    .restart local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    .line 591
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 594
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getZOrder()I

    move-result v1

    if-ne v1, p1, :cond_1

    goto :goto_1
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 116
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetHandlerIconPressed()V
    .locals 1

    .prologue
    .line 319
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchPressed:Z

    .line 321
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LT:Landroid/graphics/Bitmap;

    .line 322
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 323
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 324
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 325
    return-void
.end method

.method public resetStickerOrdering()V
    .locals 2

    .prologue
    .line 602
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    .line 606
    return-void

    .line 603
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 604
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->setZOrder(I)V

    .line 602
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setStickerMode(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 10
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "targetOrgRoi"    # Landroid/graphics/Rect;

    .prologue
    .line 137
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 139
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->destory()V

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 142
    const v0, 0x10001008

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 143
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    .line 144
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 146
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    .line 147
    const/4 v5, 0x2

    .line 148
    const/4 v6, 0x0

    move-object v3, p1

    move-object v7, p2

    .line 149
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/graphics/Bitmap;IIZLandroid/graphics/Rect;)V

    .line 143
    aput-object v0, v8, v9

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sj, StickerTestView - setStickerMode() - mCurrentStickerCount : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 152
    const-string v1, " / mSticker[mCurrentStickerCount].getZOrder() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getZOrder()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 153
    const-string v1, " / orgRoi : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getOrgDestROI()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 165
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    .line 166
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    .line 171
    :goto_0
    return-void

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mContext:Landroid/content/Context;

    const v1, 0x7f0600c3

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public setStickerOrdering(I)V
    .locals 3
    .param p1, "curIdx"    # I

    .prologue
    .line 581
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    if-ge v0, v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    array-length v2, v2

    if-le v1, v2, :cond_1

    .line 587
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, p1

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->setZOrder(I)V

    .line 588
    return-void

    .line 582
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getZOrder()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getZOrder()I

    move-result v2

    if-le v1, v2, :cond_2

    .line 583
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getZOrder()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->setZOrder(I)V

    .line 584
    :cond_2
    if-ne v0, p1, :cond_3

    .line 581
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public touch(Landroid/view/MotionEvent;Z)V
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "isDrawerOpened"    # Z

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 329
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->transformVirtualPreviewCoordinate(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)V

    .line 332
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v4

    int-to-float v4, v4

    sub-float v1, v3, v4

    .line 333
    .local v1, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v4

    int-to-float v4, v4

    sub-float v2, v3, v4

    .line 334
    .local v2, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 408
    :goto_0
    return-void

    .line 337
    :pswitch_0
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchPressed:Z

    .line 339
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchType:I

    .line 340
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    if-eq v3, v7, :cond_0

    .line 342
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->InitMoveObject(FF)I

    move-result v3

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchType:I

    .line 343
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchType:I

    if-nez v3, :cond_0

    .line 344
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    .line 347
    :cond_0
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchType:I

    if-ge v3, v5, :cond_3

    .line 349
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

    invoke-interface {v3, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;->setVisibleMenu(Z)V

    .line 351
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerCount:I

    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_1
    if-gez v0, :cond_1

    .line 379
    .end local v0    # "i":I
    :goto_2
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchType:I

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->setHandlerIconPressed(I)V

    goto :goto_0

    .line 353
    .restart local v0    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    aget-object v3, v3, v0

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->InitMoveObject(FF)I

    move-result v3

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchType:I

    .line 354
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchType:I

    if-lt v3, v5, :cond_2

    .line 356
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    .line 358
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->setStickerOrdering(I)V

    goto :goto_2

    .line 351
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 374
    .end local v0    # "i":I
    :cond_3
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->setStickerOrdering(I)V

    .line 376
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

    invoke-interface {v3, v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;->setVisibleMenu(Z)V

    goto :goto_2

    .line 383
    :pswitch_1
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchPressed:Z

    .line 385
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    if-eq v3, v7, :cond_4

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    aget-object v3, v3, v4

    if-eqz v3, :cond_4

    .line 387
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

    invoke-interface {v3, v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;->setVisibleMenu(Z)V

    .line 388
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->StartMoveObject(FF)V

    .line 391
    :cond_4
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchType:I

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->setHandlerIconPressed(I)V

    goto :goto_0

    .line 396
    :pswitch_2
    iput-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchPressed:Z

    .line 398
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mStickerCallbak:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

    invoke-interface {v3, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;->setVisibleMenu(Z)V

    .line 399
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    if-eq v3, v7, :cond_5

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    aget-object v3, v3, v4

    if-eqz v3, :cond_5

    .line 401
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mSticker:[Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mCurrentStickerIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->EndMoveObject()V

    .line 403
    :cond_5
    iput-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mDeletePressed:Z

    .line 404
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mTouchType:I

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->setHandlerIconPressed(I)V

    goto/16 :goto_0

    .line 334
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public updateOriginalBuffer([I)V
    .locals 1
    .param p1, "prevOriginalBuf"    # [I

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([I)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPreviewBuffer()V

    .line 221
    return-void
.end method
