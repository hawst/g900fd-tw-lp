.class Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;
.super Ljava/lang/Object;
.source "ImageStickerView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->getImage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;)Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    return-object v0
.end method


# virtual methods
.method public initCopyToImageData(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;)V
    .locals 5
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;

    .prologue
    .line 121
    if-eqz p1, :cond_1

    .line 123
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    if-nez v2, :cond_0

    .line 124
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2$1;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 150
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isPersonalPage()Z

    move-result v0

    .line 135
    .local v0, "isPrevBitmapPersonal":Z
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Landroid/content/Context;

    move-result-object v4

    iget-boolean v2, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isUri:Z

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->uri:Landroid/net/Uri;

    :goto_1
    invoke-virtual {v3, v4, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->determinePersonalPage(Landroid/content/Context;Ljava/lang/Object;)V

    .line 136
    if-eqz v0, :cond_1

    .line 138
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPersonalPage(Z)V

    .line 143
    .end local v0    # "isPrevBitmapPersonal":Z
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    iget-object v3, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->image:Landroid/graphics/Bitmap;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->scaleCopyBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Rect;
    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;Landroid/graphics/Bitmap;)Landroid/graphics/Rect;

    move-result-object v1

    .line 144
    .local v1, "scaleRect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2$2;

    invoke-direct {v3, p0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;Landroid/graphics/Rect;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 135
    .end local v1    # "scaleRect":Landroid/graphics/Rect;
    .restart local v0    # "isPrevBitmapPersonal":Z
    :cond_2
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->path:Ljava/lang/String;

    goto :goto_1
.end method
