.class Lcom/sec/android/mimage/photoretouching/exif/ExifReader;
.super Ljava/lang/Object;
.source "ExifReader.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ExifReader"


# instance fields
.field private final mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;)V
    .locals 0
    .param p1, "iRef"    # Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifReader;->mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    .line 20
    return-void
.end method


# virtual methods
.method protected read(Ljava/io/InputStream;)Lcom/sec/android/mimage/photoretouching/exif/ExifData;
    .locals 7
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/exif/ExifReader;->mInterface:Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    invoke-static {p1, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->parse(Ljava/io/InputStream;Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;)Lcom/sec/android/mimage/photoretouching/exif/ExifParser;

    move-result-object v3

    .line 32
    .local v3, "parser":Lcom/sec/android/mimage/photoretouching/exif/ExifParser;
    new-instance v2, Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->getByteOrder()Ljava/nio/ByteOrder;

    move-result-object v5

    invoke-direct {v2, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;-><init>(Ljava/nio/ByteOrder;)V

    .line 33
    .local v2, "exifData":Lcom/sec/android/mimage/photoretouching/exif/ExifData;
    const/4 v4, 0x0

    .line 35
    .local v4, "tag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->next()I

    move-result v1

    .line 36
    .local v1, "event":I
    :goto_0
    const/4 v5, 0x5

    if-ne v1, v5, :cond_0

    .line 77
    return-object v2

    .line 37
    :cond_0
    packed-switch v1, :pswitch_data_0

    .line 75
    :goto_1
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->next()I

    move-result v1

    goto :goto_0

    .line 39
    :pswitch_0
    new-instance v5, Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->getCurrentIfd()I

    move-result v6

    invoke-direct {v5, v6}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;-><init>(I)V

    invoke-virtual {v2, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->addIfdData(Lcom/sec/android/mimage/photoretouching/exif/IfdData;)V

    goto :goto_1

    .line 42
    :pswitch_1
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->getTag()Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v4

    .line 43
    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->hasValue()Z

    move-result v5

    if-nez v5, :cond_1

    .line 44
    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->registerForTagValue(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)V

    goto :goto_1

    .line 46
    :cond_1
    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getIfd()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    goto :goto_1

    .line 50
    :pswitch_2
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->getTag()Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v4

    .line 51
    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataType()S

    move-result v5

    const/4 v6, 0x7

    if-ne v5, v6, :cond_2

    .line 52
    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->readFullTagValue(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)V

    .line 54
    :cond_2
    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getIfd()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getIfdData(I)Lcom/sec/android/mimage/photoretouching/exif/IfdData;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    goto :goto_1

    .line 57
    :pswitch_3
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->getCompressedImageSize()I

    move-result v5

    new-array v0, v5, [B

    .line 58
    .local v0, "buf":[B
    array-length v5, v0

    invoke-virtual {v3, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->read([B)I

    move-result v6

    if-ne v5, v6, :cond_3

    .line 59
    invoke-virtual {v2, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->setCompressedThumbnail([B)V

    goto :goto_1

    .line 61
    :cond_3
    const-string v5, "ExifReader"

    const-string v6, "Failed to read the compressed thumbnail"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 65
    .end local v0    # "buf":[B
    :pswitch_4
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->getStripSize()I

    move-result v5

    new-array v0, v5, [B

    .line 66
    .restart local v0    # "buf":[B
    array-length v5, v0

    invoke-virtual {v3, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->read([B)I

    move-result v6

    if-ne v5, v6, :cond_4

    .line 67
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->getStripIndex()I

    move-result v5

    invoke-virtual {v2, v5, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->setStripBytes(I[B)V

    goto :goto_1

    .line 69
    :cond_4
    const-string v5, "ExifReader"

    const-string v6, "Failed to read the strip bytes"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
