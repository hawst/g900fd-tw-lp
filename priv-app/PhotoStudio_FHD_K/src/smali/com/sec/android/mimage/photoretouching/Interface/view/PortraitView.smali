.class public Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "PortraitView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$ResolverSetAsAdapter;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$SaveAsyncTask;
    }
.end annotation


# instance fields
.field private final REDOALL_DIALOG:I

.field private final UNDOALL_DIALOG:I

.field private forceInitPortraitEngineCheck:Z

.field isPortrait:Ljava/lang/Boolean;

.field private isReverseAnimRunning:Z

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mAlpha:I

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mClearPaint:Landroid/graphics/Paint;

.field private mColor:I

.field private mContext:Landroid/content/Context;

.field private mCurrentEffectType:I

.field private mCurrentSaveSize:I

.field private mDashPathEffectPaint:Landroid/graphics/Paint;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mDrawAnimRunnable:Ljava/lang/Runnable;

.field private mDrawOriginal:Z

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHandler:Landroid/os/Handler;

.field private mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIsMinimum:Z

.field private mLinePaint:Landroid/graphics/Paint;

.field private mLongpressButton:Z

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mOptionItemId:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mProgressTextRect:Landroid/graphics/Rect;

.field private mRedEyeAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

.field private mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

.field private mSeekbarThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

.field private mTouchDownProgress:F

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;

.field private msave:Z

.field private msetas:Z

.field rotation:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 99
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 120
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->forceInitPortraitEngineCheck:Z

    .line 1103
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->rotation:I

    .line 1104
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->isPortrait:Ljava/lang/Boolean;

    .line 2520
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mRedEyeAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    .line 2828
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 2829
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    .line 2830
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 2831
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 2832
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 2833
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2834
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 2835
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 2836
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 2837
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPaint:Landroid/graphics/Paint;

    .line 2838
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mClearPaint:Landroid/graphics/Paint;

    .line 2839
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mLinePaint:Landroid/graphics/Paint;

    .line 2840
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 2841
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    .line 2842
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 2843
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    .line 2844
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mColor:I

    .line 2845
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    .line 2846
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mIsMinimum:Z

    .line 2849
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mProgressTextRect:Landroid/graphics/Rect;

    .line 2851
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2853
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDrawOriginal:Z

    .line 2854
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 2857
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->UNDOALL_DIALOG:I

    .line 2858
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->REDOALL_DIALOG:I

    .line 2860
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mOptionItemId:I

    .line 2862
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 2864
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentSaveSize:I

    .line 2865
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mLongpressButton:Z

    .line 2866
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mTouchDownProgress:F

    .line 2867
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->isReverseAnimRunning:Z

    .line 2869
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mAlpha:I

    .line 2870
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->msetas:Z

    .line 2871
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->msave:Z

    .line 2872
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    .line 100
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initPortraitView(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 102
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initGesture()V

    .line 103
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 2828
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 2829
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;I)Z
    .locals 1

    .prologue
    .line 2046
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->runOptionItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .locals 1

    .prologue
    .line 2834
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;
    .locals 1

    .prologue
    .line 2841
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->forceInitPortraitEngineCheck:Z

    return v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V
    .locals 0

    .prologue
    .line 120
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->forceInitPortraitEngineCheck:Z

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;I)V
    .locals 0

    .prologue
    .line 2845
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)I
    .locals 1

    .prologue
    .line 2845
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    return v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;
    .locals 1

    .prologue
    .line 2520
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mRedEyeAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;)V
    .locals 0

    .prologue
    .line 2520
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mRedEyeAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    return-void
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;I)V
    .locals 0

    .prologue
    .line 2356
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->checkHelpPopup(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 2830
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 2832
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 2833
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;
    .locals 1

    .prologue
    .line 2835
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    return-object v0
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V
    .locals 0

    .prologue
    .line 803
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->init3DepthActionBar()V

    return-void
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V
    .locals 0

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->setMainBtnListener()V

    return-void
.end method

.method static synthetic access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2851
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$26(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Z
    .locals 1

    .prologue
    .line 2846
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mIsMinimum:Z

    return v0
.end method

.method static synthetic access$27(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$29(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V
    .locals 0

    .prologue
    .line 2476
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->refreshImageAndBottomButtons()V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 2831
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$30(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 2851
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V
    .locals 0

    .prologue
    .line 2865
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mLongpressButton:Z

    return-void
.end method

.method static synthetic access$32(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Z
    .locals 1

    .prologue
    .line 2865
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mLongpressButton:Z

    return v0
.end method

.method static synthetic access$33(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Landroid/content/Intent;Z)V
    .locals 0

    .prologue
    .line 2118
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    return-void
.end method

.method static synthetic access$34(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;I)V
    .locals 0

    .prologue
    .line 2860
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mOptionItemId:I

    return-void
.end method

.method static synthetic access$35(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;I)V
    .locals 0

    .prologue
    .line 2864
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentSaveSize:I

    return-void
.end method

.method static synthetic access$36(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V
    .locals 0

    .prologue
    .line 1460
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->doDone()V

    return-void
.end method

.method static synthetic access$37(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V
    .locals 0

    .prologue
    .line 1438
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->doCancel()V

    return-void
.end method

.method static synthetic access$38(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 2837
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$39(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V
    .locals 0

    .prologue
    .line 2836
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 2842
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$40(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Z
    .locals 1

    .prologue
    .line 2867
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->isReverseAnimRunning:Z

    return v0
.end method

.method static synthetic access$41(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V
    .locals 0

    .prologue
    .line 2867
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->isReverseAnimRunning:Z

    return-void
.end method

.method static synthetic access$42(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;I)V
    .locals 0

    .prologue
    .line 2869
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mAlpha:I

    return-void
.end method

.method static synthetic access$43(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Z
    .locals 1

    .prologue
    .line 1184
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->isSeekbarProgressChanged()Z

    move-result v0

    return v0
.end method

.method static synthetic access$44(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Z)V
    .locals 0

    .prologue
    .line 2853
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDrawOriginal:Z

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)I
    .locals 1

    .prologue
    .line 2864
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentSaveSize:I

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 2828
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 2854
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 2854
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)I
    .locals 1

    .prologue
    .line 2860
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mOptionItemId:I

    return v0
.end method

.method private checkHelpPopup(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 2358
    const/4 v0, 0x0

    .line 2359
    .local v0, "isNoShow":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->isDoNotShowPopup(Landroid/content/Context;I)Z

    move-result v0

    .line 2361
    if-nez v0, :cond_0

    .line 2363
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-nez v1, :cond_0

    .line 2365
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initHelpPopup(I)V

    .line 2366
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->showCurrentModeHelpPopup()V

    .line 2369
    :cond_0
    return-void
.end method

.method private doCancel()V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 1440
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 1441
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1442
    .local v1, "output":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 1443
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1446
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1449
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1450
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 1452
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 1444
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1455
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->set2depth()V

    .line 1456
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->setMainBtnListener()V

    .line 1457
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 1458
    return-void
.end method

.method private doDone()V
    .locals 6

    .prologue
    .line 1462
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->applyPreview()V

    .line 1463
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->doDone()V

    .line 1464
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-direct {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)V

    .line 1466
    .local v4, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1467
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 1468
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 1469
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getEnhance()Z

    move-result v5

    .line 1466
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V

    .line 1470
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->set2depth()V

    .line 1471
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->setMainBtnListener()V

    .line 1472
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 1473
    return-void
.end method

.method private getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 124
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    return-object v0
.end method

.method private init2DepthActionBar()V
    .locals 5

    .prologue
    const/4 v2, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 489
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isValidBackKey()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 493
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 496
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$9;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 493
    invoke-virtual {v0, v3, v2, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 576
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 579
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$10;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 576
    invoke-virtual {v0, v3, v4, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 623
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x2

    .line 626
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 623
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 675
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 678
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$12;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 675
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 707
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0x10

    .line 710
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 707
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 746
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x5

    .line 749
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$14;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 746
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 789
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 791
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 793
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 794
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isSaved()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 801
    :cond_0
    :goto_1
    return-void

    .line 536
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v4, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    goto :goto_0

    .line 797
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isSaved()Z

    move-result v1

    invoke-virtual {v0, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_1
.end method

.method private init3DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 806
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 808
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 809
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 812
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$15;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 809
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 833
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 836
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$16;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 833
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 859
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 860
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 862
    :cond_0
    return-void
.end method

.method private initBrushDialog()V
    .locals 1

    .prologue
    .line 866
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-nez v0, :cond_0

    .line 868
    :cond_0
    return-void
.end method

.method private initByConfigPortraitEffect()V
    .locals 2

    .prologue
    .line 872
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    packed-switch v0, :pswitch_data_0

    .line 888
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->invalidateViews()V

    .line 889
    return-void

    .line 878
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->getCurrentStep()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->addEvent(I)V

    goto :goto_0

    .line 881
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->getCurrentStep()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->addEvent(I)V

    goto :goto_0

    .line 884
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->getCurrentStep()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->addEvent(I)V

    goto :goto_0

    .line 872
    nop

    :pswitch_data_0
    .packed-switch 0x17001700
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private initGesture()V
    .locals 3

    .prologue
    .line 2407
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$33;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$33;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 2464
    .local v0, "gesture":Landroid/view/GestureDetector$OnGestureListener;
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 2465
    return-void
.end method

.method private initHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 2372
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 2373
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$32;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$32;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->setHelpPopupCallback(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;)V

    .line 2380
    return-void
.end method

.method private initPortraitView(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x42c80000    # 100.0f

    const/high16 v3, 0x3f800000    # 1.0f

    .line 1951
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHandler:Landroid/os/Handler;

    .line 1952
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    .line 1953
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1954
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1955
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1956
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1958
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->setInterface(Ljava/lang/Object;)V

    .line 1960
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPaint:Landroid/graphics/Paint;

    .line 1961
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1963
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mClearPaint:Landroid/graphics/Paint;

    .line 1964
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mClearPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 1966
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mLinePaint:Landroid/graphics/Paint;

    .line 1967
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1968
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1970
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 1971
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1972
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1973
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    const/4 v2, 0x4

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v1, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1975
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1976
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1977
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1979
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    .line 1980
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$29;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->setOnCallback(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;)V

    .line 2008
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    const/high16 v1, 0x18000000

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$30;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$30;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;-><init>(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 2018
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initProgressText()V

    .line 2020
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    .line 2021
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarThreadManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$31;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$31;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager;->setOnCallback(Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;)V

    .line 2036
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v4

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v4

    if-gez v0, :cond_1

    .line 2037
    :cond_0
    iput-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mIsMinimum:Z

    .line 2042
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initBrushDialog()V

    .line 2043
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->setViewLayerType(I)V

    .line 2044
    return-void

    .line 2039
    :cond_1
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mIsMinimum:Z

    goto :goto_0

    .line 1973
    nop

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method private initSaveOptionDialog()V
    .locals 11

    .prologue
    const v10, 0x7f0601f0

    const v9, 0x7f0601cb

    const/16 v2, 0x9

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 1813
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1871
    :goto_0
    return-void

    .line 1818
    :cond_0
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$23;

    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$23;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 1832
    .local v8, "saveListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1834
    const v3, 0x7f0601cd

    .line 1836
    const/4 v5, 0x1

    .line 1838
    const v7, 0x103012e

    .line 1832
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZZLandroid/graphics/Point;I)V

    .line 1840
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1845
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x810

    move v3, v9

    move v5, v9

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1849
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x820

    move v3, v10

    move v5, v10

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1852
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$24;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$24;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1859
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$25;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$25;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1870
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    goto :goto_0
.end method

.method private initSaveYesNoCancelForFinish()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 1875
    const/4 v7, 0x0

    .line 1876
    .local v7, "fileName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 1877
    .local v8, "path":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 1879
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1880
    if-nez v7, :cond_0

    .line 1882
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1886
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1887
    const/16 v2, 0x8

    .line 1888
    const v3, 0x7f06019d

    .line 1889
    const/4 v4, 0x1

    .line 1891
    const v6, 0x103012e

    .line 1886
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1893
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1894
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x500

    .line 1897
    const v4, 0x7f060192

    move v2, v9

    move v3, v9

    move-object v6, v5

    .line 1894
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1901
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    .line 1902
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$26;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$26;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 1901
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V

    .line 1924
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601b2

    .line 1925
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$27;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$27;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 1924
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1933
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1934
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$28;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$28;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 1933
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1941
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1942
    return-void
.end method

.method private initSetAsDialog()V
    .locals 7

    .prologue
    .line 2394
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 2396
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2397
    const v2, 0x7f090158

    .line 2398
    const v3, 0x7f0600a3

    .line 2399
    const/4 v4, 0x0

    .line 2400
    const/4 v5, 0x0

    .line 2401
    const v6, 0x103012e

    .line 2396
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2402
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2404
    :cond_0
    return-void
.end method

.method private initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isShareVia"    # Z

    .prologue
    .line 2120
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2121
    .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    const/4 v2, 0x0

    .line 2122
    .local v2, "shareChooser":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 2123
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    const v4, 0x7f06000f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 2127
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/content/pm/LabeledIntent;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/pm/LabeledIntent;

    .line 2128
    .local v0, "extraIntents":[Landroid/content/pm/LabeledIntent;
    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2129
    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2130
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2311
    return-void

    .line 2125
    .end local v0    # "extraIntents":[Landroid/content/pm/LabeledIntent;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    const v4, 0x7f0600a3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method private initShareViaDialog()V
    .locals 7

    .prologue
    .line 2382
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 2384
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 2385
    const v2, 0x7f090157

    .line 2386
    const v3, 0x7f06000f

    .line 2387
    const/4 v4, 0x0

    .line 2388
    const/4 v5, 0x0

    .line 2389
    const v6, 0x103012e

    .line 2384
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 2390
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 2392
    :cond_0
    return-void
.end method

.method private initUndoRedoAllDialog()V
    .locals 15

    .prologue
    const v6, 0x103012e

    const/16 v1, 0x1000

    const/16 v14, 0x500

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 1575
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1577
    const v3, 0x7f0600a6

    move v4, v2

    .line 1575
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1582
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1583
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1584
    const v3, 0x7f06009c

    .line 1586
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$17;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$17;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 1583
    invoke-virtual {v0, v14, v3, v5, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1593
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060007

    .line 1594
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$18;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$18;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 1593
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1628
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060009

    .line 1629
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$19;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$19;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 1628
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1637
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1639
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1640
    const/4 v9, 0x2

    .line 1641
    const v10, 0x7f0600a1

    move v8, v1

    move v11, v2

    move-object v12, v5

    move v13, v6

    .line 1639
    invoke-virtual/range {v7 .. v13}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1646
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1647
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1648
    const v1, 0x7f0601ce

    .line 1650
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$20;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$20;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 1647
    invoke-virtual {v0, v14, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1657
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 1658
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$21;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 1657
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1690
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1691
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$22;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$22;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    .line 1690
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1699
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1700
    return-void
.end method

.method private isSeekbarProgressChanged()Z
    .locals 3

    .prologue
    .line 1186
    const/4 v0, 0x0

    .line 1187
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v1, :cond_0

    .line 1189
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->getCurrentStep()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mTouchDownProgress:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 1190
    const/4 v0, 0x1

    .line 1192
    :cond_0
    return v0
.end method

.method private refreshImageAndBottomButtons()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/high16 v10, 0x42c80000    # 100.0f

    const/4 v2, 0x0

    .line 2478
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_1

    .line 2480
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-ltz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-gez v3, :cond_5

    .line 2481
    :cond_0
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mIsMinimum:Z

    .line 2485
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2486
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 2487
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 2488
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2491
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 2494
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 2495
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 2497
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 2489
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 2499
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "output":[I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_4

    .line 2501
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_3

    .line 2503
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-ltz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-gez v3, :cond_6

    .line 2504
    :cond_2
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mIsMinimum:Z

    .line 2508
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$34;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$34;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2517
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->invalidateViews()V

    .line 2518
    return-void

    .line 2483
    :cond_5
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mIsMinimum:Z

    goto :goto_0

    .line 2506
    :cond_6
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mIsMinimum:Z

    goto :goto_1
.end method

.method private runOptionItem(I)Z
    .locals 7
    .param p1, "optionItemId"    # I

    .prologue
    const v6, 0x7f090158

    const v5, 0x7a1200

    const/4 v4, 0x7

    const/16 v3, 0x9

    .line 2048
    const/4 v0, 0x0

    .line 2049
    .local v0, "ret":Z
    const/4 v1, 0x0

    .line 2050
    .local v1, "uri":Landroid/net/Uri;
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mOptionItemId:I

    .line 2051
    sparse-switch p1, :sswitch_data_0

    .line 2116
    :cond_0
    :goto_0
    return v0

    .line 2054
    :sswitch_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x2e000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 2055
    const/4 v0, 0x1

    .line 2056
    goto :goto_0

    .line 2058
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2060
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2067
    :goto_1
    const/4 v0, 0x1

    .line 2068
    goto :goto_0

    .line 2065
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->openCamera(Landroid/content/Context;)V

    goto :goto_1

    .line 2070
    :sswitch_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2072
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentSaveSize:I

    .line 2073
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2074
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2085
    :cond_2
    :goto_2
    const/4 v0, 0x1

    .line 2086
    goto :goto_0

    .line 2076
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f090157

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2077
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 2078
    if-nez v1, :cond_4

    .line 2080
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2082
    :cond_4
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    goto :goto_2

    .line 2089
    :sswitch_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2091
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentSaveSize:I

    .line 2092
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2093
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2106
    :cond_5
    :goto_3
    const/4 v0, 0x1

    .line 2107
    goto/16 :goto_0

    .line 2095
    :cond_6
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2098
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 2099
    if-nez v1, :cond_7

    .line 2101
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2103
    :cond_7
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    .line 2104
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v2, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    goto :goto_3

    .line 2112
    :sswitch_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 2113
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v3, 0x17000000

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 2051
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f090007 -> :sswitch_0
        0x7f090157 -> :sswitch_2
        0x7f090158 -> :sswitch_3
        0x7f09015a -> :sswitch_1
        0x7f09015b -> :sswitch_4
    .end sparse-switch
.end method

.method private set2depth()V
    .locals 1

    .prologue
    .line 1414
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->init2DepthActionBar()V

    .line 1415
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    .line 1417
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1418
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 1419
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 1423
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1424
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 1425
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 1431
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->stop()V

    .line 1432
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setClearSelectedSubButton()V

    .line 1433
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    .line 1435
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->invalidateViews()V

    .line 1436
    return-void

    .line 1421
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    goto :goto_0

    .line 1427
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 1428
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1
.end method

.method private setMainBtnListener()V
    .locals 8

    .prologue
    const v7, 0x10001005

    const v6, 0x10001004

    const v5, 0x10001006

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 197
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isVisibleTitle()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 200
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mIsMinimum:Z

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100b

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 403
    :goto_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getPreviousMode()I

    move-result v0

    const/high16 v1, -0x10000

    and-int/2addr v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 431
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 432
    return-void

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$4;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 294
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$6;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    invoke-virtual {v0, v7, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 326
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$7;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEnableDecoration()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 351
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100b

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$8;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_0

    .line 379
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100b

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_0

    .line 385
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mIsMinimum:Z

    if-eqz v0, :cond_3

    .line 387
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 388
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 389
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 391
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100b

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto/16 :goto_0

    .line 395
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100a

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 398
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5, v3, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x1000100b

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto/16 :goto_0

    .line 406
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001007

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 409
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001009

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 412
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001008

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 415
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 418
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 421
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 424
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001003

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 427
    :sswitch_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001002

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto/16 :goto_1

    .line 403
    :sswitch_data_0
    .sparse-switch
        0x11100000 -> :sswitch_7
        0x11200000 -> :sswitch_6
        0x15000000 -> :sswitch_4
        0x16000000 -> :sswitch_3
        0x18000000 -> :sswitch_5
        0x1a000000 -> :sswitch_1
        0x31100000 -> :sswitch_2
        0x31200000 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v9, 0xff

    const/4 v8, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 1108
    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDrawOriginal:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v4, :cond_0

    .line 1109
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v4, p2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 1111
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-static {p2, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->transformVirtualPreviewCoordinate(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)V

    .line 1113
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 1114
    .local v2, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 1117
    .local v3, "y":F
    const/4 v0, 0x1

    .line 1124
    .local v0, "ret":Z
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v4, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1126
    const/4 v0, 0x1

    .line 1181
    :cond_1
    :goto_0
    :pswitch_0
    return v0

    .line 1130
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 1133
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v4, :cond_3

    .line 1134
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->getCurrentStep()F

    move-result v4

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mTouchDownProgress:F

    .line 1135
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->invalidateViews()V

    goto :goto_0

    .line 1141
    :pswitch_2
    const/high16 v4, -0x40800000    # -1.0f

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mTouchDownProgress:F

    .line 1142
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    const v5, 0x17001700

    if-ne v4, v5, :cond_4

    .line 1145
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1146
    .local v1, "tempPath":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotateDegree(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->rotation:I

    .line 1147
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v7, :cond_7

    .line 1148
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->isPortrait:Ljava/lang/Boolean;

    .line 1153
    :goto_1
    cmpl-float v4, v2, v6

    if-ltz v4, :cond_4

    cmpl-float v4, v3, v6

    if-ltz v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v4, v2, v4

    if-gtz v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v4, v3, v4

    if-gtz v4, :cond_4

    .line 1155
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->rotation:I

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->isPortrait:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual {v4, v2, v3, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->applypreviewRedEye(FFIZ)V

    .line 1156
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mRedEyeAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->runAnimation(FF)V

    .line 1159
    .end local v1    # "tempPath":Ljava/lang/String;
    :cond_4
    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDrawOriginal:Z

    if-eqz v4, :cond_5

    .line 1161
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDrawOriginal:Z

    .line 1162
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->resetPreProgressValue()V

    .line 1163
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->invalidateViews()V

    .line 1165
    :cond_5
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v4

    if-eq v4, v7, :cond_1

    .line 1166
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->startseek()V

    .line 1167
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->isReverseAnimRunning:Z

    .line 1168
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mAlpha:I

    if-le v4, v9, :cond_6

    .line 1170
    iput v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mAlpha:I

    .line 1172
    :cond_6
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 1150
    .restart local v1    # "tempPath":Ljava/lang/String;
    :cond_7
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->isPortrait:Ljava/lang/Boolean;

    goto :goto_1

    .line 1130
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public backPressed()V
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isVisibleTitle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 466
    :goto_0
    return-void

    .line 463
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->doCancel()V

    .line 464
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resetMenu()V

    goto :goto_0
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 1317
    return-void
.end method

.method public drawFaceRect(Landroid/graphics/Canvas;)V
    .locals 24
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 892
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    .line 893
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->getFaceInfo()Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

    move-result-object v6

    .line 895
    .local v6, "faceInfo":Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    .line 896
    .local v9, "metrics":Landroid/util/DisplayMetrics;
    const/high16 v19, 0x3f800000    # 1.0f

    iget v0, v9, Landroid/util/DisplayMetrics;->densityDpi:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x43200000    # 160.0f

    div-float v20, v20, v21

    mul-float v11, v19, v20

    .line 899
    .local v11, "px":F
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    .line 900
    .local v10, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f04003b

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getColor(I)I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 901
    sget-object v19, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 902
    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 903
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 904
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 906
    .local v7, "faceRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v18

    .line 908
    .local v18, "viewTransform":Landroid/graphics/Matrix;
    const/16 v19, 0x9

    move/from16 v0, v19

    new-array v0, v0, [F

    move-object/from16 v17, v0

    .line 910
    .local v17, "values":[F
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 911
    const/16 v19, 0x0

    aget v15, v17, v19

    .line 912
    .local v15, "scaleX":F
    const/16 v19, 0x4

    aget v16, v17, v19

    .line 914
    .local v16, "scaleY":F
    if-eqz v6, :cond_0

    .line 916
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget v0, v6, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFacenum:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-lt v8, v0, :cond_1

    .line 945
    .end local v6    # "faceInfo":Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;
    .end local v7    # "faceRect":Landroid/graphics/Rect;
    .end local v8    # "i":I
    .end local v9    # "metrics":Landroid/util/DisplayMetrics;
    .end local v10    # "paint":Landroid/graphics/Paint;
    .end local v11    # "px":F
    .end local v15    # "scaleX":F
    .end local v16    # "scaleY":F
    .end local v17    # "values":[F
    .end local v18    # "viewTransform":Landroid/graphics/Matrix;
    :cond_0
    return-void

    .line 918
    .restart local v6    # "faceInfo":Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;
    .restart local v7    # "faceRect":Landroid/graphics/Rect;
    .restart local v8    # "i":I
    .restart local v9    # "metrics":Landroid/util/DisplayMetrics;
    .restart local v10    # "paint":Landroid/graphics/Paint;
    .restart local v11    # "px":F
    .restart local v15    # "scaleX":F
    .restart local v16    # "scaleY":F
    .restart local v17    # "values":[F
    .restart local v18    # "viewTransform":Landroid/graphics/Matrix;
    :cond_1
    iget-object v0, v6, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    move-object/from16 v19, v0

    aget-object v19, v19, v8

    if-eqz v19, :cond_2

    .line 920
    iget-object v0, v6, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    move-object/from16 v19, v0

    aget-object v19, v19, v8

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;->isValid:I

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 922
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    iget-object v0, v6, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    move-object/from16 v20, v0

    aget-object v20, v20, v8

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;->x:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    mul-float v19, v19, v15

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v7, Landroid/graphics/Rect;->left:I

    .line 923
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    iget-object v0, v6, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    move-object/from16 v20, v0

    aget-object v20, v20, v8

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;->y:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    mul-float v19, v19, v16

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v7, Landroid/graphics/Rect;->top:I

    .line 924
    iget v0, v7, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    iget-object v0, v6, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    move-object/from16 v20, v0

    aget-object v20, v20, v8

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;->width:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v20, v20, v15

    add-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v7, Landroid/graphics/Rect;->right:I

    .line 925
    iget v0, v7, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    iget-object v0, v6, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    move-object/from16 v20, v0

    aget-object v20, v20, v8

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;->height:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    mul-float v20, v20, v16

    add-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    .line 932
    invoke-virtual {v7}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    .line 933
    .local v4, "cx":I
    invoke-virtual {v7}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    .line 934
    .local v5, "cy":I
    const-wide/high16 v20, 0x4000000000000000L    # 2.0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v20

    iget v0, v7, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    iget v0, v7, Landroid/graphics/Rect;->left:I

    move/from16 v22, v0

    sub-int v19, v19, v22

    move/from16 v0, v19

    int-to-double v0, v0

    move-wide/from16 v22, v0

    mul-double v20, v20, v22

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    div-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v12, v0

    .line 935
    .local v12, "r1":F
    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    iget v0, v7, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    div-int/lit8 v19, v19, 0x2

    move/from16 v0, v19

    int-to-float v13, v0

    .line 937
    .local v13, "r2":F
    add-float v19, v12, v13

    const/high16 v20, 0x40000000    # 2.0f

    div-float v14, v19, v20

    .line 939
    .local v14, "radius":F
    int-to-float v0, v4

    move/from16 v19, v0

    int-to-float v0, v5

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2, v14, v10}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 916
    .end local v4    # "cx":I
    .end local v5    # "cy":I
    .end local v12    # "r1":F
    .end local v13    # "r2":F
    .end local v14    # "radius":F
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 1373
    const/4 v0, 0x0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 1378
    const/4 v0, 0x0

    return v0
.end method

.method public getStatusHeight()I
    .locals 3

    .prologue
    .line 1382
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1383
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1384
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1385
    iget v2, v0, Landroid/graphics/Rect;->top:I

    return v2
.end method

.method public initActionbar()V
    .locals 1

    .prologue
    .line 470
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->init2DepthActionBar()V

    .line 471
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    .line 474
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 475
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 476
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 480
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 481
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 482
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 487
    :cond_2
    :goto_1
    return-void

    .line 478
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    goto :goto_0

    .line 484
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_2

    .line 485
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1
.end method

.method public initButtons()V
    .locals 5

    .prologue
    .line 437
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v2, :cond_1

    .line 438
    const/4 v0, 0x0

    .line 439
    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v2, :cond_0

    .line 440
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initSubViewButtons(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V

    .line 441
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->showSubBottomButton(I)V

    .line 442
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->setMainBtnListener()V

    .line 444
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 450
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->invalidateViews()V

    .line 452
    .end local v0    # "i":I
    :cond_1
    return-void

    .line 446
    .restart local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 447
    .local v1, "v":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 444
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 964
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 965
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initUndoRedoAllDialog()V

    .line 966
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initSaveOptionDialog()V

    .line 967
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initShareViaDialog()V

    .line 968
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initSetAsDialog()V

    .line 969
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initSaveYesNoCancelForFinish()V

    .line 970
    return-void
.end method

.method public initEffect()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 110
    return-void
.end method

.method public initProgressText()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1074
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mProgressTextRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 1075
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mProgressTextRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1083
    :goto_0
    return-void

    .line 1077
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mProgressTextRect:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 1305
    return-void
.end method

.method public initTrayLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1308
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 1310
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 1312
    :cond_0
    return-void
.end method

.method public initView()V
    .locals 0

    .prologue
    .line 118
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 1247
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1248
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledSave()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1250
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1262
    :goto_0
    return-void

    .line 1254
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_1

    .line 1256
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 1257
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 1258
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1260
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1198
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const v2, 0x7f0900bb

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1199
    .local v0, "assitLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1203
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1204
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPaint:Landroid/graphics/Paint;

    .line 1205
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mClearPaint:Landroid/graphics/Paint;

    .line 1206
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mLinePaint:Landroid/graphics/Paint;

    .line 1207
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 1209
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 1210
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1212
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeCancelPressState(Z)V

    .line 1214
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1215
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_0

    .line 1216
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->free()V

    .line 1217
    :cond_0
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1219
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->Destroy()V

    .line 1220
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1222
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    .line 1224
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1225
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1227
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mRedEyeAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    if-eqz v1, :cond_2

    .line 1228
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mRedEyeAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->destroy()V

    .line 1231
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    if-eqz v1, :cond_3

    .line 1233
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->destroy()V

    .line 1234
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    .line 1239
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_4

    .line 1240
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1243
    :cond_4
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    .prologue
    const/16 v12, 0xff

    const/4 v2, 0x0

    .line 975
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_2

    .line 977
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDrawOriginal:Z

    if-eqz v1, :cond_3

    .line 979
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 980
    .local v11, "tempOriginalViewBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v11}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 981
    .local v0, "tempCanvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 983
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 986
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 987
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 988
    const/4 v8, 0x1

    .line 989
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 981
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 990
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mColor:I

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPaint:Landroid/graphics/Paint;

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mClearPaint:Landroid/graphics/Paint;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mLinePaint:Landroid/graphics/Paint;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    move-object v3, p1

    move-object v4, v11

    invoke-static/range {v3 .. v10}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawCanvasWithPath(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;ILandroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    .line 991
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    .line 1005
    .end local v0    # "tempCanvas":Landroid/graphics/Canvas;
    .end local v11    # "tempOriginalViewBitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    packed-switch v1, :pswitch_data_0

    .line 1025
    :goto_1
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    const v3, 0x17001701

    if-eq v1, v3, :cond_1

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    const v3, 0x17001702

    if-eq v1, v3, :cond_1

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    const v3, 0x17001703

    if-ne v1, v3, :cond_2

    .line 1027
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    if-eqz v1, :cond_2

    .line 1030
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->isReverseAnimRunning:Z

    if-eqz v1, :cond_6

    .line 1034
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mAlpha:I

    if-lez v1, :cond_5

    .line 1037
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1038
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDrawOriginal:Z

    .line 1039
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mAlpha:I

    .line 1037
    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    .line 1040
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mAlpha:I

    add-int/lit8 v1, v1, -0xf

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mAlpha:I

    .line 1042
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1069
    :cond_2
    :goto_2
    return-void

    .line 996
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 998
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 999
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mColor:I

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPaint:Landroid/graphics/Paint;

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mClearPaint:Landroid/graphics/Paint;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mLinePaint:Landroid/graphics/Paint;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-static/range {v3 .. v10}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawCanvasWithPath(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;ILandroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 1001
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPaint:Landroid/graphics/Paint;

    invoke-static {p1, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 1008
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mRedEyeAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;

    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView$RedEyeAnimation;->drawCanvas(Landroid/graphics/Canvas;)V

    .line 1009
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->drawFaceRect(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 1012
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->drawFaceRect(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 1016
    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->drawFaceRect(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 1020
    :pswitch_3
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->drawFaceRect(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 1046
    :cond_5
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->resetPreProgressValue()V

    .line 1047
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->isReverseAnimRunning:Z

    goto :goto_2

    .line 1051
    :cond_6
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mAlpha:I

    if-gt v1, v12, :cond_7

    .line 1053
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1054
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDrawOriginal:Z

    .line 1055
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mAlpha:I

    .line 1053
    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    .line 1056
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mAlpha:I

    add-int/lit8 v1, v1, 0xf

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mAlpha:I

    goto :goto_2

    .line 1060
    :cond_7
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1061
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentEffectType:I

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDrawOriginal:Z

    .line 1060
    invoke-virtual {v1, p1, v2, v3, v12}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    goto :goto_2

    .line 1005
    :pswitch_data_0
    .packed-switch 0x17001700
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 1265
    const/16 v1, 0x17

    if-eq p1, v1, :cond_0

    const/16 v1, 0x42

    if-ne p1, v1, :cond_3

    .line 1267
    :cond_0
    const/4 v0, 0x0

    .line 1268
    .local v0, "isFocused":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_1

    .line 1269
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    move-result v0

    .line 1272
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_2

    .line 1273
    if-nez v0, :cond_2

    .line 1274
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 1297
    .end local v0    # "isFocused":Z
    :cond_2
    :goto_0
    return v2

    .line 1279
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 1281
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->backPressed()V

    goto :goto_0
.end method

.method public onLayout()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1389
    const/4 v1, 0x0

    .line 1391
    .local v1, "previewBuffer":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 1392
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 1393
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1394
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewHeight()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 1396
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1397
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 1398
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1401
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1404
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1405
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 1406
    const/4 v8, 0x1

    .line 1407
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 1399
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1409
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mPortraitEffect:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->onConfigurationChanged()V

    .line 1410
    return-void
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 1367
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->runOptionItem(I)Z

    .line 1368
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1325
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->msave:Z

    if-eqz v0, :cond_0

    .line 1326
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1327
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mCurrentSaveSize:I

    const v1, 0x7a1200

    if-ne v0, v1, :cond_3

    .line 1328
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cb

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setSelectedFalseAnotherButtons(I)V

    .line 1331
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->msave:Z

    .line 1334
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_1

    .line 1335
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 1336
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1337
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 1338
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1351
    :cond_2
    return-void

    .line 1330
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601f0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setSelectedFalseAnotherButtons(I)V

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1354
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->pause()V

    .line 1355
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f090158

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1356
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->msetas:Z

    .line 1357
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 1359
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1360
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->msave:Z

    .line 1361
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 1363
    :cond_1
    return-void
.end method

.method public refreshView()V
    .locals 3

    .prologue
    .line 2470
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->getImageEditViewHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 2472
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->refreshImageAndBottomButtons()V

    .line 2473
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 1

    .prologue
    .line 1087
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 1088
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 1089
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 1090
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 1091
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 1092
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_0

    .line 1093
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 1095
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initByConfigPortraitEffect()V

    .line 1096
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->initProgressText()V

    .line 1098
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/PortraitView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPopupLayout()V

    .line 1101
    return-void
.end method
