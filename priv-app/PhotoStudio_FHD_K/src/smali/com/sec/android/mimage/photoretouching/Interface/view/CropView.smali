.class public Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "CropView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$CropLassoOnLayoutCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$MultiTouchScaleGestureListener;
    }
.end annotation


# instance fields
.field private final NUM_FRAMES:I

.field private final drawRunner:Ljava/lang/Runnable;

.field i:I

.field private isReverseAnimRunning:Z

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mAlpha:I

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mContext:Landroid/content/Context;

.field private mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

.field private mCropHandler:Landroid/os/Handler;

.field private mDarkBGBitmap:Landroid/graphics/Bitmap;

.field private mDrawAnimRunnable:Ljava/lang/Runnable;

.field private mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

.field private mHandler:Landroid/os/Handler;

.field private mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIsLassoPressed:Z

.field private mIsMultiTouch:Z

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mPaint:Landroid/graphics/Paint;

.field private mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

.field private mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

.field public mShouldReverseAnimate:Z

.field public mStartAnimation:Z

.field private mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 191
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->i:I

    .line 192
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->drawRunner:Ljava/lang/Runnable;

    .line 1195
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->NUM_FRAMES:I

    .line 1196
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1197
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    .line 1198
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1199
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1200
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1201
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1202
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPaint:Landroid/graphics/Paint;

    .line 1203
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 1204
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    .line 1205
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 1206
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1207
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1208
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 1210
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 1212
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 1214
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1215
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mDarkBGBitmap:Landroid/graphics/Bitmap;

    .line 1216
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1217
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mIsMultiTouch:Z

    .line 1219
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->isReverseAnimRunning:Z

    .line 1220
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropHandler:Landroid/os/Handler;

    .line 1222
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mAlpha:I

    .line 1223
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mShouldReverseAnimate:Z

    .line 1224
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mStartAnimation:Z

    .line 1225
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mIsLassoPressed:Z

    .line 1227
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    .line 77
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHandler:Landroid/os/Handler;

    .line 78
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    .line 79
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 80
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 81
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 82
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropHandler:Landroid/os/Handler;

    .line 83
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->setInterface(Ljava/lang/Object;)V

    .line 85
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPaint:Landroid/graphics/Paint;

    .line 86
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 89
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetSuPMatrix()V

    .line 91
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 93
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->setViewLayerType(I)V

    .line 95
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    .line 96
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$MultiTouchScaleGestureListener;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$MultiTouchScaleGestureListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    invoke-direct {v0, v1, v3}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;)V

    .line 95
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 98
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-direct {v0, p1, v4}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 99
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 135
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    const/high16 v1, 0x11200000

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$4;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    invoke-direct {v0, p1, v1, v3}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;-><init>(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 143
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->init(I)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 147
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 149
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 152
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 153
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 147
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mDarkBGBitmap:Landroid/graphics/Bitmap;

    .line 155
    new-instance v8, Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mDarkBGBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v8, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 156
    .local v8, "c":Landroid/graphics/Canvas;
    const/high16 v0, -0x80000000

    invoke-virtual {v8, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 157
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    .locals 1

    .prologue
    .line 1204
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 1220
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->drawRunner:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1197
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V
    .locals 0

    .prologue
    .line 1208
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;Z)V
    .locals 0

    .prologue
    .line 1217
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mIsMultiTouch:Z

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    .locals 1

    .prologue
    .line 1205
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 465
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->onButtonClicked(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .locals 1

    .prologue
    .line 1201
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 1200
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Z
    .locals 1

    .prologue
    .line 1225
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mIsLassoPressed:Z

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;Z)V
    .locals 0

    .prologue
    .line 1225
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mIsLassoPressed:Z

    return-void
.end method

.method private checkHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1081
    const/4 v0, 0x0

    .line 1082
    .local v0, "isNoShow":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->isDoNotShowPopup(Landroid/content/Context;I)Z

    move-result v0

    .line 1084
    if-nez v0, :cond_0

    .line 1086
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-nez v1, :cond_0

    .line 1088
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->initHelpPopup(I)V

    .line 1089
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->showCurrentModeHelpPopup()V

    .line 1092
    :cond_0
    return-void
.end method

.method private initCropAutoReframe([Landroid/graphics/Rect;)Z
    .locals 26
    .param p1, "autoReframeRect"    # [Landroid/graphics/Rect;

    .prologue
    .line 914
    const/16 v25, 0x0

    .line 915
    .local v25, "ret":Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    mul-int/2addr v3, v4

    new-array v2, v3, [I

    .line 916
    .local v2, "data":[I
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    move/from16 v0, v22

    if-lt v0, v3, :cond_0

    .line 926
    const/4 v3, 0x7

    new-array v5, v3, [I

    .line 927
    .local v5, "startX":[I
    const/4 v3, 0x7

    new-array v6, v3, [I

    .line 928
    .local v6, "startY":[I
    const/4 v3, 0x7

    new-array v7, v3, [I

    .line 929
    .local v7, "rect_widht":[I
    const/4 v3, 0x7

    new-array v8, v3, [I

    .line 931
    .local v8, "rect_height":[I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v17

    .line 932
    .local v17, "am":Landroid/content/res/AssetManager;
    const/4 v3, 0x4

    new-array v0, v3, [[B

    move-object/from16 v18, v0

    .line 933
    .local v18, "croppingData":[[B
    const/4 v3, 0x4

    new-array v0, v3, [I

    move-object/from16 v20, v0

    .line 934
    .local v20, "croppingDataSize":[I
    const/4 v3, 0x4

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/4 v3, 0x0

    const-string v4, "landscape.cl"

    aput-object v4, v19, v3

    const/4 v3, 0x1

    const-string v4, "portrait.cl"

    aput-object v4, v19, v3

    const/4 v3, 0x2

    const-string v4, "landscape_2.cl"

    aput-object v4, v19, v3

    const/4 v3, 0x3

    const-string v4, "portrait_2.cl"

    aput-object v4, v19, v3

    .line 937
    .local v19, "croppingDataFileName":[Ljava/lang/String;
    const/16 v22, 0x0

    :goto_1
    :try_start_0
    move-object/from16 v0, v19

    array-length v3, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move/from16 v0, v22

    if-lt v0, v3, :cond_1

    .line 952
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    .line 954
    const/4 v9, 0x0

    aget-object v9, v18, v9

    const/4 v10, 0x0

    aget v10, v20, v10

    const/4 v11, 0x1

    aget-object v11, v18, v11

    const/4 v12, 0x1

    aget v12, v20, v12

    .line 955
    const/4 v13, 0x2

    aget-object v13, v18, v13

    const/4 v14, 0x2

    aget v14, v20, v14

    const/4 v15, 0x3

    aget-object v15, v18, v15

    const/16 v16, 0x3

    aget v16, v20, v16

    .line 952
    invoke-static/range {v2 .. v16}, Lcom/sec/android/app/autoreframe/jni;->getSuggestedImageInfo([III[I[I[I[I[BI[BI[BI[BI)V

    .line 958
    const/16 v22, 0x0

    :goto_3
    const/4 v3, 0x7

    move/from16 v0, v22

    if-lt v0, v3, :cond_3

    .line 979
    const/4 v5, 0x0

    .line 980
    const/4 v6, 0x0

    .line 981
    const/4 v7, 0x0

    .line 982
    const/4 v8, 0x0

    .line 984
    const/4 v3, 0x1

    aget-object v3, p1, v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    .line 985
    const/4 v3, 0x1

    aget-object v3, p1, v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    if-ltz v3, :cond_8

    const/4 v3, 0x1

    aget-object v3, p1, v3

    iget v3, v3, Landroid/graphics/Rect;->top:I

    if-ltz v3, :cond_8

    const/4 v3, 0x1

    aget-object v3, p1, v3

    iget v3, v3, Landroid/graphics/Rect;->right:I

    if-ltz v3, :cond_8

    const/4 v3, 0x1

    aget-object v3, p1, v3

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    if-ltz v3, :cond_8

    .line 987
    const/16 v25, 0x1

    .line 997
    :goto_4
    return v25

    .line 918
    .end local v5    # "startX":[I
    .end local v6    # "startY":[I
    .end local v7    # "rect_widht":[I
    .end local v8    # "rect_height":[I
    .end local v17    # "am":Landroid/content/res/AssetManager;
    .end local v18    # "croppingData":[[B
    .end local v19    # "croppingDataFileName":[Ljava/lang/String;
    .end local v20    # "croppingDataSize":[I
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v3

    .line 919
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    mul-int v4, v4, v22

    .line 920
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v9

    .line 918
    invoke-static {v3, v4, v9}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v23

    .line 922
    .local v23, "in":Ljava/nio/IntBuffer;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    mul-int v3, v3, v22

    .line 923
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 921
    invoke-static {v2, v3, v4}, Ljava/nio/IntBuffer;->wrap([III)Ljava/nio/IntBuffer;

    move-result-object v24

    .line 924
    .local v24, "out":Ljava/nio/IntBuffer;
    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/IntBuffer;->put(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    .line 916
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_0

    .line 938
    .end local v23    # "in":Ljava/nio/IntBuffer;
    .end local v24    # "out":Ljava/nio/IntBuffer;
    .restart local v5    # "startX":[I
    .restart local v6    # "startY":[I
    .restart local v7    # "rect_widht":[I
    .restart local v8    # "rect_height":[I
    .restart local v17    # "am":Landroid/content/res/AssetManager;
    .restart local v18    # "croppingData":[[B
    .restart local v19    # "croppingDataFileName":[Ljava/lang/String;
    .restart local v20    # "croppingDataSize":[I
    :cond_1
    :try_start_1
    aget-object v3, v19, v22

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v23

    .line 941
    .local v23, "in":Ljava/io/InputStream;
    invoke-virtual/range {v23 .. v23}, Ljava/io/InputStream;->available()I

    move-result v3

    aput v3, v20, v22

    .line 942
    aget v3, v20, v22

    if-lez v3, :cond_2

    .line 943
    aget v3, v20, v22

    new-array v3, v3, [B

    aput-object v3, v18, v22

    .line 944
    aget-object v3, v18, v22

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    .line 946
    :cond_2
    invoke-virtual/range {v23 .. v23}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 937
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_1

    .line 948
    .end local v23    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v21

    .line 949
    .local v21, "e":Ljava/lang/Exception;
    const-string v3, "Cannot read Cropping Data"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 960
    .end local v21    # "e":Ljava/lang/Exception;
    :cond_3
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    aput-object v3, p1, v22

    .line 961
    aget-object v3, p1, v22

    aget v4, v5, v22

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 962
    aget-object v3, p1, v22

    aget v4, v6, v22

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 964
    aget-object v3, p1, v22

    aget v4, v5, v22

    aget v9, v7, v22

    add-int/2addr v4, v9

    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 965
    aget-object v3, p1, v22

    aget v4, v6, v22

    aget v9, v8, v22

    add-int/2addr v4, v9

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    .line 967
    aget-object v3, p1, v22

    iget v3, v3, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    if-lt v3, v4, :cond_4

    .line 968
    aget-object v3, p1, v22

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 969
    :cond_4
    aget-object v3, p1, v22

    iget v3, v3, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    if-lt v3, v4, :cond_5

    .line 970
    aget-object v3, p1, v22

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 972
    :cond_5
    aget-object v3, p1, v22

    iget v3, v3, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    if-lt v3, v4, :cond_6

    .line 973
    aget-object v3, p1, v22

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 975
    :cond_6
    aget-object v3, p1, v22

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    if-lt v3, v4, :cond_7

    .line 976
    aget-object v3, p1, v22

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    .line 958
    :cond_7
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_3

    .line 991
    :cond_8
    const/16 v22, 0x0

    :goto_5
    const/4 v3, 0x7

    move/from16 v0, v22

    if-lt v0, v3, :cond_9

    .line 995
    const/16 p1, 0x0

    goto/16 :goto_4

    .line 993
    :cond_9
    const/4 v3, 0x0

    aput-object v3, p1, v22

    .line 991
    add-int/lit8 v22, v22, 0x1

    goto :goto_5
.end method

.method private initHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1095
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 1096
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$14;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->setHelpPopupCallback(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;)V

    .line 1103
    return-void
.end method

.method private initPinchZoomCallback()V
    .locals 3

    .prologue
    .line 1022
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 1024
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$12;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 1050
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->init(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;)V

    .line 1051
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->resetZoom()V

    .line 1052
    return-void
.end method

.method private onButtonClicked(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v6, 0x11201304

    const v5, 0x11201303

    .line 466
    check-cast p1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getButtonType()I

    move-result v2

    .line 467
    .local v2, "type":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 479
    return-void

    .line 469
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .line 470
    .local v0, "btn":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getButtonType()I

    move-result v3

    if-ne v3, v6, :cond_1

    if-eq v2, v6, :cond_1

    .line 472
    const v3, 0x7f02016b

    const-string v4, "16:9"

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->changeRes(ILjava/lang/String;)V

    .line 474
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getButtonType()I

    move-result v3

    if-ne v3, v5, :cond_2

    if-eq v2, v5, :cond_2

    .line 476
    const v3, 0x7f020164

    const-string v4, "4:3"

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->changeRes(ILjava/lang/String;)V

    .line 467
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v6, 0xff

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 670
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    invoke-virtual {v2, p2}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 673
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 674
    .local v0, "matrix":Landroid/graphics/Matrix;
    const/16 v2, 0x9

    new-array v1, v2, [F

    .line 675
    .local v1, "value":[F
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 676
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v2, :cond_0

    .line 677
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    aget v3, v1, v5

    invoke-virtual {v2, p2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->onTouchEventForSeekbar(Landroid/view/MotionEvent;F)V

    .line 679
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 714
    :cond_1
    :goto_0
    return v4

    .line 681
    :sswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-le v2, v4, :cond_1

    .line 682
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mStartAnimation:Z

    .line 683
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->isReverseAnimRunning:Z

    if-eqz v2, :cond_2

    .line 684
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->isReverseAnimRunning:Z

    .line 685
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mShouldReverseAnimate:Z

    goto :goto_0

    .line 688
    :cond_2
    const/16 v2, 0x32

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mAlpha:I

    goto :goto_0

    .line 697
    :sswitch_1
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mStartAnimation:Z

    if-eqz v2, :cond_1

    .line 698
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->startseek()V

    .line 699
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->isReverseAnimRunning:Z

    .line 700
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mAlpha:I

    if-le v2, v6, :cond_3

    .line 701
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mAlpha:I

    .line 703
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 679
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x3 -> :sswitch_1
        0x105 -> :sswitch_0
    .end sparse-switch
.end method

.method public backPressed()V
    .locals 2

    .prologue
    .line 484
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x11000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 485
    return-void
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 887
    return-void
.end method

.method public doDone()V
    .locals 7

    .prologue
    const/high16 v6, 0x42c80000    # 100.0f

    .line 1000
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    invoke-direct {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/CropEffect;)V

    .line 1002
    .local v4, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->applyPreview()Z

    .line 1004
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1005
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 1006
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 1007
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getEnhance()Z

    move-result v5

    .line 1004
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V

    .line 1009
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->resetZoom()V

    .line 1011
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v6

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v1

    div-float/2addr v0, v1

    cmpg-float v0, v0, v6

    if-gez v0, :cond_1

    .line 1012
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 1015
    :goto_0
    return-void

    .line 1014
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x11000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 898
    const/4 v0, 0x0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 902
    const/4 v0, 0x0

    return v0
.end method

.method public getDrawCanvasRoi()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1018
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getStatusHeight()I
    .locals 3

    .prologue
    .line 906
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 907
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 908
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 909
    iget v2, v0, Landroid/graphics/Rect;->top:I

    return v2
.end method

.method public initActionbar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 488
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 491
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 494
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$10;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    .line 491
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 515
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 518
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$11;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    .line 515
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 550
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 551
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 552
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 554
    :cond_0
    return-void
.end method

.method public initButtons()V
    .locals 4

    .prologue
    const v3, 0x11201306

    .line 211
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_1

    .line 215
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 218
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    .line 217
    invoke-virtual {v0, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x11201302

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x11201303

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$7;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 365
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x11201304

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 428
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x11201305

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$9;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 457
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 458
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    const v1, 0x11201307

    if-ne v0, v1, :cond_1

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 462
    :cond_1
    return-void
.end method

.method public initDialog()V
    .locals 0

    .prologue
    .line 558
    return-void
.end method

.method public initEffect()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 161
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 162
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    .line 163
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->setActionBarManager(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V

    .line 164
    const/4 v2, 0x7

    new-array v0, v2, [Landroid/graphics/Rect;

    .line 165
    .local v0, "autoReframeRect":[Landroid/graphics/Rect;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 166
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->initCropAutoReframe([Landroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 168
    aget-object v2, v0, v5

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 169
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    if-eqz v2, :cond_0

    .line 170
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    const v3, 0x11201307

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->init(Landroid/graphics/Rect;I)V

    .line 179
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->initPinchZoomCallback()V

    .line 180
    return-void

    .line 174
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    if-eqz v2, :cond_2

    .line 175
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    const v3, 0x11201306

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->init(ILcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 176
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    invoke-virtual {v2, v5}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->calculateInitialValues(Z)V

    goto :goto_0
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 638
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 876
    return-void
.end method

.method public initTrayLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 879
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 881
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 883
    :cond_0
    return-void
.end method

.method public initView()V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->setImageEditViewPinchZoomCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;)V

    .line 189
    :cond_0
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 822
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 829
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_0

    .line 831
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 832
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 833
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 835
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 837
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 719
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 721
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 722
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 723
    :cond_0
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 725
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    .line 727
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mContext:Landroid/content/Context;

    .line 729
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_3

    .line 731
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 733
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_4

    .line 735
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPaint:Landroid/graphics/Paint;

    .line 738
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeCancelPressState(Z)V

    .line 740
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_5

    .line 741
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    .line 742
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 744
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_6

    .line 746
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->free()V

    .line 747
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 749
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_7

    .line 750
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 752
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_8

    .line 753
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 755
    :cond_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    if-eqz v0, :cond_9

    .line 756
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 758
    :cond_9
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    if-eqz v0, :cond_a

    .line 760
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->Destroy()V

    .line 761
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    .line 763
    :cond_a
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    if-eqz v0, :cond_b

    .line 765
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 767
    :cond_b
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v0, :cond_c

    .line 769
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 771
    :cond_c
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_d

    .line 773
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 775
    :cond_d
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_e

    .line 777
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 779
    :cond_e
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    if-eqz v0, :cond_f

    .line 781
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 784
    :cond_f
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mDarkBGBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_10

    .line 786
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mDarkBGBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mDarkBGBitmap:Landroid/graphics/Bitmap;

    .line 788
    :cond_10
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_11

    .line 790
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 794
    :cond_11
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v0, :cond_12

    .line 796
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->Destroy()V

    .line 797
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 818
    :cond_12
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/16 v6, 0xff

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 566
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_1

    .line 568
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPaint:Landroid/graphics/Paint;

    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 569
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    if-eqz v0, :cond_1

    .line 571
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    const v1, 0x11201305

    if-eq v0, v1, :cond_5

    .line 573
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 578
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mDarkBGBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->drawCropImageWithBackground(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 589
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mIsMultiTouch:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-nez v0, :cond_3

    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mShouldReverseAnimate:Z

    if-eqz v0, :cond_4

    .line 593
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->isReverseAnimRunning:Z

    if-eqz v0, :cond_7

    .line 597
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mAlpha:I

    if-lez v0, :cond_6

    .line 600
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 602
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mAlpha:I

    .line 600
    invoke-virtual {v0, p1, v5, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    .line 603
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mAlpha:I

    add-int/lit8 v0, v0, -0xf

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mAlpha:I

    .line 605
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 632
    :cond_4
    :goto_1
    return-void

    .line 583
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mDarkBGBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->drawCropFreeBdry(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 609
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->resetPreProgressValue()V

    .line 610
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->isReverseAnimRunning:Z

    .line 611
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mShouldReverseAnimate:Z

    .line 612
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mStartAnimation:Z

    goto :goto_1

    .line 615
    :cond_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mShouldReverseAnimate:Z

    .line 617
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mAlpha:I

    if-gt v0, v6, :cond_8

    .line 619
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 621
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mAlpha:I

    .line 619
    invoke-virtual {v0, p1, v5, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    .line 622
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mAlpha:I

    add-int/lit8 v0, v0, 0xf

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mAlpha:I

    goto :goto_1

    .line 626
    :cond_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v0, p1, v5, v4, v6}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    goto :goto_1
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 840
    const/16 v0, 0x17

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-ne p1, v0, :cond_3

    .line 842
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 843
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    .line 846
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_2

    .line 847
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 869
    :cond_2
    :goto_0
    return v1

    .line 851
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 853
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->backPressed()V

    goto :goto_0
.end method

.method public onLayout()V
    .locals 4

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 1057
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1058
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->getImageEditViewHeight()I

    move-result v2

    .line 1059
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$13;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V

    .line 1058
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(IILcom/sec/android/mimage/photoretouching/Core/ImageData$setViewSizeMatrixCallback;)V

    .line 1073
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    if-eqz v0, :cond_0

    .line 1074
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->configurationChanged()V

    .line 1076
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->invalidateViews()V

    .line 1077
    return-void
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 895
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 891
    return-void
.end method

.method public refreshView()V
    .locals 0

    .prologue
    .line 1243
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 2

    .prologue
    .line 642
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 643
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 644
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 646
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_0

    .line 647
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 649
    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    const v1, 0x11201305

    if-ne v0, v1, :cond_1

    .line 651
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mIsLassoPressed:Z

    .line 654
    :cond_1
    return-void
.end method
