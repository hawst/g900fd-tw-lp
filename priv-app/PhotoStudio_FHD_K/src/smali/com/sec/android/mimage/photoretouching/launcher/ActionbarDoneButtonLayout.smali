.class public Lcom/sec/android/mimage/photoretouching/launcher/ActionbarDoneButtonLayout;
.super Landroid/widget/LinearLayout;
.source "ActionbarDoneButtonLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 17
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/ActionbarDoneButtonLayout;->init()V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/ActionbarDoneButtonLayout;->init()V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/ActionbarDoneButtonLayout;->init()V

    .line 29
    return-void
.end method


# virtual methods
.method public init()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public setEnabled(Z)V
    .locals 5
    .param p1, "enabled"    # Z

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const v3, 0x3f19999a    # 0.6f

    .line 36
    const v2, 0x7f090015

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/launcher/ActionbarDoneButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 37
    .local v0, "icon":Landroid/widget/ImageView;
    const v2, 0x7f090016

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/launcher/ActionbarDoneButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 40
    .local v1, "text":Landroid/widget/TextView;
    if-eqz p1, :cond_2

    .line 42
    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 44
    :cond_0
    if-eqz v1, :cond_1

    .line 45
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 54
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 55
    return-void

    .line 49
    :cond_2
    if-eqz v0, :cond_3

    .line 50
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 51
    :cond_3
    if-eqz v1, :cond_1

    .line 52
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0
.end method
