.class Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;
.super Ljava/lang/Object;
.source "HistoryManager.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addHistory([III)V
    .locals 9
    .param p1, "bitmap"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/16 v6, 0x1e

    .line 529
    if-eqz p1, :cond_3

    .line 531
    const-string v4, "JW addHistory"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 532
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-direct {v0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)V

    .line 533
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iput-object p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->data:[I

    .line 534
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    .line 535
    iput p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    .line 536
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "original"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getFileName()Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    .line 537
    sget-object v4, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->HISTORY_PATH:Ljava/lang/String;

    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    .line 538
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 540
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v6, :cond_0

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)I

    move-result v4

    if-le v4, v6, :cond_0

    .line 542
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 543
    .local v1, "hi":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->deleteFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;I)V

    .line 547
    .end local v1    # "hi":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isOriginalRedo()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 548
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 549
    .local v3, "listSize":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)I

    move-result v2

    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_4

    .line 558
    .end local v2    # "i":I
    .end local v3    # "listSize":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559
    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->data:[I

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->width:I

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->height:I

    invoke-static {v4, v5, v6, v7, v8}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->saveData(Ljava/lang/String;Ljava/lang/String;[III)Z

    .line 560
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;I)V

    .line 561
    const/4 v4, 0x0

    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->data:[I

    .line 563
    :cond_2
    const-string v4, "JW addHistory end"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 565
    .end local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    :cond_3
    return-void

    .line 551
    .restart local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    .restart local v2    # "i":I
    .restart local v3    # "listSize":I
    :cond_4
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 552
    .restart local v1    # "hi":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->directory:Ljava/lang/String;

    iget-object v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;->fileName:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->deleteFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public redo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 3
    .param p1, "imagedata"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 595
    const/4 v0, 0x0

    .line 596
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 598
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 600
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;I)V

    .line 601
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gt v1, v2, :cond_2

    .line 603
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 611
    .restart local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v1, v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->loadOriginalData(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 612
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 613
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setSaved()V

    .line 615
    :cond_1
    return-void

    .line 607
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;I)V

    goto :goto_0
.end method

.method public redoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 3
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 630
    const/4 v0, 0x0

    .line 631
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 633
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 635
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .line 638
    .restart local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;I)V

    .line 639
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v1, v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->loadOriginalData(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 640
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 641
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setSaved()V

    .line 642
    :cond_1
    return-void
.end method

.method public undo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 3
    .param p1, "imagedata"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 570
    const/4 v0, 0x0

    .line 572
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 574
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 576
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;I)V

    .line 577
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_2

    .line 579
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->undoAllOriginal()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    move-result-object v0

    .line 587
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v1, v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->loadOriginalData(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 588
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 589
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setSaved()V

    .line 590
    :cond_1
    return-void

    .line 583
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalHistoryList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mOriginalCurrentIndex:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    .restart local v0    # "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    goto :goto_0
.end method

.method public undoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 3
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    .line 620
    const/4 v0, 0x0

    .line 621
    .local v0, "hInfo":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstOriginal:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 622
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->mFirstOriginal:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    move-result-object v0

    .line 623
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;I)V

    .line 624
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v1, v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->loadOriginalData(Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 625
    return-void
.end method
