.class public Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;
.super Ljava/lang/Object;
.source "SubViewButtonsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;,
        Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ThumbnailAsyncCallback;
    }
.end annotation


# static fields
.field private static final MAX_THUMB_SIZE:I = 0x80000


# instance fields
.field private mAnimate:Z

.field private mArrowLayout:Landroid/widget/LinearLayout;

.field private mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

.field private mBottomLayout:Landroid/widget/LinearLayout;

.field private mButtonInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentSelectedButton:Landroid/widget/LinearLayout;

.field private mEffectButtonInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

.field private mEffectRecentListUsed:Z

.field private mHandler:Landroid/os/Handler;

.field private mHorizontalInnerLayout:Landroid/widget/LinearLayout;

.field private mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mIsShowing:Z

.field private mMainButton:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mMiddleButton:Landroid/widget/Button;

.field private mRecentBtnCount:I

.field private mRecentButtonInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRecentButtonLayout:Landroid/widget/LinearLayout;

.field private mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mViewWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewButtonsManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p3, "effectEffect"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 550
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHandler:Landroid/os/Handler;

    .line 2662
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    .line 2663
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mInflater:Landroid/view/LayoutInflater;

    .line 2664
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mCurrentSelectedButton:Landroid/widget/LinearLayout;

    .line 2665
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    .line 2666
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    .line 2667
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    .line 2668
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    .line 2669
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    .line 2671
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    .line 2672
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonLayout:Landroid/widget/LinearLayout;

    .line 2673
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mAnimate:Z

    .line 2674
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mIsShowing:Z

    .line 2675
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewWidth:I

    .line 2677
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 2679
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 2680
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mButtonInfoList:Ljava/util/ArrayList;

    .line 2681
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonInfoList:Ljava/util/ArrayList;

    .line 2682
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectButtonInfoList:Ljava/util/ArrayList;

    .line 2683
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectRecentListUsed:Z

    .line 2684
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentBtnCount:I

    .line 2686
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .line 77
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    .line 80
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    .line 81
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 82
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0900b6

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    .line 84
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 96
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mInflater:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mInflater:Landroid/view/LayoutInflater;

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0900b8

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    .line 99
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0900b9

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    .line 100
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setBottomLayoutVisible(I)V

    .line 101
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->initDataBase(Landroid/content/Context;)V

    .line 103
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->initMiddleButton(I)V

    .line 105
    instance-of v0, p3, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    if-eqz v0, :cond_1

    .line 106
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0201b7

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->addView(Landroid/view/View;)V

    .line 111
    return-void

    .line 92
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->initHorizontalScrollView()V

    goto :goto_0

    .line 84
    nop

    :sswitch_data_0
    .sparse-switch
        0x11000000 -> :sswitch_0
        0x14000000 -> :sswitch_0
        0x15000000 -> :sswitch_0
        0x16000000 -> :sswitch_0
        0x18000000 -> :sswitch_0
        0x1d000000 -> :sswitch_0
        0x31000000 -> :sswitch_0
    .end sparse-switch
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .locals 1

    .prologue
    .line 2677
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;Z)V
    .locals 0

    .prologue
    .line 2673
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mAnimate:Z

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 2666
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 2669
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 2662
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    .locals 1

    .prologue
    .line 2668
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)I
    .locals 1

    .prologue
    .line 2675
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewWidth:I

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Z
    .locals 1

    .prologue
    .line 2673
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mAnimate:Z

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 2671
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;Z)V
    .locals 0

    .prologue
    .line 2674
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mIsShowing:Z

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 2665
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private doInitialScroll()V
    .locals 14

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 1296
    const/4 v8, 0x0

    .line 1297
    .local v8, "scrollWidth":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v11

    const/high16 v12, 0x16000000

    if-ne v11, v12, :cond_4

    move v2, v9

    .line 1298
    .local v2, "isEffectMode":Z
    :goto_0
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentBtnCount:I

    .line 1299
    .local v3, "offset":I
    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentBtnCount:I

    if-lez v11, :cond_1

    .line 1300
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v11, v10, v10}, Landroid/widget/LinearLayout;->measure(II)V

    .line 1301
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v5

    .line 1302
    .local v5, "recentLayoutWidth":I
    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v11

    if-eqz v11, :cond_0

    const/16 v10, 0x32

    :cond_0
    sub-int v10, v5, v10

    add-int/2addr v8, v10

    .line 1304
    .end local v5    # "recentLayoutWidth":I
    :cond_1
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v10, :cond_2

    .line 1305
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setAnimation(Z)V

    .line 1306
    :cond_2
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 1307
    .local v1, "childCount":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v9

    if-eqz v9, :cond_5

    const/16 v6, 0x9

    .line 1308
    .local v6, "scrollBtnCount":I
    :goto_1
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f05024b

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1309
    .local v4, "padding":I
    :goto_2
    if-ge v3, v6, :cond_3

    if-lt v3, v1, :cond_7

    .line 1317
    :cond_3
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v9, Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v9

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Display;->getWidth()I

    move-result v9

    sub-int v7, v8, v9

    .line 1318
    .local v7, "scrollTo":I
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    new-instance v11, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;

    invoke-direct {v11, p0, v1, v6, v7}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;III)V

    .line 1364
    if-eqz v2, :cond_9

    const/16 v9, 0x190

    :goto_3
    int-to-long v12, v9

    .line 1318
    invoke-virtual {v10, v11, v12, v13}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1366
    return-void

    .end local v1    # "childCount":I
    .end local v2    # "isEffectMode":Z
    .end local v3    # "offset":I
    .end local v4    # "padding":I
    .end local v6    # "scrollBtnCount":I
    .end local v7    # "scrollTo":I
    :cond_4
    move v2, v10

    .line 1297
    goto :goto_0

    .line 1307
    .restart local v1    # "childCount":I
    .restart local v2    # "isEffectMode":Z
    .restart local v3    # "offset":I
    :cond_5
    if-eqz v2, :cond_6

    const/4 v6, 0x5

    goto :goto_1

    :cond_6
    const/4 v6, 0x6

    goto :goto_1

    .line 1310
    .restart local v4    # "padding":I
    .restart local v6    # "scrollBtnCount":I
    :cond_7
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1311
    .local v0, "child":Landroid/view/View;
    instance-of v9, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    if-eqz v9, :cond_8

    .line 1312
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .end local v0    # "child":Landroid/view/View;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->getLayoutWidth()I

    move-result v9

    mul-int/lit8 v10, v4, 0x2

    add-int/2addr v9, v10

    add-int/2addr v8, v9

    .line 1315
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1314
    .restart local v0    # "child":Landroid/view/View;
    :cond_8
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v8, v9

    goto :goto_4

    .line 1364
    .end local v0    # "child":Landroid/view/View;
    .restart local v7    # "scrollTo":I
    :cond_9
    const/16 v9, 0x64

    goto :goto_3
.end method

.method private getRecentLayoutMargin()I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1801
    const/4 v3, 0x0

    .line 1803
    .local v3, "ret":I
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->isHaveRecentButtons()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1805
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f030066

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 1807
    .local v2, "recentLayout":Landroid/widget/LinearLayout;
    const v4, 0x7f090130

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1808
    .local v1, "iconLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v7, v7}, Landroid/widget/LinearLayout;->measure(II)V

    .line 1810
    const v4, 0x7f090132

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1811
    .local v0, "dividerLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v7, v7}, Landroid/widget/LinearLayout;->measure(II)V

    .line 1813
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v5

    add-int v3, v4, v5

    .line 1816
    .end local v0    # "dividerLayout":Landroid/widget/LinearLayout;
    .end local v1    # "iconLayout":Landroid/widget/LinearLayout;
    .end local v2    # "recentLayout":Landroid/widget/LinearLayout;
    :cond_0
    return v3
.end method

.method private initDataBase(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    new-instance v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 116
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->openEffect()Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 117
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->open()Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 118
    const/4 v6, 0x0

    .line 119
    .local v6, "cs":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 120
    .local v8, "effectCs":Landroid/database/Cursor;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 134
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mButtonInfoList:Ljava/util/ArrayList;

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonInfoList:Ljava/util/ArrayList;

    .line 137
    if-eqz v8, :cond_0

    .line 139
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getEffectButtonInfoList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectButtonInfoList:Ljava/util/ArrayList;

    .line 140
    const-string v0, "JW effectCs != null"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 143
    :cond_0
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 148
    :cond_1
    :try_start_0
    const-string v0, "button_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 149
    .local v2, "buttonId":I
    const-string v0, "button_idx"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 150
    .local v3, "buttonIdx":I
    const-string v0, "recent_used"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 151
    .local v4, "buttonRecent":I
    const-string v0, "use"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 153
    .local v5, "buttonUse":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mButtonInfoList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;IIII)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "moveToNext:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    .end local v2    # "buttonId":I
    .end local v3    # "buttonIdx":I
    .end local v4    # "buttonRecent":I
    .end local v5    # "buttonUse":I
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 160
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->close()V

    .line 161
    return-void

    .line 123
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentColorButtonInfo()Landroid/database/Cursor;

    move-result-object v6

    .line 124
    goto :goto_0

    .line 126
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentEffectButtonInfo()Landroid/database/Cursor;

    move-result-object v6

    .line 127
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryEffectButtonInfo()Landroid/database/Cursor;

    move-result-object v8

    .line 129
    goto/16 :goto_0

    .line 131
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentPortraitButtonInfo()Landroid/database/Cursor;

    move-result-object v6

    goto/16 :goto_0

    .line 155
    :catch_0
    move-exception v7

    .line 156
    .local v7, "e":Landroid/database/CursorIndexOutOfBoundsException;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SubViewButtonManager - initDataBase() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Landroid/database/CursorIndexOutOfBoundsException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_1

    .line 120
    nop

    :sswitch_data_0
    .sparse-switch
        0x15000000 -> :sswitch_0
        0x16000000 -> :sswitch_1
        0x18000000 -> :sswitch_2
    .end sparse-switch
.end method

.method private initEffectManagerButton()V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/high16 v11, 0x42000000    # 32.0f

    const/4 v10, 0x1

    .line 715
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v2, 0x7f030035

    invoke-direct {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 717
    .local v0, "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    const/4 v5, 0x0

    .line 719
    .local v5, "side":I
    const v1, 0x7f090005

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 720
    .local v8, "textView":Landroid/widget/TextView;
    if-eqz v8, :cond_0

    .line 721
    invoke-virtual {v8}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    .line 722
    .local v7, "param":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v10, v11, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v7, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 723
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 725
    .end local v7    # "param":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 726
    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 729
    const v1, 0x16001630

    .line 730
    const v2, 0x7f020397

    .line 731
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f06018f

    invoke-static {v3, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 729
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    .line 735
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 736
    .local v6, "icon":Landroid/widget/ImageView;
    if-eqz v6, :cond_1

    .line 738
    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    .line 739
    .restart local v7    # "param":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v10, v11, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v7, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 740
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v10, v11, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v7, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 741
    const/high16 v1, 0x40e00000    # 7.0f

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v10, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v7, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 742
    iput v10, v7, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 743
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 746
    .end local v7    # "param":Landroid/widget/FrameLayout$LayoutParams;
    :cond_1
    invoke-virtual {v0, v10}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setRecent(Z)V

    .line 747
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 749
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 757
    return-void
.end method

.method private initMiddleButton(I)V
    .locals 14
    .param p1, "viewStatus"    # I

    .prologue
    .line 189
    const/16 v0, 0x56

    .line 190
    .local v0, "MIDDLE_BTN_AUTO":I
    const/16 v3, 0x59

    .line 191
    .local v3, "MIDDLE_BTN_SELECTION":I
    const/16 v4, 0x5b

    .line 194
    .local v4, "MIDDLE_BTN_UNSELECTION":I
    const/4 v1, 0x6

    .line 195
    .local v1, "MIDDLE_BTN_LEFT_MARGIN":I
    const/16 v2, 0xe

    .line 198
    .local v2, "MIDDLE_BTN_RIGHT_MARGIN":I
    const/4 v7, 0x0

    .line 199
    .local v7, "leftMargin":I
    const/4 v10, 0x0

    .line 200
    .local v10, "rightMargin":I
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    invoke-virtual {v12}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 201
    .local v9, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget v7, v9, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 202
    iget v10, v9, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 203
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v12

    iget v6, v12, Landroid/util/DisplayMetrics;->density:F

    .line 204
    .local v6, "density":F
    const-string v5, ""

    .line 206
    .local v5, "btnText":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "sj, SVBM - initMiddleButton() - leftMargin : "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " / rightMargin : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 208
    const/4 v8, 0x0

    .line 210
    .local v8, "midBtnClickListener":Landroid/view/View$OnClickListener;
    sparse-switch p1, :sswitch_data_0

    .line 258
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/Button;->setVisibility(I)V

    .line 278
    :goto_0
    return-void

    .line 212
    :sswitch_0
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f060036

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 213
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const/16 v13, 0x56

    invoke-static {v12, v13}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v12

    iput v12, v9, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 214
    const/4 v12, 0x7

    invoke-virtual {v9, v12}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 215
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const/16 v13, 0xe

    invoke-static {v12, v13}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v10

    .line 217
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$2;

    .end local v8    # "midBtnClickListener":Landroid/view/View$OnClickListener;
    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    .line 262
    .restart local v8    # "midBtnClickListener":Landroid/view/View$OnClickListener;
    :goto_1
    if-eqz v8, :cond_0

    .line 263
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    invoke-virtual {v12, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    :cond_0
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-static {v12, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->doNeedUppercaseString(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 269
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->toUppercaseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 270
    .local v11, "uppercaseString":Ljava/lang/String;
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    invoke-virtual {v12, v11}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 275
    .end local v11    # "uppercaseString":Ljava/lang/String;
    :goto_2
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    invoke-virtual {v12, v9}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 276
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    invoke-virtual {v12}, Landroid/widget/Button;->invalidate()V

    .line 277
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    .line 228
    :sswitch_1
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f060021

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 229
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const/4 v13, 0x6

    invoke-static {v12, v13}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v7

    .line 230
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const/16 v13, 0x59

    invoke-static {v12, v13}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v12

    iput v12, v9, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 231
    const/16 v12, 0x9

    invoke-virtual {v9, v12}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 232
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$3;

    .end local v8    # "midBtnClickListener":Landroid/view/View$OnClickListener;
    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    .line 243
    .restart local v8    # "midBtnClickListener":Landroid/view/View$OnClickListener;
    goto :goto_1

    .line 245
    :sswitch_2
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f060193

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 246
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const/4 v13, 0x6

    invoke-static {v12, v13}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v7

    .line 247
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const/16 v13, 0x5b

    invoke-static {v12, v13}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v12

    iput v12, v9, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 248
    const/4 v12, 0x5

    invoke-virtual {v9, v12}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 249
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$4;

    .end local v8    # "midBtnClickListener":Landroid/view/View$OnClickListener;
    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    .line 256
    .restart local v8    # "midBtnClickListener":Landroid/view/View$OnClickListener;
    goto :goto_1

    .line 272
    :cond_1
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    invoke-virtual {v12, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 210
    nop

    :sswitch_data_0
    .sparse-switch
        0x10000000 -> :sswitch_0
        0x14000000 -> :sswitch_2
        0x15000000 -> :sswitch_1
        0x16000000 -> :sswitch_1
    .end sparse-switch
.end method

.method private initRecentButtons([III)V
    .locals 26
    .param p1, "image"    # [I
    .param p2, "imageWidth"    # I
    .param p3, "imageHeight"    # I

    .prologue
    .line 761
    const/16 v19, 0x0

    .line 763
    .local v19, "buttonWidth":I
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->isThumbnailButtonsMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 764
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030067

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonLayout:Landroid/widget/LinearLayout;

    .line 767
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonLayout:Landroid/widget/LinearLayout;

    const v4, 0x7f090131

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    .line 768
    .local v18, "buttonLayout":Landroid/widget/LinearLayout;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 943
    :cond_0
    invoke-virtual/range {v18 .. v18}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-lez v3, :cond_1

    .line 945
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 948
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v3

    const/high16 v4, 0x16000000

    if-ne v3, v4, :cond_1

    .line 949
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectRecentListUsed:Z

    .line 953
    :cond_1
    return-void

    .line 766
    .end local v18    # "buttonLayout":Landroid/widget/LinearLayout;
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030066

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonLayout:Landroid/widget/LinearLayout;

    goto :goto_0

    .line 770
    .restart local v18    # "buttonLayout":Landroid/widget/LinearLayout;
    :sswitch_0
    const/16 v21, 0x3

    .local v21, "i":I
    :goto_1
    if-ltz v21, :cond_0

    .line 771
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_4

    .line 770
    add-int/lit8 v21, v21, -0x1

    goto :goto_1

    .line 771
    :cond_4
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;

    .line 773
    .local v17, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mRecentUsed:Z
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;)Z

    move-result v3

    if-eqz v3, :cond_3

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mButtonIndex:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;)I

    move-result v3

    move/from16 v0, v21

    if-ne v3, v0, :cond_3

    .line 775
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x7f030032

    invoke-direct {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 777
    .local v2, "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    const/4 v7, 0x0

    .line 778
    .local v7, "side":I
    if-nez v21, :cond_5

    .line 779
    const/4 v7, 0x2

    .line 782
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sj, SVBM - initRecentBtns() - i : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 783
    const v3, 0x7f090005

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    .line 784
    .local v23, "textView":Landroid/widget/TextView;
    const/4 v3, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 785
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 787
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mButtonId:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;)I

    move-result v3

    sparse-switch v3, :sswitch_data_1

    .line 856
    :goto_3
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setRecent(Z)V

    .line 857
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 858
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 859
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentBtnCount:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentBtnCount:I

    .line 860
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 861
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v3

    add-int v19, v19, v3

    goto/16 :goto_2

    .line 790
    :sswitch_1
    const v3, 0x15001506

    .line 791
    const v4, 0x7f0200b0

    .line 792
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060036

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 793
    const/4 v6, 0x0

    .line 790
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_3

    .line 800
    :sswitch_2
    const v3, 0x15001500

    .line 801
    const v4, 0x7f0200b2

    .line 802
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060037

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 803
    const/4 v6, 0x0

    .line 800
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_3

    .line 807
    :sswitch_3
    const v3, 0x15001501

    .line 808
    const v4, 0x7f0200b3

    .line 809
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060038

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 810
    const/4 v6, 0x0

    .line 807
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_3

    .line 814
    :sswitch_4
    const v3, 0x15001502

    .line 815
    const v4, 0x7f0200b7

    .line 816
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060039

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 817
    const/4 v6, 0x0

    .line 814
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_3

    .line 821
    :sswitch_5
    const v3, 0x15001607

    .line 822
    const v4, 0x7f0200b6

    .line 823
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060093

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 824
    const/4 v6, 0x0

    .line 821
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto/16 :goto_3

    .line 828
    :sswitch_6
    const v3, 0x15001608

    .line 829
    const v4, 0x7f0200b4

    .line 830
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060095

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 831
    const/4 v6, 0x0

    .line 828
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto/16 :goto_3

    .line 835
    :sswitch_7
    const v3, 0x15001609

    .line 836
    const v4, 0x7f0200b1

    .line 837
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060096

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 838
    const/4 v6, 0x0

    .line 835
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto/16 :goto_3

    .line 842
    :sswitch_8
    const v3, 0x15001505

    .line 843
    const v4, 0x7f0200b8

    .line 844
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060042

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 845
    const/4 v6, 0x0

    .line 842
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto/16 :goto_3

    .line 849
    :sswitch_9
    const v3, 0x15001504

    .line 850
    const v4, 0x7f0200b5

    .line 851
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06003c

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 852
    const/4 v6, 0x0

    .line 849
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto/16 :goto_3

    .line 869
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v7    # "side":I
    .end local v17    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    .end local v21    # "i":I
    .end local v23    # "textView":Landroid/widget/TextView;
    :sswitch_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 871
    const/4 v12, 0x0

    .line 872
    .local v12, "width":I
    const/4 v13, 0x0

    .line 873
    .local v13, "height":I
    const/16 v24, 0x0

    .line 875
    .local v24, "thumbnail":[I
    const/16 v21, 0x3

    .restart local v21    # "i":I
    :goto_4
    if-ltz v21, :cond_0

    .line 876
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_7

    .line 875
    add-int/lit8 v21, v21, -0x1

    goto :goto_4

    .line 876
    :cond_7
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;

    .line 878
    .restart local v17    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;

    .line 880
    .local v20, "effectButtonInfo":Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->getButtonIndex()I

    move-result v5

    move/from16 v0, v21

    if-ne v5, v0, :cond_8

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getId()I

    move-result v5

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->getButtonId()I

    move-result v6

    if-ne v5, v6, :cond_8

    .line 882
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getUse()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->getButtonRecentUsed()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 884
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sj, SVBM - initRecentBtns() - i : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 886
    const/4 v7, 0x0

    .line 887
    .restart local v7    # "side":I
    if-nez v21, :cond_9

    .line 888
    const/4 v7, 0x2

    .line 889
    :cond_9
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    invoke-direct {v2, v4, v5, v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;I)V

    .line 892
    .local v2, "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    const v4, 0x7f090005

    invoke-virtual {v2, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    .line 893
    .restart local v23    # "textView":Landroid/widget/TextView;
    const/4 v4, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 894
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 896
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 897
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentBtnCount:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentBtnCount:I

    .line 898
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 899
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->getLayoutWidth()I

    move-result v12

    .line 900
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->getLayoutHeight()I

    move-result v13

    .line 902
    const/4 v14, 0x0

    .local v14, "resizeWidth":I
    const/4 v15, 0x0

    .line 903
    .local v15, "resizeHeight":I
    const/high16 v16, 0x3f800000    # 1.0f

    .line 904
    .local v16, "scale":F
    int-to-float v4, v12

    int-to-float v5, v13

    div-float v25, v4, v5

    .line 905
    .local v25, "viewRatio":F
    move/from16 v0, p2

    int-to-float v4, v0

    move/from16 v0, p3

    int-to-float v5, v0

    div-float v22, v4, v5

    .line 907
    .local v22, "imgRatio":F
    cmpl-float v4, v25, v22

    if-lez v4, :cond_c

    .line 909
    int-to-float v4, v12

    move/from16 v0, p2

    int-to-float v5, v0

    div-float v16, v4, v5

    .line 910
    move/from16 v0, p3

    int-to-float v4, v0

    mul-float v4, v4, v16

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v15, v4

    .line 911
    move v14, v12

    .line 919
    :goto_6
    mul-int v4, v14, v15

    const/high16 v5, 0x80000

    if-le v4, v5, :cond_a

    .line 920
    const/high16 v4, 0x49000000    # 524288.0f

    mul-int v5, p2, p3

    int-to-float v5, v5

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v0, v4

    move/from16 v16, v0

    .line 921
    move/from16 v0, p2

    int-to-float v4, v0

    mul-float v4, v4, v16

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v14, v4

    .line 922
    move/from16 v0, p3

    int-to-float v4, v0

    mul-float v4, v4, v16

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v15, v4

    :cond_a
    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move/from16 v10, p2

    move/from16 v11, p3

    .line 925
    invoke-direct/range {v8 .. v16}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->makeThumbnail([IIIIIIIF)[I

    move-result-object v24

    .line 928
    if-eqz v24, :cond_b

    .line 929
    move-object/from16 v0, v24

    invoke-virtual {v2, v0, v14, v15}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setThumbnail([III)V

    .line 931
    :cond_b
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->getButtonId()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setId(I)V

    goto/16 :goto_5

    .line 915
    :cond_c
    int-to-float v4, v13

    move/from16 v0, p3

    int-to-float v5, v0

    div-float v16, v4, v5

    .line 916
    move/from16 v0, p2

    int-to-float v4, v0

    mul-float v4, v4, v16

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v14, v4

    .line 917
    move v15, v13

    goto :goto_6

    .line 768
    nop

    :sswitch_data_0
    .sparse-switch
        0x15000000 -> :sswitch_0
        0x16000000 -> :sswitch_a
    .end sparse-switch

    .line 787
    :sswitch_data_1
    .sparse-switch
        0x15001500 -> :sswitch_2
        0x15001501 -> :sswitch_3
        0x15001502 -> :sswitch_4
        0x15001504 -> :sswitch_9
        0x15001505 -> :sswitch_8
        0x15001506 -> :sswitch_1
        0x15001607 -> :sswitch_5
        0x15001608 -> :sswitch_6
        0x15001609 -> :sswitch_7
    .end sparse-switch
.end method

.method private isHaveRecentButtons()Z
    .locals 4

    .prologue
    .line 680
    const/4 v1, 0x0

    .line 681
    .local v1, "ret":Z
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 687
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 697
    :goto_0
    return v1

    .line 684
    :pswitch_0
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectRecentListUsed:Z

    .line 685
    goto :goto_0

    .line 687
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;

    .line 689
    .local v0, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mRecentUsed:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 691
    const/4 v1, 0x1

    .line 692
    goto :goto_0

    .line 681
    :pswitch_data_0
    .packed-switch 0x16000000
        :pswitch_0
    .end packed-switch
.end method

.method private isThumbnailButtonsMode()Z
    .locals 2

    .prologue
    .line 703
    const/4 v0, 0x0

    .line 705
    .local v0, "ret":Z
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 711
    :goto_0
    return v0

    .line 708
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 705
    nop

    :pswitch_data_0
    .packed-switch 0x16000000
        :pswitch_0
    .end packed-switch
.end method

.method private makeThumbnail([IIIIIIIF)[I
    .locals 13
    .param p1, "bitmap"    # [I
    .param p2, "imageWidth"    # I
    .param p3, "imageHeight"    # I
    .param p4, "button_width"    # I
    .param p5, "button_height"    # I
    .param p6, "resizeWidth"    # I
    .param p7, "resizeHeight"    # I
    .param p8, "scale"    # F

    .prologue
    .line 1911
    const/4 v1, 0x0

    .line 1914
    .local v1, "thumbnail":[I
    move v12, p2

    .line 1915
    .local v12, "bitmap_w":I
    move/from16 v11, p3

    .line 1916
    .local v11, "bitmap_h":I
    mul-int v2, p6, p7

    new-array v1, v2, [I

    .line 1917
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "thumbnailINfo :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1918
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "thumbnailINfo resize:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p7

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1920
    const/high16 v2, 0x3f800000    # 1.0f

    div-float v7, v2, p8

    const/4 v8, 0x0

    .line 1921
    const/4 v9, 0x0

    new-instance v10, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    add-int/lit8 v4, p6, -0x1

    .line 1922
    add-int/lit8 v5, p7, -0x1

    invoke-direct {v10, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    move/from16 v2, p6

    move/from16 v3, p7

    move-object v4, p1

    move v5, p2

    move/from16 v6, p3

    .line 1919
    invoke-static/range {v1 .. v10}, Lcom/sec/android/mimage/photoretouching/jni/Util;->native_drawImage([III[IIIFIILandroid/graphics/Rect;)Z

    .line 1923
    return-object v1
.end method

.method private setSubButtonLayoutHeight()V
    .locals 3

    .prologue
    .line 2142
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2144
    .local v0, "bottomLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2161
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2162
    return-void

    .line 2148
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050247

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    goto :goto_0

    .line 2154
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050246

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    goto :goto_0

    .line 2158
    :sswitch_2
    const/4 v1, -0x2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    goto :goto_0

    .line 2144
    nop

    :sswitch_data_0
    .sparse-switch
        0x11000000 -> :sswitch_1
        0x15000000 -> :sswitch_1
        0x16000000 -> :sswitch_0
        0x18000000 -> :sswitch_1
        0x1e110000 -> :sswitch_2
        0x2c000000 -> :sswitch_2
        0x31000000 -> :sswitch_1
    .end sparse-switch
.end method

.method private setSubButtonLayoutMarginBottom()V
    .locals 0

    .prologue
    .line 2139
    return-void
.end method


# virtual methods
.method public adjustSubmenuBarPosition(II)V
    .locals 16
    .param p1, "buttonLeft"    # I
    .param p2, "viewWidth"    # I

    .prologue
    .line 1731
    const/high16 v5, 0x3f800000    # 1.0f

    .line 1733
    .local v5, "density":F
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    if-eqz v12, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 1735
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v12

    iget v5, v12, Landroid/util/DisplayMetrics;->density:F

    .line 1739
    :cond_0
    const/16 v2, 0x10

    .line 1740
    .local v2, "BGImgSideMargin":I
    const/4 v11, 0x0

    .line 1742
    .local v11, "sideMargin":I
    const/4 v7, 0x0

    .line 1744
    .local v7, "layoutLeft":I
    move/from16 v7, p1

    .line 1747
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    if-eqz v12, :cond_1

    .line 1748
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->measure(II)V

    .line 1749
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    invoke-virtual {v12}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 1752
    .local v6, "horizontalParams":Landroid/view/ViewGroup$LayoutParams;
    const/4 v12, -0x2

    iput v12, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1753
    const/4 v12, -0x2

    iput v12, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1754
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    invoke-virtual {v12, v6}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1760
    .end local v6    # "horizontalParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    if-nez v12, :cond_2

    .line 1797
    :goto_0
    return-void

    .line 1763
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/FrameLayout$LayoutParams;

    .line 1764
    .local v8, "menuLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iput v7, v8, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1769
    const/4 v4, -0x1

    .line 1770
    .local v4, "childViewWidth":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_4

    .line 1776
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->getRecentLayoutMargin()I

    move-result v10

    .line 1779
    .local v10, "recentLayoutMargin":I
    move/from16 v0, p2

    if-lt v4, v0, :cond_5

    .line 1780
    iput v11, v8, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1796
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v12, v8}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1770
    .end local v10    # "recentLayoutMargin":I
    :cond_4
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 1771
    .local v3, "childView":Landroid/widget/LinearLayout;
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v3, v13, v14}, Landroid/widget/LinearLayout;->measure(II)V

    .line 1772
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v13

    add-int/2addr v4, v13

    goto :goto_1

    .line 1782
    .end local v3    # "childView":Landroid/widget/LinearLayout;
    .restart local v10    # "recentLayoutMargin":I
    :cond_5
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v12

    const/high16 v13, 0x11000000

    if-eq v12, v13, :cond_6

    .line 1783
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v12

    const/high16 v13, 0x15000000

    if-ne v12, v13, :cond_7

    .line 1785
    :cond_6
    iput v11, v8, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    goto :goto_2

    .line 1788
    :cond_7
    const/high16 v12, 0x41800000    # 16.0f

    mul-float/2addr v12, v5

    float-to-double v12, v12

    const-wide/high16 v14, 0x3fe0000000000000L    # 0.5

    add-double/2addr v12, v14

    double-to-int v11, v12

    .line 1789
    add-int v12, v7, v4

    add-int/2addr v12, v11

    add-int/2addr v12, v10

    move/from16 v0, p2

    if-lt v12, v0, :cond_3

    .line 1790
    add-int v12, v7, v4

    add-int/2addr v12, v11

    add-int/2addr v12, v10

    sub-int v9, v12, p2

    .line 1791
    .local v9, "moveLeft":I
    iget v12, v8, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v12, v9

    iput v12, v8, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    goto :goto_2
.end method

.method public changeLanguage()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2351
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v1, v5, :cond_0

    .line 2540
    return-void

    .line 2352
    :cond_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2353
    .local v0, "button":Landroid/view/View;
    const/4 v3, 0x0

    .line 2354
    .local v3, "text":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 2514
    :goto_1
    const v5, 0x7f090005

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 2515
    .local v4, "textView":Landroid/widget/TextView;
    if-eqz v4, :cond_1

    if-eqz v3, :cond_1

    .line 2517
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v5

    const v6, 0x16001630

    if-eq v5, v6, :cond_2

    .line 2518
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 2519
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2521
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2351
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2356
    .end local v4    # "textView":Landroid/widget/TextView;
    :sswitch_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060159

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2357
    goto :goto_1

    .line 2359
    :sswitch_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0600d5

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2360
    goto :goto_1

    .line 2363
    :sswitch_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060191

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2364
    goto :goto_1

    .line 2367
    :sswitch_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060190

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2368
    goto :goto_1

    .line 2370
    :sswitch_4
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06003b

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2371
    goto :goto_1

    .line 2373
    :sswitch_5
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060039

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2374
    goto :goto_1

    .line 2376
    :sswitch_6
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060038

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2377
    goto :goto_1

    .line 2379
    :sswitch_7
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060037

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2380
    goto :goto_1

    .line 2382
    :sswitch_8
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06003c

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2383
    goto :goto_1

    .line 2385
    :sswitch_9
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060042

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2386
    goto/16 :goto_1

    .line 2388
    :sswitch_a
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060093

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2389
    goto/16 :goto_1

    .line 2391
    :sswitch_b
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060095

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2392
    goto/16 :goto_1

    .line 2394
    :sswitch_c
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060096

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2395
    goto/16 :goto_1

    .line 2397
    :sswitch_d
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06006a

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2398
    goto/16 :goto_1

    .line 2400
    :sswitch_e
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0600aa

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2401
    goto/16 :goto_1

    .line 2403
    :sswitch_f
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06005a

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2404
    goto/16 :goto_1

    .line 2407
    :sswitch_10
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0601c1

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2408
    goto/16 :goto_1

    .line 2411
    :sswitch_11
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06016f

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2412
    goto/16 :goto_1

    .line 2414
    :sswitch_12
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060186

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2415
    goto/16 :goto_1

    .line 2417
    :sswitch_13
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060036

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2418
    goto/16 :goto_1

    .line 2422
    :sswitch_14
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060044

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2423
    goto/16 :goto_1

    .line 2425
    :sswitch_15
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060151

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2426
    goto/16 :goto_1

    .line 2428
    :sswitch_16
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0600ce

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2429
    goto/16 :goto_1

    .line 2431
    :sswitch_17
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06018c

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2432
    goto/16 :goto_1

    .line 2434
    :sswitch_18
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0600d2

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2435
    goto/16 :goto_1

    .line 2437
    :sswitch_19
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060050

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2438
    goto/16 :goto_1

    .line 2440
    :sswitch_1a
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06013b

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2441
    goto/16 :goto_1

    .line 2443
    :sswitch_1b
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06004e

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2444
    goto/16 :goto_1

    .line 2446
    :sswitch_1c
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06018f

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2447
    goto/16 :goto_1

    .line 2450
    :sswitch_1d
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06012e

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2451
    goto/16 :goto_1

    .line 2453
    :sswitch_1e
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0601d0

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2454
    goto/16 :goto_1

    .line 2456
    :sswitch_1f
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060047

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2457
    goto/16 :goto_1

    .line 2459
    :sswitch_20
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060046

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2460
    goto/16 :goto_1

    .line 2462
    :sswitch_21
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06004c

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2463
    goto/16 :goto_1

    .line 2465
    :sswitch_22
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06004d

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2466
    goto/16 :goto_1

    .line 2468
    :sswitch_23
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0600cc

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2469
    goto/16 :goto_1

    .line 2471
    :sswitch_24
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0600c9

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2472
    goto/16 :goto_1

    .line 2474
    :sswitch_25
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06015a

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2475
    goto/16 :goto_1

    .line 2477
    :sswitch_26
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060165

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2478
    goto/16 :goto_1

    .line 2480
    :sswitch_27
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0600ef

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2481
    goto/16 :goto_1

    .line 2483
    :sswitch_28
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0600ec

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2484
    goto/16 :goto_1

    .line 2486
    :sswitch_29
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06012f

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2487
    goto/16 :goto_1

    .line 2489
    :sswitch_2a
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060139

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2490
    goto/16 :goto_1

    .line 2492
    :sswitch_2b
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f06015e

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2493
    goto/16 :goto_1

    .line 2495
    :sswitch_2c
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0600f4

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2496
    goto/16 :goto_1

    .line 2498
    :sswitch_2d
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060138

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2499
    goto/16 :goto_1

    .line 2501
    :sswitch_2e
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060033

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2502
    goto/16 :goto_1

    .line 2504
    :sswitch_2f
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060034

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2505
    goto/16 :goto_1

    .line 2507
    :sswitch_30
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f060032

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 2508
    goto/16 :goto_1

    .line 2510
    :sswitch_31
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v6, 0x7f0600d4

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 2525
    .restart local v4    # "textView":Landroid/widget/TextView;
    :cond_2
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 2527
    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2529
    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2530
    .local v2, "subs":[Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    aget-object v6, v2, v7

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v2, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2533
    .end local v2    # "subs":[Ljava/lang/String;
    :cond_3
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2534
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 2535
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setSingleLine(Z)V

    goto/16 :goto_2

    .line 2354
    nop

    :sswitch_data_0
    .sparse-switch
        0x10001001 -> :sswitch_30
        0x10001002 -> :sswitch_2e
        0x10001003 -> :sswitch_2f
        0x10001004 -> :sswitch_12
        0x10001007 -> :sswitch_d
        0x10001008 -> :sswitch_f
        0x10001009 -> :sswitch_31
        0x1000100c -> :sswitch_e
        0x1000100d -> :sswitch_10
        0x1000100f -> :sswitch_11
        0x15001500 -> :sswitch_7
        0x15001501 -> :sswitch_6
        0x15001502 -> :sswitch_5
        0x15001503 -> :sswitch_4
        0x15001504 -> :sswitch_8
        0x15001505 -> :sswitch_9
        0x15001506 -> :sswitch_13
        0x15001607 -> :sswitch_a
        0x15001608 -> :sswitch_b
        0x15001609 -> :sswitch_c
        0x16001600 -> :sswitch_15
        0x16001601 -> :sswitch_14
        0x16001602 -> :sswitch_16
        0x16001603 -> :sswitch_17
        0x16001604 -> :sswitch_1e
        0x16001605 -> :sswitch_1f
        0x16001606 -> :sswitch_26
        0x16001607 -> :sswitch_27
        0x16001608 -> :sswitch_28
        0x16001609 -> :sswitch_29
        0x1600160a -> :sswitch_23
        0x1600160b -> :sswitch_24
        0x1600160c -> :sswitch_21
        0x1600160d -> :sswitch_22
        0x1600160e -> :sswitch_25
        0x1600160f -> :sswitch_2a
        0x16001610 -> :sswitch_2b
        0x16001611 -> :sswitch_2c
        0x16001612 -> :sswitch_2d
        0x16001613 -> :sswitch_20
        0x16001615 -> :sswitch_18
        0x16001616 -> :sswitch_1d
        0x16001618 -> :sswitch_19
        0x16001619 -> :sswitch_1a
        0x1600161a -> :sswitch_1b
        0x16001630 -> :sswitch_1c
        0x17001700 -> :sswitch_3
        0x17001701 -> :sswitch_2
        0x17001702 -> :sswitch_1
        0x17001703 -> :sswitch_0
    .end sparse-switch
.end method

.method public changeLayoutSize(I)V
    .locals 5
    .param p1, "width"    # I

    .prologue
    .line 2590
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewWidth:I

    .line 2592
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    if-eqz v3, :cond_0

    .line 2594
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2595
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 2597
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2598
    .local v0, "layout":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 2600
    move v2, p1

    .line 2601
    .local v2, "tempWidth":I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 2603
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;

    invoke-direct {v4, p0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;Landroid/view/ViewGroup$LayoutParams;I)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2624
    .end local v0    # "layout":Landroid/widget/LinearLayout;
    .end local v1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v2    # "tempWidth":I
    :cond_0
    return-void
.end method

.method public changeSelectionBtnIcon(II)V
    .locals 4
    .param p1, "buttonId"    # I
    .param p2, "iconId"    # I

    .prologue
    .line 665
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 666
    const/4 v2, 0x0

    .line 667
    .local v2, "icon":Landroid/widget/ImageView;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 674
    :goto_1
    if-eqz v2, :cond_0

    .line 675
    invoke-virtual {v2, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 677
    .end local v1    # "i":I
    .end local v2    # "icon":Landroid/widget/ImageView;
    :cond_0
    return-void

    .line 668
    .restart local v1    # "i":I
    .restart local v2    # "icon":Landroid/widget/ImageView;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 669
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    if-ne v3, p1, :cond_2

    .line 670
    const v3, 0x7f090004

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "icon":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 671
    .restart local v2    # "icon":Landroid/widget/ImageView;
    goto :goto_1

    .line 667
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public destroy()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 359
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    if-eqz v4, :cond_0

    .line 361
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->open()Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 362
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->openEffect()Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 363
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_9

    .line 394
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->close()V

    .line 395
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 398
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    if-eqz v4, :cond_1

    .line 399
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->removeAllViews()V

    .line 400
    :cond_1
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    .line 402
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_2

    .line 403
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 404
    :cond_2
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    .line 406
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonLayout:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_3

    .line 407
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 408
    :cond_3
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonLayout:Landroid/widget/LinearLayout;

    .line 410
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_4

    .line 411
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 412
    :cond_4
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    .line 414
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_5

    .line 415
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 418
    :cond_5
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v4, :cond_7

    .line 420
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_d

    .line 436
    :cond_7
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v4, :cond_8

    .line 437
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 438
    :cond_8
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    .line 439
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mInflater:Landroid/view/LayoutInflater;

    .line 440
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mCurrentSelectedButton:Landroid/widget/LinearLayout;

    .line 441
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    .line 442
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    .line 443
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mButtonInfoList:Ljava/util/ArrayList;

    .line 444
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonInfoList:Ljava/util/ArrayList;

    .line 445
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectButtonInfoList:Ljava/util/ArrayList;

    .line 446
    iput-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    .line 447
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mIsShowing:Z

    .line 448
    return-void

    .line 363
    :cond_9
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;

    .line 365
    .local v2, "recentInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    const/4 v1, 0x0

    .line 366
    .local v1, "have":Z
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_b

    .line 375
    :goto_2
    if-eqz v1, :cond_c

    .line 376
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v6

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->getButtonId()I

    move-result v7

    invoke-virtual {v5, v6, v7, v9}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->updateButtonInfoInButtonType(IIZ)V

    goto/16 :goto_0

    .line 366
    :cond_b
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;

    .line 368
    .local v0, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->getButtonId()I

    move-result v6

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->getButtonId()I

    move-result v7

    if-ne v6, v7, :cond_a

    .line 370
    const/4 v1, 0x1

    .line 371
    goto :goto_2

    .line 379
    .end local v0    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    :cond_c
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "initRecentButtons id:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 380
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getPreviousMode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    goto/16 :goto_0

    .line 383
    :sswitch_0
    const-string v5, "initRecentButtons color"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 384
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->getButtonId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRecentColorButtonInfo(I)V

    goto/16 :goto_0

    .line 387
    :sswitch_1
    const-string v5, "initRecentButtons effect"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 388
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->getButtonId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRecentEffectButtonInfo(I)V

    .line 389
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->getButtonId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertEffectButtonInfo(I)V

    goto/16 :goto_0

    .line 420
    .end local v1    # "have":Z
    .end local v2    # "recentInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    :cond_d
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 422
    .local v3, "vGroup":Landroid/view/ViewGroup;
    instance-of v5, v3, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    if-eqz v5, :cond_e

    .line 423
    check-cast v3, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    .end local v3    # "vGroup":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->destroy()V

    goto/16 :goto_1

    .line 424
    .restart local v3    # "vGroup":Landroid/view/ViewGroup;
    :cond_e
    instance-of v5, v3, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    if-eqz v5, :cond_f

    .line 425
    check-cast v3, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .end local v3    # "vGroup":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->destroy()V

    goto/16 :goto_1

    .line 426
    .restart local v3    # "vGroup":Landroid/view/ViewGroup;
    :cond_f
    instance-of v5, v3, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v5, :cond_10

    .line 427
    check-cast v3, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local v3    # "vGroup":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->destroy()V

    goto/16 :goto_1

    .line 428
    .restart local v3    # "vGroup":Landroid/view/ViewGroup;
    :cond_10
    instance-of v5, v3, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;

    if-eqz v5, :cond_11

    .line 429
    check-cast v3, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;

    .end local v3    # "vGroup":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomPenButtonLayout;->destroy()V

    goto/16 :goto_1

    .line 430
    .restart local v3    # "vGroup":Landroid/view/ViewGroup;
    :cond_11
    instance-of v5, v3, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;

    if-eqz v5, :cond_6

    .line 431
    check-cast v3, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;

    .end local v3    # "vGroup":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonFrameLayout;->destroy()V

    goto/16 :goto_1

    .line 380
    nop

    :sswitch_data_0
    .sparse-switch
        0x15000000 -> :sswitch_0
        0x16000000 -> :sswitch_1
    .end sparse-switch
.end method

.method public getButtonHeight()I
    .locals 4

    .prologue
    .line 1900
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->measure(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1904
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v1

    return v1

    .line 1901
    :catch_0
    move-exception v0

    .line 1902
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getButtonInstance(I)Landroid/widget/LinearLayout;
    .locals 4
    .param p1, "idx"    # I

    .prologue
    .line 1875
    const/4 v2, 0x0

    .line 1876
    .local v2, "ret":Landroid/widget/LinearLayout;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 1877
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 1886
    .end local v1    # "i":I
    :cond_0
    :goto_1
    return-object v2

    .line 1878
    .restart local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1879
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    if-ne v3, p1, :cond_2

    .line 1880
    move-object v2, v0

    .line 1881
    goto :goto_1

    .line 1883
    :cond_2
    const v3, 0x7f090075

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    .line 1877
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getCurrentSelectedButton()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 2336
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mCurrentSelectedButton:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getMainBtnList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2332
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPenColorLine()Landroid/widget/LinearLayout;
    .locals 3

    .prologue
    .line 1890
    const/4 v0, 0x0

    .line 1891
    .local v0, "colorLine":Landroid/widget/LinearLayout;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 1892
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1893
    const v2, 0x7f090075

    .line 1892
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "colorLine":Landroid/widget/LinearLayout;
    check-cast v0, Landroid/widget/LinearLayout;

    .line 1895
    .restart local v0    # "colorLine":Landroid/widget/LinearLayout;
    :cond_0
    return-object v0
.end method

.method public getSelectedButton()Landroid/widget/LinearLayout;
    .locals 3

    .prologue
    .line 562
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 564
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 573
    .end local v1    # "i":I
    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0

    .line 566
    .restart local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 567
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isSelected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 564
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public hide(Z)V
    .locals 9
    .param p1, "isConfig"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 451
    if-eqz p1, :cond_0

    .line 452
    const/16 v3, 0x8

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setBottomLayoutVisible(I)V

    .line 454
    :cond_0
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .line 456
    const/high16 v8, 0x3f800000    # 1.0f

    move v3, v1

    move v4, v2

    move v5, v1

    move v6, v2

    move v7, v1

    .line 454
    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 458
    .local v0, "animation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 460
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$7;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 481
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 483
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$8;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;Landroid/view/animation/Animation;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 489
    return-void
.end method

.method public initBottomButtonWithIcon(II)V
    .locals 25
    .param p1, "viewWidth"    # I
    .param p2, "collageImgNum"    # I

    .prologue
    .line 957
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v8, :cond_1

    .line 958
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 959
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iput v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentBtnCount:I

    .line 960
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 961
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->removeAllViews()V

    .line 962
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setVisibility(I)V

    .line 963
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 968
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 969
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "sj, SVBM - initBottomButtonWithIcon() - viewWidth : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 971
    new-instance v21, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x2

    const/4 v9, -0x2

    move-object/from16 v0, v21

    invoke-direct {v0, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 972
    .local v21, "lp":Landroid/widget/LinearLayout$LayoutParams;
    const/high16 v8, 0x40000000    # 2.0f

    move-object/from16 v0, v21

    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 974
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setSubButtonLayoutMarginBottom()V

    .line 975
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setSubButtonLayoutHeight()V

    .line 976
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-object/from16 v0, v21

    invoke-virtual {v8, v9, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 979
    const/16 v17, 0x0

    .line 980
    .local v17, "buttonWidth":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 1283
    :cond_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    new-instance v10, Landroid/view/ViewGroup$LayoutParams;

    const/4 v11, -0x2

    move/from16 v0, v17

    invoke-direct {v10, v0, v11}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1284
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    const/4 v9, 0x0

    move/from16 v0, v17

    invoke-virtual {v8, v0, v9}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->scrollTo(II)V

    .line 1290
    .end local v17    # "buttonWidth":I
    .end local v21    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->doInitialScroll()V

    .line 1291
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_2

    .line 1292
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 1293
    :cond_2
    return-void

    .line 982
    .restart local v17    # "buttonWidth":I
    .restart local v21    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :sswitch_0
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_0
    const/4 v8, 0x3

    move/from16 v0, v18

    if-ge v0, v8, :cond_0

    .line 983
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f030032

    invoke-direct {v2, v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 985
    .local v2, "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    packed-switch v18, :pswitch_data_0

    .line 1002
    :goto_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1003
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1004
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1005
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v8

    add-int v17, v17, v8

    .line 982
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .line 987
    :pswitch_0
    const v3, 0x10001002

    .line 988
    const v4, 0x7f0204f0

    .line 989
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f060033

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 987
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_1

    .line 992
    :pswitch_1
    const v3, 0x10001003

    .line 993
    const v4, 0x7f020163

    .line 994
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f060034

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 992
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_1

    .line 997
    :pswitch_2
    const v3, 0x10001001

    .line 998
    const v4, 0x7f0204ea

    .line 999
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f060032

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 997
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_1

    .line 1009
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v18    # "i":I
    :sswitch_1
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->initRecentButtons([III)V

    .line 1010
    const/16 v19, 0x0

    .line 1011
    .local v19, "isAssignedLeftMargin":Z
    const/16 v18, 0x0

    .restart local v18    # "i":I
    :goto_2
    const/16 v8, 0x9

    move/from16 v0, v18

    if-ge v0, v8, :cond_0

    .line 1013
    const/4 v3, -0x1

    .line 1014
    .local v3, "buttonId":I
    const/4 v4, -0x1

    .line 1015
    .local v4, "iconId":I
    const/4 v5, 0x0

    .line 1016
    .local v5, "text":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1017
    .local v6, "subMenu":Z
    const/4 v7, -0x1

    .line 1019
    .local v7, "side":I
    packed-switch v18, :pswitch_data_1

    .line 1085
    :goto_3
    const/16 v20, 0x0

    .line 1086
    .local v20, "isContinue":Z
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_4

    .line 1093
    if-eqz v20, :cond_5

    .line 1011
    :goto_5
    add-int/lit8 v18, v18, 0x1

    goto :goto_2

    .line 1021
    .end local v20    # "isContinue":Z
    :pswitch_3
    const v3, 0x15001506

    .line 1022
    const v4, 0x7f0200b0

    .line 1023
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f060036

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1024
    const/4 v6, 0x0

    .line 1025
    const/4 v7, 0x0

    .line 1026
    goto :goto_3

    .line 1029
    :pswitch_4
    const v3, 0x15001500

    .line 1030
    const v4, 0x7f0200b2

    .line 1031
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f060037

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1032
    const/4 v6, 0x0

    .line 1033
    const/4 v7, 0x0

    .line 1034
    goto :goto_3

    .line 1036
    :pswitch_5
    const v3, 0x15001501

    .line 1037
    const v4, 0x7f0200b3

    .line 1038
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f060038

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1039
    const/4 v6, 0x0

    .line 1040
    const/4 v7, 0x0

    .line 1041
    goto :goto_3

    .line 1043
    :pswitch_6
    const v3, 0x15001502

    .line 1044
    const v4, 0x7f0200b7

    .line 1045
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f060039

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1046
    const/4 v6, 0x0

    .line 1047
    const/4 v7, 0x0

    .line 1048
    goto :goto_3

    .line 1050
    :pswitch_7
    const v3, 0x15001607

    .line 1051
    const v4, 0x7f0200b6

    .line 1052
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f060093

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1053
    const/4 v6, 0x0

    .line 1054
    const/4 v7, 0x0

    .line 1055
    goto :goto_3

    .line 1057
    :pswitch_8
    const v3, 0x15001608

    .line 1058
    const v4, 0x7f0200b4

    .line 1059
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f060095

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1060
    const/4 v6, 0x0

    .line 1061
    const/4 v7, 0x0

    .line 1062
    goto/16 :goto_3

    .line 1064
    :pswitch_9
    const v3, 0x15001609

    .line 1065
    const v4, 0x7f0200b1

    .line 1066
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f060096

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1067
    const/4 v6, 0x0

    .line 1068
    const/4 v7, 0x0

    .line 1069
    goto/16 :goto_3

    .line 1071
    :pswitch_a
    const v3, 0x15001505

    .line 1072
    const v4, 0x7f0200b8

    .line 1073
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f060042

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1074
    const/4 v6, 0x0

    .line 1075
    const/4 v7, 0x0

    .line 1076
    goto/16 :goto_3

    .line 1078
    :pswitch_b
    const v3, 0x15001504

    .line 1079
    const v4, 0x7f0200b5

    .line 1080
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f06003c

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 1081
    const/4 v6, 0x0

    .line 1082
    const/4 v7, 0x0

    goto/16 :goto_3

    .line 1086
    .restart local v20    # "isContinue":Z
    :cond_4
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;

    .line 1088
    .local v14, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mButtonId:I
    invoke-static {v14}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;)I

    move-result v9

    if-ne v3, v9, :cond_3

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mRecentUsed:Z
    invoke-static {v14}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1090
    const/16 v20, 0x1

    goto/16 :goto_4

    .line 1097
    .end local v14    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    :cond_5
    if-nez v19, :cond_6

    .line 1099
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->isHaveRecentButtons()Z

    move-result v8

    if-nez v8, :cond_6

    .line 1101
    const/4 v7, 0x1

    .line 1102
    const/16 v19, 0x1

    .line 1106
    :cond_6
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f030032

    invoke-direct {v2, v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 1107
    .restart local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    .line 1113
    const v8, 0x7f090005

    invoke-virtual {v2, v8}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/TextView;

    .line 1114
    .local v24, "textView":Landroid/widget/TextView;
    const/4 v8, 0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 1115
    sget-object v8, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1117
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1118
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1119
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1120
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v8

    add-int v17, v17, v8

    goto/16 :goto_5

    .line 1125
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v3    # "buttonId":I
    .end local v4    # "iconId":I
    .end local v5    # "text":Ljava/lang/String;
    .end local v6    # "subMenu":Z
    .end local v7    # "side":I
    .end local v18    # "i":I
    .end local v19    # "isAssignedLeftMargin":Z
    .end local v20    # "isContinue":Z
    .end local v24    # "textView":Landroid/widget/TextView;
    :sswitch_2
    const/16 v18, 0x0

    .restart local v18    # "i":I
    :goto_6
    const/4 v8, 0x4

    move/from16 v0, v18

    if-ge v0, v8, :cond_0

    .line 1126
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f030032

    invoke-direct {v2, v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 1128
    .restart local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    packed-switch v18, :pswitch_data_2

    .line 1151
    :goto_7
    const v8, 0x7f090005

    invoke-virtual {v2, v8}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/TextView;

    .line 1152
    .restart local v24    # "textView":Landroid/widget/TextView;
    const/4 v8, 0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 1153
    sget-object v8, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1155
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1156
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1157
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1158
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v8

    add-int v17, v17, v8

    .line 1125
    add-int/lit8 v18, v18, 0x1

    goto :goto_6

    .line 1130
    .end local v24    # "textView":Landroid/widget/TextView;
    :pswitch_c
    const v9, 0x17001700

    .line 1131
    const v10, 0x7f0200ae

    .line 1132
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v11, 0x7f060190

    invoke-static {v8, v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object v8, v2

    .line 1130
    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_7

    .line 1135
    :pswitch_d
    const v9, 0x17001701

    .line 1136
    const v10, 0x7f0200a8

    .line 1137
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v11, 0x7f060191

    invoke-static {v8, v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, v2

    .line 1135
    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_7

    .line 1140
    :pswitch_e
    const v9, 0x17001702

    .line 1141
    const v10, 0x7f0200a6

    .line 1142
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v11, 0x7f0600d5

    invoke-static {v8, v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, v2

    .line 1140
    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_7

    .line 1145
    :pswitch_f
    const v9, 0x17001703

    .line 1146
    const v10, 0x7f0200ac

    .line 1147
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v11, 0x7f060159

    invoke-static {v8, v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, v2

    .line 1145
    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto/16 :goto_7

    .line 1163
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v18    # "i":I
    :sswitch_3
    const/16 v18, 0x0

    .restart local v18    # "i":I
    :goto_8
    const/4 v8, 0x6

    move/from16 v0, v18

    if-ge v0, v8, :cond_0

    .line 1164
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v9, 0x7f030032

    invoke-direct {v2, v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 1165
    .restart local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    packed-switch v18, :pswitch_data_3

    .line 1202
    :goto_9
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1203
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1204
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1205
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v8

    add-int v17, v17, v8

    .line 1163
    add-int/lit8 v18, v18, 0x1

    goto :goto_8

    .line 1167
    :pswitch_10
    const v9, 0x10001008

    .line 1168
    const v10, 0x7f02052d

    .line 1169
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v11, 0x7f06005a

    .line 1168
    invoke-static {v8, v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    .line 1169
    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object v8, v2

    .line 1167
    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_9

    .line 1172
    :pswitch_11
    const v9, 0x1000100c

    .line 1173
    const v10, 0x7f02052c

    .line 1174
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v11, 0x7f0600aa

    .line 1173
    invoke-static {v8, v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    .line 1174
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, v2

    .line 1172
    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_9

    .line 1177
    :pswitch_12
    const v9, 0x1000100d

    .line 1178
    const v10, 0x7f0202f7

    .line 1179
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v11, 0x7f0601c1

    .line 1178
    invoke-static {v8, v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    .line 1179
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, v2

    .line 1177
    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_9

    .line 1187
    :pswitch_13
    const v9, 0x10001007

    .line 1188
    const v10, 0x7f0201d2

    .line 1189
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v11, 0x7f060105

    .line 1188
    invoke-static {v8, v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    .line 1189
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, v2

    .line 1187
    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_9

    .line 1192
    :pswitch_14
    const v9, 0x10001009

    .line 1193
    const v10, 0x7f020367

    .line 1194
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v11, 0x7f0600d4

    .line 1193
    invoke-static {v8, v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    .line 1194
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, v2

    .line 1192
    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto/16 :goto_9

    .line 1197
    :pswitch_15
    const v9, 0x1000100f

    .line 1198
    const v10, 0x7f0202f6

    .line 1199
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v11, 0x7f06016f

    .line 1198
    invoke-static {v8, v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    .line 1199
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, v2

    .line 1197
    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto/16 :goto_9

    .line 1212
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v18    # "i":I
    :sswitch_4
    const/16 v22, 0x0

    .line 1213
    .local v22, "maxItem":I
    const/16 v23, 0x0

    .line 1214
    .local v23, "startDrawableId":I
    const/16 v16, 0x0

    .line 1215
    .local v16, "buttonType":I
    const/4 v13, 0x0

    .line 1216
    .local v13, "buttonSide":I
    const/4 v15, 0x0

    .line 1218
    .local v15, "buttonLayoutId":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v8

    packed-switch v8, :pswitch_data_4

    .line 1245
    :goto_a
    :pswitch_16
    const/16 v18, 0x0

    .restart local v18    # "i":I
    :goto_b
    move/from16 v0, v18

    move/from16 v1, v22

    if-ge v0, v1, :cond_0

    .line 1247
    if-nez v18, :cond_7

    .line 1249
    const/4 v13, 0x1

    .line 1255
    :goto_c
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v8, v15}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;-><init>(Landroid/content/Context;I)V

    .line 1256
    .restart local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    const/4 v8, 0x2

    move/from16 v0, v22

    if-ne v0, v8, :cond_8

    .line 1259
    add-int v9, v16, v18

    .line 1260
    add-int v10, v23, v18

    .line 1261
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    const v11, 0x7f060194

    invoke-static {v8, v11}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    .line 1262
    const/4 v12, 0x1

    move-object v8, v2

    .line 1258
    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->initCollageButtonsWithoutTextDisplay(IILjava/lang/String;ZI)V

    .line 1275
    :goto_d
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1276
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1277
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->measure(II)V

    .line 1278
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getMeasuredWidth()I

    move-result v8

    add-int v17, v17, v8

    .line 1245
    add-int/lit8 v18, v18, 0x1

    goto :goto_b

    .line 1221
    .end local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v18    # "i":I
    :pswitch_17
    const/16 v22, 0x2

    .line 1222
    const/high16 v16, 0x1e500000

    .line 1223
    const v23, 0x7f02008d

    .line 1224
    const v15, 0x7f030023

    .line 1225
    goto :goto_a

    .line 1228
    :pswitch_18
    invoke-static/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getMaxStyleNum(I)I

    move-result v22

    .line 1229
    const/high16 v16, 0x1e200000

    .line 1230
    invoke-static/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getLayoutIcon(I)I

    move-result v23

    .line 1231
    const v15, 0x7f030024

    .line 1232
    goto :goto_a

    .line 1238
    :pswitch_19
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getBgImgCnt()I

    move-result v22

    .line 1239
    const/high16 v16, 0x1e400000

    .line 1240
    const v23, 0x7f020103

    .line 1241
    const v15, 0x7f030022

    goto :goto_a

    .line 1253
    .restart local v18    # "i":I
    :cond_7
    const/4 v13, 0x0

    goto :goto_c

    .line 1268
    .restart local v2    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    :cond_8
    add-int v9, v16, v18

    .line 1269
    add-int v10, v23, v18

    .line 1270
    const/4 v11, 0x0

    .line 1271
    const/4 v12, 0x0

    move-object v8, v2

    .line 1267
    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->init(IILjava/lang/String;ZI)V

    goto :goto_d

    .line 980
    nop

    :sswitch_data_0
    .sparse-switch
        0x11000000 -> :sswitch_0
        0x15000000 -> :sswitch_1
        0x18000000 -> :sswitch_2
        0x1e110000 -> :sswitch_4
        0x2c000000 -> :sswitch_4
        0x31000000 -> :sswitch_3
    .end sparse-switch

    .line 985
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1019
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 1128
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch

    .line 1165
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch

    .line 1218
    :pswitch_data_4
    .packed-switch 0x1e300000
        :pswitch_17
        :pswitch_18
        :pswitch_16
        :pswitch_19
    .end packed-switch
.end method

.method public initBottomButtonWithThumbnail(I[III)V
    .locals 28
    .param p1, "viewWidth"    # I
    .param p2, "image"    # [I
    .param p3, "imageWidth"    # I
    .param p4, "imageHeight"    # I

    .prologue
    .line 1372
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentBtnCount:I

    .line 1373
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 1668
    :cond_0
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->doInitialScroll()V

    .line 1670
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 1671
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 1672
    :cond_1
    return-void

    .line 1375
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    .line 1376
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1378
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1379
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->removeAllViews()V

    .line 1380
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setVisibility(I)V

    .line 1381
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1382
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1383
    new-instance v21, Landroid/widget/LinearLayout$LayoutParams;

    .line 1384
    const/4 v4, -0x1

    const/4 v5, -0x2

    .line 1383
    move-object/from16 v0, v21

    invoke-direct {v0, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1386
    .local v21, "lp":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->initRecentButtons([III)V

    .line 1388
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setSubButtonLayoutMarginBottom()V

    .line 1389
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setSubButtonLayoutHeight()V

    .line 1390
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-object/from16 v0, v21

    invoke-virtual {v4, v5, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1391
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, v21

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1393
    const/4 v8, 0x0

    .line 1394
    .local v8, "width":I
    const/4 v9, 0x0

    .line 1395
    .local v9, "height":I
    const/16 v25, 0x0

    .line 1397
    .local v25, "thumbnail":[I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/16 v5, 0x19

    if-eq v4, v5, :cond_c

    .line 1400
    const-string v4, "JW mEffectButtonInfoList.size() != 25"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1401
    const/16 v16, -0x1

    .line 1402
    .local v16, "buttonId":I
    const/16 v17, 0x1

    .line 1404
    .local v17, "firstButton":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->openEffect()Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 1405
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_1
    const/16 v4, 0x19

    move/from16 v0, v18

    if-lt v0, v4, :cond_3

    .line 1572
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->close()V

    .line 1664
    .end local v8    # "width":I
    .end local v9    # "height":I
    .end local v16    # "buttonId":I
    .end local v17    # "firstButton":Z
    .end local v18    # "i":I
    .end local v21    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v25    # "thumbnail":[I
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    instance-of v4, v4, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v4, :cond_0

    .line 1665
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->initEffectManagerButton()V

    goto/16 :goto_0

    .line 1407
    .restart local v8    # "width":I
    .restart local v9    # "height":I
    .restart local v16    # "buttonId":I
    .restart local v17    # "firstButton":Z
    .restart local v18    # "i":I
    .restart local v21    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .restart local v25    # "thumbnail":[I
    :cond_3
    packed-switch v18, :pswitch_data_1

    .line 1499
    :goto_2
    const/16 v20, 0x0

    .line 1500
    .local v20, "isContinue":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_5

    .line 1507
    if-eqz v20, :cond_6

    .line 1405
    :goto_4
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 1410
    .end local v20    # "isContinue":Z
    :pswitch_1
    const v16, 0x16001600

    .line 1411
    goto :goto_2

    .line 1413
    :pswitch_2
    const v16, 0x16001601

    .line 1414
    goto :goto_2

    .line 1416
    :pswitch_3
    const v16, 0x16001602

    .line 1417
    goto :goto_2

    .line 1419
    :pswitch_4
    const v16, 0x16001603

    .line 1420
    goto :goto_2

    .line 1422
    :pswitch_5
    const v16, 0x16001604

    .line 1423
    goto :goto_2

    .line 1425
    :pswitch_6
    const v16, 0x16001605

    .line 1426
    goto :goto_2

    .line 1428
    :pswitch_7
    const v16, 0x16001606

    .line 1429
    goto :goto_2

    .line 1431
    :pswitch_8
    const v16, 0x16001607

    .line 1432
    goto :goto_2

    .line 1434
    :pswitch_9
    const v16, 0x16001608

    .line 1435
    goto :goto_2

    .line 1437
    :pswitch_a
    const v16, 0x16001609

    .line 1438
    goto :goto_2

    .line 1440
    :pswitch_b
    const v16, 0x1600160a

    .line 1441
    goto :goto_2

    .line 1443
    :pswitch_c
    const v16, 0x1600160b

    .line 1444
    goto :goto_2

    .line 1446
    :pswitch_d
    const v16, 0x1600160c

    .line 1447
    goto :goto_2

    .line 1449
    :pswitch_e
    const v16, 0x1600160d

    .line 1450
    goto :goto_2

    .line 1452
    :pswitch_f
    const v16, 0x16001615

    .line 1453
    goto :goto_2

    .line 1455
    :pswitch_10
    const v16, 0x16001616

    .line 1456
    goto :goto_2

    .line 1458
    :pswitch_11
    const v16, 0x1600160e

    .line 1459
    goto :goto_2

    .line 1461
    :pswitch_12
    const v16, 0x1600160f

    .line 1462
    goto :goto_2

    .line 1464
    :pswitch_13
    const v16, 0x16001610

    .line 1465
    goto :goto_2

    .line 1467
    :pswitch_14
    const v16, 0x16001611

    .line 1468
    goto :goto_2

    .line 1470
    :pswitch_15
    const v16, 0x16001612

    .line 1471
    goto :goto_2

    .line 1473
    :pswitch_16
    const v16, 0x16001613

    .line 1474
    goto :goto_2

    .line 1476
    :pswitch_17
    const v16, 0x16001618

    .line 1477
    goto :goto_2

    .line 1479
    :pswitch_18
    const v16, 0x16001619

    .line 1480
    goto :goto_2

    .line 1482
    :pswitch_19
    const v16, 0x1600161a

    goto :goto_2

    .line 1500
    .restart local v20    # "isContinue":Z
    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;

    .line 1502
    .local v13, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mButtonId:I
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;)I

    move-result v5

    move/from16 v0, v16

    if-ne v0, v5, :cond_4

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mRecentUsed:Z
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1504
    const/16 v20, 0x1

    goto/16 :goto_3

    .line 1511
    .end local v13    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    :cond_6
    const/16 v23, 0x0

    .line 1514
    .local v23, "side":I
    if-eqz v17, :cond_8

    .line 1516
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->isHaveRecentButtons()Z

    move-result v4

    if-nez v4, :cond_7

    .line 1518
    const/16 v23, 0x1

    .line 1520
    :cond_7
    const/16 v17, 0x0

    .line 1524
    :cond_8
    new-instance v15, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move/from16 v0, v23

    invoke-direct {v15, v4, v5, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;I)V

    .line 1527
    .local v15, "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    const v4, 0x7f090005

    invoke-virtual {v15, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/TextView;

    .line 1528
    .local v24, "textView":Landroid/widget/TextView;
    const/4 v4, 0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 1529
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1531
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1532
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v15}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1533
    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->getLayoutWidth()I

    move-result v8

    .line 1534
    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->getLayoutHeight()I

    move-result v9

    .line 1535
    const/4 v4, 0x1

    const/high16 v5, 0x42960000    # 75.0f

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v4, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v22

    .line 1536
    .local v22, "px":F
    const/4 v10, 0x0

    .local v10, "resizeWidth":I
    const/4 v11, 0x0

    .line 1537
    .local v11, "resizeHeight":I
    const/high16 v12, 0x3f800000    # 1.0f

    .line 1538
    .local v12, "scale":F
    int-to-float v4, v8

    int-to-float v5, v9

    div-float v26, v4, v5

    .line 1539
    .local v26, "viewRatio":F
    move/from16 v0, p3

    int-to-float v4, v0

    move/from16 v0, p4

    int-to-float v5, v0

    div-float v19, v4, v5

    .line 1541
    .local v19, "imgRatio":F
    cmpl-float v4, v26, v19

    if-lez v4, :cond_b

    .line 1543
    int-to-float v4, v8

    move/from16 v0, p3

    int-to-float v5, v0

    div-float v12, v4, v5

    .line 1544
    move/from16 v0, p4

    int-to-float v4, v0

    mul-float/2addr v4, v12

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v11, v4

    .line 1545
    move v10, v8

    .line 1553
    :goto_5
    mul-int v4, v10, v11

    const/high16 v5, 0x80000

    if-le v4, v5, :cond_9

    .line 1554
    const/high16 v4, 0x49000000    # 524288.0f

    mul-int v5, p3, p4

    int-to-float v5, v5

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v12, v4

    .line 1555
    move/from16 v0, p3

    int-to-float v4, v0

    mul-float/2addr v4, v12

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v10, v4

    .line 1556
    move/from16 v0, p4

    int-to-float v4, v0

    mul-float/2addr v4, v12

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v11, v4

    :cond_9
    move-object/from16 v4, p0

    move-object/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    .line 1559
    invoke-direct/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->makeThumbnail([IIIIIIIF)[I

    move-result-object v25

    .line 1561
    if-eqz v25, :cond_a

    .line 1562
    move-object/from16 v0, v25

    invoke-virtual {v15, v0, v10, v11}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setThumbnail([III)V

    .line 1564
    :cond_a
    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setId(I)V

    .line 1570
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertEffectButtonInfo(I)V

    goto/16 :goto_4

    .line 1549
    :cond_b
    int-to-float v4, v9

    move/from16 v0, p4

    int-to-float v5, v0

    div-float v12, v4, v5

    .line 1550
    move/from16 v0, p3

    int-to-float v4, v0

    mul-float/2addr v4, v12

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v10, v4

    .line 1551
    move v11, v9

    goto :goto_5

    .line 1576
    .end local v10    # "resizeWidth":I
    .end local v11    # "resizeHeight":I
    .end local v12    # "scale":F
    .end local v15    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    .end local v16    # "buttonId":I
    .end local v17    # "firstButton":Z
    .end local v18    # "i":I
    .end local v19    # "imgRatio":F
    .end local v20    # "isContinue":Z
    .end local v22    # "px":F
    .end local v23    # "side":I
    .end local v24    # "textView":Landroid/widget/TextView;
    .end local v26    # "viewRatio":F
    :cond_c
    const-string v4, "JW mEffectButtonInfoList.size() == 25"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1577
    const/16 v18, 0x0

    .restart local v18    # "i":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v18

    if-ge v0, v4, :cond_2

    .line 1580
    const/16 v16, -0x1

    .line 1581
    .restart local v16    # "buttonId":I
    const/16 v17, 0x1

    .line 1583
    .restart local v17    # "firstButton":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_d
    :goto_7
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_e

    .line 1577
    add-int/lit8 v18, v18, 0x1

    goto :goto_6

    .line 1583
    :cond_e
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;

    .line 1585
    .local v14, "btnInfo":Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;
    invoke-virtual {v14}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getIndex()I

    move-result v4

    move/from16 v0, v18

    if-ne v4, v0, :cond_d

    .line 1587
    invoke-virtual {v14}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getUse()Z

    move-result v4

    if-nez v4, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    instance-of v4, v4, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v4, :cond_d

    .line 1589
    :cond_f
    invoke-virtual {v14}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getId()I

    move-result v16

    .line 1591
    const/16 v20, 0x0

    .line 1592
    .restart local v20    # "isContinue":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_10
    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_15

    .line 1599
    if-nez v20, :cond_d

    .line 1603
    const/16 v23, 0x0

    .line 1606
    .restart local v23    # "side":I
    if-eqz v17, :cond_12

    .line 1608
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->isHaveRecentButtons()Z

    move-result v4

    if-nez v4, :cond_11

    .line 1610
    const/16 v23, 0x1

    .line 1612
    :cond_11
    const/16 v17, 0x0

    .line 1616
    :cond_12
    new-instance v15, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move/from16 v0, v23

    invoke-direct {v15, v4, v5, v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;I)V

    .line 1619
    .restart local v15    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    const v4, 0x7f090005

    invoke-virtual {v15, v4}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/TextView;

    .line 1620
    .restart local v24    # "textView":Landroid/widget/TextView;
    const/4 v4, 0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 1621
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1623
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1624
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v15}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1625
    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->getLayoutWidth()I

    move-result v8

    .line 1626
    invoke-virtual {v15}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->getLayoutHeight()I

    move-result v9

    .line 1628
    const/4 v10, 0x0

    .restart local v10    # "resizeWidth":I
    const/4 v11, 0x0

    .line 1629
    .restart local v11    # "resizeHeight":I
    const/high16 v12, 0x3f800000    # 1.0f

    .line 1630
    .restart local v12    # "scale":F
    int-to-float v4, v8

    int-to-float v5, v9

    div-float v26, v4, v5

    .line 1631
    .restart local v26    # "viewRatio":F
    move/from16 v0, p3

    int-to-float v4, v0

    move/from16 v0, p4

    int-to-float v5, v0

    div-float v19, v4, v5

    .line 1633
    .restart local v19    # "imgRatio":F
    cmpl-float v4, v26, v19

    if-lez v4, :cond_16

    .line 1635
    int-to-float v4, v8

    move/from16 v0, p3

    int-to-float v5, v0

    div-float v12, v4, v5

    .line 1636
    move/from16 v0, p4

    int-to-float v4, v0

    mul-float/2addr v4, v12

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v11, v4

    .line 1637
    move v10, v8

    .line 1645
    :goto_9
    mul-int v4, v10, v11

    const/high16 v5, 0x80000

    if-le v4, v5, :cond_13

    .line 1646
    const/high16 v4, 0x49000000    # 524288.0f

    mul-int v5, p3, p4

    int-to-float v5, v5

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v12, v4

    .line 1647
    move/from16 v0, p3

    int-to-float v4, v0

    mul-float/2addr v4, v12

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v10, v4

    .line 1648
    move/from16 v0, p4

    int-to-float v4, v0

    mul-float/2addr v4, v12

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v11, v4

    :cond_13
    move-object/from16 v4, p0

    move-object/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    .line 1651
    invoke-direct/range {v4 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->makeThumbnail([IIIIIIIF)[I

    move-result-object v25

    .line 1653
    if-eqz v25, :cond_14

    .line 1654
    move-object/from16 v0, v25

    invoke-virtual {v15, v0, v10, v11}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setThumbnail([III)V

    .line 1656
    :cond_14
    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setId(I)V

    goto/16 :goto_7

    .line 1592
    .end local v10    # "resizeWidth":I
    .end local v11    # "resizeHeight":I
    .end local v12    # "scale":F
    .end local v15    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    .end local v19    # "imgRatio":F
    .end local v23    # "side":I
    .end local v24    # "textView":Landroid/widget/TextView;
    .end local v26    # "viewRatio":F
    :cond_15
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;

    .line 1594
    .restart local v13    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mButtonId:I
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;)I

    move-result v5

    move/from16 v0, v16

    if-ne v0, v5, :cond_10

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->mRecentUsed:Z
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 1596
    const/16 v20, 0x1

    goto/16 :goto_8

    .line 1641
    .end local v13    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    .restart local v10    # "resizeWidth":I
    .restart local v11    # "resizeHeight":I
    .restart local v12    # "scale":F
    .restart local v15    # "button":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    .restart local v19    # "imgRatio":F
    .restart local v23    # "side":I
    .restart local v24    # "textView":Landroid/widget/TextView;
    .restart local v26    # "viewRatio":F
    :cond_16
    int-to-float v4, v9

    move/from16 v0, p4

    int-to-float v5, v0

    div-float v12, v4, v5

    .line 1642
    move/from16 v0, p3

    int-to-float v4, v0

    mul-float/2addr v4, v12

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v10, v4

    .line 1643
    move v11, v9

    goto :goto_9

    .line 1373
    nop

    :pswitch_data_0
    .packed-switch 0x16000000
        :pswitch_0
    .end packed-switch

    .line 1407
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method public initHorizontalScrollView()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$5;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setScrollViewCallback(Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView$ScrollViewCallback;)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$6;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 338
    return-void
.end method

.method public isAnimation()Z
    .locals 1

    .prologue
    .line 492
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mAnimate:Z

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 496
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mIsShowing:Z

    return v0
.end method

.method public moveArrowToCurrentViewBtn(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 164
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v1}, Landroid/widget/LinearLayout;->measure(II)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, p1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setX(F)V

    .line 169
    :cond_0
    return-void
.end method

.method public moveScrollToFirst()V
    .locals 3

    .prologue
    .line 2578
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->getScrollY()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->scrollTo(II)V

    .line 2579
    return-void
.end method

.method public movoToPosition(I)V
    .locals 6
    .param p1, "buttonId"    # I

    .prologue
    const/4 v5, 0x0

    .line 577
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 578
    const/4 v3, 0x0

    .line 579
    .local v3, "width":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 600
    .end local v1    # "i":I
    .end local v3    # "width":I
    :cond_0
    :goto_1
    return-void

    .line 580
    .restart local v1    # "i":I
    .restart local v3    # "width":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 581
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v5, v5}, Landroid/widget/LinearLayout;->measure(II)V

    .line 582
    if-eqz v1, :cond_2

    .line 583
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    .line 585
    :cond_2
    move v2, v3

    .line 586
    .local v2, "position":I
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v4

    if-ne v4, p1, :cond_3

    .line 587
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "movoToPosition:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 588
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$11;

    invoke-direct {v5, p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;I)V

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 579
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public movoToPositionWithBackBtn(I)V
    .locals 8
    .param p1, "buttonId"    # I

    .prologue
    const/4 v5, 0x0

    .line 603
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 604
    const/4 v3, 0x0

    .line 605
    .local v3, "width":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 627
    .end local v1    # "i":I
    .end local v3    # "width":I
    :cond_0
    :goto_1
    return-void

    .line 606
    .restart local v1    # "i":I
    .restart local v3    # "width":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 607
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v5, v5}, Landroid/widget/LinearLayout;->measure(II)V

    .line 609
    move v2, v3

    .line 610
    .local v2, "position":I
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v4

    if-ne v4, p1, :cond_2

    .line 611
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "movoToPosition:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 612
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$12;

    invoke-direct {v5, p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;I)V

    .line 617
    const-wide/16 v6, 0x64

    .line 612
    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 622
    :cond_2
    if-eqz v1, :cond_3

    .line 623
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    .line 605
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public onConfigurationChanged()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2198
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2199
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    .line 2200
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    const v5, 0x7f0900b8

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    .line 2201
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    const v5, 0x7f0900b9

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    .line 2202
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    const v5, 0x7f0900b6

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    .line 2206
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setSubButtonLayoutMarginBottom()V

    .line 2207
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setSubButtonLayoutHeight()V

    .line 2208
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->changeLanguage()V

    .line 2209
    const/4 v1, 0x0

    .line 2214
    .local v1, "backButtonWidth":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2215
    .local v0, "VG":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 2216
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2218
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->initHorizontalScrollView()V

    .line 2220
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    invoke-virtual {v4, v8}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 2272
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int v2, v4, v1

    .line 2273
    .local v2, "horizontalWidth":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v7, -0x2

    invoke-direct {v6, v2, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2284
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->setBottomLayoutVisible(I)V

    .line 2286
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->initMiddleButton(I)V

    .line 2288
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mAnimate:Z

    .line 2290
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    const/high16 v5, 0x31000000

    if-ne v4, v5, :cond_1

    .line 2291
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getCurrentModeButtonId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonLeft(I)I

    move-result v3

    .line 2292
    .local v3, "left":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->adjustSubmenuBarPosition(II)V

    .line 2294
    .end local v3    # "left":I
    :cond_1
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$17;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$17;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    .line 2302
    const-wide/16 v6, 0x64

    .line 2294
    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2309
    return-void
.end method

.method public onkey_main_Enter()V
    .locals 10

    .prologue
    .line 2543
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 2545
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v9, v4, :cond_1

    .line 2575
    .end local v9    # "i":I
    :cond_0
    :goto_1
    return-void

    .line 2548
    .restart local v9    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2549
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    const v5, 0x7f09006f

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2550
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    const v5, 0x7f09006f

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2551
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SubViewButtonsManager onKeyUp mButtonList.get("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") focused"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2552
    const-wide/16 v0, 0x0

    .line 2553
    .local v0, "upTime":J
    const-wide/16 v2, 0x0

    .line 2554
    .local v2, "eventTime":J
    const/4 v8, 0x0

    .line 2556
    .local v8, "e":Landroid/view/MotionEvent;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 2557
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2559
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 2558
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 2560
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2562
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 2563
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2565
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 2564
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 2567
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto/16 :goto_1

    .line 2571
    .end local v0    # "upTime":J
    .end local v2    # "eventTime":J
    .end local v8    # "e":Landroid/view/MotionEvent;
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SubViewButtonsManager not focused onKeyUp mButtonList.get("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") not focused"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2545
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0
.end method

.method public refreshScrollView()V
    .locals 4

    .prologue
    .line 2689
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 2690
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalInnerLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$19;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$19;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    .line 2695
    const-wide/16 v2, 0x3e8

    .line 2690
    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/LinearLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2697
    :cond_0
    return-void
.end method

.method public removeButtonsInFrame()Z
    .locals 1

    .prologue
    .line 342
    const/4 v0, 0x1

    return v0
.end method

.method public resume()V
    .locals 0

    .prologue
    .line 347
    return-void
.end method

.method public scrollViewRequestLayout()V
    .locals 1

    .prologue
    .line 2582
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->requestLayout()V

    .line 2583
    return-void
.end method

.method public setBottomLayoutVisible(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 2340
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 2341
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2342
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    if-eqz v0, :cond_1

    .line 2343
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setVisibility(I)V

    .line 2344
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 2345
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2346
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    if-eqz v0, :cond_3

    .line 2347
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMiddleButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2349
    :cond_3
    return-void
.end method

.method public setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V
    .locals 8
    .param p1, "buttonType"    # I
    .param p2, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    .prologue
    .line 1967
    const/4 v1, 0x0

    .line 1968
    .local v1, "button":Landroid/widget/LinearLayout;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 1969
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_1

    .line 1997
    .end local v2    # "i":I
    :cond_0
    return-void

    .line 1970
    .restart local v2    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "button":Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 1971
    .restart local v1    # "button":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    .line 1972
    .local v3, "id":I
    if-ne v3, p1, :cond_3

    .line 1973
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, p2, v7}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    .line 1974
    .local v4, "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$14;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->setAnimationCallback(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;)V

    .line 1981
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1983
    instance-of v5, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 1985
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .line 1986
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->init(I)V

    .line 1989
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    :cond_2
    instance-of v5, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    if-eqz v5, :cond_3

    move-object v0, v1

    .line 1991
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    .line 1992
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->initFrame(I)V

    .line 1969
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    .end local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;)V
    .locals 8
    .param p1, "buttonType"    # I
    .param p2, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .param p3, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;

    .prologue
    .line 2095
    const/4 v1, 0x0

    .line 2096
    .local v1, "button":Landroid/widget/LinearLayout;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 2097
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_1

    .line 2117
    .end local v2    # "i":I
    :cond_0
    return-void

    .line 2098
    .restart local v2    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "button":Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 2099
    .restart local v1    # "button":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    .line 2100
    .local v3, "id":I
    if-ne v3, p1, :cond_3

    .line 2101
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, p2, v7}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    .line 2102
    .local v4, "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    invoke-virtual {v4, p3}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->setAnimationCallback(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;)V

    .line 2103
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2104
    instance-of v5, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 2106
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .line 2107
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->init(I)V

    .line 2109
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    :cond_2
    instance-of v5, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    if-eqz v5, :cond_3

    move-object v0, v1

    .line 2111
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    .line 2112
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->initFrame(I)V

    .line 2097
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    .end local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Ljava/util/HashMap;)V
    .locals 8
    .param p1, "buttonType"    # I
    .param p2, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[I>;)V"
        }
    .end annotation

    .prologue
    .line 2000
    .local p3, "textureData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;[I>;"
    const/4 v1, 0x0

    .line 2001
    .local v1, "button":Landroid/widget/LinearLayout;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 2002
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_1

    .line 2052
    .end local v2    # "i":I
    :cond_0
    return-void

    .line 2003
    .restart local v2    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "button":Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 2004
    .restart local v1    # "button":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    .line 2005
    .local v3, "id":I
    if-ne v3, p1, :cond_4

    .line 2006
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, p2, v7}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    .line 2007
    .local v4, "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$15;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->setAnimationCallback(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;)V

    .line 2014
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2016
    instance-of v5, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    if-eqz v5, :cond_3

    move-object v0, v1

    .line 2018
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .line 2019
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    if-eqz p3, :cond_2

    .line 2020
    const v5, 0x16001607

    if-ne v3, v5, :cond_5

    .line 2021
    const v5, 0x16001607

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setTexture1([I)V

    .line 2041
    :cond_2
    :goto_1
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->init(I)V

    .line 2044
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    :cond_3
    instance-of v5, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    if-eqz v5, :cond_4

    move-object v0, v1

    .line 2046
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    .line 2047
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->initFrame(I)V

    .line 2002
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    .end local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2023
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    .restart local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    :cond_5
    const v5, 0x16001608

    if-ne v3, v5, :cond_6

    .line 2024
    const v5, 0x16001608

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setTexture1([I)V

    goto :goto_1

    .line 2026
    :cond_6
    const v5, 0x16001609

    if-ne v3, v5, :cond_7

    .line 2027
    const v5, 0x16001609

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setTexture1([I)V

    goto :goto_1

    .line 2029
    :cond_7
    const v5, 0x16001612

    if-ne v3, v5, :cond_8

    .line 2030
    const v5, 0x16001612

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setTexture1([I)V

    goto :goto_1

    .line 2032
    :cond_8
    const v5, 0x1600160f

    if-ne v3, v5, :cond_9

    .line 2033
    const v5, 0x1600160f

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setTexture1([I)V

    .line 2034
    const v5, 0x16001614

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setTexture2([I)V

    goto :goto_1

    .line 2036
    :cond_9
    const v5, 0x16001616

    if-ne v3, v5, :cond_2

    .line 2037
    const v5, 0x16001616

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setTexture1([I)V

    .line 2038
    const v5, 0x16001617

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [I

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setTexture2([I)V

    goto/16 :goto_1
.end method

.method public setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V
    .locals 8
    .param p1, "buttonType"    # I
    .param p2, "enabled"    # Z
    .param p3, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    .prologue
    .line 2056
    const/4 v1, 0x0

    .line 2057
    .local v1, "button":Landroid/widget/LinearLayout;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 2058
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_1

    .line 2093
    .end local v2    # "i":I
    :cond_0
    return-void

    .line 2059
    .restart local v2    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "button":Landroid/widget/LinearLayout;
    check-cast v1, Landroid/widget/LinearLayout;

    .line 2061
    .restart local v1    # "button":Landroid/widget/LinearLayout;
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    .line 2062
    .local v3, "id":I
    if-ne v3, p1, :cond_3

    .line 2063
    if-eqz p2, :cond_4

    .line 2065
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, p3, v7}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V

    .line 2066
    .local v4, "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$16;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->setAnimationCallback(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;)V

    .line 2073
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2074
    instance-of v5, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 2076
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .line 2077
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->init(I)V

    .line 2079
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;
    :cond_2
    instance-of v5, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    if-eqz v5, :cond_3

    move-object v0, v1

    .line 2081
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;

    .line 2082
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;->initFrame(I)V

    .line 2058
    .end local v0    # "b":Lcom/sec/android/mimage/photoretouching/Gui/BottomFrameButtonLayout;
    .end local v4    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2087
    :cond_4
    instance-of v5, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v5, :cond_3

    move-object v5, v1

    .line 2088
    check-cast v5, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    invoke-virtual {v5, p2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setEnabledWidthChildren(Z)V

    goto :goto_1
.end method

.method public setClearSelectedButton()V
    .locals 3

    .prologue
    .line 656
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 657
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 662
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 658
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 659
    .local v0, "button":Landroid/widget/LinearLayout;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 657
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 2312
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 2313
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 2318
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 2314
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 2315
    .local v1, "v":Landroid/view/View;
    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 2313
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setEnabledWithChildren(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 2321
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 2322
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 2330
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 2323
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 2324
    .local v1, "v":Landroid/view/View;
    instance-of v2, v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v2, :cond_2

    .line 2326
    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local v1    # "v":Landroid/view/View;
    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->setEnabledWidthChildren(Z)V

    .line 2322
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setRecentButton(Landroid/view/View;)V
    .locals 11
    .param p1, "button"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 1927
    const/4 v7, 0x0

    .line 1928
    .local v7, "have":Z
    const/4 v2, -0x1

    .line 1929
    .local v2, "buttonId":I
    const/4 v8, -0x1

    .line 1930
    .local v8, "idx":I
    instance-of v0, p1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    if-eqz v0, :cond_4

    .line 1932
    const-string v0, "insertRow aaaaaa"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1933
    check-cast p1, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .end local p1    # "button":Landroid/view/View;
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getButtonType()I

    move-result v2

    .line 1939
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1948
    :goto_1
    if-nez v7, :cond_6

    .line 1950
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_2

    .line 1952
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonInfoList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1954
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "insertRow bbbb:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1955
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonInfoList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v3, v1, -0x1

    move-object v1, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;IIII)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1965
    :cond_3
    :goto_2
    return-void

    .line 1935
    .restart local p1    # "button":Landroid/view/View;
    :cond_4
    instance-of v0, p1, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    if-eqz v0, :cond_0

    .line 1937
    check-cast p1, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    .end local p1    # "button":Landroid/view/View;
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->getIconType()I

    move-result v2

    goto :goto_0

    .line 1939
    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;

    .line 1941
    .local v6, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;->getButtonId()I

    move-result v1

    if-ne v2, v1, :cond_1

    .line 1943
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v8

    .line 1944
    const/4 v7, 0x1

    .line 1945
    goto :goto_1

    .line 1959
    .end local v6    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    :cond_6
    const/4 v0, -0x1

    if-eq v8, v0, :cond_3

    .line 1961
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;

    .line 1962
    .local v9, "temp":Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$ButtonInfo;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mRecentButtonInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public setSelectedButton(IZ)V
    .locals 3
    .param p1, "buttonId"    # I
    .param p2, "b"    # Z

    .prologue
    .line 629
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 630
    const-string v2, "TouchFunction setSelectedButton"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 631
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 641
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 632
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 633
    .local v0, "button":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v2

    if-ne v2, p1, :cond_2

    .line 634
    const-string v2, "TouchFunction button.getId() == buttonId"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 635
    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 631
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 637
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_1
.end method

.method public setSelectedButton(Landroid/view/View;Z)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "b"    # Z

    .prologue
    .line 643
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 644
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 653
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 645
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mMainButton:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 646
    .local v0, "button":Landroid/widget/LinearLayout;
    if-ne p1, v0, :cond_2

    .line 647
    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 644
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 649
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_1
.end method

.method public setViewWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 2586
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewWidth:I

    .line 2587
    return-void
.end method

.method public showArrow(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 174
    if-eqz p1, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 182
    :goto_0
    return-void

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mArrowLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public startHorizontalScrollViewAnimation()V
    .locals 0

    .prologue
    .line 2194
    return-void
.end method

.method public subMenuAnimate()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const v1, 0x3f666666    # 0.9f

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    .line 500
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 501
    .local v10, "animation":Landroid/view/animation/AnimationSet;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getAnimX(Z)F

    move-result v6

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 502
    .local v0, "scaleAnim":Landroid/view/animation/ScaleAnimation;
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const v1, 0x3f19999a    # 0.6f

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 503
    .local v9, "alphaAnim":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 504
    invoke-virtual {v10, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 505
    new-instance v1, Landroid/view/animation/interpolator/SineEaseInOut;

    invoke-direct {v1}, Landroid/view/animation/interpolator/SineEaseInOut;-><init>()V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 506
    const-wide/16 v2, 0x55

    invoke-virtual {v10, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 508
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$9;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 530
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x16000000

    if-eq v1, v2, :cond_0

    .line 531
    invoke-virtual {v10, v5}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 532
    :cond_0
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mAnimate:Z

    .line 533
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_1

    .line 534
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setAnimation(Z)V

    .line 535
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 536
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v11, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 537
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$10;

    invoke-direct {v2, p0, v10}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;Landroid/view/animation/AnimationSet;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 547
    return-void
.end method

.method public updateAnimation()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2701
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2702
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2704
    return-void
.end method
