.class public Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;
.super Ljava/lang/Object;
.source "PortraitEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;,
        Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;,
        Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitBufferInfo;,
        Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;
    }
.end annotation


# static fields
.field public static final ORGINALVIEW_OFF:I = 0x0

.field public static final ORGINALVIEW_ON:I = 0x1

.field public static isFirstTimeDnL:Z

.field private static myCallback:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;


# instance fields
.field private MAX_BRUSH_SIZE:I

.field private mBrushSize:I

.field private mEffectType:I

.field private mFaces:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mPaint:Landroid/graphics/Paint;

.field private mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

.field private mPreviewBuffer:[I

.field private mPreviewHeight:I

.field private mPreviewWidth:I

.field private mStep:I

.field private mx:I

.field private my:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;

    .line 450
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->isFirstTimeDnL:Z

    .line 451
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 441
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 442
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    .line 443
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mFaces:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

    .line 445
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mEffectType:I

    .line 446
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mStep:I

    .line 447
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mx:I

    .line 448
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->my:I

    .line 452
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPaint:Landroid/graphics/Paint;

    .line 454
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mBrushSize:I

    .line 455
    const/16 v0, 0x8c

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->MAX_BRUSH_SIZE:I

    .line 456
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewBuffer:[I

    .line 457
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewWidth:I

    .line 458
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewHeight:I

    .line 23
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 3
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 441
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 442
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    .line 443
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mFaces:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

    .line 445
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mEffectType:I

    .line 446
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mStep:I

    .line 447
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mx:I

    .line 448
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->my:I

    .line 452
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPaint:Landroid/graphics/Paint;

    .line 454
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mBrushSize:I

    .line 455
    const/16 v0, 0x8c

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->MAX_BRUSH_SIZE:I

    .line 456
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewBuffer:[I

    .line 457
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewWidth:I

    .line 458
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewHeight:I

    .line 26
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 27
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPaint:Landroid/graphics/Paint;

    .line 28
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 30
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mFaces:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

    .line 31
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;-><init>(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    .line 32
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;I)V
    .locals 0

    .prologue
    .line 446
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mStep:I

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)I
    .locals 1

    .prologue
    .line 445
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mEffectType:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)I
    .locals 1

    .prologue
    .line 446
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mStep:I

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;[I)V
    .locals 0

    .prologue
    .line 456
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewBuffer:[I

    return-void
.end method

.method static synthetic access$5()Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;
    .locals 1

    .prologue
    .line 440
    sget-object v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;

    return-object v0
.end method


# virtual methods
.method public addEvent(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->applyPreview(I)I

    .line 130
    :cond_0
    return-void
.end method

.method public applyOriginal()[I
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 133
    const/4 v1, 0x0

    .line 134
    .local v1, "output":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewBuffer:[I

    if-eqz v3, :cond_0

    .line 136
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewBuffer:[I

    .line 137
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 138
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    .line 139
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 136
    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 141
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    .line 142
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v4

    .line 143
    const/4 v5, 0x1

    .line 140
    invoke-static {v8, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 144
    .local v0, "tempBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    new-array v1, v3, [I

    .line 145
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 146
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([I)V

    .line 149
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 150
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 151
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewBuffer:[I

    .line 152
    const/4 v1, 0x0

    .line 154
    .end local v0    # "tempBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v2

    return-object v2
.end method

.method public declared-synchronized applypreviewRedEye(FFIZ)V
    .locals 9
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "rotation"    # I
    .param p4, "isPortrait"    # Z

    .prologue
    const/16 v8, 0xb4

    const/16 v7, 0xa

    .line 191
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 192
    .local v0, "previewInput":[I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 193
    .local v1, "previewOutput":[I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 194
    .local v2, "previewWidth":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 195
    .local v3, "previewHeight":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    add-int/2addr v4, v5

    shr-int/lit8 v6, v4, 0x5

    .line 197
    .local v6, "radius":I
    float-to-int v4, p1

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mx:I

    .line 198
    float-to-int v4, p2

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->my:I

    .line 199
    const/16 v4, 0x10e

    if-ne p3, v4, :cond_0

    if-eqz p4, :cond_3

    :cond_0
    if-ne p3, v8, :cond_1

    if-eqz p4, :cond_3

    :cond_1
    if-nez p3, :cond_2

    if-eqz p4, :cond_3

    :cond_2
    const/16 v4, 0x5a

    if-ne p3, v4, :cond_6

    if-nez p4, :cond_6

    .line 200
    :cond_3
    const/16 v4, 0x28

    if-le v6, v4, :cond_4

    .line 201
    const/16 v6, 0x28

    .line 202
    :cond_4
    if-ge v6, v7, :cond_5

    .line 203
    const/16 v6, 0xa

    .line 218
    :cond_5
    :goto_0
    float-to-int v4, p1

    float-to-int v5, p2

    invoke-static/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->removeRedeyeApplyOnFull([I[IIIIII)I

    .line 219
    sget-object v4, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;->afterApplyPreview()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    monitor-exit p0

    return-void

    .line 204
    :cond_6
    if-ne p3, v8, :cond_8

    if-eqz p4, :cond_8

    .line 205
    const/16 v4, 0x46

    if-le v6, v4, :cond_7

    .line 206
    const/16 v6, 0x46

    .line 207
    :cond_7
    if-ge v6, v7, :cond_5

    .line 208
    const/16 v6, 0xa

    .line 209
    goto :goto_0

    .line 211
    :cond_8
    const/16 v4, 0x21

    if-le v6, v4, :cond_9

    .line 212
    const/16 v6, 0x21

    .line 213
    :cond_9
    if-ge v6, v7, :cond_5

    .line 214
    const/16 v6, 0xa

    goto :goto_0

    .line 191
    .end local v0    # "previewInput":[I
    .end local v1    # "previewOutput":[I
    .end local v2    # "previewWidth":I
    .end local v3    # "previewHeight":I
    .end local v6    # "radius":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;)V
    .locals 1
    .param p1, "portraitEffect"    # Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;

    .prologue
    .line 35
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 36
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mEffectType:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mEffectType:I

    .line 37
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewBuffer:[I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewBuffer:[I

    .line 38
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    .line 39
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewWidth:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewWidth:I

    .line 40
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewHeight:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewHeight:I

    .line 41
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 45
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPaint:Landroid/graphics/Paint;

    .line 49
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mFaces:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mFaces:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mEyes:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Eye;

    .line 52
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mFaces:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFaces:[Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo$Face;

    .line 53
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mFaces:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->finishPortraitEngine()V

    .line 59
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    .line 61
    :cond_1
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewBuffer:[I

    .line 63
    return-void
.end method

.method public doDone()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 67
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewWidth:I

    .line 68
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewHeight:I

    .line 70
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewWidth:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewHeight:I

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewBuffer:[I

    .line 71
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewBuffer:[I

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewWidth:I

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPreviewHeight:I

    mul-int/2addr v2, v3

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 72
    return-void
.end method

.method public finishPortraitEngine()V
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->finishPortraitEngine()V

    .line 240
    :cond_0
    return-void
.end method

.method public getBrushSize()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mBrushSize:I

    return v0
.end method

.method public getCurrentStep()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mStep:I

    return v0
.end method

.method public getEffectType()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mEffectType:I

    return v0
.end method

.method public getFaceInfo()Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mFaces:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 186
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMaxBrushSize()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->MAX_BRUSH_SIZE:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 181
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(IZ)Z
    .locals 4
    .param p1, "effectType"    # I
    .param p2, "forceInitPortraitEngine"    # Z

    .prologue
    const/4 v3, 0x0

    .line 76
    const-string v2, "hanyoung Portraint => init"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 78
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mEffectType:I

    .line 79
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 80
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 81
    .local v0, "previewInput":[I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 83
    .local v1, "previewOutput":[I
    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 85
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->initPortraitEngine(Z)Z

    move-result v2

    return v2
.end method

.method public initPortraitEngine(Z)Z
    .locals 1
    .param p1, "forceInitPortraitEngine"    # Z

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->initPortraitEngine(Z)V

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mFaces:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$FaceInfo;->mFacenum:I

    if-gtz v0, :cond_1

    .line 231
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->finishPortraitEngine()V

    .line 232
    const/4 v0, 0x0

    .line 234
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onConfigurationChanged()V
    .locals 0

    .prologue
    .line 119
    return-void
.end method

.method public setBrushSize(I)V
    .locals 2
    .param p1, "brushsize"    # I

    .prologue
    .line 171
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mBrushSize:I

    .line 172
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mBrushSize:I

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mBrushSize:I

    div-int/lit8 v1, v1, 0x8

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/jni/Util;->set_brush_size(II)V

    .line 173
    return-void
.end method

.method public setOnCallback(Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;

    .prologue
    .line 176
    sput-object p1, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;

    .line 177
    return-void
.end method

.method public showOrginalView(I)V
    .locals 6
    .param p1, "state"    # I

    .prologue
    const/4 v5, 0x0

    .line 90
    if-nez p1, :cond_2

    .line 92
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    if-eqz v4, :cond_0

    .line 93
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mPortraitEngineManager:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mStep:I

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$PortraitEngineManager;->applyPreview(I)I

    .line 95
    :cond_0
    sget-object v4, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;

    if-eqz v4, :cond_1

    .line 97
    sget-object v4, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;->invalidate()V

    .line 115
    :cond_1
    :goto_0
    return-void

    .line 100
    :cond_2
    const/4 v4, 0x1

    if-ne p1, v4, :cond_1

    .line 102
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 103
    .local v1, "previewInput":[I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v2

    .line 105
    .local v2, "previewOutput":[I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 106
    .local v3, "previewWidth":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    .line 108
    .local v0, "previewHeight":I
    mul-int v4, v3, v0

    invoke-static {v1, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 110
    sget-object v4, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;

    if-eqz v4, :cond_1

    .line 112
    sget-object v4, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Core/PortraitEffect$OnCallback;->invalidate()V

    goto :goto_0
.end method
