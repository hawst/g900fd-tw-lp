.class public interface abstract Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;
.super Ljava/lang/Object;
.source "ApplyOriginalThreadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnCallBackForHistory"
.end annotation


# virtual methods
.method public abstract addHistory([III)V
.end method

.method public abstract redo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
.end method

.method public abstract redoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
.end method

.method public abstract undo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
.end method

.method public abstract undoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
.end method
