.class Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter$1;
.super Landroid/widget/BaseAdapter;
.source "DecorationViewPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    .line 368
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 406
    const/4 v0, 0x1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 401
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 396
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # Landroid/view/ViewGroup;

    .prologue
    .line 372
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->mContext:Landroid/app/Activity;

    .line 373
    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 372
    check-cast v2, Landroid/view/LayoutInflater;

    .line 374
    const v3, 0x7f030052

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 375
    .local v1, "v":Landroid/view/View;
    const v2, 0x7f090127

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 376
    .local v0, "noRecentText":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter$1;->this$0:Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;

    # getter for: Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->noRecentType:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;->access$0(Lcom/sec/android/mimage/photoretouching/util/DecorationViewPagerAdapter;)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 391
    :goto_0
    return-object v1

    .line 378
    :sswitch_0
    const v2, 0x7f0601f6

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 381
    :sswitch_1
    const v2, 0x7f0601f4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 384
    :sswitch_2
    const v2, 0x7f0601f3

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 387
    :sswitch_3
    const v2, 0x7f0601f5

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 376
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x5 -> :sswitch_0
        0x9 -> :sswitch_3
        0xe -> :sswitch_1
    .end sparse-switch
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 410
    const/4 v0, 0x0

    return v0
.end method
