.class Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;
.super Ljava/lang/Object;
.source "LabelView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->initLabelTextDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    .line 781
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    .locals 1

    .prologue
    .line 781
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    return-object v0
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 8
    .param p1, "di"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v6, 0x0

    .line 784
    move-object v4, p1

    check-cast v4, Landroid/app/AlertDialog;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 785
    .local v0, "b":Landroid/widget/Button;
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 788
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Landroid/content/Context;

    move-result-object v4

    .line 789
    const-string v5, "input_method"

    .line 788
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    .local v3, "imm":Landroid/view/inputmethod/InputMethodManager;
    move-object v4, p1

    .line 790
    check-cast v4, Landroid/app/Dialog;

    const v5, 0x7f09001f

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/InputText;

    .line 791
    .local v1, "eView":Lcom/sec/android/mimage/photoretouching/Gui/InputText;
    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->setPositiveButton(Landroid/widget/Button;)V

    .line 792
    invoke-virtual {v3, v1, v6}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 795
    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v4

    if-nez v4, :cond_0

    .line 797
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    .line 798
    .local v2, "handler":Landroid/os/Handler;
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$1;

    invoke-direct {v4, p0, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;Landroid/view/inputmethod/InputMethodManager;Lcom/sec/android/mimage/photoretouching/Gui/InputText;)V

    .line 804
    const-wide/16 v6, 0x12c

    .line 798
    invoke-virtual {v2, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 841
    .end local v2    # "handler":Landroid/os/Handler;
    :cond_0
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;

    invoke-direct {v4, p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$12;Landroid/content/DialogInterface;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 888
    return-void
.end method
