.class public Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;
.super Landroid/app/Activity;
.source "EffectManagerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;
    }
.end annotation


# instance fields
.field private mBackUpBtnInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

.field private mBtnInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

.field private mCancelText:Landroid/widget/TextView;

.field private mDoneText:Landroid/widget/TextView;

.field private mEffectCancel:Landroid/widget/LinearLayout;

.field private mEffectDone:Landroid/widget/LinearLayout;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mThumbnailList:[Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 740
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    .line 741
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    .line 742
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    .line 743
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 744
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    .line 746
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectDone:Landroid/widget/LinearLayout;

    .line 747
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectCancel:Landroid/widget/LinearLayout;

    .line 46
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->initialize()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 739
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 739
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 746
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectDone:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)V
    .locals 0

    .prologue
    .line 639
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->done()V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 747
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectCancel:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)V
    .locals 0

    .prologue
    .line 512
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->cancel()V

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 749
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mCancelText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 748
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mDoneText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    return-object v0
.end method

.method private adjustActionbarHeight()V
    .locals 4

    .prologue
    .line 369
    const v2, 0x7f0900bf

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 370
    .local v0, "actionbar":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 371
    .local v1, "param":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 372
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050294

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 375
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 376
    return-void

    .line 374
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f05028f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_0
.end method

.method private backUpButtonOrder()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 257
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    if-eqz v3, :cond_1

    .line 259
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 261
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_2

    .line 265
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    .line 267
    .end local v1    # "i":I
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    .line 268
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->getButtonList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_3

    .line 277
    .end local v1    # "i":I
    :cond_1
    return-void

    .line 263
    .restart local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 261
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 270
    :cond_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->getButtonList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    if-eqz v3, :cond_4

    .line 271
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->getSelectedButtonLayout()Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

    move-result-object v3

    invoke-virtual {v3, v5, v5}, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->setDisableIntercept(FF)V

    .line 272
    :cond_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->getButtonList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    .line 273
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;
    new-instance v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getButtonId()I

    move-result v3

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->isChecked()Z

    move-result v4

    invoke-direct {v2, v3, v1, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;-><init>(IIZ)V

    .line 274
    .local v2, "info":Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private cancel()V
    .locals 1

    .prologue
    .line 514
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->setResult(I)V

    .line 515
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->finish()V

    .line 516
    return-void
.end method

.method private checkThumbnailList()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 281
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    array-length v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eq v3, v4, :cond_0

    .line 283
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-lt v3, v5, :cond_2

    .line 291
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    .line 294
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    if-nez v3, :cond_1

    .line 296
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    .line 298
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0201c5

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 299
    .local v0, "temp":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    new-array v1, v3, [I

    .line 300
    .local v1, "src":[I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 302
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v9, v2, :cond_4

    .line 307
    .end local v0    # "temp":Landroid/graphics/Bitmap;
    .end local v1    # "src":[I
    .end local v9    # "i":I
    :cond_1
    return-void

    .line 283
    :cond_2
    aget-object v8, v4, v3

    .line 285
    .local v8, "b":Landroid/graphics/Bitmap;
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v6

    if-nez v6, :cond_3

    .line 287
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 283
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 304
    .end local v8    # "b":Landroid/graphics/Bitmap;
    .restart local v0    # "temp":Landroid/graphics/Bitmap;
    .restart local v1    # "src":[I
    .restart local v9    # "i":I
    :cond_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getId()I

    move-result v2

    invoke-direct {p0, v1, v4, v5, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getThumbnail([IIII)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v3, v9

    .line 302
    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method private done()V
    .locals 1

    .prologue
    .line 641
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->saveToDB()Z

    .line 642
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->setResult(I)V

    .line 643
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->finish()V

    .line 644
    return-void
.end method

.method private getThumbnail([IIII)Landroid/graphics/Bitmap;
    .locals 23
    .param p1, "in"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "effectType"    # I

    .prologue
    .line 521
    mul-int v3, p2, p3

    new-array v5, v3, [I

    .line 522
    .local v5, "out":[I
    mul-int v3, p2, p3

    new-array v4, v3, [B

    .line 523
    .local v4, "mask":[B
    const/4 v12, 0x0

    .line 524
    .local v12, "texture1":[I
    const/16 v22, 0x0

    .line 526
    .local v22, "texture2":[I
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 527
    .local v9, "roi":Landroid/graphics/Rect;
    const/4 v3, 0x0

    const/4 v6, 0x0

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v9, v0, v1, v3, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 528
    packed-switch p4, :pswitch_data_0

    :goto_0
    :pswitch_0
    move-object/from16 v13, v22

    .line 627
    .end local v22    # "texture2":[I
    .local v13, "texture2":[I
    :goto_1
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 628
    .local v14, "ret":Landroid/graphics/Bitmap;
    const/16 v16, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object v15, v5

    move/from16 v17, p2

    move/from16 v20, p2

    move/from16 v21, p3

    invoke-virtual/range {v14 .. v21}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 630
    const/4 v5, 0x0

    .line 631
    const/4 v4, 0x0

    .line 632
    const/4 v9, 0x0

    .line 633
    const/4 v12, 0x0

    .line 634
    const/4 v13, 0x0

    .line 636
    .end local v14    # "ret":Landroid/graphics/Bitmap;
    :goto_2
    return-object v14

    .line 531
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_1
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyVintage([I[III)V

    move-object/from16 v13, v22

    .line 532
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto :goto_1

    .line 534
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_2
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyPopArt([I[III)V

    move-object/from16 v13, v22

    .line 535
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto :goto_1

    .line 537
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_3
    const/16 v8, 0xa

    move-object/from16 v3, p1

    move/from16 v6, p2

    move/from16 v7, p3

    invoke-static/range {v3 .. v9}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlGreyscale([I[B[IIIILandroid/graphics/Rect;)V

    move-object/from16 v13, v22

    .line 538
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto :goto_1

    .line 540
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_4
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySepia([I[III)I

    move-object/from16 v13, v22

    .line 541
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto :goto_1

    .line 543
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_5
    const/4 v3, 0x4

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySharpen([I[IIII)I

    move-object/from16 v13, v22

    .line 544
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto :goto_1

    .line 546
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_6
    const/16 v3, 0x32

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySoftglow([I[IIII)V

    move-object/from16 v13, v22

    .line 547
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto :goto_1

    .line 549
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_7
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyRainbow([I[III)V

    move-object/from16 v13, v22

    .line 550
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto :goto_1

    .line 552
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_8
    const v3, 0x16001607

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getTexture(III)[I

    move-result-object v12

    .line 553
    if-nez v12, :cond_0

    .line 554
    const/4 v14, 0x0

    move-object/from16 v13, v22

    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto :goto_2

    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :cond_0
    move-object/from16 v10, p1

    move-object v11, v5

    move-object v13, v4

    move/from16 v14, p2

    move/from16 v15, p3

    move-object/from16 v16, v9

    .line 555
    invoke-static/range {v10 .. v16}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyStardustPartial([I[I[I[BIILandroid/graphics/Rect;)V

    move-object/from16 v13, v22

    .line 556
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .line 558
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_9
    const v3, 0x16001608

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getTexture(III)[I

    move-result-object v12

    .line 559
    if-nez v12, :cond_1

    .line 560
    const/4 v14, 0x0

    move-object/from16 v13, v22

    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_2

    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :cond_1
    move-object/from16 v10, p1

    move-object v11, v5

    move-object v13, v4

    move/from16 v14, p2

    move/from16 v15, p3

    move-object/from16 v16, v9

    .line 561
    invoke-static/range {v10 .. v16}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyLightFlarePartial([I[I[I[BIILandroid/graphics/Rect;)V

    move-object/from16 v13, v22

    .line 562
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .line 564
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_a
    const v3, 0x16001609

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getTexture(III)[I

    move-result-object v12

    .line 565
    if-nez v12, :cond_2

    .line 566
    const/4 v14, 0x0

    move-object/from16 v13, v22

    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_2

    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :cond_2
    move-object/from16 v10, p1

    move-object v11, v5

    move-object v13, v4

    move/from16 v14, p2

    move/from16 v15, p3

    move-object/from16 v16, v9

    .line 567
    invoke-static/range {v10 .. v16}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyLightStreakPartial([I[I[I[BIILandroid/graphics/Rect;)V

    move-object/from16 v13, v22

    .line 568
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .line 570
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_b
    const v3, 0x16001612

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getTexture(III)[I

    move-result-object v12

    .line 571
    if-nez v12, :cond_3

    .line 572
    const/4 v14, 0x0

    move-object/from16 v13, v22

    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_2

    .line 573
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :cond_3
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v12, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySketchTexture([I[I[III)I

    move-object/from16 v13, v22

    .line 574
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_c
    move-object/from16 v3, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move-object v8, v9

    .line 576
    invoke-static/range {v3 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyInvert([I[B[IIILandroid/graphics/Rect;)V

    move-object/from16 v13, v22

    .line 577
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .line 579
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_d
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyCartoonize([I[III)I

    move-object/from16 v13, v22

    .line 580
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .line 582
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_e
    const v3, 0x1600160f

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getTexture(III)[I

    move-result-object v12

    .line 583
    const v3, 0x16001614

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getTexture(III)[I

    move-result-object v13

    .line 584
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    if-eqz v12, :cond_4

    if-nez v13, :cond_5

    .line 585
    :cond_4
    const/4 v14, 0x0

    goto/16 :goto_2

    :cond_5
    move-object/from16 v10, p1

    move-object v11, v5

    move/from16 v14, p2

    move/from16 v15, p3

    .line 586
    invoke-static/range {v10 .. v15}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyImpressionist([I[I[I[III)V

    goto/16 :goto_1

    .line 589
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_f
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyDownlight([I[III)V

    move-object/from16 v13, v22

    .line 590
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .line 592
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_10
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyBluewash([I[III)V

    move-object/from16 v13, v22

    .line 593
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .line 595
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_11
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyNostalgia([I[III)V

    move-object/from16 v13, v22

    .line 596
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .line 598
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_12
    const/16 v8, 0x32

    move-object/from16 v3, p1

    move/from16 v6, p2

    move/from16 v7, p3

    invoke-static/range {v3 .. v9}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlFadedColour([I[B[IIIILandroid/graphics/Rect;)V

    move-object/from16 v13, v22

    .line 599
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .line 601
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_13
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyVignette([I[III)V

    move-object/from16 v13, v22

    .line 602
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .line 604
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_14
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyTurquoise([I[III)V

    move-object/from16 v13, v22

    .line 605
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .line 607
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_15
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyYellowglow([I[III)V

    move-object/from16 v13, v22

    .line 608
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .line 610
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_16
    const v3, 0x16001616

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getTexture(III)[I

    move-result-object v12

    .line 611
    const v3, 0x16001617

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getTexture(III)[I

    move-result-object v13

    .line 612
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    if-eqz v12, :cond_6

    if-nez v13, :cond_7

    .line 613
    :cond_6
    const/4 v14, 0x0

    goto/16 :goto_2

    :cond_7
    move-object/from16 v10, p1

    move-object v11, v5

    move/from16 v14, p2

    move/from16 v15, p3

    .line 614
    invoke-static/range {v10 .. v15}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyDawnCast([I[I[I[III)V

    goto/16 :goto_1

    .line 617
    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_17
    const/16 v8, 0x64

    move-object/from16 v3, p1

    move/from16 v6, p2

    move/from16 v7, p3

    invoke-static/range {v3 .. v9}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyPosterizePartial([I[B[IIIILandroid/graphics/Rect;)I

    move-object/from16 v13, v22

    .line 618
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_18
    move-object/from16 v14, p1

    move-object v15, v5

    move-object/from16 v16, v4

    move/from16 v17, p2

    move/from16 v18, p3

    move-object/from16 v19, v9

    .line 620
    invoke-static/range {v14 .. v19}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyGothicNoirPartial([I[I[BIILandroid/graphics/Rect;)V

    move-object/from16 v13, v22

    .line 621
    .end local v22    # "texture2":[I
    .restart local v13    # "texture2":[I
    goto/16 :goto_1

    .end local v13    # "texture2":[I
    .restart local v22    # "texture2":[I
    :pswitch_19
    move-object/from16 v3, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move-object v8, v9

    .line 623
    invoke-static/range {v3 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyMagicPenPartial([I[B[IIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 528
    nop

    :pswitch_data_0
    .packed-switch 0x16001600
        :pswitch_13
        :pswitch_1
        :pswitch_11
        :pswitch_12
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_f
        :pswitch_10
        :pswitch_5
        :pswitch_6
        :pswitch_14
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_2
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method private initActionBar()V
    .locals 11

    .prologue
    const v10, 0x7f0900c3

    const v9, 0x7f0900c0

    const v8, 0x7f0201c3

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 380
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->adjustActionbarHeight()V

    .line 382
    const-string v4, "sans-serif"

    invoke-static {v4, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    .line 383
    .local v2, "font":Landroid/graphics/Typeface;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mDoneText:Landroid/widget/TextView;

    if-eqz v4, :cond_0

    .line 384
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mDoneText:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 385
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mCancelText:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    .line 386
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mCancelText:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 389
    :cond_1
    invoke-virtual {p0, v10}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectDone:Landroid/widget/LinearLayout;

    .line 390
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectDone:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 391
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectDone:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 392
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectDone:Landroid/widget/LinearLayout;

    new-instance v5, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$1;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$1;-><init>(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 417
    invoke-virtual {p0, v9}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectCancel:Landroid/widget/LinearLayout;

    .line 418
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectCancel:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 419
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectCancel:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 420
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectCancel:Landroid/widget/LinearLayout;

    new-instance v5, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$2;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$2;-><init>(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 446
    new-instance v3, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$3;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$3;-><init>(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)V

    .line 503
    .local v3, "listener":Landroid/view/View$OnTouchListener;
    invoke-virtual {p0, v10}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 504
    .local v1, "done":Landroid/widget/LinearLayout;
    invoke-virtual {p0, v9}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 506
    .local v0, "cancel":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 507
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 509
    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 510
    return-void
.end method

.method private initButton()V
    .locals 9

    .prologue
    .line 311
    const v0, 0x7f0900c3

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 313
    .local v6, "done":Landroid/widget/LinearLayout;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->destroy()V

    .line 316
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    .line 319
    :cond_0
    new-instance v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    .line 320
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    invoke-virtual {v0, v6}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->init(Landroid/view/View;)V

    .line 322
    const/4 v1, 0x0

    .local v1, "id":I
    const/4 v2, 0x0

    .line 323
    .local v2, "index":I
    const/4 v5, 0x0

    .line 325
    .local v5, "use":Z
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->checkThumbnailList()V

    .line 327
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v7, v0, :cond_1

    .line 365
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->setButtonsPos()V

    .line 366
    return-void

    .line 329
    :cond_1
    const/4 v8, 0x0

    .line 330
    .local v8, "j":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 332
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v8, v0, :cond_3

    .line 347
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v8, v0, :cond_6

    .line 327
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 334
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getId()I

    move-result v3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getId()I

    move-result v0

    if-eq v3, v0, :cond_2

    .line 332
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 342
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getIndex()I

    move-result v0

    if-eq v0, v7, :cond_2

    .line 340
    add-int/lit8 v8, v8, 0x1

    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v8, v0, :cond_4

    goto :goto_2

    .line 350
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getId()I

    move-result v1

    .line 351
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getIndex()I

    move-result v2

    .line 352
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 353
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getUse()Z

    move-result v5

    .line 357
    :goto_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    .line 360
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v8

    .line 361
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getStringId(I)I

    move-result v4

    .line 357
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->addButton(IILandroid/graphics/Bitmap;IZ)V

    goto :goto_3

    .line 355
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getUse()Z

    move-result v5

    goto :goto_4
.end method

.method private initialize()V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->loadFromDB()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->initActionBar()V

    .line 124
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->initButton()V

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->finish()V

    goto :goto_0
.end method

.method private loadFromDB()Z
    .locals 2

    .prologue
    .line 210
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    if-nez v1, :cond_0

    .line 211
    new-instance v1, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 212
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->openEffect()Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 213
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryEffectButtonInfo()Landroid/database/Cursor;

    move-result-object v0

    .line 214
    .local v0, "effectCs":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 216
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getEffectButtonInfoList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    .line 218
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->close()V

    .line 220
    const/4 v1, 0x1

    return v1
.end method

.method private makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V
    .locals 8
    .param p1, "src"    # Landroid/graphics/Bitmap;
    .param p2, "outBuffer"    # [I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 733
    const/4 v1, 0x1

    invoke-static {p1, p3, p4, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, "resized":Landroid/graphics/Bitmap;
    move-object v1, p2

    move v3, p3

    move v4, v2

    move v5, v2

    move v6, p3

    move v7, p4

    .line 734
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 735
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 736
    const/4 v0, 0x0

    .line 737
    return-void
.end method

.method private reInit()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 133
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->close()V

    .line 136
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->destroy()V

    .line 141
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    .line 143
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->adjustActionbarHeight()V

    .line 144
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->initButton()V

    .line 145
    return-void
.end method

.method private saveToDB()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    .line 225
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    if-nez v0, :cond_0

    .line 226
    new-instance v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->openEffect()Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 229
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->getButtonList()Ljava/util/ArrayList;

    move-result-object v6

    .line 231
    .local v6, "btnList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;>;"
    const/4 v1, 0x0

    .local v1, "id":I
    const/4 v2, 0x0

    .local v2, "index":I
    const/4 v4, 0x0

    .line 232
    .local v4, "use":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v7, v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->close()V

    .line 252
    return v5

    .line 234
    :cond_1
    const/4 v8, 0x0

    .line 235
    .local v8, "j":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v8, v0, :cond_3

    .line 241
    :cond_2
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getButtonId()I

    move-result v1

    .line 242
    move v2, v8

    .line 243
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 244
    const/4 v4, 0x1

    .line 248
    :goto_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    const/4 v3, -0x1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->updateButtonInfo(IIIIZ)V

    .line 232
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 237
    :cond_3
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->getButtonId()I

    move-result v3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->getId()I

    move-result v0

    if-eq v3, v0, :cond_2

    .line 235
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 246
    :cond_4
    const/4 v4, 0x0

    goto :goto_2
.end method


# virtual methods
.method public getTexture(III)[I
    .locals 10
    .param p1, "effectType"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 648
    const/4 v8, 0x0

    .line 650
    .local v8, "resId":I
    packed-switch p1, :pswitch_data_0

    .line 678
    :goto_0
    :pswitch_0
    if-eqz v8, :cond_0

    .line 679
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 680
    .local v9, "tmp":Landroid/graphics/Bitmap;
    const/4 v3, 0x1

    invoke-static {v9, p2, p3, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 681
    .local v0, "bmp":Landroid/graphics/Bitmap;
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    .line 682
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    new-array v1, v3, [I

    .line 683
    .local v1, "data":[I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 687
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    .end local v1    # "data":[I
    .end local v9    # "tmp":Landroid/graphics/Bitmap;
    :goto_1
    return-object v1

    .line 653
    :pswitch_1
    const v8, 0x7f02054d

    .line 654
    goto :goto_0

    .line 656
    :pswitch_2
    const v8, 0x7f020551

    .line 657
    goto :goto_0

    .line 659
    :pswitch_3
    const v8, 0x7f020550

    .line 660
    goto :goto_0

    .line 662
    :pswitch_4
    const v8, 0x7f020552

    .line 663
    goto :goto_0

    .line 665
    :pswitch_5
    const v8, 0x7f02054e

    .line 666
    goto :goto_0

    .line 668
    :pswitch_6
    const v8, 0x7f02054f

    .line 669
    goto :goto_0

    .line 671
    :pswitch_7
    const v8, 0x7f020553

    .line 672
    goto :goto_0

    .line 674
    :pswitch_8
    const v8, 0x7f020554

    goto :goto_0

    .line 687
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 650
    :pswitch_data_0
    .packed-switch 0x16001607
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 158
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 159
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->cancel()V

    .line 160
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 149
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 150
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->backUpButtonOrder()V

    .line 151
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->reInit()V

    .line 152
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v3, 0x400

    .line 51
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->requestWindowFeature(I)Z

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 55
    const v2, 0x7f030040

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->setContentView(I)V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 58
    .local v1, "win":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 59
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 60
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 61
    const/high16 v2, 0x100000

    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/2addr v2, v3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 62
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 65
    const v2, 0x7f0900c4

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mDoneText:Landroid/widget/TextView;

    .line 66
    const v2, 0x7f0900c2

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mCancelText:Landroid/widget/TextView;

    .line 67
    new-instance v2, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;-><init>(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$InitAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 68
    return-void
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 166
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    if-eqz v1, :cond_0

    .line 168
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->close()V

    .line 169
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBottomButtonDataBaseHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 171
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    if-eqz v1, :cond_1

    .line 173
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->destroy()V

    .line 174
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mButtonControlManager:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    .line 176
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    array-length v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 178
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    array-length v1, v1

    if-lt v0, v1, :cond_6

    .line 186
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    .line 188
    .end local v0    # "i":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    .line 190
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_8

    .line 192
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    .line 194
    .end local v0    # "i":I
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    .line 196
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_9

    .line 198
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    .line 200
    .end local v0    # "i":I
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_5

    .line 202
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 203
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 205
    :cond_5
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 206
    return-void

    .line 180
    .restart local v0    # "i":I
    :cond_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_7

    .line 182
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 183
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mThumbnailList:[Landroid/graphics/Bitmap;

    aput-object v3, v1, v0

    .line 178
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 191
    :cond_8
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 197
    :cond_9
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mBackUpBtnInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 196
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method
