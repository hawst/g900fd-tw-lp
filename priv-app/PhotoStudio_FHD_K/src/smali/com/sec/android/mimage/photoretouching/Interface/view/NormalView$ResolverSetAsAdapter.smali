.class public Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ResolverSetAsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "NormalView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ResolverSetAsAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/content/pm/ResolveInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3285
    .local p2, "objects":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 3282
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ResolverSetAsAdapter;->mContext:Landroid/content/Context;

    .line 3286
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ResolverSetAsAdapter;->mContext:Landroid/content/Context;

    .line 3287
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ResolverSetAsAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 3288
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    .line 3292
    if-nez p2, :cond_0

    .line 3293
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ResolverSetAsAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f030069

    .line 3294
    const/4 v8, 0x0

    .line 3293
    invoke-virtual {v6, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 3296
    :cond_0
    const v6, 0x7f090136

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 3297
    .local v5, "text":Landroid/widget/TextView;
    const v6, 0x7f090135

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 3298
    .local v1, "icon":Landroid/widget/ImageView;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v6

    if-nez v6, :cond_1

    .line 3299
    sget v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 3302
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ResolverSetAsAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 3304
    .local v3, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ResolverSetAsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 3305
    .local v2, "info":Landroid/content/pm/ResolveInfo;
    invoke-virtual {v2, v3}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3308
    :try_start_0
    iget-object v6, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v4

    .line 3309
    .local v4, "resources":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/pm/ResolveInfo;->getIconResource()I

    move-result v6

    .line 3310
    sget v7, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    .line 3309
    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3312
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {p2, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3313
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ResolverSetAsAdapter;->mContext:Landroid/content/Context;

    invoke-static {v6, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHovering(Landroid/content/Context;Landroid/view/View;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3319
    .end local v4    # "resources":Landroid/content/res/Resources;
    :goto_1
    return-object p2

    .line 3301
    .end local v2    # "info":Landroid/content/pm/ResolveInfo;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    const/high16 v6, -0x1000000

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 3314
    .restart local v2    # "info":Landroid/content/pm/ResolveInfo;
    .restart local v3    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v0

    .line 3315
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 3316
    invoke-virtual {v2, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method
