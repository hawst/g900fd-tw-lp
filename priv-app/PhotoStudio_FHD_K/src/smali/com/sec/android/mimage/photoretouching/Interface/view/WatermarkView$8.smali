.class Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$8;
.super Ljava/lang/Object;
.source "WatermarkView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->init3DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    .line 383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 0
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 398
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 387
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->doDone()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)V

    .line 388
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 389
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resetMenu()V

    .line 390
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    if-nez v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;->access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/WatermarkView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->openDrawer()V

    .line 392
    :cond_0
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 404
    return-void
.end method
