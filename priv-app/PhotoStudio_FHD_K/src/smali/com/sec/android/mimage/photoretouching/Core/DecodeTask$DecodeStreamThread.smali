.class Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;
.super Ljava/lang/Thread;
.source "DecodeTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DecodeStreamThread"
.end annotation


# instance fields
.field private mImageHeight:I

.field private mImageWidth:I

.field mQuramoptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

.field optsResolution:Landroid/graphics/BitmapFactory$Options;

.field stream:Ljava/io/InputStream;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/BitmapFactory$Options;Lcom/quramsoft/qrb/QuramBitmapFactory$Options;Ljava/io/InputStream;III)V
    .locals 2
    .param p2, "opts"    # Landroid/graphics/BitmapFactory$Options;
    .param p3, "quramopts"    # Lcom/quramsoft/qrb/QuramBitmapFactory$Options;
    .param p4, "str"    # Ljava/io/InputStream;
    .param p5, "mode"    # I
    .param p6, "wdt"    # I
    .param p7, "hgt"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 498
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    .line 497
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 492
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    .line 493
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->mQuramoptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .line 494
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->stream:Ljava/io/InputStream;

    .line 495
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->mImageWidth:I

    .line 496
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->mImageHeight:I

    .line 499
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    .line 500
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->stream:Ljava/io/InputStream;

    .line 501
    invoke-static {p1, p5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$0(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;I)V

    .line 503
    iput p6, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->mImageWidth:I

    .line 504
    iput p7, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->mImageHeight:I

    .line 507
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->mQuramoptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    .line 508
    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 512
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 513
    .local v2, "time":Ljava/lang/Long;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeMode:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$1(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_7

    .line 517
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "bigheadk, mIsPng = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mSkiaMode:Z
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$2(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 518
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mSkiaMode:Z
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$2(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 520
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->stream:Ljava/io/InputStream;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v5, v6, v7}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$3(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/Bitmap;)V

    .line 521
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 523
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 524
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    mul-int/2addr v5, v6

    .line 525
    const v6, 0x7a1200

    .line 523
    # invokes: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->getSampleSize(II)I
    invoke-static {v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$5(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;II)I

    move-result v1

    .line 526
    .local v1, "sampleSize":I
    if-le v1, v10, :cond_0

    .line 527
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 528
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/2addr v5, v1

    .line 529
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    div-int/2addr v6, v1

    const/4 v7, 0x1

    .line 527
    invoke-static {v4, v5, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 530
    .local v3, "tmp":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 531
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v4, v3}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$3(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 557
    .end local v1    # "sampleSize":I
    .end local v3    # "tmp":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$4(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-nez v4, :cond_6

    .line 559
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v4, v9}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$7(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 560
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    .line 567
    :goto_1
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$8(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc2:Z
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$9(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 569
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # invokes: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->makeImageHandlerAboutStream()V
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$10(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    .line 579
    :cond_1
    :goto_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_2

    .line 581
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 626
    :cond_2
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIdx:I
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$14(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]decode bitmap time:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 627
    return-void

    .line 535
    :cond_3
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mFromDecode:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$6(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)I

    move-result v4

    if-ne v4, v10, :cond_4

    .line 537
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->stream:Ljava/io/InputStream;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v5, v6, v7}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$3(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/Bitmap;)V
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 547
    :catch_0
    move-exception v0

    .line 549
    .local v0, "e":Ljava/lang/NullPointerException;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v4, v9}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$7(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 550
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 551
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto/16 :goto_0

    .line 541
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_4
    :try_start_4
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->mImageHeight:I

    if-lez v4, :cond_5

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->mImageWidth:I

    if-lez v4, :cond_5

    .line 542
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->stream:Ljava/io/InputStream;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->mImageWidth:I

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->mImageHeight:I

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->mQuramoptions:Lcom/quramsoft/qrb/QuramBitmapFactory$Options;

    invoke-static {v5, v6, v7, v8}, Lcom/quramsoft/qrb/QuramBitmapFactory;->decodeStream(Ljava/io/InputStream;IILcom/quramsoft/qrb/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$3(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/Bitmap;)V
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 553
    :catch_1
    move-exception v0

    .line 555
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v4, v11}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$3(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 544
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :cond_5
    :try_start_5
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    .line 564
    :cond_6
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v4, v10}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$7(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    goto/16 :goto_1

    .line 572
    :catch_2
    move-exception v0

    .line 573
    .restart local v0    # "e":Ljava/lang/OutOfMemoryError;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v4, v11}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$3(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Landroid/graphics/Bitmap;)V

    .line 574
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v4, v9}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$11(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 575
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 576
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto/16 :goto_2

    .line 582
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :catch_3
    move-exception v0

    .line 583
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 594
    .end local v0    # "e":Ljava/io/IOException;
    :cond_7
    :try_start_6
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->stream:Ljava/io/InputStream;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v4, v5, v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_5

    .line 603
    :goto_4
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v4, v10}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$11(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 604
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mIsTaskCancel:Z
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$8(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->mDecodeSucc1:Z
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$12(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 606
    :try_start_7
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    iget v5, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->optsResolution:Landroid/graphics/BitmapFactory$Options;

    iget v6, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    # invokes: Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->preDecodingAboutStream(II)V
    invoke-static {v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$13(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;II)V
    :try_end_7
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_7 .. :try_end_7} :catch_6

    .line 616
    :cond_8
    :goto_5
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_2

    .line 618
    :try_start_8
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_3

    .line 619
    :catch_4
    move-exception v0

    .line 620
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 596
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 598
    .local v0, "e":Ljava/lang/NullPointerException;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v4, v9}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$11(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 599
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 600
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    goto :goto_4

    .line 607
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_6
    move-exception v0

    .line 609
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-static {v4, v9}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->access$11(Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;Z)V

    .line 610
    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->printStackTrace()V

    .line 611
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask$DecodeStreamThread;->this$0:Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/DecodeTask;->taskCancel()V

    .line 612
    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->printStackTrace()V

    goto :goto_5

    .line 571
    .end local v0    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_7
    move-exception v4

    goto/16 :goto_2
.end method
