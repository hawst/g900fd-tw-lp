.class public interface abstract Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect$MirrorCallback;
.super Ljava/lang/Object;
.source "MirrorEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/MirrorEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MirrorCallback"
.end annotation


# virtual methods
.method public abstract ableDone()V
.end method

.method public abstract ableDoneWithThread()V
.end method

.method public abstract actionBarIsEnableDone()Z
.end method

.method public abstract changeMirrorButtonTOUndoButton()V
.end method

.method public abstract getImageEditViewHeight()I
.end method

.method public abstract getImageEditViewWidth()I
.end method

.method public abstract invalidate()V
.end method

.method public abstract invalidateThread()V
.end method

.method public abstract setBottomButtonEnabled(Z)V
.end method

.method public abstract setEnabledWithChildren(Z)V
.end method

.method public abstract setViewLayerType(I)V
.end method

.method public abstract startMirrorRotateAnimation(IILandroid/graphics/RectF;[ILcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$MirrorAnimationViewCallback;)V
.end method

.method public abstract unableDone()V
.end method
