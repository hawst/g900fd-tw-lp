.class public Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
.super Ljava/lang/Object;
.source "GalleryExifInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$ColorSpace;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$ComponentsConfiguration;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$Compression;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$Contrast;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$ExposureMode;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$ExposureProgram;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$FileSource;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$Flash;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$GainControl;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$GpsAltitudeRef;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$GpsDifferential;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$GpsLatitudeRef;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$GpsLongitudeRef;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$GpsMeasureMode;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$GpsSpeedRef;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$GpsStatus;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$GpsTrackRef;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$LightSource;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$MeteringMode;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$Orientation;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$PhotometricInterpretation;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$PlanarConfiguration;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$ResolutionUnit;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$Saturation;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$SceneCapture;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$SceneType;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$SensingMethod;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$Sharpness;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$SubjectDistance;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$WhiteBalance;,
        Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface$YCbCrPositioning;
    }
.end annotation


# static fields
.field public static final CAMERA_INFO:I = 0x4

.field private static final DATETIME_FORMAT_STR:Ljava/lang/String; = "yyyy:MM:dd kk:mm:ss"

.field public static final DATE_TIME:I = 0x1

.field public static final DEFAULT_BYTE_ORDER:Ljava/nio/ByteOrder;

.field public static final DEFINITION_NULL:I = 0x0

.field private static final GPS_DATE_FORMAT_STR:Ljava/lang/String; = "yyyy:MM:dd"

.field public static final GPS_LOCATION_CITY:I = 0x2

.field public static final GPS_LOCATION_THOROUGHFARE:I = 0x3

.field public static final IFD_NULL:I = -0x1

.field public static MAX_COMPONENT_COUNT:I = 0x0

.field private static final NULL_ARGUMENT_STRING:Ljava/lang/String; = "Argument is null"

.field public static final ORIENTATION_NORMAL:S = 0x1s

.field public static final ORIENTATION_ROTATE_180:S = 0x3s

.field public static final ORIENTATION_ROTATE_270:S = 0x8s

.field public static final ORIENTATION_ROTATE_90:S = 0x6s

.field public static final TAG_APERTURE_VALUE:I

.field public static final TAG_ARTIST:I

.field public static final TAG_BITS_PER_SAMPLE:I

.field public static final TAG_BRIGHTNESS_VALUE:I

.field public static final TAG_CFA_PATTERN:I

.field public static final TAG_COLOR_SPACE:I

.field public static final TAG_COMPONENTS_CONFIGURATION:I

.field public static final TAG_COMPRESSED_BITS_PER_PIXEL:I

.field public static final TAG_COMPRESSION:I

.field public static final TAG_CONTRAST:I

.field public static final TAG_COPYRIGHT:I

.field public static final TAG_CUSTOM_RENDERED:I

.field public static final TAG_DATE_TIME:I

.field public static final TAG_DATE_TIME_DIGITIZED:I

.field public static final TAG_DATE_TIME_ORIGINAL:I

.field public static final TAG_DEVICE_SETTING_DESCRIPTION:I

.field public static final TAG_DIGITAL_ZOOM_RATIO:I

.field public static final TAG_EXIF_IFD:I

.field public static final TAG_EXIF_VERSION:I

.field public static final TAG_EXPOSURE_BIAS_VALUE:I

.field public static final TAG_EXPOSURE_INDEX:I

.field public static final TAG_EXPOSURE_MODE:I

.field public static final TAG_EXPOSURE_PROGRAM:I

.field public static final TAG_EXPOSURE_TIME:I

.field public static final TAG_FILE_SOURCE:I

.field public static final TAG_FLASH:I

.field public static final TAG_FLASHPIX_VERSION:I

.field public static final TAG_FLASH_ENERGY:I

.field public static final TAG_FOCAL_LENGTH:I

.field public static final TAG_FOCAL_LENGTH_IN_35_MM_FILE:I

.field public static final TAG_FOCAL_PLANE_RESOLUTION_UNIT:I

.field public static final TAG_FOCAL_PLANE_X_RESOLUTION:I

.field public static final TAG_FOCAL_PLANE_Y_RESOLUTION:I

.field public static final TAG_F_NUMBER:I

.field public static final TAG_GAIN_CONTROL:I

.field public static final TAG_GPS_ALTITUDE:I

.field public static final TAG_GPS_ALTITUDE_REF:I

.field public static final TAG_GPS_AREA_INFORMATION:I

.field public static final TAG_GPS_DATE_STAMP:I

.field public static final TAG_GPS_DEST_BEARING:I

.field public static final TAG_GPS_DEST_BEARING_REF:I

.field public static final TAG_GPS_DEST_DISTANCE:I

.field public static final TAG_GPS_DEST_DISTANCE_REF:I

.field public static final TAG_GPS_DEST_LATITUDE:I

.field public static final TAG_GPS_DEST_LATITUDE_REF:I

.field public static final TAG_GPS_DEST_LONGITUDE:I

.field public static final TAG_GPS_DEST_LONGITUDE_REF:I

.field public static final TAG_GPS_DIFFERENTIAL:I

.field public static final TAG_GPS_DOP:I

.field public static final TAG_GPS_IFD:I

.field public static final TAG_GPS_IMG_DIRECTION:I

.field public static final TAG_GPS_IMG_DIRECTION_REF:I

.field public static final TAG_GPS_LATITUDE:I

.field public static final TAG_GPS_LATITUDE_REF:I

.field public static final TAG_GPS_LONGITUDE:I

.field public static final TAG_GPS_LONGITUDE_REF:I

.field public static final TAG_GPS_MAP_DATUM:I

.field public static final TAG_GPS_MEASURE_MODE:I

.field public static final TAG_GPS_PROCESSING_METHOD:I

.field public static final TAG_GPS_SATTELLITES:I

.field public static final TAG_GPS_SPEED:I

.field public static final TAG_GPS_SPEED_REF:I

.field public static final TAG_GPS_STATUS:I

.field public static final TAG_GPS_TIME_STAMP:I

.field public static final TAG_GPS_TRACK:I

.field public static final TAG_GPS_TRACK_REF:I

.field public static final TAG_GPS_VERSION_ID:I

.field public static final TAG_IMAGE_DESCRIPTION:I

.field public static final TAG_IMAGE_LENGTH:I

.field public static final TAG_IMAGE_UNIQUE_ID:I

.field public static final TAG_IMAGE_WIDTH:I

.field public static final TAG_INTEROPERABILITY_IFD:I

.field public static final TAG_INTEROPERABILITY_INDEX:I

.field public static final TAG_ISO_SPEED_RATINGS:I

.field public static final TAG_JPEG_INTERCHANGE_FORMAT:I

.field public static final TAG_JPEG_INTERCHANGE_FORMAT_LENGTH:I

.field public static final TAG_LIGHT_SOURCE:I

.field public static final TAG_MAKE:I

.field public static final TAG_MAKER_NOTE:I

.field public static final TAG_MAX_APERTURE_VALUE:I

.field public static final TAG_METERING_MODE:I

.field public static final TAG_MODEL:I

.field public static final TAG_NULL:I = -0x1

.field public static final TAG_OECF:I

.field public static final TAG_ORIENTATION:I

.field public static final TAG_PHOTOMETRIC_INTERPRETATION:I

.field public static final TAG_PIXEL_X_DIMENSION:I

.field public static final TAG_PIXEL_Y_DIMENSION:I

.field public static final TAG_PLANAR_CONFIGURATION:I

.field public static final TAG_PRIMARY_CHROMATICITIES:I

.field public static final TAG_REFERENCE_BLACK_WHITE:I

.field public static final TAG_RELATED_SOUND_FILE:I

.field public static final TAG_RESOLUTION_UNIT:I

.field public static final TAG_ROWS_PER_STRIP:I

.field public static final TAG_SAMPLES_PER_PIXEL:I

.field public static final TAG_SATURATION:I

.field public static final TAG_SCENE_CAPTURE_TYPE:I

.field public static final TAG_SCENE_TYPE:I

.field public static final TAG_SENSING_METHOD:I

.field public static final TAG_SHARPNESS:I

.field public static final TAG_SHUTTER_SPEED_VALUE:I

.field public static final TAG_SOFTWARE:I

.field public static final TAG_SPATIAL_FREQUENCY_RESPONSE:I

.field public static final TAG_SPECTRAL_SENSITIVITY:I

.field public static final TAG_STRIP_BYTE_COUNTS:I

.field public static final TAG_STRIP_OFFSETS:I

.field public static final TAG_SUBJECT_AREA:I

.field public static final TAG_SUBJECT_DISTANCE:I

.field public static final TAG_SUBJECT_DISTANCE_RANGE:I

.field public static final TAG_SUBJECT_LOCATION:I

.field public static final TAG_SUB_SEC_TIME:I

.field public static final TAG_SUB_SEC_TIME_DIGITIZED:I

.field public static final TAG_SUB_SEC_TIME_ORIGINAL:I

.field public static final TAG_TRANSFER_FUNCTION:I

.field public static final TAG_USER_COMMENT:I

.field public static final TAG_WHITE_BALANCE:I

.field public static final TAG_WHITE_POINT:I

.field public static final TAG_X_RESOLUTION:I

.field public static final TAG_Y_CB_CR_COEFFICIENTS:I

.field public static final TAG_Y_CB_CR_POSITIONING:I

.field public static final TAG_Y_CB_CR_SUB_SAMPLING:I

.field public static final TAG_Y_RESOLUTION:I

.field private static sOffsetTags:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Short;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

.field private final mDateTimeStampFormat:Ljava/text/DateFormat;

.field private final mGPSDateStampFormat:Ljava/text/DateFormat;

.field private mTagInfo:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 61
    sput v3, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->MAX_COMPONENT_COUNT:I

    .line 68
    const/16 v0, 0x100

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 67
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_IMAGE_WIDTH:I

    .line 70
    const/16 v0, 0x101

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 69
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_IMAGE_LENGTH:I

    .line 72
    const/16 v0, 0x102

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 71
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_BITS_PER_SAMPLE:I

    .line 74
    const/16 v0, 0x103

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 73
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_COMPRESSION:I

    .line 76
    const/16 v0, 0x106

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 75
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_PHOTOMETRIC_INTERPRETATION:I

    .line 78
    const/16 v0, 0x10e

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 77
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_IMAGE_DESCRIPTION:I

    .line 80
    const/16 v0, 0x10f

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 79
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_MAKE:I

    .line 82
    const/16 v0, 0x110

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 81
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_MODEL:I

    .line 84
    const/16 v0, 0x111

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 83
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_OFFSETS:I

    .line 86
    const/16 v0, 0x112

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 85
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_ORIENTATION:I

    .line 88
    const/16 v0, 0x115

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 87
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SAMPLES_PER_PIXEL:I

    .line 90
    const/16 v0, 0x116

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 89
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_ROWS_PER_STRIP:I

    .line 92
    const/16 v0, 0x117

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 91
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_BYTE_COUNTS:I

    .line 94
    const/16 v0, 0x11a

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 93
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_X_RESOLUTION:I

    .line 96
    const/16 v0, 0x11b

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 95
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_Y_RESOLUTION:I

    .line 98
    const/16 v0, 0x11c

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 97
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_PLANAR_CONFIGURATION:I

    .line 100
    const/16 v0, 0x128

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 99
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_RESOLUTION_UNIT:I

    .line 102
    const/16 v0, 0x12d

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 101
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_TRANSFER_FUNCTION:I

    .line 104
    const/16 v0, 0x131

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 103
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SOFTWARE:I

    .line 106
    const/16 v0, 0x132

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 105
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DATE_TIME:I

    .line 108
    const/16 v0, 0x13b

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 107
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_ARTIST:I

    .line 110
    const/16 v0, 0x13e

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 109
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_WHITE_POINT:I

    .line 112
    const/16 v0, 0x13f

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 111
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_PRIMARY_CHROMATICITIES:I

    .line 114
    const/16 v0, 0x211

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 113
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_Y_CB_CR_COEFFICIENTS:I

    .line 116
    const/16 v0, 0x212

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 115
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_Y_CB_CR_SUB_SAMPLING:I

    .line 118
    const/16 v0, 0x213

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 117
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_Y_CB_CR_POSITIONING:I

    .line 120
    const/16 v0, 0x214

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 119
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_REFERENCE_BLACK_WHITE:I

    .line 122
    const/16 v0, -0x7d68

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 121
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_COPYRIGHT:I

    .line 124
    const/16 v0, -0x7897

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 123
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXIF_IFD:I

    .line 126
    const/16 v0, -0x77db

    invoke-static {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 125
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_IFD:I

    .line 129
    const/16 v0, 0x201

    invoke-static {v1, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 128
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT:I

    .line 131
    const/16 v0, 0x202

    invoke-static {v1, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 130
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT_LENGTH:I

    .line 134
    const/16 v0, -0x7d66

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 133
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXPOSURE_TIME:I

    .line 136
    const/16 v0, -0x7d63

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 135
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_F_NUMBER:I

    .line 138
    const/16 v0, -0x77de

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 137
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXPOSURE_PROGRAM:I

    .line 140
    const/16 v0, -0x77dc

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 139
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SPECTRAL_SENSITIVITY:I

    .line 142
    const/16 v0, -0x77d9

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 141
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_ISO_SPEED_RATINGS:I

    .line 144
    const/16 v0, -0x77d8

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 143
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_OECF:I

    .line 146
    const/16 v0, -0x7000

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 145
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXIF_VERSION:I

    .line 148
    const/16 v0, -0x6ffd

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 147
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DATE_TIME_ORIGINAL:I

    .line 150
    const/16 v0, -0x6ffc

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 149
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DATE_TIME_DIGITIZED:I

    .line 152
    const/16 v0, -0x6eff

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 151
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_COMPONENTS_CONFIGURATION:I

    .line 154
    const/16 v0, -0x6efe

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 153
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_COMPRESSED_BITS_PER_PIXEL:I

    .line 156
    const/16 v0, -0x6dff

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 155
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SHUTTER_SPEED_VALUE:I

    .line 158
    const/16 v0, -0x6dfe

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 157
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_APERTURE_VALUE:I

    .line 160
    const/16 v0, -0x6dfd

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 159
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_BRIGHTNESS_VALUE:I

    .line 162
    const/16 v0, -0x6dfc

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 161
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXPOSURE_BIAS_VALUE:I

    .line 164
    const/16 v0, -0x6dfb

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 163
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_MAX_APERTURE_VALUE:I

    .line 166
    const/16 v0, -0x6dfa

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 165
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUBJECT_DISTANCE:I

    .line 168
    const/16 v0, -0x6df9

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 167
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_METERING_MODE:I

    .line 170
    const/16 v0, -0x6df8

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 169
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_LIGHT_SOURCE:I

    .line 172
    const/16 v0, -0x6df7

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 171
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FLASH:I

    .line 174
    const/16 v0, -0x6df6

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 173
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FOCAL_LENGTH:I

    .line 176
    const/16 v0, -0x6dec

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 175
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUBJECT_AREA:I

    .line 178
    const/16 v0, -0x6d84

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 177
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_MAKER_NOTE:I

    .line 180
    const/16 v0, -0x6d7a

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 179
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_USER_COMMENT:I

    .line 182
    const/16 v0, -0x6d70

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 181
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUB_SEC_TIME:I

    .line 184
    const/16 v0, -0x6d6f

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 183
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUB_SEC_TIME_ORIGINAL:I

    .line 186
    const/16 v0, -0x6d6e

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 185
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUB_SEC_TIME_DIGITIZED:I

    .line 188
    const/16 v0, -0x6000

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 187
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FLASHPIX_VERSION:I

    .line 190
    const/16 v0, -0x5fff

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 189
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_COLOR_SPACE:I

    .line 192
    const/16 v0, -0x5ffe

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 191
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_PIXEL_X_DIMENSION:I

    .line 194
    const/16 v0, -0x5ffd

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 193
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_PIXEL_Y_DIMENSION:I

    .line 196
    const/16 v0, -0x5ffc

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 195
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_RELATED_SOUND_FILE:I

    .line 198
    const/16 v0, -0x5ffb

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 197
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_INTEROPERABILITY_IFD:I

    .line 200
    const/16 v0, -0x5df5

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 199
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FLASH_ENERGY:I

    .line 202
    const/16 v0, -0x5df4

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 201
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SPATIAL_FREQUENCY_RESPONSE:I

    .line 204
    const/16 v0, -0x5df2

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 203
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FOCAL_PLANE_X_RESOLUTION:I

    .line 206
    const/16 v0, -0x5df1

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 205
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FOCAL_PLANE_Y_RESOLUTION:I

    .line 208
    const/16 v0, -0x5df0

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 207
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FOCAL_PLANE_RESOLUTION_UNIT:I

    .line 210
    const/16 v0, -0x5dec

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 209
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUBJECT_LOCATION:I

    .line 212
    const/16 v0, -0x5deb

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 211
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXPOSURE_INDEX:I

    .line 214
    const/16 v0, -0x5de9

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 213
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SENSING_METHOD:I

    .line 216
    const/16 v0, -0x5d00

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 215
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FILE_SOURCE:I

    .line 218
    const/16 v0, -0x5cff

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 217
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SCENE_TYPE:I

    .line 220
    const/16 v0, -0x5cfe

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 219
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_CFA_PATTERN:I

    .line 222
    const/16 v0, -0x5bff

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 221
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_CUSTOM_RENDERED:I

    .line 224
    const/16 v0, -0x5bfe

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 223
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXPOSURE_MODE:I

    .line 226
    const/16 v0, -0x5bfd

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 225
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_WHITE_BALANCE:I

    .line 228
    const/16 v0, -0x5bfc

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 227
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DIGITAL_ZOOM_RATIO:I

    .line 230
    const/16 v0, -0x5bfb

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 229
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FOCAL_LENGTH_IN_35_MM_FILE:I

    .line 232
    const/16 v0, -0x5bfa

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 231
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SCENE_CAPTURE_TYPE:I

    .line 234
    const/16 v0, -0x5bf9

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 233
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GAIN_CONTROL:I

    .line 236
    const/16 v0, -0x5bf8

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 235
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_CONTRAST:I

    .line 238
    const/16 v0, -0x5bf7

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 237
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SATURATION:I

    .line 240
    const/16 v0, -0x5bf6

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 239
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SHARPNESS:I

    .line 242
    const/16 v0, -0x5bf5

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 241
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DEVICE_SETTING_DESCRIPTION:I

    .line 244
    const/16 v0, -0x5bf4

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 243
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUBJECT_DISTANCE_RANGE:I

    .line 246
    const/16 v0, -0x5be0

    invoke-static {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 245
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_IMAGE_UNIQUE_ID:I

    .line 249
    invoke-static {v4, v3, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 248
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_VERSION_ID:I

    .line 251
    invoke-static {v4, v1, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 250
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LATITUDE_REF:I

    .line 253
    invoke-static {v4, v2, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 252
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LATITUDE:I

    .line 255
    invoke-static {v4, v5, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 254
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LONGITUDE_REF:I

    .line 257
    invoke-static {v4, v4, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 256
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LONGITUDE:I

    .line 259
    const/4 v0, 0x5

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 258
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_ALTITUDE_REF:I

    .line 261
    const/4 v0, 0x6

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 260
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_ALTITUDE:I

    .line 263
    const/4 v0, 0x7

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 262
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_TIME_STAMP:I

    .line 265
    const/16 v0, 0x8

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 264
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_SATTELLITES:I

    .line 267
    const/16 v0, 0x9

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 266
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_STATUS:I

    .line 269
    const/16 v0, 0xa

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 268
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_MEASURE_MODE:I

    .line 271
    const/16 v0, 0xb

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 270
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DOP:I

    .line 273
    const/16 v0, 0xc

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 272
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_SPEED_REF:I

    .line 275
    const/16 v0, 0xd

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 274
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_SPEED:I

    .line 277
    const/16 v0, 0xe

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 276
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_TRACK_REF:I

    .line 279
    const/16 v0, 0xf

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 278
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_TRACK:I

    .line 281
    const/16 v0, 0x10

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 280
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_IMG_DIRECTION_REF:I

    .line 283
    const/16 v0, 0x11

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 282
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_IMG_DIRECTION:I

    .line 285
    const/16 v0, 0x12

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 284
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_MAP_DATUM:I

    .line 287
    const/16 v0, 0x13

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 286
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_LATITUDE_REF:I

    .line 289
    const/16 v0, 0x14

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 288
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_LATITUDE:I

    .line 291
    const/16 v0, 0x15

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 290
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_LONGITUDE_REF:I

    .line 293
    const/16 v0, 0x16

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 292
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_LONGITUDE:I

    .line 295
    const/16 v0, 0x17

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 294
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_BEARING_REF:I

    .line 297
    const/16 v0, 0x18

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 296
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_BEARING:I

    .line 299
    const/16 v0, 0x19

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 298
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_DISTANCE_REF:I

    .line 301
    const/16 v0, 0x1a

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 300
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_DISTANCE:I

    .line 303
    const/16 v0, 0x1b

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 302
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_PROCESSING_METHOD:I

    .line 305
    const/16 v0, 0x1c

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 304
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_AREA_INFORMATION:I

    .line 307
    const/16 v0, 0x1d

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 306
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DATE_STAMP:I

    .line 309
    const/16 v0, 0x1e

    invoke-static {v4, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 308
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DIFFERENTIAL:I

    .line 312
    invoke-static {v5, v1, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    .line 311
    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_INTEROPERABILITY_INDEX:I

    .line 324
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->sOffsetTags:Ljava/util/HashSet;

    .line 326
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->sOffsetTags:Ljava/util/HashSet;

    sget v1, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_IFD:I

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v1

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 327
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->sOffsetTags:Ljava/util/HashSet;

    sget v1, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXIF_IFD:I

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v1

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 328
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->sOffsetTags:Ljava/util/HashSet;

    sget v1, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT:I

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v1

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 329
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->sOffsetTags:Ljava/util/HashSet;

    sget v1, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_INTEROPERABILITY_IFD:I

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v1

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 330
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->sOffsetTags:Ljava/util/HashSet;

    sget v1, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_OFFSETS:I

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v1

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 721
    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    sput-object v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->DEFAULT_BYTE_ORDER:Ljava/nio/ByteOrder;

    .line 2069
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 720
    new-instance v0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    sget-object v1, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->DEFAULT_BYTE_ORDER:Ljava/nio/ByteOrder;

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;-><init>(Ljava/nio/ByteOrder;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    .line 2070
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy:MM:dd kk:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mDateTimeStampFormat:Ljava/text/DateFormat;

    .line 2071
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy:MM:dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mGPSDateStampFormat:Ljava/text/DateFormat;

    .line 2183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    .line 725
    return-void
.end method

.method protected static closeSilently(Ljava/io/Closeable;)V
    .locals 1
    .param p0, "c"    # Ljava/io/Closeable;

    .prologue
    .line 2174
    if-eqz p0, :cond_0

    .line 2176
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2181
    :cond_0
    :goto_0
    return-void

    .line 2177
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static convertLatOrLongToDouble([Lcom/sec/android/mimage/photoretouching/exif/Rational;Ljava/lang/String;)D
    .locals 14
    .param p0, "coordinate"    # [Lcom/sec/android/mimage/photoretouching/exif/Rational;
    .param p1, "reference"    # Ljava/lang/String;

    .prologue
    .line 2032
    const/4 v3, 0x0

    :try_start_0
    aget-object v3, p0, v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/Rational;->toDouble()D

    move-result-wide v0

    .line 2033
    .local v0, "degrees":D
    const/4 v3, 0x1

    aget-object v3, p0, v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/Rational;->toDouble()D

    move-result-wide v4

    .line 2034
    .local v4, "minutes":D
    const/4 v3, 0x2

    aget-object v3, p0, v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/exif/Rational;->toDouble()D

    move-result-wide v8

    .line 2035
    .local v8, "seconds":D
    const-wide/high16 v10, 0x404e000000000000L    # 60.0

    div-double v10, v4, v10

    add-double/2addr v10, v0

    const-wide v12, 0x40ac200000000000L    # 3600.0

    div-double v12, v8, v12

    add-double v6, v10, v12

    .line 2036
    .local v6, "result":D
    const-string v3, "S"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "W"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_1

    .line 2037
    :cond_0
    neg-double v6, v6

    .line 2039
    .end local v6    # "result":D
    :cond_1
    return-wide v6

    .line 2040
    .end local v0    # "degrees":D
    .end local v4    # "minutes":D
    .end local v8    # "seconds":D
    :catch_0
    move-exception v2

    .line 2041
    .local v2, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3
.end method

.method public static defineTag(IS)I
    .locals 1
    .param p0, "ifdId"    # I
    .param p1, "tagId"    # S

    .prologue
    .line 347
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(ISZ)I

    move-result v0

    return v0
.end method

.method public static defineTag(ISZ)I
    .locals 2
    .param p0, "ifdId"    # I
    .param p1, "tagId"    # S
    .param p2, "init"    # Z

    .prologue
    .line 350
    if-eqz p2, :cond_0

    .line 351
    sget v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->MAX_COMPONENT_COUNT:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->MAX_COMPONENT_COUNT:I

    .line 353
    :cond_0
    const v0, 0xffff

    and-int/2addr v0, p1

    shl-int/lit8 v1, p0, 0x10

    or-int/2addr v0, v1

    return v0
.end method

.method private doExifStreamIO(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 5
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x400

    const/4 v3, 0x0

    .line 2165
    new-array v0, v4, [B

    .line 2166
    .local v0, "buf":[B
    invoke-virtual {p1, v0, v3, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 2167
    .local v1, "ret":I
    :goto_0
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 2171
    return-void

    .line 2168
    :cond_0
    invoke-virtual {p2, v0, v3, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 2169
    invoke-virtual {p1, v0, v3, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    goto :goto_0
.end method

.method protected static getAllowedIfdFlagsFromInfo(I)I
    .locals 1
    .param p0, "info"    # I

    .prologue
    .line 2468
    ushr-int/lit8 v0, p0, 0x18

    return v0
.end method

.method protected static getAllowedIfdsFromInfo(I)[I
    .locals 10
    .param p0, "info"    # I

    .prologue
    .line 2472
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getAllowedIfdFlagsFromInfo(I)I

    move-result v2

    .line 2473
    .local v2, "ifdFlags":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getIfds()[I

    move-result-object v3

    .line 2474
    .local v3, "ifds":[I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2475
    .local v6, "l":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v8, 0x5

    if-lt v1, v8, :cond_1

    .line 2481
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-gtz v8, :cond_3

    .line 2482
    const/4 v7, 0x0

    .line 2489
    :cond_0
    return-object v7

    .line 2476
    :cond_1
    shr-int v8, v2, v1

    and-int/lit8 v0, v8, 0x1

    .line 2477
    .local v0, "flag":I
    const/4 v8, 0x1

    if-ne v0, v8, :cond_2

    .line 2478
    aget v8, v3, v1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2475
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2484
    .end local v0    # "flag":I
    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v7, v8, [I

    .line 2485
    .local v7, "ret":[I
    const/4 v4, 0x0

    .line 2486
    .local v4, "j":I
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2487
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "j":I
    .local v5, "j":I
    aput v1, v7, v4

    move v4, v5

    .end local v5    # "j":I
    .restart local v4    # "j":I
    goto :goto_1
.end method

.method protected static getComponentCountFromInfo(I)I
    .locals 1
    .param p0, "info"    # I

    .prologue
    .line 2525
    const v0, 0xffff

    and-int/2addr v0, p0

    return v0
.end method

.method protected static getFlagsFromAllowedIfds([I)I
    .locals 8
    .param p0, "allowedIfds"    # [I

    .prologue
    const/4 v4, 0x0

    .line 2504
    if-eqz p0, :cond_0

    array-length v5, p0

    if-nez v5, :cond_2

    :cond_0
    move v0, v4

    .line 2517
    :cond_1
    return v0

    .line 2507
    :cond_2
    const/4 v0, 0x0

    .line 2508
    .local v0, "flags":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getIfds()[I

    move-result-object v2

    .line 2509
    .local v2, "ifds":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v5, 0x5

    if-ge v1, v5, :cond_1

    .line 2510
    array-length v6, p0

    move v5, v4

    :goto_1
    if-lt v5, v6, :cond_3

    .line 2509
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2510
    :cond_3
    aget v3, p0, v5

    .line 2511
    .local v3, "j":I
    aget v7, v2, v1

    if-ne v7, v3, :cond_4

    .line 2512
    const/4 v5, 0x1

    shl-int/2addr v5, v1

    or-int/2addr v0, v5

    .line 2513
    goto :goto_2

    .line 2510
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method private getLocation(Landroid/content/Context;[DI)Ljava/lang/String;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "locationInfo"    # [D
    .param p3, "type"    # I

    .prologue
    const-wide/16 v10, 0x0

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 839
    const-string v2, "JW getLocation"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 840
    const/4 v9, 0x0

    .line 842
    .local v9, "location":Ljava/lang/String;
    aget-wide v2, p2, v4

    cmpl-double v2, v2, v10

    if-eqz v2, :cond_0

    aget-wide v2, p2, v6

    cmpl-double v2, v2, v10

    if-eqz v2, :cond_0

    .line 844
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "JW latitudeInfo="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-wide v4, p2, v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 845
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "JW longitudeInfo="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-wide v4, p2, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 846
    new-instance v1, Landroid/location/Geocoder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 849
    .local v1, "gc":Landroid/location/Geocoder;
    const/4 v2, 0x0

    :try_start_0
    aget-wide v2, p2, v2

    const/4 v4, 0x1

    aget-wide v4, p2, v4

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v7

    .line 850
    .local v7, "address":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 852
    const/4 v2, 0x0

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 853
    .local v0, "ad":Landroid/location/Address;
    const/4 v2, 0x2

    if-ne p3, v2, :cond_1

    .line 854
    invoke-virtual {v0}, Landroid/location/Address;->getLocality()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 864
    .end local v0    # "ad":Landroid/location/Address;
    .end local v1    # "gc":Landroid/location/Geocoder;
    .end local v7    # "address":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :cond_0
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "JW location="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 865
    return-object v9

    .line 856
    .restart local v0    # "ad":Landroid/location/Address;
    .restart local v1    # "gc":Landroid/location/Geocoder;
    .restart local v7    # "address":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v9

    goto :goto_0

    .line 859
    .end local v0    # "ad":Landroid/location/Address;
    .end local v7    # "address":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    :catch_0
    move-exception v8

    .line 861
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getOrientationValueForRotation(I)S
    .locals 1
    .param p0, "degrees"    # I

    .prologue
    .line 1982
    rem-int/lit16 p0, p0, 0x168

    .line 1983
    if-gez p0, :cond_0

    .line 1984
    add-int/lit16 p0, p0, 0x168

    .line 1986
    :cond_0
    const/16 v0, 0x5a

    if-ge p0, v0, :cond_1

    .line 1987
    const/4 v0, 0x1

    .line 1993
    :goto_0
    return v0

    .line 1988
    :cond_1
    const/16 v0, 0xb4

    if-ge p0, v0, :cond_2

    .line 1989
    const/4 v0, 0x6

    goto :goto_0

    .line 1990
    :cond_2
    const/16 v0, 0x10e

    if-ge p0, v0, :cond_3

    .line 1991
    const/4 v0, 0x3

    goto :goto_0

    .line 1993
    :cond_3
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static getRotationForOrientationValue(S)I
    .locals 1
    .param p0, "orientation"    # S

    .prologue
    const/4 v0, 0x0

    .line 2004
    packed-switch p0, :pswitch_data_0

    .line 2014
    :goto_0
    :pswitch_0
    return v0

    .line 2008
    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    .line 2010
    :pswitch_2
    const/16 v0, 0xb4

    goto :goto_0

    .line 2012
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 2004
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getTrueIfd(I)I
    .locals 1
    .param p0, "tag"    # I

    .prologue
    .line 368
    ushr-int/lit8 v0, p0, 0x10

    return v0
.end method

.method public static getTrueTagKey(I)S
    .locals 1
    .param p0, "tag"    # I

    .prologue
    .line 361
    int-to-short v0, p0

    return v0
.end method

.method protected static getTypeFromInfo(I)S
    .locals 1
    .param p0, "info"    # I

    .prologue
    .line 2521
    shr-int/lit8 v0, p0, 0x10

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    return v0
.end method

.method private initTagInfo()V
    .locals 13

    .prologue
    .line 2201
    const/4 v10, 0x2

    new-array v5, v10, [I

    const/4 v10, 0x1

    .line 2202
    const/4 v11, 0x1

    aput v11, v5, v10

    .line 2204
    .local v5, "ifdAllowedIfds":[I
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getFlagsFromAllowedIfds([I)I

    move-result v10

    shl-int/lit8 v6, v10, 0x18

    .line 2205
    .local v6, "ifdFlags":I
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_MAKE:I

    .line 2206
    const/high16 v12, 0x20000

    or-int/2addr v12, v6

    .line 2205
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2207
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_IMAGE_WIDTH:I

    .line 2208
    const/high16 v12, 0x40000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x1

    .line 2207
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2209
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_IMAGE_LENGTH:I

    .line 2210
    const/high16 v12, 0x40000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x1

    .line 2209
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2211
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_BITS_PER_SAMPLE:I

    .line 2212
    const/high16 v12, 0x30000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x3

    .line 2211
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2213
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_COMPRESSION:I

    .line 2214
    const/high16 v12, 0x30000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x1

    .line 2213
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2215
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_PHOTOMETRIC_INTERPRETATION:I

    .line 2216
    const/high16 v12, 0x30000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x1

    .line 2215
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2217
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_ORIENTATION:I

    const/high16 v12, 0x30000

    or-int/2addr v12, v6

    .line 2218
    or-int/lit8 v12, v12, 0x1

    .line 2217
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2219
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SAMPLES_PER_PIXEL:I

    .line 2220
    const/high16 v12, 0x30000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x1

    .line 2219
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2221
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_PLANAR_CONFIGURATION:I

    .line 2222
    const/high16 v12, 0x30000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x1

    .line 2221
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2223
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_Y_CB_CR_SUB_SAMPLING:I

    .line 2224
    const/high16 v12, 0x30000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x2

    .line 2223
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2225
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_Y_CB_CR_POSITIONING:I

    .line 2226
    const/high16 v12, 0x30000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x1

    .line 2225
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2227
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_X_RESOLUTION:I

    .line 2228
    const/high16 v12, 0x50000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x1

    .line 2227
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2229
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_Y_RESOLUTION:I

    .line 2230
    const/high16 v12, 0x50000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x1

    .line 2229
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2231
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_RESOLUTION_UNIT:I

    .line 2232
    const/high16 v12, 0x30000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x1

    .line 2231
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2233
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_OFFSETS:I

    .line 2234
    const/high16 v12, 0x40000

    or-int/2addr v12, v6

    .line 2233
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2235
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_ROWS_PER_STRIP:I

    .line 2236
    const/high16 v12, 0x40000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x1

    .line 2235
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2237
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_STRIP_BYTE_COUNTS:I

    .line 2238
    const/high16 v12, 0x40000

    or-int/2addr v12, v6

    .line 2237
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2239
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_TRANSFER_FUNCTION:I

    .line 2240
    const/high16 v12, 0x30000

    or-int/2addr v12, v6

    or-int/lit16 v12, v12, 0x300

    .line 2239
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2241
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_WHITE_POINT:I

    .line 2242
    const/high16 v12, 0x50000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x2

    .line 2241
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2243
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_PRIMARY_CHROMATICITIES:I

    .line 2244
    const/high16 v12, 0x50000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x6

    .line 2243
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2245
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_Y_CB_CR_COEFFICIENTS:I

    .line 2246
    const/high16 v12, 0x50000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x3

    .line 2245
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2247
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_REFERENCE_BLACK_WHITE:I

    .line 2248
    const/high16 v12, 0x50000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x6

    .line 2247
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2249
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DATE_TIME:I

    .line 2250
    const/high16 v12, 0x20000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x14

    .line 2249
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2251
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_IMAGE_DESCRIPTION:I

    .line 2252
    const/high16 v12, 0x20000

    or-int/2addr v12, v6

    .line 2251
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2253
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_MAKE:I

    .line 2254
    const/high16 v12, 0x20000

    or-int/2addr v12, v6

    .line 2253
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2255
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_MODEL:I

    .line 2256
    const/high16 v12, 0x20000

    or-int/2addr v12, v6

    .line 2255
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2257
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SOFTWARE:I

    .line 2258
    const/high16 v12, 0x20000

    or-int/2addr v12, v6

    .line 2257
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2259
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_ARTIST:I

    .line 2260
    const/high16 v12, 0x20000

    or-int/2addr v12, v6

    .line 2259
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2261
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_COPYRIGHT:I

    .line 2262
    const/high16 v12, 0x20000

    or-int/2addr v12, v6

    .line 2261
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2263
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXIF_IFD:I

    .line 2264
    const/high16 v12, 0x40000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x1

    .line 2263
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2265
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_IFD:I

    .line 2266
    const/high16 v12, 0x40000

    or-int/2addr v12, v6

    or-int/lit8 v12, v12, 0x1

    .line 2265
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2268
    const/4 v10, 0x1

    new-array v4, v10, [I

    const/4 v10, 0x0

    .line 2269
    const/4 v11, 0x1

    aput v11, v4, v10

    .line 2271
    .local v4, "ifd1AllowedIfds":[I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getFlagsFromAllowedIfds([I)I

    move-result v10

    shl-int/lit8 v7, v10, 0x18

    .line 2272
    .local v7, "ifdFlags1":I
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT:I

    .line 2273
    const/high16 v12, 0x40000

    or-int/2addr v12, v7

    or-int/lit8 v12, v12, 0x1

    .line 2272
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2274
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_JPEG_INTERCHANGE_FORMAT_LENGTH:I

    .line 2275
    const/high16 v12, 0x40000

    or-int/2addr v12, v7

    or-int/lit8 v12, v12, 0x1

    .line 2274
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2277
    const/4 v10, 0x1

    new-array v0, v10, [I

    const/4 v10, 0x0

    .line 2278
    const/4 v11, 0x2

    aput v11, v0, v10

    .line 2280
    .local v0, "exifAllowedIfds":[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getFlagsFromAllowedIfds([I)I

    move-result v10

    shl-int/lit8 v1, v10, 0x18

    .line 2281
    .local v1, "exifFlags":I
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXIF_VERSION:I

    .line 2282
    const/high16 v12, 0x70000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x4

    .line 2281
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2283
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FLASHPIX_VERSION:I

    .line 2284
    const/high16 v12, 0x70000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x4

    .line 2283
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2285
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_COLOR_SPACE:I

    .line 2286
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2285
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2287
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_COMPONENTS_CONFIGURATION:I

    .line 2288
    const/high16 v12, 0x70000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x4

    .line 2287
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2289
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_COMPRESSED_BITS_PER_PIXEL:I

    .line 2290
    const/high16 v12, 0x50000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2289
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2291
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_PIXEL_X_DIMENSION:I

    .line 2292
    const/high16 v12, 0x40000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2291
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2293
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_PIXEL_Y_DIMENSION:I

    .line 2294
    const/high16 v12, 0x40000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2293
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2295
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_MAKER_NOTE:I

    .line 2296
    const/high16 v12, 0x70000

    or-int/2addr v12, v1

    .line 2295
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2297
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_USER_COMMENT:I

    .line 2298
    const/high16 v12, 0x70000

    or-int/2addr v12, v1

    .line 2297
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2299
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_RELATED_SOUND_FILE:I

    .line 2300
    const/high16 v12, 0x20000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0xd

    .line 2299
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2301
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DATE_TIME_ORIGINAL:I

    .line 2302
    const/high16 v12, 0x20000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x14

    .line 2301
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2303
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DATE_TIME_DIGITIZED:I

    .line 2304
    const/high16 v12, 0x20000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x14

    .line 2303
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2305
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUB_SEC_TIME:I

    .line 2306
    const/high16 v12, 0x20000

    or-int/2addr v12, v1

    .line 2305
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2307
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUB_SEC_TIME_ORIGINAL:I

    .line 2308
    const/high16 v12, 0x20000

    or-int/2addr v12, v1

    .line 2307
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2309
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUB_SEC_TIME_DIGITIZED:I

    .line 2310
    const/high16 v12, 0x20000

    or-int/2addr v12, v1

    .line 2309
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2311
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_IMAGE_UNIQUE_ID:I

    .line 2312
    const/high16 v12, 0x20000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x21

    .line 2311
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2313
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXPOSURE_TIME:I

    .line 2314
    const/high16 v12, 0x50000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2313
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2315
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_F_NUMBER:I

    .line 2316
    const/high16 v12, 0x50000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2315
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2317
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXPOSURE_PROGRAM:I

    .line 2318
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2317
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2319
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SPECTRAL_SENSITIVITY:I

    .line 2320
    const/high16 v12, 0x20000

    or-int/2addr v12, v1

    .line 2319
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2321
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_ISO_SPEED_RATINGS:I

    .line 2322
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    .line 2321
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2323
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_OECF:I

    .line 2324
    const/high16 v12, 0x70000

    or-int/2addr v12, v1

    .line 2323
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2325
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SHUTTER_SPEED_VALUE:I

    .line 2326
    const/high16 v12, 0xa0000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2325
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2327
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_APERTURE_VALUE:I

    .line 2328
    const/high16 v12, 0x50000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2327
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2329
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_BRIGHTNESS_VALUE:I

    .line 2330
    const/high16 v12, 0xa0000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2329
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2331
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXPOSURE_BIAS_VALUE:I

    .line 2332
    const/high16 v12, 0xa0000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2331
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2333
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_MAX_APERTURE_VALUE:I

    .line 2334
    const/high16 v12, 0x50000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2333
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2335
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUBJECT_DISTANCE:I

    .line 2336
    const/high16 v12, 0x50000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2335
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2337
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_METERING_MODE:I

    .line 2338
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2337
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2339
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_LIGHT_SOURCE:I

    .line 2340
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2339
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2341
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FLASH:I

    .line 2342
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2341
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2343
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FOCAL_LENGTH:I

    .line 2344
    const/high16 v12, 0x50000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2343
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2345
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUBJECT_AREA:I

    .line 2346
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    .line 2345
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2347
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FLASH_ENERGY:I

    .line 2348
    const/high16 v12, 0x50000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2347
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2349
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SPATIAL_FREQUENCY_RESPONSE:I

    .line 2350
    const/high16 v12, 0x70000

    or-int/2addr v12, v1

    .line 2349
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2351
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FOCAL_PLANE_X_RESOLUTION:I

    .line 2352
    const/high16 v12, 0x50000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2351
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2353
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FOCAL_PLANE_Y_RESOLUTION:I

    .line 2354
    const/high16 v12, 0x50000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2353
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2355
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FOCAL_PLANE_RESOLUTION_UNIT:I

    .line 2356
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2355
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2357
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUBJECT_LOCATION:I

    .line 2358
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x2

    .line 2357
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2359
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXPOSURE_INDEX:I

    .line 2360
    const/high16 v12, 0x50000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2359
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2361
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SENSING_METHOD:I

    .line 2362
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2361
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2363
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FILE_SOURCE:I

    .line 2364
    const/high16 v12, 0x70000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2363
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2365
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SCENE_TYPE:I

    .line 2366
    const/high16 v12, 0x70000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2365
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2367
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_CFA_PATTERN:I

    .line 2368
    const/high16 v12, 0x70000

    or-int/2addr v12, v1

    .line 2367
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2369
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_CUSTOM_RENDERED:I

    .line 2370
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2369
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2371
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXPOSURE_MODE:I

    .line 2372
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2371
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2373
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_WHITE_BALANCE:I

    .line 2374
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2373
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2375
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DIGITAL_ZOOM_RATIO:I

    .line 2376
    const/high16 v12, 0x50000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2375
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2377
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FOCAL_LENGTH_IN_35_MM_FILE:I

    .line 2378
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2377
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2379
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SCENE_CAPTURE_TYPE:I

    .line 2380
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2379
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2381
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GAIN_CONTROL:I

    .line 2382
    const/high16 v12, 0x50000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2381
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2383
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_CONTRAST:I

    .line 2384
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2383
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2385
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SATURATION:I

    .line 2386
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2385
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2387
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SHARPNESS:I

    .line 2388
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2387
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2389
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DEVICE_SETTING_DESCRIPTION:I

    .line 2390
    const/high16 v12, 0x70000

    or-int/2addr v12, v1

    .line 2389
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2391
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_SUBJECT_DISTANCE_RANGE:I

    .line 2392
    const/high16 v12, 0x30000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2391
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2393
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_INTEROPERABILITY_IFD:I

    .line 2394
    const/high16 v12, 0x40000

    or-int/2addr v12, v1

    or-int/lit8 v12, v12, 0x1

    .line 2393
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2396
    const/4 v10, 0x1

    new-array v2, v10, [I

    const/4 v10, 0x0

    .line 2397
    const/4 v11, 0x4

    aput v11, v2, v10

    .line 2399
    .local v2, "gpsAllowedIfds":[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getFlagsFromAllowedIfds([I)I

    move-result v10

    shl-int/lit8 v3, v10, 0x18

    .line 2400
    .local v3, "gpsFlags":I
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_VERSION_ID:I

    .line 2401
    const/high16 v12, 0x10000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x4

    .line 2400
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2402
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LATITUDE_REF:I

    .line 2403
    const/high16 v12, 0x20000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x2

    .line 2402
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2404
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LONGITUDE_REF:I

    .line 2405
    const/high16 v12, 0x20000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x2

    .line 2404
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2406
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LATITUDE:I

    .line 2407
    const/high16 v12, 0xa0000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x3

    .line 2406
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2408
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LONGITUDE:I

    .line 2409
    const/high16 v12, 0xa0000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x3

    .line 2408
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2410
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_ALTITUDE_REF:I

    .line 2411
    const/high16 v12, 0x10000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x1

    .line 2410
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2412
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_ALTITUDE:I

    .line 2413
    const/high16 v12, 0x50000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x1

    .line 2412
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2414
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_TIME_STAMP:I

    .line 2415
    const/high16 v12, 0x50000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x3

    .line 2414
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2416
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_SATTELLITES:I

    .line 2417
    const/high16 v12, 0x20000

    or-int/2addr v12, v3

    .line 2416
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2418
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_STATUS:I

    .line 2419
    const/high16 v12, 0x20000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x2

    .line 2418
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2420
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_MEASURE_MODE:I

    .line 2421
    const/high16 v12, 0x20000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x2

    .line 2420
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2422
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DOP:I

    .line 2423
    const/high16 v12, 0x50000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x1

    .line 2422
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2424
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_SPEED_REF:I

    .line 2425
    const/high16 v12, 0x20000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x2

    .line 2424
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2426
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_SPEED:I

    .line 2427
    const/high16 v12, 0x50000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x1

    .line 2426
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2428
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_TRACK_REF:I

    .line 2429
    const/high16 v12, 0x20000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x2

    .line 2428
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2430
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_TRACK:I

    .line 2431
    const/high16 v12, 0x50000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x1

    .line 2430
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2432
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_IMG_DIRECTION_REF:I

    .line 2433
    const/high16 v12, 0x20000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x2

    .line 2432
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2434
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_IMG_DIRECTION:I

    .line 2435
    const/high16 v12, 0x50000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x1

    .line 2434
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2436
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_MAP_DATUM:I

    .line 2437
    const/high16 v12, 0x20000

    or-int/2addr v12, v3

    .line 2436
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2438
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_LATITUDE_REF:I

    .line 2439
    const/high16 v12, 0x20000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x2

    .line 2438
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2440
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_LATITUDE:I

    .line 2441
    const/high16 v12, 0x50000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x1

    .line 2440
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2442
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_BEARING_REF:I

    .line 2443
    const/high16 v12, 0x20000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x2

    .line 2442
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2444
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_BEARING:I

    .line 2445
    const/high16 v12, 0x50000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x1

    .line 2444
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2446
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_DISTANCE_REF:I

    .line 2447
    const/high16 v12, 0x20000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x2

    .line 2446
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2448
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DEST_DISTANCE:I

    .line 2449
    const/high16 v12, 0x50000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0x1

    .line 2448
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2450
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_PROCESSING_METHOD:I

    .line 2451
    const/high16 v12, 0x70000

    or-int/2addr v12, v3

    .line 2450
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2452
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_AREA_INFORMATION:I

    .line 2453
    const/high16 v12, 0x70000

    or-int/2addr v12, v3

    .line 2452
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2454
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DATE_STAMP:I

    .line 2455
    const/high16 v12, 0x20000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0xb

    .line 2454
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2456
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DIFFERENTIAL:I

    .line 2457
    const/high16 v12, 0x30000

    or-int/2addr v12, v3

    or-int/lit8 v12, v12, 0xb

    .line 2456
    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2459
    const/4 v10, 0x1

    new-array v8, v10, [I

    const/4 v10, 0x0

    .line 2460
    const/4 v11, 0x3

    aput v11, v8, v10

    .line 2462
    .local v8, "interopAllowedIfds":[I
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getFlagsFromAllowedIfds([I)I

    move-result v10

    shl-int/lit8 v9, v10, 0x18

    .line 2463
    .local v9, "interopFlags":I
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_INTEROPERABILITY_INDEX:I

    const/high16 v12, 0x20000

    or-int/2addr v12, v9

    invoke-virtual {v10, v11, v12}, Landroid/util/SparseIntArray;->put(II)V

    .line 2465
    return-void
.end method

.method protected static isIfdAllowed(II)Z
    .locals 5
    .param p0, "info"    # I
    .param p1, "ifd"    # I

    .prologue
    const/4 v3, 0x1

    .line 2493
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getIfds()[I

    move-result-object v2

    .line 2494
    .local v2, "ifds":[I
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getAllowedIfdFlagsFromInfo(I)I

    move-result v1

    .line 2495
    .local v1, "ifdFlags":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-lt v0, v4, :cond_1

    .line 2500
    const/4 v3, 0x0

    :cond_0
    return v3

    .line 2496
    :cond_1
    aget v4, v2, v0

    if-ne p1, v4, :cond_2

    shr-int v4, v1, v0

    and-int/lit8 v4, v4, 0x1

    if-eq v4, v3, :cond_0

    .line 2495
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected static isOffsetTag(S)Z
    .locals 2
    .param p0, "tag"    # S

    .prologue
    .line 1604
    sget-object v0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->sOffsetTags:Ljava/util/HashSet;

    invoke-static {p0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static toExifLatLong(D)[Lcom/sec/android/mimage/photoretouching/exif/Rational;
    .locals 10
    .param p0, "value"    # D

    .prologue
    const-wide/16 v8, 0x1

    .line 2153
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(D)D

    move-result-wide p0

    .line 2154
    double-to-int v0, p0

    .line 2155
    .local v0, "degrees":I
    int-to-double v4, v0

    sub-double v4, p0, v4

    const-wide/high16 v6, 0x404e000000000000L    # 60.0

    mul-double p0, v4, v6

    .line 2156
    double-to-int v1, p0

    .line 2157
    .local v1, "minutes":I
    int-to-double v4, v1

    sub-double v4, p0, v4

    const-wide v6, 0x40b7700000000000L    # 6000.0

    mul-double p0, v4, v6

    .line 2158
    double-to-int v2, p0

    .line 2159
    .local v2, "seconds":I
    const/4 v3, 0x3

    new-array v3, v3, [Lcom/sec/android/mimage/photoretouching/exif/Rational;

    const/4 v4, 0x0

    .line 2160
    new-instance v5, Lcom/sec/android/mimage/photoretouching/exif/Rational;

    int-to-long v6, v0

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/exif/Rational;-><init>(JJ)V

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Lcom/sec/android/mimage/photoretouching/exif/Rational;

    int-to-long v6, v1

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/exif/Rational;-><init>(JJ)V

    aput-object v5, v3, v4

    const/4 v4, 0x2

    new-instance v5, Lcom/sec/android/mimage/photoretouching/exif/Rational;

    int-to-long v6, v2

    const-wide/16 v8, 0x64

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/exif/Rational;-><init>(JJ)V

    aput-object v5, v3, v4

    .line 2159
    return-object v3
.end method


# virtual methods
.method public addDateTimeStampTag(IJLjava/util/TimeZone;)Z
    .locals 4
    .param p1, "tagId"    # I
    .param p2, "timestamp"    # J
    .param p4, "timezone"    # Ljava/util/TimeZone;

    .prologue
    const/4 v1, 0x0

    .line 2086
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DATE_TIME:I

    if-eq p1, v2, :cond_0

    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DATE_TIME_DIGITIZED:I

    if-eq p1, v2, :cond_0

    .line 2087
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DATE_TIME_ORIGINAL:I

    if-ne p1, v2, :cond_1

    .line 2088
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mDateTimeStampFormat:Ljava/text/DateFormat;

    invoke-virtual {v2, p4}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 2089
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mDateTimeStampFormat:Ljava/text/DateFormat;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v0

    .line 2090
    .local v0, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v0, :cond_2

    .line 2097
    .end local v0    # "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :cond_1
    :goto_0
    return v1

    .line 2093
    .restart local v0    # "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :cond_2
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 2097
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public addGpsTags(DD)Z
    .locals 9
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    const-wide/16 v6, 0x0

    .line 2108
    sget v4, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LATITUDE:I

    invoke-static {p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->toExifLatLong(D)[Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v1

    .line 2109
    .local v1, "latTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    sget v4, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LONGITUDE:I

    invoke-static {p3, p4}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->toExifLatLong(D)[Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v3

    .line 2110
    .local v3, "longTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    sget v5, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LATITUDE_REF:I

    .line 2111
    cmpl-double v4, p1, v6

    if-ltz v4, :cond_1

    const-string v4, "N"

    .line 2110
    :goto_0
    invoke-virtual {p0, v5, v4}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v0

    .line 2113
    .local v0, "latRefTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    sget v5, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LONGITUDE_REF:I

    .line 2114
    cmpl-double v4, p3, v6

    if-ltz v4, :cond_2

    const-string v4, "E"

    .line 2113
    :goto_1
    invoke-virtual {p0, v5, v4}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v2

    .line 2116
    .local v2, "longRefTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-eqz v1, :cond_0

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    if-nez v2, :cond_3

    .line 2117
    :cond_0
    const/4 v4, 0x0

    .line 2123
    :goto_2
    return v4

    .line 2112
    .end local v0    # "latRefTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .end local v2    # "longRefTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :cond_1
    const-string v4, "S"

    goto :goto_0

    .line 2115
    .restart local v0    # "latRefTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :cond_2
    const-string v4, "W"

    goto :goto_1

    .line 2119
    .restart local v2    # "longRefTag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :cond_3
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 2120
    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 2121
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 2122
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 2123
    const/4 v4, 0x1

    goto :goto_2
.end method

.method public buildTag(IILjava/lang/Object;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .locals 8
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I
    .param p3, "val"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x0

    .line 1619
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagInfo()Landroid/util/SparseIntArray;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v6

    .line 1620
    .local v6, "info":I
    if-eqz v6, :cond_0

    if-nez p3, :cond_2

    :cond_0
    move-object v0, v7

    .line 1633
    :cond_1
    :goto_0
    return-object v0

    .line 1623
    :cond_2
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTypeFromInfo(I)S

    move-result v2

    .line 1624
    .local v2, "type":S
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getComponentCountFromInfo(I)I

    move-result v3

    .line 1625
    .local v3, "definedCount":I
    if-eqz v3, :cond_3

    const/4 v5, 0x1

    .line 1626
    .local v5, "hasDefinedCount":Z
    :goto_1
    invoke-static {v6, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->isIfdAllowed(II)Z

    move-result v1

    if-nez v1, :cond_4

    move-object v0, v7

    .line 1627
    goto :goto_0

    .line 1625
    .end local v5    # "hasDefinedCount":Z
    :cond_3
    const/4 v5, 0x0

    goto :goto_1

    .line 1629
    .restart local v5    # "hasDefinedCount":Z
    :cond_4
    new-instance v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;-><init>(SSIIZ)V

    .line 1630
    .local v0, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    invoke-virtual {v0, p3}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object v0, v7

    .line 1631
    goto :goto_0
.end method

.method public buildTag(ILjava/lang/Object;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .locals 2
    .param p1, "tagId"    # I
    .param p2, "val"    # Ljava/lang/Object;

    .prologue
    .line 1644
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueIfd(I)I

    move-result v0

    .line 1645
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildTag(IILjava/lang/Object;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v1

    return-object v1
.end method

.method protected buildUninitializedTag(I)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .locals 7
    .param p1, "tagId"    # I

    .prologue
    .line 1649
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagInfo()Landroid/util/SparseIntArray;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v6

    .line 1650
    .local v6, "info":I
    if-nez v6, :cond_0

    .line 1651
    const/4 v0, 0x0

    .line 1658
    :goto_0
    return-object v0

    .line 1653
    :cond_0
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTypeFromInfo(I)S

    move-result v2

    .line 1654
    .local v2, "type":S
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getComponentCountFromInfo(I)I

    move-result v3

    .line 1655
    .local v3, "definedCount":I
    if-eqz v3, :cond_1

    const/4 v5, 0x1

    .line 1656
    .local v5, "hasDefinedCount":Z
    :goto_1
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueIfd(I)I

    move-result v4

    .line 1657
    .local v4, "ifdId":I
    new-instance v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;-><init>(SSIIZ)V

    .line 1658
    .local v0, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    goto :goto_0

    .line 1655
    .end local v0    # "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .end local v4    # "ifdId":I
    .end local v5    # "hasDefinedCount":Z
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public clearExif()V
    .locals 2

    .prologue
    .line 905
    new-instance v0, Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    sget-object v1, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->DEFAULT_BYTE_ORDER:Ljava/nio/ByteOrder;

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;-><init>(Ljava/nio/ByteOrder;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    .line 906
    return-void
.end method

.method public deleteTag(I)V
    .locals 1
    .param p1, "tagId"    # I

    .prologue
    .line 1736
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getDefinedTagDefaultIfd(I)I

    move-result v0

    .line 1737
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->deleteTag(II)V

    .line 1738
    return-void
.end method

.method public deleteTag(II)V
    .locals 2
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I

    .prologue
    .line 1727
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->removeTag(SI)V

    .line 1728
    return-void
.end method

.method public forceRewriteExif(Ljava/lang/String;)V
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1254
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getAllTags()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->forceRewriteExif(Ljava/lang/String;Ljava/util/Collection;)V

    .line 1255
    return-void
.end method

.method public forceRewriteExif(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 9
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/mimage/photoretouching/exif/ExifTag;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1218
    .local p2, "tags":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->rewriteExif(Ljava/lang/String;Ljava/util/Collection;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1220
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    .line 1221
    .local v6, "tempData":Lcom/sec/android/mimage/photoretouching/exif/ExifData;
    new-instance v7, Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    sget-object v8, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->DEFAULT_BYTE_ORDER:Ljava/nio/ByteOrder;

    invoke-direct {v7, v8}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;-><init>(Ljava/nio/ByteOrder;)V

    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    .line 1222
    const/4 v4, 0x0

    .line 1223
    .local v4, "is":Ljava/io/FileInputStream;
    const/4 v0, 0x0

    .line 1225
    .local v0, "bytes":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1226
    .end local v4    # "is":Ljava/io/FileInputStream;
    .local v5, "is":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1227
    .end local v0    # "bytes":Ljava/io/ByteArrayOutputStream;
    .local v1, "bytes":Ljava/io/ByteArrayOutputStream;
    :try_start_2
    invoke-direct {p0, v5, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->doExifStreamIO(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 1228
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 1229
    .local v3, "imageBytes":[B
    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->readExif([B)V

    .line 1230
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->setTags(Ljava/util/Collection;)V

    .line 1231
    invoke-virtual {p0, v3, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->writeExif([BLjava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1236
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 1238
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    .line 1241
    .end local v1    # "bytes":Ljava/io/ByteArrayOutputStream;
    .end local v3    # "imageBytes":[B
    .end local v5    # "is":Ljava/io/FileInputStream;
    .end local v6    # "tempData":Lcom/sec/android/mimage/photoretouching/exif/ExifData;
    :cond_0
    return-void

    .line 1232
    .restart local v0    # "bytes":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "is":Ljava/io/FileInputStream;
    .restart local v6    # "tempData":Lcom/sec/android/mimage/photoretouching/exif/ExifData;
    :catch_0
    move-exception v2

    .line 1233
    .local v2, "e":Ljava/io/IOException;
    :goto_0
    :try_start_3
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->closeSilently(Ljava/io/Closeable;)V

    .line 1234
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1235
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 1236
    :goto_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 1238
    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    .line 1239
    throw v7

    .line 1235
    .end local v4    # "is":Ljava/io/FileInputStream;
    .restart local v5    # "is":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "is":Ljava/io/FileInputStream;
    .restart local v4    # "is":Ljava/io/FileInputStream;
    goto :goto_1

    .end local v0    # "bytes":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "is":Ljava/io/FileInputStream;
    .restart local v1    # "bytes":Ljava/io/ByteArrayOutputStream;
    .restart local v5    # "is":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v7

    move-object v0, v1

    .end local v1    # "bytes":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "bytes":Ljava/io/ByteArrayOutputStream;
    move-object v4, v5

    .end local v5    # "is":Ljava/io/FileInputStream;
    .restart local v4    # "is":Ljava/io/FileInputStream;
    goto :goto_1

    .line 1232
    .end local v4    # "is":Ljava/io/FileInputStream;
    .restart local v5    # "is":Ljava/io/FileInputStream;
    :catch_1
    move-exception v2

    move-object v4, v5

    .end local v5    # "is":Ljava/io/FileInputStream;
    .restart local v4    # "is":Ljava/io/FileInputStream;
    goto :goto_0

    .end local v0    # "bytes":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "is":Ljava/io/FileInputStream;
    .restart local v1    # "bytes":Ljava/io/ByteArrayOutputStream;
    .restart local v5    # "is":Ljava/io/FileInputStream;
    :catch_2
    move-exception v2

    move-object v0, v1

    .end local v1    # "bytes":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "bytes":Ljava/io/ByteArrayOutputStream;
    move-object v4, v5

    .end local v5    # "is":Ljava/io/FileInputStream;
    .restart local v4    # "is":Ljava/io/FileInputStream;
    goto :goto_0
.end method

.method public getActualTagCount(II)I
    .locals 2
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I

    .prologue
    .line 1555
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTag(II)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v0

    .line 1556
    .local v0, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v0, :cond_0

    .line 1557
    const/4 v1, 0x0

    .line 1559
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v1

    goto :goto_0
.end method

.method public getAllTags()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/mimage/photoretouching/exif/ExifTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1263
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getAllTags()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDefinedTagCount(I)I
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1538
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagInfo()Landroid/util/SparseIntArray;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 1539
    .local v0, "info":I
    if-nez v0, :cond_0

    .line 1540
    const/4 v1, 0x0

    .line 1542
    :goto_0
    return v1

    :cond_0
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getComponentCountFromInfo(I)I

    move-result v1

    goto :goto_0
.end method

.method public getDefinedTagDefaultIfd(I)I
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1570
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagInfo()Landroid/util/SparseIntArray;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 1571
    .local v0, "info":I
    if-nez v0, :cond_0

    .line 1572
    const/4 v1, -0x1

    .line 1574
    :goto_0
    return v1

    :cond_0
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueIfd(I)I

    move-result v1

    goto :goto_0
.end method

.method public getDefinedTagType(I)S
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1585
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagInfo()Landroid/util/SparseIntArray;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 1586
    .local v0, "info":I
    if-nez v0, :cond_0

    .line 1587
    const/4 v1, -0x1

    .line 1589
    :goto_0
    return v1

    :cond_0
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTypeFromInfo(I)S

    move-result v1

    goto :goto_0
.end method

.method public getExifData(ILandroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p1, "tagId"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 760
    const/4 v3, 0x0

    .line 761
    .local v3, "exifInfo":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    if-eqz v6, :cond_0

    .line 763
    packed-switch p1, :pswitch_data_0

    .line 834
    :cond_0
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "JW exifInfo="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    move-object v6, v3

    .line 835
    :goto_1
    return-object v6

    .line 767
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getAllTags()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 769
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getAllTags()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 771
    .local v2, "exif":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getTagId()S

    move-result v7

    sget v8, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DATE_TIME:I

    if-ne v7, v8, :cond_1

    .line 773
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->forceGetValueAsString()Ljava/lang/String;

    move-result-object v3

    .line 774
    goto :goto_0

    .line 782
    .end local v2    # "exif":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :pswitch_1
    const-string v6, "connectivity"

    invoke-virtual {p2, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 783
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v0, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v6

    if-nez v6, :cond_3

    :cond_2
    invoke-virtual {v0, v9}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v0, v9}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 785
    :cond_3
    const/4 v6, 0x2

    new-array v5, v6, [D

    .line 786
    .local v5, "value":[D
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getLatLongAsDoubles()[D

    move-result-object v5

    .line 787
    if-eqz v5, :cond_4

    .line 788
    invoke-direct {p0, p2, v5, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getLocation(Landroid/content/Context;[DI)Ljava/lang/String;

    move-result-object v3

    .line 789
    :cond_4
    const-string v6, "JW network connected"

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 794
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    .end local v5    # "value":[D
    :pswitch_2
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getAllTags()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 796
    new-instance v3, Ljava/lang/String;

    .end local v3    # "exifInfo":Ljava/lang/String;
    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    .line 797
    .restart local v3    # "exifInfo":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getAllTags()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_6

    .line 816
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sj, GEI - getExifData() 11 - exifInfo : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 818
    if-eqz v3, :cond_0

    .line 819
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_9

    .line 820
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 797
    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 799
    .restart local v2    # "exif":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getTagId()S

    move-result v7

    const v8, 0xffff

    and-int/2addr v7, v8

    const/high16 v8, 0x20000

    or-int v4, v7, v8

    .line 800
    .local v4, "tag":I
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "sj, GEI - getExifData() - tag : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getTagId()S

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / tag : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 801
    const-string v8, " / exif : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->forceGetValueAsString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 800
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 802
    if-eqz v2, :cond_7

    sget v7, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_ISO_SPEED_RATINGS:I

    if-ne v4, v7, :cond_7

    .line 803
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "sj, GEI - getExifData() - exif iso : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->forceGetValueAsString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 804
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "iso:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->forceGetValueAsString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 805
    goto/16 :goto_2

    :cond_7
    if-eqz v2, :cond_8

    sget v7, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXPOSURE_TIME:I

    if-ne v4, v7, :cond_8

    .line 806
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "sj, GEI - getExifData() - exif exposure time: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->forceGetValueAsString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 807
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "exposure:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->forceGetValueAsString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 808
    goto/16 :goto_2

    :cond_8
    if-eqz v2, :cond_5

    sget v7, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_F_NUMBER:I

    if-ne v4, v7, :cond_5

    .line 809
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "sj, GEI - getExifData() - exif f-stop : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->forceGetValueAsString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 810
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "fnumber:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->forceGetValueAsString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 822
    .end local v2    # "exif":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .end local v4    # "tag":I
    :cond_9
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    .line 823
    .local v1, "endPos":I
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_a

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 824
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    new-instance v7, Ljava/lang/String;

    const-string v8, ","

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    sub-int v1, v6, v7

    .line 825
    :cond_a
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v6, v1, :cond_b

    if-lez v1, :cond_b

    .line 826
    invoke-virtual {v3, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 827
    :cond_b
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sj, GEI - getExifData() 22 - exifInfo : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 763
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getExifWriterStream(Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 5
    .param p1, "outStream"    # Ljava/io/OutputStream;

    .prologue
    .line 1081
    if-nez p1, :cond_0

    .line 1082
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1084
    :cond_0
    new-instance v0, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;

    invoke-direct {v0, p1, p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;-><init>(Ljava/io/OutputStream;Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;)V

    .line 1085
    .local v0, "eos":Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getAllTags()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1087
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifOutputStream;->setExifData(Lcom/sec/android/mimage/photoretouching/exif/ExifData;)V

    .line 1088
    return-object v0

    .line 1085
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 1086
    .local v1, "exif":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mData:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->forceGetValueAsString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getExifWriterStream(Ljava/lang/String;)Ljava/io/OutputStream;
    .locals 5
    .param p1, "exifOutFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 1104
    if-nez p1, :cond_0

    .line 1105
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Argument is null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1107
    :cond_0
    const/4 v1, 0x0

    .line 1109
    .local v1, "out":Ljava/io/OutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1114
    .end local v1    # "out":Ljava/io/OutputStream;
    .local v2, "out":Ljava/io/OutputStream;
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getExifWriterStream(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v3

    return-object v3

    .line 1110
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v1    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 1111
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->closeSilently(Ljava/io/Closeable;)V

    .line 1112
    throw v0
.end method

.method public getLatLongAsDoubles()[D
    .locals 8

    .prologue
    const/4 v6, 0x3

    .line 2054
    sget v5, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LATITUDE:I

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagRationalValues(I)[Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v1

    .line 2055
    .local v1, "latitude":[Lcom/sec/android/mimage/photoretouching/exif/Rational;
    sget v5, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LATITUDE_REF:I

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagStringValue(I)Ljava/lang/String;

    move-result-object v2

    .line 2056
    .local v2, "latitudeRef":Ljava/lang/String;
    sget v5, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LONGITUDE:I

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagRationalValues(I)[Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v3

    .line 2057
    .local v3, "longitude":[Lcom/sec/android/mimage/photoretouching/exif/Rational;
    sget v5, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LONGITUDE_REF:I

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagStringValue(I)Ljava/lang/String;

    move-result-object v4

    .line 2058
    .local v4, "longitudeRef":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    if-eqz v4, :cond_0

    .line 2059
    array-length v5, v1

    if-lt v5, v6, :cond_0

    array-length v5, v3

    if-ge v5, v6, :cond_1

    .line 2060
    :cond_0
    const/4 v0, 0x0

    .line 2065
    :goto_0
    return-object v0

    .line 2062
    :cond_1
    const/4 v5, 0x2

    new-array v0, v5, [D

    .line 2063
    .local v0, "latLon":[D
    const/4 v5, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->convertLatOrLongToDouble([Lcom/sec/android/mimage/photoretouching/exif/Rational;Ljava/lang/String;)D

    move-result-wide v6

    aput-wide v6, v0, v5

    .line 2064
    const/4 v5, 0x1

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->convertLatOrLongToDouble([Lcom/sec/android/mimage/photoretouching/exif/Rational;Ljava/lang/String;)D

    move-result-wide v6

    aput-wide v6, v0, v5

    goto :goto_0
.end method

.method public getTag(I)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1312
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getDefinedTagDefaultIfd(I)I

    move-result v0

    .line 1313
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTag(II)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v1

    return-object v1
.end method

.method public getTag(II)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .locals 2
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I

    .prologue
    .line 1298
    invoke-static {p2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->isValidIfd(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1299
    const/4 v0, 0x0

    .line 1301
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTrueTagKey(I)S

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getTag(SI)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v0

    goto :goto_0
.end method

.method public getTagByteValue(I)Ljava/lang/Byte;
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1416
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getDefinedTagDefaultIfd(I)I

    move-result v0

    .line 1417
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagByteValue(II)Ljava/lang/Byte;

    move-result-object v1

    return-object v1
.end method

.method public getTagByteValue(II)Ljava/lang/Byte;
    .locals 3
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I

    .prologue
    .line 1405
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagByteValues(II)[B

    move-result-object v0

    .line 1406
    .local v0, "l":[B
    if-eqz v0, :cond_0

    array-length v1, v0

    if-gtz v1, :cond_1

    .line 1407
    :cond_0
    const/4 v1, 0x0

    .line 1409
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Ljava/lang/Byte;

    const/4 v2, 0x0

    aget-byte v2, v0, v2

    invoke-direct {v1, v2}, Ljava/lang/Byte;-><init>(B)V

    goto :goto_0
.end method

.method public getTagByteValues(I)[B
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1492
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getDefinedTagDefaultIfd(I)I

    move-result v0

    .line 1493
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagByteValues(II)[B

    move-result-object v1

    return-object v1
.end method

.method public getTagByteValues(II)[B
    .locals 2
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I

    .prologue
    .line 1481
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTag(II)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v0

    .line 1482
    .local v0, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v0, :cond_0

    .line 1483
    const/4 v1, 0x0

    .line 1485
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsBytes()[B

    move-result-object v1

    goto :goto_0
.end method

.method protected getTagDefinition(SI)I
    .locals 2
    .param p1, "tagId"    # S
    .param p2, "defaultIfd"    # I

    .prologue
    .line 1798
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagInfo()Landroid/util/SparseIntArray;

    move-result-object v0

    invoke-static {p2, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(IS)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    return v0
.end method

.method protected getTagDefinitionForTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)I
    .locals 4
    .param p1, "tag"    # Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .prologue
    .line 1820
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getDataType()S

    move-result v2

    .line 1821
    .local v2, "type":S
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getComponentCount()I

    move-result v0

    .line 1822
    .local v0, "count":I
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getIfd()I

    move-result v1

    .line 1823
    .local v1, "ifd":I
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getTagId()S

    move-result v3

    invoke-virtual {p0, v3, v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagDefinitionForTag(SSII)I

    move-result v3

    return v3
.end method

.method protected getTagDefinitionForTag(SSII)I
    .locals 15
    .param p1, "tagId"    # S
    .param p2, "type"    # S
    .param p3, "count"    # I
    .param p4, "ifd"    # I

    .prologue
    .line 1827
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagDefinitionsForTagId(S)[I

    move-result-object v4

    .line 1828
    .local v4, "defs":[I
    if-nez v4, :cond_1

    .line 1829
    const/4 v9, -0x1

    .line 1851
    :cond_0
    :goto_0
    return v9

    .line 1831
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagInfo()Landroid/util/SparseIntArray;

    move-result-object v7

    .line 1832
    .local v7, "infos":Landroid/util/SparseIntArray;
    const/4 v9, -0x1

    .line 1833
    .local v9, "ret":I
    array-length v13, v4

    const/4 v11, 0x0

    move v12, v11

    :goto_1
    if-ge v12, v13, :cond_0

    aget v5, v4, v12

    .line 1834
    .local v5, "i":I
    invoke-virtual {v7, v5}, Landroid/util/SparseIntArray;->get(I)I

    move-result v6

    .line 1835
    .local v6, "info":I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTypeFromInfo(I)S

    move-result v3

    .line 1836
    .local v3, "def_type":S
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getComponentCountFromInfo(I)I

    move-result v1

    .line 1837
    .local v1, "def_count":I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getAllowedIfdsFromInfo(I)[I

    move-result-object v2

    .line 1838
    .local v2, "def_ifds":[I
    const/4 v10, 0x0

    .line 1839
    .local v10, "valid_ifd":Z
    array-length v14, v2

    const/4 v11, 0x0

    :goto_2
    if-lt v11, v14, :cond_3

    .line 1845
    :goto_3
    if-eqz v10, :cond_5

    move/from16 v0, p2

    if-ne v0, v3, :cond_5

    .line 1846
    move/from16 v0, p3

    if-eq v0, v1, :cond_2

    if-nez v1, :cond_5

    .line 1847
    :cond_2
    move v9, v5

    .line 1848
    goto :goto_0

    .line 1839
    :cond_3
    aget v8, v2, v11

    .line 1840
    .local v8, "j":I
    move/from16 v0, p4

    if-ne v8, v0, :cond_4

    .line 1841
    const/4 v10, 0x1

    .line 1842
    goto :goto_3

    .line 1839
    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1833
    .end local v8    # "j":I
    :cond_5
    add-int/lit8 v11, v12, 0x1

    move v12, v11

    goto :goto_1
.end method

.method protected getTagDefinitionsForTagId(S)[I
    .locals 11
    .param p1, "tagId"    # S

    .prologue
    const/4 v8, 0x0

    .line 1802
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/exif/IfdData;->getIfds()[I

    move-result-object v5

    .line 1803
    .local v5, "ifds":[I
    array-length v7, v5

    new-array v3, v7, [I

    .line 1804
    .local v3, "defs":[I
    const/4 v0, 0x0

    .line 1805
    .local v0, "counter":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagInfo()Landroid/util/SparseIntArray;

    move-result-object v6

    .line 1806
    .local v6, "infos":Landroid/util/SparseIntArray;
    array-length v9, v5

    move v7, v8

    move v1, v0

    .end local v0    # "counter":I
    .local v1, "counter":I
    :goto_0
    if-lt v7, v9, :cond_0

    .line 1812
    if-nez v1, :cond_1

    .line 1813
    const/4 v7, 0x0

    .line 1816
    :goto_1
    return-object v7

    .line 1806
    :cond_0
    aget v4, v5, v7

    .line 1807
    .local v4, "i":I
    invoke-static {v4, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(IS)I

    move-result v2

    .line 1808
    .local v2, "def":I
    invoke-virtual {v6, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v10

    if-eqz v10, :cond_2

    .line 1809
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "counter":I
    .restart local v0    # "counter":I
    aput v2, v3, v1

    .line 1806
    :goto_2
    add-int/lit8 v7, v7, 0x1

    move v1, v0

    .end local v0    # "counter":I
    .restart local v1    # "counter":I
    goto :goto_0

    .line 1816
    .end local v2    # "def":I
    .end local v4    # "i":I
    :cond_1
    invoke-static {v3, v8, v1}, Ljava/util/Arrays;->copyOfRange([III)[I

    move-result-object v7

    goto :goto_1

    .restart local v2    # "def":I
    .restart local v4    # "i":I
    :cond_2
    move v0, v1

    .end local v1    # "counter":I
    .restart local v0    # "counter":I
    goto :goto_2
.end method

.method protected getTagInfo()Landroid/util/SparseIntArray;
    .locals 1

    .prologue
    .line 2186
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    if-nez v0, :cond_0

    .line 2187
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    .line 2188
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->initTagInfo()V

    .line 2190
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    return-object v0
.end method

.method public getTagIntValue(I)Ljava/lang/Integer;
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1397
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getDefinedTagDefaultIfd(I)I

    move-result v0

    .line 1398
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagIntValue(II)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public getTagIntValue(II)Ljava/lang/Integer;
    .locals 3
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I

    .prologue
    .line 1386
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagIntValues(II)[I

    move-result-object v0

    .line 1387
    .local v0, "l":[I
    if-eqz v0, :cond_0

    array-length v1, v0

    if-gtz v1, :cond_1

    .line 1388
    :cond_0
    const/4 v1, 0x0

    .line 1390
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Ljava/lang/Integer;

    const/4 v2, 0x0

    aget v2, v0, v2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    goto :goto_0
.end method

.method public getTagIntValues(I)[I
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1473
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getDefinedTagDefaultIfd(I)I

    move-result v0

    .line 1474
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagIntValues(II)[I

    move-result-object v1

    return-object v1
.end method

.method public getTagIntValues(II)[I
    .locals 2
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I

    .prologue
    .line 1462
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTag(II)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v0

    .line 1463
    .local v0, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v0, :cond_0

    .line 1464
    const/4 v1, 0x0

    .line 1466
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsInts()[I

    move-result-object v1

    goto :goto_0
.end method

.method public getTagLongValue(I)Ljava/lang/Long;
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1378
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getDefinedTagDefaultIfd(I)I

    move-result v0

    .line 1379
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagLongValue(II)Ljava/lang/Long;

    move-result-object v1

    return-object v1
.end method

.method public getTagLongValue(II)Ljava/lang/Long;
    .locals 4
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I

    .prologue
    .line 1367
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagLongValues(II)[J

    move-result-object v0

    .line 1368
    .local v0, "l":[J
    if-eqz v0, :cond_0

    array-length v1, v0

    if-gtz v1, :cond_1

    .line 1369
    :cond_0
    const/4 v1, 0x0

    .line 1371
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Ljava/lang/Long;

    const/4 v2, 0x0

    aget-wide v2, v0, v2

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    goto :goto_0
.end method

.method public getTagLongValues(I)[J
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1454
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getDefinedTagDefaultIfd(I)I

    move-result v0

    .line 1455
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagLongValues(II)[J

    move-result-object v1

    return-object v1
.end method

.method public getTagLongValues(II)[J
    .locals 2
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I

    .prologue
    .line 1443
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTag(II)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v0

    .line 1444
    .local v0, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v0, :cond_0

    .line 1445
    const/4 v1, 0x0

    .line 1447
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsLongs()[J

    move-result-object v1

    goto :goto_0
.end method

.method public getTagRationalValue(I)Lcom/sec/android/mimage/photoretouching/exif/Rational;
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1435
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getDefinedTagDefaultIfd(I)I

    move-result v0

    .line 1436
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagRationalValue(II)Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v1

    return-object v1
.end method

.method public getTagRationalValue(II)Lcom/sec/android/mimage/photoretouching/exif/Rational;
    .locals 3
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I

    .prologue
    .line 1424
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagRationalValues(II)[Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v0

    .line 1425
    .local v0, "l":[Lcom/sec/android/mimage/photoretouching/exif/Rational;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 1426
    :cond_0
    const/4 v1, 0x0

    .line 1428
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/sec/android/mimage/photoretouching/exif/Rational;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-direct {v1, v2}, Lcom/sec/android/mimage/photoretouching/exif/Rational;-><init>(Lcom/sec/android/mimage/photoretouching/exif/Rational;)V

    goto :goto_0
.end method

.method public getTagRationalValues(I)[Lcom/sec/android/mimage/photoretouching/exif/Rational;
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1511
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getDefinedTagDefaultIfd(I)I

    move-result v0

    .line 1512
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagRationalValues(II)[Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v1

    return-object v1
.end method

.method public getTagRationalValues(II)[Lcom/sec/android/mimage/photoretouching/exif/Rational;
    .locals 2
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I

    .prologue
    .line 1500
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTag(II)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v0

    .line 1501
    .local v0, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v0, :cond_0

    .line 1502
    const/4 v1, 0x0

    .line 1504
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsRationals()[Lcom/sec/android/mimage/photoretouching/exif/Rational;

    move-result-object v1

    goto :goto_0
.end method

.method public getTagStringValue(I)Ljava/lang/String;
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1359
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getDefinedTagDefaultIfd(I)I

    move-result v0

    .line 1360
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagStringValue(II)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getTagStringValue(II)Ljava/lang/String;
    .locals 2
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I

    .prologue
    .line 1348
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTag(II)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v0

    .line 1349
    .local v0, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v0, :cond_0

    .line 1350
    const/4 v1, 0x0

    .line 1352
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValueAsString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getTagValue(I)Ljava/lang/Object;
    .locals 2
    .param p1, "tagId"    # I

    .prologue
    .line 1335
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getDefinedTagDefaultIfd(I)I

    move-result v0

    .line 1336
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagValue(II)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public getTagValue(II)Ljava/lang/Object;
    .locals 2
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I

    .prologue
    .line 1322
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTag(II)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v0

    .line 1323
    .local v0, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->getValue()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public getTagsForIfdId(I)Ljava/util/List;
    .locals 1
    .param p1, "ifdId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/mimage/photoretouching/exif/ExifTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1289
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getAllTagsForIfd(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTagsForTagId(S)Ljava/util/List;
    .locals 1
    .param p1, "tagId"    # S
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/mimage/photoretouching/exif/ExifTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1276
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getAllTagsForTagId(S)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getThumbnail()[B
    .locals 1

    .prologue
    .line 1907
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getCompressedThumbnail()[B

    move-result-object v0

    return-object v0
.end method

.method public getThumbnailBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 1876
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->hasCompressedThumbnail()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1877
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getCompressedThumbnail()[B

    move-result-object v0

    .line 1878
    .local v0, "thumb":[B
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1882
    .end local v0    # "thumb":[B
    :goto_0
    return-object v1

    .line 1879
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->hasUncompressedStrip()Z

    .line 1882
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getThumbnailBytes()[B
    .locals 1

    .prologue
    .line 1893
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->hasCompressedThumbnail()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1894
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getCompressedThumbnail()[B

    move-result-object v0

    .line 1898
    :goto_0
    return-object v0

    .line 1895
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->hasUncompressedStrip()Z

    .line 1898
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUserComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1973
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->getUserComment()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasThumbnail()Z
    .locals 1

    .prologue
    .line 1926
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->hasCompressedThumbnail()Z

    move-result v0

    return v0
.end method

.method public isTagCountDefined(I)Z
    .locals 3
    .param p1, "tagId"    # I

    .prologue
    const/4 v1, 0x0

    .line 1522
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagInfo()Landroid/util/SparseIntArray;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 1524
    .local v0, "info":I
    if-nez v0, :cond_1

    .line 1527
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getComponentCountFromInfo(I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isThumbnailCompressed()Z
    .locals 1

    .prologue
    .line 1916
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->hasCompressedThumbnail()Z

    move-result v0

    return v0
.end method

.method public readExif(Ljava/io/InputStream;)V
    .locals 5
    .param p1, "inStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 746
    if-nez p1, :cond_0

    .line 747
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 749
    :cond_0
    const/4 v0, 0x0

    .line 751
    .local v0, "d":Lcom/sec/android/mimage/photoretouching/exif/ExifData;
    :try_start_0
    new-instance v2, Lcom/sec/android/mimage/photoretouching/exif/ExifReader;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifReader;-><init>(Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;)V

    invoke-virtual {v2, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifReader;->read(Ljava/io/InputStream;)Lcom/sec/android/mimage/photoretouching/exif/ExifData;
    :try_end_0
    .catch Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 755
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    .line 756
    return-void

    .line 752
    :catch_0
    move-exception v1

    .line 753
    .local v1, "e":Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid exif format : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public readExif(Ljava/lang/String;)V
    .locals 5
    .param p1, "inFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 876
    if-nez p1, :cond_0

    .line 877
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Argument is null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 879
    :cond_0
    const/4 v1, 0x0

    .line 881
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/BufferedInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 882
    .end local v1    # "is":Ljava/io/InputStream;
    .local v2, "is":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->readExif(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 887
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 888
    return-void

    .line 883
    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 884
    .local v0, "e":Ljava/io/IOException;
    :goto_0
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->closeSilently(Ljava/io/Closeable;)V

    .line 885
    throw v0

    .line 883
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_0
.end method

.method public readExif([B)V
    .locals 1
    .param p1, "jpeg"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 735
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->readExif(Ljava/io/InputStream;)V

    .line 736
    return-void
.end method

.method public removeCompressedThumbnail()V
    .locals 2

    .prologue
    .line 1963
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->setCompressedThumbnail([B)V

    .line 1964
    return-void
.end method

.method public removeTagDefinition(I)V
    .locals 1
    .param p1, "tagId"    # I

    .prologue
    .line 1860
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagInfo()Landroid/util/SparseIntArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->delete(I)V

    .line 1861
    return-void
.end method

.method public resetTagDefinitions()V
    .locals 1

    .prologue
    .line 1867
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mTagInfo:Landroid/util/SparseIntArray;

    .line 1868
    return-void
.end method

.method public rewriteExif(Ljava/lang/String;Ljava/util/Collection;)Z
    .locals 19
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/mimage/photoretouching/exif/ExifTag;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1133
    .local p2, "tags":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    const/4 v10, 0x0

    .line 1134
    .local v10, "file":Ljava/io/RandomAccessFile;
    const/4 v14, 0x0

    .line 1137
    .local v14, "is":Ljava/io/InputStream;
    :try_start_0
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1138
    .local v18, "temp":Ljava/io/File;
    new-instance v15, Ljava/io/BufferedInputStream;

    new-instance v2, Ljava/io/FileInputStream;

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v15, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1141
    .end local v14    # "is":Ljava/io/InputStream;
    .local v15, "is":Ljava/io/InputStream;
    const/16 v16, 0x0

    .line 1143
    .local v16, "parser":Lcom/sec/android/mimage/photoretouching/exif/ExifParser;
    :try_start_1
    move-object/from16 v0, p0

    invoke-static {v15, v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->parse(Ljava/io/InputStream;Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;)Lcom/sec/android/mimage/photoretouching/exif/ExifParser;
    :try_end_1
    .catch Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v16

    .line 1147
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/exif/ExifParser;->getOffsetToExifEndFromSOF()I

    move-result v2

    int-to-long v6, v2

    .line 1150
    .local v6, "exifSize":J
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1151
    const/4 v14, 0x0

    .line 1154
    .end local v15    # "is":Ljava/io/InputStream;
    .restart local v14    # "is":Ljava/io/InputStream;
    :try_start_3
    new-instance v11, Ljava/io/RandomAccessFile;

    const-string v2, "rw"

    move-object/from16 v0, v18

    invoke-direct {v11, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1155
    .end local v10    # "file":Ljava/io/RandomAccessFile;
    .local v11, "file":Ljava/io/RandomAccessFile;
    :try_start_4
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    .line 1156
    .local v12, "fileLength":J
    cmp-long v2, v12, v6

    if-gez v2, :cond_0

    .line 1157
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Filesize changed during operation"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1166
    .end local v12    # "fileLength":J
    :catch_0
    move-exception v9

    move-object v10, v11

    .line 1167
    .end local v6    # "exifSize":J
    .end local v11    # "file":Ljava/io/RandomAccessFile;
    .end local v16    # "parser":Lcom/sec/android/mimage/photoretouching/exif/ExifParser;
    .end local v18    # "temp":Ljava/io/File;
    .local v9, "e":Ljava/io/IOException;
    .restart local v10    # "file":Ljava/io/RandomAccessFile;
    :goto_0
    :try_start_5
    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->closeSilently(Ljava/io/Closeable;)V

    .line 1168
    throw v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1169
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 1170
    :goto_1
    invoke-static {v14}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->closeSilently(Ljava/io/Closeable;)V

    .line 1171
    throw v2

    .line 1144
    .end local v14    # "is":Ljava/io/InputStream;
    .restart local v15    # "is":Ljava/io/InputStream;
    .restart local v16    # "parser":Lcom/sec/android/mimage/photoretouching/exif/ExifParser;
    .restart local v18    # "temp":Ljava/io/File;
    :catch_1
    move-exception v9

    .line 1145
    .local v9, "e":Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;
    :try_start_6
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Invalid exif format : "

    invoke-direct {v2, v3, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1166
    .end local v9    # "e":Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;
    :catch_2
    move-exception v9

    move-object v14, v15

    .end local v15    # "is":Ljava/io/InputStream;
    .restart local v14    # "is":Ljava/io/InputStream;
    goto :goto_0

    .line 1161
    .end local v10    # "file":Ljava/io/RandomAccessFile;
    .restart local v6    # "exifSize":J
    .restart local v11    # "file":Ljava/io/RandomAccessFile;
    .restart local v12    # "fileLength":J
    :cond_0
    :try_start_7
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v8

    .line 1165
    .local v8, "buf":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v8, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->rewriteExif(Ljava/nio/ByteBuffer;Ljava/util/Collection;)Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result v17

    .line 1170
    .local v17, "ret":Z
    invoke-static {v14}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->closeSilently(Ljava/io/Closeable;)V

    .line 1172
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->close()V

    .line 1173
    return v17

    .line 1169
    .end local v6    # "exifSize":J
    .end local v8    # "buf":Ljava/nio/ByteBuffer;
    .end local v11    # "file":Ljava/io/RandomAccessFile;
    .end local v12    # "fileLength":J
    .end local v14    # "is":Ljava/io/InputStream;
    .end local v17    # "ret":Z
    .restart local v10    # "file":Ljava/io/RandomAccessFile;
    .restart local v15    # "is":Ljava/io/InputStream;
    :catchall_1
    move-exception v2

    move-object v14, v15

    .end local v15    # "is":Ljava/io/InputStream;
    .restart local v14    # "is":Ljava/io/InputStream;
    goto :goto_1

    .end local v10    # "file":Ljava/io/RandomAccessFile;
    .restart local v6    # "exifSize":J
    .restart local v11    # "file":Ljava/io/RandomAccessFile;
    :catchall_2
    move-exception v2

    move-object v10, v11

    .end local v11    # "file":Ljava/io/RandomAccessFile;
    .restart local v10    # "file":Ljava/io/RandomAccessFile;
    goto :goto_1

    .line 1166
    .end local v6    # "exifSize":J
    .end local v16    # "parser":Lcom/sec/android/mimage/photoretouching/exif/ExifParser;
    .end local v18    # "temp":Ljava/io/File;
    :catch_3
    move-exception v9

    goto :goto_0
.end method

.method public rewriteExif(Ljava/nio/ByteBuffer;Ljava/util/Collection;)Z
    .locals 7
    .param p1, "buf"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/mimage/photoretouching/exif/ExifTag;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1190
    .local p2, "tags":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    const/4 v1, 0x0

    .line 1192
    .local v1, "mod":Lcom/sec/android/mimage/photoretouching/exif/ExifModifier;
    :try_start_0
    new-instance v2, Lcom/sec/android/mimage/photoretouching/exif/ExifModifier;

    invoke-direct {v2, p1, p0}, Lcom/sec/android/mimage/photoretouching/exif/ExifModifier;-><init>(Ljava/nio/ByteBuffer;Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;)V
    :try_end_0
    .catch Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1193
    .end local v1    # "mod":Lcom/sec/android/mimage/photoretouching/exif/ExifModifier;
    .local v2, "mod":Lcom/sec/android/mimage/photoretouching/exif/ExifModifier;
    :try_start_1
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1196
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/exif/ExifModifier;->commit()Z

    move-result v4

    return v4

    .line 1193
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 1194
    .local v3, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/exif/ExifModifier;->modifyTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)V
    :try_end_1
    .catch Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1197
    .end local v3    # "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 1198
    .end local v2    # "mod":Lcom/sec/android/mimage/photoretouching/exif/ExifModifier;
    .local v0, "e":Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;
    .restart local v1    # "mod":Lcom/sec/android/mimage/photoretouching/exif/ExifModifier;
    :goto_1
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid exif format : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1197
    .end local v0    # "e":Lcom/sec/android/mimage/photoretouching/exif/ExifInvalidFormatException;
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public setCompressedThumbnail(Landroid/graphics/Bitmap;)Z
    .locals 3
    .param p1, "thumb"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1952
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1953
    .local v0, "thumbnail":Ljava/io/ByteArrayOutputStream;
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x5a

    invoke-virtual {p1, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1954
    const/4 v1, 0x0

    .line 1956
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->setCompressedThumbnail([B)Z

    move-result v1

    goto :goto_0
.end method

.method public setCompressedThumbnail([B)Z
    .locals 1
    .param p1, "thumb"    # [B

    .prologue
    .line 1939
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->clearThumbnailAndStrips()V

    .line 1940
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->setCompressedThumbnail([B)V

    .line 1941
    const/4 v0, 0x1

    return v0
.end method

.method public setExif(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/mimage/photoretouching/exif/ExifTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 897
    .local p1, "tags":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->clearExif()V

    .line 898
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->setTags(Ljava/util/Collection;)V

    .line 899
    return-void
.end method

.method public setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    .locals 1
    .param p1, "tag"    # Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .prologue
    .line 1704
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->mData:Lcom/sec/android/mimage/photoretouching/exif/ExifData;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/exif/ExifData;->addTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v0

    return-object v0
.end method

.method public setTagDefinition(SISS[I)I
    .locals 13
    .param p1, "tagId"    # S
    .param p2, "defaultIfd"    # I
    .param p3, "tagType"    # S
    .param p4, "defaultComponentCount"    # S
    .param p5, "allowedIfds"    # [I

    .prologue
    .line 1759
    invoke-static/range {p3 .. p3}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->isValidType(S)Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-static {p2}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->isValidIfd(I)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1760
    invoke-static {p2, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->defineTag(IS)I

    move-result v8

    .line 1761
    .local v8, "tagDef":I
    const/4 v10, -0x1

    if-ne v8, v10, :cond_0

    .line 1762
    const/4 v8, -0x1

    .line 1794
    .end local v8    # "tagDef":I
    :goto_0
    return v8

    .line 1764
    .restart local v8    # "tagDef":I
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagDefinitionsForTagId(S)[I

    move-result-object v7

    .line 1765
    .local v7, "otherDefs":[I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagInfo()Landroid/util/SparseIntArray;

    move-result-object v6

    .line 1767
    .local v6, "infos":Landroid/util/SparseIntArray;
    const/4 v3, 0x0

    .line 1768
    .local v3, "defaultCheck":Z
    move-object/from16 v0, p5

    array-length v11, v0

    const/4 v10, 0x0

    :goto_1
    if-lt v10, v11, :cond_1

    .line 1776
    if-nez v3, :cond_4

    .line 1777
    const/4 v8, -0x1

    goto :goto_0

    .line 1768
    :cond_1
    aget v4, p5, v10

    .line 1769
    .local v4, "i":I
    if-ne p2, v4, :cond_2

    .line 1770
    const/4 v3, 0x1

    .line 1772
    :cond_2
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->isValidIfd(I)Z

    move-result v12

    if-nez v12, :cond_3

    .line 1773
    const/4 v8, -0x1

    goto :goto_0

    .line 1768
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 1780
    .end local v4    # "i":I
    :cond_4
    invoke-static/range {p5 .. p5}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getFlagsFromAllowedIfds([I)I

    move-result v5

    .line 1782
    .local v5, "ifdFlags":I
    if-eqz v7, :cond_5

    .line 1783
    array-length v11, v7

    const/4 v10, 0x0

    :goto_2
    if-lt v10, v11, :cond_6

    .line 1791
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTagInfo()Landroid/util/SparseIntArray;

    move-result-object v10

    shl-int/lit8 v11, v5, 0x18

    shl-int/lit8 v12, p3, 0x10

    or-int/2addr v11, v12

    or-int v11, v11, p4

    invoke-virtual {v10, v8, v11}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0

    .line 1783
    :cond_6
    aget v2, v7, v10

    .line 1784
    .local v2, "def":I
    invoke-virtual {v6, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v9

    .line 1785
    .local v9, "tagInfo":I
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getAllowedIfdFlagsFromInfo(I)I

    move-result v1

    .line 1786
    .local v1, "allowedFlags":I
    and-int v12, v5, v1

    if-eqz v12, :cond_7

    .line 1787
    const/4 v8, -0x1

    goto :goto_0

    .line 1783
    :cond_7
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 1794
    .end local v1    # "allowedFlags":I
    .end local v2    # "def":I
    .end local v3    # "defaultCheck":Z
    .end local v5    # "ifdFlags":I
    .end local v6    # "infos":Landroid/util/SparseIntArray;
    .end local v7    # "otherDefs":[I
    .end local v8    # "tagDef":I
    .end local v9    # "tagInfo":I
    :cond_8
    const/4 v8, -0x1

    goto :goto_0
.end method

.method public setTagValue(IILjava/lang/Object;)Z
    .locals 2
    .param p1, "tagId"    # I
    .param p2, "ifdId"    # I
    .param p3, "val"    # Ljava/lang/Object;

    .prologue
    .line 1673
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTag(II)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v0

    .line 1674
    .local v0, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-nez v0, :cond_0

    .line 1675
    const/4 v1, 0x0

    .line 1677
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0, p3}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public setTagValue(ILjava/lang/Object;)Z
    .locals 2
    .param p1, "tagId"    # I
    .param p2, "val"    # Ljava/lang/Object;

    .prologue
    .line 1690
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getDefinedTagDefaultIfd(I)I

    move-result v0

    .line 1691
    .local v0, "ifdId":I
    invoke-virtual {p0, p1, v0, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->setTagValue(IILjava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public setTags(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/android/mimage/photoretouching/exif/ExifTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1715
    .local p1, "tags":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1718
    return-void

    .line 1715
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    .line 1716
    .local v0, "t":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->setTag(Lcom/sec/android/mimage/photoretouching/exif/ExifTag;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    goto :goto_0
.end method

.method public writeExif(Landroid/graphics/Bitmap;Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "bmap"    # Landroid/graphics/Bitmap;
    .param p2, "exifOutStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 936
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 937
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument is null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 939
    :cond_1
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getExifWriterStream(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v0

    .line 940
    .local v0, "s":Ljava/io/OutputStream;
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x5a

    invoke-virtual {p1, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 941
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 942
    return-void
.end method

.method public writeExif(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 4
    .param p1, "bmap"    # Landroid/graphics/Bitmap;
    .param p2, "exifOutFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1001
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1002
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1004
    :cond_1
    const/4 v1, 0x0

    .line 1006
    .local v1, "s":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getExifWriterStream(Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v1

    .line 1007
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x5a

    invoke-virtual {p1, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1008
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1013
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 1014
    return-void

    .line 1009
    :catch_0
    move-exception v0

    .line 1010
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->closeSilently(Ljava/io/Closeable;)V

    .line 1011
    throw v0
.end method

.method public writeExif(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "jpegStream"    # Ljava/io/InputStream;
    .param p2, "exifOutStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 954
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 955
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument is null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 957
    :cond_1
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getExifWriterStream(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v0

    .line 958
    .local v0, "s":Ljava/io/OutputStream;
    invoke-direct {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->doExifStreamIO(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 959
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 960
    return-void
.end method

.method public writeExif(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 4
    .param p1, "jpegStream"    # Ljava/io/InputStream;
    .param p2, "exifOutFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1028
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1029
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1031
    :cond_1
    const/4 v1, 0x0

    .line 1033
    .local v1, "s":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getExifWriterStream(Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v1

    .line 1034
    invoke-direct {p0, p1, v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->doExifStreamIO(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 1035
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1040
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 1041
    return-void

    .line 1036
    :catch_0
    move-exception v0

    .line 1037
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->closeSilently(Ljava/io/Closeable;)V

    .line 1038
    throw v0
.end method

.method public writeExif(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "jpegFileName"    # Ljava/lang/String;
    .param p2, "exifOutFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1055
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1056
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Argument is null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1058
    :cond_1
    const/4 v1, 0x0

    .line 1060
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1061
    .end local v1    # "is":Ljava/io/InputStream;
    .local v2, "is":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {p0, v2, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->writeExif(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1066
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 1067
    return-void

    .line 1062
    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 1063
    .local v0, "e":Ljava/io/IOException;
    :goto_0
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->closeSilently(Ljava/io/Closeable;)V

    .line 1064
    throw v0

    .line 1062
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_0
.end method

.method public writeExif([BLjava/io/OutputStream;)V
    .locals 3
    .param p1, "jpeg"    # [B
    .param p2, "exifOutStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 918
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 919
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Argument is null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 921
    :cond_1
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getExifWriterStream(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v0

    .line 922
    .local v0, "s":Ljava/io/OutputStream;
    const/4 v1, 0x0

    array-length v2, p1

    invoke-virtual {v0, p1, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 923
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 924
    return-void
.end method

.method public writeExif([BLjava/lang/String;)V
    .locals 4
    .param p1, "jpeg"    # [B
    .param p2, "exifOutFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 974
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 975
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Argument is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 977
    :cond_1
    const/4 v1, 0x0

    .line 979
    .local v1, "s":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getExifWriterStream(Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v1

    .line 980
    const/4 v2, 0x0

    array-length v3, p1

    invoke-virtual {v1, p1, v2, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 981
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 986
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 987
    return-void

    .line 982
    :catch_0
    move-exception v0

    .line 983
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->closeSilently(Ljava/io/Closeable;)V

    .line 984
    throw v0
.end method
