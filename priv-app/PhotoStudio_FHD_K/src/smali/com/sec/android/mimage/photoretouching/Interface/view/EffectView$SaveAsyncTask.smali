.class Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;
.super Landroid/os/AsyncTask;
.source "EffectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 1778
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1779
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 13
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    const/4 v12, 0x0

    .line 1783
    const/4 v3, 0x0

    .line 1784
    .local v3, "fileName":Ljava/lang/String;
    array-length v8, p1

    if-lez v8, :cond_0

    .line 1785
    const/4 v8, 0x0

    aget-object v3, p1, v8

    .line 1787
    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getAvailableExternalMemorySize()J

    move-result-wide v0

    .line 1788
    .local v0, "available_memsize":J
    const-wide/32 v8, 0xa00000

    cmp-long v8, v0, v8

    if-gez v8, :cond_3

    .line 1789
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/app/ProgressDialog;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 1790
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/app/ProgressDialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1791
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "memory size = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " %"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1793
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f06007b

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 1829
    :cond_2
    :goto_0
    return-object v12

    .line 1797
    :cond_3
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 1798
    const/4 v4, 0x0

    .line 1799
    .local v4, "filePath":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_7

    .line 1800
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 1801
    .local v5, "tempPath":Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1805
    .end local v5    # "tempPath":Ljava/lang/String;
    :goto_1
    if-nez v3, :cond_4

    .line 1806
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    move-result-object v3

    .line 1808
    :cond_4
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 1810
    :goto_2
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isDoingThread()Z

    move-result v8

    if-nez v8, :cond_8

    .line 1821
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1823
    .local v6, "time":J
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mCurrentSaveSize:I
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I

    move-result v9

    invoke-virtual {v8, v4, v3, v9}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->saveCurrentImage(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1824
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 1825
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->setCurrentSavedIndex()V

    .line 1827
    :cond_6
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "bigheadk, save time = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v6

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1803
    .end local v6    # "time":J
    :cond_7
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1813
    :cond_8
    const-wide/16 v8, 0x12c

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1814
    :catch_0
    move-exception v2

    .line 1816
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 7
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const v6, 0x7f0601df

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1842
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1843
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1844
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1846
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1848
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 1849
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 1850
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    .line 1853
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1855
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isPersonalPage()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1857
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const-string v4, "Studio"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1858
    .local v1, "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 1874
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mOptionItemId:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I

    move-result v2

    const v3, 0x7f090156

    if-eq v2, v3, :cond_2

    .line 1875
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mOptionItemId:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I

    move-result v3

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->runOptionItem(I)Z
    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;I)Z

    .line 1876
    :cond_2
    return-void

    .line 1862
    .end local v1    # "text":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPrivateSaveFolder()Ljava/lang/String;

    move-result-object v0

    .line 1863
    .local v0, "privateFolder":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1864
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 1869
    .end local v0    # "privateFolder":Ljava/lang/String;
    .end local v1    # "text":Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const-string v4, "Studio"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1870
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 1833
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1834
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;Landroid/app/ProgressDialog;)V

    .line 1835
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06008f

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1836
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1837
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1838
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1839
    return-void
.end method
