.class Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;
.super Ljava/lang/Thread;
.source "DecorationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContourThread"
.end annotation


# instance fields
.field private isRun:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V
    .locals 1

    .prologue
    .line 1861
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1863
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;->isRun:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;)V
    .locals 0

    .prologue
    .line 1861
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 1866
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;->isRun:Z

    .line 1867
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;)V

    .line 1868
    return-void
.end method

.method public run()V
    .locals 4

    .prologue
    .line 1872
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;->isRun:Z

    if-nez v1, :cond_2

    .line 1894
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;)V

    .line 1895
    return-void

    .line 1875
    :cond_2
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1880
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mColor:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)I

    move-result v2

    const v3, 0xffffff

    xor-int/2addr v2, v3

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;I)V

    .line 1881
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->invalidateViewsWithThread()V

    .line 1882
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1884
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/DecorationView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 1876
    :catch_0
    move-exception v0

    .line 1878
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
