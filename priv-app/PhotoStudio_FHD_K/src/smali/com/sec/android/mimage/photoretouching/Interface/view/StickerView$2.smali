.class Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;
.super Ljava/lang/Object;
.source "StickerView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;)Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    return-object v0
.end method


# virtual methods
.method public setLongClickSticker(I)V
    .locals 4
    .param p1, "resId"    # I

    .prologue
    .line 159
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mIsLongClicked:Z
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 161
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;Z)V

    .line 163
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 165
    .local v0, "sticker":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    .line 166
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    .line 167
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2$1;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;I)V

    .line 165
    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->setActivityLayoutThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;)V
    invoke-static {v1, v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;)V

    .line 220
    .end local v0    # "sticker":Landroid/graphics/Bitmap;
    :cond_0
    return-void
.end method

.method public setSticker(I)V
    .locals 2
    .param p1, "resId"    # I

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->setStickerMode(ILandroid/graphics/Rect;)V

    .line 155
    return-void
.end method
