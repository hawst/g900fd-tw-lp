.class public Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;
.super Lcom/sec/android/mimage/photoretouching/Core/RectController;
.source "MultigridDragObject.java"


# static fields
.field public static final ALPHA_VAL:I = 0xc8


# instance fields
.field private final ROUND_POS:I

.field private mAlphaPaint:Landroid/graphics/Paint;

.field private mDrawRect:Landroid/graphics/RectF;

.field private mOrgImg:Landroid/graphics/Bitmap;

.field private mOrgItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

.field private mSrcRect:Landroid/graphics/Rect;

.field private mStartIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imgData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p3, "moveItem"    # Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    .param p4, "applyAlpha"    # Z

    .prologue
    const/4 v2, 0x0

    .line 26
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;IZ)V

    .line 195
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->ROUND_POS:I

    .line 198
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 199
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mSrcRect:Landroid/graphics/Rect;

    .line 200
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mDrawRect:Landroid/graphics/RectF;

    .line 201
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgImg:Landroid/graphics/Bitmap;

    .line 202
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mAlphaPaint:Landroid/graphics/Paint;

    .line 204
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mStartIndex:I

    .line 28
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 29
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mSrcRect:Landroid/graphics/Rect;

    .line 30
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mSrcRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getmDrawRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 31
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mDrawRect:Landroid/graphics/RectF;

    .line 32
    invoke-direct {p0, p4}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->setOrgImg(Z)V

    .line 33
    return-void
.end method

.method private setOrgImg(Z)V
    .locals 2
    .param p1, "applyAlpha"    # Z

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getOrgImage()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgImg:Landroid/graphics/Bitmap;

    .line 46
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mAlphaPaint:Landroid/graphics/Paint;

    .line 47
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mAlphaPaint:Landroid/graphics/Paint;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 55
    return-void
.end method


# virtual methods
.method public EndMoveObject()V
    .locals 0

    .prologue
    .line 158
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->EndMoveObject()V

    .line 159
    return-void
.end method

.method public InitMoveObject(FFLandroid/graphics/RectF;)I
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "posOnCanvas"    # Landroid/graphics/RectF;

    .prologue
    .line 66
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->setDrawRect(FFLandroid/graphics/RectF;)V

    .line 68
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->InitMoveObject(FF)I

    move-result v0

    return v0
.end method

.method public StartMoveObject(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mDrawRect:Landroid/graphics/RectF;

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->setDrawRect(FFLandroid/graphics/RectF;)V

    .line 85
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->StartMoveObject(FF)V

    .line 86
    return-void
.end method

.method public changeRectSize(Landroid/graphics/RectF;Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "objectRect"    # Landroid/graphics/RectF;
    .param p2, "srcRect"    # Landroid/graphics/Rect;

    .prologue
    .line 94
    if-eqz p1, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mDrawRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mDrawRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    invoke-virtual {p0, v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->setDrawRect(FFLandroid/graphics/RectF;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v0, p2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setmDrawRect(Landroid/graphics/Rect;)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mSrcRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getmDrawRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " : changeRectSize : objectRect is null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->destroy()V

    .line 60
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 61
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgImg:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 62
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->destroy()V

    .line 63
    return-void
.end method

.method public drawObject(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgImg:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgImg:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mSrcRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mDrawRect:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mAlphaPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 193
    :cond_0
    return-void
.end method

.method public drawObject(Landroid/graphics/Canvas;I)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "alphaValue"    # I

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgImg:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mAlphaPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgImg:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mSrcRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mDrawRect:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mAlphaPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 186
    :cond_0
    return-void
.end method

.method public getDefaultSrcRect()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->setOrgImage()V

    .line 108
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mOrgItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getmDrawRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method public getDrawRect()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 173
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mDrawRect:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    return-object v0
.end method

.method public getSrcRect()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 177
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mSrcRect:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method public getStartIndex()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mStartIndex:I

    return v0
.end method

.method public setDrawRect()V
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mDrawRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mDrawRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mDrawRect:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->setDrawRect(FFLandroid/graphics/RectF;)V

    .line 91
    return-void
.end method

.method public setDrawRect(FFLandroid/graphics/RectF;)V
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "posOnCanvas"    # Landroid/graphics/RectF;

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 73
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mDrawRect:Landroid/graphics/RectF;

    .line 74
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v1, v5

    sub-float v1, p1, v1

    .line 75
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float/2addr v2, v5

    sub-float v2, p2, v2

    .line 76
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v3

    div-float/2addr v3, v5

    add-float/2addr v3, p1

    .line 77
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v4

    div-float/2addr v4, v5

    add-float/2addr v4, p2

    .line 73
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 79
    return-void
.end method

.method public setStartIndex(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 163
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mStartIndex:I

    .line 164
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->mStartIndex:I

    return v0
.end method
