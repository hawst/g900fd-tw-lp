.class Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;
.super Ljava/lang/Object;
.source "ButtonControlManager.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout$ActivityLayoutTouchUpCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->GestureLongPress(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public touchDown(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "buttonPos"    # Landroid/graphics/Rect;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->setScrollable(Z)V

    .line 147
    :cond_0
    return-void
.end method

.method public touchMove(Landroid/graphics/Rect;)V
    .locals 21
    .param p1, "selectedButtonPos"    # Landroid/graphics/Rect;

    .prologue
    .line 182
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v17

    if-eqz v17, :cond_0

    .line 183
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v17

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->setScrollable(Z)V

    .line 186
    :cond_0
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v18

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->getScrollY()I

    move-result v18

    sub-int v17, v17, v18

    if-gez v17, :cond_8

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v17

    const/16 v18, 0x0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v20

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->getScrollY()I

    move-result v20

    sub-int v19, v19, v20

    invoke-virtual/range {v17 .. v19}, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->scrollBy(II)V

    .line 195
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/widget/FrameLayout;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v17

    .line 196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v18

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/widget/FrameLayout;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/widget/FrameLayout;->getTop()I

    move-result v18

    .line 195
    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 198
    const/4 v10, 0x0

    .line 199
    .local v10, "max":F
    const/4 v14, 0x0

    .line 201
    .local v14, "target":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v18

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->getLeft()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setLeftBarColor_Move(I)V

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_2
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_9

    .line 239
    if-eqz v14, :cond_7

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v15

    .line 242
    .local v15, "targetIdx":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v18

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v11

    .line 243
    .local v11, "nullIdx":I
    if-eq v15, v11, :cond_7

    const/16 v17, -0x1

    move/from16 v0, v17

    if-le v15, v0, :cond_7

    const/16 v17, -0x1

    move/from16 v0, v17

    if-le v11, v0, :cond_7

    .line 245
    if-le v15, v11, :cond_11

    .line 247
    move v7, v15

    .local v7, "i":I
    :goto_2
    if-ge v7, v11, :cond_e

    .line 259
    const/4 v5, 0x0

    .line 260
    .local v5, "changed":Z
    move v7, v15

    :goto_3
    if-ge v7, v11, :cond_10

    .line 266
    if-eqz v5, :cond_3

    .line 267
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mDoneBtn:Landroid/view/View;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$9(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/view/View;

    move-result-object v17

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setEnabled(Z)V

    .line 268
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->getLeftButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v8

    .line 269
    .local v8, "leftFromNull":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->getRightButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v12

    .line 270
    .local v12, "rightFromNull":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    if-eqz v8, :cond_4

    .line 271
    invoke-interface {v8, v12}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 272
    :cond_4
    if-eqz v12, :cond_5

    .line 273
    invoke-interface {v12, v8}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 274
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v17

    invoke-interface {v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getRightButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 276
    invoke-interface {v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getRightButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v13

    .line 277
    .local v13, "rightFromTarget":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    if-eqz v13, :cond_6

    .line 278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v13, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 279
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v14, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 318
    .end local v13    # "rightFromTarget":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v18

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v18

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v15, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 322
    .end local v5    # "changed":Z
    .end local v7    # "i":I
    .end local v8    # "leftFromNull":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    .end local v11    # "nullIdx":I
    .end local v12    # "rightFromNull":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    .end local v15    # "targetIdx":I
    :cond_7
    return-void

    .line 190
    .end local v10    # "max":F
    .end local v14    # "target":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    :cond_8
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v18

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->getScrollY()I

    move-result v18

    sub-int v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v18

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->getHeight()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_1

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v17

    const/16 v18, 0x0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v20

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->getScrollY()I

    move-result v20

    sub-int v19, v19, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v20

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->getHeight()I

    move-result v20

    sub-int v19, v19, v20

    invoke-virtual/range {v17 .. v19}, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->scrollBy(II)V

    goto/16 :goto_0

    .line 203
    .restart local v10    # "max":F
    .restart local v14    # "target":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    :cond_9
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 205
    .local v3, "button":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->runningAnimation()Z

    move-result v18

    if-nez v18, :cond_7

    .line 207
    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v4

    .line 208
    .local v4, "buttonPos":Landroid/graphics/Rect;
    move-object/from16 v0, p1

    invoke-static {v4, v0}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 210
    const/16 v16, 0x0

    .line 211
    .local v16, "w":I
    const/4 v6, 0x0

    .line 212
    .local v6, "h":I
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 214
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    iget v0, v4, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    sub-int v16, v18, v19

    .line 215
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    iget v0, v4, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    sub-int v6, v18, v19

    .line 232
    :cond_a
    :goto_5
    mul-int v18, v16, v6

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(I)I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    cmpg-float v18, v10, v18

    if-gez v18, :cond_2

    .line 234
    move-object v14, v3

    .line 235
    mul-int v18, v16, v6

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(I)I

    move-result v18

    move/from16 v0, v18

    int-to-float v10, v0

    goto/16 :goto_1

    .line 217
    :cond_b
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 219
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    iget v0, v4, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    sub-int v16, v18, v19

    .line 220
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    iget v0, v4, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    sub-int v6, v18, v19

    .line 221
    goto :goto_5

    .line 222
    :cond_c
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v18

    if-eqz v18, :cond_d

    .line 224
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    iget v0, v4, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    sub-int v16, v18, v19

    .line 225
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    iget v0, v4, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    sub-int v6, v18, v19

    .line 226
    goto :goto_5

    .line 227
    :cond_d
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 229
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    iget v0, v4, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    sub-int v16, v18, v19

    .line 230
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    iget v0, v4, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    sub-int v6, v18, v19

    goto/16 :goto_5

    .line 249
    .end local v3    # "button":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    .end local v4    # "buttonPos":Landroid/graphics/Rect;
    .end local v6    # "h":I
    .end local v16    # "w":I
    .restart local v7    # "i":I
    .restart local v11    # "nullIdx":I
    .restart local v15    # "targetIdx":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 250
    .restart local v3    # "button":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    instance-of v0, v3, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move/from16 v17, v0

    if-eqz v17, :cond_f

    .line 252
    invoke-interface {v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v3, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setMovePosition(Landroid/graphics/Rect;)V

    .line 247
    :goto_6
    add-int/lit8 v7, v7, -0x1

    goto/16 :goto_2

    .line 256
    :cond_f
    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->moveToLeft()V

    goto :goto_6

    .line 262
    .end local v3    # "button":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    .restart local v5    # "changed":Z
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 263
    .restart local v3    # "button":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->startAnimation()V

    .line 264
    const/4 v5, 0x1

    .line 260
    add-int/lit8 v7, v7, -0x1

    goto/16 :goto_3

    .line 283
    .end local v3    # "button":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    .end local v5    # "changed":Z
    .end local v7    # "i":I
    :cond_11
    move v7, v15

    .restart local v7    # "i":I
    :goto_7
    if-le v7, v11, :cond_16

    .line 295
    const/4 v5, 0x0

    .line 296
    .restart local v5    # "changed":Z
    move v7, v15

    :goto_8
    if-le v7, v11, :cond_18

    .line 302
    if-eqz v5, :cond_12

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mDoneBtn:Landroid/view/View;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$9(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/view/View;

    move-result-object v17

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setEnabled(Z)V

    .line 304
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->getLeftButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v8

    .line 305
    .restart local v8    # "leftFromNull":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->getRightButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v12

    .line 306
    .restart local v12    # "rightFromNull":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    if-eqz v8, :cond_13

    .line 307
    invoke-interface {v8, v12}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 308
    :cond_13
    if-eqz v12, :cond_14

    .line 309
    invoke-interface {v12, v8}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 310
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v17

    invoke-interface {v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getLeftButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 312
    invoke-interface {v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getLeftButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v9

    .line 313
    .local v9, "leftFromTarget":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    if-eqz v9, :cond_15

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v9, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 315
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v14, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    goto/16 :goto_4

    .line 285
    .end local v5    # "changed":Z
    .end local v8    # "leftFromNull":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    .end local v9    # "leftFromTarget":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    .end local v12    # "rightFromNull":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 286
    .restart local v3    # "button":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    instance-of v0, v3, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move/from16 v17, v0

    if-eqz v17, :cond_17

    .line 288
    invoke-interface {v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v3, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setMovePosition(Landroid/graphics/Rect;)V

    .line 283
    :goto_9
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_7

    .line 292
    :cond_17
    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->moveToRight()V

    goto :goto_9

    .line 298
    .end local v3    # "button":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    .restart local v5    # "changed":Z
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v17

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 299
    .restart local v3    # "button":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->startAnimation()V

    .line 300
    const/4 v5, 0x1

    .line 296
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_8
.end method

.method public touchUp(Landroid/graphics/Rect;Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;)V
    .locals 4
    .param p1, "pos"    # Landroid/graphics/Rect;
    .param p2, "button"    # Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    .prologue
    const/4 v3, 0x0

    .line 151
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v1

    .line 152
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getTop()I

    move-result v2

    .line 151
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 153
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setCheckBoxVisibility(I)V

    .line 154
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 155
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->copyButtonsNPosition(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 157
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v2

    # invokes: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->changeNearButtonInfo(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V
    invoke-static {v1, v2, p2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$4(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 159
    invoke-virtual {p2, v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setTranslationX(F)V

    .line 160
    invoke-virtual {p2, v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setTranslationY(F)V

    .line 161
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p2, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setLeft(I)V

    .line 162
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setTop(I)V

    .line 163
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->right:I

    invoke-virtual {p2, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setRight(I)V

    .line 164
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p2, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setBottom(I)V

    .line 166
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->setLeftBarColor_Up()V

    .line 169
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/widget/FrameLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 170
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 171
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 172
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v2

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 173
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;->destroy()V

    .line 174
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;)V

    .line 176
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1$1;->this$1:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;->access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;)Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;

    move-result-object v1

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;->setScrollable(Z)V

    .line 178
    :cond_0
    return-void
.end method
