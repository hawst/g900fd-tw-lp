.class Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;
.super Ljava/lang/Object;
.source "ResizeView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->initButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;

    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 181
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 165
    const v0, 0x11301202

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->i:I

    .line 167
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(Landroid/view/View;Z)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->resizeButtonTouch(I)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->calculateInitialValues(Z)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->calculateanimationintertval()V

    .line 171
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->drawRunner:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 172
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->mResizeEffect:Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;)Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ResizeEffect;->startAnimationDone()V

    .line 173
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->refreshDone()V

    .line 174
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView$4;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ResizeView;->invalidateViews()V

    .line 175
    return-void
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 161
    return-void
.end method
