.class public Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;
.super Landroid/app/Activity;
.source "LauncherActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;
    }
.end annotation


# static fields
.field public static final ACTIONBAR_DONE:I = 0x1

.field public static final ACTIONBAR_TAKE:I = 0x0

.field public static final PAGE_MAX_NUM:I = 0x3

.field public static final THUMB_MAX_NUM:I = 0x2d

.field public static final THUMB_MIN_NUM:I = 0xf

.field public static final THUMB_SIZE_H:I = 0x82

.field public static final THUMB_SIZE_W:I = 0xe6


# instance fields
.field private mCaptureName:Ljava/lang/String;

.field private mCurrentFocusIndex:I

.field private mCursor:Landroid/database/Cursor;

.field private mIsMultiSelect:Z

.field private mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

.field public mPageImage:[Landroid/widget/ImageView;

.field public mPageLayout:[Landroid/widget/LinearLayout;

.field private mStartPhotoretouching:Z

.field public mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

.field private mViewPager:Landroid/support/v4/view/ViewPager;

.field private final stringKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 62
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    .line 63
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 64
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    .line 65
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCurrentFocusIndex:I

    .line 66
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCaptureName:Ljava/lang/String;

    .line 67
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageLayout:[Landroid/widget/LinearLayout;

    .line 68
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    .line 69
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    .line 70
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mIsMultiSelect:Z

    .line 71
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mStartPhotoretouching:Z

    .line 72
    const-string v0, "restoreKey"

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->stringKey:Ljava/lang/String;

    .line 60
    return-void
.end method

.method private Release()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 875
    const v3, 0x7f0900e3

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 876
    .local v2, "layout":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_0

    .line 878
    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 879
    const/4 v2, 0x0

    .line 882
    :cond_0
    const v3, 0x7f0900e6

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 883
    .local v0, "bt_open_gallery":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_1

    .line 885
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 886
    const/4 v0, 0x0

    .line 889
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    if-eqz v3, :cond_2

    .line 890
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->destroy()V

    .line 892
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v3, :cond_3

    .line 893
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->removeAllViews()V

    .line 894
    :cond_3
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 896
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v3, 0x2d

    if-lt v1, v3, :cond_4

    .line 908
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    const/4 v4, 0x0

    iput v4, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    .line 909
    return-void

    .line 898
    :cond_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v1

    if-eqz v3, :cond_5

    .line 900
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_5

    .line 902
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 905
    :cond_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aput-object v4, v3, v1

    .line 906
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    aput-object v4, v3, v1

    .line 896
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCurrentFocusIndex:I

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;I)V
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCurrentFocusIndex:I

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    return-void
.end method

.method private checkAvailableExternalMemorySize()V
    .locals 5

    .prologue
    .line 970
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/MemoryStatus;->getAvailableExternalMemorySize()J

    move-result-wide v0

    .line 971
    .local v0, "available_memsize":J
    const-wide/32 v2, 0x6e45000

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 972
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 973
    const-string v3, "share"

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 974
    const-string v3, "not enough memory to launch app"

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 975
    const-string v3, "ok"

    new-instance v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$10;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$10;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 981
    new-instance v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$11;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$11;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 990
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 992
    :cond_0
    return-void
.end method

.method private checkExternalStorageMount()V
    .locals 3

    .prologue
    .line 942
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/MemoryStatus;->GetExternalStorageMount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 944
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 945
    const-string v1, "share"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 946
    const-string v1, "SD card not available"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 947
    const-string v1, "ok"

    new-instance v2, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$8;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$8;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 954
    new-instance v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$9;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 965
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 967
    :cond_0
    return-void
.end method

.method private declared-synchronized getThumbnailNumAndUri()V
    .locals 11

    .prologue
    .line 513
    monitor-enter p0

    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 514
    .local v1, "uriImages":Landroid/net/Uri;
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "mime_type"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "orientation"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string v3, "date_modified"

    aput-object v3, v2, v0

    .line 515
    .local v2, "projection":[Ljava/lang/String;
    const-wide/16 v8, 0x0

    .line 516
    .local v8, "id":J
    const/4 v7, 0x0

    .line 518
    .local v7, "i":I
    if-nez v1, :cond_2

    .line 520
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    const/4 v3, 0x0

    iput v3, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    .line 521
    const/4 v7, 0x0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    array-length v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt v7, v0, :cond_1

    .line 552
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 522
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    const/4 v3, 0x0

    aput-object v3, v0, v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 521
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 527
    :cond_2
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "upper(date_modified) DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 532
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 533
    const/4 v10, 0x0

    .line 535
    .local v10, "idx":I
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_5

    .line 543
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iput v10, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    .line 544
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 513
    .end local v1    # "uriImages":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v7    # "i":I
    .end local v8    # "id":J
    .end local v10    # "idx":I
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 528
    .restart local v1    # "uriImages":Landroid/net/Uri;
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v7    # "i":I
    .restart local v8    # "id":J
    :catch_0
    move-exception v6

    .line 529
    .local v6, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_2

    .line 537
    .end local v6    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v10    # "idx":I
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 542
    :goto_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x2d

    if-ne v10, v0, :cond_3

    goto :goto_3

    .line 539
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 540
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v0, v10

    .line 541
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 548
    .end local v10    # "idx":I
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    const/4 v3, 0x0

    iput v3, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    .line 549
    const/4 v7, 0x0

    :goto_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    array-length v0, v0

    if-ge v7, v0, :cond_0

    .line 550
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    const/4 v3, 0x0

    aput-object v3, v0, v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 549
    add-int/lit8 v7, v7, 0x1

    goto :goto_5
.end method

.method private initActionBarLayout()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 240
    .local v1, "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 241
    .local v0, "ab":Landroid/app/ActionBar;
    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    .line 242
    invoke-direct {v2, v5, v5}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    .line 241
    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 243
    const/16 v2, 0x12

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 244
    return-void
.end method

.method private registerActionBarBtnTest()V
    .locals 4

    .prologue
    .line 365
    const v3, 0x7f090014

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 366
    .local v1, "done_layout":Landroid/widget/LinearLayout;
    const v3, 0x7f090011

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 367
    .local v0, "cancel_layout":Landroid/widget/LinearLayout;
    const v3, 0x7f09000e

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 370
    .local v2, "take_picture_layout":Landroid/widget/LinearLayout;
    invoke-static {p0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 371
    invoke-static {p0, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 372
    invoke-static {p0, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->setHovering(Landroid/content/Context;Landroid/view/View;)V

    .line 375
    new-instance v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$3;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$3;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 381
    new-instance v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$4;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 387
    new-instance v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$5;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$5;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 392
    return-void
.end method

.method private registerButton()V
    .locals 2

    .prologue
    .line 396
    const v1, 0x7f0900e6

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 398
    .local v0, "bt_open_gallery":Landroid/widget/LinearLayout;
    new-instance v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$6;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 420
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->SetEnableBtn(Z)V

    .line 421
    return-void
.end method

.method private setFilePath(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 914
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->DB_DIR:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 915
    .local v3, "saveFile":Ljava/io/File;
    if-nez v3, :cond_0

    .line 916
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->finish()V

    .line 918
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 920
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 921
    .local v1, "list":[Ljava/io/File;
    if-nez v1, :cond_1

    .line 922
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->finish()V

    .line 924
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v1

    if-lt v0, v4, :cond_4

    .line 930
    .end local v0    # "i":I
    .end local v1    # "list":[Ljava/io/File;
    :cond_2
    if-eqz p1, :cond_3

    .line 932
    const-string v4, "restoreKey"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 933
    .local v2, "map":Landroid/os/Bundle;
    if-eqz v2, :cond_3

    .line 935
    const-string v4, "filename"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCaptureName:Ljava/lang/String;

    .line 938
    .end local v2    # "map":Landroid/os/Bundle;
    :cond_3
    return-void

    .line 926
    .restart local v0    # "i":I
    .restart local v1    # "list":[Ljava/io/File;
    :cond_4
    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 924
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private startMainActivity(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 502
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mStartPhotoretouching:Z

    if-nez v1, :cond_0

    .line 504
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 505
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 506
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mStartPhotoretouching:Z

    .line 507
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 509
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private startMainActivity(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "cameraFlag"    # Z
    .param p3, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 450
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mStartPhotoretouching:Z

    if-nez v1, :cond_1

    .line 452
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 453
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "path"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 455
    if-eqz p2, :cond_0

    .line 457
    const-string v1, "camera"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 458
    const-string v1, "filename"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 461
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mStartPhotoretouching:Z

    .line 462
    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 464
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    return-void
.end method


# virtual methods
.method public SetEnableBtn(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    const/4 v3, 0x1

    .line 339
    const v2, 0x7f0900e6

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 340
    .local v0, "bt_open_gallery":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 341
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 342
    :cond_0
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 344
    const v2, 0x7f09000d

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 345
    .local v1, "take_picture_layout":Landroid/widget/LinearLayout;
    if-eqz v1, :cond_1

    .line 346
    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 347
    :cond_1
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 348
    return-void
.end method

.method public checkListModified()V
    .locals 15

    .prologue
    .line 771
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 772
    .local v1, "uriImages":Landroid/net/Uri;
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "mime_type"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "orientation"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string v3, "date_modified"

    aput-object v3, v2, v0

    .line 774
    .local v2, "projection":[Ljava/lang/String;
    const/4 v12, 0x0

    .line 776
    .local v12, "oldListcount":I
    const-wide/16 v10, 0x0

    .line 777
    .local v10, "id":J
    const/4 v8, 0x0

    .line 779
    .local v8, "i":I
    if-nez v1, :cond_1

    .line 871
    :cond_0
    return-void

    .line 784
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 788
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "upper(date_modified) DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 793
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    .line 795
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 797
    const/4 v9, 0x0

    .line 800
    .local v9, "idx":I
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_7

    .line 842
    :cond_3
    :goto_1
    if-ge v9, v12, :cond_4

    .line 844
    move v8, v9

    :goto_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    array-length v0, v0

    if-lt v8, v0, :cond_e

    .line 850
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iput v9, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    .line 860
    .end local v9    # "idx":I
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    if-eqz v0, :cond_0

    .line 862
    const/4 v8, 0x0

    :goto_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    if-ge v8, v0, :cond_0

    .line 864
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    invoke-virtual {v0, v8}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->isSetBitmap(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 866
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v8

    invoke-virtual {v0, v8, v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->setThumbnailBitmap(ILandroid/graphics/Bitmap;)V

    .line 868
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    aget-object v3, v3, v8

    invoke-virtual {v0, v8, v3}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->setImageUri(ILandroid/net/Uri;)V

    .line 862
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 789
    :catch_0
    move-exception v7

    .line 790
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0

    .line 802
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v9    # "idx":I
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 804
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 805
    invoke-static {v1, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 810
    .local v6, "cursorUri":Landroid/net/Uri;
    const/4 v8, 0x0

    :goto_4
    if-lt v8, v12, :cond_a

    .line 817
    :cond_8
    if-ne v8, v12, :cond_d

    .line 819
    add-int/lit8 v8, v12, -0x1

    :goto_5
    if-gt v8, v9, :cond_c

    .line 824
    const/4 v0, 0x1

    invoke-virtual {p0, v9, v10, v11, v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->makeSKIAThumbnail(IJZ)Z

    .line 839
    .end local v6    # "cursorUri":Landroid/net/Uri;
    :cond_9
    :goto_6
    add-int/lit8 v9, v9, 0x1

    .line 840
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x2d

    if-ne v9, v0, :cond_2

    goto :goto_1

    .line 812
    .restart local v6    # "cursorUri":Landroid/net/Uri;
    :cond_a
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    aget-object v0, v0, v8

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    aget-object v0, v0, v8

    invoke-virtual {v6, v0}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    move-result v0

    if-eqz v0, :cond_8

    .line 810
    :cond_b
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 821
    :cond_c
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    add-int/lit8 v3, v8, -0x1

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    aget-object v4, v4, v8

    aput-object v4, v0, v3

    .line 822
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    add-int/lit8 v3, v8, -0x1

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v8

    aput-object v4, v0, v3

    .line 819
    add-int/lit8 v8, v8, -0x1

    goto :goto_5

    .line 831
    :cond_d
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    aget-object v14, v0, v8

    .line 832
    .local v14, "tempUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aget-object v13, v0, v8

    .line 833
    .local v13, "tempBitmap":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    aget-object v3, v3, v9

    aput-object v3, v0, v8

    .line 834
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v9

    aput-object v3, v0, v8

    .line 835
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    aput-object v14, v0, v9

    .line 836
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aput-object v13, v0, v9

    goto :goto_6

    .line 846
    .end local v6    # "cursorUri":Landroid/net/Uri;
    .end local v13    # "tempBitmap":Landroid/graphics/Bitmap;
    .end local v14    # "tempUri":Landroid/net/Uri;
    :cond_e
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    const/4 v3, 0x0

    aput-object v3, v0, v8

    .line 847
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    aput-object v3, v0, v8

    .line 844
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    .line 854
    .end local v9    # "idx":I
    :cond_f
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    const/4 v3, 0x0

    iput v3, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    .line 855
    const/4 v8, 0x0

    :goto_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    array-length v0, v0

    if-ge v8, v0, :cond_5

    .line 856
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    const/4 v3, 0x0

    aput-object v3, v0, v8

    .line 855
    add-int/lit8 v8, v8, 0x1

    goto :goto_7
.end method

.method protected configurationChange()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const v10, 0x7f0900d3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 995
    const/4 v3, 0x0

    .line 996
    .local v3, "numPage":I
    const/4 v4, 0x0

    .line 997
    .local v4, "temp":Landroid/widget/LinearLayout;
    const/4 v1, 0x0

    .line 999
    .local v1, "iconLayout":Landroid/widget/FrameLayout;
    const v5, 0x7f030048

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setContentView(I)V

    .line 1000
    const v5, 0x7f0900e4

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/support/v4/view/ViewPager;

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 1001
    const v5, 0x7f0900e5

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "iconLayout":Landroid/widget/FrameLayout;
    check-cast v1, Landroid/widget/FrameLayout;

    .line 1003
    .restart local v1    # "iconLayout":Landroid/widget/FrameLayout;
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->initActionBarLayout()V

    .line 1004
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setActionBarLayout()V

    .line 1005
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setMultiSelectDropText()V

    .line 1006
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->registerButton()V

    .line 1007
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->registerActionBarBtnTest()V

    .line 1009
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    if-nez v5, :cond_4

    .line 1011
    const/4 v3, 0x0

    .line 1018
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    invoke-virtual {v5, v6}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 1020
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;

    invoke-direct {v2}, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;-><init>()V

    .line 1022
    .local v2, "indicatorListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;
    packed-switch v3, :pswitch_data_0

    .line 1040
    :goto_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v5, v5, v7

    if-eqz v5, :cond_0

    .line 1041
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v5, v5, v7

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1042
    :cond_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v5, v5, v8

    if-eqz v5, :cond_1

    .line 1043
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v5, v5, v8

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1044
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v5, v5, v9

    if-eqz v5, :cond_2

    .line 1045
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v5, v5, v9

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1047
    :cond_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageLayout:[Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;->initIndicatorListener([Landroid/widget/LinearLayout;Landroid/support/v4/view/ViewPager;)V

    .line 1049
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v6, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$12;

    invoke-direct {v6, p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$12;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V

    invoke-virtual {v5, v6}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 1081
    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setIndicator(I)V

    .line 1083
    if-eqz v3, :cond_3

    .line 1084
    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1086
    :cond_3
    const v5, 0x7f09000c

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1087
    .local v0, "button":Landroid/widget/LinearLayout;
    new-instance v5, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$13;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$13;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1110
    return-void

    .line 1015
    .end local v0    # "button":Landroid/widget/LinearLayout;
    .end local v2    # "indicatorListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;
    :cond_4
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    add-int/lit8 v5, v5, 0xf

    add-int/lit8 v5, v5, -0x1

    div-int/lit8 v3, v5, 0xf

    goto :goto_0

    .line 1024
    .restart local v2    # "indicatorListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;
    :pswitch_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030049

    invoke-virtual {v5, v6, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .end local v4    # "temp":Landroid/widget/LinearLayout;
    check-cast v4, Landroid/widget/LinearLayout;

    .line 1025
    .restart local v4    # "temp":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    invoke-virtual {v4, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v6, v7

    goto :goto_1

    .line 1028
    :pswitch_1
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f03004a

    invoke-virtual {v5, v6, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .end local v4    # "temp":Landroid/widget/LinearLayout;
    check-cast v4, Landroid/widget/LinearLayout;

    .line 1029
    .restart local v4    # "temp":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    invoke-virtual {v4, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v6, v7

    .line 1030
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    const v5, 0x7f0900d5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v6, v8

    goto/16 :goto_1

    .line 1033
    :pswitch_2
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f03004b

    invoke-virtual {v5, v6, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .end local v4    # "temp":Landroid/widget/LinearLayout;
    check-cast v4, Landroid/widget/LinearLayout;

    .line 1034
    .restart local v4    # "temp":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    invoke-virtual {v4, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v6, v7

    .line 1035
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    const v5, 0x7f0900d5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v6, v8

    .line 1036
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    const v5, 0x7f0900d7

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v6, v9

    goto/16 :goto_1

    .line 1022
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public declared-synchronized getThumbnail(II)V
    .locals 9
    .param p1, "startNum"    # I
    .param p2, "endNum"    # I

    .prologue
    .line 555
    monitor-enter p0

    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 556
    .local v1, "uriImages":Landroid/net/Uri;
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "mime_type"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "orientation"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string v3, "date_modified"

    aput-object v3, v2, v0

    .line 558
    .local v2, "projection":[Ljava/lang/String;
    if-nez v1, :cond_2

    .line 560
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    const/4 v3, 0x0

    iput v3, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    .line 561
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    array-length v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt v8, v0, :cond_1

    .line 627
    .end local v8    # "i":I
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 562
    .restart local v8    # "i":I
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    const/4 v3, 0x0

    aput-object v3, v0, v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 561
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 567
    .end local v8    # "i":I
    :cond_2
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "upper(date_modified) DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 573
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 575
    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    .line 576
    .local v7, "handler":Landroid/os/Handler;
    new-instance v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$7;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$7;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;I)V

    invoke-virtual {v7, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 555
    .end local v1    # "uriImages":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v7    # "handler":Landroid/os/Handler;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 569
    .restart local v1    # "uriImages":Landroid/net/Uri;
    .restart local v2    # "projection":[Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 570
    .local v6, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_2

    .line 621
    .end local v6    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_3
    move v8, p1

    .restart local v8    # "i":I
    :goto_3
    const/16 v0, 0x2d

    if-ge v8, v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v8

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 623
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 624
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    aput-object v3, v0, v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 621
    add-int/lit8 v8, v8, 0x1

    goto :goto_3
.end method

.method public getThumbnailBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 757
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 758
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, p1

    .line 759
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getThumbnailUri(I)Landroid/net/Uri;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 764
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 765
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    aget-object v0, v0, p1

    .line 766
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initVariable()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 99
    new-array v0, v1, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    .line 100
    new-array v0, v1, [Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageLayout:[Landroid/widget/LinearLayout;

    .line 101
    new-instance v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    .line 102
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCurrentFocusIndex:I

    .line 103
    return-void
.end method

.method protected initView(Z)V
    .locals 13
    .param p1, "refreshThumbnail"    # Z

    .prologue
    const v12, 0x7f0900d4

    const v11, 0x7f0900d3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 107
    const/4 v3, 0x0

    .line 108
    .local v3, "numPage":I
    const/4 v4, 0x0

    .line 109
    .local v4, "temp":Landroid/widget/LinearLayout;
    const/4 v1, 0x0

    .line 111
    .local v1, "iconLayout":Landroid/widget/FrameLayout;
    const v5, 0x7f030048

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setContentView(I)V

    .line 112
    const v5, 0x7f0900e4

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/support/v4/view/ViewPager;

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 113
    const v5, 0x7f0900e5

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "iconLayout":Landroid/widget/FrameLayout;
    check-cast v1, Landroid/widget/FrameLayout;

    .line 115
    .restart local v1    # "iconLayout":Landroid/widget/FrameLayout;
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->initActionBarLayout()V

    .line 116
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setActionBarLayout(I)V

    .line 117
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->registerButton()V

    .line 118
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->registerActionBarBtnTest()V

    .line 119
    if-eqz p1, :cond_0

    .line 120
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getThumbnailNumAndUri()V

    .line 122
    :cond_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    if-nez v5, :cond_5

    .line 124
    const/4 v3, 0x0

    .line 131
    :goto_0
    new-instance v5, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget v6, v6, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    invoke-direct {v5, p0, v6, p1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;IZ)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    .line 132
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    invoke-virtual {v5, v6}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 134
    packed-switch v3, :pswitch_data_0

    .line 159
    :goto_1
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;

    invoke-direct {v2}, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;-><init>()V

    .line 161
    .local v2, "indicatorListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v5, v5, v8

    if-eqz v5, :cond_1

    .line 162
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v5, v5, v8

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v5, v5, v9

    if-eqz v5, :cond_2

    .line 164
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v5, v5, v9

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    :cond_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v5, v5, v10

    if-eqz v5, :cond_3

    .line 166
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v5, v5, v10

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    :cond_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageLayout:[Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;->initIndicatorListener([Landroid/widget/LinearLayout;Landroid/support/v4/view/ViewPager;)V

    .line 170
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v6, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$1;

    invoke-direct {v6, p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$1;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;)V

    invoke-virtual {v5, v6}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 202
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setIndicator(I)V

    .line 204
    if-eqz v3, :cond_4

    .line 205
    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 207
    :cond_4
    const v5, 0x7f09000c

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 208
    .local v0, "button":Landroid/widget/LinearLayout;
    new-instance v5, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;

    invoke-direct {v5, p0, v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$2;-><init>(Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;Landroid/widget/LinearLayout;)V

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    return-void

    .line 128
    .end local v0    # "button":Landroid/widget/LinearLayout;
    .end local v2    # "indicatorListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;
    :cond_5
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->count:I

    add-int/lit8 v5, v5, 0xf

    add-int/lit8 v5, v5, -0x1

    div-int/lit8 v3, v5, 0xf

    goto :goto_0

    .line 136
    :pswitch_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030049

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .end local v4    # "temp":Landroid/widget/LinearLayout;
    check-cast v4, Landroid/widget/LinearLayout;

    .line 137
    .restart local v4    # "temp":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageLayout:[Landroid/widget/LinearLayout;

    invoke-virtual {v4, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    aput-object v5, v6, v8

    .line 138
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    invoke-virtual {v4, v12}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v6, v8

    goto/16 :goto_1

    .line 141
    :pswitch_1
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f03004a

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .end local v4    # "temp":Landroid/widget/LinearLayout;
    check-cast v4, Landroid/widget/LinearLayout;

    .line 142
    .restart local v4    # "temp":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageLayout:[Landroid/widget/LinearLayout;

    invoke-virtual {v4, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    aput-object v5, v6, v8

    .line 143
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageLayout:[Landroid/widget/LinearLayout;

    const v5, 0x7f0900d5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    aput-object v5, v6, v9

    .line 144
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    invoke-virtual {v4, v12}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v6, v8

    .line 145
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    const v5, 0x7f0900d6

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v6, v9

    goto/16 :goto_1

    .line 148
    :pswitch_2
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f03004b

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .end local v4    # "temp":Landroid/widget/LinearLayout;
    check-cast v4, Landroid/widget/LinearLayout;

    .line 150
    .restart local v4    # "temp":Landroid/widget/LinearLayout;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageLayout:[Landroid/widget/LinearLayout;

    invoke-virtual {v4, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    aput-object v5, v6, v8

    .line 151
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageLayout:[Landroid/widget/LinearLayout;

    const v5, 0x7f0900d5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    aput-object v5, v6, v9

    .line 152
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageLayout:[Landroid/widget/LinearLayout;

    const v5, 0x7f0900d7

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    aput-object v5, v6, v10

    .line 153
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    invoke-virtual {v4, v12}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v6, v8

    .line 154
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    const v5, 0x7f0900d6

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v6, v9

    .line 155
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    const v5, 0x7f0900d8

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    aput-object v5, v6, v10

    goto/16 :goto_1

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public makeSKIAThumbnail(IJZ)Z
    .locals 32
    .param p1, "idx"    # I
    .param p2, "id"    # J
    .param p4, "flag"    # Z

    .prologue
    .line 630
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, p2

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v25

    .line 631
    .local v25, "uri":Landroid/net/Uri;
    if-nez v25, :cond_0

    .line 632
    const/4 v3, 0x0

    .line 752
    :goto_0
    return v3

    .line 633
    :cond_0
    new-instance v20, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 634
    .local v20, "optsResolution":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    move-object/from16 v0, v20

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 635
    const/16 v24, 0x0

    .line 637
    .local v24, "stream":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v24

    .line 642
    const/4 v3, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-static {v0, v3, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 644
    :try_start_1
    invoke-virtual/range {v24 .. v24}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 649
    move-object/from16 v0, v20

    iget v5, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 650
    .local v5, "imageWidth":I
    move-object/from16 v0, v20

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 652
    .local v6, "imageHeight":I
    mul-int v3, v5, v6

    int-to-float v3, v3

    const v4, 0x46e99800    # 29900.0f

    div-float/2addr v3, v4

    float-to-double v0, v3

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-int v0, v0

    move/from16 v22, v0

    .line 653
    .local v22, "sampleSize":I
    if-nez v22, :cond_1

    .line 654
    const/16 v22, 0x1

    .line 655
    :cond_1
    const/4 v3, 0x0

    move-object/from16 v0, v20

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 656
    move/from16 v0, v22

    move-object/from16 v1, v20

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 658
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v24

    .line 663
    const/4 v3, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-static {v0, v3, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 664
    .local v2, "temp":Landroid/graphics/Bitmap;
    if-nez v2, :cond_2

    .line 665
    const/4 v3, 0x0

    goto :goto_0

    .line 638
    .end local v2    # "temp":Landroid/graphics/Bitmap;
    .end local v5    # "imageWidth":I
    .end local v6    # "imageHeight":I
    .end local v22    # "sampleSize":I
    :catch_0
    move-exception v18

    .line 639
    .local v18, "e2":Ljava/io/FileNotFoundException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 640
    const/4 v3, 0x0

    goto :goto_0

    .line 645
    .end local v18    # "e2":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v17

    .line 646
    .local v17, "e1":Ljava/io/IOException;
    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->printStackTrace()V

    .line 647
    const/4 v3, 0x0

    goto :goto_0

    .line 659
    .end local v17    # "e1":Ljava/io/IOException;
    .restart local v5    # "imageWidth":I
    .restart local v6    # "imageHeight":I
    .restart local v22    # "sampleSize":I
    :catch_2
    move-exception v16

    .line 660
    .local v16, "e":Ljava/io/FileNotFoundException;
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 661
    const/4 v3, 0x0

    goto :goto_0

    .line 667
    .end local v16    # "e":Ljava/io/FileNotFoundException;
    .restart local v2    # "temp":Landroid/graphics/Bitmap;
    :cond_2
    :try_start_3
    invoke-virtual/range {v24 .. v24}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 672
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 673
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 675
    if-lez v5, :cond_3

    if-gtz v6, :cond_4

    .line 676
    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 668
    :catch_3
    move-exception v16

    .line 669
    .local v16, "e":Ljava/io/IOException;
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    .line 670
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 678
    .end local v16    # "e":Ljava/io/IOException;
    :cond_4
    const/16 v21, -0x1

    .line 679
    .local v21, "rotate":I
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 680
    .local v7, "m":Landroid/graphics/Matrix;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-static {v3, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotateDegree(Landroid/content/ContentResolver;Landroid/net/Uri;)I

    move-result v21

    .line 681
    if-eqz v21, :cond_5

    .line 683
    const/16 v3, 0x5a

    move/from16 v0, v21

    if-ne v0, v3, :cond_9

    .line 684
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 696
    :cond_5
    :goto_1
    if-eqz v21, :cond_6

    .line 697
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 698
    .local v15, "b":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 699
    move-object v2, v15

    .line 701
    .end local v15    # "b":Landroid/graphics/Bitmap;
    :cond_6
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 702
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 703
    const v26, 0x3fe27627

    .line 704
    .local v26, "viewRatio":F
    int-to-float v3, v5

    int-to-float v4, v6

    div-float v19, v3, v4

    .line 707
    .local v19, "imgRatio":F
    const/4 v9, 0x0

    .local v9, "startX":I
    const/4 v10, 0x0

    .line 710
    .local v10, "startY":I
    move v12, v6

    .local v12, "resizeHgt":I
    move v11, v5

    .line 711
    .local v11, "resizeWdt":I
    cmpl-float v3, v26, v19

    if-lez v3, :cond_c

    .line 712
    mul-int/lit16 v3, v5, 0x82

    int-to-float v3, v3

    const/high16 v4, 0x43660000    # 230.0f

    div-float/2addr v3, v4

    float-to-int v12, v3

    .line 713
    sub-int v3, v6, v12

    div-int/lit8 v10, v3, 0x2

    .line 714
    int-to-float v3, v5

    const/high16 v4, 0x43660000    # 230.0f

    div-float v23, v3, v4

    .line 721
    .local v23, "scale":F
    :goto_2
    if-eqz v11, :cond_7

    if-nez v12, :cond_d

    .line 722
    :cond_7
    if-eqz v2, :cond_8

    .line 723
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 724
    const/4 v2, 0x0

    .line 726
    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 686
    .end local v9    # "startX":I
    .end local v10    # "startY":I
    .end local v11    # "resizeWdt":I
    .end local v12    # "resizeHgt":I
    .end local v19    # "imgRatio":F
    .end local v23    # "scale":F
    .end local v26    # "viewRatio":F
    :cond_9
    const/16 v3, 0xb4

    move/from16 v0, v21

    if-ne v0, v3, :cond_a

    .line 687
    const/high16 v3, 0x43340000    # 180.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_1

    .line 689
    :cond_a
    const/16 v3, 0x10e

    move/from16 v0, v21

    if-ne v0, v3, :cond_b

    .line 690
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_1

    .line 694
    :cond_b
    const/16 v21, 0x0

    goto :goto_1

    .line 717
    .restart local v9    # "startX":I
    .restart local v10    # "startY":I
    .restart local v11    # "resizeWdt":I
    .restart local v12    # "resizeHgt":I
    .restart local v19    # "imgRatio":F
    .restart local v26    # "viewRatio":F
    :cond_c
    int-to-float v3, v6

    mul-float v3, v3, v26

    float-to-int v11, v3

    .line 718
    sub-int v3, v5, v11

    div-int/lit8 v9, v3, 0x2

    .line 719
    int-to-float v3, v6

    const/high16 v4, 0x43020000    # 130.0f

    div-float v23, v3, v4

    .restart local v23    # "scale":F
    goto :goto_2

    .line 728
    :cond_d
    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v28, v0

    const-wide/high16 v30, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v28, v30

    if-lez v3, :cond_10

    .line 729
    new-instance v13, Landroid/graphics/Matrix;

    invoke-direct {v13}, Landroid/graphics/Matrix;-><init>()V

    .line 730
    .local v13, "mm":Landroid/graphics/Matrix;
    const/high16 v3, 0x3f800000    # 1.0f

    div-float v3, v3, v23

    const/high16 v4, 0x3f800000    # 1.0f

    div-float v4, v4, v23

    invoke-virtual {v13, v3, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 731
    const/4 v14, 0x0

    move-object v8, v2

    invoke-static/range {v8 .. v14}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 732
    .restart local v15    # "b":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 733
    const/4 v2, 0x0

    .line 734
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aput-object v15, v3, p1

    .line 742
    .end local v13    # "mm":Landroid/graphics/Matrix;
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->uri:[Landroid/net/Uri;

    aput-object v25, v3, p1

    .line 744
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    if-eqz v3, :cond_f

    .line 746
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->isSetBitmap(I)Z

    move-result v3

    if-nez v3, :cond_e

    .line 748
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, p1

    move/from16 v0, p1

    invoke-virtual {v3, v0, v4}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->setThumbnailBitmap(ILandroid/graphics/Bitmap;)V

    .line 750
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    move/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->setImageUri(ILandroid/net/Uri;)V

    .line 752
    :cond_f
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 737
    .end local v15    # "b":Landroid/graphics/Bitmap;
    :cond_10
    invoke-static {v2, v9, v10, v11, v12}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 738
    .restart local v15    # "b":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 739
    const/4 v2, 0x0

    .line 740
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mThumbData:Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity$ThumbnailData;->bitmap:[Landroid/graphics/Bitmap;

    aput-object v15, v3, p1

    goto :goto_3
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 1154
    const/4 v6, -0x1

    if-ne p2, v6, :cond_4

    .line 1156
    sparse-switch p1, :sswitch_data_0

    .line 1224
    :goto_0
    :sswitch_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1225
    :cond_0
    return-void

    .line 1159
    :sswitch_1
    if-eqz p3, :cond_3

    .line 1161
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 1163
    .local v4, "str":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 1165
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1166
    .local v3, "lfile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1168
    const-string v6, "cannot open image"

    invoke-static {p0, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 1169
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->SetEnableBtn(Z)V

    goto :goto_0

    .line 1173
    :cond_1
    const/4 v6, 0x0

    invoke-direct {p0, v4, v7, v6}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->startMainActivity(Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0

    .line 1178
    .end local v3    # "lfile":Ljava/io/File;
    :cond_2
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    .line 1179
    .local v5, "uri":Landroid/net/Uri;
    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->startMainActivity(Landroid/net/Uri;)V

    .line 1180
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->SetEnableBtn(Z)V

    goto :goto_0

    .line 1185
    .end local v4    # "str":Ljava/lang/String;
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_3
    const-string v6, "cannot open image"

    invoke-static {p0, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 1186
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->SetEnableBtn(Z)V

    goto :goto_0

    .line 1190
    :sswitch_2
    new-instance v6, Ljava/lang/StringBuilder;

    sget-object v7, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCaptureName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCaptureName:Ljava/lang/String;

    invoke-direct {p0, v6, v8, v7}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->startMainActivity(Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0

    .line 1196
    :sswitch_3
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1197
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v6, "selectedCount"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1198
    .local v1, "count":I
    if-eqz v1, :cond_0

    .line 1210
    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.sec.android.mimage.photoretouching.multigrid"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1213
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1214
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1220
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "count":I
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->checkListModified()V

    .line 1221
    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->initView(Z)V

    goto :goto_0

    .line 1156
    nop

    :sswitch_data_0
    .sparse-switch
        0x20000 -> :sswitch_1
        0x30000 -> :sswitch_2
        0x60000 -> :sswitch_0
        0x70000 -> :sswitch_3
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 1230
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 1231
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->finish()V

    .line 1232
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1114
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 1116
    .local v0, "currentNum":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    .line 1117
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->removeAllViews()V

    .line 1119
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 1120
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->configurationChange()V

    .line 1122
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1124
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    if-eqz v1, :cond_1

    .line 1126
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCurrentFocusIndex:I

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->getImageData(I)Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1128
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCurrentFocusIndex:I

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->getImageData(I)Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1

    .line 1130
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mCurrentFocusIndex:I

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->getImageData(I)Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter$ImageViewData;->frameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->requestFocus()Z

    .line 1134
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1135
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v1, 0x400

    .line 85
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 87
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->setFilePath(Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->initVariable()V

    .line 90
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->initView(Z)V

    .line 92
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->checkExternalStorageMount()V

    .line 93
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->checkAvailableExternalMemorySize()V

    .line 94
    const/high16 v0, 0x2d000000

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentMode(I)V

    .line 95
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 1237
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->Release()V

    .line 1238
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1239
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1240
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1245
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1252
    const v2, 0x7f09000d

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1253
    .local v1, "take_picture_layout":Landroid/widget/LinearLayout;
    const v2, 0x7f0900e6

    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1255
    .local v0, "bt_open_gallery":Landroid/widget/LinearLayout;
    packed-switch p1, :pswitch_data_0

    .line 1267
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    return v2

    .line 1258
    :pswitch_0
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1259
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->openFromCamera()V

    goto :goto_0

    .line 1260
    :cond_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1261
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->openFromGallery()V

    goto :goto_0

    .line 1264
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->onKeyEnter()V

    goto :goto_0

    .line 1255
    :pswitch_data_0
    .packed-switch 0x42
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 1273
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1274
    return-void
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 1279
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 1280
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1140
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1141
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mStartPhotoretouching:Z

    .line 1142
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getSavedAndDestroy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1143
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f060073

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 1144
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setSaveAndDestroy(Z)V

    .line 1146
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->checkListModified()V

    .line 1147
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->initView(Z)V

    .line 1148
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setStartNormalView(Z)V

    .line 1149
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 1285
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 1286
    return-void
.end method

.method protected openFromCamera()V
    .locals 0

    .prologue
    .line 445
    invoke-static {p0}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->sendMultigrid(Landroid/content/Context;)V

    .line 446
    return-void
.end method

.method protected openFromGallery()V
    .locals 3

    .prologue
    .line 425
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 426
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.PICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 427
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 428
    const-string v1, "Open"

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x20000

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 429
    const/4 v0, 0x0

    .line 430
    return-void
.end method

.method public setActionBarLayout()V
    .locals 11

    .prologue
    const/16 v9, 0x8

    const/4 v10, 0x0

    .line 278
    const v8, 0x7f09000d

    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 279
    .local v5, "take_picture_layout":Landroid/widget/LinearLayout;
    const v8, 0x7f090010

    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 280
    .local v1, "done_cancel_layout":Landroid/widget/LinearLayout;
    const v8, 0x7f09000c

    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 281
    .local v2, "drop_layout":Landroid/widget/LinearLayout;
    const v8, 0x7f09000b

    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 282
    .local v7, "title_layout":Landroid/widget/TextView;
    iget-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mIsMultiSelect:Z

    if-eqz v8, :cond_1

    .line 284
    invoke-virtual {v1, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 285
    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 286
    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 287
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 288
    const v8, 0x7f090005

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 290
    .local v6, "textView":Landroid/widget/TextView;
    const v8, 0x7f060101

    invoke-static {p0, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    .line 291
    .local v4, "splitString":Ljava/lang/String;
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->getMultiSelectCount()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v4, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 292
    .local v3, "selected":Ljava/lang/String;
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->getMultiSelectCount()I

    move-result v8

    if-nez v8, :cond_0

    .line 297
    const v8, 0x7f090014

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 298
    .local v0, "done":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 309
    .end local v0    # "done":Landroid/widget/LinearLayout;
    .end local v3    # "selected":Ljava/lang/String;
    .end local v4    # "splitString":Ljava/lang/String;
    .end local v6    # "textView":Landroid/widget/TextView;
    :cond_0
    :goto_0
    return-void

    .line 304
    :cond_1
    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 305
    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 306
    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 307
    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setActionBarLayout(I)V
    .locals 11
    .param p1, "mode"    # I

    .prologue
    const/4 v10, 0x1

    const/16 v8, 0x8

    const/4 v9, 0x0

    .line 248
    const v7, 0x7f09000d

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 249
    .local v4, "take_picture_layout":Landroid/widget/LinearLayout;
    const v7, 0x7f090010

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 250
    .local v0, "done_cancel_layout":Landroid/widget/LinearLayout;
    const v7, 0x7f09000c

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 251
    .local v1, "drop_layout":Landroid/widget/LinearLayout;
    const v7, 0x7f09000b

    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 253
    .local v6, "title_layout":Landroid/widget/TextView;
    if-ne p1, v10, :cond_1

    .line 255
    iput-boolean v10, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mIsMultiSelect:Z

    .line 256
    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 257
    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 258
    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 259
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 260
    const v7, 0x7f090005

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 262
    .local v5, "textView":Landroid/widget/TextView;
    const v7, 0x7f060101

    invoke-static {p0, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 263
    .local v3, "splitString":Ljava/lang/String;
    new-array v7, v10, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->getMultiSelectCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 264
    .local v2, "selected":Ljava/lang/String;
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    .end local v2    # "selected":Ljava/lang/String;
    .end local v3    # "splitString":Ljava/lang/String;
    .end local v5    # "textView":Landroid/widget/TextView;
    :cond_0
    :goto_0
    return-void

    .line 267
    :cond_1
    if-nez p1, :cond_0

    .line 269
    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mIsMultiSelect:Z

    .line 270
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 271
    invoke-virtual {v4, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 272
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 273
    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setIndicator(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 353
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    .line 359
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v1, v1, p1

    if-eqz v1, :cond_0

    .line 360
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v1, v1, p1

    const v2, 0x7f020354

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 361
    :cond_0
    return-void

    .line 355
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 356
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mPageImage:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    const v2, 0x7f020353

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 353
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setMultiSelectDropText()V
    .locals 12

    .prologue
    const v11, 0x7f090014

    const v10, 0x7f090010

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 313
    const v6, 0x7f09000c

    invoke-virtual {p0, v6}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 314
    .local v2, "drop_layout":Landroid/widget/LinearLayout;
    const v6, 0x7f090005

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 316
    .local v5, "textView":Landroid/widget/TextView;
    const v6, 0x7f060101

    invoke-static {p0, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    .line 317
    .local v4, "splitString":Ljava/lang/String;
    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->getMultiSelectCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 318
    .local v3, "selected":Ljava/lang/String;
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 320
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mLauncherAdapter:Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherAdapter;->getMultiSelectCount()I

    move-result v6

    if-nez v6, :cond_1

    .line 322
    invoke-virtual {p0, v10}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 323
    .local v1, "done_cancel_layout":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 324
    .local v0, "done":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 328
    .end local v0    # "done":Landroid/widget/LinearLayout;
    .end local v1    # "done_cancel_layout":Landroid/widget/LinearLayout;
    :cond_1
    invoke-virtual {p0, v10}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 329
    .restart local v1    # "done_cancel_layout":Landroid/widget/LinearLayout;
    invoke-virtual {v1, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 331
    .restart local v0    # "done":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v6

    if-nez v6, :cond_0

    .line 333
    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method public startMainActivity(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 469
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->mStartPhotoretouching:Z

    if-nez v5, :cond_0

    .line 471
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 472
    .local v2, "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 474
    .local v4, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v0, v5, :cond_1

    .line 488
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 489
    const/4 p1, 0x0

    .line 491
    new-instance v1, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-direct {v1, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 492
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "selectedItems"

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 493
    const-string v5, "selectedCount"

    const/4 v6, 0x1

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 494
    const-string v5, "android.intent.action.EDIT"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 496
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/launcher/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 498
    .end local v0    # "i":I
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_0
    return-void

    .line 476
    .restart local v0    # "i":I
    .restart local v2    # "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v4    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_1
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 485
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 474
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
