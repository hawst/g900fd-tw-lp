.class Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;
.super Ljava/lang/Object;
.source "MirrorAnimationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimationInfo"
.end annotation


# instance fields
.field private mAngle:F

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mDrawRoiForPreview:Landroid/graphics/RectF;

.field private mDuration:J

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mMirrorRoi:Landroid/graphics/RectF;

.field private mStartTime:J

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 202
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    iput-wide v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mDuration:J

    .line 205
    iput-wide v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mStartTime:J

    .line 206
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mAngle:F

    .line 207
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mBitmap:Landroid/graphics/Bitmap;

    .line 208
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mDrawRoiForPreview:Landroid/graphics/RectF;

    .line 209
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mMirrorRoi:Landroid/graphics/RectF;

    .line 210
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)J
    .locals 2

    .prologue
    .line 205
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)J
    .locals 2

    .prologue
    .line 204
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mDuration:J

    return-wide v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mInterpolator:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mDrawRoiForPreview:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mMirrorRoi:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)F
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mAngle:F

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method public free()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 250
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mBitmap:Landroid/graphics/Bitmap;

    .line 226
    return-void
.end method

.method public setDrawRoi(Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "roi"    # Landroid/graphics/RectF;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mDrawRoiForPreview:Landroid/graphics/RectF;

    .line 230
    return-void
.end method

.method public setDuration(I)V
    .locals 2
    .param p1, "duration"    # I

    .prologue
    .line 237
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mDuration:J

    .line 238
    return-void
.end method

.method public setInterpolator()V
    .locals 1

    .prologue
    .line 241
    new-instance v0, Lcom/sec/android/easing/QuintOut80;

    invoke-direct {v0}, Lcom/sec/android/easing/QuintOut80;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 242
    return-void
.end method

.method public setMirrorRoi(Landroid/graphics/RectF;)V
    .locals 0
    .param p1, "mirrorRoi"    # Landroid/graphics/RectF;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mMirrorRoi:Landroid/graphics/RectF;

    .line 234
    return-void
.end method

.method public setMirrorSide(I)V
    .locals 1
    .param p1, "mirrorSide"    # I

    .prologue
    .line 214
    const v0, 0xe004

    if-ne p1, v0, :cond_0

    .line 216
    const/high16 v0, -0x3ccc0000    # -180.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mAngle:F

    .line 222
    :goto_0
    return-void

    .line 220
    :cond_0
    const/high16 v0, 0x43340000    # 180.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mAngle:F

    goto :goto_0
.end method

.method public setStartTime()V
    .locals 2

    .prologue
    .line 245
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$AnimationInfo;->mStartTime:J

    .line 246
    return-void
.end method
