.class public Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$TabFactory;
.super Ljava/lang/Object;
.source "DecorationMenuLayoutManager.java"

# interfaces
.implements Landroid/widget/TabHost$TabContentFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TabFactory"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 335
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$TabFactory;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$TabFactory;->mContext:Landroid/content/Context;

    .line 337
    return-void
.end method


# virtual methods
.method public createTabContent(Ljava/lang/String;)Landroid/view/View;
    .locals 3
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 339
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$TabFactory;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 340
    .local v0, "v":Landroid/view/View;
    invoke-virtual {v0, v2}, Landroid/view/View;->setMinimumWidth(I)V

    .line 341
    invoke-virtual {v0, v2}, Landroid/view/View;->setMinimumHeight(I)V

    .line 342
    return-object v0
.end method
