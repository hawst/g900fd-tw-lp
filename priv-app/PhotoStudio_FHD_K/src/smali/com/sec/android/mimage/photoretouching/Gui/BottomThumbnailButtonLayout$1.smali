.class Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;
.super Ljava/lang/Object;
.source "BottomThumbnailButtonLayout.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$AsyncCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->applyEffectThumbnailAsync(Landroid/widget/ImageView;[III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

.field private final synthetic val$height:I

.field private final synthetic val$icon:Landroid/widget/ImageView;

.field private final synthetic val$in:[I

.field private final synthetic val$width:I


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;Landroid/widget/ImageView;II[I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$icon:Landroid/widget/ImageView;

    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    iput p4, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public donInBackground()Landroid/graphics/Bitmap;
    .locals 22

    .prologue
    .line 278
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    if-nez v2, :cond_1

    .line 279
    :cond_0
    const/4 v13, 0x0

    .line 449
    :goto_0
    return-object v13

    .line 280
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    mul-int/2addr v2, v5

    new-array v4, v2, [I

    .line 281
    .local v4, "out":[I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    mul-int/2addr v2, v5

    new-array v3, v2, [B

    .line 282
    .local v3, "mask":[B
    const/4 v11, 0x0

    .line 283
    .local v11, "texture":[I
    const/16 v21, 0x0

    .line 285
    .local v21, "texture_1":[I
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 286
    .local v8, "roi":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v8, v2, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 287
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mButtonType:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_1
    :pswitch_0
    move-object/from16 v12, v21

    .line 426
    .end local v21    # "texture_1":[I
    .local v12, "texture_1":[I
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move/from16 v19, v0

    .line 427
    .local v19, "bitmapWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    move/from16 v20, v0

    .line 428
    .local v20, "bitmapHeight":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    if-le v2, v5, :cond_22

    .line 429
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    move/from16 v19, v0

    .line 432
    :goto_3
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 433
    .local v13, "result":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    sub-int v2, v2, v20

    div-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    mul-int/2addr v2, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    sub-int v5, v5, v19

    div-int/lit8 v5, v5, 0x2

    add-int v15, v2, v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v14, v4

    invoke-virtual/range {v13 .. v20}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 444
    const/4 v4, 0x0

    .line 445
    const/4 v3, 0x0

    .line 446
    const/4 v8, 0x0

    .line 447
    const/4 v11, 0x0

    .line 448
    const/4 v12, 0x0

    .line 449
    goto/16 :goto_0

    .line 290
    .end local v12    # "texture_1":[I
    .end local v13    # "result":Landroid/graphics/Bitmap;
    .end local v19    # "bitmapWidth":I
    .end local v20    # "bitmapHeight":I
    .restart local v21    # "texture_1":[I
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyVintage([I[III)V

    move-object/from16 v12, v21

    .line 291
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto :goto_2

    .line 293
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyPopArt([I[III)V

    move-object/from16 v12, v21

    .line 294
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto :goto_2

    .line 296
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    const/16 v7, 0xa

    invoke-static/range {v2 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlGreyscale([I[B[IIIILandroid/graphics/Rect;)V

    move-object/from16 v12, v21

    .line 297
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 299
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySepia([I[III)I

    move-object/from16 v12, v21

    .line 300
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 302
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    const/4 v7, 0x4

    invoke-static {v2, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySharpen([I[IIII)I

    move-object/from16 v12, v21

    .line 303
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 305
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    const/16 v7, 0x32

    invoke-static {v2, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySoftglow([I[IIII)V

    move-object/from16 v12, v21

    .line 306
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 308
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyRainbow([I[III)V

    move-object/from16 v12, v21

    .line 309
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 311
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    if-nez v2, :cond_2

    .line 312
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 313
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    array-length v2, v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    mul-int/2addr v5, v6

    if-eq v2, v5, :cond_4

    .line 314
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    const v5, 0x16001607

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-virtual {v2, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getTexture(III)[I

    move-result-object v11

    .line 317
    :goto_4
    if-nez v11, :cond_5

    .line 318
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 316
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v11

    goto :goto_4

    .line 319
    :cond_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    move-object v10, v4

    move-object v12, v3

    move-object v15, v8

    invoke-static/range {v9 .. v15}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyStardustPartial([I[I[I[BIILandroid/graphics/Rect;)V

    move-object/from16 v12, v21

    .line 320
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 322
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    if-nez v2, :cond_6

    .line 323
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 324
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    array-length v2, v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    mul-int/2addr v5, v6

    if-eq v2, v5, :cond_8

    .line 325
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    const v5, 0x16001608

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-virtual {v2, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getTexture(III)[I

    move-result-object v11

    .line 328
    :goto_5
    if-nez v11, :cond_9

    .line 329
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 327
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v11

    goto :goto_5

    .line 330
    :cond_9
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    move-object v10, v4

    move-object v12, v3

    move-object v15, v8

    invoke-static/range {v9 .. v15}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyLightFlarePartial([I[I[I[BIILandroid/graphics/Rect;)V

    move-object/from16 v12, v21

    .line 331
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 333
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    if-nez v2, :cond_a

    .line 334
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 335
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    array-length v2, v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    mul-int/2addr v5, v6

    if-eq v2, v5, :cond_c

    .line 336
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    const v5, 0x16001609

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-virtual {v2, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getTexture(III)[I

    move-result-object v11

    .line 339
    :goto_6
    if-nez v11, :cond_d

    .line 340
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 338
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v11

    goto :goto_6

    .line 341
    :cond_d
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    move-object v10, v4

    move-object v12, v3

    move-object v15, v8

    invoke-static/range {v9 .. v15}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyLightStreakPartial([I[I[I[BIILandroid/graphics/Rect;)V

    move-object/from16 v12, v21

    .line 342
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 344
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    if-nez v2, :cond_e

    .line 345
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 346
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    array-length v2, v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    mul-int/2addr v5, v6

    if-eq v2, v5, :cond_10

    .line 347
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    const v5, 0x16001612

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-virtual {v2, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getTexture(III)[I

    move-result-object v11

    .line 350
    :goto_7
    if-nez v11, :cond_11

    .line 351
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 349
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v11

    goto :goto_7

    .line 352
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-static {v2, v4, v11, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySketchTexture([I[I[III)I

    move-object/from16 v12, v21

    .line 353
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 355
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    move-object v7, v8

    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyInvert([I[B[IIILandroid/graphics/Rect;)V

    move-object/from16 v12, v21

    .line 356
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 358
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyCartoonize([I[III)I

    move-object/from16 v12, v21

    .line 359
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 361
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    if-nez v2, :cond_12

    .line 362
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 363
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    array-length v2, v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    mul-int/2addr v5, v6

    if-eq v2, v5, :cond_14

    .line 364
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    const v5, 0x1600160f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-virtual {v2, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getTexture(III)[I

    move-result-object v11

    .line 367
    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    if-nez v2, :cond_15

    .line 368
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 366
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v11

    goto :goto_8

    .line 369
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture2:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    if-eqz v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture2:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    array-length v2, v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    mul-int/2addr v5, v6

    if-eq v2, v5, :cond_18

    .line 370
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    const v5, 0x16001614

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-virtual {v2, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getTexture(III)[I

    move-result-object v12

    .line 373
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    :goto_9
    if-eqz v11, :cond_17

    if-nez v12, :cond_19

    .line 374
    :cond_17
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 372
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture2:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v12

    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto :goto_9

    .line 375
    :cond_19
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    move-object v10, v4

    invoke-static/range {v9 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyImpressionist([I[I[I[III)V

    goto/16 :goto_2

    .line 378
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyDownlight([I[III)V

    move-object/from16 v12, v21

    .line 379
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 381
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyBluewash([I[III)V

    move-object/from16 v12, v21

    .line 382
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 384
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyNostalgia([I[III)V

    move-object/from16 v12, v21

    .line 385
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 387
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    const/16 v7, 0x32

    invoke-static/range {v2 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlFadedColour([I[B[IIIILandroid/graphics/Rect;)V

    move-object/from16 v12, v21

    .line 388
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 390
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyVignette([I[III)V

    move-object/from16 v12, v21

    .line 391
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 393
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyTurquoise([I[III)V

    move-object/from16 v12, v21

    .line 394
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 396
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyYellowglow([I[III)V

    move-object/from16 v12, v21

    .line 397
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 399
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    if-nez v2, :cond_1a

    .line 400
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 401
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    if-eqz v2, :cond_1b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    array-length v2, v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    mul-int/2addr v5, v6

    if-eq v2, v5, :cond_1c

    .line 402
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    const v5, 0x16001616

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-virtual {v2, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getTexture(III)[I

    move-result-object v11

    .line 405
    :goto_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    if-nez v2, :cond_1d

    .line 406
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 404
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture1:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$3(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v11

    goto :goto_a

    .line 407
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture2:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    if-eqz v2, :cond_1e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture2:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v2

    array-length v2, v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    mul-int/2addr v5, v6

    if-eq v2, v5, :cond_20

    .line 408
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mEffectEffect:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    move-result-object v2

    const v5, 0x16001617

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    invoke-virtual {v2, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->getTexture(III)[I

    move-result-object v12

    .line 411
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    :goto_b
    if-eqz v11, :cond_1f

    if-nez v12, :cond_21

    .line 412
    :cond_1f
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 410
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :cond_20
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->mTexture2:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->access$4(Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;)[I

    move-result-object v12

    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto :goto_b

    .line 413
    :cond_21
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    move-object v10, v4

    invoke-static/range {v9 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyDawnCast([I[I[I[III)V

    goto/16 :goto_2

    .line 416
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    const/16 v7, 0x64

    invoke-static/range {v2 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyPosterizePartial([I[B[IIIILandroid/graphics/Rect;)I

    move-object/from16 v12, v21

    .line 417
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 419
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_18
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    move/from16 v17, v0

    move-object v14, v4

    move-object v15, v3

    move-object/from16 v18, v8

    invoke-static/range {v13 .. v18}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyGothicNoirPartial([I[I[BIILandroid/graphics/Rect;)V

    move-object/from16 v12, v21

    .line 420
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    goto/16 :goto_2

    .line 422
    .end local v12    # "texture_1":[I
    .restart local v21    # "texture_1":[I
    :pswitch_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$in:[I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$height:I

    move-object v7, v8

    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyMagicPenPartial([I[B[IIILandroid/graphics/Rect;)I

    goto/16 :goto_1

    .line 431
    .end local v21    # "texture_1":[I
    .restart local v12    # "texture_1":[I
    .restart local v19    # "bitmapWidth":I
    .restart local v20    # "bitmapHeight":I
    :cond_22
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$width:I

    move/from16 v20, v0

    goto/16 :goto_3

    .line 287
    nop

    :pswitch_data_0
    .packed-switch 0x16001600
        :pswitch_13
        :pswitch_1
        :pswitch_11
        :pswitch_12
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_f
        :pswitch_10
        :pswitch_5
        :pswitch_6
        :pswitch_14
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_2
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method public onPostExcute(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 267
    if-eqz p1, :cond_0

    .line 269
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$icon:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->val$icon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout$1;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/BottomThumbnailButtonLayout;->setThumbnail(Landroid/graphics/Bitmap;)V

    .line 273
    :cond_0
    return-void
.end method
