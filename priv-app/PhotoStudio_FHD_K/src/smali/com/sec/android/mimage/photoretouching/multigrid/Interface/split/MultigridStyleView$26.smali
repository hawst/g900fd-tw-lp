.class Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;
.super Ljava/lang/Object;
.source "MultigridStyleView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->startSuffleAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

.field private final synthetic val$list:Ljava/util/ArrayList;

.field private final synthetic val$objectList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->val$list:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->val$objectList:Ljava/util/ArrayList;

    .line 3173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public calculateRectSize(Landroid/graphics/Rect;Landroid/graphics/RectF;)Landroid/graphics/Rect;
    .locals 2
    .param p1, "srcRect"    # Landroid/graphics/Rect;
    .param p2, "drawRect"    # Landroid/graphics/RectF;

    .prologue
    .line 3177
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$23(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->calculateRectSize(Landroid/graphics/Rect;Landroid/graphics/RectF;Z)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public drawBackground(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 3211
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$23(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->onDraw(Landroid/graphics/Canvas;)V

    .line 3212
    return-void
.end method

.method public endAnimation(Z)V
    .locals 2
    .param p1, "isDestroy"    # Z

    .prologue
    const/4 v1, 0x0

    .line 3187
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$23(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setJustDrawBackground(Z)V

    .line 3189
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->invalidateViewsWithThread()V

    .line 3190
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->animationMoveItem:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->destroy()V

    .line 3192
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->val$list:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 3197
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->val$list:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3199
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->val$objectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 3204
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->val$objectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3206
    return-void

    .line 3194
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->val$list:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->free()V

    .line 3195
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->val$list:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 3201
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->val$objectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridDragObject;->destroy()V

    .line 3202
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->val$objectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1
.end method

.method public startAnimation()V
    .locals 2

    .prologue
    .line 3182
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$26;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mMultiGridView:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$23(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultiGridView;->setJustDrawBackground(Z)V

    .line 3183
    return-void
.end method
