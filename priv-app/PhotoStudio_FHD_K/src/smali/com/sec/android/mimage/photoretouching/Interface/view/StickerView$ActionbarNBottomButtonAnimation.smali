.class Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;
.super Ljava/lang/Object;
.source "StickerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionbarNBottomButtonAnimation"
.end annotation


# instance fields
.field private mIsShowing:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1128
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    .line 1127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1126
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 1129
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 1130
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 1

    .prologue
    .line 1149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 1150
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1152
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->hide()V

    .line 1154
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1156
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->drawerHide()V

    .line 1158
    :cond_1
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 1133
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    return v0
.end method

.method public show()V
    .locals 1

    .prologue
    .line 1137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 1138
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1140
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->show()V

    .line 1142
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1144
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->drawerShow()V

    .line 1146
    :cond_1
    return-void
.end method
