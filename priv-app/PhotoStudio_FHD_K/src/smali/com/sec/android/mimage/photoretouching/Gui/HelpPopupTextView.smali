.class public Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;
.super Landroid/widget/TextView;
.source "HelpPopupTextView.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsTextSizeReduced:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->mContext:Landroid/content/Context;

    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->init(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->mContext:Landroid/content/Context;

    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->init(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->mContext:Landroid/content/Context;

    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->init(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method private getPixelFromDp(F)F
    .locals 1
    .param p1, "dp"    # F

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    return v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->mContext:Landroid/content/Context;

    .line 36
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setStringFormat()V

    .line 37
    return-void
.end method

.method private reduceTextSize()V
    .locals 7

    .prologue
    const/high16 v6, -0x80000000

    const/4 v5, 0x0

    .line 108
    const/4 v1, 0x2

    .line 109
    .local v1, "reduceTextSize":I
    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->getPixelFromDp(F)F

    move-result v0

    .line 111
    .local v0, "reducePixelTextSize":F
    new-instance v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 112
    .local v2, "tv":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->getTextSize()F

    move-result v3

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->getWidth()I

    move-result v3

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->getHeight()I

    move-result v4

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 114
    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 116
    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 118
    invoke-virtual {v2}, Landroid/widget/TextView;->getLineCount()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->getMaxLines()I

    move-result v4

    if-le v3, v4, :cond_0

    .line 120
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ysjeong, reduced text size = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->getTextSize()F

    move-result v4

    sub-float/2addr v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", HelpPopupTextView"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->getTextSize()F

    move-result v3

    sub-float/2addr v3, v0

    invoke-virtual {p0, v5, v3}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setTextSize(IF)V

    .line 123
    :cond_0
    const/4 v2, 0x0

    .line 124
    return-void
.end method

.method private setStringFormat()V
    .locals 7

    .prologue
    const v6, 0x7f060037

    const v5, 0x7f0601af

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 40
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 42
    .local v0, "res":Landroid/content/res/Resources;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 45
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->getId()I

    move-result v1

    const v2, 0x7f09012d

    if-ne v1, v2, :cond_0

    .line 47
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    .line 50
    :sswitch_1
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 53
    :sswitch_2
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const v3, 0x7f060038

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 56
    :sswitch_3
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const v3, 0x7f060039

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 59
    :sswitch_4
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const v3, 0x7f06003b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 62
    :sswitch_5
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const v3, 0x7f06003c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 65
    :sswitch_6
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const v3, 0x7f060042

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 68
    :sswitch_7
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const v3, 0x7f060093

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 71
    :sswitch_8
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const v3, 0x7f060095

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 74
    :sswitch_9
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const v3, 0x7f060096

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 80
    :sswitch_a
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v1

    const v2, 0x17001701

    if-ne v1, v2, :cond_1

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->getId()I

    move-result v1

    const v2, 0x7f09012e

    if-ne v1, v2, :cond_0

    .line 84
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 87
    :cond_1
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v1

    const v2, 0x17001702

    if-ne v1, v2, :cond_2

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->getId()I

    move-result v1

    const v2, 0x7f09012a

    if-ne v1, v2, :cond_0

    .line 91
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const v3, 0x7f0600d5

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 94
    :cond_2
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v1

    const v2, 0x17001703

    if-ne v1, v2, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->getId()I

    move-result v1

    const v2, 0x7f09012f

    if-ne v1, v2, :cond_0

    .line 98
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const v3, 0x7f060159

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0x15000000 -> :sswitch_0
        0x18000000 -> :sswitch_a
    .end sparse-switch

    .line 47
    :sswitch_data_1
    .sparse-switch
        0x15001500 -> :sswitch_1
        0x15001501 -> :sswitch_2
        0x15001502 -> :sswitch_3
        0x15001503 -> :sswitch_4
        0x15001504 -> :sswitch_5
        0x15001505 -> :sswitch_6
        0x15001607 -> :sswitch_7
        0x15001608 -> :sswitch_8
        0x15001609 -> :sswitch_9
    .end sparse-switch
.end method


# virtual methods
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->mIsTextSizeReduced:Z

    .line 128
    invoke-super {p0, p1}, Landroid/widget/TextView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 129
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "arg0"    # Landroid/graphics/Canvas;

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->mIsTextSizeReduced:Z

    if-nez v0, :cond_0

    .line 136
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->reduceTextSize()V

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/HelpPopupTextView;->mIsTextSizeReduced:Z

    .line 140
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 141
    return-void
.end method
