.class public Lcom/sec/android/mimage/photoretouching/MediaReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaReceiver.java"


# static fields
.field public static completeMediaScanner:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/mimage/photoretouching/MediaReceiver;->completeMediaScanner:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static getScanState()Z
    .locals 1

    .prologue
    .line 26
    sget-boolean v0, Lcom/sec/android/mimage/photoretouching/MediaReceiver;->completeMediaScanner:Z

    return v0
.end method

.method public static initScanState()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/mimage/photoretouching/MediaReceiver;->completeMediaScanner:Z

    .line 24
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 14
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 16
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18
    const-string v1, "ACTION_MEDIA_SCANNER_FINISHED"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 19
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/mimage/photoretouching/MediaReceiver;->completeMediaScanner:Z

    .line 21
    :cond_0
    return-void
.end method
