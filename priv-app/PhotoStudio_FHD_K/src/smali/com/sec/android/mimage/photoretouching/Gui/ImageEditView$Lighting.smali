.class Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;
.super Ljava/lang/Object;
.source "ImageEditView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Lighting"
.end annotation


# static fields
.field static final BOTTOM:I = 0x1

.field static final LEFT:I = 0x2

.field static final RIGHT:I = 0x3

.field static final TOP:I


# instance fields
.field private alpha:I

.field private drawLightingDstRoi:Landroid/graphics/RectF;

.field private drawLightingSrcRoi:Landroid/graphics/Rect;

.field private drawOverscrollEdgeDstRoi:Landroid/graphics/Rect;

.field private drawOverscrollEdgeSrcRoi:Landroid/graphics/Rect;

.field private drawPaint:Landroid/graphics/Paint;

.field private lightingBitmap:Landroid/graphics/Bitmap;

.field private mode:I

.field private overscrollEdgeBitmap:Landroid/graphics/Bitmap;

.field private removeThread:Ljava/lang/Thread;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;I)V
    .locals 17
    .param p2, "mode"    # I

    .prologue
    .line 439
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    .line 438
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 641
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawPaint:Landroid/graphics/Paint;

    .line 642
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawLightingSrcRoi:Landroid/graphics/Rect;

    .line 643
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawLightingDstRoi:Landroid/graphics/RectF;

    .line 644
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawOverscrollEdgeSrcRoi:Landroid/graphics/Rect;

    .line 645
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawOverscrollEdgeDstRoi:Landroid/graphics/Rect;

    .line 646
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->lightingBitmap:Landroid/graphics/Bitmap;

    .line 647
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->overscrollEdgeBitmap:Landroid/graphics/Bitmap;

    .line 648
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->removeThread:Ljava/lang/Thread;

    .line 441
    :try_start_0
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->mode:I

    .line 442
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->alpha:I

    .line 444
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawLightingSrcRoi:Landroid/graphics/Rect;

    .line 445
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawLightingDstRoi:Landroid/graphics/RectF;

    .line 446
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawOverscrollEdgeSrcRoi:Landroid/graphics/Rect;

    .line 447
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawOverscrollEdgeDstRoi:Landroid/graphics/Rect;

    .line 448
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawPaint:Landroid/graphics/Paint;

    .line 449
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 450
    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020345

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 451
    .local v2, "lightBitmapTop":Landroid/graphics/Bitmap;
    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mContext:Landroid/content/Context;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020344

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 452
    .local v16, "edgeBitmapTop":Landroid/graphics/Bitmap;
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 454
    .local v7, "m":Landroid/graphics/Matrix;
    packed-switch p2, :pswitch_data_0

    .line 509
    .end local v2    # "lightBitmapTop":Landroid/graphics/Bitmap;
    .end local v7    # "m":Landroid/graphics/Matrix;
    .end local v16    # "edgeBitmapTop":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 457
    .restart local v2    # "lightBitmapTop":Landroid/graphics/Bitmap;
    .restart local v7    # "m":Landroid/graphics/Matrix;
    .restart local v16    # "edgeBitmapTop":Landroid/graphics/Bitmap;
    :pswitch_0
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->lightingBitmap:Landroid/graphics/Bitmap;

    .line 458
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->overscrollEdgeBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 506
    .end local v2    # "lightBitmapTop":Landroid/graphics/Bitmap;
    .end local v7    # "m":Landroid/graphics/Matrix;
    .end local v16    # "edgeBitmapTop":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v15

    .line 507
    .local v15, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v15}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0

    .line 461
    .end local v15    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v2    # "lightBitmapTop":Landroid/graphics/Bitmap;
    .restart local v7    # "m":Landroid/graphics/Matrix;
    .restart local v16    # "edgeBitmapTop":Landroid/graphics/Bitmap;
    :pswitch_1
    const/high16 v3, 0x43340000    # 180.0f

    :try_start_1
    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 463
    const/4 v3, 0x0

    .line 464
    const/4 v4, 0x0

    .line 465
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 466
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 467
    const/4 v8, 0x1

    .line 462
    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->lightingBitmap:Landroid/graphics/Bitmap;

    .line 469
    const/4 v9, 0x0

    .line 470
    const/4 v10, 0x0

    .line 471
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    .line 472
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    .line 473
    const/4 v14, 0x1

    move-object/from16 v8, v16

    move-object v13, v7

    .line 468
    invoke-static/range {v8 .. v14}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->overscrollEdgeBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 476
    :pswitch_2
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 478
    const/4 v3, 0x0

    .line 479
    const/4 v4, 0x0

    .line 480
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 481
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 482
    const/4 v8, 0x1

    .line 477
    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->lightingBitmap:Landroid/graphics/Bitmap;

    .line 484
    const/4 v9, 0x0

    .line 485
    const/4 v10, 0x0

    .line 486
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    .line 487
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    .line 488
    const/4 v14, 0x1

    move-object/from16 v8, v16

    move-object v13, v7

    .line 483
    invoke-static/range {v8 .. v14}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->overscrollEdgeBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 491
    :pswitch_3
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 493
    const/4 v3, 0x0

    .line 494
    const/4 v4, 0x0

    .line 495
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 496
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 497
    const/4 v8, 0x1

    .line 492
    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->lightingBitmap:Landroid/graphics/Bitmap;

    .line 499
    const/4 v9, 0x0

    .line 500
    const/4 v10, 0x0

    .line 501
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    .line 502
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    .line 503
    const/4 v14, 0x1

    move-object/from16 v8, v16

    move-object v13, v7

    .line 498
    invoke-static/range {v8 .. v14}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->overscrollEdgeBitmap:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 454
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)I
    .locals 1

    .prologue
    .line 640
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->alpha:I

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;I)V
    .locals 0

    .prologue
    .line 640
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->alpha:I

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)V
    .locals 0

    .prologue
    .line 591
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->setDrawRoi()V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;Ljava/lang/Thread;)V
    .locals 0

    .prologue
    .line 648
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->removeThread:Ljava/lang/Thread;

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    return-object v0
.end method

.method private setDrawRoi()V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/high16 v8, 0x42c80000    # 100.0f

    .line 593
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 595
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalToPreviewMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v2

    .line 596
    .local v2, "m":Landroid/graphics/Matrix;
    new-instance v0, Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalWidth()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;)Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-direct {v0, v6, v6, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 597
    .local v0, "drawRoi":Landroid/graphics/RectF;
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 598
    .local v3, "src":Landroid/graphics/RectF;
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 600
    .local v1, "dst":Landroid/graphics/RectF;
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->mode:I

    packed-switch v4, :pswitch_data_0

    .line 636
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawLightingDstRoi:Landroid/graphics/RectF;

    invoke-virtual {v4, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 638
    .end local v0    # "drawRoi":Landroid/graphics/RectF;
    .end local v1    # "dst":Landroid/graphics/RectF;
    .end local v2    # "m":Landroid/graphics/Matrix;
    .end local v3    # "src":Landroid/graphics/RectF;
    :cond_0
    return-void

    .line 603
    .restart local v0    # "drawRoi":Landroid/graphics/RectF;
    .restart local v1    # "dst":Landroid/graphics/RectF;
    .restart local v2    # "m":Landroid/graphics/Matrix;
    .restart local v3    # "src":Landroid/graphics/RectF;
    :pswitch_0
    iget v4, v0, Landroid/graphics/RectF;->left:F

    .line 604
    iget v5, v0, Landroid/graphics/RectF;->top:F

    .line 605
    iget v6, v0, Landroid/graphics/RectF;->right:F

    .line 606
    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    .line 603
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 607
    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 608
    iget v4, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v8

    iput v4, v1, Landroid/graphics/RectF;->right:F

    goto :goto_0

    .line 611
    :pswitch_1
    iget v4, v0, Landroid/graphics/RectF;->left:F

    .line 612
    iget v5, v0, Landroid/graphics/RectF;->top:F

    .line 613
    iget v6, v0, Landroid/graphics/RectF;->right:F

    .line 614
    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    .line 611
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 615
    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 616
    iget v4, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v8

    iput v4, v1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0

    .line 619
    :pswitch_2
    iget v4, v0, Landroid/graphics/RectF;->left:F

    .line 620
    iget v5, v0, Landroid/graphics/RectF;->top:F

    .line 621
    iget v6, v0, Landroid/graphics/RectF;->right:F

    .line 622
    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    .line 619
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 623
    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 624
    iget v4, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v4, v8

    iput v4, v1, Landroid/graphics/RectF;->left:F

    goto :goto_0

    .line 627
    :pswitch_3
    iget v4, v0, Landroid/graphics/RectF;->left:F

    .line 628
    iget v5, v0, Landroid/graphics/RectF;->top:F

    .line 629
    iget v6, v0, Landroid/graphics/RectF;->right:F

    .line 630
    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    .line 627
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 631
    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 632
    iget v4, v1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v4, v8

    iput v4, v1, Landroid/graphics/RectF;->top:F

    goto :goto_0

    .line 600
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->lightingBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 524
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->overscrollEdgeBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 525
    return-void
.end method

.method public drawLighting(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 528
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->alpha:I

    if-lez v0, :cond_0

    .line 530
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 531
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->alpha:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->lightingBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawLightingSrcRoi:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawLightingDstRoi:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 533
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->overscrollEdgeBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawOverscrollEdgeSrcRoi:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawOverscrollEdgeDstRoi:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 534
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 536
    :cond_0
    return-void
.end method

.method public initLighting()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 512
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawLightingSrcRoi:Landroid/graphics/Rect;

    .line 514
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->lightingBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 515
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->lightingBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 512
    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 516
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->drawOverscrollEdgeSrcRoi:Landroid/graphics/Rect;

    .line 518
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->overscrollEdgeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 519
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->overscrollEdgeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 516
    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 520
    return-void
.end method

.method public minusAlphaLighting()V
    .locals 2

    .prologue
    .line 546
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->removeThread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 548
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->removeThread:Ljava/lang/Thread;

    .line 588
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->removeThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 590
    :cond_0
    return-void
.end method

.method public plusAlphaLighing()V
    .locals 2

    .prologue
    const/16 v0, 0xc8

    .line 539
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->alpha:I

    add-int/lit8 v1, v1, 0x1e

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->alpha:I

    .line 540
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->alpha:I

    if-le v1, v0, :cond_0

    :goto_0
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->alpha:I

    .line 541
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->setDrawRoi()V

    .line 542
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView;->invalidate()V

    .line 543
    return-void

    .line 540
    :cond_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$Lighting;->alpha:I

    goto :goto_0
.end method
