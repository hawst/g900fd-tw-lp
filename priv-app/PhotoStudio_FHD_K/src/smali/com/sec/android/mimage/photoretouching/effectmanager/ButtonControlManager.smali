.class public Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;
.super Ljava/lang/Object;
.source "ButtonControlManager.java"


# instance fields
.field private final THUMB_HEIGHT_L:I

.field private final THUMB_HEIGHT_P:I

.field private final THUMB_WIDTH_L:I

.field private final THUMB_WIDTH_P:I

.field private mBtnCtrlLayout:Landroid/widget/FrameLayout;

.field private mButtonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDoneBtn:Landroid/view/View;

.field private mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

.field private mOverflowBtnCtrlLayout:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

.field private mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    .line 409
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    .line 410
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;

    .line 411
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mOverflowBtnCtrlLayout:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

    .line 413
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    .line 414
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    .line 415
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mDoneBtn:Landroid/view/View;

    .line 27
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    .line 30
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050298

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->THUMB_WIDTH_L:I

    .line 31
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050293

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->THUMB_WIDTH_P:I

    .line 32
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050297

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->THUMB_HEIGHT_L:I

    .line 33
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050292

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->THUMB_HEIGHT_P:I

    .line 34
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;)V
    .locals 0

    .prologue
    .line 414
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V
    .locals 0

    .prologue
    .line 330
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->changeNearButtonInfo(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mOverflowBtnCtrlLayout:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mOverflowBtnCtrlLayout:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)Landroid/view/View;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mDoneBtn:Landroid/view/View;

    return-object v0
.end method

.method private changeNearButtonInfo(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V
    .locals 3
    .param p1, "fromButton"    # Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    .param p2, "toButton"    # Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .prologue
    const/4 v2, 0x0

    .line 332
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getLeftButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v0

    .line 333
    .local v0, "leftFromNull":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    invoke-interface {p1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getRightButton()Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    move-result-object v1

    .line 334
    .local v1, "rightFromNull":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    if-eqz v0, :cond_0

    .line 335
    invoke-interface {v0, p2}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 336
    :cond_0
    if-eqz v1, :cond_1

    .line 337
    invoke-interface {v1, p2}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 339
    :cond_1
    invoke-interface {p1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 340
    invoke-interface {p1, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 341
    return-void
.end method

.method private leftmargin()I
    .locals 7

    .prologue
    .line 47
    const/4 v1, 0x0

    .line 49
    .local v1, "margin":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 50
    .local v0, "d":Landroid/util/DisplayMetrics;
    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v6, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ge v5, v6, :cond_0

    .line 51
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f050296

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v3, v5

    .line 55
    .local v3, "oneThumbnailWidth":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    if-nez v5, :cond_1

    .line 56
    const/4 v5, 0x0

    .line 66
    :goto_1
    return v5

    .line 53
    .end local v3    # "oneThumbnailWidth":I
    :cond_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f050291

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v3, v5

    .restart local v3    # "oneThumbnailWidth":I
    goto :goto_0

    .line 60
    :cond_1
    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 61
    .local v4, "screenWidth":I
    div-int v2, v4, v3

    .line 62
    .local v2, "noOfThumnail":I
    mul-int v5, v3, v2

    sub-int v1, v4, v5

    .line 64
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f05028a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    sub-int/2addr v1, v5

    .line 65
    div-int/lit8 v1, v1, 0x2

    move v5, v1

    .line 66
    goto :goto_1
.end method


# virtual methods
.method public addButton(IILandroid/graphics/Bitmap;I)V
    .locals 6
    .param p1, "buttonId"    # I
    .param p2, "index"    # I
    .param p3, "icon"    # Landroid/graphics/Bitmap;
    .param p4, "textId"    # I

    .prologue
    .line 84
    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->addButton(IILandroid/graphics/Bitmap;IZ)V

    .line 85
    return-void
.end method

.method public addButton(IILandroid/graphics/Bitmap;IZ)V
    .locals 5
    .param p1, "buttonId"    # I
    .param p2, "index"    # I
    .param p3, "icon"    # Landroid/graphics/Bitmap;
    .param p4, "textId"    # I
    .param p5, "use"    # Z

    .prologue
    .line 88
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 90
    .local v2, "size":I
    new-instance v0, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;-><init>(Landroid/content/Context;)V

    .line 91
    .local v0, "button":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    invoke-virtual {v0, p1, p3, p4}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->initButton(ILandroid/graphics/Bitmap;I)V

    .line 93
    invoke-virtual {v0, p5}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setChecked(Z)V

    .line 94
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mDoneBtn:Landroid/view/View;

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setDoneBtn(Landroid/view/View;)V

    .line 96
    if-nez v2, :cond_0

    .line 97
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 105
    :goto_0
    new-instance v3, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager$1;-><init>(Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;)V

    .line 327
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    .line 105
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setTouchCallback(Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonsListener$DefaultTouchInterface;Ljava/util/ArrayList;)V

    .line 328
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 329
    return-void

    .line 100
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    add-int/lit8 v4, v2, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 101
    .local v1, "leftButton":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButton;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 102
    invoke-interface {v1, v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    goto :goto_0
.end method

.method public destroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 70
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    .line 71
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 72
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;

    .line 73
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 79
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    .line 80
    return-void

    .line 73
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .local v1, "button":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    move-object v0, v1

    .line 75
    check-cast v0, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;

    .line 76
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/DefaultButtonFrame;->destroy()V

    goto :goto_0
.end method

.method public getButtonList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNullBtn()Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mNullButton:Lcom/sec/android/mimage/photoretouching/effectmanager/NullButton;

    return-object v0
.end method

.method public getSelectedButtonLayout()Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mOverflowBtnCtrlLayout:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

    return-object v0
.end method

.method public init(Landroid/view/View;)V
    .locals 3
    .param p1, "doneBtn"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 38
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mDoneBtn:Landroid/view/View;

    .line 40
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0900c5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mScrollView:Lcom/sec/android/mimage/photoretouching/effectmanager/TogglableScrollView;

    .line 41
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0900c6

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mOverflowBtnCtrlLayout:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

    .line 43
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mOverflowBtnCtrlLayout:Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v2, v1, v2, v2}, Lcom/sec/android/mimage/photoretouching/effectmanager/SelectedButtonLayout;->setPadding(IIII)V

    .line 44
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0900c7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;

    .line 45
    return-void
.end method

.method public setButtonsPos()V
    .locals 17

    .prologue
    .line 345
    new-instance v11, Landroid/graphics/Point;

    invoke-direct {v11}, Landroid/graphics/Point;-><init>()V

    .line 346
    .local v11, "size":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    check-cast v14, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    invoke-virtual {v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v14

    invoke-interface {v14}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v14

    invoke-virtual {v14, v11}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 347
    iget v6, v11, Landroid/graphics/Point;->x:I

    .line 348
    .local v6, "layoutWidth":I
    const/4 v11, 0x0

    .line 349
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v14

    iget v14, v14, Landroid/content/res/Configuration;->orientation:I

    const/4 v15, 0x2

    if-ne v14, v15, :cond_1

    .line 351
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->THUMB_WIDTH_L:I

    .line 352
    .local v3, "buttonWidth":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->THUMB_HEIGHT_L:I

    .line 360
    .local v2, "buttonHeight":I
    :goto_0
    div-int v10, v6, v3

    .line 361
    .local v10, "rows":I
    const/4 v4, 0x0

    .line 362
    .local v4, "i":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_2

    .line 384
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_0

    .line 386
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    invoke-interface {v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->getCurrentPosition()Landroid/graphics/Rect;

    move-result-object v9

    .line 387
    .local v9, "r":Landroid/graphics/Rect;
    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v14, -0x1

    iget v15, v9, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v7, v14, v15}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 388
    .local v7, "params":Landroid/widget/FrameLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mBtnCtrlLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v14, v7}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 390
    .end local v7    # "params":Landroid/widget/FrameLayout$LayoutParams;
    .end local v9    # "r":Landroid/graphics/Rect;
    :cond_0
    return-void

    .line 356
    .end local v2    # "buttonHeight":I
    .end local v3    # "buttonWidth":I
    .end local v4    # "i":I
    .end local v10    # "rows":I
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->THUMB_WIDTH_P:I

    .line 357
    .restart local v3    # "buttonWidth":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->THUMB_HEIGHT_P:I

    .restart local v2    # "buttonHeight":I
    goto :goto_0

    .line 362
    .restart local v4    # "i":I
    .restart local v10    # "rows":I
    :cond_2
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    .line 364
    .local v1, "button":Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 366
    .local v8, "pos":Landroid/graphics/Rect;
    div-int v13, v4, v10

    .line 367
    .local v13, "y":I
    rem-int v12, v4, v10

    .line 368
    .local v12, "x":I
    const/4 v14, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v14, v0, v3, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 369
    mul-int v14, v12, v3

    mul-int v16, v13, v2

    move/from16 v0, v16

    invoke-virtual {v8, v14, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 370
    invoke-interface {v1, v8}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setCurrentPosition(Landroid/graphics/Rect;)V

    .line 371
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v5

    .line 372
    .local v5, "idx":I
    if-nez v5, :cond_3

    .line 373
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    add-int/lit8 v16, v5, 0x1

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    invoke-interface {v1, v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 381
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 374
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    if-ne v5, v14, :cond_4

    .line 375
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    add-int/lit8 v16, v5, -0x1

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    invoke-interface {v1, v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    goto :goto_2

    .line 378
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    add-int/lit8 v16, v5, -0x1

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    invoke-interface {v1, v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setLeftButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    .line 379
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/effectmanager/ButtonControlManager;->mButtonList:Ljava/util/ArrayList;

    add-int/lit8 v16, v5, 0x1

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;

    invoke-interface {v1, v14}, Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;->setRightButton(Lcom/sec/android/mimage/photoretouching/effectmanager/MyButtonInterface;)V

    goto :goto_2
.end method
