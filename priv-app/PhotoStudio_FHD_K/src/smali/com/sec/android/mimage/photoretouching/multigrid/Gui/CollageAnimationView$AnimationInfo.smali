.class Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;
.super Ljava/lang/Object;
.source "CollageAnimationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimationInfo"
.end annotation


# instance fields
.field private mAnimationObjectList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDuration:J

.field private mStart:Z

.field private mStartTime:J

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 267
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mAnimationObjectList:Ljava/util/ArrayList;

    .line 270
    iput-wide v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mDuration:J

    .line 271
    iput-wide v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mStartTime:J

    .line 272
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mStart:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)V
    .locals 0

    .prologue
    .line 267
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 269
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mAnimationObjectList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mAnimationObjectList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)J
    .locals 2

    .prologue
    .line 271
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)J
    .locals 2

    .prologue
    .line 270
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mDuration:J

    return-wide v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;)Z
    .locals 1

    .prologue
    .line 272
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mStart:Z

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;Z)V
    .locals 0

    .prologue
    .line 272
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mStart:Z

    return-void
.end method


# virtual methods
.method public free()V
    .locals 3

    .prologue
    .line 283
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mAnimationObjectList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 285
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mAnimationObjectList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 289
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mAnimationObjectList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 290
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mAnimationObjectList:Ljava/util/ArrayList;

    .line 292
    :cond_0
    return-void

    .line 285
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;

    .line 287
    .local v0, "oInfo":Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$AnimationObjectInfo;->free()V

    goto :goto_0
.end method

.method public setDuration(I)V
    .locals 2
    .param p1, "duration"    # I

    .prologue
    .line 275
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mDuration:J

    .line 276
    return-void
.end method

.method public setStartTime()V
    .locals 2

    .prologue
    .line 279
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$AnimationInfo;->mStartTime:J

    .line 280
    return-void
.end method
