.class public Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;
.super Ljava/lang/Object;
.source "RotateEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ApplyRotateInfo"
.end annotation


# instance fields
.field private mRoi:Landroid/graphics/Rect;

.field private mRotatedBuff:[I

.field private mRotatedHeight:I

.field private mRotatedWidth:I

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;[IIILandroid/graphics/Rect;)V
    .locals 2
    .param p2, "rotatedBuff"    # [I
    .param p3, "rotatedWidth"    # I
    .param p4, "rotatedHeight"    # I
    .param p5, "roi"    # Landroid/graphics/Rect;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 855
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->this$0:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    .line 854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 850
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->mRotatedBuff:[I

    .line 851
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->mRotatedWidth:I

    .line 852
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->mRotatedHeight:I

    .line 853
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->mRoi:Landroid/graphics/Rect;

    .line 856
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->mRotatedBuff:[I

    .line 857
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->mRotatedWidth:I

    .line 858
    iput p4, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->mRotatedHeight:I

    .line 859
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p5}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->mRoi:Landroid/graphics/Rect;

    .line 860
    return-void
.end method


# virtual methods
.method public getRotatedBuff()[I
    .locals 1

    .prologue
    .line 863
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->mRotatedBuff:[I

    return-object v0
.end method

.method public getRotatedHeight()I
    .locals 1

    .prologue
    .line 871
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->mRotatedHeight:I

    return v0
.end method

.method public getRotatedRoi()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->mRoi:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getRotatedWidth()I
    .locals 1

    .prologue
    .line 867
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect$ApplyRotateInfo;->mRotatedWidth:I

    return v0
.end method
