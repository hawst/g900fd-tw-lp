.class public Lcom/sec/android/mimage/photoretouching/Core/FileData;
.super Ljava/lang/Object;
.source "FileData.java"


# static fields
.field private static final EXIF_TAGS:[I

.field private static mIsPng:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38
    sput-boolean v2, Lcom/sec/android/mimage/photoretouching/Core/FileData;->mIsPng:Z

    .line 549
    const/16 v0, 0x14

    new-array v0, v0, [I

    .line 550
    sget v1, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DATE_TIME:I

    aput v1, v0, v2

    const/4 v1, 0x1

    .line 551
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_MAKE:I

    aput v2, v0, v1

    const/4 v1, 0x2

    .line 552
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_MODEL:I

    aput v2, v0, v1

    const/4 v1, 0x3

    .line 553
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FLASH:I

    aput v2, v0, v1

    const/4 v1, 0x4

    .line 554
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LATITUDE:I

    aput v2, v0, v1

    const/4 v1, 0x5

    .line 555
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LONGITUDE:I

    aput v2, v0, v1

    const/4 v1, 0x6

    .line 556
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LATITUDE_REF:I

    aput v2, v0, v1

    const/4 v1, 0x7

    .line 557
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_LONGITUDE_REF:I

    aput v2, v0, v1

    const/16 v1, 0x8

    .line 558
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_ALTITUDE:I

    aput v2, v0, v1

    const/16 v1, 0x9

    .line 559
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_ALTITUDE_REF:I

    aput v2, v0, v1

    const/16 v1, 0xa

    .line 560
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_TIME_STAMP:I

    aput v2, v0, v1

    const/16 v1, 0xb

    .line 561
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_DATE_STAMP:I

    aput v2, v0, v1

    const/16 v1, 0xc

    .line 562
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_WHITE_BALANCE:I

    aput v2, v0, v1

    const/16 v1, 0xd

    .line 563
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_FOCAL_LENGTH:I

    aput v2, v0, v1

    const/16 v1, 0xe

    .line 564
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_GPS_PROCESSING_METHOD:I

    aput v2, v0, v1

    const/16 v1, 0xf

    .line 566
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_F_NUMBER:I

    aput v2, v0, v1

    const/16 v1, 0x10

    .line 567
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXPOSURE_BIAS_VALUE:I

    aput v2, v0, v1

    const/16 v1, 0x11

    .line 568
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_EXPOSURE_TIME:I

    aput v2, v0, v1

    const/16 v1, 0x12

    .line 569
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_ISO_SPEED_RATINGS:I

    aput v2, v0, v1

    const/16 v1, 0x13

    .line 570
    sget v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_ORIENTATION:I

    aput v2, v0, v1

    .line 549
    sput-object v0, Lcom/sec/android/mimage/photoretouching/Core/FileData;->EXIF_TAGS:[I

    .line 571
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkPathValidDirectory(Ljava/lang/String;)Z
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 536
    if-nez p0, :cond_1

    .line 544
    :cond_0
    :goto_0
    return v1

    .line 538
    :cond_1
    const-string v2, "/picasa/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 540
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 541
    .local v0, "nf":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 544
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static closeSilently(Ljava/io/Closeable;)V
    .locals 2
    .param p0, "c"    # Ljava/io/Closeable;

    .prologue
    .line 680
    if-nez p0, :cond_0

    .line 687
    :goto_0
    return-void

    .line 683
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 684
    :catch_0
    move-exception v0

    .line 685
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "close fail"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static copyExif(Ljava/lang/String;)Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
    .locals 14
    .param p0, "source"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 576
    new-instance v2, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    invoke-direct {v2}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;-><init>()V

    .line 579
    .local v2, "oldExif":Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
    :try_start_0
    invoke-virtual {v2, p0}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->readExif(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 586
    :goto_0
    new-instance v1, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    invoke-direct {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;-><init>()V

    .line 587
    .local v1, "newExif":Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 589
    .local v6, "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    const/4 v3, 0x0

    .line 591
    .local v3, "orientationUsed":Z
    sget-object v9, Lcom/sec/android/mimage/photoretouching/Core/FileData;->EXIF_TAGS:[I

    array-length v10, v9

    move v7, v8

    :goto_1
    if-lt v7, v10, :cond_2

    .line 611
    if-nez v3, :cond_0

    .line 613
    sget v7, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_ORIENTATION:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->buildTag(ILjava/lang/Object;)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 614
    const-string v7, "JW orientation set"

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 618
    :cond_0
    invoke-virtual {v1, v6}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->setTags(Ljava/util/Collection;)V

    .line 619
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "GallerExifInterface end:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getAllTags()Ljava/util/List;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 621
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getAllTags()Ljava/util/List;

    move-result-object v7

    if-nez v7, :cond_1

    .line 622
    const/4 v1, 0x0

    .line 623
    :cond_1
    return-object v1

    .line 580
    .end local v1    # "newExif":Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
    .end local v3    # "orientationUsed":Z
    .end local v6    # "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    :catch_0
    move-exception v0

    .line 581
    .local v0, "e1":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 582
    .end local v0    # "e1":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 583
    .local v0, "e1":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 591
    .end local v0    # "e1":Ljava/io/IOException;
    .restart local v1    # "newExif":Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
    .restart local v3    # "orientationUsed":Z
    .restart local v6    # "tags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/exif/ExifTag;>;"
    :cond_2
    aget v5, v9, v7

    .line 592
    .local v5, "tagId":I
    invoke-virtual {v2, v5}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getTag(I)Lcom/sec/android/mimage/photoretouching/exif/ExifTag;

    move-result-object v4

    .line 594
    .local v4, "tag":Lcom/sec/android/mimage/photoretouching/exif/ExifTag;
    if-eqz v4, :cond_5

    .line 597
    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_DATE_TIME:I

    if-ne v5, v11, :cond_3

    .line 599
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-virtual {v4, v12, v13}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setTimeValue(J)Z

    .line 602
    :cond_3
    sget v11, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->TAG_ORIENTATION:I

    if-ne v5, v11, :cond_4

    .line 604
    invoke-virtual {v4, v8}, Lcom/sec/android/mimage/photoretouching/exif/ExifTag;->setValue(I)Z

    .line 605
    const/4 v3, 0x1

    .line 607
    :cond_4
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 591
    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method public static deleteFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "directory"    # Ljava/lang/String;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 530
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 531
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 532
    return-void
.end method

.method public static loadData(Ljava/lang/String;Ljava/lang/String;[III)Z
    .locals 11
    .param p0, "directory"    # Ljava/lang/String;
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "output"    # [I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 445
    const/4 v3, 0x0

    .line 446
    .local v3, "fin":Ljava/io/FileInputStream;
    const/4 v8, 0x1

    .line 448
    .local v8, "result":Z
    const-string v9, ".jpg"

    invoke-virtual {p1, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, ".JPG"

    invoke-virtual {p1, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 449
    const/4 v9, 0x1

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Core/Image;->setPng(Z)V

    .line 456
    :goto_0
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 461
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .local v4, "fin":Ljava/io/FileInputStream;
    mul-int/lit8 v9, p3, 0x4

    new-array v1, v9, [B

    .line 462
    .local v1, "data":[B
    const/4 v7, 0x0

    .line 465
    .local v7, "readBytes":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-lt v5, p4, :cond_1

    .line 485
    const/4 v1, 0x0

    .line 487
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    move-object v3, v4

    .line 492
    .end local v1    # "data":[B
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v5    # "i":I
    .end local v7    # "readBytes":I
    .end local v8    # "result":Z
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    :goto_3
    return v8

    .line 452
    .restart local v8    # "result":Z
    :cond_0
    const/4 v9, 0x0

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Core/Image;->setPng(Z)V

    goto :goto_0

    .line 457
    :catch_0
    move-exception v2

    .line 458
    .local v2, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 459
    const/4 v8, 0x0

    goto :goto_3

    .line 467
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v1    # "data":[B
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "i":I
    .restart local v7    # "readBytes":I
    :cond_1
    :try_start_2
    invoke-virtual {v4, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v7

    if-lez v7, :cond_2

    .line 469
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 470
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v6

    .line 471
    .local v6, "intBuffer":Ljava/nio/IntBuffer;
    mul-int v9, v5, p3

    invoke-virtual {v6, p2, v9, p3}, Ljava/nio/IntBuffer;->get([III)Ljava/nio/IntBuffer;

    .line 473
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 474
    invoke-virtual {v6}, Ljava/nio/IntBuffer;->clear()Ljava/nio/Buffer;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 476
    const/4 v0, 0x0

    .line 465
    .end local v0    # "byteBuffer":Ljava/nio/ByteBuffer;
    .end local v6    # "intBuffer":Ljava/nio/IntBuffer;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 480
    :catch_1
    move-exception v2

    .line 481
    .local v2, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 485
    const/4 v1, 0x0

    .line 487
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 488
    :catch_2
    move-exception v2

    .line 489
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 484
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    .line 485
    const/4 v1, 0x0

    .line 487
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 491
    :goto_4
    throw v9

    .line 488
    :catch_3
    move-exception v2

    .line 489
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 488
    .end local v2    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v2

    .line 489
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method public static loadData(Ljava/lang/String;Ljava/lang/String;II)[B
    .locals 6
    .param p0, "directory"    # Ljava/lang/String;
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 496
    const/4 v4, 0x0

    .line 497
    .local v4, "ret":[B
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 500
    const/4 v2, 0x0

    .line 502
    .local v2, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .local v3, "fis":Ljava/io/FileInputStream;
    move-object v2, v3

    .line 506
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :goto_0
    if-eqz v2, :cond_1

    .line 508
    mul-int v5, p2, p3

    new-array v4, v5, [B

    .line 510
    :try_start_1
    invoke-virtual {v2, v4}, Ljava/io/FileInputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 515
    :goto_1
    if-eqz v2, :cond_0

    .line 516
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/FileDescriptor;->sync()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 521
    :cond_0
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 527
    .end local v2    # "fis":Ljava/io/FileInputStream;
    :cond_1
    :goto_3
    return-object v4

    .line 503
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 504
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 511
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 512
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 522
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 523
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 518
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v5

    goto :goto_2
.end method

.method private static saveBitmapToOutputStream(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;Ljava/io/OutputStream;)Z
    .locals 3
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "format"    # Landroid/graphics/Bitmap$CompressFormat;
    .param p2, "os"    # Ljava/io/OutputStream;

    .prologue
    .line 633
    const/4 v1, 0x0

    .line 644
    .local v1, "result":Z
    const/16 v2, 0x64

    :try_start_0
    invoke-virtual {p0, p1, v2, p2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v1

    .line 645
    if-nez v1, :cond_0

    .line 646
    const-string v2, "Bitmap write errror!"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    move v2, v1

    .line 653
    :goto_0
    return v2

    .line 650
    :catch_0
    move-exception v0

    .line 652
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 653
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static saveCompressedBitmapToOutputStream(Landroid/graphics/Bitmap;Ljava/io/OutputStream;)Z
    .locals 5
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "os"    # Ljava/io/OutputStream;

    .prologue
    const/4 v2, 0x0

    .line 664
    const/4 v1, 0x0

    .line 666
    .local v1, "result":Z
    :try_start_0
    sget-object v3, Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;->JPEGSQ:Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;

    .line 667
    const/4 v4, 0x0

    .line 666
    invoke-static {p0, v3, v4, p1}, Lcom/sec/android/simageis/SImageis;->compress(Landroid/graphics/Bitmap;Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;ILjava/io/OutputStream;)Z

    move-result v1

    .line 668
    if-nez v1, :cond_0

    .line 669
    const-string v3, "Bitmap write errror!"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    move v2, v1

    .line 674
    :goto_0
    return v2

    .line 672
    :catch_0
    move-exception v0

    .line 673
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static saveData(Ljava/lang/String;Ljava/lang/String;[BII)Z
    .locals 7
    .param p0, "directory"    # Ljava/lang/String;
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "buffer"    # [B
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v6, 0x0

    .line 398
    const/4 v4, 0x0

    .line 399
    .local v4, "ret":Z
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 400
    .local v3, "nf":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 402
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v5

    if-nez v5, :cond_0

    .line 403
    const-string v5, "make directory!!, fail"

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 441
    :goto_0
    return v6

    .line 407
    :catch_0
    move-exception v0

    .line 408
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 412
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v3, 0x0

    .line 413
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    .local v1, "file":Ljava/io/File;
    const/4 v2, 0x0

    .line 416
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    .line 423
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v2, p2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 429
    :goto_1
    if-eqz v2, :cond_1

    .line 430
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/FileDescriptor;->sync()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 435
    :cond_1
    :goto_2
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 436
    :catch_1
    move-exception v0

    .line 438
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 417
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v0

    .line 419
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 424
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v0

    .line 426
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 432
    .end local v0    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v5

    goto :goto_2
.end method

.method public static saveData(Ljava/lang/String;Ljava/lang/String;[III)Z
    .locals 10
    .param p0, "directory"    # Ljava/lang/String;
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "image"    # [I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 342
    const/4 v7, 0x1

    .line 343
    .local v7, "result":Z
    const-string v8, "Edit Done Start"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 345
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 346
    .local v6, "nf":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 348
    :try_start_0
    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    move-result v8

    if-nez v8, :cond_0

    .line 349
    const-string v8, "make directory!!, fail"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    const/4 v8, 0x0

    .line 394
    :goto_0
    return v8

    .line 353
    :catch_0
    move-exception v1

    .line 354
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 355
    const/4 v7, 0x0

    .line 358
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v6, 0x0

    .line 361
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    .line 364
    .local v3, "fos":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-lt v4, p4, :cond_1

    .line 382
    :goto_2
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .end local v4    # "i":I
    :goto_3
    move v8, v7

    .line 394
    goto :goto_0

    .line 366
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "i":I
    :cond_1
    mul-int/lit8 v8, p3, 0x4

    :try_start_3
    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 367
    .local v0, "byteBuff":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v5

    .line 368
    .local v5, "intBuff":Ljava/nio/IntBuffer;
    mul-int v8, v4, p3

    invoke-virtual {v5, p2, v8, p3}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    .line 369
    invoke-virtual {v5}, Ljava/nio/IntBuffer;->flip()Ljava/nio/Buffer;

    .line 370
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 371
    .local v2, "fc":Ljava/nio/channels/FileChannel;
    invoke-virtual {v2, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 372
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 373
    invoke-virtual {v5}, Ljava/nio/IntBuffer;->clear()Ljava/nio/Buffer;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2

    .line 374
    const/4 v5, 0x0

    .line 364
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 377
    .end local v0    # "byteBuff":Ljava/nio/ByteBuffer;
    .end local v2    # "fc":Ljava/nio/channels/FileChannel;
    .end local v5    # "intBuff":Ljava/nio/IntBuffer;
    :catch_1
    move-exception v1

    .line 379
    .local v1, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 388
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .end local v4    # "i":I
    :catch_2
    move-exception v1

    .line 389
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_3

    .line 383
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "i":I
    :catch_3
    move-exception v1

    .line 385
    .local v1, "e":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_3
.end method

.method public static saveToImage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;)Landroid/net/Uri;
    .locals 25
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "directory"    # Ljava/lang/String;
    .param p3, "filename"    # Ljava/lang/String;
    .param p4, "bitmap"    # Landroid/graphics/Bitmap;
    .param p5, "format"    # Landroid/graphics/Bitmap$CompressFormat;

    .prologue
    .line 203
    const-string v4, "FileData : saveToImage : 00"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 204
    const/16 v20, 0x0

    .line 206
    .local v20, "result":Z
    const/16 v24, 0x0

    .line 207
    .local v24, "uri":Landroid/net/Uri;
    const/16 v17, 0x0

    .line 209
    .local v17, "fail":Landroid/net/Uri;
    if-eqz p4, :cond_3

    .line 211
    const-string v4, "FileData : saveToImage : 01"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 212
    const/16 v19, 0x0

    .line 214
    .local v19, "qdioData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    .line 215
    .local v12, "width":I
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    .line 216
    .local v13, "height":I
    const/4 v15, 0x0

    .line 218
    .local v15, "fOut":Ljava/io/FileOutputStream;
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    .local v21, "saveFile":Ljava/io/File;
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v22

    .line 220
    .local v22, "savePath":Ljava/lang/String;
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 221
    .local v18, "nf":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 223
    const-string v4, "FileData : saveToImage : 02"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 226
    :try_start_0
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-nez v4, :cond_0

    .line 228
    const-string v4, "make directory!!, fail"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    .end local v12    # "width":I
    .end local v13    # "height":I
    .end local v15    # "fOut":Ljava/io/FileOutputStream;
    .end local v17    # "fail":Landroid/net/Uri;
    .end local v18    # "nf":Ljava/io/File;
    .end local v19    # "qdioData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    .end local v21    # "saveFile":Ljava/io/File;
    .end local v22    # "savePath":Ljava/lang/String;
    :goto_0
    return-object v17

    .line 232
    .restart local v12    # "width":I
    .restart local v13    # "height":I
    .restart local v15    # "fOut":Ljava/io/FileOutputStream;
    .restart local v17    # "fail":Landroid/net/Uri;
    .restart local v18    # "nf":Ljava/io/File;
    .restart local v19    # "qdioData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    .restart local v21    # "saveFile":Ljava/io/File;
    .restart local v22    # "savePath":Ljava/lang/String;
    :catch_0
    move-exception v14

    .line 234
    .local v14, "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    .line 235
    const/16 v20, 0x0

    .line 236
    const-string v4, "FileData : saveToImage : error 02"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_0

    .line 241
    .end local v14    # "e":Ljava/lang/Exception;
    :cond_0
    const-string v4, "FileData : saveToImage : 03"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 243
    :try_start_1
    new-instance v16, Ljava/io/FileOutputStream;

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 244
    .end local v15    # "fOut":Ljava/io/FileOutputStream;
    .local v16, "fOut":Ljava/io/FileOutputStream;
    const/16 v4, 0x64

    :try_start_2
    move-object/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    .line 245
    const/16 v20, 0x1

    .line 252
    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/Core/Image;->sync(Ljava/io/FileOutputStream;)Z

    .line 254
    const-string v4, "FileData : saveToImage : 04"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 256
    :try_start_3
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 257
    const/16 v20, 0x1

    .line 265
    const-string v4, "FileData : saveToImage : 05"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 266
    if-eqz p1, :cond_1

    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->checkPathValidDirectory(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 268
    invoke-static/range {p1 .. p1}, Lcom/sec/android/secvision/sef/QdioJNI;->checkAudioInJPEG(Ljava/lang/String;)Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;

    move-result-object v19

    .line 269
    if-eqz v19, :cond_1

    .line 270
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/sec/android/secvision/sef/QdioJNI;->copyAdioInJPEGtoPNG(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_1
    const/16 v23, 0x0

    .line 276
    .local v23, "type":I
    const-string v4, "FileData : saveToImage : 06"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 277
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Core/Image;->isPng()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 279
    const/16 v23, 0x1

    .line 280
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/Image;->setPng(Z)V

    .line 291
    :goto_1
    const/4 v5, 0x0

    .line 292
    .local v5, "title":Ljava/lang/String;
    const/4 v8, 0x0

    .line 293
    .local v8, "location":Landroid/location/Location;
    const/4 v9, 0x0

    .line 294
    .local v9, "orientation":I
    const-string v4, "FileData : saveToImage : 07"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 299
    :try_start_4
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 301
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    .line 299
    invoke-static/range {v4 .. v13}, Lcom/sec/android/mimage/photoretouching/Core/Image;->addImage(Landroid/content/ContentResolver;Ljava/lang/String;JLandroid/location/Location;ILjava/lang/String;Ljava/lang/String;II)Landroid/net/Uri;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v24

    .line 316
    const-string v4, "FileData : saveToImage : 08"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 320
    :try_start_5
    new-instance v4, Landroid/content/Intent;

    const-string v6, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    move-object/from16 v0, v24

    invoke-direct {v4, v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 328
    const-string v4, "saveToImage End"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 330
    const-string v4, "FileData : saveToImage : success"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 332
    if-nez v24, :cond_2

    if-eqz v20, :cond_2

    .line 334
    invoke-static/range {v21 .. v21}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v24

    .line 336
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "FileData : saveToImage : success : "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .end local v5    # "title":Ljava/lang/String;
    .end local v8    # "location":Landroid/location/Location;
    .end local v9    # "orientation":I
    .end local v12    # "width":I
    .end local v13    # "height":I
    .end local v16    # "fOut":Ljava/io/FileOutputStream;
    .end local v18    # "nf":Ljava/io/File;
    .end local v19    # "qdioData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    .end local v21    # "saveFile":Ljava/io/File;
    .end local v22    # "savePath":Ljava/lang/String;
    .end local v23    # "type":I
    :cond_3
    move-object/from16 v17, v24

    .line 338
    goto/16 :goto_0

    .line 246
    .restart local v12    # "width":I
    .restart local v13    # "height":I
    .restart local v15    # "fOut":Ljava/io/FileOutputStream;
    .restart local v18    # "nf":Ljava/io/File;
    .restart local v19    # "qdioData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    .restart local v21    # "saveFile":Ljava/io/File;
    .restart local v22    # "savePath":Ljava/lang/String;
    :catch_1
    move-exception v14

    .line 247
    .restart local v14    # "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    .line 248
    const-string v4, "FileData : saveToImage : error 03"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 258
    .end local v14    # "e":Ljava/lang/Exception;
    .end local v15    # "fOut":Ljava/io/FileOutputStream;
    .restart local v16    # "fOut":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v14

    .line 259
    .restart local v14    # "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    .line 260
    const/16 v20, 0x0

    .line 261
    const-string v4, "FileData : saveToImage : error 04"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 284
    .end local v14    # "e":Ljava/lang/Exception;
    .restart local v23    # "type":I
    :cond_4
    const/16 v23, 0x0

    .line 285
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/Image;->setPng(Z)V

    goto :goto_1

    .line 309
    .restart local v5    # "title":Ljava/lang/String;
    .restart local v8    # "location":Landroid/location/Location;
    .restart local v9    # "orientation":I
    :catch_3
    move-exception v14

    .line 311
    .restart local v14    # "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    .line 312
    const-string v4, "FileData : saveToImage : error 06"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 322
    .end local v14    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v14

    .line 324
    .restart local v14    # "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    .line 325
    const-string v4, "FileData : saveToImage : error 07"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 246
    .end local v5    # "title":Ljava/lang/String;
    .end local v8    # "location":Landroid/location/Location;
    .end local v9    # "orientation":I
    .end local v14    # "e":Ljava/lang/Exception;
    .end local v23    # "type":I
    :catch_5
    move-exception v14

    move-object/from16 v15, v16

    .end local v16    # "fOut":Ljava/io/FileOutputStream;
    .restart local v15    # "fOut":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method public static saveToImage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[IIILandroid/graphics/Bitmap$CompressFormat;I)Landroid/net/Uri;
    .locals 30
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "directory"    # Ljava/lang/String;
    .param p3, "filename"    # Ljava/lang/String;
    .param p4, "data"    # [I
    .param p5, "width"    # I
    .param p6, "height"    # I
    .param p7, "format"    # Landroid/graphics/Bitmap$CompressFormat;
    .param p8, "quality"    # I

    .prologue
    .line 42
    const/16 v22, 0x0

    .line 43
    .local v22, "ret":Landroid/net/Uri;
    const/16 v19, 0x0

    .line 44
    .local v19, "out":Ljava/io/OutputStream;
    const/16 v20, 0x0

    .line 45
    .local v20, "qdioData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    const/16 v16, 0x0

    .line 46
    .local v16, "fOut":Ljava/io/FileOutputStream;
    const/4 v14, 0x0

    .line 50
    .local v14, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, p4

    move/from16 v1, p5

    move/from16 v2, p6

    invoke-static {v0, v1, v2, v4}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    .line 57
    const v4, 0x4c4b40

    move/from16 v0, p8

    if-ne v0, v4, :cond_0

    mul-int v4, p5, p6

    const v6, 0x4c4b40

    if-le v4, v6, :cond_0

    .line 59
    mul-int v4, p5, p6

    int-to-float v4, v4

    const v6, 0x4a989680    # 5000000.0f

    div-float/2addr v4, v6

    float-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v0, v6

    move/from16 v25, v0

    .line 60
    .local v25, "scale":F
    move/from16 v0, p5

    int-to-float v4, v0

    div-float v4, v4, v25

    float-to-int v0, v4

    move/from16 v28, v0

    .line 61
    .local v28, "scaledWidth":I
    move/from16 v0, p6

    int-to-float v4, v0

    div-float v4, v4, v25

    float-to-int v0, v4

    move/from16 v27, v0

    .line 62
    .local v27, "scaledHeight":I
    const/4 v4, 0x1

    move/from16 v0, v28

    move/from16 v1, v27

    invoke-static {v14, v0, v1, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v26

    .line 63
    .local v26, "scaledBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->recycle()V

    .line 64
    const/4 v14, 0x0

    .line 66
    move-object/from16 v14, v26

    .line 67
    move/from16 p5, v28

    .line 68
    move/from16 p6, v27

    .line 71
    .end local v25    # "scale":F
    .end local v26    # "scaledBitmap":Landroid/graphics/Bitmap;
    .end local v27    # "scaledHeight":I
    .end local v28    # "scaledWidth":I
    :cond_0
    if-nez p2, :cond_1

    .line 73
    const-string v4, "directory null, saveToImage"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 74
    sget-object p2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    .line 77
    :cond_1
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 78
    .local v18, "nf":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 80
    :try_start_1
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-nez v4, :cond_2

    .line 81
    const-string v4, "make directory!!, fail"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 82
    const/4 v4, 0x0

    .line 198
    .end local v18    # "nf":Ljava/io/File;
    :goto_0
    return-object v4

    .line 51
    :catch_0
    move-exception v15

    .line 52
    .local v15, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "FileData - saveToImage() : "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 53
    const/4 v4, 0x0

    goto :goto_0

    .line 85
    .end local v15    # "e":Ljava/lang/Exception;
    .restart local v18    # "nf":Ljava/io/File;
    :catch_1
    move-exception v15

    .line 86
    .restart local v15    # "e":Ljava/lang/Exception;
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V

    .line 87
    const/4 v4, 0x0

    goto :goto_0

    .line 90
    .end local v15    # "e":Ljava/lang/Exception;
    :cond_2
    new-instance v23, Ljava/io/File;

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    .local v23, "saveFile":Ljava/io/File;
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v24

    .line 101
    .local v24, "savePath":Ljava/lang/String;
    const/16 v21, 0x0

    .line 102
    .local v21, "resultExif":Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    move-object/from16 v0, p7

    if-ne v0, v4, :cond_4

    .line 104
    if-eqz p1, :cond_3

    .line 105
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->copyExif(Ljava/lang/String;)Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;

    move-result-object v21

    .line 107
    :cond_3
    const/4 v4, 0x1

    const/4 v6, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v6}, Ljava/io/File;->setReadable(ZZ)Z

    .line 108
    const/4 v4, 0x1

    const/4 v6, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4, v6}, Ljava/io/File;->setWritable(ZZ)Z

    .line 111
    :cond_4
    if-eqz v21, :cond_9

    .line 113
    :try_start_2
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/exif/GalleryExifInterface;->getExifWriterStream(Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v19

    .line 114
    const-string v4, "tr"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "tb"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    :cond_5
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    move-object/from16 v0, p7

    if-ne v0, v4, :cond_8

    .line 115
    move-object/from16 v0, v19

    invoke-static {v14, v0}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->saveCompressedBitmapToOutputStream(Landroid/graphics/Bitmap;Ljava/io/OutputStream;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 134
    :goto_1
    if-eqz v21, :cond_f

    .line 135
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->closeSilently(Ljava/io/Closeable;)V

    .line 149
    :cond_6
    :goto_2
    const/16 v23, 0x0

    .line 159
    invoke-static {v14}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 161
    if-eqz p1, :cond_7

    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->checkPathValidDirectory(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 163
    invoke-static/range {p1 .. p1}, Lcom/sec/android/secvision/sef/QdioJNI;->checkAudioInJPEG(Ljava/lang/String;)Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;

    move-result-object v20

    .line 164
    if-eqz v20, :cond_7

    .line 165
    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lcom/sec/android/secvision/sef/QdioJNI;->copyAdioInJPEGtoPNG(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    :cond_7
    const/4 v5, 0x0

    .line 169
    .local v5, "title":Ljava/lang/String;
    const/4 v8, 0x0

    .line 170
    .local v8, "location":Landroid/location/Location;
    const/4 v9, 0x0

    .line 173
    .local v9, "orientation":I
    const/16 v29, 0x0

    .line 175
    .local v29, "type":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Core/Image;->isPng()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 177
    const/16 v29, 0x1

    .line 185
    :goto_3
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 187
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move/from16 v12, p5

    move/from16 v13, p6

    .line 185
    invoke-static/range {v4 .. v13}, Lcom/sec/android/mimage/photoretouching/Core/Image;->addImage(Landroid/content/ContentResolver;Ljava/lang/String;JLandroid/location/Location;ILjava/lang/String;Ljava/lang/String;II)Landroid/net/Uri;

    move-result-object v22

    .line 195
    new-instance v4, Landroid/content/Intent;

    const-string v6, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    move-object/from16 v0, v22

    invoke-direct {v4, v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 196
    const-string v4, "saveToImage End"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    move-object/from16 v4, v22

    .line 198
    goto/16 :goto_0

    .line 117
    .end local v5    # "title":Ljava/lang/String;
    .end local v8    # "location":Landroid/location/Location;
    .end local v9    # "orientation":I
    .end local v29    # "type":I
    :cond_8
    :try_start_3
    move-object/from16 v0, p7

    move-object/from16 v1, v19

    invoke-static {v14, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->saveBitmapToOutputStream(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;Ljava/io/OutputStream;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 130
    :catch_2
    move-exception v15

    .line 131
    .restart local v15    # "e":Ljava/lang/Exception;
    :goto_4
    :try_start_4
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 134
    if-eqz v21, :cond_c

    .line 135
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_2

    .line 122
    .end local v15    # "e":Ljava/lang/Exception;
    :cond_9
    :try_start_5
    new-instance v17, Ljava/io/FileOutputStream;

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 123
    .end local v16    # "fOut":Ljava/io/FileOutputStream;
    .local v17, "fOut":Ljava/io/FileOutputStream;
    :try_start_6
    const-string v4, "tr"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "tb"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    :cond_a
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    move-object/from16 v0, p7

    if-ne v0, v4, :cond_b

    .line 124
    sget-object v4, Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;->JPEGSQ:Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;

    .line 125
    const/4 v6, 0x0

    .line 124
    move-object/from16 v0, v17

    invoke-static {v14, v4, v6, v0}, Lcom/sec/android/simageis/SImageis;->compress(Landroid/graphics/Bitmap;Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;ILjava/io/OutputStream;)Z

    move-object/from16 v16, v17

    .line 126
    .end local v17    # "fOut":Ljava/io/FileOutputStream;
    .restart local v16    # "fOut":Ljava/io/FileOutputStream;
    goto/16 :goto_1

    .line 127
    .end local v16    # "fOut":Ljava/io/FileOutputStream;
    .restart local v17    # "fOut":Ljava/io/FileOutputStream;
    :cond_b
    const/16 v4, 0x64

    move-object/from16 v0, p7

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-object/from16 v16, v17

    .line 130
    .end local v17    # "fOut":Ljava/io/FileOutputStream;
    .restart local v16    # "fOut":Ljava/io/FileOutputStream;
    goto/16 :goto_1

    .line 139
    .restart local v15    # "e":Ljava/lang/Exception;
    :cond_c
    if-eqz v16, :cond_6

    .line 141
    :try_start_7
    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/Core/Image;->sync(Ljava/io/FileOutputStream;)Z

    .line 142
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_2

    .line 144
    :catch_3
    move-exception v15

    .line 145
    .local v15, "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 133
    .end local v15    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 134
    :goto_5
    if-eqz v21, :cond_e

    .line 135
    invoke-static/range {v19 .. v19}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->closeSilently(Ljava/io/Closeable;)V

    .line 148
    :cond_d
    :goto_6
    throw v4

    .line 139
    :cond_e
    if-eqz v16, :cond_d

    .line 141
    :try_start_8
    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/Core/Image;->sync(Ljava/io/FileOutputStream;)Z

    .line 142
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_6

    .line 144
    :catch_4
    move-exception v15

    .line 145
    .restart local v15    # "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 139
    .end local v15    # "e":Ljava/io/IOException;
    :cond_f
    if-eqz v16, :cond_6

    .line 141
    :try_start_9
    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/Core/Image;->sync(Ljava/io/FileOutputStream;)Z

    .line 142
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    goto/16 :goto_2

    .line 144
    :catch_5
    move-exception v15

    .line 145
    .restart local v15    # "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 181
    .end local v15    # "e":Ljava/io/IOException;
    .restart local v5    # "title":Ljava/lang/String;
    .restart local v8    # "location":Landroid/location/Location;
    .restart local v9    # "orientation":I
    .restart local v29    # "type":I
    :cond_10
    const/16 v29, 0x0

    goto/16 :goto_3

    .line 133
    .end local v5    # "title":Ljava/lang/String;
    .end local v8    # "location":Landroid/location/Location;
    .end local v9    # "orientation":I
    .end local v16    # "fOut":Ljava/io/FileOutputStream;
    .end local v29    # "type":I
    .restart local v17    # "fOut":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object/from16 v16, v17

    .end local v17    # "fOut":Ljava/io/FileOutputStream;
    .restart local v16    # "fOut":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 130
    .end local v16    # "fOut":Ljava/io/FileOutputStream;
    .restart local v17    # "fOut":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v15

    move-object/from16 v16, v17

    .end local v17    # "fOut":Ljava/io/FileOutputStream;
    .restart local v16    # "fOut":Ljava/io/FileOutputStream;
    goto/16 :goto_4
.end method
