.class Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;
.super Ljava/lang/Object;
.source "StampListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionbarNBottomButtonAnimation"
.end annotation


# static fields
.field public static final TOUCH_OFFSET:I = 0x64


# instance fields
.field private mIsShowing:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 209
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 210
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 211
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 230
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 231
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->hide()V

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v0, :cond_1

    .line 237
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->indicatorHide(Z)V

    .line 239
    :cond_1
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 214
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    return v0
.end method

.method public show()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 218
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 219
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->show()V

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->indicatorShow(Z)V

    .line 227
    :cond_1
    return-void
.end method
