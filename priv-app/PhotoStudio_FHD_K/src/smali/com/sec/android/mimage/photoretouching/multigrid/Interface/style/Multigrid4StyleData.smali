.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;
.super Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;
.source "Multigrid4StyleData.java"


# static fields
.field private static final mCollagePreset0:[[F

.field private static final mCollagePreset1:[[F

.field private static final mCollagePreset10:[[F

.field private static final mCollagePreset2:[[F

.field private static final mCollagePreset3:[[F

.field private static final mCollagePreset4:[[F

.field private static final mCollagePreset5:[[F

.field private static final mCollagePreset6:[[F

.field private static final mCollagePreset7:[[F

.field private static final mCollagePreset8:[[F

.field private static final mCollagePreset9:[[F

.field private static final mPilePreset0_1to1:[[F

.field private static final mPilePreset0_9to16:[[F

.field private static final mPilePreset1_1to1:[[F

.field private static final mPilePreset1_9to16:[[F

.field private static final mPilePreset2_1to1:[[F

.field private static final mPilePreset2_9to16:[[F

.field private static final mPilePreset3_1to1:[[F

.field private static final mPilePreset3_9to16:[[F


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 9
    new-array v0, v3, [[F

    .line 10
    new-array v1, v3, [F

    .line 11
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 13
    new-array v1, v3, [F

    .line 14
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 16
    new-array v1, v3, [F

    .line 17
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 19
    new-array v1, v3, [F

    .line 20
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 7
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset10:[[F

    .line 26
    new-array v0, v3, [[F

    .line 27
    new-array v1, v3, [F

    .line 28
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 30
    new-array v1, v3, [F

    .line 31
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 33
    new-array v1, v3, [F

    .line 34
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 36
    new-array v1, v3, [F

    .line 37
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 24
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset9:[[F

    .line 43
    new-array v0, v3, [[F

    .line 44
    new-array v1, v3, [F

    .line 45
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 47
    new-array v1, v3, [F

    .line 48
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 50
    new-array v1, v3, [F

    .line 51
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 54
    new-array v1, v3, [F

    .line 55
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 41
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset8:[[F

    .line 61
    new-array v0, v3, [[F

    .line 62
    new-array v1, v3, [F

    .line 63
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 65
    new-array v1, v3, [F

    .line 66
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 68
    new-array v1, v3, [F

    .line 69
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 71
    new-array v1, v3, [F

    .line 72
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 59
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset7:[[F

    .line 78
    new-array v0, v3, [[F

    .line 79
    new-array v1, v3, [F

    .line 80
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 82
    new-array v1, v3, [F

    .line 83
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 85
    new-array v1, v3, [F

    .line 86
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 89
    new-array v1, v3, [F

    .line 90
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 76
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset6:[[F

    .line 96
    new-array v0, v3, [[F

    .line 97
    new-array v1, v3, [F

    .line 98
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 100
    new-array v1, v3, [F

    .line 101
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 103
    new-array v1, v3, [F

    .line 104
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p66:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 106
    new-array v1, v3, [F

    .line 107
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 94
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset5:[[F

    .line 113
    new-array v0, v3, [[F

    .line 114
    new-array v1, v3, [F

    .line 115
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p25:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 117
    new-array v1, v3, [F

    .line 118
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p25:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 120
    new-array v1, v3, [F

    .line 121
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p75:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 123
    new-array v1, v3, [F

    .line 124
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p75:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 111
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset4:[[F

    .line 131
    new-array v0, v3, [[F

    .line 132
    new-array v1, v3, [F

    .line 133
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p25:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 135
    new-array v1, v3, [F

    .line 136
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p25:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 138
    new-array v1, v3, [F

    .line 139
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p75:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 141
    new-array v1, v3, [F

    .line 142
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p75:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 129
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset3:[[F

    .line 148
    new-array v0, v3, [[F

    .line 149
    new-array v1, v3, [F

    .line 150
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p375:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 152
    new-array v1, v3, [F

    .line 153
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p375:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 155
    new-array v1, v3, [F

    .line 156
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p625:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 158
    new-array v1, v3, [F

    .line 159
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p625:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 146
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset2:[[F

    .line 165
    new-array v0, v3, [[F

    .line 166
    new-array v1, v3, [F

    .line 167
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p625:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 169
    new-array v1, v3, [F

    .line 170
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p375:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 172
    new-array v1, v3, [F

    .line 173
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p625:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 175
    new-array v1, v3, [F

    .line 176
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p375:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 163
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset1:[[F

    .line 182
    new-array v0, v3, [[F

    .line 183
    new-array v1, v3, [F

    .line 184
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 186
    new-array v1, v3, [F

    .line 187
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 189
    new-array v1, v3, [F

    .line 190
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v6

    .line 192
    new-array v1, v3, [F

    .line 193
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v7

    .line 180
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset0:[[F

    .line 198
    new-array v0, v3, [[F

    .line 199
    new-array v1, v3, [F

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    .line 200
    new-array v1, v3, [F

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    .line 201
    new-array v1, v3, [F

    fill-array-data v1, :array_2

    aput-object v1, v0, v6

    .line 202
    new-array v1, v3, [F

    fill-array-data v1, :array_3

    aput-object v1, v0, v7

    .line 197
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset0_1to1:[[F

    .line 206
    new-array v0, v3, [[F

    .line 207
    new-array v1, v3, [F

    fill-array-data v1, :array_4

    aput-object v1, v0, v4

    .line 208
    new-array v1, v3, [F

    fill-array-data v1, :array_5

    aput-object v1, v0, v5

    .line 209
    new-array v1, v3, [F

    fill-array-data v1, :array_6

    aput-object v1, v0, v6

    .line 210
    new-array v1, v3, [F

    fill-array-data v1, :array_7

    aput-object v1, v0, v7

    .line 205
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset1_1to1:[[F

    .line 214
    new-array v0, v3, [[F

    .line 215
    new-array v1, v3, [F

    fill-array-data v1, :array_8

    aput-object v1, v0, v4

    .line 216
    new-array v1, v3, [F

    fill-array-data v1, :array_9

    aput-object v1, v0, v5

    .line 217
    new-array v1, v3, [F

    fill-array-data v1, :array_a

    aput-object v1, v0, v6

    .line 218
    new-array v1, v3, [F

    fill-array-data v1, :array_b

    aput-object v1, v0, v7

    .line 213
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset2_1to1:[[F

    .line 222
    new-array v0, v3, [[F

    .line 223
    new-array v1, v3, [F

    fill-array-data v1, :array_c

    aput-object v1, v0, v4

    .line 224
    new-array v1, v3, [F

    fill-array-data v1, :array_d

    aput-object v1, v0, v5

    .line 225
    new-array v1, v3, [F

    fill-array-data v1, :array_e

    aput-object v1, v0, v6

    .line 226
    new-array v1, v3, [F

    fill-array-data v1, :array_f

    aput-object v1, v0, v7

    .line 221
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset3_1to1:[[F

    .line 230
    new-array v0, v3, [[F

    .line 231
    new-array v1, v3, [F

    fill-array-data v1, :array_10

    aput-object v1, v0, v4

    .line 232
    new-array v1, v3, [F

    fill-array-data v1, :array_11

    aput-object v1, v0, v5

    .line 233
    new-array v1, v3, [F

    fill-array-data v1, :array_12

    aput-object v1, v0, v6

    .line 234
    new-array v1, v3, [F

    fill-array-data v1, :array_13

    aput-object v1, v0, v7

    .line 229
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset0_9to16:[[F

    .line 238
    new-array v0, v3, [[F

    .line 239
    new-array v1, v3, [F

    fill-array-data v1, :array_14

    aput-object v1, v0, v4

    .line 240
    new-array v1, v3, [F

    fill-array-data v1, :array_15

    aput-object v1, v0, v5

    .line 241
    new-array v1, v3, [F

    fill-array-data v1, :array_16

    aput-object v1, v0, v6

    .line 242
    new-array v1, v3, [F

    fill-array-data v1, :array_17

    aput-object v1, v0, v7

    .line 237
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset1_9to16:[[F

    .line 246
    new-array v0, v3, [[F

    .line 247
    new-array v1, v3, [F

    fill-array-data v1, :array_18

    aput-object v1, v0, v4

    .line 248
    new-array v1, v3, [F

    fill-array-data v1, :array_19

    aput-object v1, v0, v5

    .line 249
    new-array v1, v3, [F

    fill-array-data v1, :array_1a

    aput-object v1, v0, v6

    .line 250
    new-array v1, v3, [F

    fill-array-data v1, :array_1b

    aput-object v1, v0, v7

    .line 245
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset2_9to16:[[F

    .line 254
    new-array v0, v3, [[F

    .line 255
    new-array v1, v3, [F

    fill-array-data v1, :array_1c

    aput-object v1, v0, v4

    .line 256
    new-array v1, v3, [F

    fill-array-data v1, :array_1d

    aput-object v1, v0, v5

    .line 257
    new-array v1, v3, [F

    fill-array-data v1, :array_1e

    aput-object v1, v0, v6

    .line 258
    new-array v1, v3, [F

    fill-array-data v1, :array_1f

    aput-object v1, v0, v7

    .line 253
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset3_9to16:[[F

    .line 259
    return-void

    .line 199
    :array_0
    .array-data 4
        0x3de38866    # 0.1111f
        -0x422d7732    # -0.1028f
        0x3f160aa6    # 0.5861f
        0x3eeaa64c    # 0.4583f
    .end array-data

    .line 200
    :array_1
    .array-data 4
        0x3ed27bb3    # 0.4111f
        0x3e000000    # 0.125f
        0x3f838ef3    # 1.0278f
        0x3f56c227    # 0.8389f
    .end array-data

    .line 201
    :array_2
    .array-data 4
        -0x43333333    # -0.025f
        0x3e800000    # 0.25f
        0x3f088659    # 0.5333f
        0x3f64f766    # 0.8944f
    .end array-data

    .line 202
    :array_3
    .array-data 4
        0x3ec9eecc    # 0.3944f
        0x3f1c710d    # 0.6111f
        0x3f6fa440    # 0.9361f
        0x3f860aa6    # 1.0472f
    .end array-data

    .line 207
    :array_4
    .array-data 4
        0x3f00b780    # 0.5028f
        0x3f00b780    # 0.5028f
        0x3f56c227    # 0.8389f
        0x3f6eecc0    # 0.9333f
    .end array-data

    .line 208
    :array_5
    .array-data 4
        0x3dcccccd    # 0.1f
        0x3f07d567    # 0.5306f
        0x3f0b5dcc    # 0.5444f
        0x3f616f00    # 0.8806f
    .end array-data

    .line 209
    :array_6
    .array-data 4
        0x3ef77319    # 0.4833f
        0x3db61134    # 0.0889f
        0x3f6d844d    # 0.9278f
        0x3ee22681    # 0.4417f
    .end array-data

    .line 210
    :array_7
    .array-data 4
        0x3e0b5dcc    # 0.1361f
        0x3e13dd98    # 0.1444f
        0x3efe9100    # 0.4972f
        0x3f1bb98c    # 0.6083f
    .end array-data

    .line 215
    :array_8
    .array-data 4
        0x3dd288ce    # 0.1028f
        0x3f0b5dcc    # 0.5444f
        0x3efe9100    # 0.4972f
        0x3f5a511a    # 0.8528f
    .end array-data

    .line 216
    :array_9
    .array-data 4
        0x3f0b5dcc    # 0.5444f
        0x3f07d567    # 0.5306f
        0x3f638ef3    # 0.8889f
        0x3f782a99    # 0.9694f
    .end array-data

    .line 217
    :array_a
    .array-data 4
        0x3e000000    # 0.125f
        0x3dcccccd    # 0.1f
        0x3ee0b780    # 0.4389f
        0x3f016f00    # 0.5056f
    .end array-data

    .line 218
    :array_b
    .array-data 4
        0x3f044674    # 0.5167f
        0x3dd288ce    # 0.1028f
        0x3f6b5dcc    # 0.9194f
        0x3ed3eab3    # 0.4139f
    .end array-data

    .line 223
    :array_c
    .array-data 4
        0x3dddcc64    # 0.1083f
        0x3eac154d    # 0.3361f
        0x3ec88ce7    # 0.3917f
        0x3f016f00    # 0.5056f
    .end array-data

    .line 224
    :array_d
    .array-data 4
        0x3f19999a    # 0.6f
        0x3e7a43fe    # 0.2444f
        0x3f59999a    # 0.85f
        0x3f127bb3    # 0.5722f
    .end array-data

    .line 225
    :array_e
    .array-data 4
        0x3e5288ce    # 0.2056f
        0x3f2b5dcc    # 0.6694f
        0x3ec43fe6    # 0.3833f
        0x3f527bb3    # 0.8222f
    .end array-data

    .line 226
    :array_f
    .array-data 4
        0x3f0b5dcc    # 0.5444f
        0x3f4ccccd    # 0.8f
        0x3f305bc0    # 0.6889f
        0x3f69f55a    # 0.9139f
    .end array-data

    .line 231
    :array_10
    .array-data 4
        -0x42ca233a    # -0.0444f
        -0x4465fd8b    # -0.0047f
        0x3f160aa6    # 0.5861f
        0x3ed404ea    # 0.4141f
    .end array-data

    .line 232
    :array_11
    .array-data 4
        0x3edb089a    # 0.4278f
        0x3e365fd9    # 0.1781f
        0x3f92233a    # 1.1417f
        0x3f253261    # 0.6453f
    .end array-data

    .line 233
    :array_12
    .array-data 4
        -0x42913405    # -0.0583f
        0x3eb8c7e3    # 0.3609f
        0x3f1e9100    # 0.6194f
        0x3f4c01a3    # 0.7969f
    .end array-data

    .line 234
    :array_13
    .array-data 4
        0x3ea7d567    # 0.3278f
        0x3f2d97f6    # 0.6781f
        0x3f782a99    # 0.9694f
        0x3f7930be    # 0.9734f
    .end array-data

    .line 239
    :array_14
    .array-data 4
        0x3f0e3bcd    # 0.5556f
        0x3edc01a3    # 0.4297f
        0x3f8b6113    # 1.0889f
        0x3f4ccccd    # 0.8f
    .end array-data

    .line 240
    :array_15
    .array-data 4
        0x3d82de01    # 0.0639f
        0x3f2068dc    # 0.6266f
        0x3f27d567    # 0.6556f
        0x3f63fe5d    # 0.8906f
    .end array-data

    .line 241
    :array_16
    .array-data 4
        0x3ebe9100    # 0.3722f
        0x3db67a10    # 0.0891f
        0x3f82233a    # 1.0167f
        0x3ec339c1    # 0.3813f
    .end array-data

    .line 242
    :array_17
    .array-data 4
        -0x43ca233a    # -0.0111f
        0x3e74d6a1    # 0.2391f
        0x3f044674    # 0.5167f
        0x3f1e69ad    # 0.6188f
    .end array-data

    .line 247
    :array_18
    .array-data 4
        0x3c88ce70    # 0.0167f
        0x3f113405    # 0.5672f
        0x3f04f766    # 0.5194f
        0x3f4a0275    # 0.7891f
    .end array-data

    .line 248
    :array_19
    .array-data 4
        0x3f13eab3    # 0.5778f
        0x3f10cb29    # 0.5656f
        0x3f733333    # 0.95f
        0x3f54d014    # 0.8313f
    .end array-data

    .line 249
    :array_1a
    .array-data 4
        0x3cfaacda    # 0.0306f
        0x3e3e5c92    # 0.1859f
        0x3ee38866    # 0.4444f
        0x3ef66cf4    # 0.4813f
    .end array-data

    .line 250
    :array_1b
    .array-data 4
        0x3ef05532    # 0.4694f
        0x3e519ce0    # 0.2047f
        0x3f7de00d    # 0.9917f
        0x3edd97f6    # 0.4328f
    .end array-data

    .line 255
    :array_1c
    .array-data 4
        0x3e3bb2ff    # 0.1833f
        0x3d800000    # 0.0625f
        0x3ef4a234    # 0.4778f
        0x3e318fc5    # 0.1734f
    .end array-data

    .line 256
    :array_1d
    .array-data 4
        0x3eac154d    # 0.3361f
        0x3ebb2fec    # 0.3656f
        0x3f271de7    # 0.6528f
        0x3f19999a    # 0.6f
    .end array-data

    .line 257
    :array_1e
    .array-data 4
        0x3e0e3bcd    # 0.1389f
        0x3f4c63f1    # 0.7984f
        0x3eba511a    # 0.3639f
        0x3f686595    # 0.9078f
    .end array-data

    .line 258
    :array_1f
    .array-data 4
        0x3f221ff3    # 0.6333f
        0x3f4c63f1    # 0.7984f
        0x3f5bb98c    # 0.8583f
        0x3f686595    # 0.9078f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;-><init>()V

    return-void
.end method

.method public static getDrawRects(II)[[F
    .locals 4
    .param p0, "style"    # I
    .param p1, "proportion"    # I

    .prologue
    const/high16 v3, 0x1e500000

    .line 265
    const/4 v0, 0x0

    .line 267
    .local v0, "returnPreset":[[F
    const/high16 v1, 0x1e200000

    const/4 v2, 0x4

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getMaxStyleNum(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    move-result p0

    .line 269
    packed-switch p0, :pswitch_data_0

    .line 361
    const/4 v0, 0x0

    .line 365
    :goto_0
    return-object v0

    .line 272
    :pswitch_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset0:[[F

    .line 273
    goto :goto_0

    .line 276
    :pswitch_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset1:[[F

    .line 277
    goto :goto_0

    .line 280
    :pswitch_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset2:[[F

    .line 281
    goto :goto_0

    .line 284
    :pswitch_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset3:[[F

    .line 285
    goto :goto_0

    .line 288
    :pswitch_4
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset4:[[F

    .line 289
    goto :goto_0

    .line 292
    :pswitch_5
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset5:[[F

    .line 293
    goto :goto_0

    .line 296
    :pswitch_6
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset6:[[F

    .line 297
    goto :goto_0

    .line 300
    :pswitch_7
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset7:[[F

    .line 301
    goto :goto_0

    .line 304
    :pswitch_8
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset8:[[F

    .line 305
    goto :goto_0

    .line 308
    :pswitch_9
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset9:[[F

    .line 309
    goto :goto_0

    .line 312
    :pswitch_a
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mCollagePreset10:[[F

    .line 313
    goto :goto_0

    .line 317
    :pswitch_b
    if-ne p1, v3, :cond_0

    .line 319
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset0_1to1:[[F

    .line 320
    goto :goto_0

    .line 323
    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset0_9to16:[[F

    .line 325
    goto :goto_0

    .line 328
    :pswitch_c
    if-ne p1, v3, :cond_1

    .line 330
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset1_1to1:[[F

    .line 331
    goto :goto_0

    .line 334
    :cond_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset1_9to16:[[F

    .line 336
    goto :goto_0

    .line 339
    :pswitch_d
    if-ne p1, v3, :cond_2

    .line 341
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset2_1to1:[[F

    .line 342
    goto :goto_0

    .line 345
    :cond_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset2_9to16:[[F

    .line 347
    goto :goto_0

    .line 350
    :pswitch_e
    if-ne p1, v3, :cond_3

    .line 352
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset3_1to1:[[F

    .line 353
    goto :goto_0

    .line 356
    :cond_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid4StyleData;->mPilePreset3_9to16:[[F

    .line 358
    goto :goto_0

    .line 269
    nop

    :pswitch_data_0
    .packed-switch 0x1e200000
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method


# virtual methods
.method public getRotate(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 372
    const/4 v0, 0x0

    return v0
.end method
