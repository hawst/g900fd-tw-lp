.class Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;
.super Landroid/os/AsyncTask;
.source "StampView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ThumbnailAsync"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field asyncCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$AsyncCallback;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$AsyncCallback;)V
    .locals 1
    .param p2, "callback"    # Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$AsyncCallback;

    .prologue
    .line 843
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 841
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->asyncCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$AsyncCallback;

    .line 844
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->asyncCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$AsyncCallback;

    .line 845
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 849
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->asyncCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$AsyncCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$AsyncCallback;->doInBackground()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 868
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->asyncCallback:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$AsyncCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$AsyncCallback;->onPostExcute(Landroid/graphics/Bitmap;)V

    .line 869
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 871
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 873
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 877
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;Landroid/app/ProgressDialog;)V

    .line 878
    return-void

    .line 874
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 853
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 854
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-nez v0, :cond_0

    .line 856
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;Landroid/app/ProgressDialog;)V

    .line 857
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v1

    .line 858
    const v2, 0x7f06008f

    .line 857
    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 859
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 860
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 862
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$ThumbnailAsync;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 865
    :cond_0
    return-void
.end method
