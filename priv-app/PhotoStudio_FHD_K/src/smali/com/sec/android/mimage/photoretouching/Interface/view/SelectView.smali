.class public Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "SelectView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$MultiTouchScaleGestureListener;
    }
.end annotation


# instance fields
.field private final REDOALL_DIALOG:I

.field private final UNDOALL_DIALOG:I

.field private addSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

.field private brushInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

.field private lassoInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mClearPaint:Landroid/graphics/Paint;

.field private mColor:I

.field private mContext:Landroid/content/Context;

.field private mContour:Z

.field private mDashPathEffectPaint:Landroid/graphics/Paint;

.field private mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

.field private mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private final mKey:Ljava/lang/String;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMiddleBtn:Landroid/widget/Button;

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mOriginalInputImage:[I

.field private mPaint:Landroid/graphics/Paint;

.field private mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

.field private mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

.field private mPreviewBuffer:[I

.field private mPreviewHeight:I

.field private mPreviewWidth:I

.field private mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

.field private mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

.field private mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;

.field private magneticInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

.field private newSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

.field private roundInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

.field private squareInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

.field private subtractSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 86
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 1302
    const-string v0, "SELECT"

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mKey:Ljava/lang/String;

    .line 1303
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1304
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mCheckBox:Landroid/widget/CheckBox;

    .line 1305
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    .line 1306
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1307
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1308
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1309
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1310
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1311
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 1312
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 1313
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPaint:Landroid/graphics/Paint;

    .line 1314
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mClearPaint:Landroid/graphics/Paint;

    .line 1315
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mLinePaint:Landroid/graphics/Paint;

    .line 1316
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 1317
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1318
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewBuffer:[I

    .line 1319
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewWidth:I

    .line 1320
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewHeight:I

    .line 1321
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

    .line 1322
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContour:Z

    .line 1323
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 1324
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 1325
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    .line 1326
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mOriginalInputImage:[I

    .line 1328
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mMiddleBtn:Landroid/widget/Button;

    .line 1330
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1332
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 1334
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1336
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->newSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1337
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->addSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1338
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->subtractSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1339
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->magneticInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1340
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->lassoInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1341
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->brushInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1342
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->roundInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1343
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->squareInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1344
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mColor:I

    .line 1345
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .line 1346
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->UNDOALL_DIALOG:I

    .line 1347
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->REDOALL_DIALOG:I

    .line 87
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initSelection(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p6, "info"    # Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 96
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 1302
    const-string v0, "SELECT"

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mKey:Ljava/lang/String;

    .line 1303
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1304
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mCheckBox:Landroid/widget/CheckBox;

    .line 1305
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    .line 1306
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1307
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1308
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1309
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1310
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1311
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 1312
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 1313
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPaint:Landroid/graphics/Paint;

    .line 1314
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mClearPaint:Landroid/graphics/Paint;

    .line 1315
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mLinePaint:Landroid/graphics/Paint;

    .line 1316
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 1317
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1318
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewBuffer:[I

    .line 1319
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewWidth:I

    .line 1320
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewHeight:I

    .line 1321
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

    .line 1322
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContour:Z

    .line 1323
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 1324
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 1325
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    .line 1326
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mOriginalInputImage:[I

    .line 1328
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mMiddleBtn:Landroid/widget/Button;

    .line 1330
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1332
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 1334
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1336
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->newSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1337
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->addSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1338
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->subtractSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1339
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->magneticInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1340
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->lassoInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1341
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->brushInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1342
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->roundInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1343
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->squareInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 1344
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mColor:I

    .line 1345
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .line 1346
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->UNDOALL_DIALOG:I

    .line 1347
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->REDOALL_DIALOG:I

    .line 97
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initSelection(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V

    .line 98
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    .line 99
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;)V
    .locals 0

    .prologue
    .line 1321
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Z
    .locals 1

    .prologue
    .line 1322
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContour:Z

    return v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .locals 1

    .prologue
    .line 1310
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;)V
    .locals 0

    .prologue
    .line 1311
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    return-void
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1305
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;
    .locals 1

    .prologue
    .line 1345
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 1330
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 1304
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mCheckBox:Landroid/widget/CheckBox;

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 1304
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 1307
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 1334
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 1334
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)I
    .locals 1

    .prologue
    .line 1344
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mColor:I

    return v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)[I
    .locals 1

    .prologue
    .line 1326
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mOriginalInputImage:[I

    return-object v0
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)[I
    .locals 1

    .prologue
    .line 1318
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewBuffer:[I

    return-object v0
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;
    .locals 1

    .prologue
    .line 1321
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;I)V
    .locals 0

    .prologue
    .line 1344
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mColor:I

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 1306
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;
    .locals 1

    .prologue
    .line 1325
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    .locals 1

    .prologue
    .line 1324
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 1309
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 1308
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Z)V
    .locals 0

    .prologue
    .line 1322
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContour:Z

    return-void
.end method

.method private checkHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 217
    const/4 v0, 0x0

    .line 218
    .local v0, "isNoShow":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->isDoNotShowPopup(Landroid/content/Context;I)Z

    move-result v0

    .line 220
    if-nez v0, :cond_0

    .line 222
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-nez v1, :cond_0

    .line 224
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initHelpPopup(I)V

    .line 225
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->showCurrentModeHelpPopup()V

    .line 228
    :cond_0
    return-void
.end method

.method private initHelpPopup(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 231
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    .line 232
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$4;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->setHelpPopupCallback(Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager$HelpPopupCallback;)V

    .line 239
    return-void
.end method

.method private initPinchZoomCallback()V
    .locals 3

    .prologue
    .line 1032
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$26;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 1080
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->init(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;)V

    .line 1081
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->resetZoom()V

    .line 1082
    return-void
.end method

.method private initSaveAsDialog()V
    .locals 14

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 849
    const/4 v10, 0x0

    .line 850
    .local v10, "fileName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v13

    .line 851
    .local v13, "path":Ljava/lang/String;
    if-eqz v13, :cond_0

    .line 853
    invoke-static {v13}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 854
    if-nez v10, :cond_0

    .line 856
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 860
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 861
    const v2, 0x7f090156

    .line 862
    const v3, 0x7f0601cd

    .line 865
    const v6, 0x103012e

    .line 860
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 867
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 868
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v7, 0x600

    move v8, v4

    move v9, v4

    move-object v11, v5

    move-object v12, v5

    invoke-virtual/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIILjava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 875
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000a

    .line 876
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$18;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    .line 875
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V

    .line 936
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 937
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$19;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$19;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    .line 936
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 951
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 953
    return-void
.end method

.method private initSelectAreaDialog()V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 663
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x3000

    const v2, 0x1400140b

    .line 664
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->getSelectAreaDialogPos()Landroid/graphics/Point;

    move-result-object v5

    const v6, 0x103012e

    .line 663
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IILjava/lang/String;ZLandroid/graphics/Point;I)V

    .line 665
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 666
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$9;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->newSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 676
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$10;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->addSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 686
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$11;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->subtractSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 697
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v6, 0x110

    const v8, 0x7f020528

    const v9, 0x7f060022

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->newSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    move v7, v4

    move-object v10, v3

    invoke-virtual/range {v5 .. v11}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 698
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v6, 0x100

    const v8, 0x7f020525

    const v9, 0x7f060023

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->addSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    move v7, v4

    move-object v10, v3

    invoke-virtual/range {v5 .. v11}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 699
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v6, 0x120

    const v8, 0x7f02052a

    const v9, 0x7f060024

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->subtractSelectionInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    move v7, v4

    move-object v10, v3

    invoke-virtual/range {v5 .. v11}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 700
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 701
    return-void
.end method

.method private initSelectModeDialog()V
    .locals 13

    .prologue
    const/16 v12, 0x100

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 758
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x3000

    const v2, 0x1400140d

    .line 759
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->getSelectModeDialogPos()Landroid/graphics/Point;

    move-result-object v5

    const v6, 0x103012e

    .line 758
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IILjava/lang/String;ZLandroid/graphics/Point;I)V

    .line 760
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 761
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$13;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->magneticInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 776
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$14;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->lassoInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 792
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$15;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->brushInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 807
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$16;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->roundInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 823
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$17;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$17;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->squareInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 840
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v6, 0x110

    const v8, 0x7f0204c8

    const v9, 0x7f06002e

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->magneticInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    move v7, v4

    move-object v10, v3

    invoke-virtual/range {v5 .. v11}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 841
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v8, 0x7f0204c7

    const v9, 0x7f060091

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->lassoInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    move v6, v12

    move v7, v4

    move-object v10, v3

    invoke-virtual/range {v5 .. v11}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 842
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v8, 0x7f0204c6

    const v9, 0x7f0600b3

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->brushInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    move v6, v12

    move v7, v4

    move-object v10, v3

    invoke-virtual/range {v5 .. v11}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 843
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v8, 0x7f0204c9

    const v9, 0x7f0600b4

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->roundInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    move v6, v12

    move v7, v4

    move-object v10, v3

    invoke-virtual/range {v5 .. v11}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 844
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v6, 0x120

    const v8, 0x7f0204ca

    const v9, 0x7f0600b5

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->squareInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    move v7, v4

    move-object v10, v3

    invoke-virtual/range {v5 .. v11}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 845
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 846
    return-void
.end method

.method private initSelectNotiDialog()V
    .locals 7

    .prologue
    .line 559
    const-string v0, "kbr : initSelectNotiDialog"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 560
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 561
    const/4 v2, 0x0

    .line 562
    const v3, 0x7f060021

    .line 563
    const/4 v4, 0x1

    .line 564
    const/4 v5, 0x0

    .line 565
    const v6, 0x103012e

    .line 560
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 566
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 568
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x300

    .line 569
    const v2, 0x7f06007f

    .line 570
    const-string v3, "SELECT"

    .line 571
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$7;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    .line 568
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 586
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 587
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$8;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    .line 586
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 612
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 614
    return-void
.end method

.method private initSelectSizeDialog()V
    .locals 14

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 704
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x3000

    const v2, 0x1400140c

    .line 705
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->getSelectSizeDialogPos()Landroid/graphics/Point;

    move-result-object v5

    const v6, 0x103012e

    .line 704
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IILjava/lang/String;ZLandroid/graphics/Point;I)V

    .line 706
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 707
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPointerDirection(I)V

    .line 708
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v6, 0x210

    .line 710
    const v8, 0x7f02035d

    .line 713
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getBrushSize()I

    move-result v11

    .line 714
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getBrushMax()I

    move-result v12

    .line 715
    new-instance v13, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$12;

    invoke-direct {v13, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    move v7, v4

    move-object v9, v3

    move-object v10, v3

    .line 708
    invoke-virtual/range {v5 .. v13}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addSeekbar(IIILandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;IILcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;)V

    .line 751
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 754
    return-void
.end method

.method private initSelection(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    .line 1162
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    .line 1163
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1164
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1165
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1166
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1168
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1170
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->setInterface(Ljava/lang/Object;)V

    .line 1172
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    .line 1173
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 1174
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1175
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1176
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPaint:Landroid/graphics/Paint;

    .line 1177
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1179
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mClearPaint:Landroid/graphics/Paint;

    .line 1180
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mClearPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 1182
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mLinePaint:Landroid/graphics/Paint;

    .line 1183
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1184
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1186
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 1187
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1188
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1189
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    const/4 v2, 0x4

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v1, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1191
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    .line 1192
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$MultiTouchScaleGestureListener;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$MultiTouchScaleGestureListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    invoke-direct {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;)V

    .line 1191
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 1194
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$28;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$28;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 1228
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->setViewLayerType(I)V

    .line 1231
    return-void

    .line 1189
    nop

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method private initUndoRedoAllDialog()V
    .locals 15

    .prologue
    const v6, 0x103012e

    const/16 v1, 0x1000

    const/16 v14, 0x500

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 956
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 958
    const v3, 0x7f0600a6

    move v4, v2

    .line 956
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 963
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 964
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 965
    const v3, 0x7f06009c

    .line 967
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$20;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$20;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    .line 964
    invoke-virtual {v0, v14, v3, v5, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 974
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060007

    .line 975
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$21;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$21;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    .line 974
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 982
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060009

    .line 983
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$22;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$22;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    .line 982
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 991
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 993
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 994
    const/4 v9, 0x2

    .line 995
    const v10, 0x7f0600a1

    move v8, v1

    move v11, v2

    move-object v12, v5

    move v13, v6

    .line 993
    invoke-virtual/range {v7 .. v13}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1000
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1001
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1002
    const v1, 0x7f0601ce

    .line 1004
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$23;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$23;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    .line 1001
    invoke-virtual {v0, v14, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1011
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 1012
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$24;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$24;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    .line 1011
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1019
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1020
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$25;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$25;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    .line 1019
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1028
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1029
    return-void
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 464
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    invoke-virtual {v0, p2}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 465
    const/4 v0, 0x1

    return v0
.end method

.method public backPressed()V
    .locals 3

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->clearSelectArea()V

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    if-eqz v0, :cond_1

    .line 284
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;->getPreviousStatus()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviousEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(ILcom/sec/android/mimage/photoretouching/Core/PreviousEffectInfo;)V

    .line 300
    :cond_1
    return-void
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 634
    return-void
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 647
    const/4 v0, 0x0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 651
    const/4 v0, 0x0

    return v0
.end method

.method public getStatusHeight()I
    .locals 3

    .prologue
    .line 655
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 656
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 657
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 658
    iget v2, v0, Landroid/graphics/Rect;->top:I

    return v2
.end method

.method public initActionbar()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 303
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v4, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 306
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 307
    const/4 v2, 0x1

    .line 309
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$5;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    .line 306
    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 334
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 337
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    .line 334
    invoke-virtual {v0, v1, v4, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 366
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 368
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getPreviewRoi()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getPreviewRoi()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 369
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 373
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 375
    :cond_0
    return-void

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    goto :goto_0
.end method

.method public initButtons()V
    .locals 4

    .prologue
    const v3, 0x14001404

    .line 126
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x14001405

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$2;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x14001402

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$3;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 213
    :cond_0
    return-void
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 383
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initSelectAreaDialog()V

    .line 384
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initSelectSizeDialog()V

    .line 385
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initSelectModeDialog()V

    .line 386
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initSaveAsDialog()V

    .line 387
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initUndoRedoAllDialog()V

    .line 389
    :cond_0
    return-void
.end method

.method public initEffect()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mOriginalInputImage:[I

    .line 105
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->initPinchZoomCallback()V

    .line 106
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 424
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 622
    return-void
.end method

.method public initTrayLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 625
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 629
    :cond_0
    return-void
.end method

.method public initView()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->init(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->setImageEditViewPinchZoomCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;)V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

    if-nez v0, :cond_1

    .line 118
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

    .line 119
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->start()V

    .line 122
    :cond_1
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 506
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 507
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isEnabledDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 521
    :goto_0
    return-void

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_1

    .line 515
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 516
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 517
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 519
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 470
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mCheckBox:Landroid/widget/CheckBox;

    .line 471
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mOriginalInputImage:[I

    .line 472
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContext:Landroid/content/Context;

    .line 473
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 475
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->destroy()V

    .line 476
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 477
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->destroy()V

    .line 478
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    .line 480
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 482
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 483
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 484
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeCancelPressState(Z)V

    .line 485
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    .line 486
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 487
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setMiddleButtonVisible(Z)V

    .line 488
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 489
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPaint:Landroid/graphics/Paint;

    .line 490
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mClearPaint:Landroid/graphics/Paint;

    .line 491
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mLinePaint:Landroid/graphics/Paint;

    .line 492
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 494
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->destroy()V

    .line 498
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 499
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 502
    :cond_1
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 393
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 399
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 400
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPaint:Landroid/graphics/Paint;

    .line 397
    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    if-eqz v0, :cond_1

    .line 416
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->draw(Landroid/graphics/Canvas;Landroid/graphics/Matrix;)V

    .line 418
    :cond_1
    return-void
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 524
    const/16 v0, 0x17

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-ne p1, v0, :cond_3

    .line 526
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 527
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    .line 530
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_2

    .line 531
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 550
    :cond_2
    :goto_0
    return v1

    .line 535
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 537
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->backPressed()V

    goto :goto_0
.end method

.method public onLayout()V
    .locals 4

    .prologue
    .line 1124
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 1125
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1126
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->getImageEditViewHeight()I

    move-result v2

    .line 1127
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$27;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$27;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    .line 1126
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(IILcom/sec/android/mimage/photoretouching/Core/ImageData$setViewSizeMatrixCallback;)V

    .line 1137
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewWidth:I

    .line 1138
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewHeight:I

    .line 1139
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->configurationChanged()V

    .line 1140
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->configurationChange()V

    .line 1152
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPreviewBuffer:[I

    .line 1155
    return-void
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 644
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 637
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_0

    .line 638
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 639
    :cond_0
    return-void
.end method

.method public refreshView()V
    .locals 0

    .prologue
    .line 1352
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 2

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 429
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 430
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 432
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPopupLayout()V

    .line 434
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 435
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    if-eqz v0, :cond_0

    .line 436
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mHelpPopupManager:Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HelpPopupManager;->configurationChange()V

    .line 438
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->setImageEditViewPinchZoomCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;)V

    .line 440
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x1400140b

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 442
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->getSelectAreaDialogPos()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->moveDialog(Landroid/graphics/Point;)V

    .line 454
    :cond_1
    :goto_0
    return-void

    .line 444
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x1400140c

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 446
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->getSelectSizeDialogPos()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->moveDialog(Landroid/graphics/Point;)V

    goto :goto_0

    .line 448
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x1400140d

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 450
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->getSelectModeDialogPos()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->moveDialog(Landroid/graphics/Point;)V

    goto :goto_0
.end method
