.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;
.super Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;
.source "Multigrid6StyleData.java"


# static fields
.field private static final mCollagePreset0:[[F

.field private static final mCollagePreset1:[[F

.field private static final mCollagePreset2:[[F

.field private static final mCollagePreset3:[[F

.field private static final mCollagePreset4:[[F

.field private static final mCollagePreset5:[[F

.field private static final mCollagePreset6:[[F

.field private static final mCollagePreset7:[[F

.field private static final mPilePreset0_1to1:[[F

.field private static final mPilePreset0_9to16:[[F

.field private static final mPilePreset1_1to1:[[F

.field private static final mPilePreset1_9to16:[[F

.field private static final mPilePreset2_1to1:[[F

.field private static final mPilePreset2_9to16:[[F

.field private static final mPilePreset3_1to1:[[F

.field private static final mPilePreset3_9to16:[[F


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 10
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 11
    new-array v1, v4, [F

    .line 12
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v8

    aput-object v1, v0, v5

    .line 14
    new-array v1, v4, [F

    .line 15
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v8

    aput-object v1, v0, v6

    .line 17
    new-array v1, v4, [F

    .line 18
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v8

    aput-object v1, v0, v7

    .line 20
    new-array v1, v4, [F

    .line 21
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v8

    aput-object v1, v0, v8

    .line 23
    new-array v1, v4, [F

    .line 24
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v8

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 26
    new-array v2, v4, [F

    .line 27
    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v3, v2, v5

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v3, v2, v6

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v7

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v8

    aput-object v2, v0, v1

    .line 8
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset7:[[F

    .line 33
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 34
    new-array v1, v4, [F

    .line 35
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v8

    aput-object v1, v0, v5

    .line 37
    new-array v1, v4, [F

    .line 38
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v8

    aput-object v1, v0, v6

    .line 40
    new-array v1, v4, [F

    .line 41
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v8

    aput-object v1, v0, v7

    .line 43
    new-array v1, v4, [F

    .line 44
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v8

    aput-object v1, v0, v8

    .line 46
    new-array v1, v4, [F

    .line 47
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v8

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 49
    new-array v2, v4, [F

    .line 50
    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v3, v2, v5

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v3, v2, v6

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v7

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v8

    aput-object v2, v0, v1

    .line 31
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset6:[[F

    .line 56
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 57
    new-array v1, v4, [F

    .line 58
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v8

    aput-object v1, v0, v5

    .line 60
    new-array v1, v4, [F

    .line 61
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v8

    aput-object v1, v0, v6

    .line 63
    new-array v1, v4, [F

    .line 64
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v8

    aput-object v1, v0, v7

    .line 66
    new-array v1, v4, [F

    .line 67
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v8

    aput-object v1, v0, v8

    .line 69
    new-array v1, v4, [F

    .line 70
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v8

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 72
    new-array v2, v4, [F

    .line 73
    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v3, v2, v5

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v3, v2, v6

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v7

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v8

    aput-object v2, v0, v1

    .line 54
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset5:[[F

    .line 79
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 80
    new-array v1, v4, [F

    .line 81
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v8

    aput-object v1, v0, v5

    .line 83
    new-array v1, v4, [F

    .line 84
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v8

    aput-object v1, v0, v6

    .line 86
    new-array v1, v4, [F

    .line 87
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v8

    aput-object v1, v0, v7

    .line 89
    new-array v1, v4, [F

    .line 90
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v8

    aput-object v1, v0, v8

    .line 92
    new-array v1, v4, [F

    .line 93
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v8

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 95
    new-array v2, v4, [F

    .line 96
    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v3, v2, v5

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v3, v2, v6

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v3, v2, v7

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v8

    aput-object v2, v0, v1

    .line 77
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset4:[[F

    .line 102
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 103
    new-array v1, v4, [F

    .line 104
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v8

    aput-object v1, v0, v5

    .line 106
    new-array v1, v4, [F

    .line 107
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v8

    aput-object v1, v0, v6

    .line 109
    new-array v1, v4, [F

    .line 110
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v8

    aput-object v1, v0, v7

    .line 112
    new-array v1, v4, [F

    .line 113
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v8

    aput-object v1, v0, v8

    .line 115
    new-array v1, v4, [F

    .line 116
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v8

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 118
    new-array v2, v4, [F

    .line 119
    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v3, v2, v5

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v3, v2, v6

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v7

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v8

    aput-object v2, v0, v1

    .line 100
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset3:[[F

    .line 125
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 126
    new-array v1, v4, [F

    .line 127
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v8

    aput-object v1, v0, v5

    .line 129
    new-array v1, v4, [F

    .line 130
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v8

    aput-object v1, v0, v6

    .line 132
    new-array v1, v4, [F

    .line 133
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v8

    aput-object v1, v0, v7

    .line 135
    new-array v1, v4, [F

    .line 136
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v8

    aput-object v1, v0, v8

    .line 138
    new-array v1, v4, [F

    .line 139
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v8

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 141
    new-array v2, v4, [F

    .line 142
    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v3, v2, v5

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v3, v2, v6

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v7

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v8

    aput-object v2, v0, v1

    .line 123
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset2:[[F

    .line 148
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 149
    new-array v1, v4, [F

    .line 150
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v8

    aput-object v1, v0, v5

    .line 152
    new-array v1, v4, [F

    .line 153
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v8

    aput-object v1, v0, v6

    .line 155
    new-array v1, v4, [F

    .line 156
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v8

    aput-object v1, v0, v7

    .line 158
    new-array v1, v4, [F

    .line 159
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v8

    aput-object v1, v0, v8

    .line 161
    new-array v1, v4, [F

    .line 162
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v8

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 164
    new-array v2, v4, [F

    .line 165
    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v3, v2, v5

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v3, v2, v6

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v7

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v8

    aput-object v2, v0, v1

    .line 146
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset1:[[F

    .line 171
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 172
    new-array v1, v4, [F

    .line 173
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v8

    aput-object v1, v0, v5

    .line 175
    new-array v1, v4, [F

    .line 176
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v8

    aput-object v1, v0, v6

    .line 178
    new-array v1, v4, [F

    .line 179
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v8

    aput-object v1, v0, v7

    .line 181
    new-array v1, v4, [F

    .line 182
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p0:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v8

    aput-object v1, v0, v8

    .line 184
    new-array v1, v4, [F

    .line 185
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p33:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v2, v1, v7

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v2, v1, v8

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 187
    new-array v2, v4, [F

    .line 188
    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p50:F

    aput v3, v2, v5

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p66:F

    aput v3, v2, v6

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v7

    sget v3, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->p100:F

    aput v3, v2, v8

    aput-object v2, v0, v1

    .line 169
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset0:[[F

    .line 194
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 195
    new-array v1, v4, [F

    fill-array-data v1, :array_0

    aput-object v1, v0, v5

    .line 196
    new-array v1, v4, [F

    fill-array-data v1, :array_1

    aput-object v1, v0, v6

    .line 197
    new-array v1, v4, [F

    fill-array-data v1, :array_2

    aput-object v1, v0, v7

    .line 198
    new-array v1, v4, [F

    fill-array-data v1, :array_3

    aput-object v1, v0, v8

    .line 199
    new-array v1, v4, [F

    fill-array-data v1, :array_4

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 200
    new-array v2, v4, [F

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    .line 192
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset0_1to1:[[F

    .line 205
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 206
    new-array v1, v4, [F

    fill-array-data v1, :array_6

    aput-object v1, v0, v5

    .line 207
    new-array v1, v4, [F

    fill-array-data v1, :array_7

    aput-object v1, v0, v6

    .line 208
    new-array v1, v4, [F

    fill-array-data v1, :array_8

    aput-object v1, v0, v7

    .line 209
    new-array v1, v4, [F

    fill-array-data v1, :array_9

    aput-object v1, v0, v8

    .line 210
    new-array v1, v4, [F

    fill-array-data v1, :array_a

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 211
    new-array v2, v4, [F

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    .line 203
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset1_1to1:[[F

    .line 216
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 217
    new-array v1, v4, [F

    fill-array-data v1, :array_c

    aput-object v1, v0, v5

    .line 218
    new-array v1, v4, [F

    fill-array-data v1, :array_d

    aput-object v1, v0, v6

    .line 219
    new-array v1, v4, [F

    fill-array-data v1, :array_e

    aput-object v1, v0, v7

    .line 220
    new-array v1, v4, [F

    fill-array-data v1, :array_f

    aput-object v1, v0, v8

    .line 221
    new-array v1, v4, [F

    fill-array-data v1, :array_10

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 222
    new-array v2, v4, [F

    fill-array-data v2, :array_11

    aput-object v2, v0, v1

    .line 214
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset2_1to1:[[F

    .line 227
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 228
    new-array v1, v4, [F

    fill-array-data v1, :array_12

    aput-object v1, v0, v5

    .line 229
    new-array v1, v4, [F

    fill-array-data v1, :array_13

    aput-object v1, v0, v6

    .line 230
    new-array v1, v4, [F

    fill-array-data v1, :array_14

    aput-object v1, v0, v7

    .line 231
    new-array v1, v4, [F

    fill-array-data v1, :array_15

    aput-object v1, v0, v8

    .line 232
    new-array v1, v4, [F

    fill-array-data v1, :array_16

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 233
    new-array v2, v4, [F

    fill-array-data v2, :array_17

    aput-object v2, v0, v1

    .line 225
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset3_1to1:[[F

    .line 238
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 239
    new-array v1, v4, [F

    fill-array-data v1, :array_18

    aput-object v1, v0, v5

    .line 240
    new-array v1, v4, [F

    fill-array-data v1, :array_19

    aput-object v1, v0, v6

    .line 241
    new-array v1, v4, [F

    fill-array-data v1, :array_1a

    aput-object v1, v0, v7

    .line 242
    new-array v1, v4, [F

    fill-array-data v1, :array_1b

    aput-object v1, v0, v8

    .line 243
    new-array v1, v4, [F

    fill-array-data v1, :array_1c

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 244
    new-array v2, v4, [F

    fill-array-data v2, :array_1d

    aput-object v2, v0, v1

    .line 236
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset0_9to16:[[F

    .line 249
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 250
    new-array v1, v4, [F

    fill-array-data v1, :array_1e

    aput-object v1, v0, v5

    .line 251
    new-array v1, v4, [F

    fill-array-data v1, :array_1f

    aput-object v1, v0, v6

    .line 252
    new-array v1, v4, [F

    fill-array-data v1, :array_20

    aput-object v1, v0, v7

    .line 253
    new-array v1, v4, [F

    fill-array-data v1, :array_21

    aput-object v1, v0, v8

    .line 254
    new-array v1, v4, [F

    fill-array-data v1, :array_22

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 255
    new-array v2, v4, [F

    fill-array-data v2, :array_23

    aput-object v2, v0, v1

    .line 247
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset1_9to16:[[F

    .line 260
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 262
    new-array v1, v4, [F

    fill-array-data v1, :array_24

    aput-object v1, v0, v5

    .line 263
    new-array v1, v4, [F

    fill-array-data v1, :array_25

    aput-object v1, v0, v6

    .line 264
    new-array v1, v4, [F

    fill-array-data v1, :array_26

    aput-object v1, v0, v7

    .line 265
    new-array v1, v4, [F

    fill-array-data v1, :array_27

    aput-object v1, v0, v8

    .line 266
    new-array v1, v4, [F

    fill-array-data v1, :array_28

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 267
    new-array v2, v4, [F

    fill-array-data v2, :array_29

    aput-object v2, v0, v1

    .line 258
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset2_9to16:[[F

    .line 272
    const/4 v0, 0x6

    new-array v0, v0, [[F

    .line 273
    new-array v1, v4, [F

    fill-array-data v1, :array_2a

    aput-object v1, v0, v5

    .line 274
    new-array v1, v4, [F

    fill-array-data v1, :array_2b

    aput-object v1, v0, v6

    .line 275
    new-array v1, v4, [F

    fill-array-data v1, :array_2c

    aput-object v1, v0, v7

    .line 276
    new-array v1, v4, [F

    fill-array-data v1, :array_2d

    aput-object v1, v0, v8

    .line 277
    new-array v1, v4, [F

    fill-array-data v1, :array_2e

    aput-object v1, v0, v4

    const/4 v1, 0x5

    .line 278
    new-array v2, v4, [F

    fill-array-data v2, :array_2f

    aput-object v2, v0, v1

    .line 270
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset3_9to16:[[F

    .line 279
    return-void

    .line 195
    :array_0
    .array-data 4
        -0x425566cf    # -0.0833f
        0x0
        0x3ed82a99    # 0.4222f
        0x3f182a99    # 0.5944f
    .end array-data

    .line 196
    :array_1
    .array-data 4
        0x3f305bc0    # 0.6889f
        0x3dd844d0    # 0.1056f
        0x3f938ef3    # 1.1528f
        0x3f266666    # 0.65f
    .end array-data

    .line 197
    :array_2
    .array-data 4
        0x3ea7d567    # 0.3278f
        -0x42beab36    # -0.0472f
        0x3f5779a7    # 0.8417f
        0x3eba511a    # 0.3639f
    .end array-data

    .line 198
    :array_3
    .array-data 4
        -0x427765fe    # -0.0667f
        0x3efbc01a    # 0.4917f
        0x3f071de7    # 0.5278f
        0x3f8fa440    # 1.1222f
    .end array-data

    .line 199
    :array_4
    .array-data 4
        0x3f071de7    # 0.5278f
        0x3f182a99    # 0.5944f
        0x3f860aa6    # 1.0472f
        0x3f83eab3    # 1.0306f
    .end array-data

    .line 200
    :array_5
    .array-data 4
        0x3e666666    # 0.225f
        0x3eaef34d    # 0.3417f
        0x3f3f4880    # 0.7472f
        0x3f42d773    # 0.7611f
    .end array-data

    .line 206
    :array_6
    .array-data 4
        -0x44487fcc    # -0.0056f
        0x3ebe9100    # 0.3722f
        0x3eba511a    # 0.3639f
        0x3f54a234    # 0.8306f
    .end array-data

    .line 207
    :array_7
    .array-data 4
        0x3f2b5dcc    # 0.6694f
        0x3ec2de01    # 0.3806f
        0x3f800000    # 1.0f
        0x3f4d844d    # 0.8028f
    .end array-data

    .line 208
    :array_8
    .array-data 4
        0x3e888ce7    # 0.2667f
        0x3f093dd9    # 0.5361f
        0x3f3f4880    # 0.7472f
        0x3f693dd9    # 0.9111f
    .end array-data

    .line 209
    :array_9
    .array-data 4
        -0x439c432d    # -0.0139f
        0x3e5288ce    # 0.2056f
        0x3ebd21ff    # 0.3694f
        0x3f02d773    # 0.5111f
    .end array-data

    .line 210
    :array_a
    .array-data 4
        0x3f160aa6    # 0.5861f
        0x3e4faace    # 0.2028f
        0x3f78e219    # 0.9722f
        0x3f038ef3    # 0.5139f
    .end array-data

    .line 211
    :array_b
    .array-data 4
        0x3eb61134    # 0.3556f
        0x3e1119ce    # 0.1417f
        0x3f3779a7    # 0.7167f
        0x3f1b089a    # 0.6056f
    .end array-data

    .line 217
    :array_c
    .array-data 4
        0x3f2ccccd    # 0.675f
        0x3e087fcc    # 0.1333f
        0x3f755326    # 0.9583f
        0x3efbc01a    # 0.4917f
    .end array-data

    .line 218
    :array_d
    .array-data 4
        0x3ea7d567    # 0.3278f
        0x3e27d567    # 0.1639f
        0x3f2e3bcd    # 0.6806f
        0x3edf4880    # 0.4361f
    .end array-data

    .line 219
    :array_e
    .array-data 4
        0x3c9eecc0    # 0.0194f
        0x3dddcc64    # 0.1083f
        0x3ead844d    # 0.3389f
        0x3f016f00    # 0.5056f
    .end array-data

    .line 220
    :array_f
    .array-data 4
        0x3f2d844d    # 0.6778f
        0x3f14a234    # 0.5806f
        0x3f711340    # 0.9417f
        0x3f693dd9    # 0.9111f
    .end array-data

    .line 221
    :array_10
    .array-data 4
        0x3ebe9100    # 0.3722f
        0x3f11c433    # 0.5694f
        0x3f266666    # 0.65f
        0x3f6b5dcc    # 0.9194f
    .end array-data

    .line 222
    :array_11
    .array-data 4
        0x3d1f559b    # 0.0389f
        0x3f16c227    # 0.5889f
        0x3eac154d    # 0.3361f
        0x3f511340    # 0.8167f
    .end array-data

    .line 228
    :array_12
    .array-data 4
        0x3e0b5dcc    # 0.1361f
        0x3e3bb2ff    # 0.1833f
        0x3ec43fe6    # 0.3833f
        0x3f016f00    # 0.5056f
    .end array-data

    .line 229
    :array_13
    .array-data 4
        0x3f11c433    # 0.5694f
        0x3e087fcc    # 0.1333f
        0x3f311340    # 0.6917f
        0x3e800000    # 0.25f
    .end array-data

    .line 230
    :array_14
    .array-data 4
        0x3f1a511a    # 0.6028f
        0x3ecfaace    # 0.4056f
        0x3f616f00    # 0.8806f
        0x3f13eab3    # 0.5778f
    .end array-data

    .line 231
    :array_15
    .array-data 4
        0x3df487fd    # 0.1194f
        0x3f3bb98c    # 0.7333f
        0x3e82de01    # 0.2556f
        0x3f5a511a    # 0.8528f
    .end array-data

    .line 232
    :array_16
    .array-data 4
        0x3ecccccd    # 0.4f
        0x3f3d288d    # 0.7389f
        0x3f13eab3    # 0.5778f
        0x3f638ef3    # 0.8889f
    .end array-data

    .line 233
    :array_17
    .array-data 4
        0x3f3a511a    # 0.7278f
        0x3f3b089a    # 0.7306f
        0x3f5e9100    # 0.8694f
        0x3f582a99    # 0.8444f
    .end array-data

    .line 239
    :array_18
    .array-data 4
        -0x43611340    # -0.0194f
        0x3f0b98c8    # 0.5453f
        0x3f0eecc0    # 0.5583f
        0x3f6f34d7    # 0.9344f
    .end array-data

    .line 240
    :array_19
    .array-data 4
        0x3eec154d    # 0.4611f
        0x3c6703b0    # 0.0141f
        0x3f8e9446    # 1.1139f
        0x3ee3fe5d    # 0.4453f
    .end array-data

    .line 241
    :array_1a
    .array-data 4
        0x3ece3bcd    # 0.4028f
        0x3ed33333    # 0.4125f
        0x3f8bbcd3    # 1.0917f
        0x3f3ccccd    # 0.7375f
    .end array-data

    .line 242
    :array_1b
    .array-data 4
        -0x425aee63    # -0.0806f
        0x3bce703b    # 0.0063f
        0x3f14a234    # 0.5806f
        0x3ea339c1    # 0.3188f
    .end array-data

    .line 243
    :array_1c
    .array-data 4
        -0x439c432d    # -0.0139f
        0x3e6809d5    # 0.2266f
        0x3f266666    # 0.65f
        0x3f22ca58    # 0.6359f
    .end array-data

    .line 244
    :array_1d
    .array-data 4
        0x3ebbc01a    # 0.3667f
        0x3f2e00d2    # 0.6797f
        0x3f83eab3    # 1.0306f
        0x3f7e00d2    # 0.9922f
    .end array-data

    .line 250
    :array_1e
    .array-data 4
        0x3d63bcd3    # 0.0556f
        0x3f2d97f6    # 0.6781f
        0x3efd21ff    # 0.4944f
        0x3f7c01a3    # 0.9844f
    .end array-data

    .line 251
    :array_1f
    .array-data 4
        0x3ed9999a    # 0.425f
        0x3f35fd8b    # 0.7109f
        0x3f79999a    # 0.975f
        0x3f753261    # 0.9578f
    .end array-data

    .line 252
    :array_20
    .array-data 4
        -0x42ec2268    # -0.0361f
        0x3ed0cb29    # 0.4078f
        0x3f0ccccd    # 0.55f
        0x3f2a64c3    # 0.6656f
    .end array-data

    .line 253
    :array_21
    .array-data 4
        0x3f13eab3    # 0.5778f
        0x3ed4c986    # 0.4156f
        0x3f7a511a    # 0.9778f
        0x3f333333    # 0.7f
    .end array-data

    .line 254
    :array_22
    .array-data 4
        0x3d8e2196    # 0.0694f
        0x3ccccccd    # 0.025f
        0x3f182a99    # 0.5944f
        0x3ece6320    # 0.4031f
    .end array-data

    .line 255
    :array_23
    .array-data 4
        0x3ee66666    # 0.45f
        0x3e1b3d08    # 0.1516f
        0x3f82233a    # 1.0167f
        0x3ed19ce0    # 0.4094f
    .end array-data

    .line 262
    :array_24
    .array-data 4
        0x3f2c154d    # 0.6722f
        0x3e3b2fec    # 0.1828f
        0x3f9a511a    # 1.2056f
        0x3ed80347    # 0.4219f
    .end array-data

    .line 263
    :array_25
    .array-data 4
        0x3e910cb3    # 0.2833f
        0x3e3e5c92    # 0.1859f
        0x3f42d773    # 0.7611f
        0x3f046738    # 0.5172f
    .end array-data

    .line 264
    :array_26
    .array-data 4
        -0x426c2268    # -0.0722f
        0x3e3b2fec    # 0.1828f
        0x3ead844d    # 0.3389f
        0x3ef66cf4    # 0.4813f
    .end array-data

    .line 265
    :array_27
    .array-data 4
        0x3f39999a    # 0.725f
        0x3f113405    # 0.5672f
        0x3f8c710d    # 1.0972f
        0x3f54d014    # 0.8313f
    .end array-data

    .line 266
    :array_28
    .array-data 4
        0x3eaaa64c    # 0.3333f
        0x3f139c0f    # 0.5766f
        0x3f34a234    # 0.7056f
        0x3f5930be    # 0.8484f
    .end array-data

    .line 267
    :array_29
    .array-data 4
        -0x4222339c    # -0.1083f
        0x3f12680a    # 0.5719f
        0x3ec16f00    # 0.3778f
        0x3f4b367a    # 0.7938f
    .end array-data

    .line 273
    :array_2a
    .array-data 4
        0x3e582a99    # 0.2111f
        0x3da6809d    # 0.0813f
        0x3f466666    # 0.775f
        0x3e90068e    # 0.2813f
    .end array-data

    .line 274
    :array_2b
    .array-data 4
        0x3e16bb99    # 0.1472f
        0x3edcd35b    # 0.4313f
        0x3edb089a    # 0.4278f
        0x3f1b98c8    # 0.6078f
    .end array-data

    .line 275
    :array_2c
    .array-data 4
        0x3f2fa440    # 0.6861f
        0x3ed59b3d    # 0.4172f
        0x3f51c433    # 0.8194f
        0x3ef9999a    # 0.4875f
    .end array-data

    .line 276
    :array_2d
    .array-data 4
        0x3f2fa440    # 0.6861f
        0x3f180347    # 0.5938f
        0x3f51c433    # 0.8194f
        0x3f2a0275    # 0.6641f
    .end array-data

    .line 277
    :array_2e
    .array-data 4
        0x3e4ccccd    # 0.2f
        0x3f4a0275    # 0.7891f
        0x3eb05532    # 0.3444f
        0x3f613405    # 0.8797f
    .end array-data

    .line 278
    :array_2f
    .array-data 4
        0x3f0eecc0    # 0.5583f
        0x3f473190    # 0.7781f
        0x3f616f00    # 0.8806f
        0x3f633333    # 0.8875f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;-><init>()V

    return-void
.end method

.method public static getDrawRects(II)[[F
    .locals 4
    .param p0, "style"    # I
    .param p1, "proportion"    # I

    .prologue
    const/high16 v3, 0x1e500000

    .line 284
    const/4 v0, 0x0

    .line 286
    .local v0, "returnPreset":[[F
    const/high16 v1, 0x1e200000

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getMaxStyleNum(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    move-result p0

    .line 288
    packed-switch p0, :pswitch_data_0

    .line 368
    const/4 v0, 0x0

    .line 372
    :goto_0
    return-object v0

    .line 291
    :pswitch_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset0:[[F

    .line 292
    goto :goto_0

    .line 295
    :pswitch_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset1:[[F

    .line 296
    goto :goto_0

    .line 299
    :pswitch_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset2:[[F

    .line 300
    goto :goto_0

    .line 303
    :pswitch_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset3:[[F

    .line 304
    goto :goto_0

    .line 307
    :pswitch_4
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset4:[[F

    .line 308
    goto :goto_0

    .line 311
    :pswitch_5
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset5:[[F

    .line 312
    goto :goto_0

    .line 315
    :pswitch_6
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset6:[[F

    .line 316
    goto :goto_0

    .line 319
    :pswitch_7
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mCollagePreset7:[[F

    .line 320
    goto :goto_0

    .line 324
    :pswitch_8
    if-ne p1, v3, :cond_0

    .line 326
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset0_1to1:[[F

    .line 327
    goto :goto_0

    .line 330
    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset0_9to16:[[F

    .line 332
    goto :goto_0

    .line 335
    :pswitch_9
    if-ne p1, v3, :cond_1

    .line 337
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset1_1to1:[[F

    .line 338
    goto :goto_0

    .line 341
    :cond_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset1_9to16:[[F

    .line 343
    goto :goto_0

    .line 346
    :pswitch_a
    if-ne p1, v3, :cond_2

    .line 348
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset2_1to1:[[F

    .line 349
    goto :goto_0

    .line 352
    :cond_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset2_9to16:[[F

    .line 354
    goto :goto_0

    .line 357
    :pswitch_b
    if-ne p1, v3, :cond_3

    .line 359
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset3_1to1:[[F

    .line 360
    goto :goto_0

    .line 363
    :cond_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid6StyleData;->mPilePreset3_9to16:[[F

    .line 365
    goto :goto_0

    .line 288
    :pswitch_data_0
    .packed-switch 0x1e200000
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method


# virtual methods
.method public getRotate(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 379
    const/4 v0, 0x0

    return v0
.end method
