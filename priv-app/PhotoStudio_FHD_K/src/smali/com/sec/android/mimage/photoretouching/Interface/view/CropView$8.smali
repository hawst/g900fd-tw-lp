.class Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;
.super Ljava/lang/Object;
.source "CropView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->initButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    .line 365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 426
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 375
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->onButtonClicked(Landroid/view/View;)V
    invoke-static {v2, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;Landroid/view/View;)V

    .line 376
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v2

    if-nez v2, :cond_4

    .line 378
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->getToggleRotate()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 379
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 388
    .end local v1    # "i":I
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->setToggleRotate(Z)V

    .line 392
    :goto_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->getToggleRotate()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object v2, p1

    .line 393
    check-cast v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    const v5, 0x7f020168

    const-string v6, "9:16"

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->changeRes(ILjava/lang/String;)V

    .line 397
    :goto_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v2

    invoke-virtual {v2, p1, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(Landroid/view/View;Z)V

    .line 398
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->resetZoom()V

    .line 399
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 400
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->pinchZoom()V

    .line 401
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v2

    const v4, 0x11201304

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->init(ILcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 403
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 404
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mIsLassoPressed:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 406
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;Z)V

    .line 407
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->calculateInitialValues(Z)V

    .line 408
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->startAnimation()V

    .line 409
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    iput v3, v2, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->i:I

    .line 410
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->calculateanimationintertval()V

    .line 411
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->drawRunner:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 419
    :goto_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v2

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 420
    return-void

    .line 381
    .restart local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    .line 382
    .local v0, "btn":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->getButtonType()I

    move-result v2

    const v5, 0x11201303

    if-ne v2, v5, :cond_3

    .line 384
    const v2, 0x7f020164

    const-string v5, "4:3"

    invoke-virtual {v0, v2, v5}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->changeRes(ILjava/lang/String;)V

    goto/16 :goto_1

    .line 379
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 391
    .end local v0    # "btn":Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;
    .end local v1    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v5

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->mCropEffect:Lcom/sec/android/mimage/photoretouching/Core/CropEffect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;)Lcom/sec/android/mimage/photoretouching/Core/CropEffect;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->getToggleRotate()Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v3

    :goto_5
    invoke-virtual {v5, v2}, Lcom/sec/android/mimage/photoretouching/Core/CropEffect;->setToggleRotate(Z)V

    goto/16 :goto_2

    :cond_5
    move v2, v4

    goto :goto_5

    :cond_6
    move-object v2, p1

    .line 395
    check-cast v2, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;

    const v5, 0x7f02016b

    const-string v6, "16:9"

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/mimage/photoretouching/Gui/BottomIconButtonLayout;->changeRes(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 415
    :cond_7
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;Z)V

    .line 416
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView$8;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CropView;->invalidateViews()V

    goto :goto_4
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 371
    return-void
.end method
