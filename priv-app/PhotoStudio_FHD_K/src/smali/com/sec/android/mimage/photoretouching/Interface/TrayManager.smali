.class public Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
.super Ljava/lang/Object;
.source "TrayManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$OnMultigridEnd;,
        Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;
    }
.end annotation


# instance fields
.field private mButtonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

.field private mDecodingAllFinish:Z

.field private mFromOtherApp:Z

.field private mIsLongClick:Z

.field protected mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

.field private mTrayTouchFunction:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

.field private mViewFrameInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

.field private mVisible:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1126
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    .line 1127
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mViewFrameInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    .line 1128
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 1130
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    .line 1131
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayTouchFunction:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    .line 1133
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mVisible:Z

    .line 1135
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mIsLongClick:Z

    .line 1136
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mFromOtherApp:Z

    .line 1137
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mDecodingAllFinish:Z

    .line 1138
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    .line 53
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    .line 55
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    .line 56
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0900b3

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    .line 57
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initBodyListener()V

    .line 58
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 60
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->addFirstButton(Ljava/util/ArrayList;)V

    .line 61
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;
    .locals 1

    .prologue
    .line 1131
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayTouchFunction:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 1130
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Landroid/view/View;)I
    .locals 1

    .prologue
    .line 816
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteFunction(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 762
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    .locals 1

    .prologue
    .line 1128
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;
    .locals 1

    .prologue
    .line 1127
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mViewFrameInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Z
    .locals 1

    .prologue
    .line 1135
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mIsLongClick:Z

    return v0
.end method

.method private addButton(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V
    .locals 9
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .param p2, "decodeFrom"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v7, -0x1

    .line 878
    if-eqz p2, :cond_0

    if-ne p2, v8, :cond_1

    .line 880
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->removeAllViews()V

    .line 881
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_4

    .line 885
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 888
    :cond_1
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)V

    .line 931
    .local v2, "deleteTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$12;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)V

    .line 959
    .local v1, "buttonTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    const/4 v0, 0x0

    .line 960
    .local v0, "button":Landroid/view/View;
    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mIsLongClick:Z

    if-eqz v4, :cond_5

    .line 962
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6, v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->addButton(Ljava/util/ArrayList;ZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    move-result-object v0

    :goto_1
    move-object v4, v0

    .line 968
    check-cast v4, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    iput-object v4, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->trayButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 970
    if-eqz p2, :cond_2

    if-ne p2, v8, :cond_7

    .line 972
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mViewFrameInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    if-eqz v4, :cond_3

    iget-boolean v4, p1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;->isMain:Z

    if-eqz v4, :cond_3

    .line 974
    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton(Landroid/view/View;)V

    .line 975
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    if-eqz v4, :cond_6

    .line 977
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getIndexFromTrayButtonList()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->changeImages(I)V

    .line 1002
    :cond_3
    :goto_2
    return-void

    .line 881
    .end local v0    # "button":Landroid/view/View;
    .end local v1    # "buttonTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .end local v2    # "deleteTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 883
    .local v3, "trayButton":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->destroy()V

    goto :goto_0

    .line 966
    .end local v3    # "trayButton":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    .restart local v0    # "button":Landroid/view/View;
    .restart local v1    # "buttonTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .restart local v2    # "deleteTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    :cond_5
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v5, v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->addButton(Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    move-result-object v0

    goto :goto_1

    .line 981
    :cond_6
    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->changeImages(I)V

    goto :goto_2

    .line 987
    :cond_7
    iget-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mIsLongClick:Z

    if-nez v4, :cond_8

    .line 989
    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton(Landroid/view/View;)V

    .line 991
    :cond_8
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mViewFrameInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    if-eqz v4, :cond_3

    .line 993
    const/4 v4, 0x7

    if-eq p2, v4, :cond_3

    .line 995
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    if-eqz v4, :cond_9

    .line 996
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getIndexFromTrayButtonList()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->changeImages(I)V

    goto :goto_2

    .line 998
    :cond_9
    invoke-virtual {p0, v7}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->changeImages(I)V

    goto :goto_2
.end method

.method private deleteFunction(Landroid/view/View;)I
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 818
    const/4 v5, -0x1

    .line 819
    .local v5, "ret":I
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 820
    .local v4, "parent":Landroid/view/ViewGroup;
    if-eqz v4, :cond_2

    .line 823
    :try_start_0
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 824
    .local v2, "grandParent":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 825
    .local v3, "idx":I
    add-int/lit8 v5, v3, -0x1

    .line 826
    if-le v3, v8, :cond_4

    .line 828
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    const v7, 0x7f09014a

    invoke-virtual {v6, v7}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 830
    .local v0, "body":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 831
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    .line 833
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 836
    :cond_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    if-ne v6, v2, :cond_1

    .line 838
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton(Landroid/view/View;)V

    .line 841
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->destroy()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 842
    const/4 v2, 0x0

    .line 872
    .end local v0    # "body":Landroid/widget/LinearLayout;
    .end local v2    # "grandParent":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    .end local v3    # "idx":I
    :cond_2
    :goto_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->isEnabled()Z

    move-result v6

    if-nez v6, :cond_3

    .line 873
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->enable()V

    .line 874
    :cond_3
    return v5

    .line 846
    .restart local v2    # "grandParent":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    .restart local v3    # "idx":I
    :cond_4
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    const v7, 0x7f09014a

    invoke-virtual {v6, v7}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 848
    .restart local v0    # "body":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 849
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v6, :cond_5

    .line 851
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 854
    :cond_5
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    if-ne v6, v2, :cond_6

    .line 856
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-le v6, v8, :cond_7

    .line 858
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton(Landroid/view/View;)V

    .line 865
    :cond_6
    :goto_1
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->destroy()V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 868
    .end local v0    # "body":Landroid/widget/LinearLayout;
    .end local v2    # "grandParent":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    .end local v3    # "idx":I
    :catch_0
    move-exception v1

    .line 869
    .local v1, "e":Ljava/lang/ClassCastException;
    invoke-virtual {v1}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    .line 862
    .end local v1    # "e":Ljava/lang/ClassCastException;
    .restart local v0    # "body":Landroid/widget/LinearLayout;
    .restart local v2    # "grandParent":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    .restart local v3    # "idx":I
    :cond_7
    const/4 v6, 0x0

    :try_start_2
    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton(Landroid/view/View;)V
    :try_end_2
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method private initBodyListener()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1005
    const/4 v0, 0x0

    .line 1006
    .local v0, "body":Landroid/view/ViewGroup;
    const/4 v2, 0x0

    .line 1007
    .local v2, "v":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    if-eqz v3, :cond_0

    .line 1008
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    const v4, 0x7f09014a

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "body":Landroid/view/ViewGroup;
    check-cast v0, Landroid/view/ViewGroup;

    .line 1009
    .restart local v0    # "body":Landroid/view/ViewGroup;
    :cond_0
    if-eqz v0, :cond_1

    .line 1010
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .end local v2    # "v":Landroid/view/View;
    check-cast v2, Landroid/view/View;

    .line 1011
    .restart local v2    # "v":Landroid/view/View;
    :cond_1
    if-eqz v2, :cond_3

    instance-of v3, v2, Landroid/widget/HorizontalScrollView;

    if-eqz v3, :cond_3

    move-object v1, v2

    .line 1013
    check-cast v1, Landroid/widget/HorizontalScrollView;

    .line 1014
    .local v1, "scroll":Landroid/widget/HorizontalScrollView;
    invoke-virtual {v1, v5}, Landroid/widget/HorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 1015
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$13;

    invoke-direct {v3, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Landroid/widget/HorizontalScrollView;)V

    invoke-virtual {v1, v3}, Landroid/widget/HorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1066
    .end local v1    # "scroll":Landroid/widget/HorizontalScrollView;
    :cond_2
    :goto_0
    return-void

    .line 1038
    :cond_3
    instance-of v3, v2, Landroid/widget/ScrollView;

    if-eqz v3, :cond_2

    move-object v1, v2

    .line 1040
    check-cast v1, Landroid/widget/ScrollView;

    .line 1041
    .local v1, "scroll":Landroid/widget/ScrollView;
    invoke-virtual {v1, v5}, Landroid/widget/ScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 1042
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$14;

    invoke-direct {v3, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Landroid/widget/ScrollView;)V

    invoke-virtual {v1, v3}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method private initCurrentButton(Landroid/view/View;)V
    .locals 4
    .param p1, "button"    # Landroid/view/View;

    .prologue
    .line 764
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 776
    check-cast p1, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .end local p1    # "button":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 777
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    if-eqz v2, :cond_0

    .line 779
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getId()I

    .line 784
    :cond_0
    return-void

    .line 766
    .restart local p1    # "button":Landroid/view/View;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 767
    .local v0, "b":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    if-eq v0, p1, :cond_2

    .line 768
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$10;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 764
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private reloadButton(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V
    .locals 2
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .param p2, "decodeFrom"    # I

    .prologue
    .line 754
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 756
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 758
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->reloadDecodeInfo(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V

    .line 761
    :cond_0
    return-void
.end method

.method private saveImageData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Ljava/lang/String;)V
    .locals 10
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 494
    const/4 v7, 0x0

    .line 495
    .local v7, "format":Landroid/graphics/Bitmap$CompressFormat;
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 496
    .local v1, "srcPath":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 497
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 498
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v4

    .line 499
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v6

    const v8, 0x7a1200

    move-object v3, p2

    .line 498
    invoke-static/range {v0 .. v8}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->saveToImage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[IIILandroid/graphics/Bitmap$CompressFormat;I)Landroid/net/Uri;

    move-result-object v9

    .line 500
    .local v9, "uri":Landroid/net/Uri;
    if-eqz v9, :cond_1

    .line 501
    invoke-virtual {p1, v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setUri(Landroid/net/Uri;)V

    .line 504
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setSaved()V

    .line 505
    return-void

    .line 503
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPath(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private saveImageData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 10
    .param p1, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "quality"    # I

    .prologue
    .line 508
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 509
    .local v1, "srcPath":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 510
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 512
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isPersonalPage()Z

    move-result v0

    if-nez v0, :cond_2

    .line 514
    sget-object p2, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    .line 522
    :cond_1
    :goto_0
    const/4 v7, 0x0

    .line 523
    .local v7, "format":Landroid/graphics/Bitmap$CompressFormat;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Core/Image;->isPng()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 524
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    .line 525
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".png"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 533
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v4

    .line 534
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v6

    move-object v2, p2

    move-object v3, p3

    move v8, p4

    .line 533
    invoke-static/range {v0 .. v8}, Lcom/sec/android/mimage/photoretouching/Core/FileData;->saveToImage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[IIILandroid/graphics/Bitmap$CompressFormat;I)Landroid/net/Uri;

    move-result-object v9

    .line 536
    .local v9, "uri":Landroid/net/Uri;
    if-eqz v9, :cond_4

    .line 537
    invoke-virtual {p1, v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setUri(Landroid/net/Uri;)V

    .line 540
    :goto_2
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setSaved()V

    .line 541
    const-string v0, "JW saveImageData: uri created"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 542
    return-void

    .line 518
    .end local v7    # "format":Landroid/graphics/Bitmap$CompressFormat;
    .end local v9    # "uri":Landroid/net/Uri;
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPersonalPagePath()Ljava/lang/String;

    move-result-object v0

    const-string v2, "/storage/Private"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "/storage/Private"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Studio"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 529
    .restart local v7    # "format":Landroid/graphics/Bitmap$CompressFormat;
    :cond_3
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    .line 530
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 539
    .restart local v9    # "uri":Landroid/net/Uri;
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPath(Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public changeImages(I)V
    .locals 1
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 798
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mViewFrameInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    if-eqz v0, :cond_0

    .line 800
    const-string v0, "normal tray changeImage"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 801
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mViewFrameInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->changeImage(I)V

    .line 803
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mViewFrameInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;->initTrayLayout()V

    .line 805
    :cond_0
    return-void
.end method

.method public contain(Landroid/net/Uri;)Z
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const v8, 0x7f0600da

    .line 579
    const/4 v5, 0x0

    .line 580
    .local v5, "ret":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v1, v6, :cond_0

    .line 609
    :goto_1
    return v5

    .line 582
    :cond_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 583
    .local v0, "button":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    .line 584
    .local v2, "imageData":Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    if-eqz v2, :cond_2

    .line 586
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v6

    if-nez v6, :cond_1

    .line 588
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 589
    .local v4, "path":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    invoke-static {v6, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 590
    .local v3, "loadedPath":Ljava/lang/String;
    if-eqz v4, :cond_2

    invoke-virtual {v4, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 592
    const/4 v5, 0x1

    .line 593
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    invoke-static {v6, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    goto :goto_1

    .line 599
    .end local v3    # "loadedPath":Ljava/lang/String;
    .end local v4    # "path":Ljava/lang/String;
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "uri.compareTo(imageData.getUri():"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 600
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    move-result v6

    if-nez v6, :cond_2

    .line 602
    const/4 v5, 0x1

    .line 603
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    invoke-static {v6, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;I)V

    goto :goto_1

    .line 580
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public deleteButton(Landroid/graphics/Bitmap;)V
    .locals 11
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const v10, 0x7f09014a

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 331
    const/4 v5, 0x0

    .line 332
    .local v5, "ret":I
    const/4 v2, 0x0

    .line 333
    .local v2, "deleteButton":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_5

    .line 345
    :goto_0
    if-eqz v2, :cond_3

    .line 347
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 348
    .local v4, "idx":I
    add-int/lit8 v5, v4, -0x1

    .line 349
    if-le v4, v8, :cond_6

    .line 351
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v6, v10}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 353
    .local v0, "body":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 354
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v6, :cond_1

    .line 356
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 359
    :cond_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    if-ne v6, v2, :cond_2

    .line 361
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton(Landroid/view/View;)V

    .line 364
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->destroy()V

    .line 365
    const/4 v2, 0x0

    .line 393
    .end local v0    # "body":Landroid/widget/LinearLayout;
    .end local v4    # "idx":I
    :cond_3
    :goto_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->isEnabled()Z

    move-result v6

    if-nez v6, :cond_4

    .line 394
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->enable()V

    .line 395
    :cond_4
    return-void

    .line 333
    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 335
    .local v1, "button":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    .line 336
    .local v3, "iData":Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    if-eqz v3, :cond_0

    .line 338
    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalInputBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    if-ne p1, v7, :cond_0

    .line 340
    move-object v2, v1

    .line 341
    goto :goto_0

    .line 369
    .end local v1    # "button":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    .end local v3    # "iData":Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .restart local v4    # "idx":I
    :cond_6
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v6, v10}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 371
    .restart local v0    # "body":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 372
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v6, :cond_7

    .line 374
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 377
    :cond_7
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    if-ne v6, v2, :cond_8

    .line 379
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-le v6, v8, :cond_9

    .line 381
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton(Landroid/view/View;)V

    .line 389
    :cond_8
    :goto_2
    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->destroy()V

    .line 390
    const/4 v2, 0x0

    goto :goto_1

    .line 385
    :cond_9
    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton(Landroid/view/View;)V

    goto :goto_2
.end method

.method public deleteCurrentButton()V
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteFunction(Landroid/view/View;)I

    .line 720
    return-void
.end method

.method public destroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 282
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 283
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->destroy()V

    .line 284
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    .line 285
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 287
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 292
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 294
    .end local v1    # "i":I
    :cond_0
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    .line 295
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    .line 296
    return-void

    .line 289
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 290
    .local v0, "bData":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->destroy()V

    .line 287
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getButtonSize()I
    .locals 2

    .prologue
    .line 700
    const/4 v0, 0x0

    .line 701
    .local v0, "ret":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 702
    return v0
.end method

.method public getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 4

    .prologue
    .line 737
    const/4 v1, 0x0

    .line 738
    .local v1, "ret":Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 740
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 742
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 743
    .local v0, "button":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    .line 746
    .end local v0    # "button":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    :cond_0
    return-object v1
.end method

.method public getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 4

    .prologue
    .line 706
    const/4 v1, 0x0

    .line 707
    .local v1, "ret":Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 709
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 711
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 712
    .local v0, "button":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    .line 715
    .end local v0    # "button":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    :cond_0
    return-object v1
.end method

.method public getImageData(I)Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .locals 4
    .param p1, "idx"    # I

    .prologue
    .line 723
    const/4 v2, 0x0

    .line 724
    .local v2, "ret":Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    add-int/lit8 v1, p1, 0x1

    .line 725
    .local v1, "realIdx":I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v1, :cond_0

    .line 727
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 728
    .local v0, "button":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    if-eqz v0, :cond_0

    .line 730
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    .line 733
    .end local v0    # "button":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    :cond_0
    return-object v2
.end method

.method public hasModifiedImages()Z
    .locals 6

    .prologue
    .line 477
    const/4 v2, 0x0

    .line 478
    .local v2, "ret":Z
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    if-nez v5, :cond_0

    move v3, v2

    .line 490
    .end local v2    # "ret":Z
    .local v3, "ret":I
    :goto_0
    return v3

    .line 480
    .end local v3    # "ret":I
    .restart local v2    # "ret":Z
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v0, v5, :cond_1

    :goto_2
    move v3, v2

    .line 490
    .restart local v3    # "ret":I
    goto :goto_0

    .line 482
    .end local v3    # "ret":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 483
    .local v4, "trayButton":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    .line 484
    .local v1, "imageData":Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 486
    const/4 v2, 0x1

    .line 487
    goto :goto_2

    .line 480
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->hideTray()V

    .line 328
    return-void
.end method

.method public init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V
    .locals 4
    .param p1, "visibility"    # Z
    .param p2, "viewFrameInterface"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;
    .param p3, "trayTouchFunction"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    .prologue
    .line 67
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 69
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0900b3

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    .line 70
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->addFirstButton(Ljava/util/ArrayList;)V

    .line 72
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mVisible:Z

    .line 73
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mIsLongClick:Z

    .line 74
    if-eqz p1, :cond_3

    .line 76
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mViewFrameInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    .line 77
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->enableButtonTouch()V

    .line 79
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayTouchFunction:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    .line 80
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)V

    .line 119
    .local v1, "deleteTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)V

    .line 146
    .local v0, "buttonTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->setDisableTrayiconLongClick(Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 163
    .end local v0    # "buttonTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .end local v1    # "deleteTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    if-eqz v2, :cond_2

    .line 165
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayTouchFunction:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->setShowHideButtonInterface(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 169
    :cond_2
    return-void

    .line 150
    :cond_3
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mViewFrameInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    .line 156
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    if-eqz v2, :cond_1

    .line 158
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->isShow()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 159
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->hideTray()V

    .line 160
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->disableButtonTouch()V

    goto :goto_0
.end method

.method public init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;Z)V
    .locals 4
    .param p1, "visibility"    # Z
    .param p2, "viewFrameInterface"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
    .param p3, "trayTouchFunction"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;
    .param p4, "longClick"    # Z

    .prologue
    .line 175
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mVisible:Z

    .line 176
    iput-boolean p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mIsLongClick:Z

    .line 177
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$3;

    invoke-direct {v3, p0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->post(Ljava/lang/Runnable;)Z

    .line 184
    if-eqz p1, :cond_1

    .line 186
    check-cast p2, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    .end local p2    # "viewFrameInterface":Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mViewFrameInterface:Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;

    .line 187
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->enableButtonTouch()V

    .line 188
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->setVisibility(I)V

    .line 189
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayTouchFunction:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    .line 190
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$4;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)V

    .line 232
    .local v1, "deleteTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$5;

    invoke-direct {v0, p0, p4}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Z)V

    .line 260
    .local v0, "buttonTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    if-eqz p4, :cond_0

    .line 262
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->setTrayiconLongClick(Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 268
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayTouchFunction:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->setShowHideButtonInterface(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 275
    .end local v0    # "buttonTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .end local v1    # "deleteTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    :goto_1
    return-void

    .line 266
    .restart local v0    # "buttonTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .restart local v1    # "deleteTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->setDisableTrayiconLongClick(Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_0

    .line 272
    .end local v0    # "buttonTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .end local v1    # "deleteTouch":Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .restart local p2    # "viewFrameInterface":Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->hideTray()V

    .line 273
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->disableButtonTouch()V

    goto :goto_1
.end method

.method public initCurrentButton()V
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getId()I

    move-result v0

    if-eqz v0, :cond_0

    .line 573
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->disable()V

    .line 576
    :cond_0
    return-void
.end method

.method public isDecodingAllFinish()Z
    .locals 1

    .prologue
    .line 643
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mDecodingAllFinish:Z

    return v0
.end method

.method public isFocused()Z
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 613
    const/4 v11, 0x0

    .line 614
    .local v11, "ret":Z
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    if-eqz v4, :cond_0

    .line 616
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->isTrayButtonFocused()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 618
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->dispatchTouchEvent()V

    .line 619
    const/4 v11, 0x1

    .line 639
    :cond_0
    :goto_0
    return v11

    .line 623
    :cond_1
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v10, v4, :cond_0

    .line 625
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 626
    .local v8, "button":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 628
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 629
    .local v0, "upTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 631
    .local v2, "eventTime":J
    const/4 v4, 0x1

    const/4 v7, 0x0

    move v6, v5

    .line 630
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v9

    .line 632
    .local v9, "ev":Landroid/view/MotionEvent;
    invoke-virtual {v8, v9}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 633
    const/4 v11, 0x1

    .line 634
    goto :goto_0

    .line 623
    .end local v0    # "upTime":J
    .end local v2    # "eventTime":J
    .end local v9    # "ev":Landroid/view/MotionEvent;
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public isMaximum()Z
    .locals 3

    .prologue
    .line 647
    const/4 v0, 0x0

    .line 648
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 649
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    .line 650
    :cond_0
    :goto_0
    return v0

    .line 649
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShow()Z
    .locals 2

    .prologue
    .line 654
    const/4 v0, 0x0

    .line 655
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    if-eqz v1, :cond_0

    .line 656
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->isShow()Z

    move-result v0

    .line 657
    :cond_0
    return v0
.end method

.method public newImage(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V
    .locals 2
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;
    .param p2, "decodeFrom"    # I

    .prologue
    .line 545
    packed-switch p2, :pswitch_data_0

    .line 565
    :goto_0
    :pswitch_0
    return-void

    .line 552
    :pswitch_1
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->addButton(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V

    .line 553
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mFromOtherApp:Z

    goto :goto_0

    .line 556
    :pswitch_2
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 557
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->addButton(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V

    .line 558
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mFromOtherApp:Z

    goto :goto_0

    .line 561
    :pswitch_3
    const-string v0, "newImage"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 562
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->reloadButton(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V

    goto :goto_0

    .line 545
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onConfigurationChanged()V
    .locals 3

    .prologue
    .line 299
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->isShow()Z

    move-result v0

    .line 300
    .local v0, "isShow":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->removeAllViews()V

    .line 301
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    .line 302
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    const v2, 0x7f0900b3

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    .line 303
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->onConfigurationChanged(Ljava/util/ArrayList;)V

    .line 305
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mVisible:Z

    if-eqz v1, :cond_1

    .line 307
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayTouchFunction:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->setShowHideButtonInterface(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V

    .line 308
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->post(Ljava/lang/Runnable;)Z

    .line 315
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->setVisibility(I)V

    .line 316
    if-eqz v0, :cond_0

    .line 317
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->showTray()V

    .line 318
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initBodyListener()V

    .line 320
    :cond_1
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->resume()V

    .line 279
    return-void
.end method

.method public saveCurrentImage()V
    .locals 5

    .prologue
    .line 398
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    .line 399
    .local v1, "imageData":Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 400
    .local v2, "path":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 402
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 404
    :cond_0
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 405
    .local v0, "fileName":Ljava/lang/String;
    invoke-direct {p0, v1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->saveImageData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Ljava/lang/String;)V

    .line 406
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$7;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 411
    return-void
.end method

.method public saveCurrentImage(Ljava/lang/String;)V
    .locals 3
    .param p1, "newFileName"    # Ljava/lang/String;

    .prologue
    .line 414
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    .line 415
    .local v0, "imageData":Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->saveImageData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Ljava/lang/String;)V

    .line 417
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$8;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 422
    return-void
.end method

.method public saveCurrentImage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "newFileName"    # Ljava/lang/String;

    .prologue
    .line 425
    const v0, 0x7a1200

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->saveCurrentImage(Ljava/lang/String;Ljava/lang/String;I)V

    .line 426
    return-void
.end method

.method public saveCurrentImage(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "newFileName"    # Ljava/lang/String;
    .param p3, "quality"    # I

    .prologue
    .line 429
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    if-eqz v1, :cond_0

    .line 431
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    .line 432
    .local v0, "imageData":Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    if-eqz v0, :cond_0

    .line 434
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->saveImageData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Ljava/lang/String;Ljava/lang/String;I)V

    .line 436
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 437
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$9;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 445
    .end local v0    # "imageData":Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    :cond_0
    return-void
.end method

.method public setDecodingAllFinish()V
    .locals 1

    .prologue
    .line 661
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mDecodingAllFinish:Z

    .line 662
    return-void
.end method

.method public setEdited()V
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    if-eqz v0, :cond_0

    .line 678
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->invisibleSaveNoti()V

    .line 680
    :cond_0
    return-void
.end method

.method public setEnableTrayButtons()V
    .locals 3

    .prologue
    .line 683
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 685
    const/4 v1, 0x0

    .line 690
    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 696
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 692
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 693
    .local v0, "button":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->enable()V

    .line 690
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setVisibleDeleteButton(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    .line 665
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 667
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 673
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 669
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    .line 670
    .local v0, "button":Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->setVisibleDeleteButton(I)V

    .line 667
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public show()V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayLayout:Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Gui/TrayLayout;->showTray()V

    .line 324
    return-void
.end method
