.class Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$2;
.super Ljava/lang/Object;
.source "TrayManager.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->init(ZLcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 143
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 125
    const-string v0, "tray button touch"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton(Landroid/view/View;)V
    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Landroid/view/View;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getIndexFromTrayButtonList()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->changeImages(I)V

    .line 131
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$2;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->changeImages(I)V

    goto :goto_0
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 137
    return-void
.end method
