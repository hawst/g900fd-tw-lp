.class Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;
.super Ljava/lang/Thread;
.source "AdjustmentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContourThread"
.end annotation


# instance fields
.field private isRun:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V
    .locals 1

    .prologue
    .line 2603
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2605
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;->isRun:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;)V
    .locals 0

    .prologue
    .line 2603
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 2608
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;->isRun:Z

    .line 2609
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;)V

    .line 2610
    return-void
.end method

.method public run()V
    .locals 4

    .prologue
    .line 2614
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;->isRun:Z

    if-nez v1, :cond_2

    .line 2636
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;)V

    .line 2637
    return-void

    .line 2617
    :cond_2
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2622
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mColor:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)I

    move-result v2

    const v3, 0xffffff

    xor-int/2addr v2, v3

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;I)V

    .line 2623
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->invalidateViewsWithThread()V

    .line 2624
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2626
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 2618
    :catch_0
    move-exception v0

    .line 2620
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
