.class Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$2;
.super Ljava/lang/Object;
.source "EffectManagerActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->initActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    .line 420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 426
    const/16 v0, 0x42

    if-eq p2, v0, :cond_0

    const/16 v0, 0x17

    if-ne p2, v0, :cond_1

    .line 428
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 442
    :cond_1
    :goto_0
    return v2

    .line 430
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectCancel:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->playSoundEffect(I)V

    .line 431
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectCancel:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setPressed(Z)V

    goto :goto_0

    .line 434
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->mEffectCancel:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$5(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setPressed(Z)V

    .line 436
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity$2;->this$0:Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;

    # invokes: Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->cancel()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;->access$6(Lcom/sec/android/mimage/photoretouching/effectmanager/EffectManagerActivity;)V

    goto :goto_0

    .line 428
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
