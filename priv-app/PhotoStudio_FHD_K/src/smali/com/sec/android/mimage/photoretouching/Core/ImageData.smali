.class public Lcom/sec/android/mimage/photoretouching/Core/ImageData;
.super Ljava/lang/Object;
.source "ImageData.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/ImageData$OnCallback;,
        Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;,
        Lcom/sec/android/mimage/photoretouching/Core/ImageData$setViewSizeMatrixCallback;
    }
.end annotation


# static fields
.field private static FIRST_STAMP:I = 0x0

.field private static FOURTH_STAMP:I = 0x0

.field private static MAX_DECORATION_RATIO:F = 0.0f

.field private static final MAX_THUMB_SIZE:I = 0x80000

.field private static MIN_DECORATION_RATIO:F

.field private static SECOND_STAMP:I

.field private static THIRD_STAMP:I


# instance fields
.field private mCallback:Lcom/sec/android/mimage/photoretouching/Core/ImageData$OnCallback;

.field private mIsReadyForPreview:Z

.field private mOrientation:I

.field private mSetPreviewBuffer:Z

.field protected mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

.field private mTextureData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[I>;"
        }
    .end annotation
.end field

.field private mTextureIds:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2236
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->FIRST_STAMP:I

    .line 2237
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->SECOND_STAMP:I

    .line 2238
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->THIRD_STAMP:I

    .line 2239
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->FOURTH_STAMP:I

    .line 2241
    const/high16 v0, 0x40600000    # 3.5f

    sput v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->MAX_DECORATION_RATIO:F

    .line 2242
    const/high16 v0, 0x3e800000    # 0.25f

    sput v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->MIN_DECORATION_RATIO:F

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 997
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 1004
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mTextureIds:[I

    .line 2232
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    .line 2233
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mCallback:Lcom/sec/android/mimage/photoretouching/Core/ImageData$OnCallback;

    .line 2234
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSetPreviewBuffer:Z

    .line 2235
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mIsReadyForPreview:Z

    .line 64
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    .line 65
    return-void

    .line 997
    :array_0
    .array-data 4
        0x16001607
        0x16001612
        0x1600160f
        0x16001614
        0x16001608
        0x16001609
        0x16001616
        0x16001617
    .end array-data
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 2240
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mTextureData:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 2240
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mTextureData:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)[I
    .locals 1

    .prologue
    .line 997
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mTextureIds:[I

    return-object v0
.end method

.method private calcPreviewRoi(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Z
    .locals 24
    .param p1, "property"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    .prologue
    .line 1929
    const/4 v9, 0x0

    .line 1930
    .local v9, "ret":Z
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMaskRoi:Landroid/graphics/Rect;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$59(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v19

    if-eqz v19, :cond_0

    .line 1932
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMaskRoi:Landroid/graphics/Rect;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$59(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->setEmpty()V

    .line 1934
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v8

    .line 1935
    .local v8, "originalMaskRoi":Landroid/graphics/Rect;
    if-eqz v8, :cond_0

    .line 1937
    invoke-virtual {v8}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 1938
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMaskRoi:Landroid/graphics/Rect;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$59(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1939
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMaskRoi:Landroid/graphics/Rect;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$59(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1940
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMaskRoi:Landroid/graphics/Rect;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$59(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v19

    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1941
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMaskRoi:Landroid/graphics/Rect;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$59(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v19

    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1996
    .end local v8    # "originalMaskRoi":Landroid/graphics/Rect;
    :cond_0
    :goto_0
    return v9

    .line 1944
    .restart local v8    # "originalMaskRoi":Landroid/graphics/Rect;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewMatrix()Landroid/graphics/Matrix;

    move-result-object v7

    .line 1945
    .local v7, "matrix":Landroid/graphics/Matrix;
    const/16 v19, 0x9

    move/from16 v0, v19

    new-array v0, v0, [F

    move-object/from16 v18, v0

    .line 1946
    .local v18, "v":[F
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1950
    const/16 v19, 0x0

    aget v12, v18, v19

    .line 1951
    .local v12, "scale":F
    const/high16 v19, 0x3f800000    # 1.0f

    const/16 v20, 0x0

    aget v20, v18, v20

    div-float v4, v19, v20

    .line 1953
    .local v4, "inverseScale":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v14

    .line 1954
    .local v14, "scaleWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v13

    .line 1956
    .local v13, "scaleHeight":I
    const/16 v19, 0x2

    aget v6, v18, v19

    .line 1957
    .local v6, "leftFromOrg":F
    const/16 v19, 0x0

    cmpl-float v19, v6, v19

    if-lez v19, :cond_2

    .line 1958
    const/4 v6, 0x0

    .line 1960
    :cond_2
    const/16 v19, 0x5

    aget v17, v18, v19

    .line 1961
    .local v17, "topFromOrg":F
    const/16 v19, 0x0

    cmpl-float v19, v17, v19

    if-lez v19, :cond_3

    .line 1962
    const/16 v17, 0x0

    .line 1964
    :cond_3
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 1965
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v17

    .line 1966
    int-to-float v0, v14

    move/from16 v19, v0

    add-float v11, v6, v19

    .line 1967
    .local v11, "rightFromOrg":F
    int-to-float v0, v13

    move/from16 v19, v0

    add-float v3, v17, v19

    .line 1969
    .local v3, "bottomFromOrg":F
    mul-float/2addr v6, v4

    .line 1970
    mul-float v17, v17, v4

    .line 1971
    mul-float/2addr v11, v4

    .line 1972
    mul-float/2addr v3, v4

    .line 1975
    iget v0, v8, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-static {v0, v6}, Ljava/lang/Math;->max(FF)F

    move-result v15

    .line 1976
    .local v15, "temp":F
    sub-float v19, v15, v6

    mul-float v19, v19, v12

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v5, v0

    .line 1978
    .local v5, "left":F
    iget v0, v8, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v15

    .line 1979
    sub-float v19, v15, v17

    mul-float v19, v19, v12

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v16, v0

    .line 1981
    .local v16, "top":F
    iget v0, v8, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-static {v0, v11}, Ljava/lang/Math;->min(FF)F

    move-result v15

    .line 1982
    sub-float v19, v15, v6

    mul-float v19, v19, v12

    const/high16 v20, 0x3f800000    # 1.0f

    add-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v10, v0

    .line 1983
    .local v10, "right":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    cmpl-float v19, v10, v19

    if-lez v19, :cond_4

    .line 1984
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, v19

    int-to-float v10, v0

    .line 1986
    :cond_4
    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v15

    .line 1987
    sub-float v19, v15, v17

    mul-float v19, v19, v12

    const v20, 0x3f666666    # 0.9f

    add-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v2, v0

    .line 1988
    .local v2, "bottom":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    cmpl-float v19, v2, v19

    if-lez v19, :cond_5

    .line 1989
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, v19

    int-to-float v2, v0

    .line 1991
    :cond_5
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMaskRoi:Landroid/graphics/Rect;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$59(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v19

    float-to-int v0, v5

    move/from16 v20, v0

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v21, v0

    float-to-int v0, v10

    move/from16 v22, v0

    float-to-int v0, v2

    move/from16 v23, v0

    invoke-virtual/range {v19 .. v23}, Landroid/graphics/Rect;->set(IIII)V

    .line 1992
    const/4 v9, 0x1

    goto/16 :goto_0
.end method

.method private initDrawCanvasRoi(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;FFFF)V
    .locals 7
    .param p1, "p"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;
    .param p2, "scaleW"    # F
    .param p3, "scaleH"    # F
    .param p4, "left"    # F
    .param p5, "top"    # F

    .prologue
    const/4 v6, 0x0

    .line 1829
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawCanvasRoi:Landroid/graphics/Rect;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$71(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 1832
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float v4, p2, v5

    .line 1833
    .local v4, "scaleWidth":F
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float v3, p3, v5

    .line 1836
    .local v3, "scaleHeight":F
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawCanvasRoi:Landroid/graphics/Rect;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$71(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v2

    .line 1837
    .local v2, "roi":Landroid/graphics/Rect;
    if-eqz v2, :cond_4

    .line 1839
    add-float v1, p4, v4

    .line 1840
    .local v1, "right":F
    add-float v0, p5, v3

    .line 1841
    .local v0, "bottom":F
    cmpg-float v5, p4, v6

    if-gez v5, :cond_0

    .line 1842
    const/4 p4, 0x0

    .line 1843
    :cond_0
    cmpg-float v5, p5, v6

    if-gez v5, :cond_1

    .line 1844
    const/4 p5, 0x0

    .line 1845
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v5

    int-to-float v5, v5

    cmpl-float v5, v1, v5

    if-lez v5, :cond_2

    .line 1847
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v5, p4

    sub-float v1, v5, p4

    .line 1849
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v5

    int-to-float v5, v5

    cmpl-float v5, v0, v5

    if-lez v5, :cond_3

    .line 1851
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v5, p5

    sub-float v0, v5, p5

    .line 1853
    :cond_3
    float-to-int v5, p4

    iput v5, v2, Landroid/graphics/Rect;->left:I

    .line 1854
    float-to-int v5, p5

    iput v5, v2, Landroid/graphics/Rect;->top:I

    .line 1855
    float-to-int v5, v1

    iput v5, v2, Landroid/graphics/Rect;->right:I

    .line 1856
    float-to-int v5, v0

    iput v5, v2, Landroid/graphics/Rect;->bottom:I

    .line 1860
    .end local v0    # "bottom":F
    .end local v1    # "right":F
    .end local v2    # "roi":Landroid/graphics/Rect;
    .end local v3    # "scaleHeight":F
    .end local v4    # "scaleWidth":F
    :cond_4
    return-void
.end method

.method private makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V
    .locals 8
    .param p1, "src"    # Landroid/graphics/Bitmap;
    .param p2, "outBuffer"    # [I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 1137
    const/4 v1, 0x1

    invoke-static {p1, p3, p4, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, "resized":Landroid/graphics/Bitmap;
    move-object v1, p2

    move v3, p3

    move v4, v2

    move v5, v2

    move v6, p3

    move v7, p4

    .line 1138
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1139
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1140
    const/4 v0, 0x0

    .line 1141
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1143
    return-void
.end method

.method private nullBuffer(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)V
    .locals 1
    .param p1, "p"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    .prologue
    const/4 v0, 0x0

    .line 627
    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 628
    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$8(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 629
    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$9(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 630
    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$14(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 631
    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 632
    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$41(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/net/Uri;)V

    .line 633
    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$42(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Matrix;)V

    .line 634
    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 635
    return-void
.end method

.method private recycleBitmap(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)V
    .locals 2
    .param p1, "p"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    .prologue
    .line 620
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBuffer:[I
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$38(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v0

    if-eqz v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 624
    :cond_0
    return-void
.end method


# virtual methods
.method public applyPreview()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2057
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-nez v0, :cond_0

    .line 2062
    :goto_0
    return-void

    .line 2059
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$16(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v2

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2060
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$50(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 2061
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$57(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    goto :goto_0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->removeAllView()V

    .line 69
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->destroy()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    .line 71
    return-void
.end method

.method public determinePersonalPage(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 654
    const/4 v10, 0x0

    .line 656
    .local v10, "path":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 727
    .end local p2    # "object":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 658
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_1
    if-eqz p2, :cond_0

    .line 661
    instance-of v0, p2, Landroid/net/Uri;

    if-eqz v0, :cond_8

    .line 663
    const/4 v6, 0x0

    .line 664
    .local v6, "c":Landroid/database/Cursor;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 665
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p2

    check-cast v1, Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 666
    :cond_2
    if-nez v6, :cond_7

    .line 668
    check-cast p2, Landroid/net/Uri;

    .end local p2    # "object":Ljava/lang/Object;
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    .line 689
    :cond_3
    :goto_1
    if-eqz v6, :cond_4

    .line 690
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 699
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_4
    :goto_2
    if-eqz v10, :cond_0

    .line 701
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 703
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPersonalPageRoot(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    .line 704
    .local v11, "personalPageRoot":Ljava/lang/String;
    if-eqz v11, :cond_0

    .line 706
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$44(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    if-eqz v1, :cond_0

    .line 708
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    .line 711
    .local v9, "lastFileSeparatorIdx":I
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v0

    if-le v9, v0, :cond_5

    .line 712
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v9

    .line 713
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v1, 0x0

    invoke-virtual {v10, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$45(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    .line 715
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPersonalPagePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$46(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v9, v0, 0x1

    .line 716
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPersonalPagePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$46(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v9, v0, :cond_6

    .line 717
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPersonalPagePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$46(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    .line 718
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPersonalPagePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$46(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPersonalPagePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$46(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$47(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 672
    .end local v9    # "lastFileSeparatorIdx":I
    .end local v11    # "personalPageRoot":Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    .restart local p2    # "object":Ljava/lang/Object;
    :cond_7
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 674
    :try_start_0
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 675
    .local v8, "idx":I
    invoke-interface {v6}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 677
    const/4 v0, -0x1

    if-eq v8, v0, :cond_3

    .line 678
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    goto/16 :goto_1

    .line 680
    .end local v8    # "idx":I
    :catch_0
    move-exception v7

    .line 682
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ImageData - determinePersonalPage() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 693
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    :cond_8
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_4

    move-object v10, p2

    .line 695
    check-cast v10, Ljava/lang/String;

    goto/16 :goto_2
.end method

.method public getCopyToDrawCanvasRoi()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1668
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1669
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mCopyToDrawCanvasRoi:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$70(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1671
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentScale()F
    .locals 3

    .prologue
    .line 1405
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 1406
    .local v0, "m":Landroid/graphics/Matrix;
    const/16 v2, 0x9

    new-array v1, v2, [F

    .line 1407
    .local v1, "values":[F
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1408
    const/4 v2, 0x0

    aget v2, v1, v2

    return v2
.end method

.method public getDrawCanvasRoi()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1695
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1697
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawCanvasRoi:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$71(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1700
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 1675
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v4, :cond_4

    .line 1677
    new-instance v2, Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v4

    int-to-float v4, v4

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v6

    int-to-float v6, v6

    invoke-direct {v2, v5, v5, v4, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1678
    .local v2, "src":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v0

    .line 1679
    .local v0, "m":Landroid/graphics/Matrix;
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1680
    new-instance v3, Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewWidth:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$24(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v4

    int-to-float v4, v4

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewHeight:I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$25(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v6

    int-to-float v6, v6

    invoke-direct {v3, v5, v5, v4, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1681
    .local v3, "viewRoi":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 1682
    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1683
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1684
    .local v1, "ret":Landroid/graphics/Rect;
    iget v4, v2, Landroid/graphics/RectF;->left:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    move v4, v5

    :goto_0
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 1685
    iget v4, v2, Landroid/graphics/RectF;->top:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    :goto_1
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 1686
    iget v4, v2, Landroid/graphics/RectF;->right:F

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v7

    cmpl-float v4, v4, v7

    if-lez v4, :cond_2

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v4

    :goto_2
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 1687
    iget v4, v2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v8

    cmpl-float v4, v4, v8

    if-lez v4, :cond_3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v4

    :goto_3
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 1684
    invoke-virtual {v1, v6, v5, v7, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1691
    .end local v0    # "m":Landroid/graphics/Matrix;
    .end local v1    # "ret":Landroid/graphics/Rect;
    .end local v2    # "src":Landroid/graphics/RectF;
    .end local v3    # "viewRoi":Landroid/graphics/RectF;
    :goto_4
    return-object v1

    .line 1684
    .restart local v0    # "m":Landroid/graphics/Matrix;
    .restart local v1    # "ret":Landroid/graphics/Rect;
    .restart local v2    # "src":Landroid/graphics/RectF;
    .restart local v3    # "viewRoi":Landroid/graphics/RectF;
    :cond_0
    iget v4, v2, Landroid/graphics/RectF;->left:F

    goto :goto_0

    .line 1685
    :cond_1
    iget v5, v2, Landroid/graphics/RectF;->top:F

    goto :goto_1

    .line 1686
    :cond_2
    iget v4, v2, Landroid/graphics/RectF;->right:F

    goto :goto_2

    .line 1687
    :cond_3
    iget v4, v2, Landroid/graphics/RectF;->bottom:F

    goto :goto_3

    .line 1691
    .end local v0    # "m":Landroid/graphics/Matrix;
    .end local v1    # "ret":Landroid/graphics/Rect;
    .end local v2    # "src":Landroid/graphics/RectF;
    .end local v3    # "viewRoi":Landroid/graphics/RectF;
    :cond_4
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public getDrawPathList()Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 1401
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$43(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Path;

    move-result-object v0

    return-object v0
.end method

.method public getInitialPreviewInputBuffer()[I
    .locals 1

    .prologue
    .line 1601
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1602
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mInitialPreviewInputBuff:[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$21(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v0

    .line 1604
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMagnifyMaxRatio()F
    .locals 1

    .prologue
    .line 1637
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1638
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mMaxRatio:F
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$66(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F

    move-result v0

    .line 1640
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getMagnifyMinRatio()F
    .locals 1

    .prologue
    .line 1645
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1646
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mMinRatio:F
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$67(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F

    move-result v0

    .line 1648
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getOrgPreviewHeight()I
    .locals 1

    .prologue
    .line 1754
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1755
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPreviewHeight:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$74(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    .line 1757
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOrgPreviewWidth()I
    .locals 1

    .prologue
    .line 1746
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1747
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPreviewWidth:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$73(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    .line 1749
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOrgSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;
    .locals 3

    .prologue
    .line 1467
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v1, :cond_1

    .line 1469
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1470
    .local v0, "matrix":Landroid/graphics/Matrix;
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mOrientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1472
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitSupportMatrix:Landroid/graphics/Matrix;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$34(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1473
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$35(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1474
    const-string v1, "JW portraitMatrix set"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1484
    .end local v0    # "matrix":Landroid/graphics/Matrix;
    :goto_0
    return-object v0

    .line 1478
    .restart local v0    # "matrix":Landroid/graphics/Matrix;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandSupportMatrix:Landroid/graphics/Matrix;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$36(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1479
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$37(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    goto :goto_0

    .line 1484
    .end local v0    # "matrix":Landroid/graphics/Matrix;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOriginalHeight()I
    .locals 1

    .prologue
    .line 1629
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1630
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    .line 1632
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOriginalInputBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1799
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1800
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1802
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOriginalInputData()[I
    .locals 1

    .prologue
    .line 1810
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1812
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBuffer:[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$38(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v0

    .line 1815
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOriginalMaskBuffer()[B
    .locals 1

    .prologue
    .line 1412
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrigianlMaskBuff:[B
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$55(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[B

    move-result-object v0

    return-object v0
.end method

.method public getOriginalMaskRoi()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1711
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1712
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalMaskRoi:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$54(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1714
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOriginalPaddingX()F
    .locals 1

    .prologue
    .line 1653
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1654
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalPaddingX:F
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$68(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F

    move-result v0

    .line 1656
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOriginalPaddingY()F
    .locals 1

    .prologue
    .line 1661
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1662
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalPaddingY:F
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$69(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F

    move-result v0

    .line 1664
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOriginalToPreviewMatrix()Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 1416
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v1, :cond_0

    .line 1418
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1419
    .local v0, "matrix":Landroid/graphics/Matrix;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$18(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1420
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1424
    .end local v0    # "matrix":Landroid/graphics/Matrix;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOriginalToPreviewMatrixBasedOnViewTransform()Landroid/graphics/Matrix;
    .locals 3

    .prologue
    .line 1436
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v2, :cond_3

    .line 1438
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1439
    .local v0, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 1440
    .local v1, "supMatrix":Landroid/graphics/Matrix;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$18(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1441
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$18(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1442
    :cond_0
    if-eqz v1, :cond_1

    .line 1443
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1444
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$32(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1445
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$32(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1449
    .end local v0    # "matrix":Landroid/graphics/Matrix;
    .end local v1    # "supMatrix":Landroid/graphics/Matrix;
    :cond_2
    :goto_0
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOriginalToPreviewRatio()F
    .locals 1

    .prologue
    .line 1819
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewRatio:F
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$72(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F

    move-result v0

    return v0
.end method

.method public getOriginalWidth()I
    .locals 1

    .prologue
    .line 1622
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1623
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    .line 1625
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1587
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1588
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mFilePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$64(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Ljava/lang/String;

    move-result-object v0

    .line 1590
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPathBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1806
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPathBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$20(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getPersonalPagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 741
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-nez v0, :cond_0

    .line 742
    const/4 v0, 0x0

    .line 743
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPersonalPagePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$46(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPreviewBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1392
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$12(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getPreviewHeight()I
    .locals 1

    .prologue
    .line 1738
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1739
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    .line 1741
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPreviewInputBuffer()[I
    .locals 1

    .prologue
    .line 1594
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1595
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v0

    .line 1597
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPreviewMaskBuffer()[B
    .locals 1

    .prologue
    .line 1608
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1609
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMask:[B
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$65(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[B

    move-result-object v0

    .line 1611
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPreviewMaskRoi()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 1704
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1705
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMaskRoi:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$59(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1707
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPreviewOutputBuffer()[I
    .locals 1

    .prologue
    .line 1615
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1616
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$16(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v0

    .line 1618
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPreviewPaddingX()I
    .locals 1

    .prologue
    .line 1778
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1779
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawCanvasRoi:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$71(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 1781
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPreviewPaddingY()I
    .locals 1

    .prologue
    .line 1786
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1787
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawCanvasRoi:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$71(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 1789
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPreviewWidth()I
    .locals 1

    .prologue
    .line 1730
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1731
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    .line 1733
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPrivateSaveFolder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 747
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-nez v0, :cond_0

    .line 748
    const/4 v0, 0x0

    .line 751
    :goto_0
    return-object v0

    .line 749
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPrivateSaveFolder:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$49(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Private"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 750
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const-string v1, "Private"

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$47(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    .line 751
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPrivateSaveFolder:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$49(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Studio"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRealViewPaddingY(I)I
    .locals 2
    .param p1, "heightPixel"    # I

    .prologue
    .line 1794
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int v1, p1, v1

    div-int/lit8 v0, v1, 0x2

    .line 1795
    .local v0, "paddingY":I
    return v0
.end method

.method public getSupMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 1428
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1429
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mSupportMatrix:Landroid/graphics/Matrix;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$26(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 1431
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 1453
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v1, :cond_0

    .line 1455
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1456
    .local v0, "matrix":Landroid/graphics/Matrix;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mSupportMatrix:Landroid/graphics/Matrix;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$26(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1457
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$32(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1462
    .end local v0    # "matrix":Landroid/graphics/Matrix;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTexture(Landroid/content/Context;III)[I
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "effectType"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const v5, 0x7f02054d

    const v4, 0x7f0201c8

    const/16 v7, 0x12c

    .line 1070
    if-nez p1, :cond_0

    .line 1071
    const/4 v3, 0x0

    .line 1132
    :goto_0
    return-object v3

    .line 1072
    :cond_0
    mul-int v6, p3, p4

    new-array v3, v6, [I

    .line 1073
    .local v3, "texturePreview":[I
    const/4 v1, 0x0

    .line 1074
    .local v1, "texture":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    .line 1076
    .local v2, "textureID":I
    packed-switch p2, :pswitch_data_0

    .line 1118
    :goto_1
    :pswitch_0
    if-eqz p1, :cond_1

    .line 1119
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1127
    :cond_1
    :goto_2
    if-eqz v1, :cond_2

    .line 1128
    invoke-direct {p0, v1, v3, p3, p4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 1129
    :cond_2
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1130
    const/4 v1, 0x0

    .line 1131
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_0

    .line 1080
    :pswitch_1
    if-lt p3, v7, :cond_3

    if-ge p4, v7, :cond_4

    :cond_3
    move v2, v5

    .line 1081
    :goto_3
    goto :goto_1

    :cond_4
    move v2, v4

    .line 1080
    goto :goto_3

    .line 1084
    :pswitch_2
    if-lt p3, v7, :cond_5

    if-ge p4, v7, :cond_6

    :cond_5
    move v2, v5

    .line 1085
    :goto_4
    const v2, 0x7f020551

    .line 1086
    goto :goto_1

    :cond_6
    move v2, v4

    .line 1084
    goto :goto_4

    .line 1089
    :pswitch_3
    if-lt p3, v7, :cond_7

    if-ge p4, v7, :cond_8

    :cond_7
    const v2, 0x7f020550

    .line 1090
    :goto_5
    goto :goto_1

    .line 1089
    :cond_8
    const v2, 0x7f020555

    goto :goto_5

    .line 1093
    :pswitch_4
    if-lt p3, v7, :cond_9

    if-ge p4, v7, :cond_a

    :cond_9
    const v2, 0x7f020552

    .line 1094
    :goto_6
    goto :goto_1

    .line 1093
    :cond_a
    const v2, 0x7f0200d5

    goto :goto_6

    .line 1097
    :pswitch_5
    if-lt p3, v7, :cond_b

    if-ge p4, v7, :cond_c

    :cond_b
    const v2, 0x7f02054e

    .line 1098
    :goto_7
    goto :goto_1

    .line 1097
    :cond_c
    const v2, 0x7f0201c7

    goto :goto_7

    .line 1101
    :pswitch_6
    if-lt p3, v7, :cond_d

    if-ge p4, v7, :cond_e

    :cond_d
    const v2, 0x7f02054f

    .line 1102
    :goto_8
    goto :goto_1

    .line 1101
    :cond_e
    const v2, 0x7f0201c9

    goto :goto_8

    .line 1105
    :pswitch_7
    if-lt p3, v7, :cond_f

    if-ge p4, v7, :cond_10

    :cond_f
    const v2, 0x7f020553

    .line 1106
    :goto_9
    goto :goto_1

    .line 1105
    :cond_10
    const v2, 0x7f0201ca

    goto :goto_9

    .line 1109
    :pswitch_8
    if-lt p3, v7, :cond_11

    if-ge p4, v7, :cond_12

    :cond_11
    const v2, 0x7f020554

    :goto_a
    goto :goto_1

    :cond_12
    const v2, 0x7f0201cb

    goto :goto_a

    .line 1121
    :catch_0
    move-exception v0

    .line 1123
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_2

    .line 1076
    :pswitch_data_0
    .packed-switch 0x16001607
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public getTextureData()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[I>;"
        }
    .end annotation

    .prologue
    .line 2246
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mTextureData:Ljava/util/HashMap;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1580
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1581
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mUri:Landroid/net/Uri;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$56(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/net/Uri;

    move-result-object v0

    .line 1583
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getViewHeight()I
    .locals 1

    .prologue
    .line 1770
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1771
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewHeight:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$25(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    .line 1773
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getViewTransformMatrix()Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 1493
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1494
    .local v0, "matrix":Landroid/graphics/Matrix;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$32(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1495
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$32(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1496
    :cond_0
    return-object v0
.end method

.method public getViewWidth()I
    .locals 1

    .prologue
    .line 1762
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1763
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewWidth:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$24(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    .line 1765
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getoriginalToPreviewMaskRoi()Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 1718
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v1, :cond_0

    .line 1719
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalMaskRoi:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$54(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1720
    .local v0, "rect":Landroid/graphics/Rect;
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewRatio:F
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$72(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewRatio:F
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$72(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1721
    iget v3, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewRatio:F
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$72(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F

    move-result v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewRatio:F
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$72(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)F

    move-result v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    .line 1720
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1724
    .end local v0    # "rect":Landroid/graphics/Rect;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initDrawCanvasRoi(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;II)V
    .locals 5
    .param p1, "p"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;
    .param p2, "scaleWidth"    # I
    .param p3, "scaleHeight"    # I

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 1863
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawCanvasRoi:Landroid/graphics/Rect;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$71(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1865
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawCanvasRoi:Landroid/graphics/Rect;
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$71(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v2

    .line 1866
    .local v2, "roi":Landroid/graphics/Rect;
    if-eqz v2, :cond_0

    .line 1869
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewWidth:I
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$24(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    if-le v3, p2, :cond_1

    .line 1871
    move v1, p2

    .line 1878
    .local v1, "previewWidth":I
    :goto_0
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewHeight:I
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$25(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    if-le v3, p3, :cond_2

    .line 1880
    move v0, p3

    .line 1887
    .local v0, "previewHeight":I
    :goto_1
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewWidth:I
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$24(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    sub-int/2addr v3, v1

    int-to-float v3, v3

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 1888
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewHeight:I
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$25(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    sub-int/2addr v3, v0

    int-to-float v3, v3

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 1889
    iget v3, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 1890
    iget v3, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 1893
    .end local v0    # "previewHeight":I
    .end local v1    # "previewWidth":I
    .end local v2    # "roi":Landroid/graphics/Rect;
    :cond_0
    return-void

    .line 1875
    .restart local v2    # "roi":Landroid/graphics/Rect;
    :cond_1
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewWidth:I
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$24(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    .restart local v1    # "previewWidth":I
    goto :goto_0

    .line 1884
    :cond_2
    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewHeight:I
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$25(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    .restart local v0    # "previewHeight":I
    goto :goto_1
.end method

.method public isAtLeastSaved()Z
    .locals 2

    .prologue
    .line 1572
    const/4 v0, 0x0

    .line 1573
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v1, :cond_0

    .line 1574
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->atLeastSaved:Z
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$63(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Z

    move-result v0

    .line 1575
    :cond_0
    return v0
.end method

.method public isEdited()Z
    .locals 2

    .prologue
    .line 1558
    const/4 v0, 0x0

    .line 1559
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v1, :cond_0

    .line 1560
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isEdited:Z
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$61(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Z

    move-result v0

    .line 1561
    :cond_0
    return v0
.end method

.method public isEnableDecoration()Z
    .locals 1

    .prologue
    .line 1824
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isEnableDecoration:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$75(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Z

    move-result v0

    return v0
.end method

.method public isPersonalPage()Z
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-nez v0, :cond_0

    .line 731
    const/4 v0, 0x0

    .line 732
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isFromPersonalPage:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$48(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Z

    move-result v0

    goto :goto_0
.end method

.method public isReadyForPreview()Z
    .locals 1

    .prologue
    .line 2071
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mIsReadyForPreview:Z

    return v0
.end method

.method public isSaved()Z
    .locals 2

    .prologue
    .line 1565
    const/4 v0, 0x0

    .line 1566
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v1, :cond_0

    .line 1567
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->isSaved:Z
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$62(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Z

    move-result v0

    .line 1568
    :cond_0
    return v0
.end method

.method public loadTextures(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1007
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/content/Context;)V

    .line 1061
    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$1;->start()V

    .line 1063
    return-void
.end method

.method public makePreviewMask()V
    .locals 23

    .prologue
    .line 2001
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskBuffer()[B

    move-result-object v14

    .line 2002
    .local v14, "previewMask":[B
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-static {v14, v0}, Ljava/util/Arrays;->fill([BB)V

    .line 2003
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v6

    .line 2004
    .local v6, "mask":[B
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewMatrix()Landroid/graphics/Matrix;

    move-result-object v7

    .line 2005
    .local v7, "matrix":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->calcPreviewRoi(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Z

    .line 2006
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v15

    .line 2008
    .local v15, "previewRoi":Landroid/graphics/Rect;
    const/16 v21, 0x9

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v17, v0

    .line 2009
    .local v17, "v":[F
    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 2010
    const/high16 v21, 0x3f800000    # 1.0f

    const/16 v22, 0x0

    aget v22, v17, v22

    div-float v3, v21, v22

    .line 2012
    .local v3, "inverseScale":F
    const/16 v21, 0x2

    aget v5, v17, v21

    .line 2013
    .local v5, "leftFromOrg":F
    const/16 v21, 0x0

    cmpl-float v21, v5, v21

    if-lez v21, :cond_0

    .line 2014
    const/4 v5, 0x0

    .line 2016
    :cond_0
    const/16 v21, 0x5

    aget v16, v17, v21

    .line 2017
    .local v16, "topFromOrg":F
    const/16 v21, 0x0

    cmpl-float v21, v16, v21

    if-lez v21, :cond_1

    .line 2018
    const/16 v16, 0x0

    .line 2020
    :cond_1
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 2021
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v16

    .line 2023
    mul-float v8, v5, v3

    .line 2024
    .local v8, "moveX":F
    mul-float v9, v16, v3

    .line 2026
    .local v9, "moveY":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v11

    .line 2027
    .local v11, "originalWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v10

    .line 2029
    .local v10, "originalHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v18

    .line 2031
    .local v18, "viewWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v12

    .line 2032
    .local v12, "paddingX":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v13

    .line 2034
    .local v13, "paddingY":I
    iget v2, v15, Landroid/graphics/Rect;->top:I

    .local v2, "i":I
    :goto_0
    iget v0, v15, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-le v2, v0, :cond_2

    .line 2054
    return-void

    .line 2036
    :cond_2
    int-to-float v0, v2

    move/from16 v21, v0

    mul-float v21, v21, v3

    add-float v21, v21, v9

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v20, v0

    .line 2037
    .local v20, "ypos":I
    add-int/lit8 v21, v10, -0x1

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->min(II)I

    move-result v20

    .line 2038
    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->max(II)I

    move-result v20

    .line 2039
    mul-int v20, v20, v11

    .line 2040
    iget v4, v15, Landroid/graphics/Rect;->left:I

    .local v4, "j":I
    :goto_1
    iget v0, v15, Landroid/graphics/Rect;->right:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-le v4, v0, :cond_3

    .line 2034
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2042
    :cond_3
    int-to-float v0, v4

    move/from16 v21, v0

    mul-float v21, v21, v3

    add-float v19, v21, v8

    .line 2043
    .local v19, "xpos":F
    add-int/lit8 v21, v11, -0x1

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move/from16 v0, v19

    move/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v19

    .line 2044
    const/16 v21, 0x0

    move/from16 v0, v19

    move/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v19

    .line 2046
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v21, v0

    add-float v21, v21, v19

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    aget-byte v21, v6, v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_4

    .line 2048
    add-int v21, v2, v13

    mul-int v21, v21, v18

    add-int v22, v4, v12

    add-int v21, v21, v22

    const/16 v22, 0x1

    aput-byte v22, v14, v21

    .line 2040
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 2051
    :cond_4
    add-int v21, v2, v13

    mul-int v21, v21, v18

    add-int v22, v4, v12

    add-int v21, v21, v22

    const/16 v22, 0x0

    aput-byte v22, v14, v21

    goto :goto_2
.end method

.method public recycleOriginalBitmap()V
    .locals 2

    .prologue
    .line 1363
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1364
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$17(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1365
    return-void
.end method

.method public removeAllView()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->recycleBitmap(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->nullBuffer(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)V

    .line 79
    :cond_0
    return-void
.end method

.method public resetPreview()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2065
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-nez v0, :cond_0

    .line 2068
    :goto_0
    return-void

    .line 2067
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$16(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v2

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public resetSuPMatrix()V
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const/4 v11, 0x0

    .line 295
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewWidth:I
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$24(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v7

    .line 296
    .local v7, "virtualViewWidth":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewHeight:I
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$25(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v6

    .line 298
    .local v6, "virtualViewHeight":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mSupportMatrix:Landroid/graphics/Matrix;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$26(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Matrix;->reset()V

    .line 299
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mSupportMatrix:Landroid/graphics/Matrix;
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$26(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v3

    .line 300
    .local v3, "matrix":Landroid/graphics/Matrix;
    const/16 v9, 0x9

    new-array v5, v9, [F

    .line 301
    .local v5, "value":[F
    invoke-virtual {v3, v5}, Landroid/graphics/Matrix;->getValues([F)V

    .line 302
    const/4 v4, 0x0

    .line 303
    .local v4, "r":Landroid/graphics/RectF;
    new-instance v4, Landroid/graphics/RectF;

    .end local v4    # "r":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v10

    int-to-float v10, v10

    invoke-direct {v4, v11, v11, v9, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 305
    .restart local v4    # "r":Landroid/graphics/RectF;
    if-eqz v3, :cond_0

    .line 306
    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 308
    :cond_0
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v2

    .line 309
    .local v2, "height":F
    const/4 v1, 0x0

    .line 311
    .local v1, "deltaY":F
    int-to-float v9, v6

    cmpg-float v9, v2, v9

    if-gez v9, :cond_3

    .line 313
    int-to-float v9, v6

    sub-float/2addr v9, v2

    div-float/2addr v9, v12

    iget v10, v4, Landroid/graphics/RectF;->top:F

    sub-float v1, v9, v10

    .line 326
    :cond_1
    :goto_0
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v8

    .line 328
    .local v8, "width":F
    const/4 v0, 0x0

    .line 330
    .local v0, "deltaX":F
    int-to-float v9, v7

    cmpg-float v9, v8, v9

    if-gez v9, :cond_5

    .line 332
    int-to-float v9, v7

    sub-float/2addr v9, v8

    div-float/2addr v9, v12

    iget v10, v4, Landroid/graphics/RectF;->left:F

    sub-float v0, v9, v10

    .line 345
    :cond_2
    :goto_1
    invoke-virtual {v3, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 346
    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setSupMatrix(Landroid/graphics/Matrix;)V

    .line 347
    return-void

    .line 317
    .end local v0    # "deltaX":F
    .end local v8    # "width":F
    :cond_3
    iget v9, v4, Landroid/graphics/RectF;->top:F

    cmpl-float v9, v9, v11

    if-lez v9, :cond_4

    .line 319
    iget v9, v4, Landroid/graphics/RectF;->top:F

    neg-float v1, v9

    .line 320
    goto :goto_0

    .line 321
    :cond_4
    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v10, v6

    cmpg-float v9, v9, v10

    if-gez v9, :cond_1

    .line 323
    int-to-float v9, v6

    iget v10, v4, Landroid/graphics/RectF;->bottom:F

    sub-float v1, v9, v10

    goto :goto_0

    .line 336
    .restart local v0    # "deltaX":F
    .restart local v8    # "width":F
    :cond_5
    iget v9, v4, Landroid/graphics/RectF;->left:F

    cmpl-float v9, v9, v11

    if-lez v9, :cond_6

    .line 338
    iget v9, v4, Landroid/graphics/RectF;->left:F

    neg-float v0, v9

    .line 339
    goto :goto_1

    .line 340
    :cond_6
    iget v9, v4, Landroid/graphics/RectF;->right:F

    int-to-float v10, v7

    cmpg-float v9, v9, v10

    if-gez v9, :cond_2

    .line 342
    int-to-float v9, v7

    iget v10, v4, Landroid/graphics/RectF;->right:F

    sub-float v0, v9, v10

    goto :goto_1
.end method

.method public setCopyToDrawCanvasRoi(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "roi"    # Landroid/graphics/Rect;

    .prologue
    .line 1548
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1549
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$60(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Rect;)V

    .line 1550
    :cond_0
    return-void
.end method

.method public setDecodedImage(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/net/Uri;I)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "decodeFrom"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 757
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 759
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$50(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 760
    packed-switch p4, :pswitch_data_0

    .line 864
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 766
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$41(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/net/Uri;)V

    .line 767
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$17(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 768
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$51(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    .line 769
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$52(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 770
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$53(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 771
    invoke-virtual {p0, p1, p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->determinePersonalPage(Landroid/content/Context;Ljava/lang/Object;)V

    .line 772
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$51(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    .line 773
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$52(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 774
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$53(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 780
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 781
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    mul-int/2addr v1, v2

    new-array v1, v1, [I

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 782
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBuffer:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$38(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    .line 783
    const/4 v2, 0x0

    .line 784
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    .line 785
    const/4 v4, 0x0

    .line 786
    const/4 v5, 0x0

    .line 787
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v6

    .line 788
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v7

    .line 782
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 789
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalMaskRoi:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$54(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 791
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrigianlMaskBuff:[B
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$55(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[B

    move-result-object v0

    if-eqz v0, :cond_1

    .line 793
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 795
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    mul-int/2addr v1, v2

    new-array v1, v1, [B

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 803
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPreviewBuffer()V

    goto/16 :goto_0

    .line 796
    :catch_0
    move-exception v8

    .line 797
    .local v8, "e":Ljava/lang/NullPointerException;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TrayButton - setDecodedImage() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_1

    .line 824
    .end local v8    # "e":Ljava/lang/NullPointerException;
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$41(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/net/Uri;)V

    .line 825
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$17(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 826
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$51(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    .line 827
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$52(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 828
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$53(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 829
    invoke-virtual {p0, p1, p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->determinePersonalPage(Landroid/content/Context;Ljava/lang/Object;)V

    .line 830
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$51(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    .line 831
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$52(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 832
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$53(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 838
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 839
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    mul-int/2addr v1, v2

    new-array v1, v1, [I

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 840
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBuffer:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$38(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    .line 841
    const/4 v2, 0x0

    .line 842
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    .line 843
    const/4 v4, 0x0

    .line 844
    const/4 v5, 0x0

    .line 845
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v6

    .line 846
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v7

    .line 840
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 847
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalMaskRoi:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$54(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 849
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrigianlMaskBuff:[B
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$55(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    .line 851
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 853
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    mul-int/2addr v1, v2

    new-array v1, v1, [B

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 861
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPreviewBufferMultiGrid()V

    goto/16 :goto_0

    .line 854
    :catch_1
    move-exception v8

    .line 855
    .restart local v8    # "e":Ljava/lang/NullPointerException;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TrayButton - setDecodedImage() : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    goto :goto_2

    .line 760
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setDecodedImage(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/String;ZLjava/lang/String;I)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "isCamera"    # Z
    .param p5, "fileName"    # Ljava/lang/String;
    .param p6, "decodeFrom"    # I

    .prologue
    .line 915
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v2, :cond_0

    .line 917
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$50(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 918
    packed-switch p6, :pswitch_data_0

    .line 986
    :goto_0
    :pswitch_0
    const/4 v2, 0x5

    move/from16 v0, p6

    if-ne v0, v2, :cond_0

    .line 988
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$50(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 991
    :cond_0
    return-void

    .line 924
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$41(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/net/Uri;)V

    .line 925
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$51(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    .line 926
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v0, p3

    invoke-static {v2, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$51(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    .line 927
    move-object/from16 v0, p3

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->determinePersonalPage(Landroid/content/Context;Ljava/lang/Object;)V

    .line 928
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$52(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 929
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$53(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 931
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 932
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v4

    mul-int/2addr v3, v4

    new-array v3, v3, [I

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 933
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBuffer:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$38(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v3

    .line 934
    const/4 v4, 0x0

    .line 935
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v5

    .line 936
    const/4 v6, 0x0

    .line 937
    const/4 v7, 0x0

    .line 938
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v8

    .line 939
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v9

    move-object v2, p2

    .line 933
    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 941
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalMaskRoi:Landroid/graphics/Rect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$54(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v4

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-virtual {v2, v3, v4, v5, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 943
    invoke-static {p2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 945
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrigianlMaskBuff:[B
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$55(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[B

    move-result-object v2

    if-eqz v2, :cond_1

    .line 947
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 949
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v4

    mul-int/2addr v3, v4

    new-array v3, v3, [B

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 950
    if-eqz p4, :cond_2

    .line 952
    const/4 v6, 0x0

    .line 954
    .local v6, "location":Landroid/location/Location;
    invoke-static/range {p3 .. p3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotateDegree(Ljava/lang/String;)I

    move-result v7

    .line 956
    .local v7, "orientation":I
    :try_start_0
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v8, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->SAVE_DIR:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    move-object/from16 v3, p5

    move-object/from16 v9, p5

    invoke-static/range {v2 .. v11}, Lcom/sec/android/mimage/photoretouching/Core/Image;->addImage(Landroid/content/ContentResolver;Ljava/lang/String;JLandroid/location/Location;ILjava/lang/String;Ljava/lang/String;II)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v12, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$41(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/net/Uri;)V

    .line 958
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mUri:Landroid/net/Uri;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$56(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 964
    .end local v6    # "location":Landroid/location/Location;
    .end local v7    # "orientation":I
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPreviewBuffer()V

    goto/16 :goto_0

    .line 970
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$41(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/net/Uri;)V

    .line 971
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$51(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    .line 972
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v0, p3

    invoke-static {v2, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$51(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    .line 973
    move-object/from16 v0, p3

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->determinePersonalPage(Landroid/content/Context;Ljava/lang/Object;)V

    .line 974
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$52(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 975
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$53(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 977
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$17(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 978
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v4, 0x1

    invoke-virtual {p2, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$17(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 979
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V

    .line 980
    const/4 p2, 0x0

    .line 982
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalMaskRoi:Landroid/graphics/Rect;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$54(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v4

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-virtual {v2, v3, v4, v5, v8}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    .line 960
    .restart local v6    # "location":Landroid/location/Location;
    .restart local v7    # "orientation":I
    :catch_0
    move-exception v2

    goto :goto_1

    .line 918
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setDecodedImage(Landroid/content/Context;[IIILandroid/net/Uri;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "buffer"    # [I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "uri"    # Landroid/net/Uri;
    .param p6, "decodeFrom"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 867
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 869
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$50(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 870
    packed-switch p6, :pswitch_data_0

    .line 912
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 876
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$41(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/net/Uri;)V

    .line 877
    invoke-virtual {p0, p1, p5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->determinePersonalPage(Landroid/content/Context;Ljava/lang/Object;)V

    .line 878
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$51(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    .line 879
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$52(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 880
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$53(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 882
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 883
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 884
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalMaskRoi:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$54(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 886
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrigianlMaskBuff:[B
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$55(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[B

    move-result-object v0

    if-eqz v0, :cond_1

    .line 888
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 890
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    mul-int/2addr v1, v2

    new-array v1, v1, [B

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 891
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPreviewBuffer()V

    goto :goto_0

    .line 896
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$41(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/net/Uri;)V

    .line 897
    invoke-virtual {p0, p1, p5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->determinePersonalPage(Landroid/content/Context;Ljava/lang/Object;)V

    .line 898
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$51(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    .line 899
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$52(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 900
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$53(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 902
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, p4, v1}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$17(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 903
    const/4 p2, 0x0

    .line 904
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalMaskRoi:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$54(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 905
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrigianlMaskBuff:[B
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$55(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    .line 907
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    goto/16 :goto_0

    .line 870
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setDrawPath(Landroid/graphics/Path;)V
    .locals 1
    .param p1, "pathList"    # Landroid/graphics/Path;

    .prologue
    .line 638
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$43(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    .line 639
    return-void
.end method

.method public setFreeOriginalData()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1356
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1357
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 1358
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$52(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1359
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$53(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1360
    return-void
.end method

.method public setIsEdited(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 1553
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1554
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$50(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1555
    :cond_0
    return-void
.end method

.method public setOrientation(I)V
    .locals 0
    .param p1, "orientation"    # I

    .prologue
    .line 1488
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mOrientation:I

    .line 1489
    return-void
.end method

.method public setOriginalMaskRoi(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "roi"    # Landroid/graphics/Rect;

    .prologue
    .line 1543
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1544
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalMaskRoi:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$54(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1545
    :cond_0
    return-void
.end method

.method public setOriginalToPreviewMatrix(Landroid/graphics/Matrix;)V
    .locals 1
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 1522
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1523
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$18(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1524
    :cond_0
    const/4 p1, 0x0

    .line 1525
    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1533
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1534
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$51(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Ljava/lang/String;)V

    .line 1535
    :cond_0
    return-void
.end method

.method public setPersonalPage(Z)V
    .locals 1
    .param p1, "val"    # Z

    .prologue
    .line 736
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 737
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$44(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 738
    :cond_0
    return-void
.end method

.method public setPreviewBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1396
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$12(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1397
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1398
    return-void
.end method

.method public setPreviewBuffer()V
    .locals 19

    .prologue
    .line 82
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getDisplayWidthBasedOnLand()I

    move-result v10

    .line 83
    .local v10, "displayWidth":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getDisplayHeightBasedOnLand()I

    move-result v9

    .line 84
    .local v9, "displayHeight":I
    mul-int v16, v10, v9

    .line 85
    .local v16, "size":I
    if-gtz v16, :cond_0

    .line 191
    :goto_0
    return-void

    .line 87
    :cond_0
    const/16 v18, 0x1

    .line 88
    .local v18, "y":I
    int-to-float v2, v10

    int-to-float v3, v9

    div-float v17, v2, v3

    .line 90
    .local v17, "viewRatio":F
    :goto_1
    move/from16 v0, v18

    int-to-float v2, v0

    mul-float v2, v2, v17

    move/from16 v0, v18

    int-to-float v3, v0

    mul-float/2addr v2, v3

    const v3, 0x49f42400    # 2000000.0f

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_5

    .line 94
    move/from16 v0, v18

    int-to-float v2, v0

    mul-float v2, v2, v17

    float-to-int v15, v2

    .line 95
    .local v15, "resizeViewWidth":I
    move/from16 v14, v18

    .line 96
    .local v14, "resizeViewHeight":I
    mul-int v16, v15, v14

    .line 97
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    int-to-float v3, v3

    div-float v12, v2, v3

    .line 98
    .local v12, "imgRatio":F
    const/high16 v13, 0x3f800000    # 1.0f

    .line 99
    .local v13, "originalToPreviewRatio":F
    cmpl-float v2, v17, v12

    if-lez v2, :cond_6

    .line 101
    int-to-float v2, v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    int-to-float v3, v3

    div-float v13, v2, v3

    .line 107
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/high16 v3, 0x40400000    # 3.0f

    mul-float/2addr v3, v13

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$2(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V

    .line 108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, v13}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$3(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V

    .line 110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v13

    float-to-int v3, v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$4(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v13

    float-to-int v3, v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$5(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_1

    .line 115
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$4(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 118
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_2

    .line 119
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$5(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 122
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "KHJ4 setPreviewBuffer() originalToPreviewRatio="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 123
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "KHJ4 setPreviewBuffer() mSingleProperty.mPreviewWidth="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 124
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "KHJ4 setPreviewBuffer() mSingleProperty.mPreviewHeight="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    mul-int v16, v2, v3

    .line 127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move/from16 v0, v16

    new-array v3, v0, [I

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$8(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 128
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move/from16 v0, v16

    new-array v3, v0, [I

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$9(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 134
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 135
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    .line 136
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v4

    .line 137
    const/4 v5, 0x1

    .line 134
    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 139
    .local v1, "preview":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v2

    .line 140
    const/4 v3, 0x0

    .line 141
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v4

    .line 142
    const/4 v5, 0x0

    .line 143
    const/4 v6, 0x0

    .line 144
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v7

    .line 145
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v8

    .line 139
    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 147
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$12(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 148
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$14(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 151
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 155
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move/from16 v0, v16

    new-array v3, v0, [B

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$14(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 156
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move/from16 v0, v16

    new-array v3, v0, [I

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$16(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v2

    if-eqz v2, :cond_3

    .line 163
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$16(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v6

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 166
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 167
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$17(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 168
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 169
    const/4 v1, 0x0

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$18(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 172
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$18(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v2, v13, v13}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 173
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, v13}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$19(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V

    .line 176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPathBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$20(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 180
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v2, :cond_4

    .line 181
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mInitialPreviewInputBuff:[I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$21(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v6

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 182
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$22(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 183
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$23(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 185
    :cond_4
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSetPreviewBuffer:Z

    .line 186
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mIsReadyForPreview:Z

    goto/16 :goto_0

    .line 91
    .end local v1    # "preview":Landroid/graphics/Bitmap;
    .end local v12    # "imgRatio":F
    .end local v13    # "originalToPreviewRatio":F
    .end local v14    # "resizeViewHeight":I
    .end local v15    # "resizeViewWidth":I
    :cond_5
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_1

    .line 105
    .restart local v12    # "imgRatio":F
    .restart local v13    # "originalToPreviewRatio":F
    .restart local v14    # "resizeViewHeight":I
    .restart local v15    # "resizeViewWidth":I
    :cond_6
    int-to-float v2, v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    int-to-float v3, v3

    div-float v13, v2, v3

    goto/16 :goto_2

    .line 158
    .restart local v1    # "preview":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v11

    .line 160
    .local v11, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v11}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_3
.end method

.method public setPreviewBufferMultiGrid()V
    .locals 15

    .prologue
    const v4, 0x1e8480

    const/4 v14, 0x0

    const/4 v13, 0x1

    const/4 v2, 0x0

    .line 195
    const/high16 v10, 0x3f800000    # 1.0f

    .line 197
    .local v10, "originalToPreviewRatio":F
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    mul-int v12, v1, v3

    .line 198
    .local v12, "size":I
    const/4 v11, 0x1

    .line 199
    .local v11, "ret":I
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "KHJ4 setPreviewBuffer() mSingleProperty.mOriginalWidth="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 200
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "KHJ4 setPreviewBuffer() mSingleProperty.mOriginalHeight="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 201
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "KHJ4 setPreviewBuffer() size="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 202
    if-le v12, v4, :cond_0

    .line 204
    const/4 v9, 0x1

    .local v9, "i":I
    :goto_0
    div-int v1, v12, v9

    if-gt v1, v4, :cond_3

    .line 208
    add-int/lit8 v11, v11, 0x1

    .line 211
    .end local v9    # "i":I
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    int-to-float v3, v11

    div-float v10, v1, v3

    .line 212
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/high16 v3, 0x40400000    # 3.0f

    mul-float/2addr v3, v10

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$2(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V

    .line 213
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v1, v10}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$3(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V

    .line 215
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v10

    float-to-int v3, v3

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$4(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 216
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v10

    float-to-int v3, v3

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$5(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 218
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "KHJ4 setPreviewBuffer() originalToPreviewRatio="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 219
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "KHJ4 setPreviewBuffer() mSingleProperty.mPreviewWidth="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 220
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "KHJ4 setPreviewBuffer() mSingleProperty.mPreviewHeight="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 222
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    mul-int v12, v1, v3

    .line 223
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    new-array v3, v12, [I

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$8(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 224
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    new-array v3, v12, [I

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$9(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 229
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 230
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    .line 231
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v4

    .line 229
    invoke-static {v1, v3, v4, v13}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 233
    .local v0, "preview":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    .line 235
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    .line 238
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v6

    .line 239
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v7

    move v4, v2

    move v5, v2

    .line 233
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 241
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$12(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 242
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v3, v13}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 244
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v1, v14}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$14(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 245
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v1, v14}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 249
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    new-array v3, v12, [B

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$14(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 250
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    new-array v3, v12, [I

    invoke-static {v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$16(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    if-eqz v1, :cond_1

    .line 257
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$16(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v4

    array-length v4, v4

    invoke-static {v1, v2, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 260
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 261
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v1, v14}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$17(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 262
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 263
    const/4 v0, 0x0

    .line 265
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$18(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 266
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$18(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v1, v10, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 267
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v1, v10}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$19(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V

    .line 270
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPathBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$20(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 274
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v1, :cond_2

    .line 275
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mInitialPreviewInputBuff:[I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$21(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v4

    array-length v4, v4

    invoke-static {v1, v2, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 276
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$22(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 277
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$23(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 279
    :cond_2
    iput-boolean v13, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSetPreviewBuffer:Z

    .line 280
    iput-boolean v13, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mIsReadyForPreview:Z

    .line 281
    return-void

    .line 206
    .end local v0    # "preview":Landroid/graphics/Bitmap;
    .restart local v9    # "i":I
    :cond_3
    move v11, v9

    .line 204
    mul-int/lit8 v9, v9, 0x2

    goto/16 :goto_0

    .line 252
    .end local v9    # "i":I
    .restart local v0    # "preview":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v8

    .line 254
    .local v8, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v8}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public setPreviewMaskRoi(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "roi"    # Landroid/graphics/Rect;

    .prologue
    .line 1538
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1539
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewMaskRoi:Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$59(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1540
    :cond_0
    return-void
.end method

.method public setPreviewMatrix()V
    .locals 10

    .prologue
    .line 350
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getDisplayWidthBasedOnLand()I

    move-result v1

    .line 351
    .local v1, "displayWidth":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getDisplayHeightBasedOnLand()I

    move-result v0

    .line 352
    .local v0, "displayHeight":I
    mul-int v6, v1, v0

    .line 353
    .local v6, "size":I
    if-gtz v6, :cond_0

    .line 380
    :goto_0
    return-void

    .line 356
    :cond_0
    int-to-float v8, v1

    int-to-float v9, v0

    div-float v7, v8, v9

    .line 362
    .local v7, "viewRatio":F
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v5

    .line 363
    .local v5, "resizeViewWidth":I
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v4

    .line 364
    .local v4, "resizeViewHeight":I
    mul-int v6, v5, v4

    .line 365
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v9

    int-to-float v9, v9

    div-float v2, v8, v9

    .line 366
    .local v2, "imgRatio":F
    const/high16 v3, 0x3f800000    # 1.0f

    .line 367
    .local v3, "originalToPreviewRatio":F
    cmpl-float v8, v7, v2

    if-lez v8, :cond_1

    .line 369
    int-to-float v8, v4

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v9

    int-to-float v9, v9

    div-float v3, v8, v9

    .line 376
    :goto_1
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$18(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Matrix;->reset()V

    .line 377
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalToPreviewMatrix:Landroid/graphics/Matrix;
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$18(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v8

    invoke-virtual {v8, v3, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 378
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v8, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$19(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V

    goto :goto_0

    .line 373
    :cond_1
    int-to-float v8, v5

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v9

    int-to-float v9, v9

    div-float v3, v8, v9

    goto :goto_1
.end method

.method public setSaved()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1368
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1370
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$50(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1371
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$57(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1372
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$58(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1374
    :cond_0
    return-void
.end method

.method public setSupMatrix(Landroid/graphics/Matrix;)V
    .locals 1
    .param p1, "supMatrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 1517
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1518
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mSupportMatrix:Landroid/graphics/Matrix;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$26(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1519
    :cond_0
    return-void
.end method

.method public setTextureData(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[I>;)V"
        }
    .end annotation

    .prologue
    .line 994
    .local p1, "textureData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;[I>;"
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mTextureData:Ljava/util/HashMap;

    .line 995
    return-void
.end method

.method public setUri(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1528
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1529
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$41(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/net/Uri;)V

    .line 1530
    :cond_0
    return-void
.end method

.method public setViewSize(II)V
    .locals 1
    .param p1, "viewWidth"    # I
    .param p2, "viewHeight"    # I

    .prologue
    .line 290
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(IILcom/sec/android/mimage/photoretouching/Core/ImageData$setViewSizeMatrixCallback;)V

    .line 291
    return-void
.end method

.method public setViewSize(IILcom/sec/android/mimage/photoretouching/Core/ImageData$setViewSizeMatrixCallback;)V
    .locals 29
    .param p1, "viewWidth"    # I
    .param p2, "viewHeight"    # I
    .param p3, "callback"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData$setViewSizeMatrixCallback;

    .prologue
    .line 385
    const/16 v23, 0x0

    .line 386
    .local v23, "virtualViewWidth":I
    const/16 v22, 0x0

    .line 387
    .local v22, "virtualViewHeight":I
    const/high16 v12, 0x3f800000    # 1.0f

    .line 388
    .local v12, "scale":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v10, v25, v26

    .line 389
    .local v10, "previewRatio":F
    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v21, v25, v26

    .line 390
    .local v21, "viewRatio":F
    cmpl-float v25, v21, v10

    if-lez v25, :cond_6

    .line 392
    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v23, v0

    .line 393
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v22

    .line 394
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v12, v25, v26

    .line 402
    :goto_0
    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "setViewSize virtualViewWidth,Height:("

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ", "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ")"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 403
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, v23

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$27(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$28(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    sub-int v26, v23, v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    const/high16 v27, 0x3f000000    # 0.5f

    mul-float v26, v26, v27

    invoke-static/range {v25 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$29(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V

    .line 407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    sub-int v26, v22, v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    const/high16 v27, 0x3f000000    # 0.5f

    mul-float v26, v26, v27

    invoke-static/range {v25 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$30(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPathBitmap:Landroid/graphics/Bitmap;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$20(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    if-eqz v25, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewWidth:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$24(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v25

    if-lez v25, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewHeight:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$25(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v25

    if-lez v25, :cond_0

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewWidth:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$24(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewHeight:I
    invoke-static/range {v27 .. v27}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$25(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v27

    sget-object v28, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v26 .. v28}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$31(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 413
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static/range {v27 .. v27}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v27

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->initDrawCanvasRoi(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;II)V

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$32(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Matrix;->reset()V

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$32(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v25

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v12, v12, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 418
    sget v25, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->MAX_DECORATION_RATIO:F

    cmpl-float v25, v10, v25

    if-gtz v25, :cond_1

    sget v25, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->MIN_DECORATION_RATIO:F

    cmpg-float v25, v10, v25

    if-gez v25, :cond_7

    .line 419
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static/range {v25 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$33(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 424
    :goto_1
    if-eqz p3, :cond_8

    .line 425
    invoke-interface/range {p3 .. p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$setViewSizeMatrixCallback;->pinchZoomConfigurationChanged()V

    .line 480
    :goto_2
    const/high16 v5, 0x3f800000    # 1.0f

    .line 481
    .local v5, "configchangedScale":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSetPreviewBuffer:Z

    move/from16 v25, v0

    if-eqz v25, :cond_5

    .line 483
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v10, v25, v26

    .line 484
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v21, v25, v26

    .line 485
    cmpl-float v25, v21, v10

    if-lez v25, :cond_10

    .line 487
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v23, v0

    .line 488
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v22

    .line 489
    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v5, v25, v26

    .line 498
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mOrientation:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_15

    .line 500
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitSupportMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$34(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mSupportMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$26(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 501
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$35(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$32(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 503
    new-instance v16, Landroid/graphics/Matrix;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Matrix;-><init>()V

    .line 504
    .local v16, "tempMatrix":Landroid/graphics/Matrix;
    const/16 v25, 0x9

    move/from16 v0, v25

    new-array v0, v0, [F

    move-object/from16 v18, v0

    .line 505
    .local v18, "tempValue":[F
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 506
    const/16 v17, 0x0

    .line 507
    .local v17, "tempRect":Landroid/graphics/RectF;
    new-instance v17, Landroid/graphics/RectF;

    .end local v17    # "tempRect":Landroid/graphics/RectF;
    const/16 v25, 0x0

    const/16 v26, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move-object/from16 v0, v17

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 509
    .restart local v17    # "tempRect":Landroid/graphics/RectF;
    if-eqz v16, :cond_2

    .line 510
    invoke-virtual/range {v16 .. v17}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 512
    :cond_2
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/RectF;->width()F

    move-result v19

    .line 513
    .local v19, "tempWidth":F
    const/4 v13, 0x0

    .line 515
    .local v13, "tempDeltaX":F
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v25, v0

    cmpg-float v25, v19, v25

    if-gez v25, :cond_11

    .line 517
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v25, v0

    sub-float v25, v25, v19

    const/high16 v26, 0x40000000    # 2.0f

    div-float v25, v25, v26

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v26, v0

    sub-float v13, v25, v26

    .line 530
    :cond_3
    :goto_4
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/RectF;->height()F

    move-result v15

    .line 532
    .local v15, "tempHeight":F
    const/4 v14, 0x0

    .line 534
    .local v14, "tempDeltaY":F
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v25, v0

    cmpg-float v25, v15, v25

    if-gez v25, :cond_13

    .line 536
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v25, v0

    sub-float v25, v25, v15

    const/high16 v26, 0x40000000    # 2.0f

    div-float v25, v25, v26

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    sub-float v14, v25, v26

    .line 549
    :cond_4
    :goto_5
    move-object/from16 v0, v16

    invoke-virtual {v0, v13, v14}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandSupportMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$36(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 552
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$37(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Matrix;->reset()V

    .line 553
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$37(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v25

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 612
    :goto_6
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSetPreviewBuffer:Z

    .line 616
    .end local v13    # "tempDeltaX":F
    .end local v14    # "tempDeltaY":F
    .end local v15    # "tempHeight":F
    .end local v16    # "tempMatrix":Landroid/graphics/Matrix;
    .end local v17    # "tempRect":Landroid/graphics/RectF;
    .end local v18    # "tempValue":[F
    .end local v19    # "tempWidth":F
    :cond_5
    return-void

    .line 398
    .end local v5    # "configchangedScale":F
    :cond_6
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v22, v0

    .line 399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v23

    .line 400
    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v12, v25, v26

    goto/16 :goto_0

    .line 421
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    invoke-static/range {v25 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$33(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    goto/16 :goto_1

    .line 428
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mSupportMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$26(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Matrix;->reset()V

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mSupportMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$26(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v9

    .line 430
    .local v9, "matrix":Landroid/graphics/Matrix;
    const/16 v25, 0x9

    move/from16 v0, v25

    new-array v0, v0, [F

    move-object/from16 v20, v0

    .line 431
    .local v20, "value":[F
    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 432
    const/4 v11, 0x0

    .line 433
    .local v11, "r":Landroid/graphics/RectF;
    new-instance v11, Landroid/graphics/RectF;

    .end local v11    # "r":Landroid/graphics/RectF;
    const/16 v25, 0x0

    const/16 v26, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-direct {v11, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 435
    .restart local v11    # "r":Landroid/graphics/RectF;
    if-eqz v9, :cond_9

    .line 436
    invoke-virtual {v9, v11}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 438
    :cond_9
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v8

    .line 439
    .local v8, "height":F
    const/4 v7, 0x0

    .line 441
    .local v7, "deltaY":F
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v25, v0

    cmpg-float v25, v8, v25

    if-gez v25, :cond_c

    .line 443
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v25, v0

    sub-float v25, v25, v8

    const/high16 v26, 0x40000000    # 2.0f

    div-float v25, v25, v26

    iget v0, v11, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    sub-float v7, v25, v26

    .line 456
    :cond_a
    :goto_7
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v24

    .line 458
    .local v24, "width":F
    const/4 v6, 0x0

    .line 460
    .local v6, "deltaX":F
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v25, v0

    cmpg-float v25, v24, v25

    if-gez v25, :cond_e

    .line 462
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v25, v0

    sub-float v25, v25, v24

    const/high16 v26, 0x40000000    # 2.0f

    div-float v25, v25, v26

    iget v0, v11, Landroid/graphics/RectF;->left:F

    move/from16 v26, v0

    sub-float v6, v25, v26

    .line 475
    :cond_b
    :goto_8
    invoke-virtual {v9, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 476
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setSupMatrix(Landroid/graphics/Matrix;)V

    .line 477
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPreviewMatrix()V

    goto/16 :goto_2

    .line 447
    .end local v6    # "deltaX":F
    .end local v24    # "width":F
    :cond_c
    iget v0, v11, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    const/16 v26, 0x0

    cmpl-float v25, v25, v26

    if-lez v25, :cond_d

    .line 449
    iget v0, v11, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    move/from16 v0, v25

    neg-float v7, v0

    .line 450
    goto :goto_7

    .line 451
    :cond_d
    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v26, v0

    cmpg-float v25, v25, v26

    if-gez v25, :cond_a

    .line 453
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v25, v0

    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    move/from16 v26, v0

    sub-float v7, v25, v26

    goto :goto_7

    .line 466
    .restart local v6    # "deltaX":F
    .restart local v24    # "width":F
    :cond_e
    iget v0, v11, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    const/16 v26, 0x0

    cmpl-float v25, v25, v26

    if-lez v25, :cond_f

    .line 468
    iget v0, v11, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    move/from16 v0, v25

    neg-float v6, v0

    .line 469
    goto :goto_8

    .line 470
    :cond_f
    iget v0, v11, Landroid/graphics/RectF;->right:F

    move/from16 v25, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v26, v0

    cmpg-float v25, v25, v26

    if-gez v25, :cond_b

    .line 472
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v25, v0

    iget v0, v11, Landroid/graphics/RectF;->right:F

    move/from16 v26, v0

    sub-float v6, v25, v26

    goto :goto_8

    .line 493
    .end local v6    # "deltaX":F
    .end local v7    # "deltaY":F
    .end local v8    # "height":F
    .end local v9    # "matrix":Landroid/graphics/Matrix;
    .end local v11    # "r":Landroid/graphics/RectF;
    .end local v20    # "value":[F
    .end local v24    # "width":F
    .restart local v5    # "configchangedScale":F
    :cond_10
    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v27, v0

    div-float v26, v26, v27

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v22, v0

    .line 494
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v23

    .line 495
    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v5, v25, v26

    goto/16 :goto_3

    .line 521
    .restart local v13    # "tempDeltaX":F
    .restart local v16    # "tempMatrix":Landroid/graphics/Matrix;
    .restart local v17    # "tempRect":Landroid/graphics/RectF;
    .restart local v18    # "tempValue":[F
    .restart local v19    # "tempWidth":F
    :cond_11
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    const/16 v26, 0x0

    cmpl-float v25, v25, v26

    if-lez v25, :cond_12

    .line 523
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    move/from16 v0, v25

    neg-float v13, v0

    .line 524
    goto/16 :goto_4

    .line 525
    :cond_12
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v26, v0

    cmpg-float v25, v25, v26

    if-gez v25, :cond_3

    .line 527
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v26, v0

    sub-float v13, v25, v26

    goto/16 :goto_4

    .line 540
    .restart local v14    # "tempDeltaY":F
    .restart local v15    # "tempHeight":F
    :cond_13
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    const/16 v26, 0x0

    cmpl-float v25, v25, v26

    if-lez v25, :cond_14

    .line 542
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    move/from16 v0, v25

    neg-float v14, v0

    .line 543
    goto/16 :goto_5

    .line 544
    :cond_14
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v26, v0

    cmpg-float v25, v25, v26

    if-gez v25, :cond_4

    .line 546
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v26, v0

    sub-float v14, v25, v26

    goto/16 :goto_5

    .line 557
    .end local v13    # "tempDeltaX":F
    .end local v14    # "tempDeltaY":F
    .end local v15    # "tempHeight":F
    .end local v16    # "tempMatrix":Landroid/graphics/Matrix;
    .end local v17    # "tempRect":Landroid/graphics/RectF;
    .end local v18    # "tempValue":[F
    .end local v19    # "tempWidth":F
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandSupportMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$36(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mSupportMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$26(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 558
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgLandViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$37(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$32(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 560
    new-instance v16, Landroid/graphics/Matrix;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Matrix;-><init>()V

    .line 561
    .restart local v16    # "tempMatrix":Landroid/graphics/Matrix;
    const/16 v25, 0x9

    move/from16 v0, v25

    new-array v0, v0, [F

    move-object/from16 v18, v0

    .line 562
    .restart local v18    # "tempValue":[F
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 563
    const/16 v17, 0x0

    .line 564
    .restart local v17    # "tempRect":Landroid/graphics/RectF;
    new-instance v17, Landroid/graphics/RectF;

    .end local v17    # "tempRect":Landroid/graphics/RectF;
    const/16 v25, 0x0

    const/16 v26, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move-object/from16 v0, v17

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 566
    .restart local v17    # "tempRect":Landroid/graphics/RectF;
    if-eqz v16, :cond_16

    .line 567
    invoke-virtual/range {v16 .. v17}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 569
    :cond_16
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/RectF;->width()F

    move-result v19

    .line 570
    .restart local v19    # "tempWidth":F
    const/4 v13, 0x0

    .line 572
    .restart local v13    # "tempDeltaX":F
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v25, v0

    cmpg-float v25, v19, v25

    if-gez v25, :cond_19

    .line 574
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v25, v0

    sub-float v25, v25, v19

    const/high16 v26, 0x40000000    # 2.0f

    div-float v25, v25, v26

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v26, v0

    sub-float v13, v25, v26

    .line 587
    :cond_17
    :goto_9
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/RectF;->height()F

    move-result v15

    .line 589
    .restart local v15    # "tempHeight":F
    const/4 v14, 0x0

    .line 591
    .restart local v14    # "tempDeltaY":F
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v25, v0

    cmpg-float v25, v15, v25

    if-gez v25, :cond_1b

    .line 593
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v25, v0

    sub-float v25, v25, v15

    const/high16 v26, 0x40000000    # 2.0f

    div-float v25, v25, v26

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v26, v0

    sub-float v14, v25, v26

    .line 606
    :cond_18
    :goto_a
    move-object/from16 v0, v16

    invoke-virtual {v0, v13, v14}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 608
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitSupportMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$34(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 609
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$35(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Matrix;->reset()V

    .line 610
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrgPortraitViewTransformMatrix:Landroid/graphics/Matrix;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$35(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Matrix;

    move-result-object v25

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto/16 :goto_6

    .line 578
    .end local v14    # "tempDeltaY":F
    .end local v15    # "tempHeight":F
    :cond_19
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    const/16 v26, 0x0

    cmpl-float v25, v25, v26

    if-lez v25, :cond_1a

    .line 580
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v25, v0

    move/from16 v0, v25

    neg-float v13, v0

    .line 581
    goto :goto_9

    .line 582
    :cond_1a
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v26, v0

    cmpg-float v25, v25, v26

    if-gez v25, :cond_17

    .line 584
    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v26, v0

    sub-float v13, v25, v26

    goto/16 :goto_9

    .line 597
    .restart local v14    # "tempDeltaY":F
    .restart local v15    # "tempHeight":F
    :cond_1b
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    const/16 v26, 0x0

    cmpl-float v25, v25, v26

    if-lez v25, :cond_1c

    .line 599
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    move/from16 v0, v25

    neg-float v14, v0

    .line 600
    goto/16 :goto_a

    .line 601
    :cond_1c
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v25, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v26, v0

    cmpg-float v25, v25, v26

    if-gez v25, :cond_18

    .line 603
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v26, v0

    sub-float v14, v25, v26

    goto/16 :goto_a
.end method

.method public updateDrawing()V
    .locals 2

    .prologue
    .line 1387
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$50(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1388
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$57(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1389
    return-void
.end method

.method public updateOrgToPreviewRatio(I)V
    .locals 3
    .param p1, "previewWdt"    # I

    .prologue
    .line 1329
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    int-to-float v1, p1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->getOriginalWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$19(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V

    .line 1330
    return-void
.end method

.method public updateOriginalBuffer(Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 1290
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 1291
    .local v3, "w":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 1293
    .local v7, "h":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$52(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1294
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$53(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1296
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1297
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$10(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$17(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1298
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalWidth:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$0(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalHeight:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$1(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    if-eq v0, v7, :cond_1

    .line 1300
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1301
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 1302
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPathBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$20(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$17(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1304
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    mul-int v1, v3, v7

    new-array v1, v1, [I

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1305
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    mul-int v1, v3, v7

    new-array v1, v1, [B

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 1309
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    mul-int v1, v3, v7

    new-array v1, v1, [I

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1310
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBuffer:[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$38(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    move-object v0, p1

    move v4, v2

    move v5, v2

    move v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1311
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v4, 0x1

    invoke-virtual {p1, v1, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$17(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1312
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1313
    const/4 p1, 0x0

    .line 1318
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$57(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1325
    return-void
.end method

.method public updateOriginalBuffer([I)V
    .locals 3
    .param p1, "input"    # [I

    .prologue
    const/4 v2, 0x0

    .line 1377
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOriginalBuffer:[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$38(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v0

    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1380
    return-void
.end method

.method public updateOriginalBuffer([III)V
    .locals 3
    .param p1, "input"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v1, 0x0

    .line 1334
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1335
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1337
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$52(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1338
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$53(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1340
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$39(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1341
    const/4 p1, 0x0

    .line 1343
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrigianlMaskBuff:[B
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$55(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1345
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 1346
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1348
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    mul-int v1, p2, p3

    new-array v1, v1, [B

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$40(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 1351
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$57(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1352
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->getOriginalWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$19(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;F)V

    .line 1353
    return-void
.end method

.method public updateOriginalMaskBuff([B)V
    .locals 3
    .param p1, "input"    # [B

    .prologue
    const/4 v2, 0x0

    .line 1383
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mOrigianlMaskBuff:[B
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$55(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[B

    move-result-object v0

    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1384
    return-void
.end method

.method public updatePinchZoomMatrix(Landroid/graphics/Matrix;)V
    .locals 8
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 1500
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    if-eqz v0, :cond_0

    .line 1502
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 1504
    .local v6, "m":Landroid/graphics/Matrix;
    invoke-virtual {v6, p1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1506
    const/16 v0, 0x9

    new-array v7, v0, [F

    .line 1507
    .local v7, "v":[F
    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1508
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    .line 1509
    const/4 v0, 0x0

    aget v2, v7, v0

    .line 1510
    const/4 v0, 0x4

    aget v3, v7, v0

    .line 1511
    const/4 v0, 0x2

    aget v4, v7, v0

    .line 1512
    const/4 v0, 0x5

    aget v5, v7, v0

    move-object v0, p0

    .line 1508
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->initDrawCanvasRoi(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;FFFF)V

    .line 1514
    .end local v6    # "m":Landroid/graphics/Matrix;
    .end local v7    # "v":[F
    :cond_0
    return-void
.end method

.method public updatePreviewBuffer(Landroid/graphics/Bitmap;)V
    .locals 10
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 1182
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 1183
    .local v3, "w":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 1185
    .local v7, "h":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    if-eq v0, v7, :cond_1

    .line 1187
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$4(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1188
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$5(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1190
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$8(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1191
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    mul-int v1, v3, v7

    new-array v1, v1, [I

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$8(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1194
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1195
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$14(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 1197
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    mul-int v1, v3, v7

    new-array v1, v1, [I

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1198
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    mul-int v1, v3, v7

    new-array v1, v1, [B

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$14(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 1201
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$12(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1202
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    move-object v0, p1

    move v4, v2

    move v5, v2

    move v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1203
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p1, v1, v9}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1204
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1205
    const/4 p1, 0x0

    .line 1207
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$16(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    mul-int v4, v3, v7

    invoke-static {v0, v2, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1209
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$50(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1210
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$57(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1212
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$43(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Path;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1214
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$43(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Path;

    move-result-object v8

    .line 1215
    .local v8, "path":Landroid/graphics/Path;
    invoke-virtual {v8}, Landroid/graphics/Path;->reset()V

    .line 1217
    .end local v8    # "path":Landroid/graphics/Path;
    :cond_2
    return-void
.end method

.method public updatePreviewBuffer([I)V
    .locals 7
    .param p1, "input"    # [I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1165
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$8(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1166
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$8(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1168
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$12(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1170
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1171
    .local v0, "temp":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v2, v6}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1172
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1175
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$16(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v4

    mul-int/2addr v3, v4

    invoke-static {v1, v5, v2, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1177
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v1, v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$50(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1178
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v1, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$57(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1179
    return-void
.end method

.method public updatePreviewBuffer([III)V
    .locals 7
    .param p1, "input"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1221
    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    .line 1251
    :cond_0
    :goto_0
    return-void

    .line 1223
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$8(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1224
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$8(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1227
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$12(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1228
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$4(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1229
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$5(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1230
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1231
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$14(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 1233
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    mul-int v3, p2, p3

    new-array v3, v3, [I

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1234
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    mul-int v3, p2, p3

    new-array v3, v3, [B

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$14(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 1236
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, p3, v2}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1237
    .local v1, "temp":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v1, v3, v6}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1238
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1241
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$16(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v3

    mul-int v4, p2, p3

    invoke-static {v2, v5, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1243
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$50(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1244
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$57(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1246
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$43(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Path;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1248
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$43(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Path;

    move-result-object v0

    .line 1249
    .local v0, "path":Landroid/graphics/Path;
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    goto :goto_0
.end method

.method public updatePreviewBufferForMirror([III)V
    .locals 7
    .param p1, "input"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1256
    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    .line 1286
    :cond_0
    :goto_0
    return-void

    .line 1258
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$8(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1259
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$8(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1262
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$12(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1263
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$4(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1264
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, p3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$5(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1265
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1266
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$14(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 1268
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    mul-int v3, p2, p3

    new-array v3, v3, [I

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1269
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    mul-int v3, p2, p3

    new-array v3, v3, [B

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$14(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[B)V

    .line 1271
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, p3, v2}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1272
    .local v1, "temp":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v1, v3, v6}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$13(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Landroid/graphics/Bitmap;)V

    .line 1273
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1276
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewInputBuff:[I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$11(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$16(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v3

    mul-int v4, p2, p3

    invoke-static {v2, v5, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1278
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$50(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1279
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v2, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$57(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 1281
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$43(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Path;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1283
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mDrawPathForSelectedArea:Landroid/graphics/Path;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$43(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)Landroid/graphics/Path;

    move-result-object v0

    .line 1284
    .local v0, "path":Landroid/graphics/Path;
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    goto :goto_0
.end method

.method public updatePreviewOutputBuffer(Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v2, 0x0

    .line 1146
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 1147
    .local v3, "w":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 1149
    .local v7, "h":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$4(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1150
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    invoke-static {v0, v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$5(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;I)V

    .line 1152
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v0

    if-eq v0, v7, :cond_1

    .line 1154
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1155
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    mul-int v1, v3, v7

    new-array v1, v1, [I

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$15(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;[I)V

    .line 1159
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewOutBuff:[I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$16(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)[I

    move-result-object v1

    move-object v0, p1

    move v4, v2

    move v5, v2

    move v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 1160
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1161
    const/4 p1, 0x0

    .line 1162
    return-void
.end method

.method public updatePreviewRatio()V
    .locals 3

    .prologue
    .line 643
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewWidth:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$6(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->mPreviewHeight:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$7(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;)I

    move-result v2

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 646
    .local v0, "previewRatio":F
    sget v1, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->MAX_DECORATION_RATIO:F

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_0

    sget v1, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->MIN_DECORATION_RATIO:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 647
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$33(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    .line 650
    :goto_0
    return-void

    .line 649
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->mSingleProperty:Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;->access$33(Lcom/sec/android/mimage/photoretouching/Core/ImageData$Property;Z)V

    goto :goto_0
.end method
