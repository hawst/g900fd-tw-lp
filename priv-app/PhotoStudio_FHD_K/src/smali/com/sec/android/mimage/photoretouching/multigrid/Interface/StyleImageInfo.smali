.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;
.super Ljava/lang/Object;
.source "StyleImageInfo.java"


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private path:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->path:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->bitmap:Landroid/graphics/Bitmap;

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "p"    # Ljava/lang/String;
    .param p2, "b"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->path:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->bitmap:Landroid/graphics/Bitmap;

    .line 12
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->path:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->bitmap:Landroid/graphics/Bitmap;

    .line 14
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 29
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->bitmap:Landroid/graphics/Bitmap;

    .line 31
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->path:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public deleteData()V
    .locals 1

    .prologue
    .line 21
    const-string v0, "empty"

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->path:Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->bitmap:Landroid/graphics/Bitmap;

    .line 23
    return-void
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->path:Ljava/lang/String;

    return-object v0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "b"    # Landroid/graphics/Bitmap;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->bitmap:Landroid/graphics/Bitmap;

    .line 44
    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "p"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->path:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public setSytleInfo(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 0
    .param p1, "b"    # Landroid/graphics/Bitmap;
    .param p2, "p"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->bitmap:Landroid/graphics/Bitmap;

    .line 52
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/StyleImageInfo;->path:Ljava/lang/String;

    .line 53
    return-void
.end method
