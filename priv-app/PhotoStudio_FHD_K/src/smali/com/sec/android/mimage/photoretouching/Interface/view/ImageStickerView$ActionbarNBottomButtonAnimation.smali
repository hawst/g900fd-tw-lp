.class Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;
.super Ljava/lang/Object;
.source "ImageStickerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionbarNBottomButtonAnimation"
.end annotation


# instance fields
.field private mIsShowing:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1176
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    .line 1175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1174
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 1177
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 1178
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 1

    .prologue
    .line 1197
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 1198
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1200
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->hide()V

    .line 1202
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1204
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->drawerHide()V

    .line 1206
    :cond_1
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 1181
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    return v0
.end method

.method public show()V
    .locals 1

    .prologue
    .line 1185
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->mIsShowing:Z

    .line 1186
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1188
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->show()V

    .line 1190
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1192
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->drawerShow()V

    .line 1194
    :cond_1
    return-void
.end method
