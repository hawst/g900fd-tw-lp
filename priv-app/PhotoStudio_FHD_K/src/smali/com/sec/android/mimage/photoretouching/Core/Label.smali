.class public Lcom/sec/android/mimage/photoretouching/Core/Label;
.super Lcom/sec/android/mimage/photoretouching/Core/RectController;
.source "Label.java"


# instance fields
.field private OrgObjectBmp:Landroid/graphics/Bitmap;

.field private mBoundaryType:I

.field private mContext:Landroid/content/Context;

.field private mCurrentResId:I

.field private mFreeRect:Z

.field private mZOrder:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;-><init>()V

    .line 18
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mCurrentResId:I

    .line 19
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mZOrder:I

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mContext:Landroid/content/Context;

    .line 21
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mBoundaryType:I

    .line 22
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mFreeRect:Z

    .line 110
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;IIIZLandroid/graphics/Bitmap;I)V
    .locals 27
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imgData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p3, "resId"    # I
    .param p4, "zOrder"    # I
    .param p5, "boundaryType"    # I
    .param p6, "freeRect"    # Z
    .param p7, "labelBitmap"    # Landroid/graphics/Bitmap;
    .param p8, "handlerMaxSize"    # I

    .prologue
    .line 26
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p5

    move/from16 v4, p6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/RectController;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;IZ)V

    .line 18
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Label;->mCurrentResId:I

    .line 19
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Label;->mZOrder:I

    .line 20
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Label;->mContext:Landroid/content/Context;

    .line 21
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Label;->mBoundaryType:I

    .line 22
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Label;->mFreeRect:Z

    .line 27
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Label;->mContext:Landroid/content/Context;

    .line 28
    move/from16 v0, p5

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Label;->mBoundaryType:I

    .line 29
    move/from16 v0, p6

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Label;->mFreeRect:Z

    .line 30
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Label;->mCurrentResId:I

    .line 31
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Label;->mZOrder:I

    .line 33
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOrgPreviewWidth()I

    move-result v13

    .line 34
    .local v13, "orgviewWidth":I
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOrgPreviewHeight()I

    move-result v12

    .line 36
    .local v12, "orgviewHeight":I
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v16

    .line 37
    .local v16, "previewWidth":I
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v15

    .line 39
    .local v15, "previewHeight":I
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v25, v0

    int-to-float v0, v13

    move/from16 v26, v0

    div-float v19, v25, v26

    .line 40
    .local v19, "scaleWidth":F
    int-to-float v0, v15

    move/from16 v25, v0

    int-to-float v0, v12

    move/from16 v26, v0

    div-float v18, v25, v26

    .line 41
    .local v18, "scaleHeight":F
    add-float v25, v19, v18

    const/high16 v26, 0x40000000    # 2.0f

    div-float v7, v25, v26

    .line 46
    .local v7, "aveScale":F
    div-int/lit8 v23, v16, 0x2

    .line 47
    .local v23, "x_center":I
    div-int/lit8 v24, v15, 0x2

    .line 48
    .local v24, "y_center":I
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/Label;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 49
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Label;->OrgObjectBmp:Landroid/graphics/Bitmap;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 50
    .local v6, "ObjectWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Label;->OrgObjectBmp:Landroid/graphics/Bitmap;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 52
    .local v5, "ObjectHeight":I
    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "bigheadk, ObjectWidth = "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 53
    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "bigheadk, ObjectHeight = "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 55
    int-to-float v0, v6

    move/from16 v25, v0

    int-to-float v0, v5

    move/from16 v26, v0

    div-float v20, v25, v26

    .line 56
    .local v20, "tmpScale":F
    move/from16 v0, v16

    if-lt v6, v0, :cond_0

    .line 57
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v25, v0

    const v26, 0x3f333333    # 0.7f

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v6, v0

    .line 58
    int-to-float v0, v6

    move/from16 v25, v0

    div-float v25, v25, v20

    move/from16 v0, v25

    float-to-int v5, v0

    .line 61
    :cond_0
    int-to-float v0, v5

    move/from16 v25, v0

    move/from16 v0, p8

    int-to-float v0, v0

    move/from16 v26, v0

    mul-float v26, v26, v7

    cmpg-float v25, v25, v26

    if-gez v25, :cond_1

    .line 63
    move/from16 v0, p8

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v25, v25, v7

    move/from16 v0, v25

    float-to-int v5, v0

    .line 64
    int-to-float v0, v5

    move/from16 v25, v0

    mul-float v25, v25, v20

    move/from16 v0, v25

    float-to-int v6, v0

    .line 70
    :cond_1
    new-instance v17, Landroid/graphics/Rect;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Rect;-><init>()V

    .line 71
    .local v17, "roi":Landroid/graphics/Rect;
    div-int/lit8 v25, v6, 0x2

    sub-int v25, v23, v25

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 72
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    add-int v25, v25, v6

    add-int/lit8 v25, v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 73
    div-int/lit8 v25, v5, 0x2

    sub-int v25, v24, v25

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 74
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    add-int v25, v25, v5

    add-int/lit8 v25, v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 75
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/Label;->setRectRoi(Landroid/graphics/Rect;)V

    .line 80
    const/high16 v9, 0x3fc00000    # 1.5f

    .line 83
    .local v9, "maxScale":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v22

    .line 84
    .local v22, "viewTransformMatrix":Landroid/graphics/Matrix;
    const/16 v25, 0x9

    move/from16 v0, v25

    new-array v0, v0, [F

    move-object/from16 v21, v0

    .line 85
    .local v21, "val":[F
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 88
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_2

    const/4 v8, 0x1

    .line 89
    .local v8, "isImageLandscape":Z
    :goto_0
    const/high16 v14, 0x3f800000    # 1.0f

    .line 99
    .local v14, "orientationScale":F
    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3fc00000    # 1.5f

    mul-float v25, v25, v26

    mul-float v25, v25, v14

    move/from16 v0, v25

    float-to-int v10, v0

    .line 100
    .local v10, "maximumSize":I
    const/16 v25, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v25

    move/from16 v0, p8

    move/from16 v1, v25

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v25, v25, v7

    move/from16 v0, v25

    float-to-int v11, v0

    .line 104
    .local v11, "minimumSize":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/android/mimage/photoretouching/Core/Label;->setMaximumSize(I)V

    .line 105
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/mimage/photoretouching/Core/Label;->setMinimumSize(I)V

    .line 106
    return-void

    .line 88
    .end local v8    # "isImageLandscape":Z
    .end local v10    # "maximumSize":I
    .end local v11    # "minimumSize":I
    .end local v14    # "orientationScale":F
    :cond_2
    const/4 v8, 0x0

    goto :goto_0
.end method


# virtual methods
.method public EndMoveObject()V
    .locals 0

    .prologue
    .line 280
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->EndMoveObject()V

    .line 281
    return-void
.end method

.method public GetObjectBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->OrgObjectBmp:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public InitMoveObject(FF)I
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 271
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->InitMoveObject(FF)I

    move-result v0

    return v0
.end method

.method public StartMoveObject(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 276
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->StartMoveObject(FF)V

    .line 277
    return-void
.end method

.method public applyOriginal(Landroid/graphics/Bitmap;)V
    .locals 16
    .param p1, "originalBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 181
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v13}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalToPreviewRatio()F

    move-result v9

    .line 182
    .local v9, "scale":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getRoi()Landroid/graphics/RectF;

    move-result-object v6

    .line 183
    .local v6, "rectRoi":Landroid/graphics/RectF;
    new-instance v5, Landroid/graphics/Paint;

    const/4 v13, 0x1

    invoke-direct {v5, v13}, Landroid/graphics/Paint;-><init>(I)V

    .line 184
    .local v5, "paint":Landroid/graphics/Paint;
    const/4 v13, 0x1

    invoke-virtual {v5, v13}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 186
    iget v13, v6, Landroid/graphics/RectF;->left:F

    div-float/2addr v13, v9

    iput v13, v6, Landroid/graphics/RectF;->left:F

    .line 187
    iget v13, v6, Landroid/graphics/RectF;->top:F

    div-float/2addr v13, v9

    iput v13, v6, Landroid/graphics/RectF;->top:F

    .line 188
    iget v13, v6, Landroid/graphics/RectF;->right:F

    div-float/2addr v13, v9

    iput v13, v6, Landroid/graphics/RectF;->right:F

    .line 189
    iget v13, v6, Landroid/graphics/RectF;->bottom:F

    div-float/2addr v13, v9

    iput v13, v6, Landroid/graphics/RectF;->bottom:F

    .line 191
    new-instance v1, Landroid/graphics/Canvas;

    move-object/from16 v0, p1

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 192
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 194
    invoke-virtual {v1}, Landroid/graphics/Canvas;->save()I

    .line 195
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getAngle()I

    move-result v13

    int-to-float v13, v13

    .line 196
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v14

    iget v14, v14, Landroid/graphics/PointF;->x:F

    div-float/2addr v14, v9

    .line 197
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v15

    iget v15, v15, Landroid/graphics/PointF;->y:F

    div-float/2addr v15, v9

    .line 195
    invoke-virtual {v1, v13, v14, v15}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 199
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    .line 200
    .local v12, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 201
    .local v3, "height":I
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v13

    float-to-int v8, v13

    .line 202
    .local v8, "resizeWidth":I
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v13

    float-to-int v7, v13

    .line 203
    .local v7, "resizeHeight":I
    int-to-float v13, v8

    int-to-float v14, v12

    div-float v11, v13, v14

    .line 204
    .local v11, "scaleWidth":F
    int-to-float v13, v7

    int-to-float v14, v3

    div-float v10, v13, v14

    .line 206
    .local v10, "scaleHeight":F
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 207
    .local v4, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v4, v11, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 208
    iget v13, v6, Landroid/graphics/RectF;->left:F

    iget v14, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v13, v14}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 210
    const/high16 v13, 0x3f800000    # 1.0f

    cmpl-float v13, v11, v13

    if-eqz v13, :cond_1

    .line 213
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v1, v13, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :goto_0
    invoke-virtual {v1}, Landroid/graphics/Canvas;->restore()V

    .line 225
    .end local v3    # "height":I
    .end local v4    # "matrix":Landroid/graphics/Matrix;
    .end local v7    # "resizeHeight":I
    .end local v8    # "resizeWidth":I
    .end local v10    # "scaleHeight":F
    .end local v11    # "scaleWidth":F
    .end local v12    # "width":I
    :cond_0
    :goto_1
    return-void

    .line 215
    .restart local v3    # "height":I
    .restart local v4    # "matrix":Landroid/graphics/Matrix;
    .restart local v7    # "resizeHeight":I
    .restart local v8    # "resizeWidth":I
    .restart local v10    # "scaleHeight":F
    .restart local v11    # "scaleWidth":F
    .restart local v12    # "width":I
    :catch_0
    move-exception v2

    .line 216
    .local v2, "e":Ljava/lang/Exception;
    goto :goto_1

    .line 221
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    iget v14, v6, Landroid/graphics/RectF;->left:F

    iget v15, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v13, v14, v15, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public applyPreview(Landroid/graphics/Bitmap;)V
    .locals 14
    .param p1, "preview"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v11, 0x1

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getRoi()Landroid/graphics/RectF;

    move-result-object v5

    .line 230
    .local v5, "rectRoi":Landroid/graphics/RectF;
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v11}, Landroid/graphics/Paint;-><init>(I)V

    .line 231
    .local v4, "paint":Landroid/graphics/Paint;
    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 232
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 234
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 236
    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getAngle()I

    move-result v11

    int-to-float v11, v11

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v12

    iget v12, v12, Landroid/graphics/PointF;->x:F

    .line 239
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v13

    iget v13, v13, Landroid/graphics/PointF;->y:F

    .line 237
    invoke-virtual {v0, v11, v12, v13}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 241
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    .line 242
    .local v10, "width":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 243
    .local v2, "height":I
    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v11

    float-to-int v7, v11

    .line 244
    .local v7, "resizeWidth":I
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v11

    float-to-int v6, v11

    .line 245
    .local v6, "resizeHeight":I
    int-to-float v11, v7

    int-to-float v12, v10

    div-float v9, v11, v12

    .line 246
    .local v9, "scaleWidth":F
    int-to-float v11, v6

    int-to-float v12, v2

    div-float v8, v11, v12

    .line 248
    .local v8, "scaleHeight":F
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 249
    .local v3, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v3, v9, v8}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 250
    iget v11, v5, Landroid/graphics/RectF;->left:F

    iget v12, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {v3, v11, v12}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 252
    const/high16 v11, 0x3f800000    # 1.0f

    cmpl-float v11, v9, v11

    if-eqz v11, :cond_1

    .line 255
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    invoke-virtual {v0, v11, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :goto_0
    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 267
    .end local v2    # "height":I
    .end local v3    # "matrix":Landroid/graphics/Matrix;
    .end local v6    # "resizeHeight":I
    .end local v7    # "resizeWidth":I
    .end local v8    # "scaleHeight":F
    .end local v9    # "scaleWidth":F
    .end local v10    # "width":I
    :cond_0
    :goto_1
    return-void

    .line 257
    .restart local v2    # "height":I
    .restart local v3    # "matrix":Landroid/graphics/Matrix;
    .restart local v6    # "resizeHeight":I
    .restart local v7    # "resizeWidth":I
    .restart local v8    # "scaleHeight":F
    .restart local v9    # "scaleWidth":F
    .restart local v10    # "width":I
    :catch_0
    move-exception v1

    .line 258
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_1

    .line 263
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    iget v12, v5, Landroid/graphics/RectF;->left:F

    iget v13, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v11, v12, v13, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/Label;)V
    .locals 1
    .param p1, "label"    # Lcom/sec/android/mimage/photoretouching/Core/Label;

    .prologue
    .line 113
    if-nez p1, :cond_0

    .line 118
    :goto_0
    return-void

    .line 116
    :cond_0
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/Label;->mCurrentResId:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mCurrentResId:I

    .line 117
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/Label;->mZOrder:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mZOrder:I

    goto :goto_0
.end method

.method public destory()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->OrgObjectBmp:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 137
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->destroy()V

    .line 138
    return-void
.end method

.method public getAngle()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mAngle:I

    return v0
.end method

.method public getCurrentResId()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mCurrentResId:I

    return v0
.end method

.method public getDrawBdry()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getDrawCenterPT()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 173
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 174
    .local v0, "ret":Landroid/graphics/PointF;
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingX()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 175
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingY()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 176
    return-object v0
.end method

.method public getOrgDestROI()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mOriginalROI:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getZOrder()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mZOrder:I

    return v0
.end method

.method public setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V
    .locals 0
    .param p1, "roiPoint"    # Landroid/graphics/RectF;
    .param p2, "mOrgCenterPt"    # Landroid/graphics/PointF;
    .param p3, "mOrgDestPt1"    # Landroid/graphics/PointF;
    .param p4, "mOrgDestPt2"    # Landroid/graphics/PointF;
    .param p5, "mOrgDestPt3"    # Landroid/graphics/PointF;
    .param p6, "mOrgDestPt4"    # Landroid/graphics/PointF;
    .param p7, "angle"    # I

    .prologue
    .line 145
    invoke-super/range {p0 .. p7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V

    .line 146
    return-void
.end method

.method public setZOrder(I)V
    .locals 0
    .param p1, "idx"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->mZOrder:I

    .line 142
    return-void
.end method

.method public updateLabelBitmap(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "imgData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "labelBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 120
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 121
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->OrgObjectBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 122
    .local v1, "ObjectWidth":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/Label;->OrgObjectBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 124
    .local v0, "ObjectHeight":I
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    div-int/lit8 v3, v5, 0x2

    .line 125
    .local v3, "x_center":I
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    div-int/lit8 v4, v5, 0x2

    .line 127
    .local v4, "y_center":I
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 128
    .local v2, "roi":Landroid/graphics/Rect;
    div-int/lit8 v5, v1, 0x2

    sub-int v5, v3, v5

    iput v5, v2, Landroid/graphics/Rect;->left:I

    .line 129
    iget v5, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v1

    add-int/lit8 v5, v5, -0x1

    iput v5, v2, Landroid/graphics/Rect;->right:I

    .line 130
    div-int/lit8 v5, v0, 0x2

    sub-int v5, v4, v5

    iput v5, v2, Landroid/graphics/Rect;->top:I

    .line 131
    iget v5, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    iput v5, v2, Landroid/graphics/Rect;->bottom:I

    .line 132
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->setRectRoi(Landroid/graphics/Rect;)V

    .line 133
    return-void
.end method
