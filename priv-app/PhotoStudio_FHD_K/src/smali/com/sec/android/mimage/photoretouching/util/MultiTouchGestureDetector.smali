.class public Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;
.super Ljava/lang/Object;
.source "MultiTouchGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;,
        Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;,
        Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$ScaleListener;,
        Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnLongPressGestureListener;,
        Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;,
        Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;
    }
.end annotation


# static fields
.field private static final CHECK_TIMER:I = 0x1e

.field private static final TAP_TIMER:I = 0x12c

.field private static final TOUCH_TOLERANCE:F = 2.0f


# instance fields
.field private mCancelDoubleTap:Z

.field mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

.field private mCurrMotion:Landroid/view/MotionEvent;

.field private mIsDoubleTapEnd:Z

.field private mIsDoubleTapFirstStart:Z

.field private mIsDoubleTapSecondStart:Z

.field private mIsMultiTouchEnd:Z

.field private mIsMultiTouchStart:Z

.field private mListener:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

.field private mPreMotionEvent:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;",
            ">;"
        }
    .end annotation
.end field

.field mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

.field private mScaleDetector:Landroid/view/ScaleGestureDetector;

.field private mScaleFactor:F

.field private mStartDoubleTapTime:J

.field private mStartDoubleTapTouchDownTime:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapSecondStart:Z

    .line 180
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapFirstStart:Z

    .line 181
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapEnd:Z

    .line 182
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCancelDoubleTap:Z

    .line 192
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    .line 193
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    .line 226
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mListener:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

    .line 227
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsMultiTouchStart:Z

    .line 228
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsMultiTouchEnd:Z

    .line 230
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;-><init>(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    .line 231
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;-><init>(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    .line 233
    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$ScaleListener;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$ScaleListener;-><init>(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$ScaleListener;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 234
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)V
    .locals 0

    .prologue
    .line 455
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->removeMotion()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mListener:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapFirstStart:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapSecondStart:Z

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)Z
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCancelDoubleTap:Z

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)V
    .locals 0

    .prologue
    .line 465
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->loadMotion()V

    return-void
.end method

.method private julery_isqrt(I)I
    .locals 6
    .param p1, "val"    # I

    .prologue
    .line 617
    const/4 v3, 0x0

    .local v3, "g":I
    const v0, 0x8000

    .local v0, "b":I
    const/16 v1, 0xf

    .local v1, "bshft":I
    move v2, v1

    .line 619
    .end local v1    # "bshft":I
    .local v2, "bshft":I
    :goto_0
    shl-int/lit8 v5, v3, 0x1

    add-int/2addr v5, v0

    add-int/lit8 v1, v2, -0x1

    .end local v2    # "bshft":I
    .restart local v1    # "bshft":I
    shl-int v4, v5, v2

    .local v4, "temp":I
    if-lt p1, v4, :cond_0

    .line 620
    add-int/2addr v3, v0

    .line 621
    sub-int/2addr p1, v4

    .line 623
    :cond_0
    shr-int/lit8 v0, v0, 0x1

    .line 618
    if-gtz v0, :cond_1

    .line 624
    return v3

    :cond_1
    move v2, v1

    .end local v1    # "bshft":I
    .restart local v2    # "bshft":I
    goto :goto_0
.end method

.method private loadMotion()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 466
    const/4 v0, 0x0

    .line 467
    .local v0, "moveCount":I
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCancelDoubleTap:Z

    .line 468
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapEnd:Z

    .line 470
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 494
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapSecondStart:Z

    .line 495
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapFirstStart:Z

    .line 496
    iput-wide v8, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mStartDoubleTapTouchDownTime:J

    .line 497
    iput-wide v8, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mStartDoubleTapTime:J

    .line 498
    return-void

    .line 472
    :cond_0
    if-lez v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;

    # getter for: Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->actionType:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->access$5(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;)I

    move-result v1

    if-ne v1, v6, :cond_1

    .line 474
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 478
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;

    # getter for: Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->actionType:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->access$5(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;)I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 479
    add-int/lit8 v0, v0, 0x1

    .line 480
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurrMotion:Landroid/view/MotionEvent;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;

    # getter for: Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->actionType:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->access$5(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 481
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurrMotion:Landroid/view/MotionEvent;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;

    # getter for: Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->x:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->access$0(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;)F

    move-result v3

    .line 482
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;

    # getter for: Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->y:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->access$1(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;)F

    move-result v1

    .line 481
    invoke-virtual {v2, v3, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 484
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mListener:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurrMotion:Landroid/view/MotionEvent;

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;->onSingleTouchEvent(Landroid/view/MotionEvent;)Z

    .line 485
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurrMotion:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_3

    .line 486
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurrMotion:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v5, :cond_4

    .line 487
    :cond_3
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapEnd:Z

    .line 489
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private removeMotion()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 456
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCancelDoubleTap:Z

    .line 457
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapSecondStart:Z

    .line 458
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapFirstStart:Z

    .line 459
    iput-wide v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mStartDoubleTapTouchDownTime:J

    .line 460
    iput-wide v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mStartDoubleTapTime:J

    .line 462
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 463
    return-void
.end method

.method private reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 568
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 569
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 570
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 572
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    if-eqz v0, :cond_1

    .line 573
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 574
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 576
    :cond_1
    return-void
.end method

.method private saveMotion(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 440
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 441
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    .line 444
    :cond_0
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurrMotion:Landroid/view/MotionEvent;

    .line 446
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;-><init>(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)V

    .line 448
    .local v0, "touchInfo":Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->access$2(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;I)V

    .line 449
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->access$3(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;F)V

    .line 450
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->access$4(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;F)V

    .line 452
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 453
    return-void
.end method

.method private set(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1, "curr"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    .line 530
    if-nez p1, :cond_1

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 536
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    if-eqz v2, :cond_2

    .line 537
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v2, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 539
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 541
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    const/high16 v3, -0x40800000    # -1.0f

    iput v3, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->length:F

    .line 542
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    const/high16 v3, -0x40800000    # -1.0f

    iput v3, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->length:F

    .line 543
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mScaleFactor:F

    .line 545
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v1, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 546
    .local v1, "prev":Landroid/view/MotionEvent;
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    .line 549
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    sub-float/2addr v3, v4

    iput v3, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->fingerDiffX:F

    .line 550
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    sub-float/2addr v3, v4

    iput v3, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->fingerDiffY:F

    .line 551
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    sub-float/2addr v3, v4

    iput v3, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->fingerDiffX:F

    .line 552
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    sub-float/2addr v3, v4

    iput v3, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->fingerDiffY:F

    .line 554
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v4, v4, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->fingerDiffX:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->focusX:F

    .line 555
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v4, v4, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->fingerDiffY:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->focusY:F

    .line 557
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v4, v4, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->fingerDiffX:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->focusX:F

    .line 558
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v4, v4, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->fingerDiffY:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->focusY:F
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 560
    .end local v1    # "prev":Landroid/view/MotionEvent;
    :catch_0
    move-exception v0

    .line 562
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private startDoubleTapTimer()V
    .locals 4

    .prologue
    .line 501
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$1;-><init>(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;)V

    .line 526
    .local v0, "handler":Landroid/os/Handler;
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 527
    return-void
.end method


# virtual methods
.method public getCurrentSpan()F
    .locals 5

    .prologue
    .line 591
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v2, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->length:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 592
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v0, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->fingerDiffX:F

    .line 593
    .local v0, "cvx":F
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v1, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->fingerDiffY:F

    .line 594
    .local v1, "cvy":F
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    mul-float v3, v0, v0

    mul-float v4, v1, v1

    add-float/2addr v3, v4

    float-to-int v3, v3

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->julery_isqrt(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->length:F

    .line 596
    .end local v0    # "cvx":F
    .end local v1    # "cvy":F
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v2, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->length:F

    return v2
.end method

.method public getFocusX()F
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->focusX:F

    return v0
.end method

.method public getFocusY()F
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->focusY:F

    return v0
.end method

.method public getPreviousSpan()F
    .locals 5

    .prologue
    .line 600
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v2, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->length:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 601
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v0, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->fingerDiffX:F

    .line 602
    .local v0, "pvx":F
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v1, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->fingerDiffY:F

    .line 603
    .local v1, "pvy":F
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    mul-float v3, v0, v0

    mul-float v4, v1, v1

    add-float/2addr v3, v4

    float-to-int v3, v3

    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->julery_isqrt(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->length:F

    .line 605
    .end local v0    # "pvx":F
    .end local v1    # "pvy":F
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v2, v2, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->length:F

    return v2
.end method

.method public getScaleFactor()F
    .locals 2

    .prologue
    .line 609
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mScaleFactor:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 610
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->getCurrentSpan()F

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->getPreviousSpan()F

    move-result v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mScaleFactor:F

    .line 612
    :cond_0
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mScaleFactor:F

    return v0
.end method

.method public isMultiTouch()Z
    .locals 1

    .prologue
    .line 579
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsMultiTouchStart:Z

    return v0
.end method

.method public onSingleTouchEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mListener:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mListener:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;->onSingleTouchEvent(Landroid/view/MotionEvent;)Z

    .line 239
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 253
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 254
    .local v0, "action":I
    const/4 v5, 0x1

    .line 256
    .local v5, "handled":Z
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapSecondStart:Z

    if-nez v9, :cond_0

    .line 257
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v9, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 258
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "bigheadk, action = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 259
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "bigheadk, ev.getPointerCount() = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 261
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsMultiTouchStart:Z

    if-nez v9, :cond_e

    .line 262
    const/4 v9, 0x5

    if-eq v0, v9, :cond_1

    const/16 v9, 0x105

    if-ne v0, v9, :cond_3

    .line 263
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v9

    const/4 v10, 0x2

    if-lt v9, v10, :cond_3

    .line 264
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->reset()V

    .line 266
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v10

    iput-object v10, v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 268
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->set(Landroid/view/MotionEvent;)V

    .line 270
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsMultiTouchStart:Z

    .line 271
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsMultiTouchEnd:Z

    .line 273
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mListener:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;->onMultiTouchDown()Z

    .line 436
    :cond_2
    :goto_0
    const/4 v9, 0x0

    :goto_1
    return v9

    .line 276
    :cond_3
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsMultiTouchEnd:Z

    if-nez v9, :cond_4

    .line 277
    const/4 v9, 0x1

    if-ne v0, v9, :cond_2

    .line 278
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_2

    .line 279
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsMultiTouchEnd:Z

    goto :goto_0

    .line 280
    :cond_4
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapEnd:Z

    if-nez v9, :cond_b

    .line 281
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->saveMotion(Landroid/view/MotionEvent;)V

    .line 283
    packed-switch v0, :pswitch_data_0

    .line 339
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->loadMotion()V

    goto :goto_0

    .line 285
    :pswitch_0
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapSecondStart:Z

    if-nez v9, :cond_5

    .line 287
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 286
    iput-wide v10, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mStartDoubleTapTouchDownTime:J

    .line 288
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapFirstStart:Z

    if-nez v9, :cond_2

    .line 289
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCancelDoubleTap:Z

    .line 290
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->startDoubleTapTimer()V

    .line 291
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapFirstStart:Z

    goto :goto_0

    .line 294
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 295
    iget-wide v12, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mStartDoubleTapTime:J

    .line 294
    sub-long/2addr v10, v12

    .line 295
    const-wide/16 v12, 0x12c

    cmp-long v9, v10, v12

    if-gez v9, :cond_6

    .line 297
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 296
    iput-wide v10, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mStartDoubleTapTouchDownTime:J

    goto :goto_0

    .line 299
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->loadMotion()V

    goto :goto_0

    .line 304
    :pswitch_1
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapSecondStart:Z

    if-eqz v9, :cond_8

    .line 305
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 306
    iget-wide v12, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mStartDoubleTapTouchDownTime:J

    .line 305
    sub-long/2addr v10, v12

    .line 306
    const-wide/16 v12, 0x12c

    cmp-long v9, v10, v12

    if-gez v9, :cond_7

    .line 307
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapEnd:Z

    .line 308
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mListener:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

    invoke-virtual {v9, p1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    .line 310
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->removeMotion()V

    goto :goto_0

    .line 312
    :cond_7
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->loadMotion()V

    goto :goto_0

    .line 315
    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 316
    iget-wide v12, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mStartDoubleTapTouchDownTime:J

    .line 315
    sub-long/2addr v10, v12

    .line 316
    const-wide/16 v12, 0x12c

    cmp-long v9, v10, v12

    if-gez v9, :cond_9

    .line 318
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 317
    iput-wide v10, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mStartDoubleTapTime:J

    .line 319
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapSecondStart:Z

    .line 320
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapFirstStart:Z

    goto/16 :goto_0

    .line 322
    :cond_9
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapFirstStart:Z

    .line 323
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->loadMotion()V

    goto/16 :goto_0

    .line 328
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;

    # getter for: Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->x:F
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->access$0(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;)F

    move-result v9

    sub-float v9, v10, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x430c0000    # 140.0f

    cmpg-float v9, v9, v10

    if-gez v9, :cond_a

    .line 329
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPreMotionEvent:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;

    # getter for: Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->y:F
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;->access$1(Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$TouchInfoClass;)F

    move-result v9

    sub-float v9, v10, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x430c0000    # 140.0f

    cmpg-float v9, v9, v10

    if-ltz v9, :cond_2

    .line 335
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->loadMotion()V

    goto/16 :goto_0

    .line 343
    :cond_b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    const/4 v10, 0x3

    if-eq v9, v10, :cond_c

    .line 344
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_d

    .line 345
    :cond_c
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsDoubleTapEnd:Z

    .line 347
    :cond_d
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mListener:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

    invoke-virtual {v9, p1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;->onSingleTouchEvent(Landroid/view/MotionEvent;)Z

    goto/16 :goto_0

    .line 351
    :cond_e
    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 379
    :sswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->set(Landroid/view/MotionEvent;)V

    .line 381
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v9, v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/MotionEvent;->getX(I)F

    move-result v9

    float-to-int v9, v9

    .line 382
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    float-to-int v10, v10

    .line 381
    sub-int v1, v9, v10

    .line 383
    .local v1, "deltaX0":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v9, v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/MotionEvent;->getY(I)F

    move-result v9

    float-to-int v9, v9

    .line 384
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v10

    float-to-int v10, v10

    .line 383
    sub-int v3, v9, v10

    .line 386
    .local v3, "deltaY0":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v9, v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v9}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v9

    const/4 v10, 0x2

    if-ge v9, v10, :cond_10

    .line 387
    const/4 v9, 0x1

    goto/16 :goto_1

    .line 355
    .end local v1    # "deltaX0":I
    .end local v3    # "deltaY0":I
    :sswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->set(Landroid/view/MotionEvent;)V

    .line 358
    const v9, 0xff00

    and-int/2addr v9, v0

    shr-int/lit8 v9, v9, 0x8

    if-nez v9, :cond_f

    const/4 v6, 0x1

    .line 361
    .local v6, "id":I
    :goto_2
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    iput v10, v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->focusX:F

    .line 362
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v10

    iput v10, v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->focusY:F

    .line 364
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mListener:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;->onMultiTouchUp()Z

    .line 365
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsMultiTouchStart:Z

    .line 367
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->reset()V

    goto/16 :goto_0

    .line 359
    .end local v6    # "id":I
    :cond_f
    const/4 v6, 0x0

    goto :goto_2

    .line 371
    :sswitch_2
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mListener:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;->onMultiTouchUp()Z

    .line 372
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mIsMultiTouchStart:Z

    .line 374
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->reset()V

    goto/16 :goto_0

    .line 389
    .restart local v1    # "deltaX0":I
    .restart local v3    # "deltaY0":I
    :cond_10
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v9, v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/view/MotionEvent;->getX(I)F

    move-result v9

    float-to-int v9, v9

    .line 390
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    float-to-int v10, v10

    .line 389
    sub-int v2, v9, v10

    .line 391
    .local v2, "deltaX1":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v9, v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/view/MotionEvent;->getY(I)F

    move-result v9

    float-to-int v9, v9

    .line 392
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v10, v10, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v10

    float-to-int v10, v10

    .line 391
    sub-int v4, v9, v10

    .line 394
    .local v4, "deltaY1":I
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x40000000    # 2.0f

    cmpg-float v9, v9, v10

    if-gez v9, :cond_11

    .line 395
    const/4 v1, 0x0

    .line 396
    :cond_11
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x40000000    # 2.0f

    cmpg-float v9, v9, v10

    if-gez v9, :cond_12

    .line 397
    const/4 v3, 0x0

    .line 398
    :cond_12
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x40000000    # 2.0f

    cmpg-float v9, v9, v10

    if-gez v9, :cond_13

    .line 399
    const/4 v2, 0x0

    .line 400
    :cond_13
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v9

    int-to-float v9, v9

    const/high16 v10, 0x40000000    # 2.0f

    cmpg-float v9, v9, v10

    if-gez v9, :cond_14

    .line 401
    const/4 v4, 0x0

    .line 404
    :cond_14
    mul-int v9, v1, v2

    if-gtz v9, :cond_15

    mul-int v9, v3, v4

    if-lez v9, :cond_16

    .line 405
    :cond_15
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v9, v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->focusX:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v10, v10, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->focusX:F

    sub-float v7, v9, v10

    .line 406
    .local v7, "scrollX":F
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v9, v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->focusY:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mCurr:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget v10, v10, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->focusY:F

    sub-float v8, v9, v10

    .line 410
    .local v8, "scrollY":F
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mListener:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;

    invoke-virtual {v9, v7, v8}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;->onMultiTouchScroll(FF)Z

    .line 413
    .end local v7    # "scrollX":F
    .end local v8    # "scrollY":F
    :cond_16
    mul-int v9, v1, v2

    if-lez v9, :cond_17

    .line 430
    :cond_17
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    iget-object v9, v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v9}, Landroid/view/MotionEvent;->recycle()V

    .line 431
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->mPrev:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v10

    iput-object v10, v9, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    goto/16 :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 351
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_2
        0x6 -> :sswitch_1
        0x106 -> :sswitch_1
    .end sparse-switch
.end method
