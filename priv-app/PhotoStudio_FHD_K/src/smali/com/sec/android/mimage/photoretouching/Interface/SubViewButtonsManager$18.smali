.class Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;
.super Ljava/lang/Object;
.source "SubViewButtonsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->changeLayoutSize(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

.field private final synthetic val$lp:Landroid/view/ViewGroup$LayoutParams;

.field private final synthetic val$tempWidth:I


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;Landroid/view/ViewGroup$LayoutParams;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->val$lp:Landroid/view/ViewGroup$LayoutParams;

    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->val$tempWidth:I

    .line 2603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 2606
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x31000000

    if-eq v1, v2, :cond_0

    .line 2607
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v1

    const/high16 v2, 0x1a000000

    if-eq v1, v2, :cond_0

    .line 2608
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->val$lp:Landroid/view/ViewGroup$LayoutParams;

    if-nez v1, :cond_1

    .line 2617
    :cond_0
    :goto_0
    return-void

    .line 2610
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->val$lp:Landroid/view/ViewGroup$LayoutParams;

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->val$tempWidth:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2611
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mHorizontalScrollView:Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->val$lp:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Gui/QHorizontalScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2612
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getCurrentModeButtonId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonLeft(I)I

    move-result v0

    .line 2613
    .local v0, "left":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->adjustSubmenuBarPosition(II)V

    .line 2614
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/widget/LinearLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2615
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager$18;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->mBottomLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/SubViewButtonsManager;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->invalidate()V

    goto :goto_0
.end method
