.class public Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;
.super Lcom/sec/android/mimage/photoretouching/Core/RectController;
.source "ImageSticker.java"


# instance fields
.field private OrgObjectBmp:Landroid/graphics/Bitmap;

.field private mBoundaryType:I

.field private mContext:Landroid/content/Context;

.field private mCurrentResId:I

.field private mFreeRect:Z

.field private mZOrder:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;-><init>()V

    .line 17
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mCurrentResId:I

    .line 18
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mZOrder:I

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mContext:Landroid/content/Context;

    .line 20
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mBoundaryType:I

    .line 21
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mFreeRect:Z

    .line 123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Landroid/graphics/Bitmap;IIZLandroid/graphics/Rect;)V
    .locals 25
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imgData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p4, "zOrder"    # I
    .param p5, "boundaryType"    # I
    .param p6, "freeRect"    # Z
    .param p7, "targetRoi"    # Landroid/graphics/Rect;

    .prologue
    .line 25
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p5

    move/from16 v4, p6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/RectController;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;IZ)V

    .line 17
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mCurrentResId:I

    .line 18
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mZOrder:I

    .line 19
    const/16 v23, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mContext:Landroid/content/Context;

    .line 20
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mBoundaryType:I

    .line 21
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mFreeRect:Z

    .line 26
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mContext:Landroid/content/Context;

    .line 27
    move/from16 v0, p5

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mBoundaryType:I

    .line 28
    move/from16 v0, p6

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mFreeRect:Z

    .line 30
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mZOrder:I

    .line 32
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v23

    div-int/lit8 v21, v23, 0x2

    .line 33
    .local v21, "x_center":I
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v23

    div-int/lit8 v22, v23, 0x2

    .line 35
    .local v22, "y_center":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v23, v0

    if-eqz v23, :cond_2

    .line 36
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v19

    .line 37
    .local v19, "viewTransform":Landroid/graphics/Matrix;
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 38
    .local v10, "invertMatrix":Landroid/graphics/Matrix;
    const/16 v23, 0x9

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v17, v0

    .line 40
    .local v17, "val":[F
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 41
    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 44
    if-eqz p3, :cond_2

    .line 46
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    .line 47
    .local v9, "bitmapWidth":I
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 49
    .local v7, "bitmapHeight":I
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getGLRendererMaxSize()I

    move-result v15

    .line 51
    .local v15, "rendererMaxSize":I
    const/4 v8, 0x0

    .line 52
    .local v8, "bitmapSize":[I
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v23

    move/from16 v0, v23

    if-gt v0, v15, :cond_0

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v23

    move/from16 v0, v23

    if-le v0, v15, :cond_1

    :cond_0
    if-eqz v15, :cond_1

    .line 54
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v15}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->limitBitmapRendererSize(Landroid/graphics/Bitmap;I)[I

    move-result-object v8

    .line 56
    const/16 v23, 0x0

    aget v9, v8, v23

    .line 57
    const/16 v23, 0x1

    aget v7, v8, v23

    .line 62
    :cond_1
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v23

    move/from16 v0, v23

    if-ne v0, v9, :cond_5

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v23

    move/from16 v0, v23

    if-ne v0, v7, :cond_5

    .line 64
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 78
    .end local v7    # "bitmapHeight":I
    .end local v8    # "bitmapSize":[I
    .end local v9    # "bitmapWidth":I
    .end local v10    # "invertMatrix":Landroid/graphics/Matrix;
    .end local v15    # "rendererMaxSize":I
    .end local v17    # "val":[F
    .end local v19    # "viewTransform":Landroid/graphics/Matrix;
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 79
    .local v6, "ObjectWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 81
    .local v5, "ObjectHeight":I
    if-eqz p7, :cond_3

    .line 83
    invoke-virtual/range {p7 .. p7}, Landroid/graphics/Rect;->width()I

    move-result v6

    .line 84
    invoke-virtual/range {p7 .. p7}, Landroid/graphics/Rect;->height()I

    move-result v5

    .line 87
    :cond_3
    new-instance v16, Landroid/graphics/Rect;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Rect;-><init>()V

    .line 88
    .local v16, "roi":Landroid/graphics/Rect;
    div-int/lit8 v23, v6, 0x2

    sub-int v23, v21, v23

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 89
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v23, v0

    add-int v23, v23, v6

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 90
    div-int/lit8 v23, v5, 0x2

    sub-int v23, v22, v23

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 91
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v23, v0

    add-int v23, v23, v5

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v23

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 92
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->setRectRoi(Landroid/graphics/Rect;)V

    .line 94
    const v12, 0x3f4ccccd    # 0.8f

    .line 95
    .local v12, "maxScale":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v20

    .line 96
    .local v20, "viewTransformMatrix":Landroid/graphics/Matrix;
    const/16 v23, 0x9

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v17, v0

    .line 97
    .restart local v17    # "val":[F
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 98
    const/16 v23, 0x0

    aget v23, v17, v23

    const/16 v24, 0x4

    aget v24, v17, v24

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->max(FF)F

    move-result v18

    .line 102
    .local v18, "viewTransScale":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v24

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_6

    const/4 v11, 0x1

    .line 103
    .local v11, "isImageLandscape":Z
    :goto_1
    const/high16 v14, 0x3f800000    # 1.0f

    .line 104
    .local v14, "orientationScale":F
    if-eqz v11, :cond_7

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v14, v23, v24

    .line 109
    :goto_2
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v23

    move/from16 v0, v23

    if-ne v11, v0, :cond_4

    .line 110
    const/high16 v14, 0x3f800000    # 1.0f

    .line 112
    :cond_4
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v23

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v24

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->max(II)I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v12

    mul-float v24, v18, v14

    mul-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v13, v0

    .line 113
    .local v13, "maxSize":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->setMaximumSize(I)V

    .line 114
    const v23, 0x3dcccccd    # 0.1f

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->setCustomMinSize(F)V

    .line 119
    return-void

    .line 67
    .end local v5    # "ObjectHeight":I
    .end local v6    # "ObjectWidth":I
    .end local v11    # "isImageLandscape":Z
    .end local v12    # "maxScale":F
    .end local v13    # "maxSize":I
    .end local v14    # "orientationScale":F
    .end local v16    # "roi":Landroid/graphics/Rect;
    .end local v18    # "viewTransScale":F
    .end local v20    # "viewTransformMatrix":Landroid/graphics/Matrix;
    .restart local v7    # "bitmapHeight":I
    .restart local v8    # "bitmapSize":[I
    .restart local v9    # "bitmapWidth":I
    .restart local v10    # "invertMatrix":Landroid/graphics/Matrix;
    .restart local v15    # "rendererMaxSize":I
    .restart local v19    # "viewTransform":Landroid/graphics/Matrix;
    :cond_5
    const/16 v23, 0x1

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-static {v0, v9, v7, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 68
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Bitmap;->recycle()V

    .line 69
    const/16 p3, 0x0

    goto/16 :goto_0

    .line 102
    .end local v7    # "bitmapHeight":I
    .end local v8    # "bitmapSize":[I
    .end local v9    # "bitmapWidth":I
    .end local v10    # "invertMatrix":Landroid/graphics/Matrix;
    .end local v15    # "rendererMaxSize":I
    .end local v19    # "viewTransform":Landroid/graphics/Matrix;
    .restart local v5    # "ObjectHeight":I
    .restart local v6    # "ObjectWidth":I
    .restart local v12    # "maxScale":F
    .restart local v16    # "roi":Landroid/graphics/Rect;
    .restart local v18    # "viewTransScale":F
    .restart local v20    # "viewTransformMatrix":Landroid/graphics/Matrix;
    :cond_6
    const/4 v11, 0x0

    goto :goto_1

    .line 107
    .restart local v11    # "isImageLandscape":Z
    .restart local v14    # "orientationScale":F
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v14, v23, v24

    goto :goto_2
.end method

.method private limitBitmapRendererSize(Landroid/graphics/Bitmap;I)[I
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "rendererMaxSize"    # I

    .prologue
    .line 181
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 182
    :cond_0
    const/4 v1, 0x0

    .line 206
    :goto_0
    return-object v1

    .line 184
    :cond_1
    const/4 v4, 0x2

    new-array v1, v4, [I

    .line 186
    .local v1, "bitmapSize":[I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 187
    .local v2, "bitmapWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 189
    .local v0, "bitmapHeight":I
    if-le v2, p2, :cond_2

    .line 191
    int-to-float v4, v2

    int-to-float v5, v0

    div-float v3, v4, v5

    .line 192
    .local v3, "ratio":F
    move v2, p2

    .line 193
    int-to-float v4, v2

    div-float/2addr v4, v3

    float-to-int v0, v4

    .line 196
    .end local v3    # "ratio":F
    :cond_2
    if-le v0, p2, :cond_3

    .line 198
    int-to-float v4, v2

    int-to-float v5, v0

    div-float v3, v4, v5

    .line 199
    .restart local v3    # "ratio":F
    move v0, p2

    .line 200
    int-to-float v4, v0

    mul-float/2addr v4, v3

    float-to-int v2, v4

    .line 203
    .end local v3    # "ratio":F
    :cond_3
    const/4 v4, 0x0

    aput v2, v1, v4

    .line 204
    const/4 v4, 0x1

    aput v0, v1, v4

    goto :goto_0
.end method


# virtual methods
.method public EndMoveObject()V
    .locals 0

    .prologue
    .line 311
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->EndMoveObject()V

    .line 312
    return-void
.end method

.method public GetObjectBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public InitMoveObject(FF)I
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 302
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->InitMoveObject(FF)I

    move-result v0

    return v0
.end method

.method public StartMoveObject(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 307
    invoke-super {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->StartMoveObject(FF)V

    .line 308
    return-void
.end method

.method public applyOriginal(Landroid/graphics/Bitmap;)V
    .locals 16
    .param p1, "originalBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 212
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v13}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalToPreviewRatio()F

    move-result v9

    .line 213
    .local v9, "scale":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getRoi()Landroid/graphics/RectF;

    move-result-object v6

    .line 215
    .local v6, "rectRoi":Landroid/graphics/RectF;
    new-instance v5, Landroid/graphics/Paint;

    const/4 v13, 0x1

    invoke-direct {v5, v13}, Landroid/graphics/Paint;-><init>(I)V

    .line 216
    .local v5, "paint":Landroid/graphics/Paint;
    const/4 v13, 0x1

    invoke-virtual {v5, v13}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 218
    iget v13, v6, Landroid/graphics/RectF;->left:F

    div-float/2addr v13, v9

    iput v13, v6, Landroid/graphics/RectF;->left:F

    .line 219
    iget v13, v6, Landroid/graphics/RectF;->top:F

    div-float/2addr v13, v9

    iput v13, v6, Landroid/graphics/RectF;->top:F

    .line 220
    iget v13, v6, Landroid/graphics/RectF;->right:F

    div-float/2addr v13, v9

    iput v13, v6, Landroid/graphics/RectF;->right:F

    .line 221
    iget v13, v6, Landroid/graphics/RectF;->bottom:F

    div-float/2addr v13, v9

    iput v13, v6, Landroid/graphics/RectF;->bottom:F

    .line 223
    new-instance v1, Landroid/graphics/Canvas;

    move-object/from16 v0, p1

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 224
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 226
    invoke-virtual {v1}, Landroid/graphics/Canvas;->save()I

    .line 227
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getAngle()I

    move-result v13

    int-to-float v13, v13

    .line 228
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v14

    iget v14, v14, Landroid/graphics/PointF;->x:F

    div-float/2addr v14, v9

    .line 229
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v15

    iget v15, v15, Landroid/graphics/PointF;->y:F

    div-float/2addr v15, v9

    .line 227
    invoke-virtual {v1, v13, v14, v15}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 231
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    .line 232
    .local v12, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 233
    .local v3, "height":I
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v13

    float-to-int v8, v13

    .line 234
    .local v8, "resizeWidth":I
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v13

    float-to-int v7, v13

    .line 235
    .local v7, "resizeHeight":I
    int-to-float v13, v8

    int-to-float v14, v12

    div-float v11, v13, v14

    .line 236
    .local v11, "scaleWidth":F
    int-to-float v13, v7

    int-to-float v14, v3

    div-float v10, v13, v14

    .line 238
    .local v10, "scaleHeight":F
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 239
    .local v4, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v4, v11, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 240
    iget v13, v6, Landroid/graphics/RectF;->left:F

    iget v14, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v13, v14}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 242
    const/high16 v13, 0x3f800000    # 1.0f

    cmpl-float v13, v11, v13

    if-eqz v13, :cond_1

    .line 245
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v1, v13, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    :goto_0
    invoke-virtual {v1}, Landroid/graphics/Canvas;->restore()V

    .line 257
    .end local v3    # "height":I
    .end local v4    # "matrix":Landroid/graphics/Matrix;
    .end local v7    # "resizeHeight":I
    .end local v8    # "resizeWidth":I
    .end local v10    # "scaleHeight":F
    .end local v11    # "scaleWidth":F
    .end local v12    # "width":I
    :cond_0
    :goto_1
    return-void

    .line 247
    .restart local v3    # "height":I
    .restart local v4    # "matrix":Landroid/graphics/Matrix;
    .restart local v7    # "resizeHeight":I
    .restart local v8    # "resizeWidth":I
    .restart local v10    # "scaleHeight":F
    .restart local v11    # "scaleWidth":F
    .restart local v12    # "width":I
    :catch_0
    move-exception v2

    .line 248
    .local v2, "e":Ljava/lang/Exception;
    goto :goto_1

    .line 253
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    iget v14, v6, Landroid/graphics/RectF;->left:F

    iget v15, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v13, v14, v15, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public applyPreview(Landroid/graphics/Bitmap;)V
    .locals 14
    .param p1, "preview"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v11, 0x1

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getRoi()Landroid/graphics/RectF;

    move-result-object v5

    .line 262
    .local v5, "rectRoi":Landroid/graphics/RectF;
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v11}, Landroid/graphics/Paint;-><init>(I)V

    .line 263
    .local v4, "paint":Landroid/graphics/Paint;
    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 264
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 265
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 267
    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getAngle()I

    move-result v11

    int-to-float v11, v11

    .line 269
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v12

    iget v12, v12, Landroid/graphics/PointF;->x:F

    .line 270
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v13

    iget v13, v13, Landroid/graphics/PointF;->y:F

    .line 268
    invoke-virtual {v0, v11, v12, v13}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 272
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    .line 273
    .local v10, "width":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 274
    .local v2, "height":I
    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v11

    float-to-int v7, v11

    .line 275
    .local v7, "resizeWidth":I
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v11

    float-to-int v6, v11

    .line 276
    .local v6, "resizeHeight":I
    int-to-float v11, v7

    int-to-float v12, v10

    div-float v9, v11, v12

    .line 277
    .local v9, "scaleWidth":F
    int-to-float v11, v6

    int-to-float v12, v2

    div-float v8, v11, v12

    .line 279
    .local v8, "scaleHeight":F
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 280
    .local v3, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v3, v9, v8}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 281
    iget v11, v5, Landroid/graphics/RectF;->left:F

    iget v12, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {v3, v11, v12}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 283
    const/high16 v11, 0x3f800000    # 1.0f

    cmpl-float v11, v9, v11

    if-eqz v11, :cond_1

    .line 286
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    invoke-virtual {v0, v11, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    :goto_0
    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 298
    .end local v2    # "height":I
    .end local v3    # "matrix":Landroid/graphics/Matrix;
    .end local v6    # "resizeHeight":I
    .end local v7    # "resizeWidth":I
    .end local v8    # "scaleHeight":F
    .end local v9    # "scaleWidth":F
    .end local v10    # "width":I
    :cond_0
    :goto_1
    return-void

    .line 288
    .restart local v2    # "height":I
    .restart local v3    # "matrix":Landroid/graphics/Matrix;
    .restart local v6    # "resizeHeight":I
    .restart local v7    # "resizeWidth":I
    .restart local v8    # "scaleHeight":F
    .restart local v9    # "scaleWidth":F
    .restart local v10    # "width":I
    :catch_0
    move-exception v1

    .line 289
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_1

    .line 294
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    iget v12, v5, Landroid/graphics/RectF;->left:F

    iget v13, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v11, v12, v13, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;)V
    .locals 2
    .param p1, "sticker"    # Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;

    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW sticker="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 127
    if-nez p1, :cond_0

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_0
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mCurrentResId:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mCurrentResId:I

    .line 131
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mZOrder:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mZOrder:I

    goto :goto_0
.end method

.method public destory()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->OrgObjectBmp:Landroid/graphics/Bitmap;

    .line 136
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->destroy()V

    .line 137
    return-void
.end method

.method public getAngle()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mAngle:I

    return v0
.end method

.method public getCurrentResId()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mCurrentResId:I

    return v0
.end method

.method public getDrawBdry()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getDrawCenterPT()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 172
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 173
    .local v0, "ret":Landroid/graphics/PointF;
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingX()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 174
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getCenterPT()Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingY()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 175
    return-object v0
.end method

.method public getOrgDestROI()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mOriginalROI:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getZOrder()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mZOrder:I

    return v0
.end method

.method public setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V
    .locals 0
    .param p1, "roiPoint"    # Landroid/graphics/RectF;
    .param p2, "mOrgCenterPt"    # Landroid/graphics/PointF;
    .param p3, "mOrgDestPt1"    # Landroid/graphics/PointF;
    .param p4, "mOrgDestPt2"    # Landroid/graphics/PointF;
    .param p5, "mOrgDestPt3"    # Landroid/graphics/PointF;
    .param p6, "mOrgDestPt4"    # Landroid/graphics/PointF;
    .param p7, "angle"    # I

    .prologue
    .line 144
    invoke-super/range {p0 .. p7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V

    .line 145
    return-void
.end method

.method public setZOrder(I)V
    .locals 0
    .param p1, "idx"    # I

    .prologue
    .line 140
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/ImageSticker;->mZOrder:I

    .line 141
    return-void
.end method
