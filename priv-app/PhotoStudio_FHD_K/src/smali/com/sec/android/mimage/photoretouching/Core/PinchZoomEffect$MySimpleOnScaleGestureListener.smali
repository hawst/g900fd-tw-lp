.class Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MySimpleOnScaleGestureListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "PinchZoomEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MySimpleOnScaleGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;)V
    .locals 0

    .prologue
    .line 751
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MySimpleOnScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MySimpleOnScaleGestureListener;)V
    .locals 0

    .prologue
    .line 751
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MySimpleOnScaleGestureListener;-><init>(Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 6
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v5, 0x1

    .line 758
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MySimpleOnScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 759
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$MySimpleOnScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-static {v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->access$1(Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;Z)V

    .line 760
    return v5
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 766
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 0
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 772
    return-void
.end method
