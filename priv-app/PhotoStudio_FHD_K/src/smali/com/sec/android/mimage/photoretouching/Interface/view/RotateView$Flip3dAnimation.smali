.class public Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;
.super Landroid/view/animation/Animation;
.source "RotateView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Flip3dAnimation"
.end annotation


# instance fields
.field private mAnimType:I

.field private mCamera:Landroid/graphics/Camera;

.field private final mCenterX:F

.field private final mCenterY:F

.field private final mFromDegreesX:F

.field private final mFromDegreesY:F

.field private final mFromDegreesZ:F

.field private final mFromScale:F

.field private mIsAnimate:Z

.field private final mToDegreesX:F

.field private final mToDegreesY:F

.field private final mToDegreesZ:F

.field private final mToScale:F

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;FFFFFFFFFFI)V
    .locals 1
    .param p2, "fromDegreesX"    # F
    .param p3, "toDegreesX"    # F
    .param p4, "fromDegreesY"    # F
    .param p5, "toDegreesY"    # F
    .param p6, "fromDegreesZ"    # F
    .param p7, "toDegreesZ"    # F
    .param p8, "fromScale"    # F
    .param p9, "toScale"    # F
    .param p10, "centerX"    # F
    .param p11, "centerY"    # F
    .param p12, "animType"    # I

    .prologue
    const/4 v0, 0x0

    .line 1106
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    .line 1102
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1096
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mAnimType:I

    .line 1098
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mIsAnimate:Z

    .line 1107
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mFromDegreesX:F

    .line 1108
    iput p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mFromDegreesY:F

    .line 1109
    iput p6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mFromDegreesZ:F

    .line 1110
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mToDegreesX:F

    .line 1111
    iput p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mToDegreesY:F

    .line 1112
    iput p7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mToDegreesZ:F

    .line 1113
    iput p8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mFromScale:F

    .line 1114
    iput p9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mToScale:F

    .line 1115
    iput p10, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mCenterX:F

    .line 1116
    iput p11, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mCenterY:F

    .line 1117
    iput p12, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mAnimType:I

    .line 1118
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;Z)V
    .locals 0

    .prologue
    .line 1098
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mIsAnimate:Z

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)I
    .locals 1

    .prologue
    .line 1096
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mAnimType:I

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)F
    .locals 1

    .prologue
    .line 1091
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mToScale:F

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    .locals 1

    .prologue
    .line 1081
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    return-object v0
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 8
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 1136
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mFromDegreesX:F

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mToDegreesX:F

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mFromDegreesX:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, p1

    add-float v0, v5, v6

    .line 1137
    .local v0, "degreesX":F
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mFromDegreesY:F

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mToDegreesY:F

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mFromDegreesY:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, p1

    add-float v1, v5, v6

    .line 1138
    .local v1, "degreesY":F
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mFromDegreesZ:F

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mToDegreesZ:F

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mFromDegreesZ:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, p1

    add-float v2, v5, v6

    .line 1139
    .local v2, "degreesZ":F
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mFromScale:F

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mToScale:F

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mFromScale:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, p1

    add-float v4, v5, v6

    .line 1141
    .local v4, "scale":F
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    .line 1143
    .local v3, "m":Landroid/graphics/Matrix;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mCamera:Landroid/graphics/Camera;

    invoke-virtual {v5}, Landroid/graphics/Camera;->save()V

    .line 1145
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mCamera:Landroid/graphics/Camera;

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Camera;->rotate(FFF)V

    .line 1147
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mCamera:Landroid/graphics/Camera;

    invoke-virtual {v5, v3}, Landroid/graphics/Camera;->getMatrix(Landroid/graphics/Matrix;)V

    .line 1148
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mCamera:Landroid/graphics/Camera;

    invoke-virtual {v5}, Landroid/graphics/Camera;->restore()V

    .line 1150
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mCenterX:F

    neg-float v5, v5

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mCenterY:F

    neg-float v6, v6

    invoke-virtual {v3, v5, v6}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 1151
    invoke-virtual {v3, v4, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1152
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mCenterX:F

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mCenterY:F

    invoke-virtual {v3, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1153
    return-void
.end method

.method public getIsAnimate()Z
    .locals 1

    .prologue
    .line 1121
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mIsAnimate:Z

    return v0
.end method

.method public initialize(IIII)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "parentWidth"    # I
    .param p4, "parentHeight"    # I

    .prologue
    .line 1129
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/animation/Animation;->initialize(IIII)V

    .line 1130
    new-instance v0, Landroid/graphics/Camera;

    invoke-direct {v0}, Landroid/graphics/Camera;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mCamera:Landroid/graphics/Camera;

    .line 1131
    return-void
.end method

.method public releaseAnimation()V
    .locals 1

    .prologue
    .line 1125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mIsAnimate:Z

    .line 1126
    return-void
.end method

.method public setAnimationListenerCallback()V
    .locals 1

    .prologue
    .line 1157
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1215
    return-void
.end method
