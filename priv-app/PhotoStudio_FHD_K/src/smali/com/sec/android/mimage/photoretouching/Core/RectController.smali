.class public Lcom/sec/android/mimage/photoretouching/Core/RectController;
.super Ljava/lang/Object;
.source "RectController.java"


# static fields
.field public static final BOTTOM:I = 0x5

.field public static final LEFT:I = 0x2

.field public static final LEFT_BOTTOM:I = 0x8

.field public static final LEFT_TOP:I = 0x6

.field public static final MAX_WIDTH_HEIGHT:I = 0xc8

.field public static final MIN_WIDTH_HEIGHT:I = 0x11

.field public static final MOVE:I = 0x1

.field public static final NONE_TOUCH_TYPE:I = 0x0

.field public static final RECT_CENTER_BOUNDARY:I = 0x2

.field public static final RECT_ROI_BOUNDARY:I = 0x1

.field public static final RIGHT:I = 0x4

.field public static final RIGHT_BOTTOM:I = 0x7

.field public static final RIGHT_TOP:I = 0x9

.field public static final TOP:I = 0x3


# instance fields
.field protected limit:I

.field protected mAngle:I

.field private mBitmapPaint:Landroid/graphics/Paint;

.field private mBitmapResource:Landroid/graphics/Bitmap;

.field private mBitmapRotateResource:Landroid/graphics/Bitmap;

.field private mBoundaryType:I

.field private mCenterPT:Landroid/graphics/PointF;

.field private mCheckBoundary:Z

.field private mCustomMaxSize:F

.field private mCustomMinSize:F

.field protected mDestPt1:Landroid/graphics/PointF;

.field protected mDestPt2:Landroid/graphics/PointF;

.field protected mDestPt3:Landroid/graphics/PointF;

.field protected mDestPt4:Landroid/graphics/PointF;

.field private mDrawLinePaint:Landroid/graphics/Paint;

.field private mDrawLinePaint1:Landroid/graphics/Paint;

.field private mDrawROI:Landroid/graphics/RectF;

.field private mEndPT:Landroid/graphics/PointF;

.field private mFixedHeight:I

.field private mFixedWidth:I

.field private mFreeRect:Z

.field private mGreyPaint:Landroid/graphics/Paint;

.field protected mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

.field private mInitROI:Landroid/graphics/RectF;

.field private mOriginalCenterPT:Landroid/graphics/PointF;

.field private mOriginalDestPt1:Landroid/graphics/PointF;

.field private mOriginalDestPt2:Landroid/graphics/PointF;

.field private mOriginalDestPt3:Landroid/graphics/PointF;

.field private mOriginalDestPt4:Landroid/graphics/PointF;

.field protected mOriginalROI:Landroid/graphics/RectF;

.field private mPrevX:F

.field private mPrevY:F

.field protected mPreviewHeight:I

.field protected mPreviewWidth:I

.field private mPreviousDrawRoi:Landroid/graphics/RectF;

.field protected mROI:Landroid/graphics/RectF;

.field private mRectPaint:Landroid/graphics/Paint;

.field private mRightTopRotate:Z

.field private mStartPT:Landroid/graphics/PointF;

.field protected mTouchType:I

.field protected minValueForClipboard:I

.field private mtouchScale:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666
    const/16 v0, 0x96

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->limit:I

    .line 2135
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    .line 2157
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedWidth:I

    .line 2158
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedHeight:I

    .line 2159
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    .line 2160
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    .line 2161
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFreeRect:Z

    .line 2162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRightTopRotate:Z

    .line 2163
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCheckBoundary:Z

    .line 2164
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    .line 2165
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMaxSize:F

    .line 2166
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mtouchScale:F

    .line 2167
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviousDrawRoi:Landroid/graphics/RectF;

    .line 2169
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 2170
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 2171
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRectPaint:Landroid/graphics/Paint;

    .line 2172
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    .line 2173
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mGreyPaint:Landroid/graphics/Paint;

    .line 2175
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .line 2176
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    .line 2177
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mInitROI:Landroid/graphics/RectF;

    .line 2178
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalROI:Landroid/graphics/RectF;

    .line 2179
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    .line 2184
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mStartPT:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mEndPT:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    .line 2185
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    .line 2186
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    .line 2187
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    .line 2188
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    .line 2189
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapRotateResource:Landroid/graphics/Bitmap;

    .line 2191
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->minValueForClipboard:I

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;IZ)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imgInfo"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;
    .param p3, "boundaryType"    # I
    .param p4, "freeRect"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666
    const/16 v0, 0x96

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->limit:I

    .line 2135
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    .line 2157
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedWidth:I

    .line 2158
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedHeight:I

    .line 2159
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    .line 2160
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    .line 2161
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFreeRect:Z

    .line 2162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRightTopRotate:Z

    .line 2163
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCheckBoundary:Z

    .line 2164
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    .line 2165
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMaxSize:F

    .line 2166
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mtouchScale:F

    .line 2167
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviousDrawRoi:Landroid/graphics/RectF;

    .line 2169
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 2170
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 2171
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRectPaint:Landroid/graphics/Paint;

    .line 2172
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    .line 2173
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mGreyPaint:Landroid/graphics/Paint;

    .line 2175
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .line 2176
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    .line 2177
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mInitROI:Landroid/graphics/RectF;

    .line 2178
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalROI:Landroid/graphics/RectF;

    .line 2179
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    .line 2184
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mStartPT:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mEndPT:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    .line 2185
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    .line 2186
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    .line 2187
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    .line 2188
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    .line 2189
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapRotateResource:Landroid/graphics/Bitmap;

    .line 2191
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->minValueForClipboard:I

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->initResource(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;)V

    .line 27
    iput-boolean p4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFreeRect:Z

    .line 28
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    .line 29
    return-void
.end method

.method private checkBoundary(FFFF)Z
    .locals 34
    .param p1, "left"    # F
    .param p2, "right"    # F
    .param p3, "top"    # F
    .param p4, "bottom"    # F

    .prologue
    .line 1368
    cmpl-float v27, p1, p2

    if-gtz v27, :cond_0

    cmpl-float v27, p3, p4

    if-lez v27, :cond_1

    .line 1369
    :cond_0
    const/16 v21, 0x0

    .line 1499
    :goto_0
    return v21

    .line 1370
    :cond_1
    const/16 v21, 0x0

    .line 1371
    .local v21, "ret":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v4, v0, Landroid/graphics/PointF;->x:F

    .line 1372
    .local v4, "CenterPTx":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v5, v0, Landroid/graphics/PointF;->y:F

    .line 1375
    .local v5, "CenterPTy":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move/from16 v0, p1

    move/from16 v1, p3

    move/from16 v2, v27

    invoke-static {v0, v1, v2, v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v18

    .line 1376
    .local v18, "pt":Landroid/graphics/PointF;
    new-instance v6, Landroid/graphics/PointF;

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    invoke-direct {v6, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1377
    .local v6, "checkPt1":Landroid/graphics/PointF;
    iget v14, v6, Landroid/graphics/PointF;->x:F

    .line 1378
    .local v14, "minLeft":F
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    .line 1379
    .local v16, "minTop":F
    iget v12, v6, Landroid/graphics/PointF;->x:F

    .line 1380
    .local v12, "maxRight":F
    iget v11, v6, Landroid/graphics/PointF;->y:F

    .line 1381
    .local v11, "maxBottom":F
    const/16 v18, 0x0

    .line 1383
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, v27

    invoke-static {v0, v1, v2, v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v18

    .line 1384
    new-instance v7, Landroid/graphics/PointF;

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    invoke-direct {v7, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1385
    .local v7, "checkPt2":Landroid/graphics/PointF;
    const/16 v18, 0x0

    .line 1386
    iget v0, v7, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    cmpg-float v27, v14, v27

    if-gez v27, :cond_5

    .line 1387
    :goto_1
    iget v0, v7, Landroid/graphics/PointF;->y:F

    move/from16 v27, v0

    cmpg-float v27, v16, v27

    if-gez v27, :cond_6

    .line 1388
    :goto_2
    iget v0, v7, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    cmpl-float v27, v12, v27

    if-lez v27, :cond_7

    .line 1389
    :goto_3
    iget v0, v7, Landroid/graphics/PointF;->y:F

    move/from16 v27, v0

    cmpl-float v27, v11, v27

    if-lez v27, :cond_8

    .line 1391
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move/from16 v0, p2

    move/from16 v1, p4

    move/from16 v2, v27

    invoke-static {v0, v1, v2, v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v18

    .line 1392
    new-instance v8, Landroid/graphics/PointF;

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    invoke-direct {v8, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1393
    .local v8, "checkPt3":Landroid/graphics/PointF;
    const/16 v18, 0x0

    .line 1394
    iget v0, v8, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    cmpg-float v27, v14, v27

    if-gez v27, :cond_9

    .line 1395
    :goto_5
    iget v0, v8, Landroid/graphics/PointF;->y:F

    move/from16 v27, v0

    cmpg-float v27, v16, v27

    if-gez v27, :cond_a

    .line 1396
    :goto_6
    iget v0, v8, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    cmpl-float v27, v12, v27

    if-lez v27, :cond_b

    .line 1397
    :goto_7
    iget v0, v8, Landroid/graphics/PointF;->y:F

    move/from16 v27, v0

    cmpl-float v27, v11, v27

    if-lez v27, :cond_c

    .line 1399
    :goto_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move/from16 v0, p1

    move/from16 v1, p4

    move/from16 v2, v27

    invoke-static {v0, v1, v2, v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v18

    .line 1400
    new-instance v9, Landroid/graphics/PointF;

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    invoke-direct {v9, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1401
    .local v9, "checkPt4":Landroid/graphics/PointF;
    const/16 v18, 0x0

    .line 1403
    iget v0, v9, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    cmpg-float v27, v14, v27

    if-gez v27, :cond_d

    .line 1404
    :goto_9
    iget v0, v9, Landroid/graphics/PointF;->y:F

    move/from16 v27, v0

    cmpg-float v27, v16, v27

    if-gez v27, :cond_e

    .line 1405
    :goto_a
    iget v0, v9, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    cmpl-float v27, v12, v27

    if-lez v27, :cond_f

    .line 1406
    :goto_b
    iget v0, v9, Landroid/graphics/PointF;->y:F

    move/from16 v27, v0

    cmpl-float v27, v11, v27

    if-lez v27, :cond_10

    .line 1409
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v26

    .line 1410
    .local v26, "viewTransMatrix":Landroid/graphics/Matrix;
    const/16 v27, 0x9

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v24, v0

    .line 1411
    .local v24, "val":[F
    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1413
    const/16 v27, 0x0

    aget v22, v24, v27

    .line 1414
    .local v22, "scaleX":F
    const/16 v27, 0x4

    aget v23, v24, v27

    .line 1415
    .local v23, "scaleY":F
    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->max(FF)F

    move-result v25

    .line 1419
    .local v25, "viewScale":F
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v27

    const/high16 v28, 0x17000000

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_2

    .line 1420
    const/high16 v25, 0x3f800000    # 1.0f

    .line 1424
    :cond_2
    const/16 v20, 0x0

    .line 1425
    .local v20, "rectWidth":I
    const/16 v19, 0x0

    .line 1426
    .local v19, "rectHeight":I
    iget v0, v7, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v28, v0

    sub-float v27, v27, v28

    move/from16 v0, v27

    float-to-double v0, v0

    move-wide/from16 v28, v0

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    invoke-static/range {v28 .. v31}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v28

    iget v0, v7, Landroid/graphics/PointF;->y:F

    move/from16 v27, v0

    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v30, v0

    sub-float v27, v27, v30

    move/from16 v0, v27

    float-to-double v0, v0

    move-wide/from16 v30, v0

    const-wide/high16 v32, 0x4000000000000000L    # 2.0

    invoke-static/range {v30 .. v33}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v30

    add-double v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-int v0, v0

    move/from16 v20, v0

    .line 1427
    iget v0, v8, Landroid/graphics/PointF;->x:F

    move/from16 v27, v0

    iget v0, v7, Landroid/graphics/PointF;->x:F

    move/from16 v28, v0

    sub-float v27, v27, v28

    move/from16 v0, v27

    float-to-double v0, v0

    move-wide/from16 v28, v0

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    invoke-static/range {v28 .. v31}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v28

    iget v0, v8, Landroid/graphics/PointF;->y:F

    move/from16 v27, v0

    iget v0, v7, Landroid/graphics/PointF;->y:F

    move/from16 v30, v0

    sub-float v27, v27, v30

    move/from16 v0, v27

    float-to-double v0, v0

    move-wide/from16 v30, v0

    const-wide/high16 v32, 0x4000000000000000L    # 2.0

    invoke-static/range {v30 .. v33}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v30

    add-double v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-int v0, v0

    move/from16 v19, v0

    .line 1428
    move/from16 v0, v20

    move/from16 v1, v19

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v15

    .line 1429
    .local v15, "minSize":I
    move/from16 v0, v20

    move/from16 v1, v19

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 1433
    .local v13, "maxSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v28, v0

    invoke-interface/range {v28 .. v28}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v28

    move/from16 v0, v27

    move/from16 v1, v28

    if-lt v0, v1, :cond_11

    const/4 v10, 0x1

    .line 1434
    .local v10, "isImageLandscape":Z
    :goto_d
    const/high16 v17, 0x3f800000    # 1.0f

    .line 1435
    .local v17, "orientationScale":F
    if-eqz v10, :cond_12

    .line 1436
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v28, v0

    invoke-interface/range {v28 .. v28}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    div-float v17, v27, v28

    .line 1440
    :goto_e
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v27

    move/from16 v0, v27

    if-eq v10, v0, :cond_3

    .line 1441
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v27

    const/high16 v28, 0x17000000

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_4

    .line 1442
    :cond_3
    const/high16 v17, 0x3f800000    # 1.0f

    .line 1444
    :cond_4
    mul-float v27, v25, v17

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mtouchScale:F

    .line 1447
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "touchScale : "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mtouchScale:F

    move/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1449
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v27

    const/high16 v28, 0x31300000

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_14

    .line 1455
    int-to-float v0, v15

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    move/from16 v28, v0

    cmpl-float v27, v27, v28

    if-ltz v27, :cond_13

    int-to-float v0, v13

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMaxSize:F

    move/from16 v28, v0

    cmpg-float v27, v27, v28

    if-gtz v27, :cond_13

    .line 1456
    const/16 v21, 0x1

    .line 1495
    :goto_f
    const/4 v6, 0x0

    .line 1496
    const/4 v7, 0x0

    .line 1497
    const/4 v8, 0x0

    .line 1498
    const/4 v9, 0x0

    .line 1499
    goto/16 :goto_0

    .line 1386
    .end local v8    # "checkPt3":Landroid/graphics/PointF;
    .end local v9    # "checkPt4":Landroid/graphics/PointF;
    .end local v10    # "isImageLandscape":Z
    .end local v13    # "maxSize":I
    .end local v15    # "minSize":I
    .end local v17    # "orientationScale":F
    .end local v19    # "rectHeight":I
    .end local v20    # "rectWidth":I
    .end local v22    # "scaleX":F
    .end local v23    # "scaleY":F
    .end local v24    # "val":[F
    .end local v25    # "viewScale":F
    .end local v26    # "viewTransMatrix":Landroid/graphics/Matrix;
    :cond_5
    iget v14, v7, Landroid/graphics/PointF;->x:F

    goto/16 :goto_1

    .line 1387
    :cond_6
    iget v0, v7, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    goto/16 :goto_2

    .line 1388
    :cond_7
    iget v12, v7, Landroid/graphics/PointF;->x:F

    goto/16 :goto_3

    .line 1389
    :cond_8
    iget v11, v7, Landroid/graphics/PointF;->y:F

    goto/16 :goto_4

    .line 1394
    .restart local v8    # "checkPt3":Landroid/graphics/PointF;
    :cond_9
    iget v14, v8, Landroid/graphics/PointF;->x:F

    goto/16 :goto_5

    .line 1395
    :cond_a
    iget v0, v8, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    goto/16 :goto_6

    .line 1396
    :cond_b
    iget v12, v8, Landroid/graphics/PointF;->x:F

    goto/16 :goto_7

    .line 1397
    :cond_c
    iget v11, v8, Landroid/graphics/PointF;->y:F

    goto/16 :goto_8

    .line 1403
    .restart local v9    # "checkPt4":Landroid/graphics/PointF;
    :cond_d
    iget v14, v9, Landroid/graphics/PointF;->x:F

    goto/16 :goto_9

    .line 1404
    :cond_e
    iget v0, v9, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    goto/16 :goto_a

    .line 1405
    :cond_f
    iget v12, v9, Landroid/graphics/PointF;->x:F

    goto/16 :goto_b

    .line 1406
    :cond_10
    iget v11, v9, Landroid/graphics/PointF;->y:F

    goto/16 :goto_c

    .line 1433
    .restart local v13    # "maxSize":I
    .restart local v15    # "minSize":I
    .restart local v19    # "rectHeight":I
    .restart local v20    # "rectWidth":I
    .restart local v22    # "scaleX":F
    .restart local v23    # "scaleY":F
    .restart local v24    # "val":[F
    .restart local v25    # "viewScale":F
    .restart local v26    # "viewTransMatrix":Landroid/graphics/Matrix;
    :cond_11
    const/4 v10, 0x0

    goto/16 :goto_d

    .line 1438
    .restart local v10    # "isImageLandscape":Z
    .restart local v17    # "orientationScale":F
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    move-object/from16 v28, v0

    invoke-interface/range {v28 .. v28}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewWidth()I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    div-float v17, v27, v28

    goto/16 :goto_e

    .line 1458
    :cond_13
    const/16 v21, 0x0

    .line 1459
    goto :goto_f

    .line 1460
    :cond_14
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v27

    const/high16 v28, 0x31100000

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_15

    .line 1461
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v27

    const/high16 v28, 0x31600000

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_17

    .line 1462
    :cond_15
    int-to-float v0, v15

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mtouchScale:F

    move/from16 v29, v0

    div-float v28, v28, v29

    cmpl-float v27, v27, v28

    if-ltz v27, :cond_16

    int-to-float v0, v13

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMaxSize:F

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mtouchScale:F

    move/from16 v29, v0

    div-float v28, v28, v29

    cmpg-float v27, v27, v28

    if-gtz v27, :cond_16

    .line 1463
    const/16 v21, 0x1

    goto/16 :goto_f

    .line 1465
    :cond_16
    const/16 v21, 0x0

    .line 1466
    goto/16 :goto_f

    .line 1467
    :cond_17
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v27

    const/high16 v28, 0x17000000

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1a

    .line 1469
    int-to-float v0, v15

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    move/from16 v28, v0

    cmpl-float v27, v27, v28

    if-ltz v27, :cond_19

    int-to-float v0, v13

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMaxSize:F

    move/from16 v28, v0

    cmpg-float v27, v27, v28

    if-gtz v27, :cond_19

    .line 1471
    sub-float v27, v12, v14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->minValueForClipboard:I

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    cmpl-float v27, v27, v28

    if-ltz v27, :cond_18

    sub-float v27, v11, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->minValueForClipboard:I

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    cmpl-float v27, v27, v28

    if-ltz v27, :cond_18

    .line 1472
    const/16 v21, 0x1

    goto/16 :goto_f

    .line 1474
    :cond_18
    const/16 v21, 0x0

    .line 1475
    goto/16 :goto_f

    .line 1477
    :cond_19
    const/16 v21, 0x0

    .line 1478
    goto/16 :goto_f

    .line 1481
    :cond_1a
    const/16 v27, 0x0

    cmpl-float v27, v14, v27

    if-ltz v27, :cond_1c

    const/16 v27, 0x0

    cmpl-float v27, v16, v27

    if-ltz v27, :cond_1c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    cmpg-float v27, v12, v27

    if-gez v27, :cond_1c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    cmpg-float v27, v11, v27

    if-gez v27, :cond_1c

    .line 1486
    sub-float v27, v12, v14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mtouchScale:F

    move/from16 v29, v0

    div-float v28, v28, v29

    cmpl-float v27, v27, v28

    if-ltz v27, :cond_1b

    sub-float v27, v11, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mtouchScale:F

    move/from16 v29, v0

    div-float v28, v28, v29

    cmpl-float v27, v27, v28

    if-ltz v27, :cond_1b

    .line 1487
    const/16 v21, 0x1

    goto/16 :goto_f

    .line 1489
    :cond_1b
    const/16 v21, 0x0

    .line 1490
    goto/16 :goto_f

    .line 1492
    :cond_1c
    const/16 v21, 0x0

    goto/16 :goto_f
.end method

.method private checkPinchZoomBoundary(IIII)V
    .locals 22
    .param p1, "roiLeft"    # I
    .param p2, "roiTop"    # I
    .param p3, "roiRight"    # I
    .param p4, "roiBottom"    # I

    .prologue
    .line 1975
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v15, v1, Landroid/graphics/PointF;->x:F

    .line 1976
    .local v15, "CenterPTx":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v0, v1, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    .line 1979
    .local v16, "CenterPTy":F
    move/from16 v0, p1

    int-to-float v1, v0

    move/from16 v0, p2

    int-to-float v8, v0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v9, v9

    move/from16 v0, v16

    invoke-static {v1, v8, v9, v15, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v21

    .line 1980
    .local v21, "pt":Landroid/graphics/PointF;
    new-instance v4, Landroid/graphics/PointF;

    move-object/from16 v0, v21

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v4, v1, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1981
    .local v4, "checkPt1":Landroid/graphics/PointF;
    const/16 v21, 0x0

    .line 1982
    iget v0, v4, Landroid/graphics/PointF;->x:F

    move/from16 v19, v0

    .line 1983
    .local v19, "minLeft":F
    iget v0, v4, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    .line 1984
    .local v20, "minTop":F
    iget v0, v4, Landroid/graphics/PointF;->x:F

    move/from16 v18, v0

    .line 1985
    .local v18, "maxRight":F
    iget v0, v4, Landroid/graphics/PointF;->y:F

    move/from16 v17, v0

    .line 1987
    .local v17, "maxBottom":F
    move/from16 v0, p3

    int-to-float v1, v0

    move/from16 v0, p2

    int-to-float v8, v0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v9, v9

    move/from16 v0, v16

    invoke-static {v1, v8, v9, v15, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v21

    .line 1988
    new-instance v5, Landroid/graphics/PointF;

    move-object/from16 v0, v21

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v5, v1, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1989
    .local v5, "checkPt2":Landroid/graphics/PointF;
    const/16 v21, 0x0

    .line 1990
    iget v1, v5, Landroid/graphics/PointF;->x:F

    cmpg-float v1, v19, v1

    if-gez v1, :cond_6

    .line 1991
    :goto_0
    iget v1, v5, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v20, v1

    if-gez v1, :cond_7

    .line 1992
    :goto_1
    iget v1, v5, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v18, v1

    if-lez v1, :cond_8

    .line 1993
    :goto_2
    iget v1, v5, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v17, v1

    if-lez v1, :cond_9

    .line 1995
    :goto_3
    move/from16 v0, p3

    int-to-float v1, v0

    move/from16 v0, p4

    int-to-float v8, v0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v9, v9

    move/from16 v0, v16

    invoke-static {v1, v8, v9, v15, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v21

    .line 1996
    new-instance v6, Landroid/graphics/PointF;

    move-object/from16 v0, v21

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v6, v1, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1997
    .local v6, "checkPt3":Landroid/graphics/PointF;
    const/16 v21, 0x0

    .line 1998
    iget v1, v6, Landroid/graphics/PointF;->x:F

    cmpg-float v1, v19, v1

    if-gez v1, :cond_a

    .line 1999
    :goto_4
    iget v1, v6, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v20, v1

    if-gez v1, :cond_b

    .line 2000
    :goto_5
    iget v1, v6, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v18, v1

    if-lez v1, :cond_c

    .line 2001
    :goto_6
    iget v1, v6, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v17, v1

    if-lez v1, :cond_d

    .line 2003
    :goto_7
    move/from16 v0, p1

    int-to-float v1, v0

    move/from16 v0, p4

    int-to-float v8, v0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v9, v9

    move/from16 v0, v16

    invoke-static {v1, v8, v9, v15, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v21

    .line 2004
    new-instance v7, Landroid/graphics/PointF;

    move-object/from16 v0, v21

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v7, v1, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2005
    .local v7, "checkPt4":Landroid/graphics/PointF;
    const/16 v21, 0x0

    .line 2006
    iget v1, v7, Landroid/graphics/PointF;->x:F

    cmpg-float v1, v19, v1

    if-gez v1, :cond_e

    .line 2007
    :goto_8
    iget v1, v7, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v20, v1

    if-gez v1, :cond_f

    .line 2008
    :goto_9
    iget v1, v7, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v18, v1

    if-lez v1, :cond_10

    .line 2009
    :goto_a
    iget v1, v7, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v17, v1

    if-lez v1, :cond_11

    .line 2011
    :goto_b
    const/4 v1, 0x0

    cmpl-float v1, v19, v1

    if-ltz v1, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, v20, v1

    if-ltz v1, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    int-to-float v1, v1

    cmpg-float v1, v18, v1

    if-gez v1, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    int-to-float v1, v1

    cmpg-float v1, v17, v1

    if-ltz v1, :cond_5

    .line 2013
    :cond_0
    const/4 v2, 0x0

    .line 2014
    .local v2, "dx":F
    const/4 v3, 0x0

    .line 2015
    .local v3, "dy":F
    const/4 v1, 0x0

    cmpg-float v1, v19, v1

    if-gez v1, :cond_1

    .line 2016
    const/4 v1, 0x0

    sub-float v2, v1, v19

    .line 2017
    :cond_1
    const/4 v1, 0x0

    cmpg-float v1, v20, v1

    if-gez v1, :cond_2

    .line 2018
    const/4 v1, 0x0

    sub-float v3, v1, v20

    .line 2019
    :cond_2
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    int-to-float v1, v1

    cmpl-float v1, v18, v1

    if-ltz v1, :cond_3

    .line 2020
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    int-to-float v1, v1

    sub-float v1, v1, v18

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float v2, v1, v8

    .line 2021
    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    int-to-float v1, v1

    cmpl-float v1, v17, v1

    if-ltz v1, :cond_4

    .line 2022
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    int-to-float v1, v1

    sub-float v1, v1, v17

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float v3, v1, v8

    :cond_4
    move-object/from16 v1, p0

    .line 2024
    invoke-direct/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->moveDestCropRect(FFLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 2026
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v8, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v8, v2

    iput v8, v1, Landroid/graphics/RectF;->left:F

    .line 2027
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v8, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v8, v3

    iput v8, v1, Landroid/graphics/RectF;->top:F

    .line 2028
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v8, v1, Landroid/graphics/RectF;->right:F

    add-float/2addr v8, v2

    iput v8, v1, Landroid/graphics/RectF;->right:F

    .line 2029
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v8, v1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v8, v3

    iput v8, v1, Landroid/graphics/RectF;->bottom:F

    .line 2073
    .end local v2    # "dx":F
    .end local v3    # "dy":F
    :cond_5
    :goto_c
    const/4 v4, 0x0

    .line 2074
    const/4 v5, 0x0

    .line 2075
    const/4 v6, 0x0

    .line 2076
    const/4 v7, 0x0

    .line 2077
    return-void

    .line 1990
    .end local v6    # "checkPt3":Landroid/graphics/PointF;
    .end local v7    # "checkPt4":Landroid/graphics/PointF;
    :cond_6
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v19, v0

    goto/16 :goto_0

    .line 1991
    :cond_7
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    goto/16 :goto_1

    .line 1992
    :cond_8
    iget v0, v5, Landroid/graphics/PointF;->x:F

    move/from16 v18, v0

    goto/16 :goto_2

    .line 1993
    :cond_9
    iget v0, v5, Landroid/graphics/PointF;->y:F

    move/from16 v17, v0

    goto/16 :goto_3

    .line 1998
    .restart local v6    # "checkPt3":Landroid/graphics/PointF;
    :cond_a
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v19, v0

    goto/16 :goto_4

    .line 1999
    :cond_b
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    goto/16 :goto_5

    .line 2000
    :cond_c
    iget v0, v6, Landroid/graphics/PointF;->x:F

    move/from16 v18, v0

    goto/16 :goto_6

    .line 2001
    :cond_d
    iget v0, v6, Landroid/graphics/PointF;->y:F

    move/from16 v17, v0

    goto/16 :goto_7

    .line 2006
    .restart local v7    # "checkPt4":Landroid/graphics/PointF;
    :cond_e
    iget v0, v7, Landroid/graphics/PointF;->x:F

    move/from16 v19, v0

    goto/16 :goto_8

    .line 2007
    :cond_f
    iget v0, v7, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    goto/16 :goto_9

    .line 2008
    :cond_10
    iget v0, v7, Landroid/graphics/PointF;->x:F

    move/from16 v18, v0

    goto/16 :goto_a

    .line 2009
    :cond_11
    iget v0, v7, Landroid/graphics/PointF;->y:F

    move/from16 v17, v0

    goto/16 :goto_b

    .line 2033
    .restart local v2    # "dx":F
    .restart local v3    # "dy":F
    :cond_12
    const/16 v21, 0x0

    .line 2034
    new-instance v11, Landroid/graphics/PointF;

    const/4 v1, 0x0

    const/4 v8, 0x0

    invoke-direct {v11, v1, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2035
    .local v11, "leftTopBdry":Landroid/graphics/PointF;
    new-instance v12, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    int-to-float v1, v1

    const/4 v8, 0x0

    invoke-direct {v12, v1, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2036
    .local v12, "rightTopBdry":Landroid/graphics/PointF;
    new-instance v13, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    int-to-float v8, v8

    invoke-direct {v13, v1, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2037
    .local v13, "rightBottomBdry":Landroid/graphics/PointF;
    new-instance v14, Landroid/graphics/PointF;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    int-to-float v8, v8

    invoke-direct {v14, v1, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2039
    .local v14, "leftBottomBdry":Landroid/graphics/PointF;
    iget v9, v4, Landroid/graphics/PointF;->x:F

    iget v10, v4, Landroid/graphics/PointF;->y:F

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->clipPointInRect(FFLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v21

    .line 2040
    if-eqz v21, :cond_13

    .line 2042
    move-object/from16 v0, v21

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    neg-int v9, v9

    int-to-float v9, v9

    move/from16 v0, v16

    invoke-static {v1, v8, v9, v15, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v21

    .line 2043
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->x:F

    iput v8, v1, Landroid/graphics/RectF;->left:F

    .line 2044
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->y:F

    iput v8, v1, Landroid/graphics/RectF;->top:F

    .line 2045
    const/16 v21, 0x0

    .line 2047
    :cond_13
    iget v9, v5, Landroid/graphics/PointF;->x:F

    iget v10, v5, Landroid/graphics/PointF;->y:F

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->clipPointInRect(FFLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v21

    .line 2048
    if-eqz v21, :cond_14

    .line 2050
    move-object/from16 v0, v21

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    neg-int v9, v9

    int-to-float v9, v9

    move/from16 v0, v16

    invoke-static {v1, v8, v9, v15, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v21

    .line 2051
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->x:F

    iput v8, v1, Landroid/graphics/RectF;->right:F

    .line 2052
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->y:F

    iput v8, v1, Landroid/graphics/RectF;->top:F

    .line 2053
    const/16 v21, 0x0

    .line 2055
    :cond_14
    iget v9, v6, Landroid/graphics/PointF;->x:F

    iget v10, v6, Landroid/graphics/PointF;->y:F

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->clipPointInRect(FFLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v21

    .line 2056
    if-eqz v21, :cond_15

    .line 2058
    move-object/from16 v0, v21

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    neg-int v9, v9

    int-to-float v9, v9

    move/from16 v0, v16

    invoke-static {v1, v8, v9, v15, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v21

    .line 2059
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->x:F

    iput v8, v1, Landroid/graphics/RectF;->right:F

    .line 2060
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->y:F

    iput v8, v1, Landroid/graphics/RectF;->bottom:F

    .line 2061
    const/16 v21, 0x0

    .line 2063
    :cond_15
    iget v9, v7, Landroid/graphics/PointF;->x:F

    iget v10, v7, Landroid/graphics/PointF;->y:F

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->clipPointInRect(FFLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v21

    .line 2064
    if-eqz v21, :cond_5

    .line 2066
    move-object/from16 v0, v21

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    neg-int v9, v9

    int-to-float v9, v9

    move/from16 v0, v16

    invoke-static {v1, v8, v9, v15, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v21

    .line 2067
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->x:F

    iput v8, v1, Landroid/graphics/RectF;->left:F

    .line 2068
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v21

    iget v8, v0, Landroid/graphics/PointF;->y:F

    iput v8, v1, Landroid/graphics/RectF;->bottom:F

    .line 2069
    const/16 v21, 0x0

    goto/16 :goto_c
.end method

.method private clipPointInRect(FFLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "leftTop"    # Landroid/graphics/PointF;
    .param p4, "rightTop"    # Landroid/graphics/PointF;
    .param p5, "rightBottom"    # Landroid/graphics/PointF;
    .param p6, "leftBottom"    # Landroid/graphics/PointF;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 1505
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1506
    .local v1, "ret":Landroid/graphics/PointF;
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1508
    .local v0, "currentPoint":Landroid/graphics/PointF;
    invoke-static {p3, p4, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->ccw(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v2

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 1511
    iget v2, p3, Landroid/graphics/PointF;->y:F

    iput v2, v1, Landroid/graphics/PointF;->y:F

    .line 1513
    :cond_0
    invoke-static {p4, p5, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->ccw(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v2

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_1

    .line 1516
    iget v2, p4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v4

    iput v2, v1, Landroid/graphics/PointF;->x:F

    .line 1518
    :cond_1
    invoke-static {p5, p6, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->ccw(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v2

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_2

    .line 1521
    iget v2, p5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v4

    iput v2, v1, Landroid/graphics/PointF;->y:F

    .line 1523
    :cond_2
    invoke-static {p6, p3, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->ccw(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v2

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_3

    .line 1526
    iget v2, p6, Landroid/graphics/PointF;->x:F

    iput v2, v1, Landroid/graphics/PointF;->x:F

    .line 1528
    :cond_3
    const/4 v0, 0x0

    .line 1529
    return-object v1
.end method

.method private getClipROI(Landroid/graphics/RectF;IFFIIII)Landroid/graphics/RectF;
    .locals 7
    .param p1, "roi"    # Landroid/graphics/RectF;
    .param p2, "type"    # I
    .param p3, "dx"    # F
    .param p4, "dy"    # F
    .param p5, "boundaryLeft"    # I
    .param p6, "boundaryTop"    # I
    .param p7, "boundaryRight"    # I
    .param p8, "boundaryBottom"    # I

    .prologue
    .line 1844
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 1845
    .local v2, "ret":Landroid/graphics/RectF;
    const/4 v1, 0x0

    .local v1, "left":F
    const/4 v4, 0x0

    .local v4, "top":F
    const/4 v3, 0x0

    .local v3, "right":F
    const/4 v0, 0x0

    .line 1846
    .local v0, "bottom":F
    iget v1, p1, Landroid/graphics/RectF;->left:F

    .line 1847
    iget v4, p1, Landroid/graphics/RectF;->top:F

    .line 1848
    iget v3, p1, Landroid/graphics/RectF;->right:F

    .line 1849
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    .line 1850
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 1852
    packed-switch p2, :pswitch_data_0

    .line 1936
    :goto_0
    invoke-virtual {v2, v1, v4, v3, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1937
    return-object v2

    .line 1855
    :pswitch_0
    iget v5, p1, Landroid/graphics/RectF;->left:F

    add-float v1, v5, p3

    .line 1856
    goto :goto_0

    .line 1858
    :pswitch_1
    iget v5, p1, Landroid/graphics/RectF;->top:F

    add-float v4, v5, p4

    .line 1859
    goto :goto_0

    .line 1861
    :pswitch_2
    iget v5, p1, Landroid/graphics/RectF;->right:F

    add-float v3, v5, p3

    .line 1862
    goto :goto_0

    .line 1864
    :pswitch_3
    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    add-float v0, v5, p4

    .line 1865
    goto :goto_0

    .line 1867
    :pswitch_4
    iget v5, p1, Landroid/graphics/RectF;->left:F

    add-float v1, v5, p3

    .line 1868
    iget v5, p1, Landroid/graphics/RectF;->top:F

    add-float v4, v5, p4

    .line 1869
    goto :goto_0

    .line 1871
    :pswitch_5
    iget v5, p1, Landroid/graphics/RectF;->right:F

    add-float v3, v5, p3

    .line 1872
    iget v5, p1, Landroid/graphics/RectF;->top:F

    add-float v4, v5, p4

    .line 1873
    goto :goto_0

    .line 1875
    :pswitch_6
    iget v5, p1, Landroid/graphics/RectF;->right:F

    add-float v3, v5, p3

    .line 1876
    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    add-float v0, v5, p4

    .line 1877
    goto :goto_0

    .line 1879
    :pswitch_7
    iget v5, p1, Landroid/graphics/RectF;->left:F

    add-float v1, v5, p3

    .line 1880
    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    add-float v0, v5, p4

    goto :goto_0

    .line 1886
    :cond_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 1929
    :pswitch_8
    iget v1, p1, Landroid/graphics/RectF;->left:F

    .line 1930
    iget v3, p1, Landroid/graphics/RectF;->right:F

    .line 1931
    iget v4, p1, Landroid/graphics/RectF;->top:F

    .line 1932
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0

    .line 1889
    :pswitch_9
    iget v5, p1, Landroid/graphics/RectF;->left:F

    add-float v1, v5, p3

    .line 1890
    iget v5, p1, Landroid/graphics/RectF;->right:F

    sub-float v3, v5, p3

    .line 1891
    goto :goto_0

    .line 1893
    :pswitch_a
    iget v5, p1, Landroid/graphics/RectF;->top:F

    add-float v4, v5, p4

    .line 1894
    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    sub-float v0, v5, p4

    .line 1895
    goto :goto_0

    .line 1897
    :pswitch_b
    iget v5, p1, Landroid/graphics/RectF;->left:F

    sub-float v1, v5, p3

    .line 1898
    iget v5, p1, Landroid/graphics/RectF;->right:F

    add-float v3, v5, p3

    .line 1899
    goto :goto_0

    .line 1901
    :pswitch_c
    iget v5, p1, Landroid/graphics/RectF;->top:F

    sub-float v4, v5, p4

    .line 1902
    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    add-float v0, v5, p4

    .line 1903
    goto :goto_0

    .line 1905
    :pswitch_d
    iget v5, p1, Landroid/graphics/RectF;->left:F

    add-float v1, v5, p3

    .line 1906
    iget v5, p1, Landroid/graphics/RectF;->right:F

    sub-float v3, v5, p3

    .line 1907
    iget v5, p1, Landroid/graphics/RectF;->top:F

    add-float v4, v5, p4

    .line 1908
    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    sub-float v0, v5, p4

    .line 1909
    goto :goto_0

    .line 1911
    :pswitch_e
    iget v5, p1, Landroid/graphics/RectF;->left:F

    sub-float v1, v5, p3

    .line 1912
    iget v5, p1, Landroid/graphics/RectF;->right:F

    add-float v3, v5, p3

    .line 1913
    iget v5, p1, Landroid/graphics/RectF;->top:F

    add-float v4, v5, p4

    .line 1914
    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    sub-float v0, v5, p4

    .line 1915
    goto/16 :goto_0

    .line 1917
    :pswitch_f
    iget v5, p1, Landroid/graphics/RectF;->left:F

    sub-float v1, v5, p3

    .line 1918
    iget v5, p1, Landroid/graphics/RectF;->right:F

    add-float v3, v5, p3

    .line 1919
    iget v5, p1, Landroid/graphics/RectF;->top:F

    sub-float v4, v5, p4

    .line 1920
    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    add-float v0, v5, p4

    .line 1921
    goto/16 :goto_0

    .line 1923
    :pswitch_10
    iget v5, p1, Landroid/graphics/RectF;->left:F

    add-float v1, v5, p3

    .line 1924
    iget v5, p1, Landroid/graphics/RectF;->right:F

    sub-float v3, v5, p3

    .line 1925
    iget v5, p1, Landroid/graphics/RectF;->top:F

    sub-float v4, v5, p4

    .line 1926
    iget v5, p1, Landroid/graphics/RectF;->bottom:F

    add-float v0, v5, p4

    .line 1927
    goto/16 :goto_0

    .line 1852
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_5
    .end packed-switch

    .line 1886
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_f
        :pswitch_10
        :pswitch_e
    .end packed-switch
.end method

.method private getDiffFromCropRatio(FFI)[F
    .locals 8
    .param p1, "dx"    # F
    .param p2, "dy"    # F
    .param p3, "type"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1672
    const/4 v3, 0x2

    new-array v2, v3, [F

    .line 1673
    .local v2, "ret":[F
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1674
    .local v0, "absDx":F
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 1676
    .local v1, "absDy":F
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sj, RC - getDiffFromCropRatio() - dx : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / dy : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / absDx : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / absDy : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1770
    cmpl-float v3, v0, v1

    if-lez v3, :cond_2

    .line 1772
    const-string v3, "dx"

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1773
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    mul-float/2addr v3, v0

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float v1, v3, v4

    .line 1774
    packed-switch p3, :pswitch_data_0

    .line 1839
    :goto_0
    return-object v2

    .line 1778
    :pswitch_0
    cmpg-float v3, p1, v7

    if-gez v3, :cond_0

    .line 1780
    aput p1, v2, v5

    .line 1781
    aput v1, v2, v6

    goto :goto_0

    .line 1785
    :cond_0
    aput p1, v2, v5

    .line 1786
    neg-float v3, v1

    aput v3, v2, v6

    goto :goto_0

    .line 1791
    :pswitch_1
    cmpg-float v3, p1, v7

    if-gez v3, :cond_1

    .line 1793
    aput p1, v2, v5

    .line 1794
    neg-float v3, v1

    aput v3, v2, v6

    goto :goto_0

    .line 1798
    :cond_1
    aput p1, v2, v5

    .line 1799
    aput v1, v2, v6

    goto :goto_0

    .line 1806
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dy:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1807
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    mul-float/2addr v3, v1

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    div-float v0, v3, v4

    .line 1808
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "absDx:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",roi w,h:["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 1809
    packed-switch p3, :pswitch_data_1

    goto :goto_0

    .line 1826
    :pswitch_2
    cmpg-float v3, p2, v7

    if-gez v3, :cond_4

    .line 1828
    neg-float v3, v0

    aput v3, v2, v5

    .line 1829
    aput p2, v2, v6

    goto/16 :goto_0

    .line 1813
    :pswitch_3
    cmpg-float v3, p2, v7

    if-gez v3, :cond_3

    .line 1815
    aput v0, v2, v5

    .line 1816
    aput p2, v2, v6

    goto/16 :goto_0

    .line 1820
    :cond_3
    neg-float v3, v0

    aput v3, v2, v5

    .line 1821
    aput p2, v2, v6

    goto/16 :goto_0

    .line 1833
    :cond_4
    aput v0, v2, v5

    .line 1834
    aput p2, v2, v6

    goto/16 :goto_0

    .line 1774
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1809
    :pswitch_data_1
    .packed-switch 0x6
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private initResource(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imgInfo"    # Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 1595
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    .line 1597
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalROI:Landroid/graphics/RectF;

    .line 1599
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mStartPT:Landroid/graphics/PointF;

    .line 1600
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    .line 1601
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    .line 1602
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mEndPT:Landroid/graphics/PointF;

    .line 1604
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    .line 1606
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mStartPT:Landroid/graphics/PointF;

    .line 1607
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    .line 1608
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    .line 1609
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mEndPT:Landroid/graphics/PointF;

    .line 1611
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    .line 1612
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    .line 1613
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    .line 1614
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    .line 1616
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    .line 1617
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    .line 1618
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    .line 1619
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    .line 1621
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    .line 1622
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mInitROI:Landroid/graphics/RectF;

    .line 1623
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalROI:Landroid/graphics/RectF;

    .line 1624
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    .line 1626
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviousDrawRoi:Landroid/graphics/RectF;

    .line 1627
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    if-eqz v2, :cond_0

    .line 1628
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviousDrawRoi:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 1630
    :cond_0
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 1631
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint:Landroid/graphics/Paint;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1632
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/DashPathEffect;

    const/4 v4, 0x2

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    const/high16 v5, 0x40400000    # 3.0f

    invoke-direct {v3, v4, v5}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1633
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1634
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1636
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 1637
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint1:Landroid/graphics/Paint;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1638
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint1:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1639
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1641
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRectPaint:Landroid/graphics/Paint;

    .line 1642
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRectPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40800000    # 4.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1643
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRectPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1644
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRectPaint:Landroid/graphics/Paint;

    const v3, -0xeea080

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1646
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    .line 1648
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mGreyPaint:Landroid/graphics/Paint;

    .line 1649
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 1650
    .local v0, "cm":Landroid/graphics/ColorMatrix;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 1651
    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 1652
    .local v1, "f":Landroid/graphics/ColorMatrixColorFilter;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mGreyPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1654
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    if-eqz v2, :cond_1

    .line 1656
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    .line 1657
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    .line 1661
    :cond_1
    const/16 v2, 0x11

    invoke-static {p1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    .line 1662
    const/16 v2, 0xc8

    invoke-static {p1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getDpToPixel(Landroid/content/Context;I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMaxSize:F

    .line 1664
    return-void

    .line 1632
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method private moveDestCropRect(FFLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 8
    .param p1, "dx"    # F
    .param p2, "dy"    # F
    .param p3, "dstPt1"    # Landroid/graphics/PointF;
    .param p4, "dstPt2"    # Landroid/graphics/PointF;
    .param p5, "dstPt3"    # Landroid/graphics/PointF;
    .param p6, "dstPt4"    # Landroid/graphics/PointF;

    .prologue
    const/4 v7, 0x0

    .line 1942
    const/4 v2, 0x0

    .line 1943
    .local v2, "ret":Z
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    .line 1944
    .local v1, "leftTop":Landroid/graphics/PointF;
    iget v5, p3, Landroid/graphics/PointF;->x:F

    iget v6, p3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v5, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 1945
    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4}, Landroid/graphics/PointF;-><init>()V

    .line 1946
    .local v4, "rightTop":Landroid/graphics/PointF;
    iget v5, p4, Landroid/graphics/PointF;->x:F

    iget v6, p4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v5, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 1947
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    .line 1948
    .local v3, "rightBottom":Landroid/graphics/PointF;
    iget v5, p5, Landroid/graphics/PointF;->x:F

    iget v6, p5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v5, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 1949
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1950
    .local v0, "leftBottom":Landroid/graphics/PointF;
    iget v5, p6, Landroid/graphics/PointF;->x:F

    iget v6, p6, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v5, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 1951
    iget v5, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, p1

    iput v5, v1, Landroid/graphics/PointF;->x:F

    .line 1952
    iget v5, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v5, p2

    iput v5, v1, Landroid/graphics/PointF;->y:F

    .line 1953
    iget v5, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, p1

    iput v5, v4, Landroid/graphics/PointF;->x:F

    .line 1954
    iget v5, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v5, p2

    iput v5, v4, Landroid/graphics/PointF;->y:F

    .line 1955
    iget v5, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, p1

    iput v5, v3, Landroid/graphics/PointF;->x:F

    .line 1956
    iget v5, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v5, p2

    iput v5, v3, Landroid/graphics/PointF;->y:F

    .line 1957
    iget v5, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, p1

    iput v5, v0, Landroid/graphics/PointF;->x:F

    .line 1958
    iget v5, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v5, p2

    iput v5, v0, Landroid/graphics/PointF;->y:F

    .line 1960
    iget v5, v1, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v7

    if-ltz v5, :cond_0

    iget v5, v1, Landroid/graphics/PointF;->x:F

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 1961
    iget v5, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v7

    if-ltz v5, :cond_0

    iget v5, v1, Landroid/graphics/PointF;->y:F

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 1962
    iget v5, v4, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v7

    if-ltz v5, :cond_0

    iget v5, v4, Landroid/graphics/PointF;->x:F

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 1963
    iget v5, v4, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v7

    if-ltz v5, :cond_0

    iget v5, v4, Landroid/graphics/PointF;->y:F

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 1964
    iget v5, v3, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v7

    if-ltz v5, :cond_0

    iget v5, v3, Landroid/graphics/PointF;->x:F

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 1965
    iget v5, v3, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v7

    if-ltz v5, :cond_0

    iget v5, v3, Landroid/graphics/PointF;->y:F

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 1966
    iget v5, v0, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v5, v7

    if-ltz v5, :cond_0

    iget v5, v0, Landroid/graphics/PointF;->x:F

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 1967
    iget v5, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v5, v7

    if-ltz v5, :cond_0

    iget v5, v0, Landroid/graphics/PointF;->y:F

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 1969
    const/4 v2, 0x1

    .line 1971
    :cond_0
    return v2
.end method

.method private resizeRectWithRotationAngle(FFIII)V
    .locals 24
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "mode"    # I

    .prologue
    .line 995
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    neg-int v2, v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v20

    .line 996
    .local v20, "CurrPt1":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevX:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevY:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    neg-int v4, v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    invoke-static {v2, v3, v4, v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v21

    .line 998
    .local v21, "CurrPt2":Landroid/graphics/PointF;
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v21

    iget v3, v0, Landroid/graphics/PointF;->x:F

    sub-float v5, v2, v3

    .line 999
    .local v5, "dx":F
    move-object/from16 v0, v20

    iget v2, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, v21

    iget v3, v0, Landroid/graphics/PointF;->y:F

    sub-float v10, v2, v3

    .line 1000
    .local v10, "dy":F
    const/16 v23, 0x0

    .line 1002
    .local v23, "tempRoi":Landroid/graphics/RectF;
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->limit:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_0

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->limit:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 1060
    .end local v10    # "dy":F
    :cond_0
    :goto_0
    return-void

    .line 1005
    .restart local v10    # "dy":F
    :cond_1
    const/4 v2, 0x2

    move/from16 v0, p5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    move/from16 v0, p5

    if-ne v0, v2, :cond_4

    .line 1006
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p0

    move/from16 v4, p5

    move/from16 v9, p3

    move/from16 v10, p4

    invoke-direct/range {v2 .. v10}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getClipROI(Landroid/graphics/RectF;IFFIIII)Landroid/graphics/RectF;

    .end local v10    # "dy":F
    move-result-object v23

    .line 1017
    :cond_3
    :goto_1
    const/16 v20, 0x0

    .line 1018
    const/16 v21, 0x0

    .line 1020
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_9

    .line 1022
    move-object/from16 v0, v23

    iget v2, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, v23

    iget v6, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v6}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->checkBoundary(FFFF)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1024
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto :goto_0

    .line 1007
    .restart local v10    # "dy":F
    :cond_4
    const/4 v2, 0x3

    move/from16 v0, p5

    if-eq v0, v2, :cond_5

    const/4 v2, 0x5

    move/from16 v0, p5

    if-ne v0, v2, :cond_6

    .line 1008
    :cond_5
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v6, p0

    move/from16 v8, p5

    move/from16 v13, p3

    move/from16 v14, p4

    invoke-direct/range {v6 .. v14}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getClipROI(Landroid/graphics/RectF;IFFIIII)Landroid/graphics/RectF;

    move-result-object v23

    goto :goto_1

    .line 1009
    :cond_6
    const/4 v2, 0x6

    move/from16 v0, p5

    if-eq v0, v2, :cond_7

    const/16 v2, 0x9

    move/from16 v0, p5

    if-eq v0, v2, :cond_7

    const/16 v2, 0x8

    move/from16 v0, p5

    if-eq v0, v2, :cond_7

    const/4 v2, 0x7

    move/from16 v0, p5

    if-ne v0, v2, :cond_3

    .line 1011
    :cond_7
    move-object/from16 v0, p0

    move/from16 v1, p5

    invoke-direct {v0, v5, v10, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getDiffFromCropRatio(FFI)[F

    move-result-object v22

    .line 1012
    .local v22, "diff":[F
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    const/4 v2, 0x0

    aget v14, v22, v2

    const/4 v2, 0x1

    aget v15, v22, v2

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v11, p0

    move/from16 v13, p5

    move/from16 v18, p3

    move/from16 v19, p4

    invoke-direct/range {v11 .. v19}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getClipROI(Landroid/graphics/RectF;IFFIIII)Landroid/graphics/RectF;

    move-result-object v23

    goto/16 :goto_1

    .line 1028
    .end local v10    # "dy":F
    .end local v22    # "diff":[F
    :cond_8
    const-string v2, "checkBoundary false"

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1031
    :cond_9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 1033
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x31100000

    if-eq v2, v3, :cond_a

    .line 1034
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x31300000

    if-eq v2, v3, :cond_a

    .line 1035
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x31600000

    if-eq v2, v3, :cond_a

    .line 1036
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x17000000

    if-ne v2, v3, :cond_b

    .line 1037
    :cond_a
    move-object/from16 v0, v23

    iget v2, v0, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, v23

    iget v4, v0, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, v23

    iget v6, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v6}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->checkBoundary(FFFF)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1039
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, RC - resize() - mROI : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / tempRoi : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / mAngle : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1040
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto/16 :goto_0

    .line 1043
    :cond_b
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v2

    const/high16 v3, 0x11300000

    if-ne v2, v3, :cond_c

    .line 1045
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/RectF;->width()F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 1047
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto/16 :goto_0

    .line 1051
    :cond_c
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/RectF;->width()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mInitROI:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 1052
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/RectF;->width()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mInitROI:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 1053
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/RectF;->height()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mInitROI:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 1054
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/RectF;->height()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mInitROI:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 1056
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto/16 :goto_0
.end method

.method private touchRect(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 444
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 445
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 446
    .local v1, "y":F
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingX()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 447
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingY()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 449
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 467
    :cond_0
    :goto_0
    return-void

    .line 452
    :pswitch_0
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->InitMoveObject(FF)I

    move-result v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    .line 453
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    if-lt v2, v3, :cond_0

    .line 454
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->StartMoveObject(FF)V

    goto :goto_0

    .line 457
    :pswitch_1
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    if-lt v2, v3, :cond_0

    .line 458
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->StartMoveObject(FF)V

    goto :goto_0

    .line 462
    :pswitch_2
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    if-lt v2, v3, :cond_1

    .line 463
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->EndMoveObject()V

    .line 464
    :cond_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    goto :goto_0

    .line 449
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public EndMoveObject()V
    .locals 14

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/high16 v13, 0x40000000    # 2.0f

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    .line 1064
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    const/16 v7, 0x9

    if-eq v6, v7, :cond_0

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    if-ne v6, v8, :cond_3

    .line 1067
    :cond_0
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    if-ne v6, v8, :cond_2

    .line 1069
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v6}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 1070
    .local v5, "width":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v6}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 1073
    .local v0, "height":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1074
    .local v4, "pt":Landroid/graphics/PointF;
    iget v6, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1075
    add-int/lit8 v6, v5, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1076
    iget v6, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1077
    add-int/lit8 v6, v0, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1078
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1080
    const/4 v4, 0x0

    .line 1082
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1083
    iget v6, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1084
    add-int/lit8 v6, v5, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1085
    iget v6, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1086
    add-int/lit8 v6, v0, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1087
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1088
    const/4 v4, 0x0

    .line 1090
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1091
    iget v6, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1092
    add-int/lit8 v6, v5, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1093
    iget v6, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1094
    add-int/lit8 v6, v0, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1095
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1096
    const/4 v4, 0x0

    .line 1098
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1099
    iget v6, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1100
    add-int/lit8 v6, v5, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1101
    iget v6, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1102
    add-int/lit8 v6, v0, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1103
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1221
    .end local v0    # "height":I
    .end local v4    # "pt":Landroid/graphics/PointF;
    .end local v5    # "width":I
    :cond_1
    :goto_0
    const/4 v6, 0x0

    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    .line 1222
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->calculateOriginal()V

    .line 1223
    return-void

    .line 1106
    :cond_2
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    if-ne v6, v9, :cond_1

    .line 1109
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1110
    .restart local v4    # "pt":Landroid/graphics/PointF;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1111
    const/4 v4, 0x0

    .line 1113
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1114
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1115
    const/4 v4, 0x0

    .line 1117
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1118
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1119
    const/4 v4, 0x0

    .line 1121
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1122
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_0

    .line 1126
    .end local v4    # "pt":Landroid/graphics/PointF;
    :cond_3
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    if-lez v6, :cond_1

    .line 1127
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    if-ne v6, v8, :cond_4

    .line 1129
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 1130
    .local v2, "objectROI":Landroid/graphics/RectF;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v6}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 1131
    .restart local v5    # "width":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v6}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 1134
    .restart local v0    # "height":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1135
    .restart local v4    # "pt":Landroid/graphics/PointF;
    iget v6, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1136
    add-int/lit8 v6, v5, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1137
    iget v6, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1138
    add-int/lit8 v6, v0, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1139
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1141
    const/4 v4, 0x0

    .line 1143
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1144
    iget v6, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1145
    add-int/lit8 v6, v5, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1146
    iget v6, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1147
    add-int/lit8 v6, v0, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1148
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1149
    const/4 v4, 0x0

    .line 1151
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1152
    iget v6, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1153
    add-int/lit8 v6, v5, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1154
    iget v6, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1155
    add-int/lit8 v6, v0, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1156
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1157
    const/4 v4, 0x0

    .line 1159
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1160
    iget v6, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1161
    add-int/lit8 v6, v5, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->x:F

    .line 1162
    iget v6, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v11, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1163
    add-int/lit8 v6, v0, -0x1

    int-to-float v6, v6

    iget v7, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v4, Landroid/graphics/PointF;->y:F

    .line 1164
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1165
    const/4 v4, 0x0

    .line 1167
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v7

    add-float v3, v6, v12

    .line 1168
    .local v3, "objectW":F
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    sub-float/2addr v6, v7

    add-float v1, v6, v12

    .line 1170
    .local v1, "objectH":F
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v2, Landroid/graphics/RectF;->left:F

    .line 1171
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v2, Landroid/graphics/RectF;->right:F

    .line 1172
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v2, Landroid/graphics/RectF;->top:F

    .line 1173
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v2, Landroid/graphics/RectF;->bottom:F

    .line 1176
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v7, v2, Landroid/graphics/RectF;->left:F

    iget v8, v2, Landroid/graphics/RectF;->right:F

    add-float/2addr v7, v8

    div-float/2addr v7, v13

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 1177
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v7, v2, Landroid/graphics/RectF;->top:F

    iget v8, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v7, v8

    div-float/2addr v7, v13

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 1179
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    neg-int v7, v7

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(Landroid/graphics/PointF;IFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1182
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iput v7, v6, Landroid/graphics/RectF;->left:F

    .line 1183
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v4, Landroid/graphics/PointF;->y:F

    iput v7, v6, Landroid/graphics/RectF;->top:F

    .line 1185
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    add-float/2addr v7, v3

    sub-float/2addr v7, v12

    iput v7, v6, Landroid/graphics/RectF;->right:F

    .line 1186
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    add-float/2addr v7, v1

    sub-float/2addr v7, v12

    iput v7, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 1190
    .end local v0    # "height":I
    .end local v1    # "objectH":F
    .end local v2    # "objectROI":Landroid/graphics/RectF;
    .end local v3    # "objectW":F
    .end local v4    # "pt":Landroid/graphics/PointF;
    .end local v5    # "width":I
    :cond_4
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    if-ne v6, v9, :cond_1

    .line 1193
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1194
    .restart local v4    # "pt":Landroid/graphics/PointF;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1195
    const/4 v4, 0x0

    .line 1197
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1198
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1199
    const/4 v4, 0x0

    .line 1201
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1202
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1203
    const/4 v4, 0x0

    .line 1205
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    invoke-static {v6, v7, v8, v9, v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v4

    .line 1206
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v7, v4, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 1207
    const/4 v4, 0x0

    .line 1209
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 1210
    .restart local v2    # "objectROI":Landroid/graphics/RectF;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v2, Landroid/graphics/RectF;->left:F

    .line 1211
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v2, Landroid/graphics/RectF;->right:F

    .line 1212
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, v2, Landroid/graphics/RectF;->top:F

    .line 1213
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, v2, Landroid/graphics/RectF;->bottom:F

    .line 1215
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v7, v2, Landroid/graphics/RectF;->left:F

    iget v8, v2, Landroid/graphics/RectF;->right:F

    add-float/2addr v7, v8

    div-float/2addr v7, v13

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 1216
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v7, v2, Landroid/graphics/RectF;->top:F

    iget v8, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v7, v8

    div-float/2addr v7, v13

    iput v7, v6, Landroid/graphics/PointF;->y:F

    goto/16 :goto_0
.end method

.method protected InitMoveObject(FF)I
    .locals 11
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/high16 v1, 0x43200000    # 160.0f

    const/high16 v3, 0x40800000    # 4.0f

    const/high16 v2, 0x40000000    # 2.0f

    .line 561
    iput v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    .line 563
    const/16 v7, 0x28

    .line 565
    .local v7, "real_touch_area_offset":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 567
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 569
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    div-float/2addr v0, v3

    float-to-int v7, v0

    .line 575
    :goto_0
    if-ge v7, v10, :cond_1

    .line 577
    const/4 v7, 0x1

    .line 584
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float v0, p2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 586
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    add-float/2addr v0, v1

    div-float v6, v0, v2

    .line 587
    .local v6, "h_mid":F
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v1

    div-float v8, v0, v2

    .line 589
    .local v8, "v_mid":F
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 597
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    invoke-virtual {v0, v6, v8}, Landroid/graphics/PointF;->set(FF)V

    .line 600
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mStartPT:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 602
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    .line 628
    .end local v6    # "h_mid":F
    .end local v8    # "v_mid":F
    :cond_2
    :goto_1
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->getSubMode()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 660
    :goto_2
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevX:F

    .line 661
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevY:F

    .line 663
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    return v0

    .line 573
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    div-float/2addr v0, v3

    float-to-int v7, v0

    goto :goto_0

    .line 604
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 605
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    sub-float v0, p2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFreeRect:Z

    if-eqz v0, :cond_5

    .line 606
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    goto :goto_1

    .line 607
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    .line 608
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    sub-float v0, p2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFreeRect:Z

    if-eqz v0, :cond_6

    .line 609
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    goto :goto_1

    .line 610
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    .line 611
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    sub-float v0, p2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFreeRect:Z

    if-eqz v0, :cond_7

    .line 612
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    goto/16 :goto_1

    .line 613
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_8

    .line 614
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    sub-float v0, p2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_8

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFreeRect:Z

    if-eqz v0, :cond_8

    .line 615
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    goto/16 :goto_1

    .line 616
    :cond_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_9

    .line 617
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float v0, p2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_9

    .line 618
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    goto/16 :goto_1

    .line 619
    :cond_9
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_a

    .line 620
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float v0, p2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_a

    .line 621
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    goto/16 :goto_1

    .line 622
    :cond_a
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_b

    .line 623
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float v0, p2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v1, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_b

    .line 624
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    goto/16 :goto_1

    .line 625
    :cond_b
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    move v0, p1

    move v1, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->checkPointInRect(FFLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 626
    iput v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    goto/16 :goto_1

    .line 634
    :pswitch_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_2

    .line 640
    :pswitch_2
    iput v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    goto/16 :goto_2

    .line 589
    :pswitch_data_0
    .packed-switch 0x11201302
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 628
    :pswitch_data_1
    .packed-switch 0x11201302
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 634
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected StartMoveObject(FF)V
    .locals 31
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 671
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 672
    .local v15, "objectROI":Landroid/graphics/RectF;
    const/4 v6, 0x0

    .line 673
    .local v6, "width":I
    const/4 v7, 0x0

    .line 675
    .local v7, "height":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    if-eqz v3, :cond_0

    .line 677
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v6

    .line 678
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v7

    .line 681
    :cond_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    const/16 v4, 0x9

    if-ne v3, v4, :cond_4

    .line 682
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRightTopRotate:Z

    if-eqz v3, :cond_3

    .line 684
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mEndPT:Landroid/graphics/PointF;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v3, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 686
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mStartPT:Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mEndPT:Landroid/graphics/PointF;

    invoke-static {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getAngle(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)I

    move-result v9

    .line 688
    .local v9, "angle":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    int-to-float v5, v9

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-static {v3, v4, v5, v8, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v17

    .line 689
    .local v17, "pt":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v5, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 690
    const/16 v17, 0x0

    .line 692
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    int-to-float v5, v9

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-static {v3, v4, v5, v8, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v17

    .line 693
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v5, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 694
    const/16 v17, 0x0

    .line 696
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v5, v9

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-static {v3, v4, v5, v8, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v17

    .line 697
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v5, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 698
    const/16 v17, 0x0

    .line 700
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v5, v9

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-static {v3, v4, v5, v8, v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v17

    .line 701
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, v17

    iget v5, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 702
    const/16 v17, 0x0

    .line 704
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 706
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v4, v6, -0x1

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    .line 707
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-int/lit8 v4, v7, -0x1

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    .line 708
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v4, v6, -0x1

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    .line 709
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-int/lit8 v4, v7, -0x1

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    .line 710
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v4, v6, -0x1

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    .line 711
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-int/lit8 v4, v7, -0x1

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    .line 712
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v4, v6, -0x1

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    .line 713
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-int/lit8 v4, v7, -0x1

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    .line 714
    move-object/from16 v0, p0

    iput v9, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    .line 860
    .end local v9    # "angle":I
    .end local v17    # "pt":Landroid/graphics/PointF;
    :cond_1
    :goto_0
    const/4 v15, 0x0

    .line 862
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevX:F

    .line 863
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevY:F

    .line 864
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->calculateOriginal()V

    .line 865
    return-void

    .line 716
    .restart local v9    # "angle":I
    .restart local v17    # "pt":Landroid/graphics/PointF;
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 732
    move-object/from16 v0, p0

    iput v9, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    goto :goto_0

    .line 738
    .end local v9    # "angle":I
    .end local v17    # "pt":Landroid/graphics/PointF;
    :cond_3
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    move-object/from16 v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->resizeRectWithRotationAngle(FFIII)V

    goto :goto_0

    .line 741
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    if-lez v3, :cond_1

    .line 742
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_9

    .line 745
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevX:F

    sub-float v12, p1, v3

    .line 746
    .local v12, "dx":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevY:F

    sub-float v13, p2, v3

    .line 748
    .local v13, "dy":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float v22, v3, v12

    .line 749
    .local v22, "x1":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float v26, v3, v13

    .line 750
    .local v26, "y1":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float v23, v3, v12

    .line 751
    .local v23, "x2":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float v27, v3, v13

    .line 752
    .local v27, "y2":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float v24, v3, v12

    .line 753
    .local v24, "x3":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float v28, v3, v13

    .line 754
    .local v28, "y3":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float v25, v3, v12

    .line 755
    .local v25, "x4":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float v29, v3, v13

    .line 757
    .local v29, "y4":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    .line 759
    const/4 v3, 0x0

    cmpl-float v3, v22, v3

    if-ltz v3, :cond_5

    add-int/lit8 v3, v6, -0x1

    int-to-float v3, v3

    cmpg-float v3, v22, v3

    if-gtz v3, :cond_5

    .line 760
    const/4 v3, 0x0

    cmpl-float v3, v23, v3

    if-ltz v3, :cond_5

    add-int/lit8 v3, v6, -0x1

    int-to-float v3, v3

    cmpg-float v3, v23, v3

    if-gtz v3, :cond_5

    .line 761
    const/4 v3, 0x0

    cmpl-float v3, v24, v3

    if-ltz v3, :cond_5

    add-int/lit8 v3, v6, -0x1

    int-to-float v3, v3

    cmpg-float v3, v24, v3

    if-gtz v3, :cond_5

    .line 762
    const/4 v3, 0x0

    cmpl-float v3, v25, v3

    if-ltz v3, :cond_5

    add-int/lit8 v3, v6, -0x1

    int-to-float v3, v3

    cmpg-float v3, v25, v3

    if-gtz v3, :cond_5

    .line 763
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    move/from16 v0, v22

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 764
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    move/from16 v0, v23

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 765
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    move/from16 v0, v24

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 766
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    move/from16 v0, v25

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 769
    :cond_5
    const/4 v3, 0x0

    cmpl-float v3, v26, v3

    if-ltz v3, :cond_6

    add-int/lit8 v3, v7, -0x1

    int-to-float v3, v3

    cmpg-float v3, v26, v3

    if-gtz v3, :cond_6

    .line 770
    const/4 v3, 0x0

    cmpl-float v3, v27, v3

    if-ltz v3, :cond_6

    add-int/lit8 v3, v7, -0x1

    int-to-float v3, v3

    cmpg-float v3, v27, v3

    if-gtz v3, :cond_6

    .line 771
    const/4 v3, 0x0

    cmpl-float v3, v28, v3

    if-ltz v3, :cond_6

    add-int/lit8 v3, v7, -0x1

    int-to-float v3, v3

    cmpg-float v3, v28, v3

    if-gtz v3, :cond_6

    .line 772
    const/4 v3, 0x0

    cmpl-float v3, v29, v3

    if-ltz v3, :cond_6

    add-int/lit8 v3, v7, -0x1

    int-to-float v3, v3

    cmpg-float v3, v29, v3

    if-gtz v3, :cond_6

    .line 773
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    move/from16 v0, v26

    iput v0, v3, Landroid/graphics/PointF;->y:F

    .line 774
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    move/from16 v0, v27

    iput v0, v3, Landroid/graphics/PointF;->y:F

    .line 775
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    move/from16 v0, v28

    iput v0, v3, Landroid/graphics/PointF;->y:F

    .line 776
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    move/from16 v0, v29

    iput v0, v3, Landroid/graphics/PointF;->y:F

    .line 833
    :cond_6
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    add-float v16, v3, v4

    .line 834
    .local v16, "objectW":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    add-float v14, v3, v4

    .line 836
    .local v14, "objectH":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    invoke-static {v5, v8}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iput v3, v15, Landroid/graphics/RectF;->left:F

    .line 837
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    invoke-static {v5, v8}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iput v3, v15, Landroid/graphics/RectF;->right:F

    .line 838
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    invoke-static {v5, v8}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iput v3, v15, Landroid/graphics/RectF;->top:F

    .line 839
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    invoke-static {v5, v8}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iput v3, v15, Landroid/graphics/RectF;->bottom:F

    .line 841
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v4, v15, Landroid/graphics/RectF;->left:F

    iget v5, v15, Landroid/graphics/RectF;->right:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/PointF;->x:F

    .line 842
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v4, v15, Landroid/graphics/RectF;->top:F

    iget v5, v15, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/PointF;->y:F

    .line 845
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    neg-int v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    invoke-static {v3, v4, v5, v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(Landroid/graphics/PointF;IFF)Landroid/graphics/PointF;

    move-result-object v17

    .line 847
    .restart local v17    # "pt":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 848
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, v17

    iget v4, v0, Landroid/graphics/PointF;->y:F

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 849
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    add-float v4, v4, v16

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 850
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v14

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 779
    .end local v14    # "objectH":F
    .end local v16    # "objectW":F
    .end local v17    # "pt":Landroid/graphics/PointF;
    :cond_7
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 781
    add-float v3, v22, v23

    add-float v3, v3, v24

    add-float v3, v3, v25

    const/high16 v4, 0x40800000    # 4.0f

    div-float v10, v3, v4

    .line 782
    .local v10, "centerX":F
    add-float v3, v26, v27

    add-float v3, v3, v28

    add-float v3, v3, v29

    const/high16 v4, 0x40800000    # 4.0f

    div-float v11, v3, v4

    .line 785
    .local v11, "centerY":F
    new-instance v18, Landroid/graphics/PointF;

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 786
    .local v18, "pt1":Landroid/graphics/PointF;
    new-instance v19, Landroid/graphics/PointF;

    move-object/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 787
    .local v19, "pt2":Landroid/graphics/PointF;
    new-instance v20, Landroid/graphics/PointF;

    move-object/from16 v0, v20

    move/from16 v1, v24

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 788
    .local v20, "pt3":Landroid/graphics/PointF;
    new-instance v21, Landroid/graphics/PointF;

    move-object/from16 v0, v21

    move/from16 v1, v25

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 816
    .local v21, "pt4":Landroid/graphics/PointF;
    const/4 v3, 0x0

    cmpl-float v3, v10, v3

    if-ltz v3, :cond_8

    add-int/lit8 v3, v6, -0x1

    int-to-float v3, v3

    cmpg-float v3, v10, v3

    if-gtz v3, :cond_8

    .line 818
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    move/from16 v0, v22

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 819
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    move/from16 v0, v23

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 820
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    move/from16 v0, v24

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 821
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    move/from16 v0, v25

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 824
    :cond_8
    const/4 v3, 0x0

    cmpl-float v3, v11, v3

    if-ltz v3, :cond_6

    add-int/lit8 v3, v7, -0x1

    int-to-float v3, v3

    cmpg-float v3, v11, v3

    if-gtz v3, :cond_6

    .line 826
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    move/from16 v0, v26

    iput v0, v3, Landroid/graphics/PointF;->y:F

    .line 827
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    move/from16 v0, v27

    iput v0, v3, Landroid/graphics/PointF;->y:F

    .line 828
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    move/from16 v0, v28

    iput v0, v3, Landroid/graphics/PointF;->y:F

    .line 829
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    move/from16 v0, v29

    iput v0, v3, Landroid/graphics/PointF;->y:F

    goto/16 :goto_1

    .line 854
    .end local v10    # "centerX":F
    .end local v11    # "centerY":F
    .end local v12    # "dx":F
    .end local v13    # "dy":F
    .end local v18    # "pt1":Landroid/graphics/PointF;
    .end local v19    # "pt2":Landroid/graphics/PointF;
    .end local v20    # "pt3":Landroid/graphics/PointF;
    .end local v21    # "pt4":Landroid/graphics/PointF;
    .end local v22    # "x1":F
    .end local v23    # "x2":F
    .end local v24    # "x3":F
    .end local v25    # "x4":F
    .end local v26    # "y1":F
    .end local v27    # "y2":F
    .end local v28    # "y3":F
    .end local v29    # "y4":F
    :cond_9
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    const/4 v4, 0x2

    if-lt v3, v4, :cond_1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    const/16 v4, 0x8

    if-gt v3, v4, :cond_1

    .line 856
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    move-object/from16 v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->resizeRectWithRotationAngle(FFIII)V

    goto/16 :goto_0
.end method

.method protected calculateOriginal()V
    .locals 17

    .prologue
    .line 1256
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    if-eqz v12, :cond_0

    .line 1260
    const/16 v12, 0x9

    new-array v11, v12, [F

    .line 1261
    .local v11, "values":[F
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 1262
    .local v1, "applyMatrix":Landroid/graphics/Matrix;
    const/4 v5, 0x0

    .line 1263
    .local v5, "m":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    instance-of v12, v0, Lcom/sec/android/mimage/photoretouching/Core/Sticker;

    if-eqz v12, :cond_1

    .line 1264
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v12}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getOriginalToPreviewMatrix()Landroid/graphics/Matrix;

    move-result-object v5

    .line 1267
    :goto_0
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 1268
    .local v3, "im":Landroid/graphics/Matrix;
    invoke-virtual {v5, v11}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1269
    const/4 v12, 0x2

    aget v12, v11, v12

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-lez v12, :cond_2

    .line 1270
    const/4 v4, 0x0

    .line 1273
    .local v4, "left":F
    :goto_1
    const/4 v12, 0x5

    aget v12, v11, v12

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-lez v12, :cond_3

    .line 1274
    const/4 v10, 0x0

    .line 1278
    .local v10, "top":F
    :goto_2
    const/4 v12, 0x0

    aget v7, v11, v12

    .line 1279
    .local v7, "scaleX":F
    const/4 v12, 0x4

    aget v8, v11, v12

    .line 1281
    .local v8, "scaleY":F
    invoke-virtual {v1, v7, v8}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1282
    invoke-virtual {v1, v4, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1284
    invoke-virtual {v1, v3}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1285
    new-instance v9, Landroid/graphics/RectF;

    invoke-direct {v9}, Landroid/graphics/RectF;-><init>()V

    .line 1286
    .local v9, "src":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v12, v12, Landroid/graphics/RectF;->left:F

    .line 1287
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v13, v13, Landroid/graphics/RectF;->top:F

    .line 1288
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v14, v14, Landroid/graphics/RectF;->right:F

    .line 1289
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->bottom:F

    .line 1286
    invoke-virtual {v9, v12, v13, v14, v15}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1290
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 1291
    .local v2, "dst":Landroid/graphics/RectF;
    invoke-virtual {v3, v2, v9}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1292
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalROI:Landroid/graphics/RectF;

    iget v13, v2, Landroid/graphics/RectF;->left:F

    iget v14, v2, Landroid/graphics/RectF;->top:F

    iget v15, v2, Landroid/graphics/RectF;->right:F

    iget v0, v2, Landroid/graphics/RectF;->bottom:F

    move/from16 v16, v0

    invoke-virtual/range {v12 .. v16}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1293
    const/16 v12, 0x8

    new-array v6, v12, [F

    .line 1294
    .local v6, "pts":[F
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->x:F

    aput v13, v6, v12

    .line 1295
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->y:F

    aput v13, v6, v12

    .line 1296
    invoke-virtual {v3, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1297
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    const/4 v13, 0x0

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->x:F

    .line 1298
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    const/4 v13, 0x1

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->y:F

    .line 1299
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "original center:"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    invoke-virtual {v13}, Landroid/graphics/PointF;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1300
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->x:F

    aput v13, v6, v12

    .line 1301
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->y:F

    aput v13, v6, v12

    .line 1302
    const/4 v12, 0x2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->x:F

    aput v13, v6, v12

    .line 1303
    const/4 v12, 0x3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->y:F

    aput v13, v6, v12

    .line 1304
    const/4 v12, 0x4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->x:F

    aput v13, v6, v12

    .line 1305
    const/4 v12, 0x5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->y:F

    aput v13, v6, v12

    .line 1306
    const/4 v12, 0x6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->x:F

    aput v13, v6, v12

    .line 1307
    const/4 v12, 0x7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->y:F

    aput v13, v6, v12

    .line 1308
    invoke-virtual {v3, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1309
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    const/4 v13, 0x0

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->x:F

    .line 1310
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    const/4 v13, 0x1

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->y:F

    .line 1311
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    const/4 v13, 0x2

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->x:F

    .line 1312
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    const/4 v13, 0x3

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->y:F

    .line 1313
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    const/4 v13, 0x4

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->x:F

    .line 1314
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    const/4 v13, 0x5

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->y:F

    .line 1315
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    const/4 v13, 0x6

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->x:F

    .line 1316
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    const/4 v13, 0x7

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->y:F

    .line 1317
    const/4 v6, 0x0

    .line 1320
    .end local v1    # "applyMatrix":Landroid/graphics/Matrix;
    .end local v2    # "dst":Landroid/graphics/RectF;
    .end local v3    # "im":Landroid/graphics/Matrix;
    .end local v4    # "left":F
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v6    # "pts":[F
    .end local v7    # "scaleX":F
    .end local v8    # "scaleY":F
    .end local v9    # "src":Landroid/graphics/RectF;
    .end local v10    # "top":F
    .end local v11    # "values":[F
    :cond_0
    return-void

    .line 1266
    .restart local v1    # "applyMatrix":Landroid/graphics/Matrix;
    .restart local v5    # "m":Landroid/graphics/Matrix;
    .restart local v11    # "values":[F
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v12}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v5

    goto/16 :goto_0

    .line 1272
    .restart local v3    # "im":Landroid/graphics/Matrix;
    :cond_2
    const/4 v12, 0x2

    aget v4, v11, v12

    .restart local v4    # "left":F
    goto/16 :goto_1

    .line 1276
    :cond_3
    const/4 v12, 0x5

    aget v10, v11, v12

    .restart local v10    # "top":F
    goto/16 :goto_2
.end method

.method public configurationChanged()V
    .locals 15

    .prologue
    .line 232
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviousDrawRoi:Landroid/graphics/RectF;

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v11}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 234
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v10}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    .line 237
    .local v3, "m":Landroid/graphics/Matrix;
    const/16 v10, 0x9

    new-array v9, v10, [F

    .line 238
    .local v9, "values":[F
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 239
    .local v0, "applyMatrix":Landroid/graphics/Matrix;
    invoke-virtual {v3, v9}, Landroid/graphics/Matrix;->getValues([F)V

    .line 240
    const/4 v10, 0x2

    aget v10, v9, v10

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-lez v10, :cond_1

    .line 241
    const/4 v2, 0x0

    .line 244
    .local v2, "left":F
    :goto_0
    const/4 v10, 0x5

    aget v10, v9, v10

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-lez v10, :cond_2

    .line 245
    const/4 v8, 0x0

    .line 249
    .local v8, "top":F
    :goto_1
    const/4 v10, 0x0

    aget v5, v9, v10

    .line 250
    .local v5, "scaleX":F
    const/4 v10, 0x4

    aget v6, v9, v10

    .line 251
    .local v6, "scaleY":F
    invoke-virtual {v0, v5, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 252
    invoke-virtual {v0, v2, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 254
    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    .line 255
    .local v7, "src":Landroid/graphics/RectF;
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalROI:Landroid/graphics/RectF;

    invoke-virtual {v7, v10}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 256
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 257
    .local v1, "dst":Landroid/graphics/RectF;
    invoke-virtual {v0, v1, v7}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 258
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v11, v1, Landroid/graphics/RectF;->left:F

    float-to-int v11, v11

    int-to-float v11, v11

    .line 259
    iget v12, v1, Landroid/graphics/RectF;->top:F

    float-to-int v12, v12

    int-to-float v12, v12

    .line 260
    iget v13, v1, Landroid/graphics/RectF;->right:F

    float-to-int v13, v13

    int-to-float v13, v13

    .line 261
    iget v14, v1, Landroid/graphics/RectF;->bottom:F

    float-to-int v14, v14

    int-to-float v14, v14

    .line 258
    invoke-virtual {v10, v11, v12, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 262
    const/16 v10, 0x8

    new-array v4, v10, [F

    .line 263
    .local v4, "pts":[F
    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->x:F

    aput v11, v4, v10

    .line 264
    const/4 v10, 0x1

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    aput v11, v4, v10

    .line 265
    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 266
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    const/4 v11, 0x0

    aget v11, v4, v11

    float-to-int v11, v11

    int-to-float v11, v11

    iput v11, v10, Landroid/graphics/PointF;->x:F

    .line 267
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    const/4 v11, 0x1

    aget v11, v4, v11

    float-to-int v11, v11

    int-to-float v11, v11

    iput v11, v10, Landroid/graphics/PointF;->y:F

    .line 269
    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->x:F

    aput v11, v4, v10

    .line 270
    const/4 v10, 0x1

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    aput v11, v4, v10

    .line 271
    const/4 v10, 0x2

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->x:F

    aput v11, v4, v10

    .line 272
    const/4 v10, 0x3

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    aput v11, v4, v10

    .line 273
    const/4 v10, 0x4

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->x:F

    aput v11, v4, v10

    .line 274
    const/4 v10, 0x5

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    aput v11, v4, v10

    .line 275
    const/4 v10, 0x6

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->x:F

    aput v11, v4, v10

    .line 276
    const/4 v10, 0x7

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    aput v11, v4, v10

    .line 277
    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 278
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    const/4 v11, 0x0

    aget v11, v4, v11

    float-to-int v11, v11

    int-to-float v11, v11

    iput v11, v10, Landroid/graphics/PointF;->x:F

    .line 279
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    const/4 v11, 0x1

    aget v11, v4, v11

    float-to-int v11, v11

    int-to-float v11, v11

    iput v11, v10, Landroid/graphics/PointF;->y:F

    .line 280
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    const/4 v11, 0x2

    aget v11, v4, v11

    float-to-int v11, v11

    int-to-float v11, v11

    iput v11, v10, Landroid/graphics/PointF;->x:F

    .line 281
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    const/4 v11, 0x3

    aget v11, v4, v11

    float-to-int v11, v11

    int-to-float v11, v11

    iput v11, v10, Landroid/graphics/PointF;->y:F

    .line 282
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    const/4 v11, 0x4

    aget v11, v4, v11

    float-to-int v11, v11

    int-to-float v11, v11

    iput v11, v10, Landroid/graphics/PointF;->x:F

    .line 283
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    const/4 v11, 0x5

    aget v11, v4, v11

    float-to-int v11, v11

    int-to-float v11, v11

    iput v11, v10, Landroid/graphics/PointF;->y:F

    .line 284
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    const/4 v11, 0x6

    aget v11, v4, v11

    float-to-int v11, v11

    int-to-float v11, v11

    iput v11, v10, Landroid/graphics/PointF;->x:F

    .line 285
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    const/4 v11, 0x7

    aget v11, v4, v11

    float-to-int v11, v11

    int-to-float v11, v11

    iput v11, v10, Landroid/graphics/PointF;->y:F

    .line 287
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v10}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    iput v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    .line 288
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v10}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    iput v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    .line 291
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v10

    const/high16 v11, 0x31300000

    if-eq v10, v11, :cond_0

    .line 293
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->pinchZoom()V

    .line 295
    :cond_0
    return-void

    .line 243
    .end local v1    # "dst":Landroid/graphics/RectF;
    .end local v2    # "left":F
    .end local v4    # "pts":[F
    .end local v5    # "scaleX":F
    .end local v6    # "scaleY":F
    .end local v7    # "src":Landroid/graphics/RectF;
    .end local v8    # "top":F
    :cond_1
    const/4 v10, 0x2

    aget v2, v9, v10

    .restart local v2    # "left":F
    goto/16 :goto_0

    .line 247
    :cond_2
    const/4 v10, 0x5

    aget v8, v9, v10

    .restart local v8    # "top":F
    goto/16 :goto_1
.end method

.method public copy()Lcom/sec/android/mimage/photoretouching/Core/RectController;
    .locals 4

    .prologue
    .line 2087
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;-><init>()V

    .line 2089
    .local v0, "ret":Lcom/sec/android/mimage/photoretouching/Core/RectController;
    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    .line 2091
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedWidth:I

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedWidth:I

    .line 2092
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedHeight:I

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedHeight:I

    .line 2093
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    .line 2094
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    .line 2095
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFreeRect:Z

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFreeRect:Z

    .line 2096
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRightTopRotate:Z

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRightTopRotate:Z

    .line 2097
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCheckBoundary:Z

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCheckBoundary:Z

    .line 2098
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    .line 2099
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMaxSize:F

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMaxSize:F

    .line 2100
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviousDrawRoi:Landroid/graphics/RectF;

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviousDrawRoi:Landroid/graphics/RectF;

    .line 2102
    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 2103
    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint1:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 2104
    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRectPaint:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRectPaint:Landroid/graphics/Paint;

    .line 2105
    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    .line 2106
    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mGreyPaint:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mGreyPaint:Landroid/graphics/Paint;

    .line 2108
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    .line 2109
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mInitROI:Landroid/graphics/RectF;

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mInitROI:Landroid/graphics/RectF;

    .line 2110
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalROI:Landroid/graphics/RectF;

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalROI:Landroid/graphics/RectF;

    .line 2111
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    .line 2112
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mTouchType:I

    .line 2113
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevX:F

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevX:F

    .line 2114
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevY:F

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevY:F

    .line 2115
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    .line 2116
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mStartPT:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mStartPT:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mStartPT:Landroid/graphics/PointF;

    .line 2117
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mEndPT:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mEndPT:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mEndPT:Landroid/graphics/PointF;

    .line 2118
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    .line 2119
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    .line 2120
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    .line 2121
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    .line 2122
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    .line 2123
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    .line 2124
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    .line 2125
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    .line 2126
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    .line 2127
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    .line 2128
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    .line 2129
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapRotateResource:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapRotateResource:Landroid/graphics/Bitmap;

    .line 2131
    return-object v0
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 194
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    .line 195
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    .line 196
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mInitROI:Landroid/graphics/RectF;

    .line 197
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mStartPT:Landroid/graphics/PointF;

    .line 198
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    .line 199
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mEndPT:Landroid/graphics/PointF;

    .line 201
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    .line 202
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    .line 203
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    .line 204
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    .line 206
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 210
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    .line 211
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapRotateResource:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 213
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapRotateResource:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 215
    :cond_1
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapRotateResource:Landroid/graphics/Bitmap;

    .line 216
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint:Landroid/graphics/Paint;

    .line 217
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawLinePaint1:Landroid/graphics/Paint;

    .line 218
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRectPaint:Landroid/graphics/Paint;

    .line 219
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    .line 220
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mGreyPaint:Landroid/graphics/Paint;

    .line 221
    return-void
.end method

.method public drawRectBdry(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 379
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->getDrawROI()Landroid/graphics/RectF;

    move-result-object v0

    .line 380
    .local v0, "drawRectRoi":Landroid/graphics/RectF;
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    if-eqz v7, :cond_0

    if-eqz v0, :cond_0

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapRotateResource:Landroid/graphics/Bitmap;

    if-nez v7, :cond_1

    .line 441
    :cond_0
    :goto_0
    return-void

    .line 383
    :cond_1
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    .line 385
    .local v6, "viewTransform":Landroid/graphics/Matrix;
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 386
    .local v1, "drawRoi":Landroid/graphics/RectF;
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    .line 387
    .local v5, "src":Landroid/graphics/RectF;
    invoke-virtual {v5, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 388
    invoke-virtual {v6, v1, v5}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 390
    const/4 v7, 0x2

    new-array v4, v7, [F

    .line 391
    .local v4, "point":[F
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v8}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingX()I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    aput v7, v4, v9

    .line 392
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v8}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewPaddingY()I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    aput v7, v4, v10

    .line 393
    invoke-virtual {v6, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 395
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 396
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v7, v7

    .line 397
    aget v8, v4, v9

    .line 398
    aget v9, v4, v10

    .line 396
    invoke-virtual {p1, v7, v8, v9}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 399
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRectPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 401
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    shr-int/lit8 v3, v7, 0x1

    .line 402
    .local v3, "halfWidth":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    shr-int/lit8 v2, v7, 0x1

    .line 403
    .local v2, "halfHeight":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    .line 404
    iget v8, v1, Landroid/graphics/RectF;->right:F

    int-to-float v9, v3

    sub-float/2addr v8, v9

    .line 405
    iget v9, v1, Landroid/graphics/RectF;->bottom:F

    int-to-float v10, v2

    sub-float/2addr v9, v10

    .line 406
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    .line 403
    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 407
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    .line 408
    iget v8, v1, Landroid/graphics/RectF;->left:F

    int-to-float v9, v3

    sub-float/2addr v8, v9

    .line 409
    iget v9, v1, Landroid/graphics/RectF;->top:F

    int-to-float v10, v2

    sub-float/2addr v9, v10

    .line 410
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    .line 407
    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 411
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    .line 412
    iget v8, v1, Landroid/graphics/RectF;->left:F

    int-to-float v9, v3

    sub-float/2addr v8, v9

    .line 413
    iget v9, v1, Landroid/graphics/RectF;->bottom:F

    int-to-float v10, v2

    sub-float/2addr v9, v10

    .line 414
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    .line 411
    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 415
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapRotateResource:Landroid/graphics/Bitmap;

    .line 416
    iget v8, v1, Landroid/graphics/RectF;->right:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapRotateResource:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    sub-float/2addr v8, v9

    .line 417
    iget v9, v1, Landroid/graphics/RectF;->top:F

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapRotateResource:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    sub-float/2addr v9, v10

    .line 418
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    .line 415
    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 420
    iget-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFreeRect:Z

    if-eqz v7, :cond_2

    .line 422
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    .line 423
    iget v8, v1, Landroid/graphics/RectF;->left:F

    int-to-float v9, v3

    sub-float/2addr v8, v9

    .line 424
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v9

    int-to-float v10, v2

    sub-float/2addr v9, v10

    .line 425
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    .line 422
    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 426
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    .line 427
    iget v8, v1, Landroid/graphics/RectF;->right:F

    int-to-float v9, v3

    sub-float/2addr v8, v9

    .line 428
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v9

    int-to-float v10, v2

    sub-float/2addr v9, v10

    .line 429
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    .line 426
    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 430
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    .line 431
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    int-to-float v9, v3

    sub-float/2addr v8, v9

    .line 432
    iget v9, v1, Landroid/graphics/RectF;->top:F

    int-to-float v10, v2

    sub-float/2addr v9, v10

    .line 433
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    .line 430
    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 434
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapResource:Landroid/graphics/Bitmap;

    .line 435
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    int-to-float v9, v3

    sub-float/2addr v8, v9

    .line 436
    iget v9, v1, Landroid/graphics/RectF;->bottom:F

    int-to-float v10, v2

    sub-float/2addr v9, v10

    .line 437
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBitmapPaint:Landroid/graphics/Paint;

    .line 434
    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 440
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method

.method public getAngle()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    return v0
.end method

.method public getCenterPT()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 1537
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1538
    .local v0, "pt":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1539
    return-object v0
.end method

.method public getDestPt1()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 2079
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    return-object v0
.end method

.method public getDestPt2()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 2080
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    return-object v0
.end method

.method public getDestPt3()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 2081
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    return-object v0
.end method

.method public getDestPt4()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 2082
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    return-object v0
.end method

.method protected getDrawROI()Landroid/graphics/RectF;
    .locals 8

    .prologue
    .line 469
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    if-eqz v3, :cond_0

    .line 472
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 473
    .local v1, "m":Landroid/graphics/Matrix;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 474
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 475
    .local v2, "src":Landroid/graphics/RectF;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v2, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 476
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 477
    .local v0, "dst":Landroid/graphics/RectF;
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 478
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/RectF;->left:F

    .line 479
    iget v5, v0, Landroid/graphics/RectF;->top:F

    .line 480
    iget v6, v0, Landroid/graphics/RectF;->right:F

    .line 481
    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    .line 478
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 482
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    .line 485
    .end local v0    # "dst":Landroid/graphics/RectF;
    .end local v1    # "m":Landroid/graphics/Matrix;
    .end local v2    # "src":Landroid/graphics/RectF;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getOrgDestPt1()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 1232
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1233
    .local v0, "ret":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1234
    return-object v0
.end method

.method public getOrgDestPt2()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 1238
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1239
    .local v0, "ret":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1240
    return-object v0
.end method

.method public getOrgDestPt3()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 1244
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1245
    .local v0, "ret":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1246
    return-object v0
.end method

.method public getOrgDestPt4()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 1250
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1251
    .local v0, "ret":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1252
    return-object v0
.end method

.method public getOriginalCenter()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 1226
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1227
    .local v0, "ret":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalCenterPT:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1228
    return-object v0
.end method

.method public getOriginalDestPt1()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 1543
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1544
    .local v0, "pt":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt1:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1545
    return-object v0
.end method

.method public getOriginalDestPt2()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 1549
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1550
    .local v0, "pt":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt2:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1551
    return-object v0
.end method

.method public getOriginalDestPt3()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 1555
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1556
    .local v0, "pt":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt3:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1557
    return-object v0
.end method

.method public getOriginalDestPt4()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 1561
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 1562
    .local v0, "pt":Landroid/graphics/PointF;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalDestPt4:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1563
    return-object v0
.end method

.method public getOriginalRoi()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 489
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalROI:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 490
    .local v0, "ret":Landroid/graphics/RectF;
    return-object v0
.end method

.method public getPrevPT()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 1533
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevX:F

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPrevY:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method public getROIHeight()I
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getROIWidth()I
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getRoi()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 494
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    return-object v0
.end method

.method public isFreeRect()Z
    .locals 1

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFreeRect:Z

    return v0
.end method

.method public pinchZoom()V
    .locals 12

    .prologue
    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    .line 299
    const/16 v5, 0x9

    new-array v3, v5, [F

    .line 300
    .local v3, "values":[F
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 301
    .local v1, "m":Landroid/graphics/Matrix;
    invoke-virtual {v1, v3}, Landroid/graphics/Matrix;->getValues([F)V

    .line 302
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v4

    .line 303
    .local v4, "width":F
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v0

    .line 305
    .local v0, "height":F
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviousDrawRoi:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    add-float/2addr v6, v7

    float-to-int v6, v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Rect;->left:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 306
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviousDrawRoi:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    float-to-int v6, v6

    int-to-float v6, v6

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 307
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    add-float/2addr v6, v4

    float-to-int v6, v6

    int-to-float v6, v6

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 308
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    add-float/2addr v6, v0

    float-to-int v6, v6

    int-to-float v6, v6

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 310
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->centerX()F

    move-result v6

    float-to-int v6, v6

    int-to-float v6, v6

    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 311
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->centerY()F

    move-result v6

    float-to-int v6, v6

    int-to-float v6, v6

    iput v6, v5, Landroid/graphics/PointF;->y:F

    .line 313
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    .line 314
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    .line 316
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mBoundaryType:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 317
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    float-to-int v6, v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    float-to-int v7, v7

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    float-to-int v8, v8

    invoke-direct {p0, v5, v6, v7, v8}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->checkPinchZoomBoundary(IIII)V

    .line 319
    :cond_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v4, v5

    .line 320
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v0, v5

    .line 323
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v5, v6, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 324
    .local v2, "pt":Landroid/graphics/PointF;
    iget v5, v2, Landroid/graphics/PointF;->x:F

    invoke-static {v10, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 325
    sub-float v5, v4, v11

    float-to-int v5, v5

    int-to-float v5, v5

    iget v6, v2, Landroid/graphics/PointF;->x:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 326
    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v10, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 327
    sub-float v5, v0, v11

    float-to-int v5, v5

    int-to-float v5, v5

    iget v6, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 328
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v6, v2, Landroid/graphics/PointF;->x:F

    iget v7, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    .line 330
    const/4 v2, 0x0

    .line 332
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v5, v6, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 333
    iget v5, v2, Landroid/graphics/PointF;->x:F

    invoke-static {v10, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 334
    sub-float v5, v4, v11

    float-to-int v5, v5

    int-to-float v5, v5

    iget v6, v2, Landroid/graphics/PointF;->x:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 335
    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v10, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 336
    sub-float v5, v0, v11

    float-to-int v5, v5

    int-to-float v5, v5

    iget v6, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 337
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v6, v2, Landroid/graphics/PointF;->x:F

    iget v7, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    .line 338
    const/4 v2, 0x0

    .line 340
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v5, v6, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 341
    iget v5, v2, Landroid/graphics/PointF;->x:F

    invoke-static {v10, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 342
    sub-float v5, v4, v11

    float-to-int v5, v5

    int-to-float v5, v5

    iget v6, v2, Landroid/graphics/PointF;->x:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 343
    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v10, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 344
    sub-float v5, v0, v11

    float-to-int v5, v5

    int-to-float v5, v5

    iget v6, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 345
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v6, v2, Landroid/graphics/PointF;->x:F

    iget v7, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    .line 346
    const/4 v2, 0x0

    .line 348
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v5, v6, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 349
    iget v5, v2, Landroid/graphics/PointF;->x:F

    invoke-static {v10, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 350
    sub-float v5, v4, v11

    float-to-int v5, v5

    int-to-float v5, v5

    iget v6, v2, Landroid/graphics/PointF;->x:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->x:F

    .line 351
    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v10, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 352
    sub-float v5, v0, v11

    float-to-int v5, v5

    int-to-float v5, v5

    iget v6, v2, Landroid/graphics/PointF;->y:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    iput v5, v2, Landroid/graphics/PointF;->y:F

    .line 353
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v6, v2, Landroid/graphics/PointF;->x:F

    iget v7, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    .line 354
    const/4 v2, 0x0

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->calculateOriginal()V

    .line 358
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviousDrawRoi:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v6}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 359
    return-void
.end method

.method public setAngle(I)V
    .locals 0
    .param p1, "angle"    # I

    .prologue
    .line 189
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    .line 190
    return-void
.end method

.method protected setCheckBoundary(Z)V
    .locals 0
    .param p1, "checkBoundary"    # Z

    .prologue
    .line 174
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCheckBoundary:Z

    .line 175
    return-void
.end method

.method protected setCustomMinSize(F)V
    .locals 2
    .param p1, "scale"    # F

    .prologue
    .line 179
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 182
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    goto :goto_0
.end method

.method public setDestROI(FFFF)V
    .locals 9
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "right"    # F
    .param p4, "bottom"    # F

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 1568
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v3, p1, p2, p3, p4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1570
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    add-float/2addr v3, v4

    div-float/2addr v3, v8

    float-to-int v0, v3

    .line 1571
    .local v0, "offset_x":I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, v4

    div-float/2addr v3, v8

    float-to-int v1, v3

    .line 1574
    .local v1, "offset_y":I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v5, v5

    int-to-float v6, v0

    int-to-float v7, v1

    invoke-static {v3, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 1575
    .local v2, "pt":Landroid/graphics/PointF;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 1576
    const/4 v2, 0x0

    .line 1578
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v5, v5

    int-to-float v6, v0

    int-to-float v7, v1

    invoke-static {v3, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 1579
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 1580
    const/4 v2, 0x0

    .line 1582
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v5, v5

    int-to-float v6, v0

    int-to-float v7, v1

    invoke-static {v3, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 1583
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 1584
    const/4 v2, 0x0

    .line 1586
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    int-to-float v5, v5

    int-to-float v6, v0

    int-to-float v7, v1

    invoke-static {v3, v4, v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getRotationPoint(FFFFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 1587
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 1588
    const/4 v2, 0x0

    .line 1590
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    add-float/2addr v4, v5

    div-float/2addr v4, v8

    iput v4, v3, Landroid/graphics/PointF;->x:F

    .line 1591
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v4, v5

    div-float/2addr v4, v8

    iput v4, v3, Landroid/graphics/PointF;->y:F

    .line 1592
    return-void
.end method

.method protected setFreeRect(Z)V
    .locals 0
    .param p1, "freeRect"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFreeRect:Z

    .line 37
    return-void
.end method

.method protected setMaximumSize(I)V
    .locals 1
    .param p1, "maximum"    # I

    .prologue
    .line 1666
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMaxSize:F

    .line 1667
    return-void
.end method

.method protected setMinimumSize(I)V
    .locals 1
    .param p1, "minimum"    # I

    .prologue
    .line 1669
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCustomMinSize:F

    .line 1670
    return-void
.end method

.method protected setMultiGridStampRectRoi(Landroid/graphics/Rect;)V
    .locals 9
    .param p1, "roi"    # Landroid/graphics/Rect;

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 112
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedWidth:I

    .line 113
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedHeight:I

    .line 114
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v5, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 116
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    float-to-int v2, v5

    .line 117
    .local v2, "rectWidth":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-int v1, v5

    .line 119
    .local v1, "rectHeight":I
    if-le v2, v1, :cond_1

    .line 121
    int-to-float v5, v1

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedHeight:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    mul-float/2addr v5, v6

    float-to-int v4, v5

    .line 123
    .local v4, "tempWidth":I
    if-le v4, v2, :cond_0

    .line 125
    sub-int v0, v4, v2

    .line 126
    .local v0, "diff":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->right:F

    int-to-float v7, v0

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 150
    .end local v4    # "tempWidth":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 151
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 152
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 153
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 155
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    float-to-int v6, v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    float-to-int v7, v7

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    .line 156
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    float-to-int v6, v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    float-to-int v7, v7

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    .line 157
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    float-to-int v6, v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v7, v7

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    .line 158
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    float-to-int v6, v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v7, v7

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    .line 160
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    add-float/2addr v6, v7

    div-float/2addr v6, v8

    float-to-int v6, v6

    int-to-float v6, v6

    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 161
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v6, v7

    div-float/2addr v6, v8

    float-to-int v6, v6

    int-to-float v6, v6

    iput v6, v5, Landroid/graphics/PointF;->y:F

    .line 163
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    .line 164
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->calculateOriginal()V

    .line 166
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mInitROI:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v5, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 167
    return-void

    .line 130
    .end local v0    # "diff":I
    .restart local v4    # "tempWidth":I
    :cond_0
    sub-int v0, v2, v4

    .line 131
    .restart local v0    # "diff":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->right:F

    int-to-float v7, v0

    sub-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->right:F

    goto/16 :goto_0

    .line 136
    .end local v0    # "diff":I
    .end local v4    # "tempWidth":I
    :cond_1
    int-to-float v5, v2

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedHeight:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedWidth:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    mul-float/2addr v5, v6

    float-to-int v3, v5

    .line 138
    .local v3, "tempHeight":I
    if-le v3, v1, :cond_2

    .line 140
    sub-int v0, v3, v1

    .line 141
    .restart local v0    # "diff":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->bottom:F

    int-to-float v7, v0

    add-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 145
    .end local v0    # "diff":I
    :cond_2
    sub-int v0, v1, v3

    .line 146
    .restart local v0    # "diff":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->bottom:F

    int-to-float v7, v0

    sub-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0
.end method

.method public setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V
    .locals 17
    .param p1, "roi"    # Landroid/graphics/RectF;
    .param p2, "center"    # Landroid/graphics/PointF;
    .param p3, "mOrgDestPt1"    # Landroid/graphics/PointF;
    .param p4, "mOrgDestPt2"    # Landroid/graphics/PointF;
    .param p5, "mOrgDestPt3"    # Landroid/graphics/PointF;
    .param p6, "mOrgDestPt4"    # Landroid/graphics/PointF;
    .param p7, "angle"    # I

    .prologue
    .line 498
    move/from16 v0, p7

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mAngle:I

    .line 499
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mOriginalROI:Landroid/graphics/RectF;

    .line 501
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    if-eqz v12, :cond_0

    .line 503
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v12}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v5

    .line 506
    .local v5, "m":Landroid/graphics/Matrix;
    const/16 v12, 0x9

    new-array v11, v12, [F

    .line 507
    .local v11, "values":[F
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 508
    .local v2, "applyMatrix":Landroid/graphics/Matrix;
    invoke-virtual {v5, v11}, Landroid/graphics/Matrix;->getValues([F)V

    .line 509
    const/4 v12, 0x2

    aget v12, v11, v12

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-lez v12, :cond_1

    .line 510
    const/4 v4, 0x0

    .line 513
    .local v4, "left":F
    :goto_0
    const/4 v12, 0x5

    aget v12, v11, v12

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-lez v12, :cond_2

    .line 514
    const/4 v10, 0x0

    .line 518
    .local v10, "top":F
    :goto_1
    const/4 v12, 0x0

    aget v7, v11, v12

    .line 519
    .local v7, "scaleX":F
    const/4 v12, 0x4

    aget v8, v11, v12

    .line 520
    .local v8, "scaleY":F
    invoke-virtual {v2, v7, v8}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 521
    invoke-virtual {v2, v4, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 524
    new-instance v9, Landroid/graphics/RectF;

    invoke-direct {v9}, Landroid/graphics/RectF;-><init>()V

    .line 525
    .local v9, "src":Landroid/graphics/RectF;
    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 526
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 527
    .local v3, "dst":Landroid/graphics/RectF;
    invoke-virtual {v2, v3, v9}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 528
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v13, v3, Landroid/graphics/RectF;->left:F

    float-to-int v13, v13

    int-to-float v13, v13

    .line 529
    iget v14, v3, Landroid/graphics/RectF;->top:F

    float-to-int v14, v14

    int-to-float v14, v14

    .line 530
    iget v15, v3, Landroid/graphics/RectF;->right:F

    float-to-int v15, v15

    int-to-float v15, v15

    .line 531
    iget v0, v3, Landroid/graphics/RectF;->bottom:F

    move/from16 v16, v0

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    .line 528
    invoke-virtual/range {v12 .. v16}, Landroid/graphics/RectF;->set(FFFF)V

    .line 533
    const/16 v12, 0x8

    new-array v6, v12, [F

    .line 534
    .local v6, "pts":[F
    const/4 v12, 0x0

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/PointF;->x:F

    aput v13, v6, v12

    .line 535
    const/4 v12, 0x1

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/PointF;->y:F

    aput v13, v6, v12

    .line 536
    invoke-virtual {v2, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 537
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    const/4 v13, 0x0

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->x:F

    .line 538
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    const/4 v13, 0x1

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->y:F

    .line 540
    const/4 v12, 0x0

    move-object/from16 v0, p3

    iget v13, v0, Landroid/graphics/PointF;->x:F

    aput v13, v6, v12

    .line 541
    const/4 v12, 0x1

    move-object/from16 v0, p3

    iget v13, v0, Landroid/graphics/PointF;->y:F

    aput v13, v6, v12

    .line 542
    const/4 v12, 0x2

    move-object/from16 v0, p4

    iget v13, v0, Landroid/graphics/PointF;->x:F

    aput v13, v6, v12

    .line 543
    const/4 v12, 0x3

    move-object/from16 v0, p4

    iget v13, v0, Landroid/graphics/PointF;->y:F

    aput v13, v6, v12

    .line 544
    const/4 v12, 0x4

    move-object/from16 v0, p5

    iget v13, v0, Landroid/graphics/PointF;->x:F

    aput v13, v6, v12

    .line 545
    const/4 v12, 0x5

    move-object/from16 v0, p5

    iget v13, v0, Landroid/graphics/PointF;->y:F

    aput v13, v6, v12

    .line 546
    const/4 v12, 0x6

    move-object/from16 v0, p6

    iget v13, v0, Landroid/graphics/PointF;->x:F

    aput v13, v6, v12

    .line 547
    const/4 v12, 0x7

    move-object/from16 v0, p6

    iget v13, v0, Landroid/graphics/PointF;->y:F

    aput v13, v6, v12

    .line 548
    invoke-virtual {v2, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 549
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    const/4 v13, 0x0

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->x:F

    .line 550
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    const/4 v13, 0x1

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->y:F

    .line 551
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    const/4 v13, 0x2

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->x:F

    .line 552
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    const/4 v13, 0x3

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->y:F

    .line 553
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    const/4 v13, 0x4

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->x:F

    .line 554
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    const/4 v13, 0x5

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->y:F

    .line 555
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    const/4 v13, 0x6

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->x:F

    .line 556
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    const/4 v13, 0x7

    aget v13, v6, v13

    float-to-int v13, v13

    int-to-float v13, v13

    iput v13, v12, Landroid/graphics/PointF;->y:F

    .line 558
    .end local v2    # "applyMatrix":Landroid/graphics/Matrix;
    .end local v3    # "dst":Landroid/graphics/RectF;
    .end local v4    # "left":F
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v6    # "pts":[F
    .end local v7    # "scaleX":F
    .end local v8    # "scaleY":F
    .end local v9    # "src":Landroid/graphics/RectF;
    .end local v10    # "top":F
    .end local v11    # "values":[F
    :cond_0
    return-void

    .line 512
    .restart local v2    # "applyMatrix":Landroid/graphics/Matrix;
    .restart local v5    # "m":Landroid/graphics/Matrix;
    .restart local v11    # "values":[F
    :cond_1
    const/4 v12, 0x2

    aget v4, v11, v12

    .restart local v4    # "left":F
    goto/16 :goto_0

    .line 516
    :cond_2
    const/4 v12, 0x5

    aget v10, v11, v12

    .restart local v10    # "top":F
    goto/16 :goto_1
.end method

.method protected setRectRoi(Landroid/graphics/Rect;)V
    .locals 13
    .param p1, "roi"    # Landroid/graphics/Rect;

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    .line 40
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v9

    iput v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedWidth:I

    .line 41
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v9

    iput v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedHeight:I

    .line 42
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v9, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 44
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v9

    float-to-int v4, v9

    .line 45
    .local v4, "rectWidth":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v9

    float-to-int v3, v9

    .line 47
    .local v3, "rectHeight":I
    if-le v4, v3, :cond_1

    .line 49
    int-to-float v9, v3

    iget v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedWidth:I

    int-to-float v10, v10

    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedHeight:I

    int-to-float v11, v11

    div-float/2addr v10, v11

    mul-float/2addr v9, v10

    float-to-int v8, v9

    .line 51
    .local v8, "tempWidth":I
    if-le v8, v4, :cond_0

    .line 53
    sub-int v2, v8, v4

    .line 54
    .local v2, "diff":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v9, Landroid/graphics/RectF;->right:F

    int-to-float v11, v2

    add-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->right:F

    .line 63
    :goto_0
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v9}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v10}, Landroid/graphics/RectF;->width()F

    move-result v10

    sub-float/2addr v9, v10

    div-float/2addr v9, v12

    float-to-int v0, v9

    .line 64
    .local v0, "absoluteMarginX":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v9

    float-to-int v7, v9

    .line 66
    .local v7, "tempRectWidth":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    int-to-float v10, v0

    iput v10, v9, Landroid/graphics/RectF;->left:F

    .line 67
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    int-to-float v11, v7

    add-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->right:F

    .line 91
    .end local v0    # "absoluteMarginX":I
    .end local v7    # "tempRectWidth":I
    .end local v8    # "tempWidth":I
    :goto_1
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v11}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v11

    iget v11, v11, Landroid/graphics/Rect;->left:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->left:F

    .line 92
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v11}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v11

    iget v11, v11, Landroid/graphics/Rect;->left:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->right:F

    .line 93
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v11}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v11

    iget v11, v11, Landroid/graphics/Rect;->top:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->top:F

    .line 94
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDrawROI:Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v11}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v11

    iget v11, v11, Landroid/graphics/Rect;->top:I

    int-to-float v11, v11

    add-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->bottom:F

    .line 96
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt1:Landroid/graphics/PointF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    invoke-virtual {v9, v10, v11}, Landroid/graphics/PointF;->set(FF)V

    .line 97
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt2:Landroid/graphics/PointF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    invoke-virtual {v9, v10, v11}, Landroid/graphics/PointF;->set(FF)V

    .line 98
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt3:Landroid/graphics/PointF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v9, v10, v11}, Landroid/graphics/PointF;->set(FF)V

    .line 99
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mDestPt4:Landroid/graphics/PointF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v9, v10, v11}, Landroid/graphics/PointF;->set(FF)V

    .line 101
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->right:F

    add-float/2addr v10, v11

    div-float/2addr v10, v12

    iput v10, v9, Landroid/graphics/PointF;->x:F

    .line 102
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mCenterPT:Landroid/graphics/PointF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v10, v11

    div-float/2addr v10, v12

    iput v10, v9, Landroid/graphics/PointF;->y:F

    .line 105
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v9}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    iput v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewWidth:I

    .line 106
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v9}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    iput v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mPreviewHeight:I

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->calculateOriginal()V

    .line 108
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mInitROI:Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v9, v10}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 109
    return-void

    .line 58
    .end local v2    # "diff":I
    .restart local v8    # "tempWidth":I
    :cond_0
    sub-int v2, v4, v8

    .line 59
    .restart local v2    # "diff":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v9, Landroid/graphics/RectF;->right:F

    int-to-float v11, v2

    sub-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->right:F

    goto/16 :goto_0

    .line 71
    .end local v2    # "diff":I
    .end local v8    # "tempWidth":I
    :cond_1
    int-to-float v9, v4

    iget v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedHeight:I

    int-to-float v10, v10

    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mFixedWidth:I

    int-to-float v11, v11

    div-float/2addr v10, v11

    mul-float/2addr v9, v10

    float-to-int v5, v9

    .line 73
    .local v5, "tempHeight":I
    if-le v5, v3, :cond_2

    .line 75
    sub-int v2, v5, v3

    .line 76
    .restart local v2    # "diff":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v9, Landroid/graphics/RectF;->bottom:F

    int-to-float v11, v2

    add-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->bottom:F

    .line 84
    :goto_2
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mImageData:Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;

    invoke-interface {v9}, Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;->getPreviewHeight()I

    move-result v9

    int-to-float v9, v9

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v10

    sub-float/2addr v9, v10

    div-float/2addr v9, v12

    float-to-int v1, v9

    .line 85
    .local v1, "absoluteMarginY":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v9

    float-to-int v6, v9

    .line 87
    .local v6, "tempRectHeight":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    int-to-float v10, v1

    iput v10, v9, Landroid/graphics/RectF;->top:F

    .line 88
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    int-to-float v11, v6

    add-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    .line 80
    .end local v1    # "absoluteMarginY":I
    .end local v2    # "diff":I
    .end local v6    # "tempRectHeight":I
    :cond_2
    sub-int v2, v3, v5

    .line 81
    .restart local v2    # "diff":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mROI:Landroid/graphics/RectF;

    iget v10, v9, Landroid/graphics/RectF;->bottom:F

    int-to-float v11, v2

    sub-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/RectF;->bottom:F

    goto :goto_2
.end method

.method protected setRightTopRotate(Z)V
    .locals 0
    .param p1, "isRotate"    # Z

    .prologue
    .line 170
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Core/RectController;->mRightTopRotate:Z

    .line 171
    return-void
.end method

.method public touch(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 362
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/RectController;->touchRect(Landroid/view/MotionEvent;)V

    .line 363
    return-void
.end method
