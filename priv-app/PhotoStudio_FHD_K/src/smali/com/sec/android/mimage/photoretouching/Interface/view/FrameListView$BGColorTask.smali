.class Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;
.super Landroid/os/AsyncTask;
.source "FrameListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BGColorTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field bitmap:Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

.field time:J


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)V
    .locals 1

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->bitmap:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->doInBackground([Ljava/lang/Integer;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Integer;)Ljava/lang/Void;
    .locals 11
    .param p1, "arg0"    # [Ljava/lang/Integer;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 143
    aget-object v4, p1, v9

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 144
    .local v3, "maxPage":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 145
    .local v1, "doList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v4, 0x2

    aget-object v4, p1, v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 146
    .local v0, "actualIdx":I
    add-int/lit8 v4, v0, -0x1

    if-ltz v4, :cond_0

    .line 147
    add-int/lit8 v4, v0, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    :cond_0
    add-int/lit8 v4, v0, 0x1

    if-ge v4, v3, :cond_1

    .line 149
    add-int/lit8 v4, v0, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 159
    const/4 v4, 0x0

    return-object v4

    .line 151
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 152
    .local v2, "id":Ljava/lang/Integer;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6, v9, v10}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->applyImage(IZZ)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->bitmap:Landroid/graphics/Bitmap;

    .line 153
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_3

    .line 154
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->bitmap:Landroid/graphics/Bitmap;

    .line 155
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getFrameVectorColor()I

    move-result v7

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 154
    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->setBitmap(Landroid/graphics/Bitmap;IIZ)V

    .line 156
    :cond_3
    new-array v5, v9, [Ljava/lang/Integer;

    aput-object v2, v5, v10

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->onCancelled(Ljava/lang/Void;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 164
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->bitmap:Landroid/graphics/Bitmap;

    .line 169
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 186
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 187
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 173
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->time:J

    .line 174
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 175
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 1
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->invalidate()V

    .line 181
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
