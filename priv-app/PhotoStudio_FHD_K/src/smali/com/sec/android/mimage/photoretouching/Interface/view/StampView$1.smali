.class Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$1;
.super Ljava/lang/Object;
.source "StampView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnStampCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setStamp(I)Z
    .locals 6
    .param p1, "viewId"    # I

    .prologue
    const/4 v0, 0x1

    .line 140
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTime:J
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 141
    const/4 v0, 0x0

    .line 146
    :goto_0
    return v0

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableCancel()V

    .line 143
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    invoke-virtual {v1, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->setStampMode(I)V

    .line 144
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    iput-boolean v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->isStampApplied:Z

    .line 145
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;J)V

    goto :goto_0
.end method
