.class public Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;
.super Ljava/lang/Object;
.source "EffectEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;,
        Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    }
.end annotation


# static fields
.field public static final SPHERICITY_MAX_BRUSH_SIZE:I = 0x8c

.field public static isFirstTimeDnL:Z


# instance fields
.field private final GREAYSCALE_APPLY_STEP:I

.field private mContext:Landroid/content/Context;

.field private mEffectType:I

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mMaskAlpha:I

.field private mStartInitializing:Z

.field private mStep:I

.field private mTexturePreview:[I

.field private mTexturePreview2:[I

.field public mutexOn:Z

.field private myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

.field private myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 584
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->isFirstTimeDnL:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    .line 577
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .line 578
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .line 580
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 582
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mEffectType:I

    .line 585
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mutexOn:Z

    .line 587
    const/16 v0, 0x80

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mMaskAlpha:I

    .line 589
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mStartInitializing:Z

    .line 590
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    .line 591
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview2:[I

    .line 592
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mStep:I

    .line 594
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->GREAYSCALE_APPLY_STEP:I

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    .line 577
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .line 578
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .line 580
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 582
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mEffectType:I

    .line 585
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mutexOn:Z

    .line 587
    const/16 v0, 0x80

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mMaskAlpha:I

    .line 589
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mStartInitializing:Z

    .line 590
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    .line 591
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview2:[I

    .line 592
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mStep:I

    .line 594
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->GREAYSCALE_APPLY_STEP:I

    .line 39
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 42
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mStep:I

    .line 44
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    .line 45
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    .locals 1

    .prologue
    .line 577
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    return-object v0
.end method

.method private makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V
    .locals 8
    .param p1, "src"    # Landroid/graphics/Bitmap;
    .param p2, "outBuffer"    # [I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v2, 0x0

    .line 395
    const/4 v1, 0x1

    invoke-static {p1, p3, p4, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .local v0, "resized":Landroid/graphics/Bitmap;
    move-object v1, p2

    move v3, p3

    move v4, v2

    move v5, v2

    move v6, p3

    move v7, p4

    .line 396
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 397
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 398
    const/4 v0, 0x0

    .line 399
    return-void
.end method


# virtual methods
.method public applyOriginal()[I
    .locals 26

    .prologue
    .line 231
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    move-result-object v3

    .line 232
    .local v3, "mask":[B
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v2

    .line 233
    .local v2, "input":[I
    const/4 v4, 0x0

    .line 234
    .local v4, "output":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v8, :cond_3

    .line 236
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v5

    .line 237
    .local v5, "w":I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v6

    .line 238
    .local v6, "h":I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v25

    .line 240
    .local v25, "rect":Landroid/graphics/Rect;
    mul-int v8, v5, v6

    new-array v4, v8, [I

    .line 241
    const/4 v8, 0x0

    const/4 v9, 0x0

    array-length v14, v4

    invoke-static {v2, v8, v4, v9, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 244
    new-instance v7, Landroid/graphics/Rect;

    move-object/from16 v0, v25

    invoke-direct {v7, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 246
    .local v7, "r":Landroid/graphics/Rect;
    invoke-virtual {v7}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 247
    const/16 v8, 0x64

    const/4 v9, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v7, v8, v9, v14, v15}, Landroid/graphics/Rect;->set(IIII)V

    .line 249
    :cond_0
    iget v8, v7, Landroid/graphics/Rect;->right:I

    add-int/lit8 v9, v5, -0x1

    if-le v8, v9, :cond_1

    .line 250
    add-int/lit8 v8, v5, -0x1

    iput v8, v7, Landroid/graphics/Rect;->right:I

    .line 251
    :cond_1
    iget v8, v7, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v9, v6, -0x1

    if-le v8, v9, :cond_2

    .line 252
    add-int/lit8 v8, v6, -0x1

    iput v8, v7, Landroid/graphics/Rect;->bottom:I

    .line 255
    :cond_2
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mEffectType:I

    packed-switch v8, :pswitch_data_0

    .line 375
    :goto_0
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v9

    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 376
    .local v12, "ret":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    .line 377
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v19

    .line 376
    invoke-virtual/range {v12 .. v19}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 378
    new-instance v13, Landroid/graphics/Canvas;

    invoke-direct {v13, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 379
    .local v13, "canvas":Landroid/graphics/Canvas;
    new-instance v22, Landroid/graphics/Paint;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Paint;-><init>()V

    .line 380
    .local v22, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mMaskAlpha:I

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 381
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 382
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v20

    const/16 v21, 0x1

    move-object v14, v4

    .line 381
    invoke-virtual/range {v13 .. v22}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 384
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    .line 385
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v21

    move-object v14, v12

    .line 384
    invoke-virtual/range {v14 .. v21}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 387
    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 389
    const-string v8, "bigheadk, applyOriginal end"

    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 391
    .end local v5    # "w":I
    .end local v6    # "h":I
    .end local v7    # "r":Landroid/graphics/Rect;
    .end local v12    # "ret":Landroid/graphics/Bitmap;
    .end local v13    # "canvas":Landroid/graphics/Canvas;
    .end local v22    # "paint":Landroid/graphics/Paint;
    .end local v25    # "rect":Landroid/graphics/Rect;
    :cond_3
    return-object v2

    .line 258
    .restart local v5    # "w":I
    .restart local v6    # "h":I
    .restart local v7    # "r":Landroid/graphics/Rect;
    .restart local v25    # "rect":Landroid/graphics/Rect;
    :pswitch_1
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyVignettePartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 261
    :pswitch_2
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyTurquoisePartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 264
    :pswitch_3
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyVintagePartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 267
    :pswitch_4
    const/16 v13, 0xa

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move v12, v6

    move-object v14, v7

    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlGreyscale([I[B[IIIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 270
    :pswitch_5
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySepiaPartial([I[B[IIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 273
    :pswitch_6
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyDownlightPartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 276
    :pswitch_7
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyBluewashPartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 279
    :pswitch_8
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyNostalgiaPartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 282
    :pswitch_9
    const/16 v13, 0x32

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move v12, v6

    move-object v14, v7

    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlFadedColour([I[B[IIIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 285
    :pswitch_a
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mStep:I

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move v12, v6

    move-object v14, v7

    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySharpenPartial([I[B[IIIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 288
    :pswitch_b
    const/16 v13, 0x32

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move v12, v6

    move-object v14, v7

    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySoftglowPartial([I[B[IIIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 291
    :pswitch_c
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyRainbowPartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 294
    :pswitch_d
    mul-int v8, v5, v6

    new-array v10, v8, [I

    .line 295
    .local v10, "texture":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0201c8

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 296
    .local v23, "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    move-object v8, v2

    move-object v9, v4

    move-object v11, v3

    move v12, v5

    move v13, v6

    move-object v14, v7

    .line 297
    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyStardustPartial([I[I[I[BIILandroid/graphics/Rect;)V

    .line 298
    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 299
    const/4 v10, 0x0

    .line 300
    goto/16 :goto_0

    .line 302
    .end local v10    # "texture":[I
    .end local v23    # "bitmap":Landroid/graphics/Bitmap;
    :pswitch_e
    mul-int v8, v5, v6

    new-array v10, v8, [I

    .line 303
    .restart local v10    # "texture":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0201c7

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 304
    .restart local v23    # "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    move-object v8, v2

    move-object v9, v4

    move-object v11, v3

    move v12, v5

    move v13, v6

    move-object v14, v7

    .line 305
    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyLightFlarePartial([I[I[I[BIILandroid/graphics/Rect;)V

    .line 306
    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 307
    const/4 v10, 0x0

    .line 308
    goto/16 :goto_0

    .line 310
    .end local v10    # "texture":[I
    .end local v23    # "bitmap":Landroid/graphics/Bitmap;
    :pswitch_f
    mul-int v8, v5, v6

    new-array v10, v8, [I

    .line 311
    .restart local v10    # "texture":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0201c9

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 312
    .restart local v23    # "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    move-object v8, v2

    move-object v9, v4

    move-object v11, v3

    move v12, v5

    move v13, v6

    move-object v14, v7

    .line 313
    invoke-static/range {v8 .. v14}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyLightStreakPartial([I[I[I[BIILandroid/graphics/Rect;)V

    .line 314
    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 315
    const/4 v10, 0x0

    .line 316
    goto/16 :goto_0

    .line 318
    .end local v10    # "texture":[I
    .end local v23    # "bitmap":Landroid/graphics/Bitmap;
    :pswitch_10
    mul-int v8, v5, v6

    new-array v10, v8, [I

    .line 319
    .restart local v10    # "texture":[I
    mul-int v8, v5, v6

    new-array v11, v8, [I

    .line 320
    .local v11, "texture2":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0200d5

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 321
    .restart local v23    # "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 322
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020555

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v24

    .line 323
    .local v24, "bitmap2":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 324
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v11, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    move-object v8, v2

    move-object v9, v4

    move-object v12, v3

    move v13, v5

    move v14, v6

    move-object v15, v7

    .line 325
    invoke-static/range {v8 .. v15}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyImpressionistPartial([I[I[I[I[BIILandroid/graphics/Rect;)V

    .line 326
    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 327
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 328
    const/4 v10, 0x0

    .line 329
    const/4 v11, 0x0

    .line 330
    goto/16 :goto_0

    .line 332
    .end local v10    # "texture":[I
    .end local v11    # "texture2":[I
    .end local v23    # "bitmap":Landroid/graphics/Bitmap;
    .end local v24    # "bitmap2":Landroid/graphics/Bitmap;
    :pswitch_11
    mul-int v8, v5, v6

    new-array v10, v8, [I

    .line 333
    .restart local v10    # "texture":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f02052b

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 334
    .restart local v23    # "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    move-object v12, v2

    move-object v13, v4

    move-object v14, v10

    move-object v15, v3

    move/from16 v16, v5

    move/from16 v17, v6

    move-object/from16 v18, v7

    .line 335
    invoke-static/range {v12 .. v18}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySketchTexturePartial([I[I[I[BIILandroid/graphics/Rect;)I

    .line 336
    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 337
    const/4 v10, 0x0

    .line 338
    goto/16 :goto_0

    .line 340
    .end local v10    # "texture":[I
    .end local v23    # "bitmap":Landroid/graphics/Bitmap;
    :pswitch_12
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyCartoonizePartial([I[B[IIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 343
    :pswitch_13
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyInvert([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 346
    :pswitch_14
    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyPopArt([I[III)V

    goto/16 :goto_0

    .line 349
    :pswitch_15
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyYellowglowPartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 352
    :pswitch_16
    mul-int v8, v5, v6

    new-array v10, v8, [I

    .line 353
    .restart local v10    # "texture":[I
    mul-int v8, v5, v6

    new-array v11, v8, [I

    .line 354
    .restart local v11    # "texture2":[I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0201ca

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v23

    .line 355
    .restart local v23    # "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 356
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0201cb

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v24

    .line 357
    .restart local v24    # "bitmap2":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v10, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 358
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v11, v5, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    move-object v8, v2

    move-object v9, v4

    move-object v12, v3

    move v13, v5

    move v14, v6

    move-object v15, v7

    .line 359
    invoke-static/range {v8 .. v15}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyDawnCastPartial([I[I[I[I[BIILandroid/graphics/Rect;)V

    .line 360
    invoke-static/range {v23 .. v23}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 361
    invoke-static/range {v24 .. v24}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 362
    const/4 v10, 0x0

    .line 363
    const/4 v11, 0x0

    .line 364
    goto/16 :goto_0

    .line 366
    .end local v10    # "texture":[I
    .end local v11    # "texture2":[I
    .end local v23    # "bitmap":Landroid/graphics/Bitmap;
    .end local v24    # "bitmap2":Landroid/graphics/Bitmap;
    :pswitch_17
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mStep:I

    move/from16 v17, v0

    move-object v12, v2

    move-object v13, v3

    move-object v14, v4

    move v15, v5

    move/from16 v16, v6

    move-object/from16 v18, v7

    invoke-static/range {v12 .. v18}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyPosterizePartial([I[B[IIIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    :pswitch_18
    move-object v12, v2

    move-object v13, v4

    move-object v14, v3

    move v15, v5

    move/from16 v16, v6

    move-object/from16 v17, v7

    .line 369
    invoke-static/range {v12 .. v17}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyGothicNoirPartial([I[I[BIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 372
    :pswitch_19
    invoke-static/range {v2 .. v7}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyMagicPenPartial([I[B[IIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 255
    :pswitch_data_0
    .packed-switch 0x16001600
        :pswitch_1
        :pswitch_3
        :pswitch_8
        :pswitch_9
        :pswitch_4
        :pswitch_5
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_2
        :pswitch_10
        :pswitch_12
        :pswitch_13
        :pswitch_11
        :pswitch_14
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method public applyPreview()I
    .locals 15

    .prologue
    const/16 v11, 0x32

    const/4 v7, 0x0

    .line 409
    const/4 v14, -0x1

    .line 410
    .local v14, "ret":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v6, :cond_2

    .line 413
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 414
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskBuffer()[B

    move-result-object v1

    .line 415
    .local v1, "previewMask":[B
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 416
    .local v0, "previewInput":[I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v2

    .line 418
    .local v2, "previewOutput":[I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 419
    .local v3, "previewWidth":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    .line 421
    .local v4, "previewHeight":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewMaskRoi()Landroid/graphics/Rect;

    move-result-object v5

    .line 422
    .local v5, "r":Landroid/graphics/Rect;
    invoke-virtual {v5}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 423
    invoke-virtual {v5, v3, v4, v7, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 424
    :cond_0
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    .line 426
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mEffectType:I

    packed-switch v6, :pswitch_data_0

    .line 506
    :goto_0
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v6, :cond_1

    .line 507
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    const/4 v7, 0x1

    invoke-interface {v6, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    .line 508
    :cond_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->afterApplyPreview()V

    .line 509
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "applyPreview preview roi:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 512
    .end local v0    # "previewInput":[I
    .end local v1    # "previewMask":[B
    .end local v2    # "previewOutput":[I
    .end local v3    # "previewWidth":I
    .end local v4    # "previewHeight":I
    .end local v5    # "r":Landroid/graphics/Rect;
    :cond_2
    return v14

    .line 428
    .restart local v0    # "previewInput":[I
    .restart local v1    # "previewMask":[B
    .restart local v2    # "previewOutput":[I
    .restart local v3    # "previewWidth":I
    .restart local v4    # "previewHeight":I
    .restart local v5    # "r":Landroid/graphics/Rect;
    :pswitch_1
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyVignettePartial([I[B[IIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 431
    :pswitch_2
    const-string v6, "bigheadk, applyPreview"

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 432
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyTurquoisePartial([I[B[IIILandroid/graphics/Rect;)V

    .line 433
    const-string v6, "bigheadk, applyPreview done"

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 436
    :pswitch_3
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyVintagePartial([I[B[IIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 439
    :pswitch_4
    const/16 v11, 0xa

    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move v9, v3

    move v10, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlGreyscale([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 442
    :pswitch_5
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySepiaPartial([I[B[IIILandroid/graphics/Rect;)I

    goto :goto_0

    .line 445
    :pswitch_6
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyDownlightPartial([I[B[IIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 448
    :pswitch_7
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyBluewashPartial([I[B[IIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 451
    :pswitch_8
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyNostalgiaPartial([I[B[IIILandroid/graphics/Rect;)V

    goto :goto_0

    :pswitch_9
    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move v9, v3

    move v10, v4

    move-object v12, v5

    .line 454
    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->controlFadedColour([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 457
    :pswitch_a
    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mStep:I

    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move v9, v3

    move v10, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySharpenPartial([I[B[IIIILandroid/graphics/Rect;)I

    goto :goto_0

    :pswitch_b
    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move v9, v3

    move v10, v4

    move-object v12, v5

    .line 460
    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySoftglowPartial([I[B[IIIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 463
    :pswitch_c
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyRainbowPartial([I[B[IIILandroid/graphics/Rect;)V

    goto :goto_0

    .line 466
    :pswitch_d
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    move-object v6, v0

    move-object v7, v2

    move-object v9, v1

    move v10, v3

    move v11, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyStardustPartial([I[I[I[BIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 469
    :pswitch_e
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    move-object v6, v0

    move-object v7, v2

    move-object v9, v1

    move v10, v3

    move v11, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyLightFlarePartial([I[I[I[BIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 472
    :pswitch_f
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    move-object v6, v0

    move-object v7, v2

    move-object v9, v1

    move v10, v3

    move v11, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyLightStreakPartial([I[I[I[BIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 475
    :pswitch_10
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview2:[I

    move-object v6, v0

    move-object v7, v2

    move-object v10, v1

    move v11, v3

    move v12, v4

    move-object v13, v5

    invoke-static/range {v6 .. v13}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyImpressionistPartial([I[I[I[I[BIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 478
    :pswitch_11
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyCartoonizePartial([I[B[IIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 481
    :pswitch_12
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "negative r:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 482
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyInvert([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 485
    :pswitch_13
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    move-object v6, v0

    move-object v7, v2

    move-object v9, v1

    move v10, v3

    move v11, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applySketchTexturePartial([I[I[I[BIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 488
    :pswitch_14
    invoke-static {v0, v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyPopArt([I[III)V

    goto/16 :goto_0

    .line 491
    :pswitch_15
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyYellowglowPartial([I[B[IIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 494
    :pswitch_16
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview2:[I

    move-object v6, v0

    move-object v7, v2

    move-object v10, v1

    move v11, v3

    move v12, v4

    move-object v13, v5

    invoke-static/range {v6 .. v13}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyDawnCastPartial([I[I[I[I[BIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 497
    :pswitch_17
    iget v11, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mStep:I

    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move v9, v3

    move v10, v4

    move-object v12, v5

    invoke-static/range {v6 .. v12}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyPosterizePartial([I[B[IIIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    :pswitch_18
    move-object v6, v0

    move-object v7, v2

    move-object v8, v1

    move v9, v3

    move v10, v4

    move-object v11, v5

    .line 500
    invoke-static/range {v6 .. v11}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyGothicNoirPartial([I[I[BIILandroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 503
    :pswitch_19
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->applyMagicPenPartial([I[B[IIILandroid/graphics/Rect;)I

    goto/16 :goto_0

    .line 426
    :pswitch_data_0
    .packed-switch 0x16001600
        :pswitch_1
        :pswitch_3
        :pswitch_8
        :pswitch_9
        :pswitch_4
        :pswitch_5
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_2
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method public blending()V
    .locals 12

    .prologue
    .line 525
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 526
    .local v0, "ret":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 527
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 526
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 528
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 529
    .local v1, "canvas":Landroid/graphics/Canvas;
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    .line 530
    .local v10, "paint":Landroid/graphics/Paint;
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mMaskAlpha:I

    invoke-virtual {v10, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 531
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 532
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v8}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v8

    const/4 v9, 0x1

    .line 531
    invoke-virtual/range {v1 .. v10}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 534
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v3

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 535
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v8

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v9

    move-object v2, v0

    .line 534
    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 537
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 543
    .end local v0    # "ret":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v10    # "paint":Landroid/graphics/Paint;
    :goto_0
    return-void

    .line 538
    :catch_0
    move-exception v11

    .line 540
    .local v11, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v11}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method public configurationChanged()V
    .locals 0

    .prologue
    .line 402
    return-void
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V
    .locals 1
    .param p1, "effectEffect"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;

    .prologue
    .line 60
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 61
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mEffectType:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mEffectType:I

    .line 62
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mMaskAlpha:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mMaskAlpha:I

    .line 63
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    .line 64
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 67
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 68
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    .line 71
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview2:[I

    .line 72
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    .line 73
    return-void
.end method

.method public getEffectType()I
    .locals 1

    .prologue
    .line 405
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mEffectType:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 572
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMaskAlpha()I
    .locals 1

    .prologue
    .line 516
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mMaskAlpha:I

    return v0
.end method

.method public getMutexOn()Z
    .locals 1

    .prologue
    .line 562
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mutexOn:Z

    return v0
.end method

.method public getSphericityMaxSeekbar()I
    .locals 1

    .prologue
    .line 558
    const/16 v0, 0x8c

    return v0
.end method

.method public getTexture(III)[I
    .locals 8
    .param p1, "effectType"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const v5, 0x7f02054d

    const v4, 0x7f0201c8

    const/16 v7, 0x12c

    .line 77
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 79
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    if-nez v6, :cond_0

    .line 80
    const/4 v3, 0x0

    .line 140
    :goto_0
    return-object v3

    .line 81
    :cond_0
    mul-int v6, p2, p3

    new-array v3, v6, [I

    .line 82
    .local v3, "texturePreview":[I
    const/4 v1, 0x0

    .line 83
    .local v1, "texture":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    .line 85
    .local v2, "textureID":I
    packed-switch p1, :pswitch_data_0

    .line 126
    :goto_1
    :pswitch_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_1

    .line 127
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 135
    :cond_1
    :goto_2
    if-eqz v1, :cond_2

    .line 136
    invoke-direct {p0, v1, v3, p2, p3}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 137
    :cond_2
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 138
    const/4 v1, 0x0

    .line 139
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_0

    .line 89
    :pswitch_1
    if-lt p2, v7, :cond_3

    if-ge p3, v7, :cond_4

    :cond_3
    move v2, v5

    .line 90
    :goto_3
    goto :goto_1

    :cond_4
    move v2, v4

    .line 89
    goto :goto_3

    .line 93
    :pswitch_2
    if-lt p2, v7, :cond_5

    if-ge p3, v7, :cond_6

    :cond_5
    move v2, v5

    .line 94
    :goto_4
    const v2, 0x7f020551

    .line 95
    goto :goto_1

    :cond_6
    move v2, v4

    .line 93
    goto :goto_4

    .line 98
    :pswitch_3
    if-lt p2, v7, :cond_7

    if-ge p3, v7, :cond_8

    :cond_7
    const v2, 0x7f020550

    .line 99
    :goto_5
    goto :goto_1

    .line 98
    :cond_8
    const v2, 0x7f020555

    goto :goto_5

    .line 102
    :pswitch_4
    if-lt p2, v7, :cond_9

    if-ge p3, v7, :cond_a

    :cond_9
    const v2, 0x7f020552

    .line 103
    :goto_6
    goto :goto_1

    .line 102
    :cond_a
    const v2, 0x7f0200d5

    goto :goto_6

    .line 106
    :pswitch_5
    if-lt p2, v7, :cond_b

    if-ge p3, v7, :cond_c

    :cond_b
    const v2, 0x7f02054e

    .line 107
    :goto_7
    goto :goto_1

    .line 106
    :cond_c
    const v2, 0x7f0201c7

    goto :goto_7

    .line 110
    :pswitch_6
    if-lt p2, v7, :cond_d

    if-ge p3, v7, :cond_e

    :cond_d
    const v2, 0x7f02054f

    .line 111
    :goto_8
    goto :goto_1

    .line 110
    :cond_e
    const v2, 0x7f0201c9

    goto :goto_8

    .line 114
    :pswitch_7
    if-lt p2, v7, :cond_f

    if-ge p3, v7, :cond_10

    :cond_f
    const v2, 0x7f020553

    .line 115
    :goto_9
    goto :goto_1

    .line 114
    :cond_10
    const v2, 0x7f0201ca

    goto :goto_9

    .line 118
    :pswitch_8
    if-lt p2, v7, :cond_11

    if-ge p3, v7, :cond_12

    :cond_11
    const v2, 0x7f020554

    :goto_a
    goto :goto_1

    :cond_12
    const v2, 0x7f0201cb

    goto :goto_a

    .line 129
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_2

    .line 85
    nop

    :pswitch_data_0
    .packed-switch 0x16001607
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 567
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(I)V
    .locals 7
    .param p1, "effectType"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 145
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 228
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-static {p1}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 155
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 156
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mStartInitializing:Z

    .line 158
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    if-eqz v3, :cond_1

    .line 159
    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    .line 160
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    mul-int/2addr v3, v4

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    .line 161
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    invoke-static {v3, v6}, Ljava/util/Arrays;->fill([II)V

    .line 163
    :cond_1
    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview2:[I

    .line 164
    const/4 v1, 0x0

    .line 168
    .local v1, "texture":Landroid/graphics/Bitmap;
    sparse-switch p1, :sswitch_data_0

    .line 204
    :goto_1
    if-eqz v1, :cond_2

    .line 205
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview:[I

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    invoke-direct {p0, v1, v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 206
    :cond_2
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 207
    const/4 v1, 0x0

    .line 208
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 210
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v3, :cond_3

    .line 212
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v3, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    .line 214
    :cond_3
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mEffectType:I

    .line 215
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_4

    .line 217
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->applyPreview()I

    .line 219
    :cond_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$1;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 227
    iput-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mStartInitializing:Z

    goto :goto_0

    .line 171
    :sswitch_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0201c8

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 172
    goto :goto_1

    .line 174
    :sswitch_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02052b

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 175
    goto :goto_1

    .line 177
    :sswitch_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    mul-int/2addr v3, v4

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview2:[I

    .line 178
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200d5

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 179
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020555

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 180
    .local v2, "texture1":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview2:[I

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 181
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 182
    const/4 v2, 0x0

    .line 183
    goto/16 :goto_1

    .line 185
    .end local v2    # "texture1":Landroid/graphics/Bitmap;
    :sswitch_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0201c7

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 186
    goto/16 :goto_1

    .line 188
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0201c9

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 189
    goto/16 :goto_1

    .line 191
    :sswitch_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v4

    mul-int/2addr v3, v4

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview2:[I

    .line 192
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0201ca

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 193
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0201cb

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 194
    .restart local v2    # "texture1":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mTexturePreview2:[I

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->makeScaledIntBuffer(Landroid/graphics/Bitmap;[III)V

    .line 195
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 200
    .end local v2    # "texture1":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto/16 :goto_1

    .line 168
    :sswitch_data_0
    .sparse-switch
        0x16001607 -> :sswitch_0
        0x16001608 -> :sswitch_3
        0x16001609 -> :sswitch_4
        0x1600160f -> :sswitch_2
        0x16001612 -> :sswitch_1
        0x16001616 -> :sswitch_5
    .end sparse-switch
.end method

.method public isStartInitializing()Z
    .locals 1

    .prologue
    .line 554
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mStartInitializing:Z

    return v0
.end method

.method public setMaskAlpha(I)V
    .locals 2
    .param p1, "progress"    # I

    .prologue
    .line 520
    int-to-float v0, p1

    const v1, 0x40233333    # 2.55f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->mMaskAlpha:I

    .line 521
    return-void
.end method

.method public setOnActionbarCallback(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .prologue
    .line 550
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .line 551
    return-void
.end method

.method public setOnCallback(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .prologue
    .line 546
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .line 547
    return-void
.end method
