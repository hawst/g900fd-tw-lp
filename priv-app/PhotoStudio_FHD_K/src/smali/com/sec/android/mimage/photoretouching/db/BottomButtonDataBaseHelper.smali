.class public Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;
.super Ljava/lang/Object;
.source "BottomButtonDataBaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;,
        Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_VERSION:I = 0x1

.field public static final MAX_RECENT_COLOR_EFFECT:I = 0x3

.field public static final MAX_RECENT_LABEL:I = 0xa

.field public static final MAX_RECENT_OTHER:I = 0x6

.field public static final MAX_RECENT_STICKER:I = 0x10

.field public static mDB:Landroid/database/sqlite/SQLiteDatabase;

.field public static mEffectDB:Landroid/database/sqlite/SQLiteDatabase;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;

.field private mEffectDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mContext:Landroid/content/Context;

    .line 30
    return-void
.end method

.method public static copyDBFromAssets(Landroid/content/Context;Ljava/lang/String;)V
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dbName"    # Ljava/lang/String;

    .prologue
    .line 152
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v13

    .line 153
    .local v13, "manager":Landroid/content/res/AssetManager;
    const-string v9, "data/data/com.sec.android.mimage.photoretouching/databases"

    .line 154
    .local v9, "folderPath":Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "data/data/com.sec.android.mimage.photoretouching/databases/"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 156
    .local v7, "filePath":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 157
    .local v8, "folder":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 158
    .local v6, "file":Ljava/io/File;
    const/4 v10, 0x0

    .line 159
    .local v10, "fos":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .line 162
    .local v2, "bos":Ljava/io/BufferedOutputStream;
    :try_start_0
    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "db/"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v12

    .line 163
    .local v12, "is":Ljava/io/InputStream;
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v12}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 164
    .local v1, "bis":Ljava/io/BufferedInputStream;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v15

    if-nez v15, :cond_0

    .line 165
    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    .line 166
    :cond_0
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v15

    if-nez v15, :cond_1

    .line 168
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 169
    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z

    .line 172
    :cond_1
    new-instance v11, Ljava/io/FileOutputStream;

    invoke-direct {v11, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 173
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .local v11, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v3, Ljava/io/BufferedOutputStream;

    invoke-direct {v3, v11}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 174
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .local v3, "bos":Ljava/io/BufferedOutputStream;
    const/4 v14, -0x1

    .line 175
    .local v14, "read":I
    const/16 v15, 0x400

    :try_start_2
    new-array v4, v15, [B

    .line 177
    .local v4, "buffer":[B
    :goto_0
    const/4 v15, 0x0

    const/16 v16, 0x400

    move/from16 v0, v16

    invoke-virtual {v1, v4, v15, v0}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_2

    .line 182
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->flush()V

    .line 183
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V

    .line 184
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V

    .line 185
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 186
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V

    move-object v2, v3

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .line 191
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v4    # "buffer":[B
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v14    # "read":I
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    :goto_1
    return-void

    .line 179
    .end local v2    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "buffer":[B
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "is":Ljava/io/InputStream;
    .restart local v14    # "read":I
    :cond_2
    const/4 v15, 0x0

    invoke-virtual {v3, v4, v15, v14}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 187
    .end local v4    # "buffer":[B
    :catch_0
    move-exception v5

    move-object v2, v3

    .end local v3    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .line 189
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v14    # "read":I
    .local v5, "e":Ljava/io/IOException;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    :goto_2
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 187
    .end local v5    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v5

    goto :goto_2

    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "is":Ljava/io/InputStream;
    :catch_2
    move-exception v5

    move-object v10, v11

    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method public static copyDBFromAssets(Landroid/content/Context;Z)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "forEffectManager"    # Z

    .prologue
    .line 132
    if-eqz p1, :cond_0

    .line 133
    const-string v0, "effect_button_table.db"

    invoke-static {p0, v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->copyDBFromAssets(Landroid/content/Context;Ljava/lang/String;)V

    .line 137
    :goto_0
    return-void

    .line 135
    :cond_0
    const-string v0, "common_button_table.db"

    invoke-static {p0, v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->copyDBFromAssets(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private insertButtonInfo(IIZ)V
    .locals 22
    .param p1, "buttonId"    # I
    .param p2, "buttonType"    # I
    .param p3, "forEffectManager"    # Z

    .prologue
    .line 195
    const/high16 v4, -0x10000

    and-int v4, v4, p1

    move/from16 v0, p2

    if-ne v4, v0, :cond_3

    .line 197
    const/4 v12, 0x0

    .line 198
    .local v12, "cs":Landroid/database/Cursor;
    if-eqz p3, :cond_4

    .line 199
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryEffectButton(I)Landroid/database/Cursor;

    move-result-object v12

    .line 202
    :goto_0
    const/16 v18, 0x0

    .line 203
    .local v18, "maxCnt":I
    const/high16 v4, -0x10000

    and-int v4, v4, p1

    const/high16 v5, 0x15000000

    if-eq v4, v5, :cond_0

    const/high16 v4, -0x10000

    and-int v4, v4, p1

    const/high16 v5, 0x16000000

    if-ne v4, v5, :cond_5

    .line 204
    :cond_0
    const/16 v18, 0x3

    .line 214
    :goto_1
    if-eqz v12, :cond_2

    .line 216
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v11

    .line 217
    .local v11, "cnt":I
    if-nez v11, :cond_9

    .line 219
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x1

    move-object/from16 v4, p0

    move/from16 v5, p1

    move/from16 v6, p2

    move/from16 v10, p3

    invoke-direct/range {v4 .. v10}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRow(IIIZZZ)V

    .line 272
    :cond_1
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "JW insertButtonInfo: cnt="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 274
    .end local v11    # "cnt":I
    :cond_2
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 276
    .end local v12    # "cs":Landroid/database/Cursor;
    .end local v18    # "maxCnt":I
    :cond_3
    return-void

    .line 201
    .restart local v12    # "cs":Landroid/database/Cursor;
    :cond_4
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryButtonType(I)Landroid/database/Cursor;

    move-result-object v12

    goto :goto_0

    .line 206
    .restart local v18    # "maxCnt":I
    :cond_5
    const/high16 v4, 0x31100000

    move/from16 v0, p2

    if-eq v0, v4, :cond_6

    const/high16 v4, 0x31200000

    move/from16 v0, p2

    if-ne v0, v4, :cond_7

    .line 207
    :cond_6
    const/16 v18, 0x10

    goto :goto_1

    .line 208
    :cond_7
    const/high16 v4, 0x31300000

    move/from16 v0, p2

    if-ne v0, v4, :cond_8

    .line 209
    const/16 v18, 0xa

    goto :goto_1

    .line 211
    :cond_8
    const/16 v18, 0x6

    goto :goto_1

    .line 221
    .restart local v11    # "cnt":I
    :cond_9
    move/from16 v0, v18

    if-lt v11, v0, :cond_c

    if-nez p3, :cond_c

    .line 223
    const/16 v21, -0x1

    .line 224
    .local v21, "tempId":I
    const/16 v19, 0x270f

    .line 226
    .local v19, "old":I
    :cond_a
    const-string v4, "button_id"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 227
    .local v15, "id":I
    const-string v4, "button_idx"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 228
    .local v16, "idx":I
    move/from16 v0, v19

    move/from16 v1, v16

    if-le v0, v1, :cond_b

    .line 230
    move/from16 v19, v16

    .line 231
    move/from16 v21, v15

    .line 233
    :cond_b
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_a

    .line 235
    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->deleteButtonInfo(IIZ)V

    .line 237
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v13

    .line 238
    .local v13, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "update common_button_table set button_idx = button_idx - 1  where effect_type = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 240
    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 241
    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "button_idx"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " > "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 238
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 242
    .local v20, "query":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "JW insert query="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 245
    :try_start_0
    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    :goto_3
    add-int/lit8 v7, v11, -0x1

    const/4 v8, 0x1

    const/4 v9, 0x1

    move-object/from16 v4, p0

    move/from16 v5, p1

    move/from16 v6, p2

    move/from16 v10, p3

    invoke-direct/range {v4 .. v10}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRow(IIIZZZ)V

    goto/16 :goto_2

    .line 246
    :catch_0
    move-exception v14

    .line 247
    .local v14, "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    goto :goto_3

    .line 257
    .end local v13    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v14    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    .end local v15    # "id":I
    .end local v16    # "idx":I
    .end local v19    # "old":I
    .end local v20    # "query":Ljava/lang/String;
    .end local v21    # "tempId":I
    :cond_c
    const/16 v17, 0x0

    .line 259
    .local v17, "isThere":Z
    :cond_d
    const-string v4, "button_id"

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 261
    .restart local v15    # "id":I
    move/from16 v0, p1

    if-ne v0, v15, :cond_e

    .line 263
    const/16 v17, 0x1

    .line 267
    :goto_4
    if-nez v17, :cond_1

    .line 269
    const/4 v8, 0x1

    const/4 v9, 0x1

    move-object/from16 v4, p0

    move/from16 v5, p1

    move/from16 v6, p2

    move v7, v11

    move/from16 v10, p3

    invoke-direct/range {v4 .. v10}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertRow(IIIZZZ)V

    goto/16 :goto_2

    .line 266
    :cond_e
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_d

    goto :goto_4
.end method

.method private insertRow(IIIZZZ)V
    .locals 6
    .param p1, "buttonId"    # I
    .param p2, "effectType"    # I
    .param p3, "buttonIdx"    # I
    .param p4, "recentUsed"    # Z
    .param p5, "use"    # Z
    .param p6, "forEffectManager"    # Z

    .prologue
    .line 466
    const/4 v2, 0x0

    .local v2, "recent_values":I
    const/4 v3, 0x0

    .line 467
    .local v3, "use_values":I
    if-eqz p4, :cond_0

    .line 468
    const/4 v2, 0x1

    .line 469
    :cond_0
    if-eqz p5, :cond_1

    .line 470
    const/4 v3, 0x1

    .line 471
    :cond_1
    const/4 v1, 0x0

    .line 472
    .local v1, "query":Ljava/lang/String;
    if-eqz p6, :cond_3

    .line 474
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "insert into effect_button_table(button_id, effect_type, button_idx, use) values("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 480
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 481
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 482
    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 483
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 484
    const-string v5, ");"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 474
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 487
    :try_start_0
    sget-object v4, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mEffectDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v4, :cond_2

    .line 488
    sget-object v4, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mEffectDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 518
    :cond_2
    :goto_0
    return-void

    .line 489
    :catch_0
    move-exception v0

    .line 490
    .local v0, "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    goto :goto_0

    .line 496
    .end local v0    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "insert into common_button_table(button_id, effect_type, button_idx, recent_used, use) values("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 503
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 504
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 505
    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 506
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 507
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 508
    const-string v5, ");"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 496
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 511
    :try_start_1
    sget-object v4, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v4, :cond_2

    .line 512
    sget-object v4, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 513
    :catch_1
    move-exception v0

    .line 514
    .restart local v0    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isCheckDB(Ljava/lang/String;)Z
    .locals 5
    .param p0, "dbName"    # Ljava/lang/String;

    .prologue
    .line 140
    const/4 v2, 0x0

    .line 141
    .local v2, "ret":Z
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "data/data/com.sec.android.mimage.photoretouching/databases/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 143
    .local v1, "filePath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 144
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 146
    const/4 v2, 0x1

    .line 148
    :cond_0
    return v2
.end method

.method public static isCheckDB(Z)Z
    .locals 1
    .param p0, "forEffectManager"    # Z

    .prologue
    .line 124
    if-eqz p0, :cond_0

    .line 125
    const-string v0, "effect_button_table.db"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->isCheckDB(Ljava/lang/String;)Z

    move-result v0

    .line 127
    :goto_0
    return v0

    :cond_0
    const-string v0, "common_button_table.db"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->isCheckDB(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private queryButtonId(I)Landroid/database/Cursor;
    .locals 5
    .param p1, "buttonId"    # I

    .prologue
    .line 400
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 401
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "select * from common_button_table where button_id = \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 403
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 404
    const-string v4, " order by "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "button_idx"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 405
    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 401
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 406
    .local v2, "query":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 407
    .local v0, "cs":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 409
    return-object v0
.end method

.method private queryButtonType(I)Landroid/database/Cursor;
    .locals 5
    .param p1, "buttonType"    # I

    .prologue
    .line 453
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 454
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "select * from common_button_table where effect_type = \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 456
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 457
    const-string v4, " order by "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "button_idx"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 458
    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 454
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 459
    .local v2, "query":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 460
    .local v0, "cs":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 462
    return-object v0
.end method

.method private queryEffectButton(I)Landroid/database/Cursor;
    .locals 5
    .param p1, "buttonType"    # I

    .prologue
    .line 436
    const/4 v0, 0x0

    .line 437
    .local v0, "cs":Landroid/database/Cursor;
    const/high16 v3, 0x16000000

    if-ne p1, v3, :cond_0

    .line 439
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mEffectDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 440
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "select * from effect_button_table where effect_type = \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 442
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 443
    const-string v4, " order by "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "button_idx"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 444
    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 440
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 445
    .local v2, "query":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 446
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 449
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "query":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private queryRecentButton(I)Landroid/database/Cursor;
    .locals 6
    .param p1, "buttonType"    # I

    .prologue
    .line 413
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 414
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "select * from common_button_table where effect_type = \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 416
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 417
    const-string v5, "recent_used"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 418
    const-string v5, " = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 419
    const-string v5, " order by "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "button_idx"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 420
    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 414
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 423
    .local v3, "query":Ljava/lang/String;
    const/4 v0, 0x0

    .line 425
    .local v0, "cs":Landroid/database/Cursor;
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 426
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Landroid/database/sqlite/DatabaseObjectNotClosedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 432
    :goto_0
    return-object v0

    .line 427
    :catch_0
    move-exception v2

    .line 428
    .local v2, "e":Landroid/database/sqlite/DatabaseObjectNotClosedException;
    invoke-virtual {v2}, Landroid/database/sqlite/DatabaseObjectNotClosedException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    sget-object v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 46
    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mEffectDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mEffectDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    sget-object v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mEffectDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 48
    :cond_1
    return-void
.end method

.method public deleteButtonInfo(IIZ)V
    .locals 5
    .param p1, "buttonId"    # I
    .param p2, "buttonType"    # I
    .param p3, "forEffectManager"    # Z

    .prologue
    .line 279
    const/4 v0, 0x0

    .line 280
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x0

    .line 281
    .local v2, "query":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 283
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mEffectDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 284
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "delete from effect_button_table where button_id = \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 286
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 287
    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 284
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 299
    :goto_0
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    :goto_1
    return-void

    .line 291
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 292
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "delete from common_button_table where button_id = \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 294
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 295
    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 292
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 300
    :catch_0
    move-exception v1

    .line 301
    .local v1, "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    goto :goto_1
.end method

.method public insertEffectButtonInfo(I)V
    .locals 2
    .param p1, "buttonId"    # I

    .prologue
    .line 88
    const/high16 v0, 0x16000000

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertButtonInfo(IIZ)V

    .line 89
    return-void
.end method

.method public insertRecentColorButtonInfo(I)V
    .locals 2
    .param p1, "buttonId"    # I

    .prologue
    .line 92
    const/high16 v0, 0x15000000

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertButtonInfo(IIZ)V

    .line 93
    return-void
.end method

.method public insertRecentEffectButtonInfo(I)V
    .locals 2
    .param p1, "buttonId"    # I

    .prologue
    .line 96
    const/high16 v0, 0x16000000

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertButtonInfo(IIZ)V

    .line 97
    return-void
.end method

.method public insertRecentFrameButtonInfo(I)V
    .locals 2
    .param p1, "buttonId"    # I

    .prologue
    .line 104
    const/high16 v0, 0x31200000

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertButtonInfo(IIZ)V

    .line 105
    return-void
.end method

.method public insertRecentLabelButtonInfo(I)V
    .locals 2
    .param p1, "buttonId"    # I

    .prologue
    .line 108
    const/high16 v0, 0x31300000

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertButtonInfo(IIZ)V

    .line 109
    return-void
.end method

.method public insertRecentPortraitButtonInfo(I)V
    .locals 2
    .param p1, "buttonId"    # I

    .prologue
    .line 100
    const/high16 v0, 0x18000000

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertButtonInfo(IIZ)V

    .line 101
    return-void
.end method

.method public insertRecentStampButtonInfo(I)V
    .locals 2
    .param p1, "buttonId"    # I

    .prologue
    .line 116
    const/high16 v0, 0x31500000

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertButtonInfo(IIZ)V

    .line 117
    return-void
.end method

.method public insertRecentStickerButtonInfo(I)V
    .locals 2
    .param p1, "buttonId"    # I

    .prologue
    .line 120
    const/high16 v0, 0x31100000

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertButtonInfo(IIZ)V

    .line 121
    return-void
.end method

.method public insertRecentWaterMarkButtonInfo(I)V
    .locals 2
    .param p1, "buttonId"    # I

    .prologue
    .line 112
    const/high16 v0, 0x31400000

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->insertButtonInfo(IIZ)V

    .line 113
    return-void
.end method

.method public open()Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 33
    new-instance v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mContext:Landroid/content/Context;

    const-string v3, "common_button_table.db"

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;-><init>(Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;

    .line 34
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 35
    return-object p0
.end method

.method public openEffect()Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mContext:Landroid/content/Context;

    const-string v3, "effect_button_table.db"

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;-><init>(Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mEffectDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;

    .line 39
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mEffectDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sput-object v0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mEffectDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 40
    return-object p0
.end method

.method public queryEffectButtonInfo()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 52
    const/high16 v0, 0x16000000

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryEffectButton(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public queryRecentColorButtonInfo()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 60
    const/high16 v0, 0x15000000

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentButton(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public queryRecentEffectButtonInfo()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 56
    const/high16 v0, 0x16000000

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentButton(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public queryRecentFrameButtonInfo()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 72
    const/high16 v0, 0x31200000

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentButton(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public queryRecentLabelButtonInfo()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 76
    const/high16 v0, 0x31300000

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentButton(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public queryRecentPortraitButtonInfo()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 64
    const/high16 v0, 0x18000000

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentButton(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public queryRecentStampButtonInfo()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 84
    const/high16 v0, 0x31500000

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentButton(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public queryRecentStrickerButtonInfo()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 68
    const/high16 v0, 0x31100000

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentButton(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public queryRecentWatermarkButtonInfo()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 80
    const/high16 v0, 0x31400000

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryRecentButton(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public updateButtonInfo(IIIIZ)V
    .locals 6
    .param p1, "buttonId"    # I
    .param p2, "buttonIdx"    # I
    .param p3, "recent"    # I
    .param p4, "use"    # I
    .param p5, "forEffectManager"    # Z

    .prologue
    const/4 v5, 0x0

    .line 307
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 308
    .local v1, "values":Landroid/content/ContentValues;
    const/4 v0, 0x0

    .line 309
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz p5, :cond_0

    .line 311
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mEffectDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 312
    const-string v2, "button_idx"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 313
    const-string v2, "use"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 315
    const-string v2, "effect_button_table"

    .line 317
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "button_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 315
    invoke-virtual {v0, v2, v1, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 332
    :goto_0
    return-void

    .line 322
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 323
    const-string v2, "button_idx"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 324
    const-string v2, "recent_used"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 325
    const-string v2, "use"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 327
    const-string v2, "common_button_table"

    .line 329
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "button_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 327
    invoke-virtual {v0, v2, v1, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateButtonInfoInButtonType(IIZ)V
    .locals 13
    .param p1, "buttonType"    # I
    .param p2, "buttonId"    # I
    .param p3, "forEffectManager"    # Z

    .prologue
    .line 336
    const/high16 v0, -0x10000

    and-int/2addr v0, p2

    if-ne v0, p1, :cond_2

    .line 338
    const/4 v6, 0x0

    .line 339
    .local v6, "cs":Landroid/database/Cursor;
    if-eqz p3, :cond_3

    .line 340
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryEffectButton(I)Landroid/database/Cursor;

    move-result-object v6

    .line 344
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 345
    const/4 v9, -0x1

    .line 346
    .local v9, "id":I
    const/4 v10, -0x1

    .line 347
    .local v10, "idx":I
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v12

    .line 348
    .local v12, "size":I
    if-lez v12, :cond_1

    .line 351
    :cond_0
    const-string v0, "button_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 353
    if-ne v9, p2, :cond_4

    .line 355
    const-string v0, "button_idx"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 360
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 362
    :cond_1
    const/4 v0, -0x1

    if-eq v10, v0, :cond_2

    .line 364
    if-ltz v10, :cond_2

    .line 366
    const/4 v7, 0x0

    .line 367
    .local v7, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v11, 0x0

    .line 368
    .local v11, "query":Ljava/lang/String;
    if-eqz p3, :cond_5

    .line 370
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mEffectDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$EffectDataBaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 371
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "update effect_button_table set button_idx = button_idx - 1  where effect_type = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 373
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 374
    const-string v1, " and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "button_idx"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " > "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 371
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 384
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW update query="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 387
    :try_start_0
    invoke-virtual {v7, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    :goto_3
    add-int/lit8 v2, v12, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x1

    move-object v0, p0

    move v1, p2

    move/from16 v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->updateButtonInfo(IIIIZ)V

    .line 397
    .end local v6    # "cs":Landroid/database/Cursor;
    .end local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v9    # "id":I
    .end local v10    # "idx":I
    .end local v11    # "query":Ljava/lang/String;
    .end local v12    # "size":I
    :cond_2
    return-void

    .line 342
    .restart local v6    # "cs":Landroid/database/Cursor;
    :cond_3
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->queryButtonType(I)Landroid/database/Cursor;

    move-result-object v6

    goto/16 :goto_0

    .line 359
    .restart local v9    # "id":I
    .restart local v10    # "idx":I
    .restart local v12    # "size":I
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 378
    .restart local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v11    # "query":Ljava/lang/String;
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;->mDBHelper:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "update common_button_table set button_idx = button_idx - 1  where effect_type = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 381
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 382
    const-string v1, " and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "button_idx"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " > "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 379
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_2

    .line 388
    :catch_0
    move-exception v8

    .line 389
    .local v8, "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    goto :goto_3
.end method
