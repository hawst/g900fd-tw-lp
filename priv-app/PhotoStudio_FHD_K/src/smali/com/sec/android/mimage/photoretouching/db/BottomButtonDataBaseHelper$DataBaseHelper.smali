.class Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "BottomButtonDataBaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataBaseHelper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "factory"    # Landroid/database/sqlite/SQLiteDatabase$CursorFactory;
    .param p5, "version"    # I

    .prologue
    .line 522
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;->this$0:Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper;

    .line 523
    invoke-direct {p0, p2, p3, p4, p5}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 525
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 531
    :try_start_0
    const-string v1, "create table common_button_table(button_id integer primary key , effect_type integer , button_idx integer , recent_used integer , use integer );"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 536
    :goto_0
    return-void

    .line 532
    :catch_0
    move-exception v0

    .line 533
    .local v0, "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    goto :goto_0
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    .line 542
    :try_start_0
    const-string v1, "DROP TABLE IF EXISTS common_button_table"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 547
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/db/BottomButtonDataBaseHelper$DataBaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 548
    return-void

    .line 543
    :catch_0
    move-exception v0

    .line 544
    .local v0, "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    goto :goto_0
.end method
