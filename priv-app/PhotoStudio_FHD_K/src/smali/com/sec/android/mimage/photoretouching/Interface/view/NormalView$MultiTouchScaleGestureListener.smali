.class public Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;
.super Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;
.source "NormalView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MultiTouchScaleGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V
    .locals 0

    .prologue
    .line 3014
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;)Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
    .locals 1

    .prologue
    .line 3014
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    return-object v0
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 3044
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Z)V

    .line 3045
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->isZoomMinRatio()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3046
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->setZoom(FF)V

    .line 3049
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 3048
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->resetZoom()V

    goto :goto_0
.end method

.method public onMultiTouchDown()Z
    .locals 1

    .prologue
    .line 3020
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;->onMultiTouchDown()Z

    move-result v0

    return v0
.end method

.method public onMultiTouchScale(FFF)Z
    .locals 1
    .param p1, "scalefactor"    # F
    .param p2, "focusX"    # F
    .param p3, "focusY"    # F

    .prologue
    .line 3032
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->setZoom(FFF)V

    .line 3033
    const/4 v0, 0x1

    return v0
.end method

.method public onMultiTouchScroll(FF)Z
    .locals 1
    .param p1, "distanceX"    # F
    .param p2, "distanceY"    # F

    .prologue
    .line 3026
    const/4 v0, 0x1

    return v0
.end method

.method public onMultiTouchUp()Z
    .locals 1

    .prologue
    .line 3038
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->endZoom()V

    .line 3039
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v2, 0x41200000    # 10.0f

    .line 3054
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDrawOriginal:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3056
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 3120
    :cond_0
    :goto_0
    return v4

    .line 3059
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    invoke-static {v0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Z)V

    .line 3060
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 3061
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mFirstPt:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Point;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 3064
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mFirstPt:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 3065
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mFirstPt:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 3066
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Z)V

    goto :goto_0

    .line 3069
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mShowTopBottomButtons:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3071
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener$1;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;)V

    .line 3087
    const-wide/16 v2, 0x78

    .line 3071
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 3115
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 3116
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$MultiTouchScaleGestureListener;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    invoke-static {v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Z)V

    goto :goto_0

    .line 3056
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
