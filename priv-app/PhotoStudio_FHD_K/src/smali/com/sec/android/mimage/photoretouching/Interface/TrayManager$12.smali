.class Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$12;
.super Ljava/lang/Object;
.source "TrayManager.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->addButton(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$DecodeInfo;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GestureLongPress(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 955
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mTrayTouchFunction:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$TrayTouchFunction;->onLongClick(Landroid/view/View;)Z

    .line 956
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 937
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mIsLongClick:Z
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 939
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->initCurrentButton(Landroid/view/View;)V
    invoke-static {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Landroid/view/View;)V

    .line 940
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 941
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->mCurrentButton:Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;)Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Gui/TrayButton;->getIndexFromTrayButtonList()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->changeImages(I)V

    .line 945
    :cond_0
    :goto_0
    return-void

    .line 943
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager$12;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->changeImages(I)V

    goto :goto_0
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 951
    return-void
.end method
