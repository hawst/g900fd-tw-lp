.class public Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "ImageStickerView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ResolverSetAsAdapter;
    }
.end annotation


# instance fields
.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentSaveSize:I

.field private mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

.field private mIsLongClicked:Z

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mOptionItemId:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mTemporaryOriginalBuffer:[I

.field private mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;

.field stickerCallback:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p6, "decoManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 87
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 215
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->stickerCallback:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

    .line 1664
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1666
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1667
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    .line 1668
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1669
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1670
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1671
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1672
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1673
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1674
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 1675
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;

    .line 1678
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    .line 1680
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1681
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mPaint:Landroid/graphics/Paint;

    .line 1686
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mIsLongClicked:Z

    .line 1688
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mOptionItemId:I

    .line 1690
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mCurrentSaveSize:I

    .line 1692
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 1693
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 1694
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 89
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    .line 90
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 91
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 92
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 93
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 94
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 96
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->setInterface(Ljava/lang/Object;)V

    .line 98
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mPaint:Landroid/graphics/Paint;

    .line 99
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 102
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 104
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->stickerCallback:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    .line 108
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->setViewLayerType(I)V

    .line 110
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;

    .line 112
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mTemporaryOriginalBuffer:[I

    .line 114
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->getImage()V

    .line 115
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;Z)V
    .locals 0

    .prologue
    .line 413
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->setVisibleTopBottomMenu(Z)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 1670
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .locals 1

    .prologue
    .line 1674
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 1668
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1667
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;Landroid/graphics/Bitmap;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 154
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->scaleCopyBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)V
    .locals 0

    .prologue
    .line 1087
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->doCancel()V

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)V
    .locals 0

    .prologue
    .line 1115
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->doDone()V

    return-void
.end method

.method private doCancel()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1089
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v3, :cond_0

    .line 1113
    :goto_0
    return-void

    .line 1092
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    if-eqz v3, :cond_1

    .line 1093
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->Destroy()V

    .line 1094
    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    .line 1096
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mTemporaryOriginalBuffer:[I

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([I)V

    .line 1097
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 1099
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1100
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1103
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1106
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1107
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 1108
    const/4 v8, 0x1

    .line 1109
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 1101
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private doDone()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1117
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    if-eqz v0, :cond_0

    .line 1118
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->applyPreview()V

    .line 1126
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 1148
    :cond_1
    :goto_0
    return-void

    .line 1128
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1129
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1131
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1134
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1135
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 1129
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1137
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->applyPreview()V

    .line 1138
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    invoke-direct {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;)V

    .line 1140
    .local v4, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    .line 1142
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1143
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 1144
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 1145
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getEnhance()Z

    move-result v5

    .line 1142
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V

    goto :goto_0
.end method

.method private getImage()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->openToolGallery(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$CopyToDecodeCallback;)V

    .line 152
    return-void
.end method

.method private init2DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 946
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 948
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v4, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 949
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 950
    const/4 v2, 0x1

    .line 952
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$3;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)V

    .line 949
    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 981
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 984
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$4;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)V

    .line 981
    invoke-virtual {v0, v1, v4, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1010
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 1012
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 1013
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 1015
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 1016
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 1019
    :cond_0
    return-void
.end method

.method private init3DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1022
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 1024
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 1025
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 1028
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$5;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)V

    .line 1025
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1053
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 1056
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$6;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;)V

    .line 1053
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1081
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 1084
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 1086
    :cond_0
    return-void
.end method

.method private initSetAsDialog()V
    .locals 7

    .prologue
    .line 1577
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1579
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1580
    const v2, 0x7f090158

    .line 1581
    const v3, 0x7f0600a3

    .line 1582
    const/4 v4, 0x0

    .line 1583
    const/4 v5, 0x0

    .line 1584
    const v6, 0x103012e

    .line 1579
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1585
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1587
    :cond_0
    return-void
.end method

.method private initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isShareVia"    # Z

    .prologue
    .line 1329
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1330
    .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    const/4 v2, 0x0

    .line 1331
    .local v2, "shareChooser":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 1332
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    const v4, 0x7f06000f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 1336
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/content/pm/LabeledIntent;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/pm/LabeledIntent;

    .line 1337
    .local v0, "extraIntents":[Landroid/content/pm/LabeledIntent;
    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1338
    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1339
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1519
    return-void

    .line 1334
    .end local v0    # "extraIntents":[Landroid/content/pm/LabeledIntent;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    const v4, 0x7f0600a3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method private initShareViaDialog()V
    .locals 7

    .prologue
    .line 1565
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1567
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1568
    const v2, 0x7f090157

    .line 1569
    const v3, 0x7f06000f

    .line 1570
    const/4 v4, 0x0

    .line 1571
    const/4 v5, 0x0

    .line 1572
    const v6, 0x103012e

    .line 1567
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1573
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1575
    :cond_0
    return-void
.end method

.method private initTabLayout()V
    .locals 0

    .prologue
    .line 576
    return-void
.end method

.method private runOptionItem(I)Z
    .locals 9
    .param p1, "optionItemId"    # I

    .prologue
    const v8, 0x7f090156

    const v7, 0x7a1200

    const/4 v6, 0x7

    const/4 v5, 0x0

    const/16 v4, 0x9

    .line 1210
    const/4 v1, 0x0

    .line 1211
    .local v1, "ret":Z
    const/4 v2, 0x0

    .line 1212
    .local v2, "uri":Landroid/net/Uri;
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mOptionItemId:I

    .line 1213
    sparse-switch p1, :sswitch_data_0

    .line 1325
    :cond_0
    :goto_0
    return v1

    .line 1216
    :sswitch_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1218
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1225
    :goto_1
    const/4 v1, 0x1

    .line 1226
    goto :goto_0

    .line 1223
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->openCamera(Landroid/content/Context;)V

    goto :goto_1

    .line 1228
    :sswitch_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1245
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mCurrentSaveSize:I

    .line 1246
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1247
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1258
    :cond_2
    :goto_2
    const/4 v1, 0x1

    .line 1259
    goto :goto_0

    .line 1249
    :cond_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090157

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1250
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 1251
    if-nez v2, :cond_4

    .line 1253
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1255
    :cond_4
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    goto :goto_2

    .line 1261
    :sswitch_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1263
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1265
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1267
    .local v0, "fileName":Ljava/lang/String;
    if-nez v0, :cond_6

    .line 1268
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    .line 1272
    :goto_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v0, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTextToDialog(Ljava/lang/String;I)V

    .line 1274
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v4, 0x2e000000

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 1275
    const/4 v1, 0x1

    .line 1276
    goto/16 :goto_0

    .line 1270
    .restart local v0    # "fileName":Ljava/lang/String;
    :cond_6
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1279
    .end local v0    # "fileName":Ljava/lang/String;
    :sswitch_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1296
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mCurrentSaveSize:I

    .line 1297
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1298
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1311
    :cond_7
    :goto_4
    const/4 v1, 0x1

    .line 1312
    goto/16 :goto_0

    .line 1300
    :cond_8
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090158

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1303
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 1304
    if-nez v2, :cond_9

    .line 1306
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1308
    :cond_9
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    .line 1309
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090158

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    goto :goto_4

    .line 1315
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1316
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    .line 1317
    :cond_a
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v3, :cond_b

    .line 1318
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setVisibilityDirectly(I)V

    .line 1319
    :cond_b
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 1320
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v4, 0x17000000

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 1213
    :sswitch_data_0
    .sparse-switch
        0x7f090007 -> :sswitch_2
        0x7f090157 -> :sswitch_1
        0x7f090158 -> :sswitch_3
        0x7f09015a -> :sswitch_0
        0x7f09015b -> :sswitch_4
    .end sparse-switch
.end method

.method private scaleCopyBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Rect;
    .locals 10
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x0

    .line 155
    const/4 v6, 0x0

    .line 156
    .local v6, "scaleRect":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v8, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-object v7

    .line 159
    :cond_1
    if-eqz p1, :cond_0

    .line 162
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 163
    .local v5, "prevWidth":I
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v4

    .line 164
    .local v4, "prevHeight":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 165
    .local v2, "copyBitmapWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 167
    .local v0, "copyBitmapHeight":I
    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 168
    .local v3, "minSize":I
    int-to-float v7, v2

    int-to-float v8, v0

    div-float v1, v7, v8

    .line 169
    .local v1, "copyBitmapScale":F
    if-lt v2, v0, :cond_2

    .line 170
    move v2, v3

    .line 171
    div-int/lit8 v2, v2, 0x2

    .line 172
    int-to-float v7, v2

    div-float/2addr v7, v1

    float-to-int v0, v7

    .line 187
    :goto_1
    new-instance v6, Landroid/graphics/Rect;

    .end local v6    # "scaleRect":Landroid/graphics/Rect;
    invoke-direct {v6, v9, v9, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v6    # "scaleRect":Landroid/graphics/Rect;
    move-object v7, v6

    .line 189
    goto :goto_0

    .line 174
    :cond_2
    move v0, v3

    .line 175
    div-int/lit8 v0, v0, 0x2

    .line 176
    int-to-float v7, v0

    mul-float/2addr v7, v1

    float-to-int v2, v7

    goto :goto_1
.end method

.method private set2depth()V
    .locals 0

    .prologue
    .line 1151
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->init2DepthActionBar()V

    .line 1169
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->invalidateViews()V

    .line 1170
    return-void
.end method

.method private setVisibleTopBottomMenu(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 414
    if-eqz p1, :cond_1

    .line 415
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->show()V

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 418
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView$ActionbarNBottomButtonAnimation;->hide()V

    goto :goto_0
.end method

.method private showTabLayout(IZZ)V
    .locals 1
    .param p1, "assistantType"    # I
    .param p2, "initByconfig"    # Z
    .param p3, "doOpen"    # Z

    .prologue
    .line 564
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_0

    .line 565
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0, p1, p3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->showTabView(IZ)V

    .line 566
    :cond_0
    return-void
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 427
    const/4 v1, 0x1

    .line 429
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    .line 430
    .local v0, "isDrawerOpened":Z
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    if-eqz v2, :cond_0

    .line 431
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    invoke-virtual {v2, p2, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->touch(Landroid/view/MotionEvent;Z)V

    .line 433
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->invalidateViews()V

    .line 434
    return v1

    .line 429
    .end local v0    # "isDrawerOpened":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public backPressed()V
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x31000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 350
    return-void
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 536
    return-void
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 548
    const/4 v0, 0x0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 552
    const/4 v0, 0x0

    return v0
.end method

.method public getStatusHeight()I
    .locals 1

    .prologue
    .line 559
    const/4 v0, 0x0

    return v0
.end method

.method public getmDecorationMenuLayoutManager()Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    return-object v0
.end method

.method public initActionbar()V
    .locals 0

    .prologue
    .line 354
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->init2DepthActionBar()V

    .line 358
    return-void
.end method

.method public initButtons()V
    .locals 0

    .prologue
    .line 334
    return-void
.end method

.method public initDialog()V
    .locals 0

    .prologue
    .line 371
    return-void
.end method

.method public initEffect()V
    .locals 0

    .prologue
    .line 320
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 395
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 524
    return-void
.end method

.method public initTrayLayout()V
    .locals 0

    .prologue
    .line 531
    return-void
.end method

.method public initView()V
    .locals 0

    .prologue
    .line 325
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 469
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 470
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 484
    :goto_0
    return-void

    .line 476
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_1

    .line 478
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 479
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 480
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 482
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 440
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 441
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mPaint:Landroid/graphics/Paint;

    .line 443
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 445
    :cond_0
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 447
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeCancelPressState(Z)V

    .line 449
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 450
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 452
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 453
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_1

    .line 454
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->destroy()V

    .line 456
    :cond_1
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mTemporaryOriginalBuffer:[I

    .line 458
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 461
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 462
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 465
    :cond_2
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 375
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 379
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 380
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mPaint:Landroid/graphics/Paint;

    .line 377
    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 382
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "bigheadk, mImageStickerEffect = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->drawStickerBdry(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 389
    :cond_0
    return-void
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 487
    const/16 v1, 0x17

    if-eq p1, v1, :cond_0

    const/16 v1, 0x42

    if-ne p1, v1, :cond_3

    .line 489
    :cond_0
    const/4 v0, 0x0

    .line 490
    .local v0, "isFocused":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_1

    .line 491
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    move-result v0

    .line 494
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_2

    .line 495
    if-nez v0, :cond_2

    .line 496
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 518
    .end local v0    # "isFocused":Z
    :cond_2
    :goto_0
    return v2

    .line 500
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 502
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->backPressed()V

    goto :goto_0
.end method

.method public onLayout()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 912
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 915
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 918
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 919
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 920
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_2

    .line 921
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 922
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 923
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 924
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 926
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 929
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 930
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 924
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 931
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW onLayout: mImageData.getPreviewHeight()="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 944
    return-void
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 544
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->runOptionItem(I)Z

    .line 545
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 540
    return-void
.end method

.method public refreshView()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1641
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->getImageEditViewWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->getImageEditViewHeight()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 1643
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1645
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1646
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1649
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1652
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1653
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 1654
    const/4 v8, 0x1

    .line 1655
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 1647
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1657
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->invalidateViews()V

    .line 1658
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 3

    .prologue
    .line 399
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 402
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 403
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_2

    .line 404
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 405
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_3

    .line 406
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 407
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_4

    .line 408
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->configurationChanged()V

    .line 409
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getCurrnetType()I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->showTabLayout(IZZ)V

    .line 411
    :cond_4
    return-void
.end method

.method public setStickerMode(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "targetOrgRoi"    # Landroid/graphics/Rect;

    .prologue
    .line 823
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    if-nez v0, :cond_0

    .line 824
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->stickerCallback:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect$OnMyStickerCallback;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    .line 825
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->mImageStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/ImageStickerEffect;->setStickerMode(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V

    .line 827
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->init3DepthActionBar()V

    .line 828
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ImageStickerView;->invalidateViews()V

    .line 829
    return-void
.end method
