.class Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$LocalBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MultiGridActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LocalBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$LocalBroadcastReceiver;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 277
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 279
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$LocalBroadcastReceiver;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->mSrcItemList:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->access$0(Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;)Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItemList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 297
    :cond_1
    return-void

    .line 279
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;

    .line 283
    .local v2, "item":Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridSrcItem;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 285
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 287
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity$LocalBroadcastReceiver;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 291
    .end local v1    # "f":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 293
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
