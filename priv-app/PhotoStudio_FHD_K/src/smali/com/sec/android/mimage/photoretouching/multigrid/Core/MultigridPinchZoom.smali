.class public Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;
.super Ljava/lang/Object;
.source "MultigridPinchZoom.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;,
        Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;
    }
.end annotation


# instance fields
.field private final TOUCH_TOLERANCE:F

.field private isZoom:Z

.field private mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

.field private mIsMultiTouchEnd:Z

.field private mIsMultiTouchStart:Z

.field private mListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;

.field private mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

.field private mWithSingleTouch:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->isZoom:Z

    .line 176
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->TOUCH_TOLERANCE:F

    .line 178
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    .line 179
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    .line 183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mWithSingleTouch:Z

    .line 184
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;

    .line 9
    return-void
.end method

.method private reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 144
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 149
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    if-eqz v0, :cond_1

    .line 156
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 157
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 160
    :cond_1
    return-void
.end method

.method private set(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "curr"    # Landroid/view/MotionEvent;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 140
    return-void
.end method


# virtual methods
.method public init(Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;)V
    .locals 2
    .param p1, "touch"    # Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 169
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;

    .line 170
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mIsMultiTouchStart:Z

    .line 171
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mIsMultiTouchEnd:Z

    .line 172
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    .line 173
    new-instance v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;-><init>(Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    .line 174
    return-void
.end method

.method public isZoom()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->isZoom:Z

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 18
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 21
    const/4 v9, 0x1

    .line 22
    .local v9, "ret":Z
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 28
    .local v2, "action":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    if-eqz v14, :cond_1

    .line 30
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mIsMultiTouchStart:Z

    if-nez v14, :cond_5

    .line 32
    const/4 v14, 0x5

    if-eq v2, v14, :cond_0

    const/16 v14, 0x105

    if-ne v2, v14, :cond_2

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v14

    const/4 v15, 0x2

    if-lt v14, v15, :cond_2

    .line 35
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->reset()V

    .line 37
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    .line 39
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->set(Landroid/view/MotionEvent;)V

    .line 41
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mIsMultiTouchStart:Z

    .line 42
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mIsMultiTouchEnd:Z

    .line 122
    :cond_1
    :goto_0
    return v9

    .line 48
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mIsMultiTouchEnd:Z

    if-nez v14, :cond_3

    .line 50
    const/4 v14, 0x1

    if-ne v2, v14, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_1

    .line 52
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mIsMultiTouchEnd:Z

    goto :goto_0

    .line 57
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mWithSingleTouch:Z

    if-eqz v14, :cond_1

    .line 59
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;

    if-eqz v14, :cond_1

    .line 61
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->isZoom:Z

    if-eqz v14, :cond_4

    .line 62
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->isZoom:Z

    .line 63
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;

    move-object/from16 v0, p1

    invoke-interface {v14, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;->onSingleTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 75
    :cond_5
    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 95
    :sswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v14

    const/4 v15, 0x2

    if-lt v14, v15, :cond_1

    .line 98
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->set(Landroid/view/MotionEvent;)V

    .line 100
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v14, v14, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/MotionEvent;->getX(I)F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v15, v15, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/view/MotionEvent;->getX(I)F

    move-result v15

    sub-float/2addr v14, v15

    float-to-double v10, v14

    .line 101
    .local v10, "prevDstX":D
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v14, v14, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/MotionEvent;->getY(I)F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v15, v15, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/view/MotionEvent;->getY(I)F

    move-result v15

    sub-float/2addr v14, v15

    float-to-double v12, v14

    .line 103
    .local v12, "prevDstY":D
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v14, v14, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/MotionEvent;->getX(I)F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v15, v15, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/view/MotionEvent;->getX(I)F

    move-result v15

    sub-float/2addr v14, v15

    float-to-double v4, v14

    .line 104
    .local v4, "currDstX":D
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v14, v14, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/MotionEvent;->getY(I)F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mCurr:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v15, v15, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/view/MotionEvent;->getY(I)F

    move-result v15

    sub-float/2addr v14, v15

    float-to-double v6, v14

    .line 106
    .local v6, "currDstY":D
    mul-double v14, v12, v12

    mul-double v16, v10, v10

    add-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v14

    double-to-int v8, v14

    .line 107
    .local v8, "prevDst":I
    mul-double v14, v6, v6

    mul-double v16, v4, v4

    add-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v14

    double-to-int v3, v14

    .line 109
    .local v3, "currDst":I
    sub-int v14, v3, v8

    invoke-static {v14}, Ljava/lang/Math;->abs(I)I

    move-result v14

    int-to-float v14, v14

    const/high16 v15, 0x40000000    # 2.0f

    cmpl-float v14, v14, v15

    if-lez v14, :cond_6

    .line 111
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->isZoom:Z

    .line 112
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mListener:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;

    sub-int v15, v3, v8

    move-object/from16 v0, p1

    invoke-interface {v14, v0, v15}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$OnCollagePinchZoomCallback;->onPinchZoom(Landroid/view/MotionEvent;I)V

    .line 114
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    iget-object v14, v14, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    invoke-virtual {v14}, Landroid/view/MotionEvent;->recycle()V

    .line 115
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mPrev:Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;

    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom$MultiTouchInfo;->event:Landroid/view/MotionEvent;

    goto/16 :goto_0

    .line 79
    .end local v3    # "currDst":I
    .end local v4    # "currDstX":D
    .end local v6    # "currDstY":D
    .end local v8    # "prevDst":I
    .end local v10    # "prevDstX":D
    .end local v12    # "prevDstY":D
    :sswitch_1
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->set(Landroid/view/MotionEvent;)V

    .line 84
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mIsMultiTouchStart:Z

    .line 86
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->reset()V

    goto/16 :goto_0

    .line 90
    :sswitch_2
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->mIsMultiTouchStart:Z

    .line 91
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Core/MultigridPinchZoom;->reset()V

    goto/16 :goto_0

    .line 75
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_2
        0x6 -> :sswitch_1
        0x106 -> :sswitch_1
    .end sparse-switch
.end method
