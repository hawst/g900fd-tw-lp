.class public Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
.super Ljava/lang/Object;
.source "DialogsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;,
        Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;,
        Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    }
.end annotation


# static fields
.field public static final ALERT_DIALOG:I = 0x1000

.field public static final BRUSH_SEEKBAR_BUTTON:I = 0x210

.field public static final CANCEL_DISCARD_DIALOG:I = 0xa

.field public static final CUSTOM_DIALOG_HORIZONTAL:I = 0x4000

.field public static final CUSTOM_DIALOG_NOTITLE:I = 0x3000

.field public static final CUSTOM_DIALOG_TITLE:I = 0x2000

.field public static final DEFAULT_BUTTON:I = 0x100

.field public static final DEFAULT_FIRST_BUTTON:I = 0x110

.field public static final DEFAULT_LAST_BUTTON:I = 0x120

.field public static final DEFAULT_RADIO_BUTTON:I = 0x800

.field public static final DEFAULT_RADIO_FIRST_BUTTON:I = 0x810

.field public static final DEFAULT_RADIO_LAST_BUTTON:I = 0x820

.field public static final DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

.field public static final DIALOG_DEFAULT_TEXT_COLOR_LIGHT_THEME:I = -0x1000000

.field public static final DIALOG_POINTER_DOWN:I = 0x1

.field public static final DIALOG_POINTER_UP:I = 0x0

.field public static final INPUT_TEXT:I = 0x900

.field public static final NONE:I = 0x0

.field public static final NOTI_INFO:I = 0x300

.field public static final PHOTOEDITOR_DIALOG:I = 0x6

.field public static final QUESTION:I = 0x500

.field public static final REDOALL_DIALOG:I = 0x5

.field public static final SAVE:I = 0x600

.field public static final SAVE_OPTION_DIALOG:I = 0x9

.field public static final SAVE_YES_NO_CANCEL_FINISH_DIALOG:I = 0x8

.field public static final SAVE_YES_NO_CANCEL_OPEN_DIALOG:I = 0x7

.field public static final SEEKBAR_BUTTON:I = 0x200

.field public static final SELECTION_DIALOG:I = 0x1

.field public static final SHARE_VIA_BUTTON:I = 0x700

.field public static final SHARE_VIA_LAST_BUTTON:I = 0x710

.field public static final THUMBNAIL:I = 0x400

.field public static final THUMBNAIL_SMALL:I = 0x410

.field public static final TRAY_DELETE_DIALOG:I = 0x3

.field public static final UNDOALL_DIALOG:I = 0x4

.field public static final UNSELECTION_DIALOG:I = 0x2


# instance fields
.field private final DIALOG_PHOTOEDITOR_INTRO_BODY_TEXT_COLOR:I

.field private final DIALOG_PHOTOEDITOR_INTRO_BODY_TEXT_SIZE:I

.field private final DIALOG_PHOTOEDITOR_INTRO_BOTTOM_TEXT_SIZE:I

.field private final DIALOG_PHOTOEDITOR_INTRO_TITLE_TEXT_SIZE:I

.field private TYPEFACE_ROBOTO_LIGHT_BOLD:Landroid/graphics/Typeface;

.field private TYPEFACE_ROBOTO_REGULAR:Landroid/graphics/Typeface;

.field private mContext:Landroid/content/Context;

.field private mCurrentSelectedButton:Landroid/view/View;

.field private mDialogButtonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDialogContentView:Landroid/view/View;

.field private mDialogList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDlgKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mPointerDirection:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2115
    const-string v0, "#fff5f5f5"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    .line 2129
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2078
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    .line 2079
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mCurrentSelectedButton:Landroid/view/View;

    .line 2080
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mPointerDirection:I

    .line 2081
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    .line 2082
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogButtonList:Ljava/util/ArrayList;

    .line 2083
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogContentView:Landroid/view/View;

    .line 2084
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDlgKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 2111
    const/16 v0, 0x16

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_PHOTOEDITOR_INTRO_BODY_TEXT_SIZE:I

    .line 2112
    const/16 v0, 0x18

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_PHOTOEDITOR_INTRO_TITLE_TEXT_SIZE:I

    .line 2113
    const/16 v0, 0x17

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_PHOTOEDITOR_INTRO_BOTTOM_TEXT_SIZE:I

    .line 2114
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_PHOTOEDITOR_INTRO_BODY_TEXT_COLOR:I

    .line 2117
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->TYPEFACE_ROBOTO_REGULAR:Landroid/graphics/Typeface;

    .line 2118
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->TYPEFACE_ROBOTO_LIGHT_BOLD:Landroid/graphics/Typeface;

    .line 67
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    .line 70
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->initDlgKeyListener()V

    .line 71
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 2081
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private buildDialog(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;)Landroid/app/Dialog;
    .locals 10
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .prologue
    const v9, 0x7f0900ab

    const v8, 0x7f0900aa

    const/16 v6, 0x8

    const/4 v7, 0x0

    .line 1133
    const/4 v1, 0x0

    .line 1134
    .local v1, "d":Landroid/app/Dialog;
    iget v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialogType:I

    sparse-switch v5, :sswitch_data_0

    .line 1243
    :goto_0
    return-object v1

    .line 1139
    :sswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1140
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    if-eqz v5, :cond_0

    .line 1142
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->listener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v5, :cond_6

    .line 1144
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    iget-object v6, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->text:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->doNeedUppercaseString(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1145
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->text:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->toUppercaseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1146
    .local v4, "upperCaseText":Ljava/lang/String;
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->listener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1167
    .end local v4    # "upperCaseText":Ljava/lang/String;
    :cond_0
    :goto_1
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    if-eqz v5, :cond_1

    .line 1169
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    iget-object v6, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->text:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->doNeedUppercaseString(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1170
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->text:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->toUppercaseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1171
    .restart local v4    # "upperCaseText":Ljava/lang/String;
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->listener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1176
    .end local v4    # "upperCaseText":Ljava/lang/String;
    :cond_1
    :goto_2
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    if-eqz v5, :cond_2

    .line 1178
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    iget-object v6, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->text:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->doNeedUppercaseString(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1179
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->text:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->toUppercaseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1180
    .restart local v4    # "upperCaseText":Ljava/lang/String;
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->listener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1186
    .end local v4    # "upperCaseText":Ljava/lang/String;
    :cond_2
    :goto_3
    iget-boolean v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->hasContentView:Z

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogContentView:Landroid/view/View;

    if-eqz v5, :cond_3

    .line 1188
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogContentView:Landroid/view/View;

    invoke-virtual {v5, v7}, Landroid/view/View;->setFocusable(Z)V

    .line 1189
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogContentView:Landroid/view/View;

    invoke-virtual {v5, v7}, Landroid/view/View;->setSelected(Z)V

    .line 1190
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogContentView:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1192
    :cond_3
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 1193
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    if-eqz v5, :cond_4

    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->showlistener:Landroid/content/DialogInterface$OnShowListener;

    if-eqz v5, :cond_4

    .line 1194
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->showlistener:Landroid/content/DialogInterface$OnShowListener;

    invoke-virtual {v1, v5}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 1196
    :cond_4
    iget-boolean v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isModal:Z

    invoke-direct {p0, v1, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setDialogFlag(Landroid/app/Dialog;Z)V

    goto/16 :goto_0

    .line 1148
    :cond_5
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    iget-object v6, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->listener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 1154
    :cond_6
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    iget-object v6, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->text:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->doNeedUppercaseString(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1155
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->text:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->toUppercaseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1156
    .restart local v4    # "upperCaseText":Ljava/lang/String;
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$1;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    .line 1160
    .end local v4    # "upperCaseText":Ljava/lang/String;
    :cond_7
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    new-instance v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$2;

    invoke-direct {v6, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    .line 1173
    :cond_8
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    iget-object v6, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->listener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_2

    .line 1182
    :cond_9
    iget-object v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget v5, v5, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    iget-object v6, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v6, v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->listener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_3

    .line 1200
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :sswitch_1
    new-instance v1, Landroid/app/Dialog;

    .end local v1    # "d":Landroid/app/Dialog;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 1201
    .restart local v1    # "d":Landroid/app/Dialog;
    iget-boolean v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isModal:Z

    invoke-direct {p0, v1, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setDialogFlag(Landroid/app/Dialog;Z)V

    .line 1202
    const v5, 0x7f03003c

    invoke-virtual {v1, v5}, Landroid/app/Dialog;->setContentView(I)V

    goto/16 :goto_0

    .line 1205
    :sswitch_2
    new-instance v1, Landroid/app/Dialog;

    .end local v1    # "d":Landroid/app/Dialog;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 1206
    .restart local v1    # "d":Landroid/app/Dialog;
    iget-boolean v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isModal:Z

    invoke-direct {p0, v1, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setDialogFlag(Landroid/app/Dialog;Z)V

    .line 1207
    const v5, 0x7f03003d

    invoke-virtual {v1, v5}, Landroid/app/Dialog;->setContentView(I)V

    .line 1208
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mPointerDirection:I

    if-nez v5, :cond_a

    .line 1210
    invoke-virtual {v1, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 1211
    .local v3, "up":Landroid/widget/ImageView;
    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1213
    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 1214
    .local v2, "down":Landroid/widget/ImageView;
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1218
    .end local v2    # "down":Landroid/widget/ImageView;
    .end local v3    # "up":Landroid/widget/ImageView;
    :cond_a
    invoke-virtual {v1, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 1219
    .restart local v3    # "up":Landroid/widget/ImageView;
    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1221
    invoke-virtual {v1, v9}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 1222
    .restart local v2    # "down":Landroid/widget/ImageView;
    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1226
    .end local v2    # "down":Landroid/widget/ImageView;
    .end local v3    # "up":Landroid/widget/ImageView;
    :sswitch_3
    new-instance v1, Landroid/app/Dialog;

    .end local v1    # "d":Landroid/app/Dialog;
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 1227
    .restart local v1    # "d":Landroid/app/Dialog;
    iget-boolean v5, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isModal:Z

    invoke-direct {p0, v1, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setDialogFlag(Landroid/app/Dialog;Z)V

    .line 1228
    const v5, 0x7f03003b

    invoke-virtual {v1, v5}, Landroid/app/Dialog;->setContentView(I)V

    goto/16 :goto_0

    .line 1134
    nop

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x2000 -> :sswitch_1
        0x3000 -> :sswitch_2
        0x4000 -> :sswitch_3
    .end sparse-switch
.end method

.method private getLastDialogInfo()Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    .locals 3

    .prologue
    .line 860
    const/4 v0, 0x0

    .line 861
    .local v0, "ret":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 863
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 865
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "ret":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 868
    .restart local v0    # "ret":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    :cond_0
    return-object v0
.end method

.method private initDlgKeyListener()V
    .locals 1

    .prologue
    .line 1949
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$6;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDlgKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 2076
    return-void
.end method

.method private isShow(Ljava/util/ArrayList;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1379
    .local p1, "buttonList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;>;"
    const/4 v3, 0x1

    .line 1380
    .local v3, "ret":Z
    if-eqz p1, :cond_0

    .line 1382
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 1397
    .end local v2    # "i":I
    :cond_0
    return v3

    .line 1384
    .restart local v2    # "i":I
    :cond_1
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    .line 1385
    .local v0, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const/16 v5, 0x300

    if-ne v4, v5, :cond_2

    .line 1388
    :try_start_0
    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->key:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 1389
    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->key:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isDoNotShow(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 1382
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1390
    :catch_0
    move-exception v1

    .line 1392
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private setAlertDialogContext(Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;)V
    .locals 0
    .param p1, "dContext"    # Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;

    .prologue
    .line 1405
    return-void
.end method

.method private setContext(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;Landroid/app/Dialog;Ljava/util/ArrayList;)V
    .locals 15
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    .param p2, "d"    # Landroid/app/Dialog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;",
            "Landroid/app/Dialog;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1408
    .local p3, "buttonList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;>;"
    new-instance v9, Landroid/widget/ScrollView;

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-direct {v9, v12}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 1409
    .local v9, "parent":Landroid/widget/ScrollView;
    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v12}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1410
    .local v2, "child":Landroid/widget/LinearLayout;
    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1412
    new-instance v12, Landroid/view/ViewGroup$LayoutParams;

    const/4 v13, -0x2

    const/4 v14, -0x2

    invoke-direct {v12, v13, v14}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v12}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1413
    invoke-virtual {v9, v2}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 1414
    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Landroid/widget/ScrollView;->setFocusable(Z)V

    .line 1415
    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$3;

    invoke-direct {v12, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    invoke-virtual {v9, v12}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1430
    if-eqz p3, :cond_0

    .line 1432
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-lt v4, v12, :cond_1

    .line 1837
    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDlgKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1838
    move-object/from16 v0, p2

    instance-of v12, v0, Landroid/app/AlertDialog;

    if-eqz v12, :cond_2c

    .line 1840
    check-cast p2, Landroid/app/AlertDialog;

    .end local p2    # "d":Landroid/app/Dialog;
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 1848
    .end local v4    # "i":I
    :cond_0
    :goto_1
    return-void

    .line 1434
    .restart local v4    # "i":I
    .restart local p2    # "d":Landroid/app/Dialog;
    :cond_1
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    .line 1435
    .local v1, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;

    iget-object v12, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;-><init>(Landroid/content/Context;)V

    .line 1436
    .local v3, "dContext":Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;
    iput-object v3, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    .line 1437
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonId:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setId(I)V

    .line 1439
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const v13, 0xff00

    and-int/2addr v12, v13

    sparse-switch v12, :sswitch_data_0

    .line 1835
    :cond_2
    :goto_2
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1432
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1442
    :sswitch_0
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const/16 v13, 0x120

    if-ne v12, v13, :cond_5

    .line 1444
    const v12, 0x7f030013

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    .line 1445
    move-object/from16 v0, p1

    iget-boolean v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isDivider:Z

    if-nez v12, :cond_3

    .line 1446
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->removeDivider()V

    .line 1459
    :cond_3
    :goto_3
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setDividerBackground(Z)V

    .line 1460
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->iconId:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setIcon(I)V

    .line 1461
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_4

    .line 1462
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_7

    .line 1463
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1466
    :goto_4
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setText(Ljava/lang/String;)V

    .line 1468
    :cond_4
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;

    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->touchFunction:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    invoke-direct {v5, p0, v12}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1469
    .local v5, "l":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;
    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_2

    .line 1448
    .end local v5    # "l":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;
    :cond_5
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const/16 v13, 0x110

    if-ne v12, v13, :cond_6

    .line 1450
    const v12, 0x7f030011

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    .line 1451
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->removeDivider()V

    goto :goto_3

    .line 1455
    :cond_6
    const v12, 0x7f030011

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    .line 1456
    move-object/from16 v0, p1

    iget-boolean v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isDivider:Z

    if-nez v12, :cond_3

    .line 1457
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->removeDivider()V

    goto :goto_3

    .line 1465
    :cond_7
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_4

    .line 1540
    :sswitch_1
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 1542
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_8

    .line 1544
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setText(Ljava/lang/String;)V

    .line 1545
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_9

    .line 1546
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1549
    :goto_5
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setAlertDialogContext(Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;)V

    .line 1551
    :cond_8
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;

    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->touchFunction:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    invoke-direct {v5, p0, v12}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1552
    .restart local v5    # "l":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;
    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_2

    .line 1548
    .end local v5    # "l":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;
    :cond_9
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_5

    .line 1555
    :sswitch_2
    move-object/from16 v0, p1

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    const v13, 0x103012e

    if-ne v12, v13, :cond_c

    .line 1557
    const v12, 0x7f030015

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    .line 1558
    const/16 v12, 0x300

    invoke-virtual {p0, v3, v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setDialogPopupLayout(Landroid/widget/LinearLayout;I)V

    .line 1559
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_a

    .line 1561
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_b

    .line 1562
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1565
    :goto_6
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setText(Ljava/lang/String;)V

    .line 1586
    :cond_a
    :goto_7
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_f

    .line 1587
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1590
    :goto_8
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;

    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->touchFunction:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    invoke-direct {v5, p0, v12}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1591
    .restart local v5    # "l":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;
    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setCheckButtonListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_2

    .line 1564
    .end local v5    # "l":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;
    :cond_b
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_6

    .line 1568
    :cond_c
    move-object/from16 v0, p1

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    const v13, 0x1030132

    if-ne v12, v13, :cond_e

    .line 1570
    const v12, 0x7f030016

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    .line 1571
    const/16 v12, 0x300

    invoke-virtual {p0, v3, v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setDialogPopupLayout(Landroid/widget/LinearLayout;I)V

    .line 1572
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_a

    .line 1574
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_d

    .line 1575
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1578
    :goto_9
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setText(Ljava/lang/String;)V

    .line 1579
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setAlertDialogContext(Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;)V

    goto :goto_7

    .line 1577
    :cond_d
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_9

    .line 1584
    :cond_e
    const-string v12, "Failed Noti_Info dialog setContext, DialogsManager"

    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    goto :goto_7

    .line 1589
    :cond_f
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_8

    .line 1595
    :sswitch_3
    const v12, 0x7f030017

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    .line 1596
    const/16 v12, 0x500

    invoke-virtual {p0, v3, v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setDialogPopupLayout(Landroid/widget/LinearLayout;I)V

    .line 1597
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_10

    .line 1599
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_11

    .line 1600
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1603
    :goto_a
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setText(Ljava/lang/String;)V

    .line 1604
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setAlertDialogContext(Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;)V

    .line 1606
    :cond_10
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;

    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->touchFunction:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    invoke-direct {v5, p0, v12}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1607
    .restart local v5    # "l":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;
    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setCheckButtonListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_2

    .line 1602
    .end local v5    # "l":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;
    :cond_11
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_a

    .line 1610
    :sswitch_4
    const v12, 0x7f03001b

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    .line 1611
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    iget v13, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->iconId:I

    iget-object v14, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarLeftListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v12, v13, v14}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setLeftButton(IILandroid/view/View$OnClickListener;)V

    .line 1612
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    iget v13, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->iconId2:I

    iget-object v14, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarRightListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v12, v13, v14}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setRightButton(IILandroid/view/View$OnClickListener;)V

    .line 1613
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const/16 v13, 0x200

    if-ne v12, v13, :cond_12

    .line 1615
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarStartPos:I

    iget v13, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarMax:I

    iget-object v14, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarListener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    invoke-virtual {v3, v12, v13, v14}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setSeekBar(IILcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;)V

    goto/16 :goto_2

    .line 1619
    :cond_12
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarStartPos:I

    iget v13, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarMax:I

    iget-object v14, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarListener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    move-object/from16 v0, p2

    invoke-virtual {v3, v0, v12, v13, v14}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setBrushSeekBar(Landroid/app/Dialog;IILcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;)V

    goto/16 :goto_2

    .line 1624
    :sswitch_5
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const/16 v13, 0x410

    if-ne v12, v13, :cond_14

    .line 1626
    const v12, 0x7f030020

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    .line 1632
    :goto_b
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_13

    .line 1633
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_15

    .line 1634
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1637
    :goto_c
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setText(Ljava/lang/String;)V

    .line 1639
    :cond_13
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->thumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setThumbnail(Landroid/graphics/Bitmap;)V

    .line 1640
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;

    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->touchFunction:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    invoke-direct {v5, p0, v12}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1641
    .restart local v5    # "l":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;
    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_2

    .line 1630
    .end local v5    # "l":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;
    :cond_14
    const v12, 0x7f03001f

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    goto :goto_b

    .line 1636
    :cond_15
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_c

    .line 1644
    :sswitch_6
    const v12, 0x7f03001a

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    .line 1645
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_2

    .line 1647
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_16

    .line 1648
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1652
    :goto_d
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_17

    .line 1653
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setEditTextColor(I)V

    .line 1657
    :goto_e
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setText(Ljava/lang/String;)V

    .line 1658
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setAlertDialogContext(Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;)V

    goto/16 :goto_2

    .line 1650
    :cond_16
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_d

    .line 1655
    :cond_17
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setEditTextColor(I)V

    goto :goto_e

    .line 1662
    :sswitch_7
    const-string v12, "bigheadk, INPUT_TEXT"

    invoke-static {v12}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 1663
    const v12, 0x7f03001e

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    .line 1664
    const/16 v12, 0x900

    invoke-virtual {p0, v3, v12}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setDialogPopupLayout(Landroid/widget/LinearLayout;I)V

    .line 1665
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_2

    .line 1667
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_18

    .line 1668
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setEditTextColor(I)V

    .line 1672
    :goto_f
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_19

    .line 1673
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1676
    :goto_10
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setText(Ljava/lang/String;)V

    .line 1677
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setAlertDialogContext(Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;)V

    goto/16 :goto_2

    .line 1670
    :cond_18
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setEditTextColor(I)V

    goto :goto_f

    .line 1675
    :cond_19
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_10

    .line 1682
    :sswitch_8
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const/16 v13, 0x820

    if-ne v12, v13, :cond_21

    .line 1684
    const v12, 0x7f030019

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    .line 1685
    move-object/from16 v0, p1

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    const v13, 0x103012e

    if-ne v12, v13, :cond_1f

    .line 1687
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_1a

    .line 1689
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_1e

    .line 1690
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1706
    :cond_1a
    :goto_11
    move-object/from16 v0, p1

    iget-boolean v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isDivider:Z

    if-nez v12, :cond_1b

    .line 1707
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->removeDivider()V

    .line 1708
    :cond_1b
    const v12, 0x7f09001e

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RadioButton;

    .line 1711
    .local v11, "rb2":Landroid/widget/RadioButton;
    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$4;

    invoke-direct {v12, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    invoke-virtual {v11, v12}, Landroid/widget/RadioButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1731
    const v12, 0x7f090018

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 1732
    .local v8, "ll":Landroid/widget/LinearLayout;
    const/4 v12, 0x0

    invoke-virtual {v8, v12}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 1820
    .end local v8    # "ll":Landroid/widget/LinearLayout;
    .end local v11    # "rb2":Landroid/widget/RadioButton;
    :cond_1c
    :goto_12
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setDividerBackground(Z)V

    .line 1821
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->iconId:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setIcon(I)V

    .line 1822
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_1d

    .line 1823
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_2b

    .line 1824
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1827
    :goto_13
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setText(Ljava/lang/String;)V

    .line 1829
    :cond_1d
    new-instance v7, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;

    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->touchFunction:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    invoke-direct {v7, p0, v12}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1830
    .local v7, "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;
    invoke-virtual {v3, v7}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_2

    .line 1692
    .end local v7    # "listener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener;
    :cond_1e
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_11

    .line 1695
    :cond_1f
    move-object/from16 v0, p1

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    const v13, 0x1030132

    if-ne v12, v13, :cond_1a

    .line 1697
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_1a

    .line 1699
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_20

    .line 1700
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1703
    :goto_14
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setAlertDialogContext(Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;)V

    goto :goto_11

    .line 1702
    :cond_20
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_14

    .line 1735
    :cond_21
    iget v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    const/16 v13, 0x810

    if-ne v12, v13, :cond_26

    .line 1737
    const v12, 0x7f030018

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    .line 1738
    move-object/from16 v0, p1

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    const v13, 0x103012e

    if-ne v12, v13, :cond_24

    .line 1740
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_22

    .line 1742
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_23

    .line 1743
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1759
    :cond_22
    :goto_15
    const v12, 0x7f09001e

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RadioButton;

    .line 1760
    .local v10, "rb1":Landroid/widget/RadioButton;
    const/4 v12, 0x1

    invoke-virtual {v10, v12}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 1761
    new-instance v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$5;

    invoke-direct {v12, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    invoke-virtual {v10, v12}, Landroid/widget/RadioButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1780
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->removeDivider()V

    goto/16 :goto_12

    .line 1745
    .end local v10    # "rb1":Landroid/widget/RadioButton;
    :cond_23
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_15

    .line 1748
    :cond_24
    move-object/from16 v0, p1

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    const v13, 0x1030132

    if-ne v12, v13, :cond_22

    .line 1750
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_22

    .line 1752
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_25

    .line 1753
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1756
    :goto_16
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setAlertDialogContext(Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;)V

    goto :goto_15

    .line 1755
    :cond_25
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_16

    .line 1784
    :cond_26
    const v12, 0x7f030018

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->init(I)V

    .line 1785
    move-object/from16 v0, p1

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    const v13, 0x103012e

    if-ne v12, v13, :cond_29

    .line 1787
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_27

    .line 1789
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_28

    .line 1790
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1806
    :cond_27
    :goto_17
    move-object/from16 v0, p1

    iget-boolean v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isDivider:Z

    if-nez v12, :cond_1c

    .line 1807
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->removeDivider()V

    goto/16 :goto_12

    .line 1792
    :cond_28
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_17

    .line 1795
    :cond_29
    move-object/from16 v0, p1

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    const v13, 0x1030132

    if-ne v12, v13, :cond_27

    .line 1797
    iget-object v12, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v12, :cond_27

    .line 1799
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v12

    if-nez v12, :cond_2a

    .line 1800
    sget v12, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->DIALOG_DEFAULT_TEXT_COLOR_DARK_THEME:I

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    .line 1803
    :goto_18
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setAlertDialogContext(Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;)V

    goto :goto_17

    .line 1802
    :cond_2a
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto :goto_18

    .line 1826
    :cond_2b
    const/high16 v12, -0x1000000

    invoke-virtual {v3, v12}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setTextColor(I)V

    goto/16 :goto_13

    .line 1844
    .end local v1    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    .end local v3    # "dContext":Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;
    :cond_2c
    const v12, 0x7f0900a9

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 1845
    .local v6, "layout":Landroid/widget/LinearLayout;
    invoke-virtual {v6, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 1439
    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_4
        0x300 -> :sswitch_2
        0x400 -> :sswitch_5
        0x500 -> :sswitch_3
        0x600 -> :sswitch_6
        0x700 -> :sswitch_1
        0x800 -> :sswitch_8
        0x900 -> :sswitch_7
    .end sparse-switch
.end method

.method private setDialogFlag(Landroid/app/Dialog;Z)V
    .locals 5
    .param p1, "d"    # Landroid/app/Dialog;
    .param p2, "isModal"    # Z

    .prologue
    const/4 v4, -0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1360
    invoke-virtual {p1, v3}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 1361
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLOSdevices()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1362
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1364
    :cond_0
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setLayout(II)V

    .line 1365
    if-eqz p2, :cond_1

    .line 1367
    invoke-virtual {p1, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1368
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setCloseOnTouchOutside(Z)V

    .line 1376
    :goto_0
    return-void

    .line 1372
    :cond_1
    invoke-virtual {p1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1374
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->setCloseOnTouchOutside(Z)V

    goto :goto_0
.end method

.method private setPosition(Landroid/app/Dialog;Landroid/graphics/Point;)V
    .locals 2
    .param p1, "d"    # Landroid/app/Dialog;
    .param p2, "position"    # Landroid/graphics/Point;

    .prologue
    .line 1851
    if-eqz p2, :cond_0

    .line 1853
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    .line 1854
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v1, p2, Landroid/graphics/Point;->x:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1855
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v1, p2, Landroid/graphics/Point;->y:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1857
    :cond_0
    return-void
.end method

.method private setTitle(Landroid/app/Dialog;II)V
    .locals 6
    .param p1, "d"    # Landroid/app/Dialog;
    .param p2, "titleId"    # I
    .param p3, "theme"    # I

    .prologue
    const v5, 0x7f090017

    const/4 v4, 0x0

    const v3, 0x7f030010

    .line 1248
    instance-of v2, p1, Landroid/app/AlertDialog;

    if-eqz v2, :cond_7

    .line 1250
    if-nez p2, :cond_1

    .line 1252
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 1305
    .end local p1    # "d":Landroid/app/Dialog;
    :cond_0
    :goto_0
    return-void

    .line 1256
    .restart local p1    # "d":Landroid/app/Dialog;
    :cond_1
    const/4 v1, 0x0

    .line 1257
    .local v1, "v":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v2, :cond_4

    .line 1259
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1269
    :cond_2
    :goto_1
    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1271
    .local v0, "t":Landroid/widget/TextView;
    const v2, 0x103012e

    if-eq p3, v2, :cond_3

    .line 1280
    :cond_3
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1281
    const-string v2, "#f5f5f5"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1286
    :goto_2
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 1287
    check-cast p1, Landroid/app/AlertDialog;

    .end local p1    # "d":Landroid/app/Dialog;
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog;->setCustomTitle(Landroid/view/View;)V

    goto :goto_0

    .line 1261
    .end local v0    # "t":Landroid/widget/TextView;
    .restart local p1    # "d":Landroid/app/Dialog;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    if-eqz v2, :cond_5

    .line 1263
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1264
    goto :goto_1

    .line 1265
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    if-eqz v2, :cond_2

    .line 1267
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1283
    .restart local v0    # "t":Landroid/widget/TextView;
    :cond_6
    const-string v2, "#000000"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 1292
    .end local v0    # "t":Landroid/widget/TextView;
    .end local v1    # "v":Landroid/view/View;
    :cond_7
    if-eqz p2, :cond_0

    .line 1294
    invoke-virtual {p1, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1295
    .restart local v0    # "t":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 1296
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 1297
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v2

    if-nez v2, :cond_8

    .line 1298
    const-string v2, "#f5f5f5"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 1300
    :cond_8
    const-string v2, "#007990"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0
.end method

.method private setTitle(Landroid/app/Dialog;Ljava/lang/String;I)V
    .locals 6
    .param p1, "d"    # Landroid/app/Dialog;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "theme"    # I

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f090017

    const v3, 0x7f030010

    .line 1308
    instance-of v2, p1, Landroid/app/AlertDialog;

    if-eqz v2, :cond_6

    .line 1310
    if-nez p2, :cond_1

    .line 1312
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 1357
    .end local p1    # "d":Landroid/app/Dialog;
    :cond_0
    :goto_0
    return-void

    .line 1316
    .restart local p1    # "d":Landroid/app/Dialog;
    :cond_1
    const/4 v1, 0x0

    .line 1317
    .local v1, "v":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    if-eqz v2, :cond_4

    .line 1319
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1326
    :cond_2
    :goto_1
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1327
    .local v0, "t":Landroid/widget/TextView;
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1329
    const v2, 0x103012e

    if-eq p3, v2, :cond_3

    .line 1336
    :cond_3
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1337
    const-string v2, "#f5f5f5"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1340
    :goto_2
    check-cast p1, Landroid/app/AlertDialog;

    .end local p1    # "d":Landroid/app/Dialog;
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog;->setCustomTitle(Landroid/view/View;)V

    goto :goto_0

    .line 1321
    .end local v0    # "t":Landroid/widget/TextView;
    .restart local p1    # "d":Landroid/app/Dialog;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    if-eqz v2, :cond_2

    .line 1323
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1339
    .restart local v0    # "t":Landroid/widget/TextView;
    :cond_5
    const-string v2, "#007990"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 1345
    .end local v0    # "t":Landroid/widget/TextView;
    .end local v1    # "v":Landroid/view/View;
    :cond_6
    if-eqz p2, :cond_0

    .line 1347
    invoke-virtual {p1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1348
    .restart local v0    # "t":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 1349
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1350
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1351
    const-string v2, "#f5f5f5"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 1353
    :cond_7
    const-string v2, "#007990"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private showDialog(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;)Z
    .locals 4
    .param p1, "dInfo"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .prologue
    .line 1860
    const/4 v1, 0x0

    .line 1861
    .local v1, "ret":Z
    if-eqz p1, :cond_1

    .line 1862
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    .line 1864
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->cancel()V

    .line 1865
    const/4 v2, 0x0

    iput-object v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    .line 1867
    :cond_0
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isShow(Ljava/util/ArrayList;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1869
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->buildDialog(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;)Landroid/app/Dialog;

    move-result-object v0

    .line 1870
    .local v0, "d":Landroid/app/Dialog;
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->title:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1871
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->title:Ljava/lang/String;

    iget v3, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTitle(Landroid/app/Dialog;Ljava/lang/String;I)V

    .line 1874
    :goto_0
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setContext(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;Landroid/app/Dialog;Ljava/util/ArrayList;)V

    .line 1875
    iget-object v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->position:Landroid/graphics/Point;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPosition(Landroid/app/Dialog;Landroid/graphics/Point;)V

    .line 1877
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1878
    iput-object v0, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    .line 1881
    .end local v0    # "d":Landroid/app/Dialog;
    :cond_1
    return v1

    .line 1873
    .restart local v0    # "d":Landroid/app/Dialog;
    :cond_2
    iget v2, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->titleId:I

    iget v3, p1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    invoke-direct {p0, v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTitle(Landroid/app/Dialog;II)V

    goto :goto_0
.end method


# virtual methods
.method public IsDialogShown(I)Z
    .locals 4
    .param p1, "callerId"    # I

    .prologue
    .line 726
    const/4 v2, 0x0

    .line 728
    .local v2, "ret":Z
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 730
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 741
    .end local v1    # "i":I
    :cond_0
    return v2

    .line 732
    .restart local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 734
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    if-ne v3, p1, :cond_2

    .line 736
    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v3, :cond_2

    .line 737
    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    .line 730
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V
    .locals 2
    .param p1, "buttonType"    # I
    .param p2, "buttonId"    # I
    .param p3, "iconId"    # I
    .param p4, "textId"    # I
    .param p5, "thumbnail"    # Landroid/graphics/Bitmap;
    .param p6, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .prologue
    .line 885
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    .line 886
    .local v0, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iput p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    .line 887
    iput p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->iconId:I

    .line 888
    iput p4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    .line 889
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-static {v1, p4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    .line 890
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonId:I

    .line 891
    iput-object p5, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->thumbnail:Landroid/graphics/Bitmap;

    .line 892
    iput-object p6, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->touchFunction:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 893
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 894
    return-void
.end method

.method public addDialogButton(IIILjava/lang/String;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V
    .locals 2
    .param p1, "buttonType"    # I
    .param p2, "buttonId"    # I
    .param p3, "iconId"    # I
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "thumbnail"    # Landroid/graphics/Bitmap;
    .param p6, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .prologue
    .line 872
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    .line 873
    .local v0, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iput p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    .line 874
    iput p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->iconId:I

    .line 875
    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    .line 876
    iput-object p4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    .line 877
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonId:I

    .line 878
    iput-object p5, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->thumbnail:Landroid/graphics/Bitmap;

    .line 879
    iput-object p6, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->touchFunction:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 881
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 882
    return-void
.end method

.method public addDialogButton(IILandroid/graphics/drawable/Drawable;ILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V
    .locals 2
    .param p1, "buttonType"    # I
    .param p2, "buttonId"    # I
    .param p3, "icon"    # Landroid/graphics/drawable/Drawable;
    .param p4, "textId"    # I
    .param p5, "thumbnail"    # Landroid/graphics/Bitmap;
    .param p6, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .prologue
    .line 897
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    .line 898
    .local v0, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iput p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    .line 899
    iput-object p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->icon:Landroid/graphics/drawable/Drawable;

    .line 900
    iput p4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    .line 901
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-static {v1, p4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    .line 902
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonId:I

    .line 903
    iput-object p5, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->thumbnail:Landroid/graphics/Bitmap;

    .line 904
    iput-object p6, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->touchFunction:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 906
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 907
    return-void
.end method

.method public addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V
    .locals 2
    .param p1, "buttonType"    # I
    .param p2, "textId"    # I
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .prologue
    .line 910
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    .line 911
    .local v0, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iput p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    .line 912
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    .line 913
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-static {v1, p2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    .line 914
    iput-object p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->key:Ljava/lang/String;

    .line 915
    iput-object p4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->touchFunction:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 917
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 918
    return-void
.end method

.method public addNoti(ILjava/lang/String;Ljava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V
    .locals 2
    .param p1, "buttonType"    # I
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "touch"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .prologue
    .line 921
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    .line 922
    .local v0, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iput p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    .line 923
    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    .line 924
    iput-object p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    .line 925
    iput-object p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->key:Ljava/lang/String;

    .line 926
    iput-object p4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->touchFunction:Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;

    .line 928
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 929
    return-void
.end method

.method public addSeekbar(IIILandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;IILcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;)V
    .locals 2
    .param p1, "buttonType"    # I
    .param p2, "leftIconId"    # I
    .param p3, "rightIconId"    # I
    .param p4, "leftListener"    # Landroid/view/View$OnClickListener;
    .param p5, "rightListener"    # Landroid/view/View$OnClickListener;
    .param p6, "startPos"    # I
    .param p7, "seekBarMax"    # I
    .param p8, "listener"    # Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    .prologue
    .line 934
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    .line 935
    .local v0, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iput p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    .line 936
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->iconId:I

    .line 937
    iput p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->iconId2:I

    .line 938
    iput p6, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarStartPos:I

    .line 939
    iput p7, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarMax:I

    .line 940
    iput-object p8, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarListener:Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout$OnSeekbarTextCallback;

    .line 941
    iput-object p4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarLeftListener:Landroid/view/View$OnClickListener;

    .line 942
    iput-object p5, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->seekBarRightListener:Landroid/view/View$OnClickListener;

    .line 944
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogButtonList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 945
    return-void
.end method

.method public cancelDialog()Z
    .locals 5

    .prologue
    .line 746
    const/4 v3, 0x0

    .line 747
    .local v3, "isHided":Z
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 749
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 765
    .end local v2    # "i":I
    :cond_0
    return v3

    .line 751
    .restart local v2    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 752
    .local v1, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    .line 753
    .local v0, "d":Landroid/app/Dialog;
    if-eqz v0, :cond_3

    .line 755
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 756
    or-int/lit8 v3, v3, 0x1

    .line 758
    :cond_2
    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    .line 761
    :cond_3
    const/4 v4, 0x0

    iput-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    .line 749
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public changeLanguage()V
    .locals 14

    .prologue
    const v13, 0x7f090005

    .line 132
    const/4 v7, 0x0

    .line 133
    .local v7, "showDialogInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-nez v10, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v5, v10, :cond_6

    .line 154
    if-eqz v7, :cond_0

    .line 156
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    const v11, 0x7f090017

    invoke-virtual {v10, v11}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 157
    .local v9, "titleView":Landroid/widget/TextView;
    if-eqz v9, :cond_2

    .line 159
    iget v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->titleId:I

    if-eqz v10, :cond_2

    .line 160
    iget v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->titleId:I

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 162
    :cond_2
    iget v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialogType:I

    const/16 v11, 0x1000

    if-ne v10, v11, :cond_f

    .line 164
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    instance-of v10, v10, Landroid/app/AlertDialog;

    if-eqz v10, :cond_0

    .line 166
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    if-eqz v10, :cond_3

    .line 168
    const/4 v5, 0x0

    :goto_2
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v5, v10, :cond_b

    .line 201
    :cond_3
    iget-object v0, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    check-cast v0, Landroid/app/AlertDialog;

    .line 202
    .local v0, "aDialog":Landroid/app/AlertDialog;
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    if-eqz v10, :cond_4

    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget v10, v10, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    if-eqz v10, :cond_4

    .line 204
    const/4 v10, -0x1

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    .line 205
    .local v2, "button":Landroid/widget/Button;
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget v10, v10, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setText(I)V

    .line 207
    .end local v2    # "button":Landroid/widget/Button;
    :cond_4
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    if-eqz v10, :cond_5

    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget v10, v10, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    if-eqz v10, :cond_5

    .line 209
    const/4 v10, -0x3

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    .line 210
    .restart local v2    # "button":Landroid/widget/Button;
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget v10, v10, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setText(I)V

    .line 212
    .end local v2    # "button":Landroid/widget/Button;
    :cond_5
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    if-eqz v10, :cond_0

    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget v10, v10, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    if-eqz v10, :cond_0

    .line 214
    const/4 v10, -0x2

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    .line 215
    .restart local v2    # "button":Landroid/widget/Button;
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget v10, v10, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_0

    .line 137
    .end local v0    # "aDialog":Landroid/app/AlertDialog;
    .end local v2    # "button":Landroid/widget/Button;
    .end local v9    # "titleView":Landroid/widget/TextView;
    :cond_6
    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 138
    .local v4, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget-object v10, v4, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v10, :cond_7

    iget-object v10, v4, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v10}, Landroid/app/Dialog;->isShowing()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 140
    move-object v7, v4

    .line 142
    :cond_7
    iget-object v10, v4, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    if-eqz v10, :cond_9

    iget-object v10, v4, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_9

    .line 143
    iget-object v10, v4, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_8
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_a

    .line 135
    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 143
    :cond_a
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    .line 144
    .local v6, "info":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iget-object v11, v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;

    if-eqz v11, :cond_8

    iget v11, v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    if-lez v11, :cond_8

    .line 146
    :try_start_0
    iget-object v11, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    iget v12, v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    invoke-static {v11, v12}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->text:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 147
    :catch_0
    move-exception v11

    goto :goto_3

    .line 170
    .end local v4    # "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    .end local v6    # "info":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    .restart local v9    # "titleView":Landroid/widget/TextView;
    :cond_b
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    .line 171
    .local v1, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    sparse-switch v10, :sswitch_data_0

    .line 168
    :cond_c
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 174
    :sswitch_0
    iget-object v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 175
    .local v8, "textView":Landroid/widget/TextView;
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    if-eqz v10, :cond_d

    .line 176
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(I)V

    .line 177
    :cond_d
    iget-object v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    const v11, 0x7f09001d

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 179
    .local v3, "checkBox":Landroid/widget/CheckBox;
    if-eqz v3, :cond_c

    .line 180
    const v10, 0x7f060072

    invoke-virtual {v3, v10}, Landroid/widget/CheckBox;->setText(I)V

    goto :goto_4

    .line 183
    .end local v3    # "checkBox":Landroid/widget/CheckBox;
    .end local v8    # "textView":Landroid/widget/TextView;
    :sswitch_1
    iget-object v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 184
    .restart local v8    # "textView":Landroid/widget/TextView;
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    if-eqz v10, :cond_c

    .line 185
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(I)V

    goto :goto_4

    .line 188
    .end local v8    # "textView":Landroid/widget/TextView;
    :sswitch_2
    iget-object v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 189
    .restart local v8    # "textView":Landroid/widget/TextView;
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    if-eqz v10, :cond_e

    .line 190
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(I)V

    .line 192
    .end local v8    # "textView":Landroid/widget/TextView;
    :cond_e
    :sswitch_3
    iget-object v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 193
    .restart local v8    # "textView":Landroid/widget/TextView;
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    if-eqz v10, :cond_c

    .line 194
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(I)V

    goto :goto_4

    .line 221
    .end local v1    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    .end local v8    # "textView":Landroid/widget/TextView;
    :cond_f
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    if-eqz v10, :cond_0

    .line 223
    const/4 v5, 0x0

    :goto_5
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v5, v10, :cond_0

    .line 225
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    .line 226
    .restart local v1    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    sparse-switch v10, :sswitch_data_1

    .line 248
    iget-object v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    if-eqz v10, :cond_10

    .line 250
    iget-object v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 251
    .restart local v8    # "textView":Landroid/widget/TextView;
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    if-eqz v10, :cond_10

    .line 252
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(I)V

    .line 223
    .end local v8    # "textView":Landroid/widget/TextView;
    :cond_10
    :goto_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 229
    :sswitch_4
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v10, v13}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 230
    .restart local v8    # "textView":Landroid/widget/TextView;
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    if-eqz v10, :cond_10

    .line 231
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setId(I)V

    goto :goto_6

    .line 234
    .end local v8    # "textView":Landroid/widget/TextView;
    :sswitch_5
    iget-object v10, v7, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v10, v13}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 235
    .restart local v8    # "textView":Landroid/widget/TextView;
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    if-eqz v10, :cond_10

    .line 236
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setId(I)V

    goto :goto_6

    .line 239
    .end local v8    # "textView":Landroid/widget/TextView;
    :sswitch_6
    iget-object v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 240
    .restart local v8    # "textView":Landroid/widget/TextView;
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    if-eqz v10, :cond_11

    .line 241
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(I)V

    .line 243
    .end local v8    # "textView":Landroid/widget/TextView;
    :cond_11
    :sswitch_7
    iget-object v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 244
    .restart local v8    # "textView":Landroid/widget/TextView;
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    if-eqz v10, :cond_10

    .line 245
    iget v10, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->textId:I

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    .line 171
    nop

    :sswitch_data_0
    .sparse-switch
        0x300 -> :sswitch_0
        0x500 -> :sswitch_1
        0x810 -> :sswitch_2
        0x820 -> :sswitch_3
    .end sparse-switch

    .line 226
    :sswitch_data_1
    .sparse-switch
        0x300 -> :sswitch_4
        0x500 -> :sswitch_5
        0x810 -> :sswitch_6
        0x820 -> :sswitch_7
    .end sparse-switch
.end method

.method public destroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 74
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 76
    const/4 v2, 0x0

    .local v2, "i":I
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_2

    .line 111
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 113
    .end local v2    # "i":I
    :cond_1
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    .line 114
    return-void

    .line 78
    .restart local v2    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 87
    .local v1, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iput-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->position:Landroid/graphics/Point;

    .line 88
    iput-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    .line 89
    iput-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    .line 91
    iget-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    .line 92
    .local v0, "buttons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;>;"
    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public disableAnotherButtons(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1071
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    .line 1073
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v4, v6, :cond_1

    .line 1095
    .end local v4    # "i":I
    :cond_0
    return-void

    .line 1075
    .restart local v4    # "i":I
    :cond_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 1076
    .local v3, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget-object v2, v3, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    .line 1078
    .local v2, "buttons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;>;"
    if-eqz v2, :cond_2

    .line 1080
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v5, v6, :cond_3

    .line 1073
    .end local v5    # "j":I
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1082
    .restart local v5    # "j":I
    :cond_3
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    .line 1083
    .local v1, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iget-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    .line 1084
    .local v0, "b":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_4

    .line 1086
    if-eq v0, p1, :cond_4

    .line 1088
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1080
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method public enableButtons()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1098
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    .line 1100
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v4, v7, :cond_1

    .line 1120
    .end local v4    # "i":I
    :cond_0
    return-void

    .line 1102
    .restart local v4    # "i":I
    :cond_1
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 1103
    .local v3, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget-object v2, v3, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    .line 1104
    .local v2, "buttons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;>;"
    if-eqz v2, :cond_2

    .line 1106
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v5, v7, :cond_3

    .line 1100
    .end local v5    # "j":I
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1108
    .restart local v5    # "j":I
    :cond_3
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    .line 1109
    .local v1, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iget-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    .line 1110
    .local v0, "b":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_4

    .line 1112
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1113
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1114
    .local v6, "v":Landroid/view/View;
    invoke-virtual {v6, v8}, Landroid/view/View;->setPressed(Z)V

    .line 1106
    .end local v6    # "v":Landroid/view/View;
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method public finishRegistingButtons()V
    .locals 3

    .prologue
    .line 838
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->getLastDialogInfo()Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    move-result-object v0

    .line 839
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    if-eqz v0, :cond_0

    .line 841
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogButtonList:Ljava/util/ArrayList;

    iput-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    .line 845
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogButtonList:Ljava/util/ArrayList;

    .line 847
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 848
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 857
    .end local v1    # "i":I
    :cond_1
    return-void

    .line 850
    .restart local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 851
    .restart local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    if-nez v2, :cond_3

    .line 853
    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-nez v2, :cond_3

    .line 854
    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;)Z

    .line 848
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getCurrentButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 774
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mCurrentSelectedButton:Landroid/view/View;

    return-object v0
.end method

.method public getPoint(Landroid/app/Dialog;II)Landroid/graphics/Point;
    .locals 5
    .param p1, "d"    # Landroid/app/Dialog;
    .param p2, "width"    # I
    .param p3, "left"    # I

    .prologue
    const/4 v4, 0x0

    .line 1938
    const v3, 0x7f0900a9

    invoke-virtual {p1, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1939
    .local v0, "dialogBody":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 1940
    .local v2, "vGroup":Landroid/view/ViewGroup;
    invoke-virtual {v2, v4, v4}, Landroid/view/ViewGroup;->measure(II)V

    .line 1941
    new-instance v1, Landroid/graphics/Point;

    sub-int v3, p2, p3

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/Gui/DialogsPosition;->getBrushDialogPos()Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-direct {v1, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 1943
    .local v1, "tempPoint":Landroid/graphics/Point;
    return-object v1
.end method

.method public init()V
    .locals 1

    .prologue
    .line 1123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    .line 1124
    return-void
.end method

.method public isDoNotShow(Ljava/lang/String;)Z
    .locals 8
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 116
    const/4 v1, 0x0

    .line 118
    .local v1, "ret":Z
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPackageInfo(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 119
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v3, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 121
    .local v3, "versionCode":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    const-string v6, "PhotoEditor"

    invoke-static {v5, v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 122
    .local v2, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v2, p1, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 124
    .local v4, "viewedChangelogVersion":I
    if-ge v4, v3, :cond_0

    .line 125
    const/4 v1, 0x1

    .line 128
    :goto_0
    return v1

    .line 127
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isRegisterd(I)Z
    .locals 4
    .param p1, "callerId"    # I

    .prologue
    .line 652
    const/4 v2, 0x0

    .line 654
    .local v2, "ret":Z
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 656
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 665
    .end local v1    # "i":I
    :cond_0
    return v2

    .line 658
    .restart local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 659
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    if-ne v3, p1, :cond_2

    .line 661
    const/4 v2, 0x1

    .line 656
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public moveDialog(II)V
    .locals 5
    .param p1, "width"    # I
    .param p2, "left"    # I

    .prologue
    .line 436
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 438
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 452
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 440
    .restart local v1    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 442
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 444
    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {p0, v4, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->getPoint(Landroid/app/Dialog;II)Landroid/graphics/Point;

    move-result-object v3

    .line 445
    .local v3, "point":Landroid/graphics/Point;
    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 446
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v4, v3, Landroid/graphics/Point;->x:I

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 447
    iget v4, v3, Landroid/graphics/Point;->y:I

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 448
    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 438
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v3    # "point":Landroid/graphics/Point;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public moveDialog(Landroid/graphics/Point;)V
    .locals 4
    .param p1, "point"    # Landroid/graphics/Point;

    .prologue
    .line 418
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 420
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 433
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 422
    .restart local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 424
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 426
    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 427
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v3, p1, Landroid/graphics/Point;->x:I

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 428
    iget v3, p1, Landroid/graphics/Point;->y:I

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 429
    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 420
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public registButtons()V
    .locals 1

    .prologue
    .line 834
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogButtonList:Ljava/util/ArrayList;

    .line 835
    return-void
.end method

.method public registDialog(IIIZLandroid/graphics/Point;I)V
    .locals 2
    .param p1, "dialogType"    # I
    .param p2, "callerId"    # I
    .param p3, "titleId"    # I
    .param p4, "isModal"    # Z
    .param p5, "dialogPosition"    # Landroid/graphics/Point;
    .param p6, "theme"    # I

    .prologue
    .line 779
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    .line 780
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iput p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialogType:I

    .line 781
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    .line 782
    iput p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->titleId:I

    .line 783
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isChild:Z

    .line 784
    iput-boolean p4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isModal:Z

    .line 785
    iput-object p5, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->position:Landroid/graphics/Point;

    .line 786
    iput p6, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    .line 788
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 790
    return-void
.end method

.method public registDialog(IIIZZLandroid/graphics/Point;I)V
    .locals 2
    .param p1, "dialogType"    # I
    .param p2, "callerId"    # I
    .param p3, "titleId"    # I
    .param p4, "isModal"    # Z
    .param p5, "isDivider"    # Z
    .param p6, "dialogPosition"    # Landroid/graphics/Point;
    .param p7, "theme"    # I

    .prologue
    .line 794
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    .line 795
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iput p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialogType:I

    .line 796
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    .line 797
    iput p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->titleId:I

    .line 798
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isChild:Z

    .line 799
    iput-boolean p4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isModal:Z

    .line 800
    iput-object p6, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->position:Landroid/graphics/Point;

    .line 801
    iput-boolean p5, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isDivider:Z

    .line 802
    iput p7, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    .line 804
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 806
    return-void
.end method

.method public registDialog(IILjava/lang/String;ZLandroid/graphics/Point;I)V
    .locals 2
    .param p1, "dialogType"    # I
    .param p2, "callerId"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "isModal"    # Z
    .param p5, "dialogPosition"    # Landroid/graphics/Point;
    .param p6, "theme"    # I

    .prologue
    .line 810
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    .line 811
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iput p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialogType:I

    .line 812
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    .line 813
    iput-object p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->title:Ljava/lang/String;

    .line 814
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isChild:Z

    .line 815
    iput-boolean p4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isModal:Z

    .line 816
    iput-object p5, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->position:Landroid/graphics/Point;

    .line 817
    iput p6, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    .line 819
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 820
    return-void
.end method

.method public registSecondDialog(IIIZLandroid/graphics/Point;)V
    .locals 2
    .param p1, "dialogType"    # I
    .param p2, "callerId"    # I
    .param p3, "titleId"    # I
    .param p4, "isModal"    # Z
    .param p5, "dialogPosition"    # Landroid/graphics/Point;

    .prologue
    .line 823
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    .line 824
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iput p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialogType:I

    .line 825
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    .line 826
    iput p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->titleId:I

    .line 827
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isChild:Z

    .line 828
    iput-boolean p4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->isModal:Z

    .line 829
    iput-object p5, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->position:Landroid/graphics/Point;

    .line 830
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 831
    return-void
.end method

.method public resetDialogPosition(ILandroid/graphics/Point;)V
    .locals 3
    .param p1, "callerId"    # I
    .param p2, "newPosition"    # Landroid/graphics/Point;

    .prologue
    .line 710
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 712
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 722
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 714
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 716
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    if-ne v2, p1, :cond_2

    .line 718
    iput-object p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->position:Landroid/graphics/Point;

    .line 712
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setCurrentButton(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 769
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mCurrentSelectedButton:Landroid/view/View;

    .line 770
    return-void
.end method

.method public setDialogPopupLayout(Landroid/widget/LinearLayout;I)V
    .locals 29
    .param p1, "layout"    # Landroid/widget/LinearLayout;
    .param p2, "buttonType"    # I

    .prologue
    .line 264
    const-string v27, "bigheadk, setDialogPopupLayout"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f050265

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v26

    .line 267
    .local v26, "width_port":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f050266

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    .line 269
    .local v25, "width_land":I
    sparse-switch p2, :sswitch_data_0

    .line 379
    :goto_0
    return-void

    .line 272
    :sswitch_0
    const v27, 0x7f090005

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    .line 274
    .local v21, "textView":Landroid/widget/TextView;
    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 276
    .local v9, "ll":Landroid/widget/LinearLayout;
    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 277
    .local v7, "l2":Landroid/widget/LinearLayout;
    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout$LayoutParams;

    .line 279
    .local v10, "lp2":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v27

    if-nez v27, :cond_0

    .line 280
    move/from16 v0, v26

    iput v0, v10, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 284
    :goto_1
    const/16 v27, 0x0

    move/from16 v0, v27

    iput v0, v10, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 285
    const/16 v27, 0x0

    move/from16 v0, v27

    iput v0, v10, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 286
    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 282
    :cond_0
    move/from16 v0, v25

    iput v0, v10, Landroid/widget/LinearLayout$LayoutParams;->width:I

    goto :goto_1

    .line 289
    .end local v7    # "l2":Landroid/widget/LinearLayout;
    .end local v9    # "ll":Landroid/widget/LinearLayout;
    .end local v10    # "lp2":Landroid/widget/LinearLayout$LayoutParams;
    .end local v21    # "textView":Landroid/widget/TextView;
    :sswitch_1
    const v27, 0x7f09001f

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/Gui/InputText;

    .line 290
    .local v3, "editText":Lcom/sec/android/mimage/photoretouching/Gui/InputText;
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 292
    .local v5, "editTextlp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Gui/InputText;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 293
    .local v4, "editTextParent":Landroid/widget/LinearLayout;
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/widget/LinearLayout$LayoutParams;

    .line 295
    .local v14, "parentlp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v27

    if-nez v27, :cond_1

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f050270

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    .line 298
    .local v23, "width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f050271

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 299
    .local v6, "height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f050274

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 300
    .local v8, "left_margin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f050275

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    .line 302
    .local v22, "top_margin":I
    move/from16 v0, v23

    iput v0, v14, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 303
    iput v6, v14, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 304
    const/16 v27, 0x11

    move/from16 v0, v27

    iput v0, v14, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 305
    mul-int/lit8 v27, v8, 0x2

    sub-int v27, v23, v27

    move/from16 v0, v27

    iput v0, v5, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 306
    mul-int/lit8 v27, v22, 0x2

    sub-int v27, v6, v27

    move/from16 v0, v27

    iput v0, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 310
    .end local v6    # "height":I
    .end local v8    # "left_margin":I
    .end local v22    # "top_margin":I
    .end local v23    # "width":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f050272

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    .line 311
    .restart local v23    # "width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f050273

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 312
    .restart local v6    # "height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f050276

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 313
    .restart local v8    # "left_margin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f050277

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    .line 315
    .restart local v22    # "top_margin":I
    move/from16 v0, v23

    iput v0, v14, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 316
    iput v6, v14, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 317
    const/16 v27, 0x11

    move/from16 v0, v27

    iput v0, v14, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 318
    mul-int/lit8 v27, v8, 0x2

    sub-int v27, v23, v27

    move/from16 v0, v27

    iput v0, v5, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 319
    mul-int/lit8 v27, v22, 0x2

    sub-int v27, v6, v27

    move/from16 v0, v27

    iput v0, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_0

    .line 325
    .end local v3    # "editText":Lcom/sec/android/mimage/photoretouching/Gui/InputText;
    .end local v4    # "editTextParent":Landroid/widget/LinearLayout;
    .end local v5    # "editTextlp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v6    # "height":I
    .end local v8    # "left_margin":I
    .end local v14    # "parentlp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v22    # "top_margin":I
    .end local v23    # "width":I
    :sswitch_2
    const v27, 0x7f090005

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 327
    .local v19, "question_tv":Landroid/widget/TextView;
    invoke-virtual/range {v19 .. v19}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v16

    check-cast v16, Landroid/widget/LinearLayout;

    .line 328
    .local v16, "question_ll":Landroid/widget/LinearLayout;
    invoke-virtual/range {v16 .. v16}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout$LayoutParams;

    .line 330
    .local v17, "question_lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual/range {v16 .. v16}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v15

    check-cast v15, Landroid/widget/LinearLayout;

    .line 331
    .local v15, "question_l2":Landroid/widget/LinearLayout;
    invoke-virtual {v15}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout$LayoutParams;

    .line 333
    .local v18, "question_lp2":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual/range {v19 .. v19}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    check-cast v20, Landroid/widget/LinearLayout$LayoutParams;

    .line 335
    .local v20, "question_tv_lp":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f05026b

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 337
    .local v11, "margin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f05026c

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 339
    .local v12, "margin1":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f050278

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    .line 340
    .local v13, "margin_21":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 341
    .local v2, "config":Landroid/content/res/Configuration;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v27

    if-nez v27, :cond_3

    .line 342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f050267

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    .line 344
    .restart local v23    # "width":I
    add-int v27, v23, v13

    move/from16 v0, v27

    move-object/from16 v1, v17

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 345
    add-int v27, v23, v13

    move/from16 v0, v27

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 347
    move/from16 v0, v23

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 348
    const/16 v27, 0x10

    move/from16 v0, v27

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 349
    invoke-virtual {v2}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v27

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_2

    .line 350
    move-object/from16 v0, v20

    iput v13, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 374
    :cond_2
    :goto_2
    invoke-virtual/range {v16 .. v17}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 375
    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 376
    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 353
    .end local v23    # "width":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f050268

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    .line 355
    .restart local v23    # "width":I
    move/from16 v0, v23

    move-object/from16 v1, v17

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 356
    move/from16 v0, v23

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 358
    move/from16 v0, v23

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 359
    const/16 v27, 0x11

    move/from16 v0, v27

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 360
    invoke-virtual {v2}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v27

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_4

    .line 361
    move-object/from16 v0, v20

    iput v13, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_2

    .line 363
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f05026a

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v24

    .line 365
    .local v24, "width1":I
    move/from16 v0, v24

    move-object/from16 v1, v17

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 366
    move/from16 v0, v24

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 368
    sub-int v27, v24, v12

    move/from16 v0, v27

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 369
    const/16 v27, 0x11

    move/from16 v0, v27

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 370
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, v20

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_2

    .line 269
    :sswitch_data_0
    .sparse-switch
        0x300 -> :sswitch_0
        0x500 -> :sswitch_2
        0x900 -> :sswitch_1
    .end sparse-switch
.end method

.method public setDialogView(ILandroid/view/View;)V
    .locals 3
    .param p1, "callerId"    # I
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 669
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 671
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 682
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 673
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 675
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    if-ne v2, p1, :cond_2

    .line 677
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->hasContentView:Z

    .line 678
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogContentView:Landroid/view/View;

    .line 671
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V
    .locals 3
    .param p1, "textId"    # I
    .param p2, "onClickListener"    # Landroid/content/DialogInterface$OnClickListener;

    .prologue
    .line 986
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 988
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 990
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 991
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialogType:I

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_1

    .line 993
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    .line 994
    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iput p1, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    .line 995
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 996
    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->text:Ljava/lang/String;

    .line 998
    :cond_0
    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->negative:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iput-object p2, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->listener:Landroid/content/DialogInterface$OnClickListener;

    .line 1002
    .end local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    :cond_1
    return-void
.end method

.method public setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)V
    .locals 3
    .param p1, "textId"    # I
    .param p2, "onClickListener"    # Landroid/content/DialogInterface$OnClickListener;

    .prologue
    .line 1005
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1007
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1009
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 1010
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialogType:I

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_1

    .line 1012
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    .line 1013
    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iput p1, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    .line 1014
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1015
    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->text:Ljava/lang/String;

    .line 1017
    :cond_0
    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->neutral:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iput-object p2, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->listener:Landroid/content/DialogInterface$OnClickListener;

    .line 1021
    .end local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    :cond_1
    return-void
.end method

.method public setPointerDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 1128
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mPointerDirection:I

    .line 1129
    return-void
.end method

.method public setPopupLayout()V
    .locals 6

    .prologue
    .line 384
    const/4 v3, 0x0

    .line 385
    .local v3, "showDialogInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-nez v4, :cond_1

    .line 414
    :cond_0
    return-void

    .line 387
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_2

    .line 397
    :goto_1
    if-eqz v3, :cond_0

    .line 399
    iget v4, v3, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialogType:I

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_0

    .line 401
    iget-object v4, v3, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    instance-of v4, v4, Landroid/app/AlertDialog;

    if-eqz v4, :cond_0

    .line 403
    iget-object v4, v3, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 405
    const/4 v2, 0x0

    :goto_2
    iget-object v4, v3, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 407
    iget-object v4, v3, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    .line 408
    .local v0, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    iget v5, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->buttonType:I

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setDialogPopupLayout(Landroid/widget/LinearLayout;I)V

    .line 405
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 389
    .end local v0    # "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 390
    .local v1, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v4, :cond_3

    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 392
    move-object v3, v1

    .line 393
    goto :goto_1

    .line 387
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V
    .locals 3
    .param p1, "textId"    # I
    .param p2, "onClickListener"    # Landroid/content/DialogInterface$OnClickListener;

    .prologue
    .line 950
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 952
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 954
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 955
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialogType:I

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_0

    .line 957
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    .line 958
    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iput p1, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    .line 959
    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->text:Ljava/lang/String;

    .line 960
    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iput-object p2, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->listener:Landroid/content/DialogInterface$OnClickListener;

    .line 964
    .end local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    :cond_0
    return-void
.end method

.method public setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V
    .locals 3
    .param p1, "textId"    # I
    .param p2, "onShowListener"    # Landroid/content/DialogInterface$OnShowListener;

    .prologue
    .line 967
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 969
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 971
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 972
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialogType:I

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_1

    .line 974
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;)V

    iput-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    .line 975
    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iput p1, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->textId:I

    .line 976
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 977
    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mContext:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->text:Ljava/lang/String;

    .line 979
    :cond_0
    iget-object v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->positive:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;

    iput-object p2, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$AlertDialogButtonInfo;->showlistener:Landroid/content/DialogInterface$OnShowListener;

    .line 983
    .end local v0    # "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    :cond_1
    return-void
.end method

.method public setSelectedFalseAnotherButtons(I)V
    .locals 11
    .param p1, "id"    # I

    .prologue
    const v10, 0x7f09001e

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1029
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    .line 1031
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v4, v7, :cond_1

    .line 1066
    .end local v4    # "i":I
    :cond_0
    return-void

    .line 1033
    .restart local v4    # "i":I
    :cond_1
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 1034
    .local v3, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget-object v2, v3, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    .line 1036
    .local v2, "buttons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;>;"
    if-eqz v2, :cond_2

    .line 1038
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v5, v7, :cond_3

    .line 1031
    .end local v5    # "j":I
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1040
    .restart local v5    # "j":I
    :cond_3
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    .line 1041
    .local v1, "bInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;
    if-eqz v1, :cond_4

    .line 1043
    iget-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    .line 1044
    .local v0, "b":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_4

    .line 1046
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v7

    if-eq v7, p1, :cond_5

    .line 1048
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 1049
    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RadioButton;

    .line 1050
    .local v6, "radioBtn":Landroid/widget/RadioButton;
    if-eqz v6, :cond_4

    .line 1051
    invoke-virtual {v6, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 1038
    .end local v0    # "b":Landroid/widget/LinearLayout;
    .end local v6    # "radioBtn":Landroid/widget/RadioButton;
    :cond_4
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1055
    .restart local v0    # "b":Landroid/widget/LinearLayout;
    :cond_5
    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 1056
    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RadioButton;

    .line 1057
    .restart local v6    # "radioBtn":Landroid/widget/RadioButton;
    if-eqz v6, :cond_4

    .line 1058
    invoke-virtual {v6, v9}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_2
.end method

.method public setSelectedFalseAnotherButtons(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1024
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setSelectedFalseAnotherButtons(I)V

    .line 1025
    return-void
.end method

.method public setTextToDialog(Ljava/lang/String;I)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "callerId"    # I

    .prologue
    .line 687
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 689
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 705
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 691
    .restart local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 692
    .local v0, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    if-ne v3, p2, :cond_2

    .line 694
    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    .line 696
    const/4 v2, 0x0

    .local v2, "j":I
    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 698
    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;

    iget-object v3, v3, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$ButtonInfo;->button:Landroid/widget/LinearLayout;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;

    invoke-virtual {v3, p1}, Lcom/sec/android/mimage/photoretouching/Gui/DialogContextLayout;->setEditText(Ljava/lang/String;)V

    .line 689
    .end local v2    # "j":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public showDialog(I)Z
    .locals 7
    .param p1, "callerId"    # I

    .prologue
    const/4 v6, 0x0

    .line 455
    const/4 v3, 0x0

    .line 457
    .local v3, "ret":Z
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 459
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 510
    .end local v2    # "i":I
    :cond_0
    return v3

    .line 461
    .restart local v2    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 462
    .local v1, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    if-ne v4, p1, :cond_6

    .line 464
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v4, :cond_3

    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 466
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->cancel()V

    .line 467
    iput-object v6, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    .line 459
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 473
    :cond_3
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->buildDialog(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;)Landroid/app/Dialog;

    move-result-object v0

    .line 474
    .local v0, "d":Landroid/app/Dialog;
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->title:Ljava/lang/String;

    if-eqz v4, :cond_5

    .line 476
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->title:Ljava/lang/String;

    iget v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    invoke-direct {p0, v0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTitle(Landroid/app/Dialog;Ljava/lang/String;I)V

    .line 483
    :goto_2
    iget-boolean v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->hasContentView:Z

    if-nez v4, :cond_4

    .line 485
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setContext(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;Landroid/app/Dialog;Ljava/util/ArrayList;)V

    .line 486
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->position:Landroid/graphics/Point;

    invoke-direct {p0, v0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPosition(Landroid/app/Dialog;Landroid/graphics/Point;)V

    .line 489
    :cond_4
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 490
    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    .line 491
    const-string v4, "JW showDialog"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_1

    .line 480
    :cond_5
    iget v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->titleId:I

    iget v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    invoke-direct {p0, v0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTitle(Landroid/app/Dialog;II)V

    goto :goto_2

    .line 500
    .end local v0    # "d":Landroid/app/Dialog;
    :cond_6
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v4, :cond_2

    .line 502
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->cancel()V

    .line 503
    iput-object v6, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    .line 504
    const-string v4, "JW showDialog: cancel22"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public showDialog(III)Z
    .locals 8
    .param p1, "callerId"    # I
    .param p2, "width"    # I
    .param p3, "left"    # I

    .prologue
    const/4 v7, 0x0

    .line 560
    const/4 v4, 0x0

    .line 561
    .local v4, "ret":Z
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 563
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v2, v5, :cond_1

    .line 603
    .end local v2    # "i":I
    :cond_0
    return v4

    .line 565
    .restart local v2    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 566
    .local v1, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    if-ne v5, p1, :cond_6

    .line 568
    iget-object v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v5, :cond_3

    .line 570
    iget-object v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->cancel()V

    .line 571
    iput-object v7, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    .line 563
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 575
    :cond_3
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->buildDialog(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;)Landroid/app/Dialog;

    move-result-object v0

    .line 576
    .local v0, "d":Landroid/app/Dialog;
    iget-object v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->title:Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 577
    iget-object v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->title:Ljava/lang/String;

    iget v6, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    invoke-direct {p0, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTitle(Landroid/app/Dialog;Ljava/lang/String;I)V

    .line 581
    :goto_2
    iget-boolean v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->hasContentView:Z

    if-nez v5, :cond_4

    .line 583
    iget-object v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v0, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setContext(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;Landroid/app/Dialog;Ljava/util/ArrayList;)V

    .line 585
    :cond_4
    invoke-virtual {p0, v0, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->getPoint(Landroid/app/Dialog;II)Landroid/graphics/Point;

    move-result-object v3

    .line 586
    .local v3, "pt":Landroid/graphics/Point;
    invoke-direct {p0, v0, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPosition(Landroid/app/Dialog;Landroid/graphics/Point;)V

    .line 588
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 589
    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    goto :goto_1

    .line 580
    .end local v3    # "pt":Landroid/graphics/Point;
    :cond_5
    iget v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->titleId:I

    iget v6, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    invoke-direct {p0, v0, v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTitle(Landroid/app/Dialog;II)V

    goto :goto_2

    .line 595
    .end local v0    # "d":Landroid/app/Dialog;
    :cond_6
    iget-object v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v5, :cond_2

    .line 597
    iget-object v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->cancel()V

    .line 598
    iput-object v7, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    goto :goto_1
.end method

.method public showDialog(ILandroid/graphics/Point;)Z
    .locals 7
    .param p1, "callerId"    # I
    .param p2, "pt"    # Landroid/graphics/Point;

    .prologue
    const/4 v6, 0x0

    .line 514
    const/4 v3, 0x0

    .line 515
    .local v3, "ret":Z
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 517
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 555
    .end local v2    # "i":I
    :cond_0
    return v3

    .line 519
    .restart local v2    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 520
    .local v1, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    if-ne v4, p1, :cond_6

    .line 522
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v4, :cond_3

    .line 524
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->cancel()V

    .line 525
    iput-object v6, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    .line 517
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 529
    :cond_3
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->buildDialog(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;)Landroid/app/Dialog;

    move-result-object v0

    .line 530
    .local v0, "d":Landroid/app/Dialog;
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->title:Ljava/lang/String;

    if-eqz v4, :cond_5

    .line 531
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->title:Ljava/lang/String;

    iget v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    invoke-direct {p0, v0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTitle(Landroid/app/Dialog;Ljava/lang/String;I)V

    .line 534
    :goto_2
    iget-boolean v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->hasContentView:Z

    if-nez v4, :cond_4

    .line 536
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setContext(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;Landroid/app/Dialog;Ljava/util/ArrayList;)V

    .line 539
    :cond_4
    invoke-direct {p0, v0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPosition(Landroid/app/Dialog;Landroid/graphics/Point;)V

    .line 541
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 542
    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    goto :goto_1

    .line 533
    :cond_5
    iget v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->titleId:I

    iget v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    invoke-direct {p0, v0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTitle(Landroid/app/Dialog;II)V

    goto :goto_2

    .line 547
    .end local v0    # "d":Landroid/app/Dialog;
    :cond_6
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v4, :cond_2

    .line 549
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->cancel()V

    .line 550
    iput-object v6, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    goto :goto_1
.end method

.method public showDialog(ILandroid/graphics/Point;I)Z
    .locals 7
    .param p1, "callerId"    # I
    .param p2, "pt"    # Landroid/graphics/Point;
    .param p3, "direction"    # I

    .prologue
    const/4 v6, 0x0

    .line 607
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mPointerDirection:I

    .line 608
    const/4 v3, 0x0

    .line 609
    .local v3, "ret":Z
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 611
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 648
    .end local v2    # "i":I
    :cond_0
    return v3

    .line 613
    .restart local v2    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->mDialogList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;

    .line 614
    .local v1, "dInfo":Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;
    iget v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->callerId:I

    if-ne v4, p1, :cond_6

    .line 616
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v4, :cond_3

    .line 618
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->cancel()V

    .line 619
    iput-object v6, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    .line 611
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 623
    :cond_3
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->buildDialog(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;)Landroid/app/Dialog;

    move-result-object v0

    .line 624
    .local v0, "d":Landroid/app/Dialog;
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->title:Ljava/lang/String;

    if-eqz v4, :cond_5

    .line 625
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->title:Ljava/lang/String;

    iget v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    invoke-direct {p0, v0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTitle(Landroid/app/Dialog;Ljava/lang/String;I)V

    .line 628
    :goto_2
    iget-boolean v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->hasContentView:Z

    if-nez v4, :cond_4

    .line 630
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->buttonList:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setContext(Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;Landroid/app/Dialog;Ljava/util/ArrayList;)V

    .line 631
    invoke-direct {p0, v0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPosition(Landroid/app/Dialog;Landroid/graphics/Point;)V

    .line 634
    :cond_4
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 635
    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    goto :goto_1

    .line 627
    :cond_5
    iget v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->titleId:I

    iget v5, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->theme:I

    invoke-direct {p0, v0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTitle(Landroid/app/Dialog;II)V

    goto :goto_2

    .line 640
    .end local v0    # "d":Landroid/app/Dialog;
    :cond_6
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    if-eqz v4, :cond_2

    .line 642
    iget-object v4, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->cancel()V

    .line 643
    iput-object v6, v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager$DialogInfo;->dialog:Landroid/app/Dialog;

    goto :goto_1
.end method
