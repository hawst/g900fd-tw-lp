.class public Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;
.super Ljava/lang/Object;
.source "DefaultButtonsListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;,
        Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    }
.end annotation


# instance fields
.field private mAnimationCallback:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;

.field private mButton:Landroid/view/View;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mIsFunction:Z

.field private mList:Ljava/util/ArrayList;

.field private mLongPress:Z

.field private mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "list"    # Ljava/util/ArrayList;
    .param p3, "touchInterface"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .param p4, "longClick"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mIsFunction:Z

    .line 169
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mLongPress:Z

    .line 170
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    .line 171
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    .line 172
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mAnimationCallback:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;

    .line 173
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    .line 174
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mButton:Landroid/view/View;

    .line 31
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    .line 32
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    .line 33
    iput-boolean p4, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mLongPress:Z

    .line 34
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mLongPress:Z

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 36
    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$1;-><init>(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 62
    :goto_0
    return-void

    .line 59
    :cond_0
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;)Landroid/view/View;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;Landroid/view/GestureDetector;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    return-void
.end method

.method private disableAnotherButtons(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 70
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 72
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 81
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 74
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 75
    .local v0, "b":Landroid/view/View;
    if-eq v0, p1, :cond_2

    .line 77
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 72
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private enableButtons(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 84
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 86
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 96
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 88
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 89
    .local v0, "b":Landroid/view/View;
    if-eq v0, p1, :cond_3

    .line 91
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x15001506

    if-ne v2, v3, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v2

    const v3, 0x3e99999a    # 0.3f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    .line 92
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 86
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private resetPressState(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 99
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 100
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 101
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 106
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mAnimationCallback:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mAnimationCallback:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;->isAnimation()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v2

    .line 111
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mLongPress:Z

    if-eqz v0, :cond_2

    .line 113
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    .line 115
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mButton:Landroid/view/View;

    .line 116
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    if-eqz v0, :cond_3

    .line 122
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;->TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 123
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 125
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->resetPressState(Landroid/view/View;)V

    .line 126
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    .line 127
    invoke-virtual {p1, v2}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 128
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->disableAnotherButtons(Landroid/view/View;)V

    goto :goto_0

    .line 131
    :pswitch_1
    invoke-static {p1, p2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isInButton(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 133
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 137
    :cond_4
    invoke-virtual {p1, v3}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 143
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mIsFunction:Z

    if-nez v0, :cond_9

    .line 144
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mIsFunction:Z

    .line 146
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_6

    .line 147
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->isCheckPreventRetouchButton(I)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_6

    .line 148
    :cond_5
    invoke-virtual {p1, v3}, Landroid/view/View;->playSoundEffect(I)V

    .line 150
    :cond_6
    invoke-virtual {p1, v3}, Landroid/view/View;->setPressed(Z)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    if-eqz v0, :cond_8

    .line 152
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->isCheckPreventRetouchButton(I)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_8

    .line 153
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mTouchInterface:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;->TouchFunction(Landroid/view/View;)V

    .line 155
    :cond_8
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mIsFunction:Z

    .line 157
    :cond_9
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->enableButtons(Landroid/view/View;)V

    goto/16 :goto_0

    .line 160
    :pswitch_3
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 161
    invoke-virtual {p1, v3}, Landroid/view/View;->setPressed(Z)V

    .line 162
    :cond_a
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->enableButtons(Landroid/view/View;)V

    goto/16 :goto_0

    .line 123
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setAnimationCallback(Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener;->mAnimationCallback:Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$AnimationCallback;

    .line 67
    return-void
.end method
