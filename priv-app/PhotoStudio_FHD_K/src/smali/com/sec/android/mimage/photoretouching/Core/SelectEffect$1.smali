.class Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;
.super Ljava/lang/Object;
.source "SelectEffect.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$OnTouchDown;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->touchDown([ILandroid/view/MotionEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

.field private final synthetic val$previewBuffer:[I

.field private final synthetic val$time:J

.field private final synthetic val$x:F

.field private final synthetic val$y:F


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;FF[IJ)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$x:F

    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$y:F

    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$previewBuffer:[I

    iput-wide p5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$time:J

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public firstTouchTime()J
    .locals 2

    .prologue
    .line 296
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$time:J

    return-wide v0
.end method

.method public touchDown(Landroid/view/MotionEvent;)V
    .locals 10
    .param p1, "currentEvent"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 180
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-static {v0, v6}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$0(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;Z)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$1(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Paint;

    move-result-object v0

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$1(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Paint;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPathEffect:Landroid/graphics/DashPathEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$2(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/DashPathEffect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 184
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$1(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Paint;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$1(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$3(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;Landroid/graphics/Paint;)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$4(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Paint;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$4(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Paint;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mDrawLinePaint1:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$4(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$5(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-static {v0, v8}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$6(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;Z)V

    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "touchDown start:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$5(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 196
    new-instance v9, Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$5(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$5(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-direct {v9, v8, v8, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 197
    .local v9, "roi":Landroid/graphics/Rect;
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$x:F

    float-to-int v0, v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$y:F

    float-to-int v1, v1

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    const-string v0, "touchDown start"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 200
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-gt v0, v6, :cond_1

    .line 202
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mBrushSize:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$7(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v1

    # invokes: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->updataBrushSize(I)V
    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$8(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;I)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    invoke-static {v0, v6}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$6(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;Z)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$x:F

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$y:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getRealPosition(FF)Landroid/graphics/Point;

    move-result-object v4

    .line 207
    .local v4, "point":Landroid/graphics/Point;
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 208
    .local v7, "r":Landroid/graphics/Rect;
    invoke-virtual {v7}, Landroid/graphics/Rect;->setEmpty()V

    .line 209
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectType:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$9(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v0

    const v1, 0x14001408

    if-ne v0, v1, :cond_2

    .line 211
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectDrawType:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$10(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 235
    :goto_0
    invoke-virtual {v7}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    invoke-virtual {v0, v7}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$14(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Point;

    move-result-object v0

    iget v1, v4, Landroid/graphics/Point;->x:I

    iget v2, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mFirstPt:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$17(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Point;

    move-result-object v0

    iget v1, v4, Landroid/graphics/Point;->x:I

    iget v2, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 239
    const/4 v4, 0x0

    .line 240
    const/4 v7, 0x0

    .line 292
    .end local v4    # "point":Landroid/graphics/Point;
    .end local v7    # "r":Landroid/graphics/Rect;
    .end local v9    # "roi":Landroid/graphics/Rect;
    :cond_1
    :goto_1
    return-void

    .line 214
    .restart local v4    # "point":Landroid/graphics/Point;
    .restart local v7    # "r":Landroid/graphics/Rect;
    .restart local v9    # "roi":Landroid/graphics/Rect;
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$previewBuffer:[I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$11(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$12(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$13(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v3

    .line 215
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$14(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Point;

    move-result-object v5

    .line 214
    invoke-static/range {v0 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    goto :goto_0

    .line 218
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$previewBuffer:[I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$11(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$12(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$13(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v3

    .line 219
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$14(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Point;

    move-result-object v5

    const/4 v6, 0x3

    .line 218
    invoke-static/range {v0 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    goto :goto_0

    .line 222
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$previewBuffer:[I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$11(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$12(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$13(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v3

    .line 223
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$14(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Point;

    move-result-object v5

    const/4 v6, 0x5

    .line 222
    invoke-static/range {v0 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mLassoPath:Landroid/graphics/Path;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$15(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 225
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mLassoPath:Landroid/graphics/Path;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$15(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Path;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$x:F

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    goto/16 :goto_0

    .line 229
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$16(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$x:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 230
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$16(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$y:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 231
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$16(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$x:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 232
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$16(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$y:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    .line 244
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectType:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$9(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v0

    const v1, 0x14001407

    if-ne v0, v1, :cond_3

    .line 250
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewPath:Landroid/graphics/Path;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$18(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 251
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$11(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)[B

    move-result-object v0

    invoke-static {v0, v8}, Ljava/util/Arrays;->fill([BB)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$5(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 253
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 257
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectDrawType:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$10(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 281
    :goto_2
    invoke-virtual {v7}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 282
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewRoi:Landroid/graphics/Rect;

    invoke-virtual {v0, v7}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 283
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$14(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Point;

    move-result-object v0

    iget v1, v4, Landroid/graphics/Point;->x:I

    iget v2, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mFirstPt:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$17(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Point;

    move-result-object v0

    iget v1, v4, Landroid/graphics/Point;->x:I

    iget v2, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 285
    const/4 v4, 0x0

    .line 286
    goto/16 :goto_1

    .line 260
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$previewBuffer:[I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$11(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$12(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$13(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v3

    .line 261
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$14(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Point;

    move-result-object v5

    move v6, v8

    .line 260
    invoke-static/range {v0 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    goto :goto_2

    .line 264
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$previewBuffer:[I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$11(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$12(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$13(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v3

    .line 265
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$14(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Point;

    move-result-object v5

    const/4 v6, 0x2

    .line 264
    invoke-static/range {v0 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    goto :goto_2

    .line 268
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$previewBuffer:[I

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewMask:[B
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$11(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewWidth:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$12(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPreviewHeight:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$13(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)I

    move-result v3

    .line 269
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mPrePt:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$14(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Point;

    move-result-object v5

    const/4 v6, 0x4

    .line 268
    invoke-static/range {v0 .. v8}, Lcom/sec/android/mimage/photoretouching/jni/Util;->modify_objectsel([I[BIILandroid/graphics/Point;Landroid/graphics/Point;ILandroid/graphics/Rect;I)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mLassoPath:Landroid/graphics/Path;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$15(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 271
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mLassoPath:Landroid/graphics/Path;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$15(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/Path;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$x:F

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    goto/16 :goto_2

    .line 275
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$16(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$x:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 276
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$16(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$y:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 277
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$16(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$x:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 278
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->this$0:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    # getter for: Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->mSelectRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->access$16(Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;)Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect$1;->val$y:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_2

    .line 211
    nop

    :pswitch_data_0
    .packed-switch 0x14001401
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 257
    :pswitch_data_1
    .packed-switch 0x14001401
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
