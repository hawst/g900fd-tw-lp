.class Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;
.super Ljava/lang/Object;
.source "EffectView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->init2DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    .line 844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 878
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->UNDOALL_DIALOG:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$35(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 880
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 848
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewWidth()I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$32(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->getImageEditViewHeight()I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$33(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->undo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    .line 849
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 850
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 851
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 852
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 853
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 866
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 867
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 868
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 873
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->refreshImageAndBottomButtons()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$34(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)V

    .line 874
    return-void

    .line 855
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_0

    .line 857
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    .line 858
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableSave()V

    .line 859
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 860
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 863
    :goto_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setIsEdited(Z)V

    goto :goto_0

    .line 862
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v5, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_2

    .line 870
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 871
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView$16;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/EffectView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 886
    return-void
.end method
