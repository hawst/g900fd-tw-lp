.class Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$2;
.super Ljava/lang/Object;
.source "MirrorAnimationView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->createThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 183
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mIsLoop:Z
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->access$0(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 197
    return-void

    .line 184
    :cond_1
    const/4 v0, 0x0

    .line 186
    .local v0, "canvas":Landroid/graphics/Canvas;
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;)Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 188
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;)Landroid/view/SurfaceHolder;

    move-result-object v2

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 189
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->drawCanvas(Landroid/graphics/Canvas;)V
    invoke-static {v1, v0}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->access$2(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;Landroid/graphics/Canvas;)V

    .line 188
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 192
    if-eqz v0, :cond_0

    .line 193
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;)Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 188
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 191
    :catchall_1
    move-exception v1

    .line 192
    if-eqz v0, :cond_2

    .line 193
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView$2;->this$0:Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->mHolder:Landroid/view/SurfaceHolder;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;->access$1(Lcom/sec/android/mimage/photoretouching/Gui/MirrorAnimationView;)Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 195
    :cond_2
    throw v1
.end method
