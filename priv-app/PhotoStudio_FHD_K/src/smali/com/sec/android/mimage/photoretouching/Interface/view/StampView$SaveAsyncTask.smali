.class Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;
.super Landroid/os/AsyncTask;
.source "StampView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)V
    .locals 0

    .prologue
    .line 5469
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 5470
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 9
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 5474
    const/4 v3, 0x0

    .line 5475
    .local v3, "fileName":Ljava/lang/String;
    array-length v6, p1

    if-lez v6, :cond_0

    .line 5476
    const/4 v6, 0x0

    aget-object v3, p1, v6

    .line 5478
    :cond_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getAvailableExternalMemorySize()J

    move-result-wide v0

    .line 5479
    .local v0, "available_memsize":J
    const-wide/32 v6, 0xa00000

    cmp-long v6, v0, v6

    if-gez v6, :cond_3

    .line 5480
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 5481
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->dismiss()V

    .line 5482
    :cond_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v6

    .line 5483
    const v7, 0x7f06007b

    .line 5482
    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToast(Landroid/content/Context;I)V

    .line 5514
    :cond_2
    :goto_0
    return-object v8

    .line 5486
    :cond_3
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 5487
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 5488
    const/4 v4, 0x0

    .line 5489
    .local v4, "filePath":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_6

    .line 5490
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v6

    .line 5491
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v7

    .line 5490
    invoke-static {v6, v7}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 5492
    .local v5, "tempPath":Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 5496
    .end local v5    # "tempPath":Ljava/lang/String;
    :goto_1
    if-nez v3, :cond_4

    .line 5497
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    move-result-object v3

    .line 5498
    :cond_4
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 5499
    :goto_2
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isDoingThread()Z

    move-result v6

    if-nez v6, :cond_7

    .line 5509
    :cond_5
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 5510
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v6

    .line 5511
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mCurrentSaveSize:I
    invoke-static {v7}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)I

    move-result v7

    .line 5510
    invoke-virtual {v6, v4, v3, v7}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->saveCurrentImage(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 5494
    :cond_6
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 5501
    :cond_7
    const-wide/16 v6, 0x12c

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 5502
    :catch_0
    move-exception v2

    .line 5504
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const v4, 0x7f0601df

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 5528
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 5529
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 5530
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 5533
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 5534
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isPersonalPage()Z

    move-result v2

    if-nez v2, :cond_4

    .line 5535
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Studio"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 5536
    .local v1, "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    .line 5549
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 5550
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 5551
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 5552
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 5553
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V

    .line 5555
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mOptionItemId:I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)I

    move-result v2

    const v3, 0x7f090156

    if-eq v2, v3, :cond_3

    .line 5557
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mOptionItemId:I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)I

    move-result v3

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->runOptionItem(I)Z
    invoke-static {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;I)Z

    .line 5558
    :cond_3
    return-void

    .line 5538
    .end local v1    # "text":Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPrivateSaveFolder()Ljava/lang/String;

    move-result-object v0

    .line 5539
    .local v0, "privateFolder":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 5540
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    .line 5541
    aput-object v0, v3, v5

    .line 5539
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 5542
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 5545
    .end local v0    # "privateFolder":Ljava/lang/String;
    .end local v1    # "text":Ljava/lang/String;
    :cond_5
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Studio"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 5546
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->showToastShort(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 5518
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 5519
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;Landroid/app/ProgressDialog;)V

    .line 5520
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/content/Context;

    move-result-object v1

    .line 5521
    const v2, 0x7f06008f

    .line 5520
    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 5522
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 5523
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 5524
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView$SaveAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StampView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 5525
    return-void
.end method
