.class Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;
.super Ljava/lang/Object;
.source "FrameListView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Core/ViewPagerAdapter$ViewPagerInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->initView(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/CollageColorPicker;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field maxPage:I

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->maxPage:I

    return-void
.end method


# virtual methods
.method public applyEffect(I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->applyImage(IZZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getBGColor()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameEffect:Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/FrameEffect;->getFrameVectorColor()I

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, "ret":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)I

    move-result v1

    const v2, 0x3120006e

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)I

    move-result v1

    const v2, 0x31200077

    if-gt v1, v2, :cond_0

    .line 55
    const/16 v0, 0xa

    .line 60
    :goto_0
    return v0

    .line 56
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)I

    move-result v1

    const v2, 0x31200078

    if-lt v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)I

    move-result v1

    const v2, 0x3120007e

    if-gt v1, v2, :cond_1

    .line 57
    const/4 v0, 0x7

    goto :goto_0

    .line 59
    :cond_1
    const/16 v0, 0xc

    goto :goto_0
.end method

.method public setBackgroundColor(I)V
    .locals 9
    .param p1, "color"    # I

    .prologue
    const/4 v8, 0x1

    .line 79
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 80
    .local v2, "time":J
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->setFrameVectorColor(I)V

    .line 83
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 86
    .local v1, "actualIdx":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)I

    move-result v4

    const v5, 0x3120006e

    if-lt v4, v5, :cond_2

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)I

    move-result v4

    const v5, 0x31200077

    if-gt v4, v5, :cond_2

    .line 88
    const/16 v4, 0xa

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->maxPage:I

    .line 99
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 100
    .local v0, "actualId":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBitmapListClass:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mFrameView:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;
    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameView;->getFrameVectorColor()I

    move-result v5

    invoke-virtual {v4, v0, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager$BitmapListClass;->validBitmap(II)Z

    move-result v4

    if-nez v4, :cond_1

    .line 103
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    iget-object v4, v4, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mViewPagerManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;

    invoke-virtual {v4, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewPagerManager;->setEffect(I)V

    .line 105
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBGColorTask:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 106
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBGColorTask:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->cancel(Z)Z

    .line 107
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;)V

    .line 109
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    invoke-direct {v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)V

    invoke-static {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;)V

    .line 110
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mBGColorTask:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;

    move-result-object v4

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Integer;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->maxPage:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$BGColorTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 113
    :cond_1
    return-void

    .line 90
    .end local v0    # "actualId":I
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)I

    move-result v4

    const v5, 0x31200078

    if-lt v4, v5, :cond_3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->mResId:I
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView;)I

    move-result v4

    const v5, 0x3120007e

    if-gt v4, v5, :cond_3

    .line 92
    const/4 v4, 0x7

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->maxPage:I

    goto/16 :goto_0

    .line 96
    :cond_3
    const/16 v4, 0xc

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/FrameListView$1;->maxPage:I

    goto/16 :goto_0
.end method
