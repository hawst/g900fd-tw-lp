.class public Lcom/sec/android/mimage/photoretouching/pen/PenActivity;
.super Landroid/app/Activity;
.source "PenActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/pen/PenActivity$DoDoneAsyncTask;,
        Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenImageData;,
        Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;,
        Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;,
        Lcom/sec/android/mimage/photoretouching/pen/PenActivity$WaitingThread;,
        Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;,
        Lcom/sec/android/mimage/photoretouching/pen/PenActivity$selectionChangeListener;
    }
.end annotation


# static fields
.field public static final BTN_STATE_BACK:I = 0x0

.field public static final BTN_STATE_ERASER:I = 0x2

.field public static final BTN_STATE_PEN:I = 0x1

.field public static final BTN_STATE_REDO:I = 0x4

.field public static final BTN_STATE_UNDO:I = 0x3

.field public static final REDOALL_DIALOG:I = 0x1

.field public static final UNDOALL_DIALOG:I = 0x2

.field private static mPrevSpenSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field private static mPrevSpenSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;


# instance fields
.field private final CURRENT_SHOWING_SETTING_VIEW:I

.field private final MULTI_WINDOW_WINDOW_BAR_HEIGHT:I

.field private final SPEN_MAX_ZOOM_RATIO:F

.field private configChange:Z

.field private isDrawing:Z

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mAngleTextBitmap:Landroid/graphics/Bitmap;

.field private mBitmap_bg:Landroid/graphics/Bitmap;

.field private mCanvasContainer:Landroid/widget/RelativeLayout;

.field private mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

.field private mContext:Landroid/content/Context;

.field private mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

.field private mCurMotionEvent:Landroid/view/MotionEvent;

.field private mCurrentMode:I

.field private mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field mEraserLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

.field private mEraserSettingViewHeight:I

.field private mEraserSettingViewWidth:I

.field private mEventListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

.field private mGaugeRatio:F

.field private mHistoryListener:Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;

.field mIsEntered:Z

.field private mIsEraserSettingViewInited:Z

.field private mIsPenSettingViewInited:Z

.field private mIsRestore:Z

.field private mMode:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

.field private mPenColorImage:Landroid/widget/FrameLayout;

.field private mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

.field mPenLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

.field private mPenMultitouch:Z

.field private mPenSettingViewHeight:I

.field private mPenSettingViewWidth:I

.field private mPenTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field mPopupHandler:Landroid/os/Handler;

.field private mPrevLayoutRect:Landroid/graphics/Rect;

.field private mPrevOrientation:I

.field private mPrevTime:J

.field private mPrevZoomRatio:F

.field private mPreviewBitmap:Landroid/graphics/Bitmap;

.field private mPreviewBuffer:[I

.field private mPreviewHeight:I

.field private mPreviewWidth:I

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field private mSPenCanvasHeight:I

.field private mSPenCanvasWidth:I

.field private mSPenInitialHeight:I

.field private mSPenInitialWidth:I

.field private mSettingContainer:Landroid/widget/RelativeLayout;

.field private mSettingView:Lcom/sec/android/pen/util/SettingView;

.field private mSpenNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

.field private mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

.field private mSpenPenInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

.field private mSpenZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

.field private mSrcImageRect:Landroid/graphics/Rect;

.field private mStoredSettingManager:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;

.field private mUndoStarckEmpty:Z

.field private mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mZoomMultitouch:Z

.field private mZoomRatio:F

.field private nfcAdapter:Landroid/nfc/NfcAdapter;

.field posX:I

.field posY:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 172
    sput-object v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 173
    sput-object v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 93
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 95
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 96
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 97
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 98
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mTrayManager:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;

    .line 100
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPaint:Landroid/graphics/Paint;

    .line 101
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->nfcAdapter:Landroid/nfc/NfcAdapter;

    .line 102
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 117
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->SPEN_MAX_ZOOM_RATIO:F

    .line 119
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSrcImageRect:Landroid/graphics/Rect;

    .line 120
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    .line 121
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    .line 126
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBuffer:[I

    .line 127
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mHistoryListener:Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;

    .line 128
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    .line 129
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 130
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 131
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    .line 132
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 133
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 134
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEventListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .line 136
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevLayoutRect:Landroid/graphics/Rect;

    .line 140
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    .line 141
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 142
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mCanvasContainer:Landroid/widget/RelativeLayout;

    .line 143
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingContainer:Landroid/widget/RelativeLayout;

    .line 147
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBitmap:Landroid/graphics/Bitmap;

    .line 149
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 151
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewHeight:I

    .line 152
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewWidth:I

    .line 153
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewHeight:I

    .line 154
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewWidth:I

    .line 155
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevZoomRatio:F

    .line 156
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F

    .line 157
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mGaugeRatio:F

    .line 158
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevTime:J

    .line 159
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 160
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    .line 161
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenMultitouch:Z

    .line 162
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomMultitouch:Z

    .line 163
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mContext:Landroid/content/Context;

    .line 164
    const v0, 0x1a001842

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mMode:I

    .line 167
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsPenSettingViewInited:Z

    .line 168
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsEraserSettingViewInited:Z

    .line 170
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mUndoStarckEmpty:Z

    .line 175
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mStoredSettingManager:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$StoredSettingManager;

    .line 177
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevOrientation:I

    .line 178
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mCurrentMode:I

    .line 180
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mCurMotionEvent:Landroid/view/MotionEvent;

    .line 181
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->isDrawing:Z

    .line 182
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsEntered:Z

    .line 184
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->configChange:Z

    .line 910
    new-instance v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$1;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPopupHandler:Landroid/os/Handler;

    .line 923
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    .line 924
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posY:I

    .line 2233
    const/16 v0, 0x19

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->MULTI_WINDOW_WINDOW_BAR_HEIGHT:I

    .line 2234
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->CURRENT_SHOWING_SETTING_VIEW:I

    .line 93
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;II)V
    .locals 0

    .prologue
    .line 1448
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->fitPopupIntoMotherLayout(II)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 2025
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;I)V
    .locals 0

    .prologue
    .line 123
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenInitialWidth:I

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;I)V
    .locals 0

    .prologue
    .line 123
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenInitialHeight:I

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 640
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->doDone()V

    return-void
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/pen/util/SettingView;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mBitmap_bg:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 338
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->fitSPenIntoMotherLayout()V

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    return-object v0
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 322
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->backPressed()V

    return-void
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 649
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->close()V

    return-void
.end method

.method static synthetic access$25(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    return-object v0
.end method

.method static synthetic access$26(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V
    .locals 0

    .prologue
    .line 170
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mUndoStarckEmpty:Z

    return-void
.end method

.method static synthetic access$27(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mUndoStarckEmpty:Z

    return v0
.end method

.method static synthetic access$28(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$29(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V
    .locals 0

    .prologue
    .line 161
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenMultitouch:Z

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 1292
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setBitmapBG(Landroid/graphics/Canvas;)V

    return-void
.end method

.method static synthetic access$30(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V
    .locals 0

    .prologue
    .line 162
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomMultitouch:Z

    return-void
.end method

.method static synthetic access$31(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V
    .locals 0

    .prologue
    .line 181
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->isDrawing:Z

    return-void
.end method

.method static synthetic access$32(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mCurMotionEvent:Landroid/view/MotionEvent;

    return-void
.end method

.method static synthetic access$33(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)J
    .locals 2

    .prologue
    .line 158
    iget-wide v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevTime:J

    return-wide v0
.end method

.method static synthetic access$34(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F

    return v0
.end method

.method static synthetic access$35(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;F)V
    .locals 0

    .prologue
    .line 155
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevZoomRatio:F

    return-void
.end method

.method static synthetic access$36(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevZoomRatio:F

    return v0
.end method

.method static synthetic access$37(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;F)V
    .locals 0

    .prologue
    .line 157
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mGaugeRatio:F

    return-void
.end method

.method static synthetic access$38(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;J)V
    .locals 1

    .prologue
    .line 158
    iput-wide p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevTime:J

    return-void
.end method

.method static synthetic access$39(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V
    .locals 0

    .prologue
    .line 2191
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->selectPenButton()V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenMultitouch:Z

    return v0
.end method

.method static synthetic access$40(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V
    .locals 0

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsPenSettingViewInited:Z

    return-void
.end method

.method static synthetic access$41(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Z)V
    .locals 0

    .prologue
    .line 168
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsEraserSettingViewInited:Z

    return-void
.end method

.method static synthetic access$42(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;IZ)V
    .locals 0

    .prologue
    .line 927
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->refreshPopupState(IZ)V

    return-void
.end method

.method static synthetic access$43(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mMode:I

    return v0
.end method

.method static synthetic access$44(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;I)V
    .locals 0

    .prologue
    .line 164
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mMode:I

    return-void
.end method

.method static synthetic access$45(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;I)Z
    .locals 1

    .prologue
    .line 1920
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->checkSettingViewInited(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomMultitouch:Z

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)F
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mGaugeRatio:F

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mBitmap_bg:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private applyToOriginal()V
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 673
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->capturePage(F)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 676
    .local v9, "temp":Landroid/graphics/Bitmap;
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 677
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getCanvasWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getCanvasHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 678
    .local v10, "temp2":Landroid/graphics/Bitmap;
    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 682
    .local v8, "mCanvas":Landroid/graphics/Canvas;
    const/4 v2, 0x0

    invoke-virtual {v8, v9, v5, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 686
    :try_start_0
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    .line 687
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewHeight:I

    .line 688
    const/4 v4, 0x1

    .line 685
    invoke-static {v10, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 690
    .local v0, "canvasBitmap":Landroid/graphics/Bitmap;
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewHeight:I

    mul-int/2addr v2, v3

    new-array v1, v2, [I

    .line 693
    .local v1, "penData":[I
    const/4 v2, 0x0

    .line 694
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    .line 695
    const/4 v4, 0x0

    .line 696
    const/4 v5, 0x0

    .line 697
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    .line 698
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewHeight:I

    .line 692
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 699
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->runBlending([I)V

    .line 700
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 703
    .end local v0    # "canvasBitmap":Landroid/graphics/Bitmap;
    .end local v1    # "penData":[I
    :goto_0
    invoke-static {v10}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 705
    .end local v8    # "mCanvas":Landroid/graphics/Canvas;
    .end local v10    # "temp2":Landroid/graphics/Bitmap;
    :cond_0
    return-void

    .line 702
    .restart local v8    # "mCanvas":Landroid/graphics/Canvas;
    .restart local v10    # "temp2":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private backPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 330
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 331
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "button_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 333
    invoke-virtual {p0, v2, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setResult(ILandroid/content/Intent;)V

    .line 334
    const/high16 v1, 0x31000000

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentMode(I)V

    .line 335
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->close()V

    .line 337
    return-void
.end method

.method private checkSettingViewInited(I)Z
    .locals 4
    .param p1, "mode"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1922
    const/4 v0, 0x1

    .line 1924
    .local v0, "ret":Z
    if-ne p1, v2, :cond_1

    .line 1926
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsPenSettingViewInited:Z

    if-nez v1, :cond_0

    .line 1928
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v1, v2}, Lcom/sec/android/pen/util/SettingView;->isSettingViewVisible(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1930
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsPenSettingViewInited:Z

    .line 1931
    const/4 v0, 0x0

    .line 1946
    :cond_0
    :goto_0
    return v0

    .line 1935
    :cond_1
    if-ne p1, v3, :cond_0

    .line 1937
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsEraserSettingViewInited:Z

    if-nez v1, :cond_0

    .line 1939
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v1, v3}, Lcom/sec/android/pen/util/SettingView;->isSettingViewVisible(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1941
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsEraserSettingViewInited:Z

    .line 1942
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private close()V
    .locals 6

    .prologue
    .line 650
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 651
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 652
    .local v0, "b":Landroid/os/Bundle;
    sget-object v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->CONFIG_CHANGE:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->configChange:Z

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 653
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 654
    sget-object v2, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 655
    iget-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->configChange:Z

    if-eqz v2, :cond_0

    .line 656
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$6;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$6;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 663
    const-wide/16 v4, 0x190

    .line 656
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 665
    :goto_0
    return-void

    .line 664
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->finish()V

    goto :goto_0
.end method

.method private createManager()V
    .locals 2

    .prologue
    .line 362
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 363
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 364
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 365
    new-instance v1, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;

    invoke-direct {v1, p0, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mTrayManager:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;

    .line 366
    new-instance v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 367
    new-instance v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$2;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 442
    .local v0, "stateChangedListener":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 443
    return-void
.end method

.method private doDone()V
    .locals 1

    .prologue
    .line 642
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsEntered:Z

    .line 643
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->applyToOriginal()V

    .line 644
    const v0, 0x50002

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setResult(I)V

    .line 645
    const/high16 v0, 0x31000000

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentMode(I)V

    .line 646
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->close()V

    .line 647
    return-void
.end method

.method private fitPopupIntoMotherLayout(II)V
    .locals 7
    .param p1, "layoutW"    # I
    .param p2, "layoutH"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1449
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevLayoutRect:Landroid/graphics/Rect;

    if-nez v3, :cond_1

    .line 1491
    :cond_0
    :goto_0
    return-void

    .line 1452
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevLayoutRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevLayoutRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-lez v3, :cond_0

    .line 1457
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v3}, Lcom/sec/android/pen/util/SettingView;->getSpenSettingPenLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v2

    .line 1458
    .local v2, "pen":Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v3}, Lcom/sec/android/pen/util/SettingView;->getSpenSettingEraserLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    move-result-object v0

    .line 1462
    .local v0, "eraser":Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v4, v4, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1483
    .local v1, "layoutRect":Landroid/graphics/Rect;
    invoke-virtual {v2, v4, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setPosition(II)V

    .line 1484
    invoke-virtual {v0, v4, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setPosition(II)V

    .line 1485
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v3, v5}, Lcom/sec/android/pen/util/SettingView;->isSettingViewVisible(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1486
    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setViewMode(I)V

    .line 1490
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevLayoutRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 1487
    :cond_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v3, v6}, Lcom/sec/android/pen/util/SettingView;->isSettingViewVisible(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1488
    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setViewMode(I)V

    goto :goto_1
.end method

.method private fitSPenIntoMotherLayout()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 339
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setZoomable(Z)V

    .line 340
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSrcImageRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 341
    .local v0, "height":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSrcImageRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v4

    .line 343
    .local v4, "witdh":I
    const/4 v1, 0x0

    .line 344
    .local v1, "scale":F
    int-to-float v5, v0

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float v2, v5, v6

    .line 345
    .local v2, "scaleH":F
    int-to-float v5, v4

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 348
    .local v3, "scaleW":F
    cmpl-float v5, v2, v3

    if-ltz v5, :cond_0

    .line 349
    move v1, v3

    .line 354
    :goto_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/high16 v6, 0x3f400000    # 0.75f

    mul-float/2addr v6, v1

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setMinZoomRatio(F)Z

    .line 355
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/high16 v6, 0x40400000    # 3.0f

    mul-float/2addr v6, v1

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setMaxZoomRatio(F)Z

    .line 356
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v5, v7, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setZoom(FFF)V

    .line 357
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomRatio:F

    .line 359
    return-void

    .line 351
    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private getContext()Landroid/content/Context;
    .locals 0

    .prologue
    .line 2026
    iput-object p0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method private getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 1805
    new-instance v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$22;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    return-object v0
.end method

.method private getSettingViewLeft(I)I
    .locals 7
    .param p1, "mode"    # I

    .prologue
    const/4 v6, 0x0

    const/high16 v5, -0x80000000

    .line 886
    const/4 v0, 0x0

    .line 887
    .local v0, "bottomLeft":I
    const/4 v3, 0x0

    .line 889
    .local v3, "size":I
    const/4 v4, 0x1

    if-ne p1, v4, :cond_1

    .line 891
    const/4 v3, 0x2

    .line 898
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v3, :cond_2

    .line 904
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 905
    .local v2, "l":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v5, v5}, Landroid/widget/LinearLayout;->measure(II)V

    .line 906
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v0, v4

    .line 908
    return v0

    .line 893
    .end local v1    # "i":I
    .end local v2    # "l":Landroid/widget/LinearLayout;
    :cond_1
    const/4 v4, 0x2

    if-ne p1, v4, :cond_0

    .line 895
    const/4 v3, 0x3

    goto :goto_0

    .line 900
    .restart local v1    # "i":I
    :cond_2
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 901
    .restart local v2    # "l":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v6, v6}, Landroid/widget/LinearLayout;->measure(II)V

    .line 902
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v0, v4

    .line 898
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private init(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    .line 446
    new-instance v6, Landroid/graphics/Paint;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPaint:Landroid/graphics/Paint;

    .line 447
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 448
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->initActionbar()V

    .line 449
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->initButtons()V

    .line 450
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 451
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->initDialogs()V

    .line 452
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mTrayManager:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;

    # invokes: Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;->initTrayLayout()V
    invoke-static {v6}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;->access$0(Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;)V

    .line 454
    new-instance v4, Lcom/samsung/android/sdk/pen/Spen;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/Spen;-><init>()V

    .line 456
    .local v4, "spenSdk":Lcom/samsung/android/sdk/pen/Spen;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/pen/Spen;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 460
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setData(Landroid/content/Intent;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 462
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setSettingView()V

    .line 463
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setSPenView()V

    .line 464
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v6}, Lcom/sec/android/pen/util/SettingView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 465
    .local v2, "mParams":Landroid/view/ViewGroup$MarginLayoutParams;
    const/16 v6, 0x22

    iput v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 466
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v6, v2}, Lcom/sec/android/pen/util/SettingView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 468
    iget-boolean v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mUndoStarckEmpty:Z

    invoke-virtual {p0, v6}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->refreshUndoRedoBtnState(Z)V

    .line 470
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 472
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v3

    .line 473
    .local v3, "rect":Landroid/graphics/Rect;
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 474
    .local v5, "width":I
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 475
    .local v1, "height":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 477
    add-int/lit8 v1, v1, -0x19

    .line 484
    :goto_1
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonHeight()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getActionbarHeight()I

    move-result v7

    add-int/2addr v6, v7

    sub-int/2addr v1, v6

    .line 486
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevLayoutRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v8, v8, v5, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 499
    .end local v1    # "height":I
    .end local v2    # "mParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v3    # "rect":Landroid/graphics/Rect;
    .end local v5    # "width":I
    :cond_0
    :goto_2
    return-void

    .line 457
    :catch_0
    move-exception v0

    .line 458
    .local v0, "e1":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 481
    .end local v0    # "e1":Ljava/lang/Exception;
    .restart local v1    # "height":I
    .restart local v2    # "mParams":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v3    # "rect":Landroid/graphics/Rect;
    .restart local v5    # "width":I
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v5, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 482
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v1, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_1

    .line 489
    .end local v1    # "height":I
    .end local v3    # "rect":Landroid/graphics/Rect;
    .end local v5    # "width":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v5, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 490
    .restart local v5    # "width":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v1, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 492
    .restart local v1    # "height":I
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonHeight()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getActionbarHeight()I

    move-result v7

    add-int/2addr v6, v7

    sub-int/2addr v1, v6

    .line 494
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevLayoutRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v8, v8, v5, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_2
.end method

.method private init3DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 553
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 556
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 559
    new-instance v1, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$3;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 556
    invoke-virtual {v0, v4, v2, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 579
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 582
    new-instance v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$4;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 579
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 607
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 610
    new-instance v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$5;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 607
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 632
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 634
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 638
    :cond_0
    return-void
.end method

.method private initActionbar()V
    .locals 2

    .prologue
    .line 540
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 548
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->init3DepthActionBar()V

    .line 549
    return-void

    .line 546
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    goto :goto_0
.end method

.method private initButtons()V
    .locals 7

    .prologue
    .line 834
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setClearSelectedButton()V

    .line 835
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v4, :cond_0

    .line 837
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v1

    .line 838
    .local v1, "roi":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 839
    .local v3, "width":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v4, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonForPen(I)V

    .line 841
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v0, v4, :cond_1

    .line 848
    .end local v0    # "i":I
    .end local v1    # "roi":Landroid/graphics/Rect;
    .end local v3    # "width":I
    :cond_0
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getPenColorLine()Landroid/widget/FrameLayout;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenColorImage:Landroid/widget/FrameLayout;

    .line 850
    return-void

    .line 843
    .restart local v0    # "i":I
    .restart local v1    # "roi":Landroid/graphics/Rect;
    .restart local v3    # "width":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 844
    .local v2, "v":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v5

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 841
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private initDialogs()V
    .locals 8

    .prologue
    const v6, 0x1030132

    const/16 v1, 0x1000

    const/16 v7, 0x500

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 746
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 747
    const/4 v2, 0x2

    .line 748
    const v3, 0x7f0600a6

    .line 746
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 753
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 754
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 755
    const v2, 0x7f06009c

    .line 757
    new-instance v3, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$7;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$7;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 754
    invoke-virtual {v0, v7, v2, v5, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 764
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060007

    .line 765
    new-instance v3, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$8;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$8;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 764
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 778
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060009

    .line 779
    new-instance v3, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$9;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$9;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 778
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 787
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 789
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 791
    const v3, 0x7f0600a1

    move v2, v4

    .line 789
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 796
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 797
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 798
    const v1, 0x7f0601ce

    .line 800
    new-instance v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$10;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$10;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 797
    invoke-virtual {v0, v7, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 807
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 808
    new-instance v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$11;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$11;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 807
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 820
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 821
    new-instance v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$12;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$12;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    .line 820
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 829
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 830
    return-void
.end method

.method private newSpenListener()V
    .locals 1

    .prologue
    .line 1114
    new-instance v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$13;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$13;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mHistoryListener:Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;

    .line 1131
    new-instance v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$14;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$14;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    .line 1175
    new-instance v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$15;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 1221
    new-instance v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$16;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    .line 1252
    new-instance v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$17;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$17;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    .line 1256
    new-instance v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$18;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$18;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 1265
    new-instance v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$19;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$19;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 1279
    new-instance v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$20;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$20;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEventListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .line 1289
    return-void
.end method

.method private refreshPenEraserBtnState(I)V
    .locals 3
    .param p1, "btn"    # I

    .prologue
    const/4 v2, 0x1

    .line 1301
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setClearSelectedButton()V

    .line 1302
    if-ne p1, v2, :cond_1

    .line 1303
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1304
    const v1, 0x1a001842

    .line 1303
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 1320
    :cond_0
    :goto_0
    return-void

    .line 1307
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1308
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1309
    const v1, 0x1a001843

    .line 1308
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    goto :goto_0
.end method

.method private refreshPopupState(IZ)V
    .locals 20
    .param p1, "mode"    # I
    .param p2, "notInHandler"    # Z

    .prologue
    .line 929
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v16

    move-object/from16 v0, v16

    iget v12, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 930
    .local v12, "viewHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v16

    move-object/from16 v0, v16

    iget v13, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 931
    .local v13, "viewWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v16

    move-object/from16 v0, v16

    iget v9, v0, Landroid/content/res/Configuration;->orientation:I

    .line 932
    .local v9, "orientation":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonHeight()I

    move-result v4

    .line 933
    .local v4, "bottomHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f050258

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 934
    .local v2, "actionbarheight":I
    const/4 v5, 0x0

    .line 935
    .local v5, "bottomLeft":I
    const/4 v6, 0x0

    .line 936
    .local v6, "bottomPenMargin":I
    const/4 v3, 0x0

    .line 937
    .local v3, "bottomEraserMargin":I
    const/4 v14, 0x0

    .local v14, "x":I
    const/4 v15, 0x0

    .line 939
    .local v15, "y":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    move-object/from16 v16, v0

    if-nez v16, :cond_1

    .line 1111
    :cond_0
    :goto_0
    return-void

    .line 942
    :cond_1
    const/16 v16, 0x1

    move/from16 v0, p1

    move/from16 v1, v16

    if-eq v0, v1, :cond_3

    .line 943
    const/16 v16, 0x10

    move/from16 v0, p1

    move/from16 v1, v16

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/pen/util/SettingView;->isSettingViewVisible(I)Z

    move-result v16

    if-nez v16, :cond_3

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-object/from16 v16, v0

    const/16 v17, 0x2

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getToolTypeAction(I)I

    move-result v16

    const/16 v17, 0x5

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_5

    .line 945
    :cond_3
    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getSettingViewLeft(I)I

    move-result v5

    .line 946
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/pen/util/SettingView;->getSpenSettingPenLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v10

    .line 947
    .local v10, "penLayout":Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    .line 949
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewWidth:I

    move/from16 v16, v0

    if-nez v16, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewHeight:I

    move/from16 v16, v0

    if-nez v16, :cond_4

    .line 951
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/pen/util/SettingView;->getSpenSettingPenLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->measure(II)V

    .line 952
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/pen/util/SettingView;->getSpenSettingPenLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getMeasuredWidth()I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewWidth:I

    .line 953
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/pen/util/SettingView;->getSpenSettingPenLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getMeasuredHeight()I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewHeight:I

    .line 955
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v16

    if-eqz v16, :cond_e

    .line 957
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v11

    .line 958
    .local v11, "roi":Landroid/graphics/Rect;
    invoke-virtual {v11}, Landroid/graphics/Rect;->centerX()I

    move-result v7

    .line 959
    .local v7, "centerX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewWidth:I

    move/from16 v16, v0

    div-int/lit8 v16, v16, 0x2

    sub-int v14, v7, v16

    .line 960
    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewHeight:I

    move/from16 v17, v0

    add-int v17, v17, v4

    add-int v17, v17, v6

    sub-int v15, v16, v17

    .line 961
    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    .line 962
    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posY:I

    .line 963
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isNewSpenJarSupported()Z

    move-result v16

    if-eqz v16, :cond_d

    .line 964
    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setViewMode(I)V

    .line 965
    if-eqz p2, :cond_c

    .line 966
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posY:I

    move/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setPosition(II)V

    .line 1034
    .end local v7    # "centerX":I
    .end local v10    # "penLayout":Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    .end local v11    # "roi":Landroid/graphics/Rect;
    :cond_5
    :goto_1
    const/16 v16, 0x2

    move/from16 v0, p1

    move/from16 v1, v16

    if-eq v0, v1, :cond_6

    .line 1035
    const/16 v16, 0x10

    move/from16 v0, p1

    move/from16 v1, v16

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    move-object/from16 v16, v0

    const/16 v17, 0x2

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/pen/util/SettingView;->isSettingViewVisible(I)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 1037
    :cond_6
    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getSettingViewLeft(I)I

    move-result v5

    .line 1038
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/pen/util/SettingView;->getSpenSettingEraserLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    move-result-object v8

    .line 1039
    .local v8, "eraserLayout":Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewWidth:I

    move/from16 v16, v0

    if-nez v16, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewHeight:I

    move/from16 v16, v0

    if-nez v16, :cond_7

    .line 1041
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/pen/util/SettingView;->getSpenSettingEraserLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    move-result-object v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->measure(II)V

    .line 1042
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/pen/util/SettingView;->getSpenSettingEraserLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMeasuredWidth()I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewWidth:I

    .line 1043
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/pen/util/SettingView;->getSpenSettingEraserLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getMeasuredHeight()I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewHeight:I

    .line 1045
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v16

    if-eqz v16, :cond_16

    .line 1047
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v11

    .line 1048
    .restart local v11    # "roi":Landroid/graphics/Rect;
    invoke-virtual {v11}, Landroid/graphics/Rect;->centerX()I

    move-result v7

    .line 1049
    .restart local v7    # "centerX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewWidth:I

    move/from16 v16, v0

    div-int/lit8 v16, v16, 0x2

    sub-int v14, v7, v16

    .line 1050
    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewHeight:I

    move/from16 v17, v0

    add-int v17, v17, v4

    add-int v17, v17, v6

    sub-int v15, v16, v17

    .line 1085
    :cond_8
    :goto_2
    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    .line 1086
    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posY:I

    .line 1087
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    .line 1088
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isNewSpenJarSupported()Z

    move-result v16

    if-eqz v16, :cond_22

    .line 1089
    const-string v16, "tr"

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_9

    const-string v16, "tb"

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_1e

    .line 1090
    :cond_9
    add-int/lit16 v0, v14, 0x82

    move/from16 v16, v0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    .line 1093
    :cond_a
    :goto_3
    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v9, v0, :cond_20

    .line 1094
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewHeight:I

    move/from16 v16, v0

    add-int v16, v16, v4

    sub-int v16, v12, v16

    add-int/lit8 v15, v16, -0xa

    .line 1098
    :cond_b
    :goto_4
    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setViewMode(I)V

    .line 1099
    if-eqz p2, :cond_21

    .line 1100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posY:I

    move/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setPosition(II)V

    goto/16 :goto_0

    .line 968
    .end local v8    # "eraserLayout":Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;
    .restart local v10    # "penLayout":Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPopupHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Landroid/os/Handler;->removeMessages(I)V

    .line 969
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPopupHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-wide/16 v18, 0x32

    invoke-virtual/range {v16 .. v19}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 973
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posY:I

    move/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setPosition(II)V

    .line 974
    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setViewMode(I)V

    goto/16 :goto_1

    .line 979
    .end local v7    # "centerX":I
    .end local v11    # "roi":Landroid/graphics/Rect;
    :cond_e
    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v9, v0, :cond_13

    .line 980
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v11

    .line 981
    .restart local v11    # "roi":Landroid/graphics/Rect;
    invoke-virtual {v11}, Landroid/graphics/Rect;->centerX()I

    move-result v7

    .line 983
    .restart local v7    # "centerX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewWidth:I

    move/from16 v16, v0

    div-int/lit8 v16, v16, 0x2

    sub-int v14, v7, v16

    .line 984
    const-string v16, "tr"

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_f

    .line 985
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewHeight:I

    move/from16 v16, v0

    add-int v16, v16, v4

    add-int v16, v16, v6

    sub-int v16, v12, v16

    add-int/lit8 v15, v16, -0x1e

    .line 991
    :goto_5
    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    .line 992
    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posY:I

    .line 993
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isNewSpenJarSupported()Z

    move-result v16

    if-eqz v16, :cond_12

    .line 994
    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setViewMode(I)V

    .line 995
    if-eqz p2, :cond_11

    .line 996
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posY:I

    move/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setPosition(II)V

    goto/16 :goto_1

    .line 986
    :cond_f
    const-string v16, "patek"

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_10

    .line 987
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewHeight:I

    move/from16 v16, v0

    add-int v16, v16, v4

    add-int v16, v16, v6

    sub-int v16, v12, v16

    add-int/lit8 v15, v16, -0x23

    goto :goto_5

    .line 989
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewHeight:I

    move/from16 v16, v0

    add-int v16, v16, v4

    add-int v16, v16, v6

    sub-int v16, v12, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0502bf

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    sub-int v15, v16, v17

    goto :goto_5

    .line 998
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPopupHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Landroid/os/Handler;->removeMessages(I)V

    .line 999
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPopupHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-wide/16 v18, 0x32

    invoke-virtual/range {v16 .. v19}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 1003
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posY:I

    move/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setPosition(II)V

    .line 1004
    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setViewMode(I)V

    goto/16 :goto_1

    .line 1007
    .end local v7    # "centerX":I
    .end local v11    # "roi":Landroid/graphics/Rect;
    :cond_13
    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v9, v0, :cond_5

    .line 1009
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v11

    .line 1010
    .restart local v11    # "roi":Landroid/graphics/Rect;
    invoke-virtual {v11}, Landroid/graphics/Rect;->centerX()I

    move-result v7

    .line 1011
    .restart local v7    # "centerX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewWidth:I

    move/from16 v16, v0

    div-int/lit8 v16, v16, 0x2

    sub-int v14, v7, v16

    .line 1013
    add-int v16, v12, v2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenSettingViewHeight:I

    move/from16 v17, v0

    add-int v17, v17, v4

    add-int v17, v17, v6

    sub-int v15, v16, v17

    .line 1014
    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    .line 1015
    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posY:I

    .line 1016
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isNewSpenJarSupported()Z

    move-result v16

    if-eqz v16, :cond_15

    .line 1017
    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setViewMode(I)V

    .line 1018
    if-eqz p2, :cond_14

    .line 1019
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posY:I

    move/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setPosition(II)V

    goto/16 :goto_1

    .line 1021
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPopupHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Landroid/os/Handler;->removeMessages(I)V

    .line 1022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPopupHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-wide/16 v18, 0x32

    invoke-virtual/range {v16 .. v19}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 1026
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posY:I

    move/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setPosition(II)V

    .line 1027
    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setViewMode(I)V

    goto/16 :goto_1

    .line 1054
    .end local v7    # "centerX":I
    .end local v10    # "penLayout":Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    .end local v11    # "roi":Landroid/graphics/Rect;
    .restart local v8    # "eraserLayout":Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v11

    .line 1055
    .restart local v11    # "roi":Landroid/graphics/Rect;
    invoke-virtual {v11}, Landroid/graphics/Rect;->centerX()I

    move-result v7

    .line 1056
    .restart local v7    # "centerX":I
    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v9, v0, :cond_19

    .line 1058
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewWidth:I

    move/from16 v16, v0

    div-int/lit8 v16, v16, 0x2

    sub-int v16, v7, v16

    div-int/lit8 v17, v5, 0x5

    sub-int v14, v16, v17

    .line 1059
    const-string v16, "tr"

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_17

    .line 1060
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewHeight:I

    move/from16 v16, v0

    add-int v16, v16, v4

    sub-int v16, v12, v16

    add-int/lit8 v15, v16, -0x14

    .line 1066
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewWidth:I

    move/from16 v16, v0

    add-int v16, v16, v14

    move/from16 v0, v16

    if-le v0, v13, :cond_8

    .line 1067
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewWidth:I

    move/from16 v16, v0

    add-int v16, v16, v14

    sub-int v16, v16, v13

    sub-int v14, v14, v16

    .line 1069
    goto/16 :goto_2

    .line 1061
    :cond_17
    const-string v16, "patek"

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_18

    .line 1062
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewHeight:I

    move/from16 v16, v0

    add-int v16, v16, v4

    sub-int v16, v12, v16

    add-int/lit8 v15, v16, -0x1e

    goto :goto_6

    .line 1064
    :cond_18
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewHeight:I

    move/from16 v16, v0

    add-int v16, v16, v4

    sub-int v16, v12, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0502c0

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    sub-int v15, v16, v17

    goto :goto_6

    .line 1070
    :cond_19
    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v9, v0, :cond_8

    .line 1071
    const-string v16, "tr"

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_1a

    .line 1072
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewHeight:I

    move/from16 v16, v0

    add-int v16, v16, v4

    add-int v16, v16, v3

    sub-int v16, v12, v16

    add-int/lit8 v15, v16, -0x14

    .line 1079
    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewWidth:I

    move/from16 v16, v0

    div-int/lit8 v16, v16, 0x2

    sub-int v16, v7, v16

    div-int/lit8 v17, v5, 0x5

    sub-int v14, v16, v17

    .line 1080
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewWidth:I

    move/from16 v16, v0

    add-int v16, v16, v14

    move/from16 v0, v16

    if-le v0, v13, :cond_8

    .line 1081
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewWidth:I

    move/from16 v16, v0

    add-int v16, v16, v14

    sub-int v16, v16, v13

    sub-int v14, v14, v16

    goto/16 :goto_2

    .line 1073
    :cond_1a
    const-string v16, "patek"

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_1b

    .line 1074
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewHeight:I

    move/from16 v16, v0

    add-int v16, v16, v4

    add-int v16, v16, v3

    sub-int v16, v12, v16

    add-int/lit8 v15, v16, -0x1e

    goto :goto_7

    .line 1075
    :cond_1b
    const-string v16, "a5"

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_1c

    const-string v16, "a7"

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_1d

    .line 1076
    :cond_1c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewHeight:I

    move/from16 v16, v0

    add-int v16, v16, v4

    add-int v16, v16, v3

    sub-int v16, v12, v16

    add-int/lit8 v15, v16, -0xa

    goto :goto_7

    .line 1078
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewHeight:I

    move/from16 v16, v0

    add-int v16, v16, v4

    add-int v16, v16, v3

    sub-int v16, v12, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0502c0

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    sub-int v15, v16, v17

    goto :goto_7

    .line 1091
    :cond_1e
    const-string v16, "a5"

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_1f

    const-string v16, "a7"

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_1f

    const-string v16, "patek"

    invoke-static/range {v16 .. v16}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isDevice(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_a

    .line 1092
    :cond_1f
    add-int/lit8 v16, v14, 0x37

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    goto/16 :goto_3

    .line 1095
    :cond_20
    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v9, v0, :cond_b

    .line 1096
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserSettingViewHeight:I

    move/from16 v16, v0

    add-int v16, v16, v4

    add-int v16, v16, v3

    sub-int v16, v12, v16

    add-int/lit8 v15, v16, -0xa

    goto/16 :goto_4

    .line 1102
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPopupHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Landroid/os/Handler;->removeMessages(I)V

    .line 1103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPopupHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    const-wide/16 v18, 0x32

    invoke-virtual/range {v16 .. v19}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1107
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEraserLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posX:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->posY:I

    move/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setPosition(II)V

    .line 1108
    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setViewMode(I)V

    goto/16 :goto_0
.end method

.method private resetFlagSettingViewInit()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1950
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsPenSettingViewInited:Z

    .line 1951
    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsEraserSettingViewInited:Z

    .line 1952
    return-void
.end method

.method private runBlending([I)V
    .locals 19
    .param p1, "TmpObjectData"    # [I

    .prologue
    .line 709
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBuffer:[I

    if-nez v15, :cond_1

    .line 743
    :cond_0
    return-void

    .line 712
    :cond_1
    const/4 v5, 0x0

    .local v5, "indexX":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    if-ge v5, v15, :cond_0

    .line 714
    const/4 v6, 0x0

    .local v6, "indexY":I
    :goto_1
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewHeight:I

    if-lt v6, v15, :cond_2

    .line 712
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 716
    :cond_2
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    mul-int/2addr v15, v6

    add-int/2addr v15, v5

    aget v1, p1, v15

    .line 717
    .local v1, "colorData":I
    shr-int/lit8 v15, v1, 0x18

    and-int/lit16 v10, v15, 0xff

    .line 718
    .local v10, "opaque":I
    const/16 v15, 0xff

    if-ne v10, v15, :cond_4

    .line 720
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBuffer:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    move/from16 v16, v0

    mul-int v16, v16, v6

    add-int v16, v16, v5

    aput v1, v15, v16

    .line 714
    :cond_3
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 722
    :cond_4
    if-eqz v10, :cond_3

    .line 724
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBuffer:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    move/from16 v16, v0

    mul-int v16, v16, v6

    add-int v16, v16, v5

    aget v12, v15, v16

    .line 726
    .local v12, "orgColor":I
    shr-int/lit8 v15, v12, 0x10

    and-int/lit16 v14, v15, 0xff

    .line 727
    .local v14, "orgR":I
    shr-int/lit8 v15, v12, 0x8

    and-int/lit16 v13, v15, 0xff

    .line 728
    .local v13, "orgG":I
    and-int/lit16 v11, v12, 0xff

    .line 730
    .local v11, "orgB":I
    shr-int/lit8 v15, v1, 0x10

    and-int/lit16 v9, v15, 0xff

    .line 731
    .local v9, "objR":I
    shr-int/lit8 v15, v1, 0x8

    and-int/lit16 v8, v15, 0xff

    .line 732
    .local v8, "objG":I
    and-int/lit16 v7, v1, 0xff

    .line 734
    .local v7, "objB":I
    rsub-int v15, v10, 0xff

    mul-int/2addr v15, v14

    mul-int v16, v9, v10

    add-int v15, v15, v16

    div-int/lit16 v4, v15, 0xff

    .line 735
    .local v4, "finalR":I
    rsub-int v15, v10, 0xff

    mul-int/2addr v15, v13

    mul-int v16, v8, v10

    add-int v15, v15, v16

    div-int/lit16 v3, v15, 0xff

    .line 736
    .local v3, "finalG":I
    rsub-int v15, v10, 0xff

    mul-int/2addr v15, v11

    mul-int v16, v7, v10

    add-int v15, v15, v16

    div-int/lit16 v2, v15, 0xff

    .line 738
    .local v2, "finalB":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBuffer:[I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    move/from16 v16, v0

    mul-int v16, v16, v6

    add-int v16, v16, v5

    const/high16 v17, -0x1000000

    shl-int/lit8 v18, v4, 0x10

    or-int v17, v17, v18

    shl-int/lit8 v18, v3, 0x8

    or-int v17, v17, v18

    or-int v17, v17, v2

    aput v17, v15, v16

    goto :goto_2
.end method

.method private saveCurrentPenSetting()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 301
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    if-eqz v2, :cond_2

    .line 303
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v1

    .line 304
    .local v1, "penInfo":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-result-object v0

    .line 306
    .local v0, "eraserInfo":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    sget-object v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v2, :cond_0

    .line 307
    sput-object v4, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 308
    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    sput-object v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .line 309
    sget-object v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v3, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    .line 310
    sget-object v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v3, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 311
    sget-object v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iput-boolean v3, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    .line 312
    sget-object v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v3, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 313
    sget-object v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v3, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 315
    sget-object v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-eqz v2, :cond_1

    .line 316
    sput-object v4, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 317
    :cond_1
    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    sput-object v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    .line 318
    sget-object v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v3, v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 319
    sget-object v2, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    iput v3, v2, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 321
    .end local v0    # "eraserInfo":Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .end local v1    # "penInfo":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    :cond_2
    return-void
.end method

.method private selectPenButton()V
    .locals 11

    .prologue
    const v10, 0x1a001842

    const/4 v9, 0x1

    const/4 v8, 0x2

    .line 2193
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v6, v10, v9}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 2195
    const/4 v3, -0x1

    .line 2196
    .local v3, "settingMode":I
    const/4 v0, -0x1

    .line 2197
    .local v0, "fingerAction":I
    const/4 v5, -0x1

    .line 2198
    .local v5, "spenAction":I
    const/4 v4, -0x1

    .line 2199
    .local v4, "settingViewSize":I
    const/4 v1, -0x1

    .line 2200
    .local v1, "mouseAction":I
    const/4 v2, -0x1

    .line 2202
    .local v2, "penhaveremove":I
    const/4 v3, 0x1

    .line 2203
    const/4 v0, 0x2

    .line 2204
    const/4 v5, 0x2

    .line 2205
    const/4 v1, 0x2

    .line 2206
    const/4 v2, 0x4

    .line 2207
    const/4 v4, 0x2

    .line 2209
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->closeControl()V

    .line 2210
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getToolTypeAction(I)I

    move-result v6

    const/4 v7, 0x5

    if-ne v6, v7, :cond_0

    .line 2211
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v6}, Lcom/sec/android/pen/util/SettingView;->closeSettingView()V

    .line 2214
    :cond_0
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getToolTypeAction(I)I

    move-result v6

    if-eq v6, v5, :cond_1

    .line 2215
    invoke-direct {p0, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setViewMode(I)V

    .line 2217
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v6, v9, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    .line 2218
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v6, v8, v5}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    .line 2219
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v7, 0x4

    invoke-virtual {v6, v7, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    .line 2220
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v7, 0x3

    invoke-virtual {v6, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    .line 2225
    :cond_1
    iput v10, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mMode:I

    .line 2226
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v6, v3, v4}, Lcom/sec/android/pen/util/SettingView;->toggleShowSettingView(II)Z

    .line 2227
    return-void
.end method

.method private setBitmapBG(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1295
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1296
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f0203c8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 1297
    .local v0, "gauge_bg":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 1298
    return-void
.end method

.method private setData(Landroid/content/Intent;)Z
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 503
    const-string v0, "imageIndex"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 504
    .local v9, "index":I
    const-string v0, "width"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 505
    .local v11, "width":I
    const-string v0, "height"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 506
    .local v8, "height":I
    const-string v0, "viewWidth"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 507
    .local v3, "viewWidth":I
    const-string v0, "viewHeight"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 508
    .local v7, "viewHeight":I
    const-string v0, "draw_canvas_roi"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Landroid/graphics/Rect;

    .line 510
    .local v10, "r":Landroid/graphics/Rect;
    if-eq v9, v1, :cond_0

    if-eqz v11, :cond_0

    if-eqz v8, :cond_0

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/CommonInputData;->getData(I)[I

    move-result-object v0

    if-nez v0, :cond_1

    .line 512
    :cond_0
    const/high16 v0, 0x31000000

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentMode(I)V

    .line 513
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->finish()V

    .line 535
    :goto_0
    return v2

    .line 523
    :cond_1
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v7, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBitmap:Landroid/graphics/Bitmap;

    .line 524
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/CommonInputData;->getData(I)[I

    move-result-object v1

    move v4, v2

    move v5, v2

    move v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 527
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    .line 528
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewHeight:I

    .line 529
    invoke-static {v9}, Lcom/sec/android/mimage/photoretouching/CommonInputData;->getData(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBuffer:[I

    .line 531
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenInitialWidth:I

    .line 532
    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenInitialHeight:I

    .line 534
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v10}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSrcImageRect:Landroid/graphics/Rect;

    .line 535
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private setSPenView()V
    .locals 13

    .prologue
    .line 1324
    :try_start_0
    new-instance v9, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    iget v11, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenCanvasWidth:I

    iget v12, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenCanvasHeight:I

    invoke-direct {v9, v10, v11, v12}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;II)V

    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1328
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->newSpenListener()V

    .line 1329
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->appendPage()Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 1330
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    const v10, -0xd5d5d6

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setBackgroundColor(I)V

    .line 1332
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setBackgroundImageMode(I)V

    .line 1333
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setVolatileBackgroundImage(Landroid/graphics/Bitmap;)V

    .line 1335
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearHistory()V

    .line 1336
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mHistoryListener:Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setHistoryListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;)V

    .line 1337
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/high16 v10, -0x1000000

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setBlankColor(I)V

    .line 1338
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setControlListener(Lcom/samsung/android/sdk/pen/engine/SpenControlListener;)V

    .line 1339
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V

    .line 1340
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setZoomListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;)V

    .line 1341
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setPenDetachmentListener(Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;)V

    .line 1342
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V

    .line 1343
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    new-instance v10, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;

    const/4 v11, 0x0

    invoke-direct {v10, p0, v11}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;)V

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setPostDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V

    .line 1344
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    new-instance v10, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;

    const/4 v11, 0x0

    invoke-direct {v10, p0, v11}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Lcom/sec/android/mimage/photoretouching/pen/PenActivity$activityDrawListener;)V

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setPreDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V

    .line 1347
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    new-instance v10, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$selectionChangeListener;

    const/4 v11, 0x0

    invoke-direct {v10, p0, v11}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$selectionChangeListener;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;Lcom/sec/android/mimage/photoretouching/pen/PenActivity$selectionChangeListener;)V

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setSelectionChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;)V

    .line 1348
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V

    .line 1349
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    .line 1354
    const/16 v0, 0x90

    .line 1355
    .local v0, "colorPickerY":I
    const/4 v7, 0x1

    .line 1356
    .local v7, "settingMode":I
    const/4 v2, 0x2

    .line 1357
    .local v2, "fingerAction":I
    const/4 v8, 0x2

    .line 1358
    .local v8, "spenAction":I
    const/4 v3, 0x2

    .line 1359
    .local v3, "mouseAction":I
    const/4 v6, 0x4

    .line 1364
    .local v6, "penhaveremove":I
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v9}, Lcom/sec/android/pen/util/SettingView;->getSpenSettingPenLayout()Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v4

    .line 1365
    .local v4, "pen":Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    const/4 v9, 0x1

    invoke-virtual {v4, v9, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setColorPickerPosition(II)V

    .line 1368
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v9}, Lcom/sec/android/pen/util/SettingView;->closeSettingView()V

    .line 1369
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTipEnabled(Z)V

    .line 1370
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    .line 1371
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v10, 0x2

    invoke-virtual {v9, v10, v8}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    .line 1372
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10, v6}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    .line 1373
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v10, 0x3

    invoke-virtual {v9, v10, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    .line 1376
    const/4 v9, 0x1

    invoke-direct {p0, v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->refreshPenEraserBtnState(I)V

    .line 1377
    iget-boolean v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mUndoStarckEmpty:Z

    invoke-virtual {p0, v9}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->refreshUndoRedoBtnState(Z)V

    .line 1379
    new-instance v9, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->getPenInfoList()Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPenInfoList:Ljava/util/List;

    .line 1381
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mEventListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    invoke-virtual {v9, v10}, Lcom/sec/android/pen/util/SettingView;->setEraserListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;)V

    .line 1382
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->fitSPenIntoMotherLayout()V

    .line 1385
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v10, 0x2

    invoke-virtual {v9, v10, v8}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    .line 1392
    sget-object v9, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v9, :cond_0

    .line 1393
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    sget-object v10, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-virtual {v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 1394
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    sget-object v10, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevSpenSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-virtual {v9, v10}, Lcom/sec/android/pen/util/SettingView;->setSpenPenInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 1415
    :goto_1
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->update()V

    .line 1418
    return-void

    .line 1325
    .end local v0    # "colorPickerY":I
    .end local v2    # "fingerAction":I
    .end local v3    # "mouseAction":I
    .end local v4    # "pen":Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    .end local v6    # "penhaveremove":I
    .end local v7    # "settingMode":I
    .end local v8    # "spenAction":I
    :catch_0
    move-exception v1

    .line 1326
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1397
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "colorPickerY":I
    .restart local v2    # "fingerAction":I
    .restart local v3    # "mouseAction":I
    .restart local v4    # "pen":Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    .restart local v6    # "penhaveremove":I
    .restart local v7    # "settingMode":I
    .restart local v8    # "spenAction":I
    :cond_0
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v5

    .line 1398
    .local v5, "penInfo":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPenInfoList:Ljava/util/List;

    const/4 v10, 0x0

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    iput-object v9, v5, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 1399
    const/high16 v9, -0x1000000

    iput v9, v5, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 1400
    const/high16 v9, 0x41f00000    # 30.0f

    iput v9, v5, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 1402
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v9, v5}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 1403
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v9, v5}, Lcom/sec/android/pen/util/SettingView;->setSpenPenInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_1
.end method

.method private setSettingView()V
    .locals 6

    .prologue
    .line 854
    const v4, 0x7f090128

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mCanvasContainer:Landroid/widget/RelativeLayout;

    .line 855
    const v4, 0x7f090129

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingContainer:Landroid/widget/RelativeLayout;

    .line 856
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v2

    .line 857
    .local v2, "roi":Landroid/graphics/Rect;
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 858
    .local v3, "width":I
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 859
    .local v0, "height":I
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onConfigurationChanged mSMultiWindowActivity.isMultiWindow():"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 860
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 862
    add-int/lit8 v0, v0, -0x19

    .line 868
    :cond_0
    const v4, 0x7f0900ae

    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 870
    .local v1, "parent":Landroid/widget/FrameLayout;
    new-instance v4, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    .line 871
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mCanvasContainer:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 873
    invoke-virtual {p0, v3, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setCanvasSize(II)V

    .line 874
    invoke-virtual {p0, v3, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setSpenCanvas(II)V

    .line 878
    new-instance v4, Lcom/sec/android/pen/util/SettingView;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingContainer:Landroid/widget/RelativeLayout;

    invoke-direct {v4, p0, v5}, Lcom/sec/android/pen/util/SettingView;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;)V

    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    .line 879
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 880
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v4, v5}, Lcom/sec/android/pen/util/SettingView;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 882
    const-string v4, "hanyoung setSettingView"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 883
    return-void
.end method

.method private setViewMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 2230
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getToolTypeAction(I)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 2231
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/pen/util/SettingView;->setViewMode(II)V

    .line 2232
    :cond_0
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1496
    iput-boolean v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->configChange:Z

    .line 1497
    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v4, :cond_3

    .line 1499
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setPortrait()V

    .line 1506
    :cond_0
    :goto_0
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevOrientation:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 1507
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mCurrentMode:I

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentModeForActivity(I)V

    .line 1508
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->resetFlagSettingViewInit()V

    .line 1509
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mTrayManager:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;->configurationChange()V

    .line 1510
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 1511
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 1513
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1515
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1516
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeLayoutSize(I)V

    .line 1524
    :goto_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getPenColorLine()Landroid/widget/FrameLayout;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenColorImage:Landroid/widget/FrameLayout;

    .line 1526
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v1

    .line 1527
    .local v1, "roi":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 1528
    .local v2, "width":I
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 1529
    .local v0, "height":I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1531
    add-int/lit8 v0, v0, -0x19

    .line 1538
    :goto_2
    invoke-virtual {p0, v2, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setCanvasSize(II)V

    .line 1539
    invoke-direct {p0, v2, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->fitPopupIntoMotherLayout(II)V

    .line 1540
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->fitSPenIntoMotherLayout()V

    .line 1541
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->refreshMenuIcon(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 1542
    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mUndoStarckEmpty:Z

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->refreshUndoRedoBtnState(Z)V

    .line 1543
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/sec/android/pen/util/SettingView;->setVisibility(I)V

    .line 1544
    new-instance v3, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;-><init>(Lcom/sec/android/mimage/photoretouching/pen/PenActivity;)V

    new-array v4, v5, [Ljava/lang/Void;

    .line 1572
    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$21;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1574
    iget-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->isDrawing:Z

    if-eqz v3, :cond_2

    .line 1575
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mCurMotionEvent:Landroid/view/MotionEvent;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1576
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mCurMotionEvent:Landroid/view/MotionEvent;

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1579
    :cond_2
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenMultitouch:Z

    .line 1580
    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mZoomMultitouch:Z

    .line 1582
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1583
    return-void

    .line 1501
    .end local v0    # "height":I
    .end local v1    # "roi":Landroid/graphics/Rect;
    .end local v2    # "width":I
    :cond_3
    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 1503
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setLandscape()V

    goto/16 :goto_0

    .line 1520
    :cond_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 1521
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeLayoutSize(I)V

    goto/16 :goto_1

    .line 1535
    .restart local v0    # "height":I
    .restart local v1    # "roi":Landroid/graphics/Rect;
    .restart local v2    # "width":I
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v2, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1536
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x400

    .line 188
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 189
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->isLightThemeRequired()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 190
    const v3, 0x7f070003

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setTheme(I)V

    .line 192
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 193
    const/16 v3, 0x9

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->requestWindowFeature(I)Z

    .line 194
    const v3, 0x7f030053

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->setContentView(I)V

    .line 195
    const/high16 v3, 0x1a000000

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setCurrentMode(I)V

    .line 196
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->createManager()V

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 201
    .local v2, "win":Landroid/view/Window;
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 202
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 203
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 204
    const/high16 v3, 0x100000

    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/2addr v3, v4

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 205
    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->nfcAdapter:Landroid/nfc/NfcAdapter;

    .line 209
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->nfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v3, :cond_1

    .line 210
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->nfcAdapter:Landroid/nfc/NfcAdapter;

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-array v5, v5, [Landroid/app/Activity;

    invoke-virtual {v3, v4, p0, v5}, Landroid/nfc/NfcAdapter;->setNdefPushMessage(Landroid/nfc/NdefMessage;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 212
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 213
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->init(Landroid/content/Intent;)V

    .line 215
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 245
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 246
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->saveCurrentPenSetting()V

    .line 248
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    if-eqz v1, :cond_0

    .line 249
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->closeControl()V

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    if-eqz v1, :cond_1

    .line 253
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->close()V

    .line 254
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    .line 257
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    if-eqz v1, :cond_2

    .line 259
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 264
    :goto_0
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    .line 267
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    if-eqz v1, :cond_3

    .line 268
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    invoke-virtual {v1}, Lcom/sec/android/pen/util/SettingView;->close()V

    .line 269
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSettingView:Lcom/sec/android/pen/util/SettingView;

    .line 271
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_4

    .line 272
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 273
    :cond_4
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewBitmap:Landroid/graphics/Bitmap;

    .line 275
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mBitmap_bg:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_5

    .line 276
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mBitmap_bg:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 278
    :cond_5
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mBitmap_bg:Landroid/graphics/Bitmap;

    .line 280
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_6

    .line 281
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 282
    :cond_6
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mAngleTextBitmap:Landroid/graphics/Bitmap;

    .line 287
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mTrayManager:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;

    if-eqz v1, :cond_7

    .line 288
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mTrayManager:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;->destroy()V

    .line 289
    :cond_7
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mTrayManager:Lcom/sec/android/mimage/photoretouching/pen/PenActivity$PenTrayManager;

    .line 290
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_8

    .line 291
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->destroy()V

    .line 292
    :cond_8
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 293
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_9

    .line 294
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->destroy()V

    .line 295
    :cond_9
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 296
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v1, :cond_a

    .line 297
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 298
    :cond_a
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mDialogManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 299
    return-void

    .line 261
    :catch_0
    move-exception v0

    .line 262
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1763
    const/16 v0, 0x17

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-ne p1, v0, :cond_2

    .line 1765
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsEntered:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1766
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 1768
    :cond_1
    const/4 v0, 0x1

    .line 1770
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 1775
    const/16 v0, 0x17

    if-eq p1, v0, :cond_0

    const/16 v0, 0x42

    if-ne p1, v0, :cond_2

    .line 1777
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 1778
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    .line 1800
    :cond_1
    :goto_0
    return v1

    .line 1783
    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 1785
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->backPressed()V

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 218
    const-string v0, "android.nfc.action.TAG_DISCOVERED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    :goto_0
    return-void

    .line 220
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevOrientation:I

    .line 228
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mCurrentMode:I

    .line 229
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 230
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 2406
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 2407
    const-string v0, "isRestore"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsRestore:Z

    .line 2408
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mIsRestore:Z

    if-eqz v0, :cond_0

    .line 2410
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->finish()V

    .line 2413
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 233
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPrevOrientation:I

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_1

    .line 234
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenView:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->update()V

    .line 236
    :cond_0
    const/16 v0, 0x10

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->refreshPopupState(IZ)V

    .line 239
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 240
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 2399
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2400
    const-string v0, "isRestore"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2401
    return-void
.end method

.method public refreshMenuIcon(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    .prologue
    const v2, 0x1a001842

    .line 1587
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.FountainPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1588
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1589
    const v1, 0x7f020033

    .line 1588
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeSelectionBtnIcon(II)V

    .line 1611
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenColorImage:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 1612
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPenColorImage:Landroid/widget/FrameLayout;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    const/high16 v2, -0x1000000

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 1613
    :cond_1
    return-void

    .line 1590
    :cond_2
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ObliquePen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1591
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1592
    const v1, 0x7f020037

    .line 1591
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeSelectionBtnIcon(II)V

    goto :goto_0

    .line 1593
    :cond_3
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1594
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1595
    const v1, 0x7f020038

    .line 1594
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeSelectionBtnIcon(II)V

    goto :goto_0

    .line 1596
    :cond_4
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1597
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1598
    const v1, 0x7f020032

    .line 1597
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeSelectionBtnIcon(II)V

    goto :goto_0

    .line 1599
    :cond_5
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1600
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1601
    const v1, 0x7f020031

    .line 1600
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeSelectionBtnIcon(II)V

    goto :goto_0

    .line 1602
    :cond_6
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1603
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1604
    const v1, 0x7f020034

    .line 1603
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeSelectionBtnIcon(II)V

    goto :goto_0

    .line 1605
    :cond_7
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1606
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1607
    const v1, 0x7f020036

    .line 1606
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeSelectionBtnIcon(II)V

    goto/16 :goto_0

    .line 1608
    :cond_8
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1609
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1610
    const v1, 0x7f020035

    .line 1609
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->changeSelectionBtnIcon(II)V

    goto/16 :goto_0
.end method

.method public refreshUndoRedoBtnState(Z)V
    .locals 3
    .param p1, "undoStackEmpty"    # Z

    .prologue
    .line 1731
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isUndoable()Z

    move-result v1

    .line 1732
    .local v1, "undoable":Z
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSpenPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isRedoable()Z

    move-result v0

    .line 1733
    .local v0, "redoable":Z
    if-eqz v1, :cond_0

    .line 1735
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->ableUndo()V

    .line 1737
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 1749
    :goto_0
    if-eqz v0, :cond_1

    .line 1751
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->ableRedo()V

    .line 1759
    :goto_1
    return-void

    .line 1745
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->unableUndo()V

    .line 1747
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    goto :goto_0

    .line 1756
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->unableRedo()V

    goto :goto_1
.end method

.method public setCanvasSize(II)V
    .locals 9
    .param p1, "viewWidth"    # I
    .param p2, "viewHeight"    # I

    .prologue
    const/4 v8, 0x0

    .line 1617
    const/4 v2, 0x0

    .line 1618
    .local v2, "scale":F
    const/4 v4, 0x0

    .line 1619
    .local v4, "scaleW":F
    const/4 v3, 0x0

    .line 1621
    .local v3, "scaleH":F
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v1, v5, Landroid/content/res/Configuration;->orientation:I

    .line 1623
    .local v1, "orientation":I
    const/4 v5, 0x1

    if-ne v1, v5, :cond_2

    .line 1624
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setPortrait()V

    .line 1625
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1626
    .local v0, "display":Landroid/util/DisplayMetrics;
    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v6, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setDisplaySize(II)V

    .line 1627
    int-to-float v5, p2

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewHeight:I

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 1628
    int-to-float v5, p1

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    int-to-float v6, v6

    div-float v4, v5, v6

    .line 1629
    cmpl-float v5, v3, v4

    if-ltz v5, :cond_1

    .line 1630
    move v2, v4

    .line 1647
    .end local v0    # "display":Landroid/util/DisplayMetrics;
    :cond_0
    :goto_0
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    int-to-float v5, v5

    mul-float/2addr v5, v2

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenInitialWidth:I

    .line 1648
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewHeight:I

    int-to-float v5, v5

    mul-float/2addr v5, v2

    float-to-int v5, v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenInitialHeight:I

    .line 1650
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSrcImageRect:Landroid/graphics/Rect;

    if-eqz v5, :cond_4

    .line 1651
    new-instance v5, Landroid/graphics/Rect;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenInitialWidth:I

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenInitialHeight:I

    invoke-direct {v5, v8, v8, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSrcImageRect:Landroid/graphics/Rect;

    .line 1655
    :goto_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->getActionbarHeight()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mViewButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getButtonHeight()I

    move-result v6

    add-int/2addr v5, v6

    sub-int/2addr p2, v5

    .line 1657
    return-void

    .line 1632
    .restart local v0    # "display":Landroid/util/DisplayMetrics;
    :cond_1
    move v2, v3

    .line 1633
    goto :goto_0

    .line 1634
    .end local v0    # "display":Landroid/util/DisplayMetrics;
    :cond_2
    const/4 v5, 0x2

    if-ne v1, v5, :cond_0

    .line 1635
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setLandscape()V

    .line 1636
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1637
    .restart local v0    # "display":Landroid/util/DisplayMetrics;
    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v6, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v5, v6}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setDisplaySize(II)V

    .line 1639
    int-to-float v5, p2

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewHeight:I

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 1640
    int-to-float v5, p1

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    int-to-float v6, v6

    div-float v4, v5, v6

    .line 1641
    cmpl-float v5, v3, v4

    if-ltz v5, :cond_3

    .line 1642
    move v2, v4

    goto :goto_0

    .line 1644
    :cond_3
    move v2, v3

    goto :goto_0

    .line 1653
    .end local v0    # "display":Landroid/util/DisplayMetrics;
    :cond_4
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSrcImageRect:Landroid/graphics/Rect;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenInitialWidth:I

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenInitialHeight:I

    invoke-virtual {v5, v8, v8, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1
.end method

.method public setSpenCanvas(II)V
    .locals 15
    .param p1, "viewWidth"    # I
    .param p2, "viewHeight"    # I

    .prologue
    .line 1661
    const/4 v2, 0x0

    .line 1663
    .local v2, "scale":F
    const/4 v6, 0x0

    .line 1664
    .local v6, "scaleP":F
    const/4 v5, 0x0

    .line 1666
    .local v5, "scaleL":F
    const/4 v8, 0x0

    .line 1667
    .local v8, "scaleWP":F
    const/4 v4, 0x0

    .line 1669
    .local v4, "scaleHP":F
    const/4 v7, 0x0

    .line 1670
    .local v7, "scaleWL":F
    const/4 v3, 0x0

    .line 1672
    .local v3, "scaleHL":F
    const/4 v12, 0x0

    .line 1673
    .local v12, "viewWdtP":I
    const/4 v10, 0x0

    .line 1675
    .local v10, "viewHgtP":I
    const/4 v11, 0x0

    .line 1676
    .local v11, "viewWdtL":I
    const/4 v9, 0x0

    .line 1678
    .local v9, "viewHgtL":I
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v13

    iget v1, v13, Landroid/content/res/Configuration;->orientation:I

    .line 1680
    .local v1, "orientation":I
    const/4 v13, 0x1

    if-ne v1, v13, :cond_2

    .line 1681
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setPortrait()V

    .line 1683
    move/from16 v12, p1

    .line 1684
    move/from16 v10, p2

    .line 1686
    move/from16 v11, p2

    .line 1687
    move/from16 v9, p1

    .line 1701
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1703
    .local v0, "display":Landroid/util/DisplayMetrics;
    int-to-float v13, v10

    iget v14, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewHeight:I

    int-to-float v14, v14

    div-float v4, v13, v14

    .line 1704
    int-to-float v13, v12

    iget v14, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    int-to-float v14, v14

    div-float v8, v13, v14

    .line 1705
    cmpl-float v13, v4, v8

    if-ltz v13, :cond_3

    .line 1706
    move v6, v8

    .line 1713
    :goto_1
    int-to-float v13, v9

    iget v14, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewHeight:I

    int-to-float v14, v14

    div-float v3, v13, v14

    .line 1714
    int-to-float v13, v11

    iget v14, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    int-to-float v14, v14

    div-float v7, v13, v14

    .line 1715
    cmpl-float v13, v3, v7

    if-ltz v13, :cond_4

    .line 1716
    move v5, v7

    .line 1720
    :goto_2
    move v2, v6

    .line 1721
    cmpg-float v13, v6, v5

    if-gez v13, :cond_1

    .line 1722
    move v2, v5

    .line 1724
    :cond_1
    iget v13, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewWidth:I

    int-to-float v13, v13

    mul-float/2addr v13, v2

    float-to-int v13, v13

    iput v13, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenCanvasWidth:I

    .line 1725
    iget v13, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mPreviewHeight:I

    int-to-float v13, v13

    mul-float/2addr v13, v2

    float-to-int v13, v13

    iput v13, p0, Lcom/sec/android/mimage/photoretouching/pen/PenActivity;->mSPenCanvasHeight:I

    .line 1727
    return-void

    .line 1690
    .end local v0    # "display":Landroid/util/DisplayMetrics;
    :cond_2
    const/4 v13, 0x2

    if-ne v1, v13, :cond_0

    .line 1691
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->setLandscape()V

    .line 1693
    move/from16 v12, p2

    .line 1694
    move/from16 v10, p1

    .line 1696
    move/from16 v11, p1

    .line 1697
    move/from16 v9, p2

    goto :goto_0

    .line 1708
    .restart local v0    # "display":Landroid/util/DisplayMetrics;
    :cond_3
    move v6, v4

    goto :goto_1

    .line 1718
    :cond_4
    move v5, v3

    goto :goto_2
.end method
