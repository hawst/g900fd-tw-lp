.class Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;
.super Ljava/lang/Object;
.source "LabelView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->initLabelTextDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    .line 891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;)Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;
    .locals 1

    .prologue
    .line 891
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "di"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v5, 0x1

    .line 895
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 898
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mIsResetTabLayout:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 901
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Landroid/content/Context;

    move-result-object v2

    .line 902
    const-string v3, "input_method"

    .line 901
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 903
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    check-cast p1, Landroid/app/Dialog;

    .end local p1    # "di":Landroid/content/DialogInterface;
    const v2, 0x7f09001f

    invoke-virtual {p1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Gui/InputText;

    .line 904
    .local v0, "eView":Lcom/sec/android/mimage/photoretouching/Gui/InputText;
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 906
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;Landroid/view/inputmethod/InputMethodManager;Lcom/sec/android/mimage/photoretouching/Gui/InputText;)V

    .line 918
    const-wide/16 v4, 0x64

    .line 906
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 928
    .end local v0    # "eView":Lcom/sec/android/mimage/photoretouching/Gui/InputText;
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    :goto_0
    return-void

    .line 922
    .restart local v0    # "eView":Lcom/sec/android/mimage/photoretouching/Gui/InputText;
    .restart local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getCurrnetType()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v4

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->showTabLayout(IZZ)V
    invoke-static {v2, v3, v5, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;IZZ)V

    .line 923
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;

    invoke-static {v2, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;->access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/LabelView;Z)V

    goto :goto_0
.end method
