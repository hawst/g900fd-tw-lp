.class Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;
.super Ljava/lang/Object;
.source "MultigridStyleView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/multigrid/Interface/MultigridProgressDialogAsyncTask$DoAsyncTaskCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->saveToFile()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    .line 2488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doInBackground()V
    .locals 3

    .prologue
    .line 2515
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->getSaveFilePath()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$35(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Ljava/lang/String;

    move-result-object v0

    .line 2516
    .local v0, "savePath":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->saveCollage(Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v2, v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$36(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$37(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Landroid/net/Uri;)V

    .line 2517
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cheus, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : mSaveFileUri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mSaveFileUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$12(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 2518
    return-void
.end method

.method public onPostExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2493
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # invokes: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->setSaveable(Z)Z
    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$22(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;Z)Z

    .line 2500
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$34(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)I

    move-result v0

    const v1, 0x7f090157

    if-eq v0, v1, :cond_0

    .line 2501
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$34(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)I

    move-result v0

    const v1, 0x7f090158

    if-ne v0, v1, :cond_2

    .line 2503
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$34(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->onOptionsItemSelected(I)V

    .line 2504
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    invoke-static {v0, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$13(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;I)V

    .line 2511
    :cond_1
    :goto_0
    return-void

    .line 2506
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    # getter for: Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->mCurrentOptionId:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->access$34(Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 2508
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView$23;->this$0:Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/split/MultigridStyleView;->backPressed()V

    goto :goto_0
.end method
