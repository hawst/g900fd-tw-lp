.class public Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;
.super Ljava/lang/Object;
.source "ApplyOriginalThreadManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;,
        Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;,
        Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;
    }
.end annotation


# static fields
.field public static final APPLY_EFFECT:I = 0x0

.field public static final APPLY_MASK:I = 0x5

.field public static final REDO:I = 0x2

.field public static final REDO_ALL:I = 0x4

.field public static final UNDO:I = 0x1

.field public static final UNDO_ALL:I = 0x3

.field private static mDoing:Z


# instance fields
.field private mApplyList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

.field private myCallback:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mDoing:Z

    .line 192
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;)V
    .locals 1
    .param p1, "onCallBack"    # Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    .line 183
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mApplyList:Ljava/util/ArrayList;

    .line 184
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;

    .line 27
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mApplyList:Ljava/util/ArrayList;

    .line 29
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;

    .line 30
    return-void
.end method

.method static synthetic access$0(Z)V
    .locals 0

    .prologue
    .line 185
    sput-boolean p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mDoing:Z

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mApplyList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->applyData(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;)V

    return-void
.end method

.method private applyData(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;)V
    .locals 2
    .param p1, "applyinfo"    # Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;

    .prologue
    .line 63
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mMode:I

    packed-switch v0, :pswitch_data_0

    .line 93
    :cond_0
    :goto_0
    const-string v0, "JW, applyData end"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 95
    return-void

    .line 66
    :pswitch_0
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->setEffect(Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;)V

    goto :goto_0

    .line 70
    :pswitch_1
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;

    iget-object v1, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;->undo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    goto :goto_0

    .line 74
    :pswitch_2
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;

    iget-object v1, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;->redo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    goto :goto_0

    .line 78
    :pswitch_3
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;

    iget-object v1, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;->undoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    goto :goto_0

    .line 82
    :pswitch_4
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;

    iget-object v1, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;->redoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    goto :goto_0

    .line 86
    :pswitch_5
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->applyOriginal()[I

    .line 89
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->destroy()V

    goto :goto_0

    .line 63
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static isDoingState()Z
    .locals 2

    .prologue
    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW isDoingThread: mDoing="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mDoing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 115
    sget-boolean v0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mDoing:Z

    if-nez v0, :cond_0

    .line 116
    const/4 v0, 0x0

    .line 117
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setData(Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;Lcom/sec/android/mimage/photoretouching/Core/ImageData;I)V
    .locals 2
    .param p1, "effectinfo"    # Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    .param p2, "imagedata"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p3, "mode"    # I

    .prologue
    .line 41
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;)V

    .line 42
    .local v0, "applyinfo":Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;
    iput-object p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mEffectInfo:Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    .line 43
    iput-object p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 44
    iput p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mMode:I

    .line 45
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mApplyList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->startThread()V

    .line 47
    return-void
.end method

.method private setEffect(Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;)V
    .locals 4
    .param p1, "effectinfo"    # Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->myCallback:Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->applyOriginal()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->getHeight()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$OnCallBackForHistory;->addHistory([III)V

    .line 100
    invoke-virtual {p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;->destroy()V

    .line 101
    return-void
.end method

.method private startThread()V
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    invoke-direct {v0}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    .line 54
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW, mDoing = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mDoing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mDoing:Z

    if-nez v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mThreadPool:Lcom/sec/android/mimage/photoretouching/util/ThreadPool;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyThread;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/ThreadPool;->submit(Lcom/sec/android/mimage/photoretouching/util/ThreadPool$Job;)Lcom/sec/android/mimage/photoretouching/util/Future;

    .line 59
    :cond_1
    return-void
.end method


# virtual methods
.method public isDoingThread()Z
    .locals 2

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW isDoingThread: mDoing="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v1, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mDoing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW isDoingThread: mApplyList.size()="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mApplyList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 107
    sget-boolean v0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mDoing:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mApplyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 108
    const/4 v0, 0x0

    .line 109
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isUndoRedoThread()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 122
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "JW isUndoRedoThread: mDoing="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v3, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mDoing:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 123
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "JW isUndoRedoThread: mApplyList.size()="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mApplyList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 124
    sget-boolean v2, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mDoing:Z

    if-eqz v2, :cond_1

    .line 125
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->mApplyList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 130
    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    return v1

    .line 125
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;

    .line 126
    .local v0, "temp":Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;
    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mMode:I

    if-eq v3, v1, :cond_2

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager$ApplyInfo;->mMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    goto :goto_0
.end method

.method public setData(Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;I)V
    .locals 1
    .param p1, "effectinfo"    # Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    .param p2, "mode"    # I

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->setData(Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;Lcom/sec/android/mimage/photoretouching/Core/ImageData;I)V

    .line 34
    return-void
.end method

.method public setData(Lcom/sec/android/mimage/photoretouching/Core/ImageData;I)V
    .locals 1
    .param p1, "imagedata"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p2, "mode"    # I

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/ApplyOriginalThreadManager;->setData(Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;Lcom/sec/android/mimage/photoretouching/Core/ImageData;I)V

    .line 38
    return-void
.end method
