.class Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;
.super Ljava/lang/Object;
.source "AdjustmentView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->init2DepthActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    .line 580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnLongFunction(Landroid/view/View;)V
    .locals 2
    .param p1, "V"    # Landroid/view/View;

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 616
    return-void
.end method

.method public TouchFunction(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 584
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewHeight()I
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->undo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    .line 585
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 586
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 588
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 589
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 601
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 602
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 603
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 609
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->refreshImageAndBottomButtons()V
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 610
    return-void

    .line 591
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_0

    .line 593
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    .line 594
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableSave()V

    .line 595
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 596
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 599
    :goto_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setIsEdited(Z)V

    goto :goto_0

    .line 598
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0, v5, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_2

    .line 605
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 606
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto :goto_1
.end method

.method public TouchFunction(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 622
    return-void
.end method
