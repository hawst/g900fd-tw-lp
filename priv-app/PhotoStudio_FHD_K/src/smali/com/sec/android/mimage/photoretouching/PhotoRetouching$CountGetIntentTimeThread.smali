.class Lcom/sec/android/mimage/photoretouching/PhotoRetouching$CountGetIntentTimeThread;
.super Ljava/lang/Thread;
.source "PhotoRetouching.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/PhotoRetouching;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CountGetIntentTimeThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;


# direct methods
.method public constructor <init>(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)V
    .locals 1

    .prologue
    .line 1169
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$CountGetIntentTimeThread;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    .line 1168
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1170
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->access$3(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Z)V

    .line 1171
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/16 v5, 0x32

    const/4 v4, 0x1

    .line 1173
    const/4 v1, 0x0

    .line 1174
    .local v1, "sleepCount":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$CountGetIntentTimeThread;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    # getter for: Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mIsGetIntentFinish:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->access$4(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)Z

    move-result v2

    if-nez v2, :cond_0

    if-lt v1, v5, :cond_1

    .line 1184
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$CountGetIntentTimeThread;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    # getter for: Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->mIsGetIntentFinish:Z
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->access$4(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)Z

    move-result v2

    if-nez v2, :cond_2

    if-ne v1, v5, :cond_2

    .line 1186
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$CountGetIntentTimeThread;->destroy()V

    .line 1187
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$CountGetIntentTimeThread;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    # invokes: Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->prcessingException()V
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->access$5(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;)V

    .line 1188
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$CountGetIntentTimeThread;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->access$3(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Z)V

    .line 1192
    :goto_1
    return-void

    .line 1177
    :cond_1
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1182
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1178
    :catch_0
    move-exception v0

    .line 1180
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 1191
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$CountGetIntentTimeThread;->this$0:Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-static {v2, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->access$3(Lcom/sec/android/mimage/photoretouching/PhotoRetouching;Z)V

    goto :goto_1
.end method
