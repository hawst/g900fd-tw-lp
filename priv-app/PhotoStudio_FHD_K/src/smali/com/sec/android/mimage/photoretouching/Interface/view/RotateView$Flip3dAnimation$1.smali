.class Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;
.super Ljava/lang/Object;
.source "RotateView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->setAnimationListenerCallback()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    .line 1157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 1176
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setEnabled(Z)V

    .line 1177
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->enableButtons()V

    .line 1178
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;Z)V

    .line 1179
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mAnimType:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1196
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->makeStraightenLines()V

    .line 1209
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mAnimType:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->changeDoneCancelStatus(I)V

    .line 1210
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->startImageEditViewAnimation(Landroid/view/animation/Animation;)V
    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;Landroid/view/animation/Animation;)V

    .line 1211
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->invalidateViews()V

    .line 1213
    return-void

    .line 1182
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mToScale:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->turnLeft(F)V

    goto :goto_0

    .line 1185
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->mToScale:F
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->turnRight(F)V

    goto :goto_0

    .line 1188
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->flipHorizontal()V

    goto :goto_0

    .line 1191
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->flipVertical()V

    goto :goto_0

    .line 1179
    :pswitch_data_0
    .packed-switch 0x11101101
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 1171
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x0

    .line 1162
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->disableButtons()V

    .line 1163
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setEnabled(Z)V

    .line 1164
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;)Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;

    move-result-object v0

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->mRotateEffect:Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;->access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView;)Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/RotateEffect;->setStraightenEnable(Z)V

    .line 1165
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation$1;->this$1:Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/RotateView$Flip3dAnimation;Z)V

    .line 1166
    return-void
.end method
