.class public final Lcom/sec/android/mimage/photoretouching/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Image_saved_in_ps:I = 0x7f0601df

.field public static final Navigate_up:I = 0x7f0601f9

.field public static final Not_enough_space_in_device_storage_delete_some_files_and_try_again:I = 0x7f0601e2

.field public static final ONLY_PICTURES_TAKEN_USING_THE_SHOT_AND_MORE_OPTIONS_IN_CAMERA_CAN_BE_EDITED_IN_THIS_STUDIO:I = 0x7f0601c5

.field public static final Object_Expressme:I = 0x7f0601e0

.field public static final STUDIO_BODY_EDIT_YOUR_IMAGE_THEN_MOVE_THE_PART_OF_THE_IMAGE_YOU_WANT_TO_SAVE_INTO_THE_MARKED_AREA_MSG:I = 0x7f0601f7

.field public static final Unable_to_open_application_delete_some_files_in_device_storage_and_try_again:I = 0x7f0601e1

.field public static final access_clear_button:I = 0x7f0600ba

.field public static final access_close_button:I = 0x7f0600b7

.field public static final access_erasersetting:I = 0x7f0600b9

.field public static final access_pensetting:I = 0x7f0600b6

.field public static final add_effect:I = 0x7f060137

.field public static final add_effect_artistic:I = 0x7f06013e

.field public static final add_effect_blended:I = 0x7f060140

.field public static final add_effect_distort:I = 0x7f06013f

.field public static final add_pictures:I = 0x7f060149

.field public static final add_selection:I = 0x7f060023

.field public static final addmorepicture:I = 0x7f060123

.field public static final addpicture:I = 0x7f06009b

.field public static final adjustment:I = 0x7f060185

.field public static final adjustrgb:I = 0x7f06003a

.field public static final advanced_effects:I = 0x7f0600b1

.field public static final airbrush_face:I = 0x7f060157

.field public static final alert_dialog_cannot_move:I = 0x7f060079

.field public static final alert_dialog_cannot_open_image:I = 0x7f06006f

.field public static final alert_dialog_change_grid:I = 0x7f0600ab

.field public static final alert_dialog_change_picture:I = 0x7f0600a0

.field public static final alert_dialog_collage_minimum_number_of_images:I = 0x7f060156

.field public static final alert_dialog_current_area:I = 0x7f060070

.field public static final alert_dialog_different_resolution:I = 0x7f060077

.field public static final alert_dialog_dont_ask_again:I = 0x7f060072

.field public static final alert_dialog_drag_effect:I = 0x7f0600ae

.field public static final alert_dialog_enter_filename:I = 0x7f0600e5

.field public static final alert_dialog_file_name_already_exists:I = 0x7f0600da

.field public static final alert_dialog_maximum_number_of_images:I = 0x7f0600fd

.field public static final alert_dialog_maximum_number_of_sticker:I = 0x7f0600c3

.field public static final alert_dialog_minimum_size_image:I = 0x7f0600c4

.field public static final alert_dialog_mirror:I = 0x7f0600a4

.field public static final alert_dialog_no_more_edits:I = 0x7f0600c0

.field public static final alert_dialog_no_region_selected:I = 0x7f060076

.field public static final alert_dialog_not_enough_mem_unable_launch_app:I = 0x7f06007c

.field public static final alert_dialog_not_enough_mem_unable_save_file:I = 0x7f06007b

.field public static final alert_dialog_nothing_to_save:I = 0x7f060078

.field public static final alert_dialog_over_character:I = 0x7f060083

.field public static final alert_dialog_over_region_20percent:I = 0x7f060071

.field public static final alert_dialog_please_wait:I = 0x7f060075

.field public static final alert_dialog_redoall:I = 0x7f06009d

.field public static final alert_dialog_reduce_selected_area:I = 0x7f06008d

.field public static final alert_dialog_save_as:I = 0x7f060074

.field public static final alert_dialog_save_rotation_first:I = 0x7f0600d7

.field public static final alert_dialog_save_straighten_first:I = 0x7f0600d8

.field public static final alert_dialog_save_success:I = 0x7f060073

.field public static final alert_dialog_sdcard_not_available:I = 0x7f06007a

.field public static final alert_dialog_select_more_pictures:I = 0x7f0600a2

.field public static final alert_dialog_turn_on_gps_or_network:I = 0x7f060084

.field public static final alert_dialog_unable_to_select_more_pictures:I = 0x7f0600a5

.field public static final alert_dialog_undoall:I = 0x7f06009c

.field public static final alert_dialog_want_to_save_before_exit:I = 0x7f06007e

.field public static final alert_dialog_want_to_save_before_open:I = 0x7f06007d

.field public static final alert_invalid_character:I = 0x7f060086

.field public static final alert_unable_to_save:I = 0x7f060087

.field public static final alert_unable_to_share:I = 0x7f060088

.field public static final alert_warning:I = 0x7f060085

.field public static final all_changes_will_be_discarded:I = 0x7f0601cf

.field public static final all_changes_will_be_discarded_new:I = 0x7f0601d9

.field public static final all_changes_will_be_reapplied:I = 0x7f0601ce

.field public static final allselection:I = 0x7f060027

.field public static final app_name:I = 0x7f060000

.field public static final app_name_camera:I = 0x7f0600f6

.field public static final area_selected:I = 0x7f06008c

.field public static final art_brush:I = 0x7f0601fd

.field public static final artistic_filters:I = 0x7f06014a

.field public static final auto:I = 0x7f060188

.field public static final autoadjustment:I = 0x7f060036

.field public static final autumn_brown:I = 0x7f06011c

.field public static final beauty:I = 0x7f060062

.field public static final beauty_face:I = 0x7f06012d

.field public static final beauty_face_lower:I = 0x7f060111

.field public static final beauty_face_upper:I = 0x7f060110

.field public static final before:I = 0x7f0601b1

.field public static final blackandwhite:I = 0x7f0600f3

.field public static final blended_filters:I = 0x7f06014b

.field public static final blue:I = 0x7f060096

.field public static final blue_tint:I = 0x7f06018b

.field public static final bluewash:I = 0x7f0600c9

.field public static final blur:I = 0x7f060052

.field public static final brighten_face:I = 0x7f060191

.field public static final brightness:I = 0x7f060037

.field public static final brush:I = 0x7f0600b3

.field public static final brush_selection:I = 0x7f060030

.field public static final brush_size:I = 0x7f060118

.field public static final brush_size_lower:I = 0x7f06011a

.field public static final brush_size_upper:I = 0x7f060119

.field public static final brushsize:I = 0x7f0600ca

.field public static final calligraphy_brush:I = 0x7f0601fe

.field public static final camera:I = 0x7f06001f

.field public static final cancel:I = 0x7f060009

.field public static final cartoon:I = 0x7f06015e

.field public static final cartoonize:I = 0x7f060048

.field public static final change_grid:I = 0x7f0600ac

.field public static final change_picture:I = 0x7f06009e

.field public static final character:I = 0x7f0600a8

.field public static final clear_all:I = 0x7f0600bb

.field public static final clipboard:I = 0x7f0600fc

.field public static final close:I = 0x7f0600fb

.field public static final collage_bottom_btn_01_proportions:I = 0x7f060194

.field public static final collage_bottom_btn_02_layout:I = 0x7f060195

.field public static final collage_bottom_btn_03_border:I = 0x7f060196

.field public static final collage_bottom_btn_04_background:I = 0x7f060197

.field public static final collage_popup_add_new_image:I = 0x7f060199

.field public static final collage_popup_remove:I = 0x7f06019c

.field public static final collage_popup_replace:I = 0x7f0601a8

.field public static final collage_studio:I = 0x7f060182

.field public static final collages_must_contain_at_least_pd_images:I = 0x7f0601aa

.field public static final color:I = 0x7f060035

.field public static final color_fade:I = 0x7f06012b

.field public static final colour_noise:I = 0x7f0600ea

.field public static final comic:I = 0x7f0600cb

.field public static final contrast:I = 0x7f060038

.field public static final copy:I = 0x7f060066

.field public static final copy_to_another:I = 0x7f060065

.field public static final copy_to_image:I = 0x7f0600de

.field public static final copy_to_original:I = 0x7f060103

.field public static final copy_to_other:I = 0x7f060104

.field public static final create_label:I = 0x7f0601bd

.field public static final crop:I = 0x7f060034

.field public static final crop_ratio:I = 0x7f0600c1

.field public static final cubism:I = 0x7f060167

.field public static final cut:I = 0x7f06017b

.field public static final darken:I = 0x7f06003e

.field public static final dawn_cast:I = 0x7f06012e

.field public static final decoration:I = 0x7f060058

.field public static final decoration_and_mirror_not_supported_for_images_with_aspect_ratios_wider_than_pd_pd:I = 0x7f0601ba

.field public static final decoration_not_supported_for_images_with_aspect_ratios_wider_than_pd_pd:I = 0x7f06019f

.field public static final delete:I = 0x7f0600d3

.field public static final deselect:I = 0x7f060193

.field public static final deselection:I = 0x7f060026

.field public static final disable:I = 0x7f0601f8

.field public static final discard:I = 0x7f0601b2

.field public static final discard_changes:I = 0x7f0601d7

.field public static final discard_image:I = 0x7f06019e

.field public static final discard_new:I = 0x7f0601da

.field public static final distortion_filters:I = 0x7f06014c

.field public static final done:I = 0x7f060008

.field public static final downlight:I = 0x7f0600cc

.field public static final download:I = 0x7f0601de

.field public static final drag_and_drop_here:I = 0x7f06014d

.field public static final draw_around_the_area_you_want_to_crop:I = 0x7f0601bf

.field public static final draw_eraser:I = 0x7f06008a

.field public static final draw_eraser_clear_all:I = 0x7f06008b

.field public static final draw_pen_setting:I = 0x7f060089

.field public static final drawing:I = 0x7f0600d4

.field public static final earlybird:I = 0x7f0600eb

.field public static final edgelens:I = 0x7f060041

.field public static final edit:I = 0x7f0600dc

.field public static final edit_image:I = 0x7f0600db

.field public static final edit_photo:I = 0x7f060098

.field public static final effect:I = 0x7f060043

.field public static final effect_manager:I = 0x7f06018f

.field public static final effect_will_be_applied_by_drawing_finger_across_screen:I = 0x7f0600fe

.field public static final effects:I = 0x7f060187

.field public static final emotion:I = 0x7f0600a7

.field public static final engraving:I = 0x7f060160

.field public static final enhance:I = 0x7f0601b8

.field public static final entertainer:I = 0x7f06011d

.field public static final erase_all:I = 0x7f0601a7

.field public static final eraser:I = 0x7f060068

.field public static final error:I = 0x7f06008e

.field public static final exposure:I = 0x7f06003b

.field public static final express_me:I = 0x7f0601f1

.field public static final eyes:I = 0x7f060130

.field public static final face_beauty:I = 0x7f0600d5

.field public static final face_brightness:I = 0x7f060158

.field public static final face_reshaping:I = 0x7f06015b

.field public static final face_retouch:I = 0x7f06010d

.field public static final face_retouch_lower:I = 0x7f06010f

.field public static final face_retouch_upper:I = 0x7f06010e

.field public static final face_shape:I = 0x7f060113

.field public static final face_tone:I = 0x7f06010c

.field public static final facebeauty:I = 0x7f060049

.field public static final fade:I = 0x7f06018c

.field public static final faded_colour:I = 0x7f060161

.field public static final fadedcolours:I = 0x7f0601fa

.field public static final failed_to_apply_the_effect:I = 0x7f06015c

.field public static final filter:I = 0x7f06005b

.field public static final fisheye:I = 0x7f0601fb

.field public static final fixed_ratio:I = 0x7f0600c2

.field public static final flip_horizontal:I = 0x7f060017

.field public static final flip_vertical:I = 0x7f060018

.field public static final frame:I = 0x7f060105

.field public static final frames:I = 0x7f06006a

.field public static final free:I = 0x7f060092

.field public static final free_paint:I = 0x7f06013a

.field public static final free_ratio:I = 0x7f0600d6

.field public static final from_clipboard:I = 0x7f060012

.field public static final from_map:I = 0x7f060013

.field public static final funny_face:I = 0x7f06014e

.field public static final gallery:I = 0x7f06001e

.field public static final get_creative_with_pictures_and_videos:I = 0x7f0601ac

.field public static final ghost_effect:I = 0x7f060060

.field public static final go_to_home:I = 0x7f0600e7

.field public static final gothic_noir:I = 0x7f06013b

.field public static final grab:I = 0x7f0600b2

.field public static final green:I = 0x7f060095

.field public static final green_tint:I = 0x7f06018a

.field public static final greyscale:I = 0x7f0601d0

.field public static final halftone:I = 0x7f0600e8

.field public static final heart_shape:I = 0x7f060168

.field public static final high:I = 0x7f0601b5

.field public static final hint_where:I = 0x7f060002

.field public static final home:I = 0x7f0600e9

.field public static final hue:I = 0x7f06003c

.field public static final icon:I = 0x7f0600cd

.field public static final if_you_cannot_see_the_guide_area_go_to_express_me_edit_and_enable_layer_effect:I = 0x7f0601f2

.field public static final image_enhanced:I = 0x7f0601ad

.field public static final image_saved_in_studio_folder:I = 0x7f0601c7

.field public static final image_saved_to_studio_folder:I = 0x7f0601a2

.field public static final images_saved_in_studio_folder:I = 0x7f0601c6

.field public static final impressionist:I = 0x7f060139

.field public static final insert:I = 0x7f060011

.field public static final instant_camera:I = 0x7f06006b

.field public static final instant_pic:I = 0x7f060169

.field public static final introduction_copy_to_another:I = 0x7f060082

.field public static final introduction_ghost_effect:I = 0x7f060080

.field public static final introduction_new_selection:I = 0x7f06007f

.field public static final introduction_red_eye_removal:I = 0x7f0600d9

.field public static final introduction_speedline_effect:I = 0x7f060081

.field public static final inverse:I = 0x7f060117

.field public static final inverse_selection:I = 0x7f060025

.field public static final invert:I = 0x7f06003d

.field public static final label:I = 0x7f0601c1

.field public static final large:I = 0x7f06002c

.field public static final lasso:I = 0x7f060091

.field public static final lasso_selection:I = 0x7f06002f

.field public static final leather:I = 0x7f06006e

.field public static final lens_blur:I = 0x7f06005e

.field public static final life:I = 0x7f060201

.field public static final light_streak:I = 0x7f06012f

.field public static final lighten:I = 0x7f06003f

.field public static final lightflare:I = 0x7f0600ec

.field public static final linear_blur:I = 0x7f06005c

.field public static final location_map:I = 0x7f060014

.field public static final lomo:I = 0x7f060044

.field public static final low:I = 0x7f0601b7

.field public static final magicdrawing:I = 0x7f060128

.field public static final magicpen:I = 0x7f06004e

.field public static final magnetic:I = 0x7f0600c6

.field public static final magnetic_selection:I = 0x7f06002e

.field public static final man:I = 0x7f06013c

.field public static final map_no_result:I = 0x7f060003

.field public static final marquee:I = 0x7f0601c0

.field public static final matt:I = 0x7f06006c

.field public static final maximum_number_of_characters_hpd_exceeded:I = 0x7f060178

.field public static final maximum_number_of_characters_reached:I = 0x7f0601c9

.field public static final maximum_number_of_images_reached:I = 0x7f0600ff

.field public static final maximum_number_of_labes_reached:I = 0x7f0601a6

.field public static final maximum_number_of_lines_is_3:I = 0x7f060179

.field public static final maximum_number_of_pictures_hpd_reached:I = 0x7f06015d

.field public static final maximum_number_of_stickers_pd_reached:I = 0x7f0601d2

.field public static final maximum_of_10_people_can_be_recognised_for_editing:I = 0x7f0601a4

.field public static final medium:I = 0x7f06002b

.field public static final men_1:I = 0x7f060133

.field public static final men_2:I = 0x7f060134

.field public static final mirror:I = 0x7f060056

.field public static final mirror_horizontal:I = 0x7f0600be

.field public static final mirror_vertical:I = 0x7f0600bf

.field public static final mode:I = 0x7f06011b

.field public static final moody:I = 0x7f060162

.field public static final more:I = 0x7f060031

.field public static final more_effects:I = 0x7f060147

.field public static final more_fx:I = 0x7f06011e

.field public static final mosaic:I = 0x7f06018e

.field public static final motionphoto:I = 0x7f060122

.field public static final mouth:I = 0x7f060131

.field public static final move:I = 0x7f06001b

.field public static final multigrid:I = 0x7f06009f

.field public static final nashville:I = 0x7f06014f

.field public static final negative:I = 0x7f0600f4

.field public static final network_error:I = 0x7f060006

.field public static final new_file:I = 0x7f060010

.field public static final new_selection:I = 0x7f060022

.field public static final no:I = 0x7f06000b

.field public static final no_apps_available:I = 0x7f060205

.field public static final no_face:I = 0x7f0600df

.field public static final no_faces_detected:I = 0x7f0601a5

.field public static final no_picture:I = 0x7f060020

.field public static final no_recently_used_frames:I = 0x7f0601f6

.field public static final no_recently_used_labels:I = 0x7f0601f5

.field public static final no_recently_used_stamp:I = 0x7f0601f4

.field public static final no_recently_used_stickers:I = 0x7f0601f3

.field public static final noir_note:I = 0x7f060152

.field public static final normal_blur:I = 0x7f06005f

.field public static final nose:I = 0x7f060132

.field public static final nostalgia:I = 0x7f0600ce

.field public static final object:I = 0x7f0600a9

.field public static final object2:I = 0x7f06012a

.field public static final oil_painting:I = 0x7f06018d

.field public static final oil_pastel:I = 0x7f060163

.field public static final oilpaint:I = 0x7f06004f

.field public static final ok:I = 0x7f060007

.field public static final old_fashioned_picture:I = 0x7f060148

.field public static final oldphoto:I = 0x7f060045

.field public static final oldtime_photo:I = 0x7f06012c

.field public static final on_the_photo:I = 0x7f060019

.field public static final onebyone:I = 0x7f06011f

.field public static final open:I = 0x7f06000c

.field public static final open_in_new_window:I = 0x7f06001a

.field public static final original:I = 0x7f060059

.field public static final out_focus:I = 0x7f060112

.field public static final out_of_focus:I = 0x7f060159

.field public static final oval_blur:I = 0x7f06016a

.field public static final paper:I = 0x7f0600cf

.field public static final paste:I = 0x7f06017c

.field public static final pasted_from_clipboard:I = 0x7f060202

.field public static final pastel_sketch:I = 0x7f060150

.field public static final pastelsketch:I = 0x7f0600f5

.field public static final pd_selected:I = 0x7f060101

.field public static final pen:I = 0x7f060069

.field public static final pen_setting:I = 0x7f0600b8

.field public static final pen_settings_already_exists:I = 0x7f0600f7

.field public static final pen_settings_preset_delete_msg:I = 0x7f0600fa

.field public static final pen_settings_preset_empty:I = 0x7f0600f9

.field public static final pen_settings_preset_maximum_msg:I = 0x7f0600f8

.field public static final pencil:I = 0x7f0601ff

.field public static final people:I = 0x7f060200

.field public static final photo_studio:I = 0x7f060181

.field public static final pinhole:I = 0x7f0600ed

.field public static final pixelize:I = 0x7f060053

.field public static final popart:I = 0x7f060046

.field public static final portrait:I = 0x7f0600e1

.field public static final posterise:I = 0x7f060129

.field public static final posterize:I = 0x7f060050

.field public static final processing:I = 0x7f06008f

.field public static final ps_is_not_installed_on_the_device_download_ps:I = 0x7f0601dd

.field public static final question_download:I = 0x7f0601db

.field public static final question_download2:I = 0x7f0601dc

.field public static final radial_blur:I = 0x7f06005d

.field public static final rainbow:I = 0x7f0601fc

.field public static final rainy_window:I = 0x7f0600ee

.field public static final rectangle:I = 0x7f060090

.field public static final red:I = 0x7f060093

.field public static final red_eye_fix:I = 0x7f060102

.field public static final red_tint:I = 0x7f060189

.field public static final redeye:I = 0x7f06004a

.field public static final redo:I = 0x7f06001c

.field public static final redoall:I = 0x7f0600a1

.field public static final remove_red_eye:I = 0x7f060190

.field public static final reset:I = 0x7f060100

.field public static final resize:I = 0x7f060032

.field public static final retro:I = 0x7f0600d0

.field public static final revised_fisheye:I = 0x7f06015f

.field public static final rotate:I = 0x7f060033

.field public static final rotate_fliphorizon:I = 0x7f060106

.field public static final rotate_fliphorizon_1:I = 0x7f060108

.field public static final rotate_fliphorizon_2:I = 0x7f060109

.field public static final rotate_flipvertical:I = 0x7f060107

.field public static final rotate_flipvertical_1:I = 0x7f06010a

.field public static final rotate_flipvertical_2:I = 0x7f06010b

.field public static final rotate_left:I = 0x7f060099

.field public static final rotate_right:I = 0x7f06009a

.field public static final round:I = 0x7f0600b4

.field public static final round_selection:I = 0x7f0600b0

.field public static final rugged:I = 0x7f060164

.field public static final s90_anticlockwise:I = 0x7f060015

.field public static final s90_clockwise:I = 0x7f060016

.field public static final sandstone:I = 0x7f060153

.field public static final saturation:I = 0x7f060039

.field public static final save:I = 0x7f06000d

.field public static final save_HQ:I = 0x7f0601e3

.field public static final save_HQ_compressed:I = 0x7f0601e4

.field public static final save_as:I = 0x7f0600e4

.field public static final save_changes:I = 0x7f06019d

.field public static final save_good:I = 0x7f0601a1

.field public static final save_good_new:I = 0x7f0601cc

.field public static final save_max:I = 0x7f0601a0

.field public static final save_max_new:I = 0x7f0601cb

.field public static final save_medium:I = 0x7f0601e5

.field public static final save_medium_new:I = 0x7f0601f0

.field public static final save_your_changes_or_discard_them:I = 0x7f060192

.field public static final saved_in_ps:I = 0x7f060166

.field public static final search:I = 0x7f060004

.field public static final search_location:I = 0x7f060005

.field public static final seekbar_layout_collage_roundness:I = 0x7f06019b

.field public static final seekbar_layout_collage_spacing:I = 0x7f06019a

.field public static final select_area:I = 0x7f0600bd

.field public static final select_area_add:I = 0x7f0600c5

.field public static final select_area_new:I = 0x7f0600c7

.field public static final select_area_subtract:I = 0x7f0600c8

.field public static final select_image:I = 0x7f0600dd

.field public static final selection:I = 0x7f060021

.field public static final selection_area:I = 0x7f060114

.field public static final selection_area_lower:I = 0x7f060116

.field public static final selection_area_upper:I = 0x7f060115

.field public static final selection_mode:I = 0x7f06002d

.field public static final sepia:I = 0x7f060047

.field public static final setas:I = 0x7f0600a3

.field public static final share:I = 0x7f06000e

.field public static final share_name:I = 0x7f060001

.field public static final share_via:I = 0x7f06000f

.field public static final sharpen:I = 0x7f06004c

.field public static final shot_and_more:I = 0x7f0601b4

.field public static final shot_more:I = 0x7f0601c4

.field public static final show_previous:I = 0x7f0600f2

.field public static final shuffle:I = 0x7f060198

.field public static final signature_align:I = 0x7f060175

.field public static final signature_font:I = 0x7f060173

.field public static final signature_handwriting:I = 0x7f06016e

.field public static final signature_image:I = 0x7f06016f

.field public static final signature_mysignature:I = 0x7f060172

.field public static final signature_paragraph:I = 0x7f060174

.field public static final signature_reset:I = 0x7f060170

.field public static final signature_text:I = 0x7f060171

.field public static final sixteenbynine:I = 0x7f060121

.field public static final size:I = 0x7f060029

.field public static final sketch:I = 0x7f060051

.field public static final sketch_art:I = 0x7f060138

.field public static final small:I = 0x7f06002a

.field public static final smart_lighten:I = 0x7f0600e2

.field public static final smart_selection:I = 0x7f060028

.field public static final softglow:I = 0x7f06004d

.field public static final softgrow:I = 0x7f060127

.field public static final solid_border:I = 0x7f06006d

.field public static final some_studios_not_available_because_of_number_of_files_or_file_type:I = 0x7f0601d8

.field public static final speedline_effect:I = 0x7f060061

.field public static final sphericity:I = 0x7f060054

.field public static final splash:I = 0x7f0600e6

.field public static final split:I = 0x7f0600ad

.field public static final split2:I = 0x7f060124

.field public static final split3:I = 0x7f060125

.field public static final split4:I = 0x7f060126

.field public static final split_dual:I = 0x7f06016b

.field public static final spot_healing:I = 0x7f060097

.field public static final spotheal:I = 0x7f060057

.field public static final square:I = 0x7f0600b5

.field public static final square_selection:I = 0x7f0600af

.field public static final stamp:I = 0x7f0600aa

.field public static final stamp_dual:I = 0x7f06016c

.field public static final stardust:I = 0x7f0600ef

.field public static final sticker:I = 0x7f06005a

.field public static final string_copy:I = 0x7f06017a

.field public static final stucchevole:I = 0x7f060155

.field public static final studio:I = 0x7f060180

.field public static final studio_ellipse:I = 0x7f0601d4

.field public static final studio_lasso:I = 0x7f0601d3

.field public static final studio_rectangle:I = 0x7f0601d5

.field public static final studio_save_as:I = 0x7f0601cd

.field public static final style_a:I = 0x7f060141

.field public static final style_b:I = 0x7f060142

.field public static final style_c:I = 0x7f060143

.field public static final style_d:I = 0x7f060144

.field public static final style_e:I = 0x7f060145

.field public static final style_f:I = 0x7f060146

.field public static final subtract_selection:I = 0x7f060024

.field public static final sunburst:I = 0x7f0600f0

.field public static final sunshine:I = 0x7f0600d1

.field public static final swipe_left_or_right_to_adjust_the_intensity_of_the_effect:I = 0x7f0601ae

.field public static final swipe_left_or_right_to_adjust_the_ps_level:I = 0x7f0601af

.field public static final swipe_left_or_right_to_explore_other_styles:I = 0x7f0601bb

.field public static final tab_classic:I = 0x7f0601ed

.field public static final tab_date_time_and_location:I = 0x7f0601e9

.field public static final tab_life:I = 0x7f0601e8

.field public static final tab_objects:I = 0x7f0601ef

.field public static final tab_people:I = 0x7f0601e7

.field public static final tab_picture_information:I = 0x7f0601ea

.field public static final tab_recent:I = 0x7f0601e6

.field public static final tab_shapes:I = 0x7f0601ee

.field public static final tab_solid:I = 0x7f0601eb

.field public static final tab_transparent:I = 0x7f0601ec

.field public static final take_picture:I = 0x7f0600bc

.field public static final tap_and_drag_the_slider_to_adjust_the_border_margins:I = 0x7f0601b0

.field public static final tap_and_drag_to_select_an_area_of_the_image:I = 0x7f0601c3

.field public static final tap_and_hold_to_view_the_before_image:I = 0x7f0601c2

.field public static final tap_on_the_subjects_eyes_to_remove_red_eyes:I = 0x7f0601a3

.field public static final tap_the_shaded_area_to_mirror_this_part_of_the_image:I = 0x7f0601ca

.field public static final tap_to_auto_enhance_your_image:I = 0x7f0601b3

.field public static final tap_to_edit_the_picture:I = 0x7f0601c8

.field public static final tap_to_shuffle_your_pictures_and_use_a_randomly_selected_layout_and_background:I = 0x7f0601b6

.field public static final temperature:I = 0x7f060042

.field public static final text_settings:I = 0x7f060176

.field public static final the_maximum_number_of_images_pd_has_been_reached:I = 0x7f0601be

.field public static final the_selected_files_are_not_supported_on_this_studio_select_other_files_and_try_again:I = 0x7f0601b9

.field public static final thickness:I = 0x7f060203

.field public static final threebyfour:I = 0x7f060120

.field public static final tilt:I = 0x7f06004b

.field public static final tiltshift:I = 0x7f060063

.field public static final tint:I = 0x7f060165

.field public static final to_select_the_picture_area_tab_and_hold_the_picture:I = 0x7f060154

.field public static final tone:I = 0x7f060186

.field public static final tool:I = 0x7f060064

.field public static final toplens:I = 0x7f060040

.field public static final transform:I = 0x7f0600f1

.field public static final translate:I = 0x7f06017d

.field public static final transparency:I = 0x7f060204

.field public static final turquoise:I = 0x7f06015a

.field public static final twirl:I = 0x7f060055

.field public static final unable_to_add_image:I = 0x7f0601bc

.field public static final unable_to_add_maximum_number_of_items_n_reached:I = 0x7f060177

.field public static final unable_to_add_signature_to_picture_of_minimum_size:I = 0x7f06017e

.field public static final unable_to_load_images_check_the_images:I = 0x7f06017f

.field public static final unable_to_remove_image:I = 0x7f0601a9

.field public static final undo:I = 0x7f06001d

.field public static final undoall:I = 0x7f0600a6

.field public static final unsupported_files:I = 0x7f0601ab

.field public static final up_to_pd_characters_allowed:I = 0x7f0601d6

.field public static final up_to_pd_characters_allowed_in_title:I = 0x7f0601d1

.field public static final up_to_pd_faces:I = 0x7f0600e0

.field public static final video_clip_studio:I = 0x7f060183

.field public static final video_trimmer:I = 0x7f060184

.field public static final view_original:I = 0x7f0600e3

.field public static final vignette:I = 0x7f060151

.field public static final warping:I = 0x7f060067

.field public static final window:I = 0x7f06016d

.field public static final woman:I = 0x7f06013d

.field public static final woman_1:I = 0x7f060135

.field public static final woman_2:I = 0x7f060136

.field public static final yellow:I = 0x7f060094

.field public static final yellowglow:I = 0x7f0600d2

.field public static final yes:I = 0x7f06000a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
