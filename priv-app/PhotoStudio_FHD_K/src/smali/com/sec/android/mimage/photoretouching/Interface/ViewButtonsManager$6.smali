.class Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$6;
.super Ljava/lang/Object;
.source "ViewButtonsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->hide(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private final synthetic val$animation:Landroid/view/animation/Animation;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$6;->val$animation:Landroid/view/animation/Animation;

    .line 349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mBottomLayout:Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$6;->val$animation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Gui/CustomButtonLinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Landroid/widget/RelativeLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    const/high16 v1, 0x10000000

    if-eq v0, v1, :cond_1

    .line 356
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$6;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->mMiddleButtonLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->access$8(Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager$6;->val$animation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 357
    :cond_1
    return-void
.end method
