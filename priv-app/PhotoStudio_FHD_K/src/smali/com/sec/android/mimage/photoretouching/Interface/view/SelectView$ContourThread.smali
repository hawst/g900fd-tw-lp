.class Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;
.super Ljava/lang/Thread;
.source "SelectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContourThread"
.end annotation


# instance fields
.field private isRun:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V
    .locals 1

    .prologue
    .line 1083
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1085
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->isRun:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;)V
    .locals 0

    .prologue
    .line 1083
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 1088
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->isRun:Z

    .line 1089
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;)V

    .line 1090
    return-void
.end method

.method public run()V
    .locals 4

    .prologue
    .line 1093
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->isRun:Z

    if-nez v1, :cond_2

    .line 1118
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;)V

    .line 1119
    return-void

    .line 1096
    :cond_2
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1100
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mContour:Z
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1102
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mColor:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)I

    move-result v2

    const v3, 0xffffff

    xor-int/2addr v2, v3

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;I)V

    .line 1103
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->invalidateViewsWithThread()V

    .line 1106
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1108
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->mSelectEffect:Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/SelectView;)Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/SelectEffect;->getPreviewRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 1097
    :catch_0
    move-exception v0

    .line 1098
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
