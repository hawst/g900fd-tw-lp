.class public Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "AdjustmentView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$MultiTouchScaleGestureListener;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ResolverSetAsAdapter;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ResolverShareAdapter;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$SaveAsyncTask;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$TrayDeleteFunction;
    }
.end annotation


# instance fields
.field public final FINISH_PROGRESS:I

.field private final REDO:I

.field private final REDOALL:I

.field public final SHOW_PROGRESS:I

.field public final SHOW_SAVE_TOAST:I

.field public final TOP_BOTTOM_SHOW_HIDE_AVAILABLE_DIFF:I

.field private final UNDO:I

.field private final UNDOALL:I

.field private isReverseAnimRunning:Z

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mAlpha:I

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mClearPaint:Landroid/graphics/Paint;

.field private mColor:I

.field private mContext:Landroid/content/Context;

.field private mCurrentSaveSize:I

.field private mDashPathEffectPaint:Landroid/graphics/Paint;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mDrawAnimRunnable:Ljava/lang/Runnable;

.field private mFileName:Ljava/lang/String;

.field private mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

.field private mHandler:Landroid/os/Handler;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIsBackPressed:Z

.field private mIsDestroy:Z

.field private mIsMinimum:Z

.field private mIsMultiTouch:Z

.field private mLinePaint:Landroid/graphics/Paint;

.field private mLongpressButton:Z

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mOptionItemId:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

.field private mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mPushSaveButton:Z

.field private mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

.field public mShouldReverseAnimate:Z

.field private mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

.field private mTrayDeleteFunction:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$TrayDeleteFunction;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;

.field private msave:Z

.field private msetas:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .prologue
    const/4 v4, 0x2

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 98
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 2716
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    .line 2717
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 2718
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 2719
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 2720
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 2721
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 2722
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 2724
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPaint:Landroid/graphics/Paint;

    .line 2725
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mClearPaint:Landroid/graphics/Paint;

    .line 2726
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mLinePaint:Landroid/graphics/Paint;

    .line 2727
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 2728
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 2729
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 2731
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2733
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayDeleteFunction:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$TrayDeleteFunction;

    .line 2734
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 2736
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

    .line 2738
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mFileName:Ljava/lang/String;

    .line 2739
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsBackPressed:Z

    .line 2740
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsMinimum:Z

    .line 2741
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPushSaveButton:Z

    .line 2742
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsDestroy:Z

    .line 2743
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mOptionItemId:I

    .line 2745
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mColor:I

    .line 2746
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->msetas:Z

    .line 2747
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->msave:Z

    .line 2748
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->UNDO:I

    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->REDO:I

    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->UNDOALL:I

    const/4 v3, 0x3

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->REDOALL:I

    .line 2749
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 2750
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->SHOW_PROGRESS:I

    .line 2751
    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->FINISH_PROGRESS:I

    .line 2752
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->SHOW_SAVE_TOAST:I

    .line 2753
    const/16 v3, 0xa

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->TOP_BOTTOM_SHOW_HIDE_AVAILABLE_DIFF:I

    .line 2755
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 2757
    const v3, 0x7a1200

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mCurrentSaveSize:I

    .line 2759
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mLongpressButton:Z

    .line 2760
    iput-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 2761
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsMultiTouch:Z

    .line 2762
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mShouldReverseAnimate:Z

    .line 2763
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->isReverseAnimRunning:Z

    .line 2765
    const/16 v3, 0x32

    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mAlpha:I

    .line 2766
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$1;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    .line 99
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHandler:Landroid/os/Handler;

    .line 100
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    .line 101
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 102
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 103
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 104
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 105
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->setInterface(Ljava/lang/Object;)V

    .line 107
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v8}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPaint:Landroid/graphics/Paint;

    .line 108
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 110
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mClearPaint:Landroid/graphics/Paint;

    .line 111
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mClearPaint:Landroid/graphics/Paint;

    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 113
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mLinePaint:Landroid/graphics/Paint;

    .line 114
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 115
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 117
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 118
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 119
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 120
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    new-instance v4, Landroid/graphics/DashPathEffect;

    const/4 v5, 0x4

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    const/high16 v6, 0x40000000    # 2.0f

    invoke-direct {v4, v5, v6}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 122
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4, v2}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;-><init>(Landroid/content/Context;Z)V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 124
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 126
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 129
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$2;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 169
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    const/high16 v4, 0x11000000

    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$3;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    invoke-direct {v3, p1, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;-><init>(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/Interface/SeekbarManager$ProgressInterface;)V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 177
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->init(I)V

    .line 178
    new-instance v3, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    .line 179
    new-instance v5, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$MultiTouchScaleGestureListener;

    invoke-direct {v5, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$MultiTouchScaleGestureListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    invoke-direct {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;)V

    .line 178
    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 182
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_1

    .line 184
    const/4 v1, 0x0

    .line 185
    .local v1, "previewBuffer":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v3, :cond_0

    .line 186
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->init(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;)V

    .line 188
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 189
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 190
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 193
    :goto_0
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 196
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 199
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 200
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 202
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 194
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 204
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "previewBuffer":[I
    :cond_1
    return-void

    .line 192
    .restart local v1    # "previewBuffer":[I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    goto :goto_0

    .line 120
    nop

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;Z)V
    .locals 0

    .prologue
    .line 2761
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsMultiTouch:Z

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    .locals 1

    .prologue
    .line 2728
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 2755
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)I
    .locals 1

    .prologue
    .line 2743
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mOptionItemId:I

    return v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;I)Z
    .locals 1

    .prologue
    .line 2096
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->runOptionItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;)V
    .locals 0

    .prologue
    .line 2736
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)I
    .locals 1

    .prologue
    .line 2745
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mColor:I

    return v0
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;I)V
    .locals 0

    .prologue
    .line 2745
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mColor:I

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;I)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->setViewLayerType(I)V

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2731
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .locals 1

    .prologue
    .line 2721
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Z
    .locals 1

    .prologue
    .line 2740
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsMinimum:Z

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 2749
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 2720
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)I
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 2719
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$24(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V
    .locals 0

    .prologue
    .line 2676
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->refreshImageAndBottomButtons()V

    return-void
.end method

.method static synthetic access$25(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;Z)V
    .locals 0

    .prologue
    .line 2759
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mLongpressButton:Z

    return-void
.end method

.method static synthetic access$26(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Z
    .locals 1

    .prologue
    .line 2759
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mLongpressButton:Z

    return v0
.end method

.method static synthetic access$27(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;Landroid/content/Intent;Z)V
    .locals 0

    .prologue
    .line 1340
    invoke-direct {p0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    return-void
.end method

.method static synthetic access$28(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;I)V
    .locals 0

    .prologue
    .line 2743
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mOptionItemId:I

    return-void
.end method

.method static synthetic access$29(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;I)V
    .locals 0

    .prologue
    .line 2757
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mCurrentSaveSize:I

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 2716
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$30(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;I)V
    .locals 0

    .prologue
    .line 1271
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->undoNredo(I)V

    return-void
.end method

.method static synthetic access$31(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Z
    .locals 1

    .prologue
    .line 2741
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPushSaveButton:Z

    return v0
.end method

.method static synthetic access$32(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;Z)V
    .locals 0

    .prologue
    .line 2741
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPushSaveButton:Z

    return-void
.end method

.method static synthetic access$33(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->setMainBtnListener()V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 2717
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 2718
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 2722
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)I
    .locals 1

    .prologue
    .line 2757
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mCurrentSaveSize:I

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 2749
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 2755
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method private getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 454
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$11;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    return-object v0
.end method

.method private init2DepthActionBar()V
    .locals 6

    .prologue
    const/4 v3, -0x1

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 527
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 529
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->isValidBackKey()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 531
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 534
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$12;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 531
    invoke-virtual {v0, v5, v3, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 577
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 578
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    .line 580
    :goto_1
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 577
    invoke-virtual {v2, v5, v0, v5, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 625
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v3, 0x2

    .line 626
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    .line 628
    :goto_2
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$14;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 625
    invoke-virtual {v2, v3, v0, v5, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 674
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v2, 0xe

    .line 677
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$15;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 674
    invoke-virtual {v0, v2, v1, v1, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 704
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v2, 0x4

    .line 707
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$16;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 704
    invoke-virtual {v0, v2, v5, v5, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 736
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v2, 0x10

    .line 739
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$17;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$17;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 736
    invoke-virtual {v0, v2, v5, v5, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 775
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v2, 0x5

    .line 778
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$18;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$18;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 775
    invoke-virtual {v0, v2, v1, v5, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 816
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 818
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 819
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resetMenu()V

    .line 822
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_1

    .line 824
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 825
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isSaved()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 832
    :cond_1
    :goto_3
    return-void

    .line 574
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v1, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 578
    goto/16 :goto_1

    :cond_4
    move v0, v1

    .line 626
    goto :goto_2

    .line 828
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isSaved()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_3
.end method

.method private initSaveOptionDialog()V
    .locals 11

    .prologue
    const v10, 0x7f0601f0

    const v9, 0x7f0601cb

    const/16 v2, 0x9

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 852
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 906
    :goto_0
    return-void

    .line 857
    :cond_0
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$19;

    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$19;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 871
    .local v8, "saveListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 873
    const v3, 0x7f0601cd

    .line 875
    const/4 v5, 0x1

    .line 877
    const v7, 0x103012e

    .line 871
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZZLandroid/graphics/Point;I)V

    .line 879
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 881
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x810

    move v3, v9

    move v5, v9

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 885
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x820

    move v3, v10

    move v5, v10

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 887
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$20;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$20;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 894
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$21;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$21;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 905
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    goto :goto_0
.end method

.method private initSaveYesNoCancel()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 1798
    const/4 v7, 0x0

    .line 1799
    .local v7, "fileName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 1800
    .local v8, "path":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 1802
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1803
    if-nez v7, :cond_0

    .line 1805
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1809
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1810
    const/4 v2, 0x7

    .line 1811
    const v3, 0x7f06000d

    .line 1812
    const/4 v4, 0x1

    .line 1814
    const v6, 0x103012e

    .line 1809
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1816
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1817
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x500

    .line 1820
    const v4, 0x7f06007d

    move v2, v9

    move v3, v9

    move-object v6, v5

    .line 1817
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1824
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000a

    .line 1825
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$28;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$28;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 1824
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V

    .line 1856
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000b

    .line 1857
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$29;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$29;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 1856
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1872
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1873
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$30;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$30;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 1872
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1896
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1898
    return-void
.end method

.method private initSaveYesNoCancelForFinish()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 1903
    const/4 v7, 0x0

    .line 1904
    .local v7, "fileName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 1905
    .local v8, "path":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 1907
    invoke-static {v8}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1908
    if-nez v7, :cond_0

    .line 1910
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1914
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1915
    const/16 v2, 0x8

    .line 1916
    const v3, 0x7f06019d

    .line 1917
    const/4 v4, 0x1

    .line 1919
    const v6, 0x103012e

    .line 1914
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1921
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1922
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x500

    .line 1925
    const v4, 0x7f060192

    move v2, v9

    move v3, v9

    move-object v6, v5

    .line 1922
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1929
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    .line 1930
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$31;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$31;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 1929
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnShowListener;)V

    .line 1956
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601b2

    .line 1957
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$32;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$32;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 1956
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1965
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1966
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$33;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$33;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 1965
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1973
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1974
    return-void
.end method

.method private initSetAsDialog()V
    .locals 7

    .prologue
    .line 1547
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1549
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1550
    const v2, 0x7f090158

    .line 1551
    const v3, 0x7f0600a3

    .line 1552
    const/4 v4, 0x0

    .line 1553
    const/4 v5, 0x0

    .line 1554
    const v6, 0x103012e

    .line 1549
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1555
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1557
    :cond_0
    return-void
.end method

.method private initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isShareVia"    # Z

    .prologue
    .line 1343
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1344
    .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    const/4 v2, 0x0

    .line 1345
    .local v2, "shareChooser":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 1346
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    const v4, 0x7f06000f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 1350
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/content/pm/LabeledIntent;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/pm/LabeledIntent;

    .line 1351
    .local v0, "extraIntents":[Landroid/content/pm/LabeledIntent;
    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1352
    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1353
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1532
    return-void

    .line 1348
    .end local v0    # "extraIntents":[Landroid/content/pm/LabeledIntent;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    const v4, 0x7f0600a3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method private initShareViaDialog()V
    .locals 7

    .prologue
    .line 1535
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1537
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1538
    const v2, 0x7f090157

    .line 1539
    const v3, 0x7f06000f

    .line 1540
    const/4 v4, 0x0

    .line 1541
    const/4 v5, 0x0

    .line 1542
    const v6, 0x103012e

    .line 1537
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1543
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1545
    :cond_0
    return-void
.end method

.method private initUndoRedoAllDialog(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V
    .locals 8
    .param p1, "actionBarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .prologue
    const v6, 0x103012e

    const/16 v1, 0x1000

    const/16 v7, 0x500

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1604
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1605
    const/4 v2, 0x4

    .line 1606
    const v3, 0x7f0600a6

    .line 1604
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1611
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1612
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1613
    const v2, 0x7f06009c

    .line 1615
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$22;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$22;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 1612
    invoke-virtual {v0, v7, v2, v5, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1622
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060007

    .line 1623
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$23;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$23;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 1622
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1631
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v2, 0x7f060009

    .line 1632
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$24;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$24;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 1631
    invoke-virtual {v0, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1640
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1642
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1643
    const/4 v2, 0x5

    .line 1644
    const v3, 0x7f0600a1

    .line 1642
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1649
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 1650
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1651
    const v1, 0x7f0601ce

    .line 1653
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$25;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$25;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 1650
    invoke-virtual {v0, v7, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 1660
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 1661
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$26;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$26;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 1660
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1669
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 1670
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$27;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$27;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 1669
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 1678
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1679
    return-void
.end method

.method private refreshImageAndBottomButtons()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/high16 v10, 0x42c80000    # 100.0f

    const/4 v2, 0x0

    .line 2678
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_0

    .line 2680
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2681
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 2682
    .local v1, "output":[I
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 2683
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2686
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 2689
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 2690
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 2692
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 2684
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 2694
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "output":[I
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_3

    .line 2696
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v3, :cond_2

    .line 2698
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-ltz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v4

    div-float/2addr v3, v4

    cmpg-float v3, v3, v10

    if-gez v3, :cond_4

    .line 2699
    :cond_1
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsMinimum:Z

    .line 2703
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$37;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$37;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2712
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->invalidateViews()V

    .line 2713
    return-void

    .line 2701
    :cond_4
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsMinimum:Z

    goto :goto_0
.end method

.method private runOptionItem(I)Z
    .locals 9
    .param p1, "optionItemId"    # I

    .prologue
    const v8, 0x7f090156

    const v7, 0x7a1200

    const/4 v6, 0x0

    const/4 v5, 0x7

    const/16 v4, 0x9

    .line 2098
    const/4 v1, 0x0

    .line 2099
    .local v1, "ret":Z
    const/4 v2, 0x0

    .line 2100
    .local v2, "uri":Landroid/net/Uri;
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mOptionItemId:I

    .line 2101
    sparse-switch p1, :sswitch_data_0

    .line 2187
    :cond_0
    :goto_0
    return v1

    .line 2104
    :sswitch_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2107
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2113
    :goto_1
    const/4 v1, 0x1

    .line 2114
    goto :goto_0

    .line 2111
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->openGallery(Landroid/content/Context;)V

    goto :goto_1

    .line 2116
    :sswitch_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v4, 0x2e000000

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 2117
    const/4 v1, 0x1

    .line 2118
    goto :goto_0

    .line 2120
    :sswitch_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2122
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2129
    :goto_2
    const/4 v1, 0x1

    .line 2130
    goto :goto_0

    .line 2127
    :cond_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->openCamera(Landroid/content/Context;)V

    goto :goto_2

    .line 2132
    :sswitch_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2134
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mCurrentSaveSize:I

    .line 2135
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2136
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2147
    :cond_3
    :goto_3
    const/4 v1, 0x1

    .line 2148
    goto :goto_0

    .line 2138
    :cond_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090157

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2139
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 2140
    if-nez v2, :cond_5

    .line 2142
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 2144
    :cond_5
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    goto :goto_3

    .line 2151
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2153
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mCurrentSaveSize:I

    .line 2154
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_6

    .line 2155
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2168
    :cond_6
    :goto_4
    const/4 v1, 0x1

    .line 2169
    goto/16 :goto_0

    .line 2157
    :cond_7
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090158

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_6

    .line 2160
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 2161
    if-nez v2, :cond_8

    .line 2163
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 2165
    :cond_8
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v3, v6}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    .line 2166
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090158

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    goto :goto_4

    .line 2171
    :sswitch_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2173
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2175
    .local v0, "fileName":Ljava/lang/String;
    if-nez v0, :cond_9

    .line 2176
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    .line 2180
    :goto_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v0, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTextToDialog(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 2178
    :cond_9
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 2183
    .end local v0    # "fileName":Ljava/lang/String;
    :sswitch_6
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 2184
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v4, 0x17000000

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 2101
    :sswitch_data_0
    .sparse-switch
        0x7f090007 -> :sswitch_1
        0x7f090155 -> :sswitch_0
        0x7f090156 -> :sswitch_5
        0x7f090157 -> :sswitch_3
        0x7f090158 -> :sswitch_4
        0x7f09015a -> :sswitch_2
        0x7f09015b -> :sswitch_6
    .end sparse-switch
.end method

.method private setMainBtnListener()V
    .locals 8

    .prologue
    const v7, 0x10001004

    const v6, 0x1000100b

    const v5, 0x1000100a

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 245
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initBottomButtonWithIcon(I)V

    .line 246
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsMinimum:Z

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->hideSubBottomButton()Z

    .line 249
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    invoke-virtual {v0, v7, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 274
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001005

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001006

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 431
    :goto_0
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getPreviousMode()I

    move-result v0

    const/high16 v1, -0x10000

    and-int/2addr v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 449
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setSelectedButton(IZ)V

    .line 450
    return-void

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$6;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$7;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    invoke-virtual {v0, v7, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001005

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$8;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 366
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001006

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$9;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEnableDecoration()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 399
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$10;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_0

    .line 427
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(IZLcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    goto :goto_0

    .line 434
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto :goto_1

    .line 437
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v7}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto :goto_1

    .line 440
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001005

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto :goto_1

    .line 443
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const v1, 0x10001006

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto :goto_1

    .line 446
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->movoToPosition(I)V

    goto :goto_1

    .line 431
    :sswitch_data_0
    .sparse-switch
        0x11000000 -> :sswitch_0
        0x15000000 -> :sswitch_1
        0x16000000 -> :sswitch_2
        0x18000000 -> :sswitch_3
        0x31000000 -> :sswitch_4
    .end sparse-switch
.end method

.method private undoNredo(I)V
    .locals 6
    .param p1, "who"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1273
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_4

    .line 1275
    packed-switch p1, :pswitch_data_0

    .line 1291
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->refreshImageAndBottomButtons()V

    .line 1293
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isUndo()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1294
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableUndo()V

    .line 1306
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->isRedo()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1308
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableRedo()V

    .line 1315
    :goto_2
    if-ne p1, v4, :cond_7

    .line 1317
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableSave()V

    .line 1318
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setIsEdited(Z)V

    .line 1325
    :cond_1
    :goto_3
    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    if-eqz p1, :cond_2

    if-ne p1, v4, :cond_3

    .line 1326
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getPreviewCurrentIndex()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getCurrentSavedIndex()I

    move-result v1

    if-ne v0, v1, :cond_8

    .line 1327
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 1331
    :cond_3
    :goto_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->resetZoom()V

    .line 1335
    :cond_4
    return-void

    .line 1278
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->undo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    goto :goto_0

    .line 1281
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->redo(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Z

    goto :goto_0

    .line 1284
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->undoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    goto :goto_0

    .line 1287
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->redoAll(Lcom/sec/android/mimage/photoretouching/Core/ImageData;II)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager$HistoryInfo;

    goto/16 :goto_0

    .line 1297
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableUndo()V

    .line 1298
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0, v5, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    .line 1299
    if-nez p1, :cond_0

    .line 1301
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableSave()V

    .line 1302
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setIsEdited(Z)V

    goto/16 :goto_1

    .line 1312
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableRedo()V

    goto/16 :goto_2

    .line 1320
    :cond_7
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 1322
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableSave()V

    .line 1323
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setIsEdited(Z)V

    goto/16 :goto_3

    .line 1329
    :cond_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initSaveBtn(ZZ)V

    goto :goto_4

    .line 1275
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v8, 0xff

    const/4 v7, 0x0

    .line 1033
    const/4 v3, 0x1

    .line 1034
    .local v3, "ret":Z
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v5, :cond_0

    move v5, v3

    .line 1091
    :goto_0
    return v5

    .line 1036
    :cond_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 1037
    .local v1, "m":Landroid/graphics/Matrix;
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1038
    .local v0, "inversM":Landroid/graphics/Matrix;
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1039
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 1041
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    invoke-virtual {v5, p2}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1043
    const/4 v3, 0x1

    .line 1050
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    .line 1051
    .local v2, "matrix":Landroid/graphics/Matrix;
    const/16 v5, 0x9

    new-array v4, v5, [F

    .line 1052
    .local v4, "value":[F
    invoke-virtual {v2, v4}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1053
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v5, :cond_2

    .line 1054
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    aget v6, v4, v7

    invoke-virtual {v5, p2, v6}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->onTouchEventForSeekbar(Landroid/view/MotionEvent;F)V

    .line 1057
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :goto_2
    move v5, v3

    .line 1091
    goto :goto_0

    .line 1045
    .end local v2    # "matrix":Landroid/graphics/Matrix;
    .end local v4    # "value":[F
    :cond_3
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v5, p2}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1047
    const/4 v3, 0x1

    goto :goto_1

    .line 1060
    .restart local v2    # "matrix":Landroid/graphics/Matrix;
    .restart local v4    # "value":[F
    :sswitch_0
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->isReverseAnimRunning:Z

    if-eqz v5, :cond_4

    .line 1062
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->isReverseAnimRunning:Z

    .line 1063
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mShouldReverseAnimate:Z

    goto :goto_2

    .line 1068
    :cond_4
    const/16 v5, 0x32

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mAlpha:I

    goto :goto_2

    .line 1077
    :sswitch_1
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->startseek()V

    .line 1078
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->isReverseAnimRunning:Z

    .line 1079
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mAlpha:I

    if-le v5, v8, :cond_5

    .line 1080
    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mAlpha:I

    .line 1082
    :cond_5
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 1057
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x3 -> :sswitch_1
        0x105 -> :sswitch_0
    .end sparse-switch
.end method

.method public backPressed()V
    .locals 2

    .prologue
    .line 514
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 515
    return-void
.end method

.method public changeImage(I)V
    .locals 5
    .param p1, "trayButtonIdx"    # I

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x42c80000    # 100.0f

    .line 1987
    const-string v1, "changeImage"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 1988
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v1, :cond_0

    .line 1990
    const-string v1, "mTrayManager != null"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 1991
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1992
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1994
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v1, :cond_2

    .line 1996
    const-string v1, "mImageData null"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 2043
    :cond_1
    :goto_0
    return-void

    .line 2002
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v2

    div-float/2addr v1, v2

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v2

    div-float/2addr v1, v2

    cmpg-float v1, v1, v3

    if-gez v1, :cond_7

    .line 2003
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsMinimum:Z

    .line 2006
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 2007
    .local v0, "viewBuffer":[I
    if-eqz v0, :cond_5

    .line 2009
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v1, :cond_4

    .line 2011
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->init(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;)V

    .line 2012
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->configurationChange()V

    .line 2014
    :cond_4
    const-string v1, "QuramUtil.recycleBitmap(mViewBitmap);"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 2015
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 2016
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_5

    .line 2018
    const-string v1, "mViewBitmap = Bitmap.createBitmap"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 2019
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 2020
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->setImageDataToImageEditView(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;)V

    .line 2023
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->initEffect()V

    .line 2024
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->invalidateViewsWithThread()V

    .line 2025
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->initDialog()V

    .line 2026
    const-string v1, "invalidateViewsWithThread"

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 2027
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_6

    .line 2029
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$34;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$34;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2037
    :cond_6
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

    if-nez v1, :cond_1

    .line 2039
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

    invoke-direct {v1, p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

    .line 2040
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;->start()V

    goto/16 :goto_0

    .line 2005
    .end local v0    # "viewBuffer":[I
    :cond_7
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsMinimum:Z

    goto :goto_1
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    .line 231
    .local v0, "ret":I
    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    .line 236
    .local v0, "ret":I
    return v0
.end method

.method public getStatusHeight()I
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x0

    .line 241
    .local v0, "ret":I
    return v0
.end method

.method public initActionbar()V
    .locals 1

    .prologue
    .line 518
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->init2DepthActionBar()V

    .line 519
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 521
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeOtherButtonLayout()V

    .line 524
    :cond_0
    return-void
.end method

.method public initButtons()V
    .locals 5

    .prologue
    const/high16 v4, 0x42c80000    # 100.0f

    .line 489
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v2, :cond_1

    .line 491
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewRatio()V

    .line 492
    const/4 v0, 0x0

    .line 493
    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v2, :cond_1

    .line 494
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->initSubViewButtons(Lcom/sec/android/mimage/photoretouching/Core/EffectEffect;)V

    .line 495
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->showSubBottomButton(I)V

    .line 496
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v3

    div-float/2addr v2, v3

    cmpg-float v2, v2, v4

    if-ltz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalToPreviewRatio()F

    move-result v3

    div-float/2addr v2, v3

    cmpg-float v2, v2, v4

    if-gez v2, :cond_2

    .line 497
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsMinimum:Z

    .line 500
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->setMainBtnListener()V

    .line 502
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_3

    .line 509
    .end local v0    # "i":I
    :cond_1
    return-void

    .line 499
    .restart local v0    # "i":I
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsMinimum:Z

    goto :goto_0

    .line 504
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->getMainBtnList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 505
    .local v1, "v":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getDefaultTouchInterface(I)Lcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setBtnListener(ILcom/sec/android/mimage/photoretouching/Gui/listener/DefaultButtonsListener$DefaultTouchInterface;)V

    .line 502
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 838
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 839
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->initShareViaDialog()V

    .line 840
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->initSetAsDialog()V

    .line 843
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->initSaveOptionDialog()V

    .line 844
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->initSaveYesNoCancel()V

    .line 845
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->initSaveYesNoCancelForFinish()V

    .line 846
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->initUndoRedoAllDialog(Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;)V

    .line 848
    :cond_0
    return-void
.end method

.method public initEffect()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

    if-nez v0, :cond_0

    .line 210
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

    .line 211
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;->start()V

    .line 213
    :cond_0
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 2644
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 1980
    return-void
.end method

.method public initTrayLayout()V
    .locals 0

    .prologue
    .line 1984
    return-void
.end method

.method public initView()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->setImageEditViewPinchZoomCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;)V

    .line 218
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->startImageEditViewAnimation(Landroid/view/animation/Animation;)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$4;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 227
    :cond_0
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 5
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    const/4 v4, 0x0

    .line 1203
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1204
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1206
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1225
    :cond_0
    :goto_0
    return-void

    .line 1210
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_0

    .line 1212
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 1213
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 1214
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1215
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1216
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1218
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 1219
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1221
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v0, :cond_0

    .line 1222
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->init(Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1098
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsDestroy:Z

    .line 1099
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 1100
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    .line 1102
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 1103
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1105
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_2

    .line 1106
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPaint:Landroid/graphics/Paint;

    .line 1108
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mClearPaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_3

    .line 1109
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mClearPaint:Landroid/graphics/Paint;

    .line 1111
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mLinePaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_4

    .line 1112
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mLinePaint:Landroid/graphics/Paint;

    .line 1114
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_5

    .line 1115
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    .line 1117
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v0, :cond_6

    .line 1118
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->destroy()V

    .line 1119
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 1121
    :cond_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_7

    .line 1122
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 1123
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1125
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_8

    .line 1126
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1128
    :cond_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_9

    .line 1129
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->show()V

    .line 1130
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1132
    :cond_9
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_a

    .line 1133
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->free()V

    .line 1134
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1137
    :cond_a
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_b

    .line 1138
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1140
    :cond_b
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    if-eqz v0, :cond_c

    .line 1141
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1143
    :cond_c
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    if-eqz v0, :cond_d

    .line 1144
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 1146
    :cond_d
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

    if-eqz v0, :cond_e

    .line 1147
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;->destroy()V

    .line 1148
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mThread:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$ContourThread;

    .line 1150
    :cond_e
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mFileName:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 1151
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mFileName:Ljava/lang/String;

    .line 1153
    :cond_f
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayDeleteFunction:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$TrayDeleteFunction;

    if-eqz v0, :cond_10

    .line 1154
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayDeleteFunction:Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$TrayDeleteFunction;

    .line 1156
    :cond_10
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    if-eqz v0, :cond_11

    .line 1157
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 1159
    :cond_11
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_12

    .line 1160
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1162
    :cond_12
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_13

    .line 1163
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    .line 1164
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1168
    :cond_13
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-eqz v0, :cond_14

    .line 1170
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->Destroy()V

    .line 1171
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 1199
    :cond_14
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/16 v10, 0xff

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 912
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 943
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 946
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 947
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 948
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPaint:Landroid/graphics/Paint;

    .line 945
    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 965
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsMultiTouch:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    if-nez v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mShouldReverseAnimate:Z

    if-eqz v0, :cond_3

    .line 969
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->isReverseAnimRunning:Z

    if-eqz v0, :cond_6

    .line 973
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mAlpha:I

    if-lez v0, :cond_5

    .line 976
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 978
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mAlpha:I

    .line 976
    invoke-virtual {v0, p1, v9, v8, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    .line 979
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mAlpha:I

    add-int/lit8 v0, v0, -0xf

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mAlpha:I

    .line 981
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDrawAnimRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1006
    :cond_3
    :goto_1
    return-void

    .line 954
    :cond_4
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 955
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 956
    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mColor:I

    .line 957
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPaint:Landroid/graphics/Paint;

    .line 958
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mClearPaint:Landroid/graphics/Paint;

    .line 959
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mLinePaint:Landroid/graphics/Paint;

    .line 960
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDashPathEffectPaint:Landroid/graphics/Paint;

    move-object v0, p1

    .line 953
    invoke-static/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawCanvasWithPath(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;ILandroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 985
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->resetPreProgressValue()V

    .line 986
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->isReverseAnimRunning:Z

    .line 987
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mShouldReverseAnimate:Z

    goto :goto_1

    .line 990
    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mShouldReverseAnimate:Z

    .line 991
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mAlpha:I

    if-gt v0, v10, :cond_7

    .line 993
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    .line 995
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mAlpha:I

    .line 993
    invoke-virtual {v0, p1, v9, v8, v1}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    .line 996
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mAlpha:I

    add-int/lit8 v0, v0, 0xf

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mAlpha:I

    goto :goto_1

    .line 1000
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mSeekbarManager:Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;

    invoke-virtual {v0, p1, v9, v8, v10}, Lcom/sec/android/mimage/photoretouching/Interface/SeekbarManager;->drawProgress(Landroid/graphics/Canvas;IZI)V

    goto :goto_1
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 1228
    const/16 v1, 0x17

    if-eq p1, v1, :cond_0

    const/16 v1, 0x42

    if-ne p1, v1, :cond_3

    .line 1230
    :cond_0
    const/4 v0, 0x0

    .line 1231
    .local v0, "isFocused":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_1

    .line 1232
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    move-result v0

    .line 1235
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_2

    .line 1236
    if-nez v0, :cond_2

    .line 1237
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 1263
    .end local v0    # "isFocused":Z
    :cond_2
    :goto_0
    return v2

    .line 1241
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_4

    .line 1243
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->backPressed()V

    goto :goto_0

    .line 1246
    :cond_4
    const/16 v1, 0x52

    if-ne p1, v1, :cond_2

    .line 1247
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    goto :goto_0
.end method

.method public onLayout()V
    .locals 4

    .prologue
    .line 2571
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 2573
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 2574
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 2576
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 2577
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 2578
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewHeight()I

    move-result v0

    if-eqz v0, :cond_2

    .line 2580
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_2

    .line 2582
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsDestroy:Z

    if-eqz v0, :cond_3

    .line 2601
    :cond_2
    :goto_0
    return-void

    .line 2584
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewHeight()I

    move-result v2

    .line 2585
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$35;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$35;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 2584
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(IILcom/sec/android/mimage/photoretouching/Core/ImageData$setViewSizeMatrixCallback;)V

    .line 2595
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v0, :cond_2

    .line 2597
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->configurationChange()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 1269
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->runOptionItem(I)Z

    .line 1270
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2046
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    if-eqz v0, :cond_0

    .line 2048
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->resume()V

    .line 2050
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_1

    .line 2052
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->resume()V

    .line 2059
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->msave:Z

    if-eqz v0, :cond_2

    .line 2060
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2061
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mCurrentSaveSize:I

    const v1, 0x7a1200

    if-ne v0, v1, :cond_4

    .line 2062
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cb

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setSelectedFalseAnotherButtons(I)V

    .line 2065
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->msave:Z

    .line 2068
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2069
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 2070
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 2083
    :cond_3
    return-void

    .line 2064
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601f0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setSelectedFalseAnotherButtons(I)V

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2086
    invoke-super {p0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;->pause()V

    .line 2087
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f090158

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2088
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->msetas:Z

    .line 2089
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 2091
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2092
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->msave:Z

    .line 2093
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->cancelDialog()Z

    .line 2095
    :cond_1
    return-void
.end method

.method public refreshView()V
    .locals 4

    .prologue
    .line 2648
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewHeight()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2650
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_1

    .line 2652
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsDestroy:Z

    if-eqz v0, :cond_0

    .line 2673
    :goto_0
    return-void

    .line 2654
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->getImageEditViewHeight()I

    move-result v2

    .line 2655
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$36;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView$36;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;)V

    .line 2654
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(IILcom/sec/android/mimage/photoretouching/Core/ImageData$setViewSizeMatrixCallback;)V

    .line 2665
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v0, :cond_1

    .line 2667
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->configurationChange()V

    .line 2672
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->refreshImageAndBottomButtons()V

    goto :goto_0
.end method

.method public setConfigurationChanged()V
    .locals 1

    .prologue
    .line 1010
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 1011
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 1012
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 1013
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v0, :cond_0

    .line 1015
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 1016
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->requestLayout()V

    .line 1019
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->setImageEditViewPinchZoomCallback(Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;)V

    .line 1021
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mIsMinimum:Z

    if-eqz v0, :cond_1

    .line 1022
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->hideSubBottomButton()Z

    .line 1027
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/AdjustmentView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPopupLayout()V

    .line 1028
    return-void
.end method
