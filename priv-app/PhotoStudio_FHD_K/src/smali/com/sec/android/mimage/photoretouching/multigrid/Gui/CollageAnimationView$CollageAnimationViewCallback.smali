.class public interface abstract Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView$CollageAnimationViewCallback;
.super Ljava/lang/Object;
.source "CollageAnimationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/multigrid/Gui/CollageAnimationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CollageAnimationViewCallback"
.end annotation


# virtual methods
.method public abstract calculateRectSize(Landroid/graphics/Rect;Landroid/graphics/RectF;)Landroid/graphics/Rect;
.end method

.method public abstract drawBackground(Landroid/graphics/Canvas;)V
.end method

.method public abstract endAnimation(Z)V
.end method

.method public abstract startAnimation()V
.end method
