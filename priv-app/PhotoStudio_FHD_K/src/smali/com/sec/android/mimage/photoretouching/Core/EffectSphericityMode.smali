.class public Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;
.super Lcom/sec/android/mimage/photoretouching/Core/EffectInfoInterface;
.source "EffectSphericityMode.java"


# instance fields
.field private final HIGH_QUALITY:I

.field private final LOW_QUALITY:I

.field private mContext:Landroid/content/Context;

.field private mEffectType:I

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mMaxRadius:I

.field private mOrgCenterX:F

.field private mOrgCenterY:F

.field private mOrgRadius:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPathEffect:Landroid/graphics/DashPathEffect;

.field private mStartInitializing:Z

.field private mStep:I

.field private mTempInput:[I

.field private mTempOutput:[I

.field private mutexOn:Z

.field private myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

.field private myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onCallback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    .param p3, "onActionbarCallback"    # Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;
    .param p4, "imageData"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 30
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfoInterface;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 524
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->LOW_QUALITY:I

    .line 525
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->HIGH_QUALITY:I

    .line 527
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mEffectType:I

    .line 529
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mutexOn:Z

    .line 530
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mStartInitializing:Z

    .line 532
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .line 533
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .line 535
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 536
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mContext:Landroid/content/Context;

    .line 541
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgRadius:I

    .line 542
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempInput:[I

    .line 543
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempOutput:[I

    .line 544
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mStep:I

    .line 545
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mPaint:Landroid/graphics/Paint;

    .line 546
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mPathEffect:Landroid/graphics/DashPathEffect;

    .line 32
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    .line 33
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    .line 34
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 35
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mContext:Landroid/content/Context;

    .line 36
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;)Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    return-object v0
.end method

.method private applySphericityTwirlPreview(I)V
    .locals 11
    .param p1, "applyMode"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 413
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v2, :cond_3

    .line 415
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterX:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v6, v2

    .line 416
    .local v6, "centerX":I
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterY:F

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v7, v2

    .line 418
    .local v7, "centerY":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    .line 419
    .local v1, "previewWidth":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v10

    .line 421
    .local v10, "previewHeight":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v8

    .line 423
    .local v8, "magnifyMinRatio":F
    iget v9, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgRadius:I

    .line 425
    .local v9, "orgRadius":I
    if-ge v6, v0, :cond_0

    move v6, v0

    .line 426
    :cond_0
    add-int/lit8 v2, v1, -0x1

    if-ge v6, v2, :cond_4

    .line 428
    :goto_0
    if-ge v7, v0, :cond_1

    move v7, v0

    .line 429
    :cond_1
    add-int/lit8 v0, v10, -0x1

    if-ge v7, v0, :cond_5

    .line 465
    :goto_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_2

    .line 467
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempOutput:[I

    .line 469
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v4, v4, v1, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 470
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v3

    .line 471
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v4

    .line 472
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v5

    .line 467
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->copyBuffer([IILandroid/graphics/Rect;[IILandroid/graphics/Rect;)V

    .line 474
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->myCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnCallback;->afterApplyPreview()V

    .line 476
    .end local v1    # "previewWidth":I
    .end local v6    # "centerX":I
    .end local v7    # "centerY":I
    .end local v8    # "magnifyMinRatio":F
    .end local v9    # "orgRadius":I
    .end local v10    # "previewHeight":I
    :cond_3
    return-void

    .line 426
    .restart local v1    # "previewWidth":I
    .restart local v6    # "centerX":I
    .restart local v7    # "centerY":I
    .restart local v8    # "magnifyMinRatio":F
    .restart local v9    # "orgRadius":I
    .restart local v10    # "previewHeight":I
    :cond_4
    add-int/lit8 v6, v1, -0x1

    goto :goto_0

    .line 429
    :cond_5
    add-int/lit8 v7, v10, -0x1

    goto :goto_1
.end method

.method private calculateMinBrushSize(F)F
    .locals 4
    .param p1, "size"    # F

    .prologue
    .line 62
    const/high16 v0, 0x41a00000    # 20.0f

    .line 63
    .local v0, "minBrushSize":F
    const/4 v1, 0x0

    .line 65
    .local v1, "tempMinBrushSize":F
    mul-float v2, v0, p1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->getMaxBrushSize()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 66
    sub-float/2addr v0, v1

    .line 67
    return v0
.end method

.method private initTwirlSphericity(Z)V
    .locals 12
    .param p1, "calledForUndo"    # Z

    .prologue
    const/high16 v2, 0x40400000    # 3.0f

    const/4 v11, 0x0

    .line 479
    const/16 v8, 0x1e

    .line 480
    .local v8, "tempStep":I
    const/4 v6, 0x0

    .line 481
    .local v6, "maxRadius":I
    const/4 v7, 0x0

    .line 482
    .local v7, "tempRadius":I
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 484
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    div-int/lit8 v6, v0, 0x4

    .line 490
    :goto_0
    div-int/lit8 v7, v6, 0x2

    .line 491
    iget v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mStep:I

    .line 493
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempInput:[I

    .line 494
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempInput:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempOutput:[I

    .line 496
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mPaint:Landroid/graphics/Paint;

    .line 497
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mPathEffect:Landroid/graphics/DashPathEffect;

    .line 498
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 500
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mPathEffect:Landroid/graphics/DashPathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 501
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 502
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 505
    iput v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mMaxRadius:I

    .line 506
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterX:F

    .line 507
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterY:F

    .line 508
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgRadius:I

    .line 510
    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mStep:I

    .line 512
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 513
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    .line 514
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v2

    .line 515
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempInput:[I

    .line 516
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 517
    new-instance v5, Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v9}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v10}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v10

    invoke-direct {v5, v11, v11, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 512
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->copyBuffer([IILandroid/graphics/Rect;[IILandroid/graphics/Rect;)V

    .line 519
    if-nez p1, :cond_0

    .line 520
    invoke-direct {p0, v11}, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->applySphericityTwirlPreview(I)V

    .line 522
    :cond_0
    return-void

    .line 488
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    div-int/lit8 v6, v0, 0x4

    goto/16 :goto_0

    .line 497
    nop

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method


# virtual methods
.method public addEvent(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 393
    add-int/lit8 p1, p1, -0xa

    .line 394
    mul-int/lit8 v0, p1, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mStep:I

    .line 395
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->applySphericityTwirlPreview(I)V

    .line 396
    return-void
.end method

.method public applyOriginal()[I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 71
    const/4 v2, 0x0

    .line 72
    .local v2, "output":[I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v4, :cond_1

    .line 74
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    .line 75
    .local v1, "input":[I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskBuffer()[B

    .line 76
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    .line 77
    .local v3, "w":I
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 78
    .local v0, "h":I
    mul-int v4, v3, v0

    new-array v2, v4, [I

    .line 79
    array-length v4, v2

    invoke-static {v1, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 111
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v4, :cond_0

    .line 112
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4, v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([I)V

    .line 113
    :cond_0
    const-string v4, "applyOriginal end"

    invoke-static {v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 116
    .end local v0    # "h":I
    .end local v1    # "input":[I
    .end local v3    # "w":I
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declared-synchronized applyPreview()I
    .locals 2

    .prologue
    .line 357
    monitor-enter p0

    const/4 v0, -0x1

    .line 358
    .local v0, "ret":I
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->applySphericityTwirlPreview(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    monitor-exit p0

    return v0

    .line 357
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public applyTwirlPreview(Landroid/view/MotionEvent;[III)V
    .locals 15
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "viewBuffer"    # [I
    .param p3, "viewWidth"    # I
    .param p4, "viewHeight"    # I

    .prologue
    .line 120
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    .line 121
    .local v10, "x":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v11

    .line 122
    .local v11, "y":F
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v0

    float-to-int v1, v10

    float-to-int v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 189
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempOutput:[I

    .line 190
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    .line 191
    new-instance v2, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 192
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v3

    .line 193
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v4

    .line 194
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v5

    .line 189
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->copyBuffer([IILandroid/graphics/Rect;[IILandroid/graphics/Rect;)V

    .line 196
    :cond_1
    return-void

    .line 128
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v10, v0

    .line 129
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v11, v0

    .line 131
    const/high16 v0, 0x41200000    # 10.0f

    cmpg-float v0, v10, v0

    if-gtz v0, :cond_2

    .line 132
    const/high16 v10, 0x41200000    # 10.0f

    .line 133
    :cond_2
    const/high16 v0, 0x41200000    # 10.0f

    cmpg-float v0, v11, v0

    if-gtz v0, :cond_3

    .line 134
    const/high16 v11, 0x41200000    # 10.0f

    .line 135
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    add-int/lit8 v0, v0, -0xa

    int-to-float v0, v0

    cmpl-float v0, v10, v0

    if-ltz v0, :cond_4

    .line 136
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    add-int/lit8 v0, v0, -0xa

    int-to-float v10, v0

    .line 137
    :cond_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0xa

    int-to-float v0, v0

    cmpl-float v0, v11, v0

    if-ltz v0, :cond_5

    .line 138
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0xa

    int-to-float v11, v0

    .line 140
    :cond_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v0

    div-float v0, v10, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterX:F

    .line 141
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v0

    div-float v0, v11, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterY:F

    .line 143
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempInput:[I

    .line 144
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempOutput:[I

    .line 145
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 146
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 147
    float-to-int v4, v10

    .line 148
    float-to-int v5, v11

    .line 149
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgRadius:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgRadius:I

    int-to-float v7, v7

    invoke-direct {p0, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->calculateMinBrushSize(F)F

    move-result v7

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v7

    mul-float/2addr v6, v7

    float-to-int v6, v6

    .line 150
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mStep:I

    .line 151
    new-instance v8, Landroid/graphics/Rect;

    const/4 v9, 0x0

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v13

    iget-object v14, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v14}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v14

    invoke-direct {v8, v9, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 152
    const/4 v9, 0x1

    .line 143
    invoke-static/range {v0 .. v9}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runTwirl([I[IIIIIIILandroid/graphics/Rect;I)I

    goto/16 :goto_0

    .line 157
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v10, v0

    .line 158
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v11, v0

    .line 160
    const/high16 v0, 0x41200000    # 10.0f

    cmpg-float v0, v10, v0

    if-gtz v0, :cond_6

    .line 161
    const/high16 v10, 0x41200000    # 10.0f

    .line 162
    :cond_6
    const/high16 v0, 0x41200000    # 10.0f

    cmpg-float v0, v11, v0

    if-gtz v0, :cond_7

    .line 163
    const/high16 v11, 0x41200000    # 10.0f

    .line 164
    :cond_7
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    add-int/lit8 v0, v0, -0xa

    int-to-float v0, v0

    cmpl-float v0, v10, v0

    if-ltz v0, :cond_8

    .line 165
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    add-int/lit8 v0, v0, -0xa

    int-to-float v10, v0

    .line 166
    :cond_8
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0xa

    int-to-float v0, v0

    cmpl-float v0, v11, v0

    if-ltz v0, :cond_9

    .line 167
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0xa

    int-to-float v11, v0

    .line 169
    :cond_9
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v0

    div-float v0, v10, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterX:F

    .line 170
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v0

    div-float v0, v11, v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterY:F

    .line 172
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempInput:[I

    .line 173
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempOutput:[I

    .line 174
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 175
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 176
    float-to-int v4, v10

    .line 177
    float-to-int v5, v11

    .line 178
    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgRadius:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgRadius:I

    int-to-float v7, v7

    invoke-direct {p0, v7}, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->calculateMinBrushSize(F)F

    move-result v7

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v7

    mul-float/2addr v6, v7

    float-to-int v6, v6

    .line 179
    iget v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mStep:I

    .line 180
    new-instance v8, Landroid/graphics/Rect;

    const/4 v9, 0x0

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v13}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v13

    iget-object v14, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v14}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v14

    invoke-direct {v8, v9, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 181
    const/4 v9, 0x0

    .line 172
    invoke-static/range {v0 .. v9}, Lcom/sec/android/mimage/photoretouching/jni/Engine;->runTwirl([I[IIIIIIILandroid/graphics/Rect;I)I

    .line 183
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    goto/16 :goto_0

    .line 124
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public configurationChange()V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x0

    .line 377
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempInput:[I

    .line 378
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempOutput:[I

    .line 380
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempInput:[I

    .line 381
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempInput:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempOutput:[I

    .line 383
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 384
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v1

    .line 385
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v2

    .line 386
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempInput:[I

    .line 387
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    .line 388
    new-instance v5, Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v7}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    invoke-direct {v5, v8, v8, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 383
    invoke-static/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->copyBuffer([IILandroid/graphics/Rect;[IILandroid/graphics/Rect;)V

    .line 389
    invoke-direct {p0, v8}, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->applySphericityTwirlPreview(I)V

    .line 390
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 370
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempInput:[I

    .line 371
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mTempOutput:[I

    .line 372
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 373
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 365
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->drawTwirlSphericity(Landroid/graphics/Canvas;)V

    .line 366
    return-void
.end method

.method public drawTwirlSphericity(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 40
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 42
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoiBasedOnViewTransform()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 43
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 44
    .local v0, "matrix":Landroid/graphics/Matrix;
    const/4 v4, 0x2

    new-array v3, v4, [F

    .line 45
    .local v3, "vecs":[F
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterX:F

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    aput v4, v3, v6

    .line 46
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterY:F

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    aput v4, v3, v7

    .line 48
    const/16 v4, 0x9

    new-array v2, v4, [F

    .line 49
    .local v2, "values":[F
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 50
    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgRadius:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgRadius:I

    int-to-float v5, v5

    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->calculateMinBrushSize(F)F

    move-result v5

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v5

    mul-float/2addr v4, v5

    aget v5, v2, v6

    mul-float v1, v4, v5

    .line 52
    .local v1, "newRadius":F
    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapVectors([F)V

    .line 53
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mPaint:Landroid/graphics/Paint;

    if-eqz v4, :cond_0

    .line 54
    aget v4, v3, v6

    .line 55
    aget v5, v3, v7

    .line 57
    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mPaint:Landroid/graphics/Paint;

    .line 54
    invoke-virtual {p1, v4, v5, v1, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 58
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 59
    return-void
.end method

.method public getBrushSize()I
    .locals 3

    .prologue
    .line 267
    const/4 v0, 0x0

    .line 268
    .local v0, "ret":I
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgRadius:I

    .line 269
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getBrushSize mOrgRadius:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgRadius:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    .line 270
    return v0
.end method

.method public getCenterX()I
    .locals 2

    .prologue
    .line 255
    const/4 v0, 0x0

    .line 256
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterX:F

    float-to-int v0, v1

    .line 257
    return v0
.end method

.method public getCenterY()I
    .locals 2

    .prologue
    .line 261
    const/4 v0, 0x0

    .line 262
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterY:F

    float-to-int v0, v1

    .line 263
    return v0
.end method

.method public getCurrentStep()I
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x0

    .line 294
    .local v0, "ret":I
    return v0
.end method

.method public getMaxBrushSize()I
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    .line 275
    .local v0, "ret":I
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mMaxRadius:I

    .line 276
    return v0
.end method

.method public getMutexOn()Z
    .locals 1

    .prologue
    .line 409
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mutexOn:Z

    return v0
.end method

.method public init(I)V
    .locals 4
    .param p1, "effectType"    # I

    .prologue
    const/4 v3, 0x0

    .line 299
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mStartInitializing:Z

    .line 300
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v1, :cond_0

    .line 302
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    .line 305
    :cond_0
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mEffectType:I

    .line 306
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v1, :cond_1

    .line 308
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v0

    .line 309
    .local v0, "viewBuffer":[I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 311
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode$1;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode$1;-><init>(Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 319
    .end local v0    # "viewBuffer":[I
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mStartInitializing:Z

    .line 320
    return-void
.end method

.method public isStartInitializing()Z
    .locals 1

    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mStartInitializing:Z

    return v0
.end method

.method public setBrushSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 280
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgRadius:I

    .line 281
    return-void
.end method

.method public setOrgCenter(II)V
    .locals 1
    .param p1, "centerX"    # I
    .param p2, "centerY"    # I

    .prologue
    .line 323
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterX:F

    .line 324
    int-to-float v0, p2

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterY:F

    .line 325
    return-void
.end method

.method public setStep(I)V
    .locals 0
    .param p1, "step"    # I

    .prologue
    .line 399
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mStep:I

    .line 400
    return-void
.end method

.method public touch(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 329
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v5, :cond_0

    .line 330
    const/4 v5, 0x0

    .line 352
    :goto_0
    return v5

    .line 332
    :cond_0
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v2

    .line 334
    .local v2, "viewBuffer":[I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewWidth()I

    move-result v4

    .line 335
    .local v4, "viewWidth":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewHeight()I

    move-result v3

    .line 337
    .local v3, "viewHeight":I
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 338
    .local v1, "m":Landroid/graphics/Matrix;
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 339
    .local v0, "inversM":Landroid/graphics/Matrix;
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 340
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 352
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public touchSphericity(Landroid/view/MotionEvent;[III)V
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "viewBuffer"    # [I
    .param p3, "viewWidth"    # I
    .param p4, "viewHeight"    # I

    .prologue
    const/4 v6, 0x1

    const/high16 v5, 0x41200000    # 10.0f

    .line 200
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 201
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 202
    .local v1, "y":F
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getDrawCanvasRoi()Landroid/graphics/Rect;

    move-result-object v2

    float-to-int v3, v0

    float-to-int v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 204
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 209
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 210
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 212
    cmpg-float v2, v0, v5

    if-gtz v2, :cond_1

    .line 213
    const/high16 v0, 0x41200000    # 10.0f

    .line 214
    :cond_1
    cmpg-float v2, v1, v5

    if-gtz v2, :cond_2

    .line 215
    const/high16 v1, 0x41200000    # 10.0f

    .line 216
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    add-int/lit8 v2, v2, -0xa

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_3

    .line 217
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    add-int/lit8 v2, v2, -0xa

    int-to-float v0, v2

    .line 218
    :cond_3
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0xa

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_4

    .line 219
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0xa

    int-to-float v1, v2

    .line 221
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v2

    div-float v2, v0, v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterX:F

    .line 222
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v2

    div-float v2, v1, v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterY:F

    .line 223
    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->applySphericityTwirlPreview(I)V

    goto :goto_0

    .line 227
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 228
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 230
    cmpg-float v2, v0, v5

    if-gtz v2, :cond_5

    .line 231
    const/high16 v0, 0x41200000    # 10.0f

    .line 232
    :cond_5
    cmpg-float v2, v1, v5

    if-gtz v2, :cond_6

    .line 233
    const/high16 v1, 0x41200000    # 10.0f

    .line 234
    :cond_6
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    add-int/lit8 v2, v2, -0xa

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_7

    .line 235
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    add-int/lit8 v2, v2, -0xa

    int-to-float v0, v2

    .line 236
    :cond_7
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0xa

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_8

    .line 237
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0xa

    int-to-float v1, v2

    .line 239
    :cond_8
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v2

    div-float v2, v0, v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterX:F

    .line 240
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getMagnifyMinRatio()F

    move-result v2

    div-float v2, v1, v2

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->mOrgCenterY:F

    .line 242
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->applySphericityTwirlPreview(I)V

    .line 244
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    if-eqz v2, :cond_0

    .line 246
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/EffectSphericityMode;->myActionbarCallback:Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;

    invoke-interface {v2, v6}, Lcom/sec/android/mimage/photoretouching/Core/EffectEffect$OnActionbarCallback;->ableDone(Z)V

    goto/16 :goto_0

    .line 204
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
