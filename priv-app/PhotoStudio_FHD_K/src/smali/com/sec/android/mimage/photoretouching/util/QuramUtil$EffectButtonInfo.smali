.class public Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;
.super Ljava/lang/Object;
.source "QuramUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/util/QuramUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EffectButtonInfo"
.end annotation


# instance fields
.field private mId:I

.field private mIndex:I

.field private mUse:Z


# direct methods
.method public constructor <init>(III)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "idx"    # I
    .param p3, "use"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 1342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1339
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mId:I

    .line 1340
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mIndex:I

    .line 1341
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mUse:Z

    .line 1344
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mId:I

    .line 1345
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mIndex:I

    .line 1346
    if-lez p3, :cond_0

    .line 1347
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mUse:Z

    .line 1350
    :goto_0
    return-void

    .line 1349
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mUse:Z

    goto :goto_0
.end method

.method public constructor <init>(IIZ)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "idx"    # I
    .param p3, "use"    # Z

    .prologue
    const/4 v0, -0x1

    .line 1351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1339
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mId:I

    .line 1340
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mIndex:I

    .line 1341
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mUse:Z

    .line 1353
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mId:I

    .line 1354
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mIndex:I

    .line 1355
    iput-boolean p3, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mUse:Z

    .line 1356
    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 1359
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mId:I

    return v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 1363
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mIndex:I

    return v0
.end method

.method public getUse()Z
    .locals 1

    .prologue
    .line 1367
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/util/QuramUtil$EffectButtonInfo;->mUse:Z

    return v0
.end method
