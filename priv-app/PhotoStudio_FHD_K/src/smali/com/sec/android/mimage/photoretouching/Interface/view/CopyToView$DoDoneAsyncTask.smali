.class Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;
.super Landroid/os/AsyncTask;
.source "CopyToView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DoDoneAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V
    .locals 0

    .prologue
    .line 927
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;)V
    .locals 0

    .prologue
    .line 927
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 931
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyPaste:Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mCopyToImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/CopyPaste;->run_blending_org(Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Core/ImageData;)V

    .line 932
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->invalidateViewsWithThread()V

    .line 933
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mHistoryManager:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 934
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mImageData:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogE(Ljava/lang/String;)V

    .line 935
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    .line 936
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v2

    .line 937
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    .line 935
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([III)V

    .line 939
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 951
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 952
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 954
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 956
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 957
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 942
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 943
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;Landroid/app/ProgressDialog;)V

    .line 944
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06008f

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 945
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 946
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 948
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView$DoDoneAsyncTask;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;->access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/CopyToView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 949
    return-void
.end method
