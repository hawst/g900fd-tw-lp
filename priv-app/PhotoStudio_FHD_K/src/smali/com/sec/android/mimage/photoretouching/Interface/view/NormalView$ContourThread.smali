.class Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;
.super Ljava/lang/Thread;
.source "NormalView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContourThread"
.end annotation


# instance fields
.field private isRun:Z

.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;


# direct methods
.method private constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V
    .locals 1

    .prologue
    .line 3556
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 3558
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->isRun:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;)V
    .locals 0

    .prologue
    .line 3556
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 3561
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->isRun:Z

    .line 3562
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;)V

    .line 3563
    return-void
.end method

.method public run()V
    .locals 4

    .prologue
    .line 3567
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->isRun:Z

    if-nez v1, :cond_2

    .line 3589
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;)V

    .line 3590
    return-void

    .line 3570
    :cond_2
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3575
    :goto_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mColor:I
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)I

    move-result v2

    const v3, 0xffffff

    xor-int/2addr v2, v3

    invoke-static {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;I)V

    .line 3576
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->invalidateViewsWithThread()V

    .line 3577
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3579
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView$ContourThread;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/NormalView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalMaskRoi()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 3571
    :catch_0
    move-exception v0

    .line 3573
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
