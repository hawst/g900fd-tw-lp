.class public Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;
.super Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;
.source "StickerView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$InitViewCallback;
.implements Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame$TestInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$MultiTouchScaleGestureListener;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ResolverSetAsAdapter;,
        Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$SaveAsyncTask;
    }
.end annotation


# instance fields
.field private final ICON_HEIGHT:I

.field private final ICON_WIDTH:I

.field private final REDOALL_DIALOG:I

.field private final UNDOALL_DIALOG:I

.field private mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

.field private mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;

.field private mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentSaveSize:I

.field private mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

.field private mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

.field private mGesture:Landroid/view/GestureDetector;

.field private mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

.field private mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mIsLongClicked:Z

.field private mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

.field private mOptionItemId:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

.field private mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

.field private mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

.field private mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

.field private mViewBitmap:Landroid/graphics/Bitmap;

.field onTempStickerCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;

.field stickerCallback:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trayManager"    # Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .param p3, "actionbarManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .param p4, "dialogManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .param p5, "buttonManager"    # Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;
    .param p6, "decoManager"    # Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .prologue
    const/16 v5, 0xe

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 92
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewFrame;-><init>(Landroid/content/Context;)V

    .line 120
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->stickerCallback:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    .line 149
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->onTempStickerCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;

    .line 1765
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 1767
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 1768
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    .line 1769
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 1770
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 1771
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 1772
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 1773
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 1774
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 1775
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 1776
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;

    .line 1778
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    .line 1783
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1784
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPaint:Landroid/graphics/Paint;

    .line 1788
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->ICON_WIDTH:I

    .line 1789
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->ICON_HEIGHT:I

    .line 1791
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->UNDOALL_DIALOG:I

    .line 1792
    iput v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->REDOALL_DIALOG:I

    .line 1793
    iput-boolean v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mIsLongClicked:Z

    .line 1795
    iput v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mOptionItemId:I

    .line 1797
    const v0, 0x7a1200

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mCurrentSaveSize:I

    .line 1799
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPinchZoomCallback:Lcom/sec/android/mimage/photoretouching/Gui/ImageEditView$ImageEditViewPinchZoomCallback;

    .line 1800
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    .line 1801
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 1802
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mGesture:Landroid/view/GestureDetector;

    .line 1804
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mTouchFunction:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect$OnPinchZoomCallback;

    .line 94
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    .line 95
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    .line 96
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 97
    iput-object p4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 98
    iput-object p5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 99
    iput-object p6, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    .line 101
    invoke-virtual {p0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->setInterface(Ljava/lang/Object;)V

    .line 103
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPaint:Landroid/graphics/Paint;

    .line 104
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentImageData()Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 107
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->getCurrentHistoryManager()Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    .line 109
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->stickerCallback:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    .line 111
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->initTabLayout()V

    .line 113
    invoke-virtual {p0, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->setViewLayerType(I)V

    .line 115
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;

    .line 117
    const-string v0, "JW stickerView"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 118
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;Z)V
    .locals 0

    .prologue
    .line 324
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->setVisibleTopBottomMenu(Z)V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;
    .locals 1

    .prologue
    .line 1771
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;
    .locals 1

    .prologue
    .line 1770
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;
    .locals 1

    .prologue
    .line 1774
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)I
    .locals 1

    .prologue
    .line 1797
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mCurrentSaveSize:I

    return v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 1767
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$14(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;
    .locals 1

    .prologue
    .line 1765
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 0

    .prologue
    .line 1765
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)I
    .locals 1

    .prologue
    .line 1795
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mOptionItemId:I

    return v0
.end method

.method static synthetic access$17(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;I)Z
    .locals 1

    .prologue
    .line 1160
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->runOptionItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$18(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;
    .locals 1

    .prologue
    .line 1778
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;
    .locals 1

    .prologue
    .line 1800
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .locals 1

    .prologue
    .line 1775
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;I)V
    .locals 0

    .prologue
    .line 1797
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mCurrentSaveSize:I

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;
    .locals 1

    .prologue
    .line 1772
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    return-object v0
.end method

.method static synthetic access$22(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V
    .locals 0

    .prologue
    .line 1042
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->doCancel()V

    return-void
.end method

.method static synthetic access$23(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V
    .locals 0

    .prologue
    .line 1070
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->doDone()V

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V
    .locals 0

    .prologue
    .line 1101
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->set2depth()V

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Z
    .locals 1

    .prologue
    .line 1793
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mIsLongClicked:Z

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;Z)V
    .locals 0

    .prologue
    .line 1793
    iput-boolean p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mIsLongClicked:Z

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1768
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 1769
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;)V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->setActivityLayoutThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Lcom/sec/android/mimage/photoretouching/Gui/ActivityLayout$ActivityLayoutTouchUpCallback;)V

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 1767
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private doCancel()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1044
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-nez v3, :cond_0

    .line 1068
    :goto_0
    return-void

    .line 1047
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    if-eqz v3, :cond_1

    .line 1048
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->Destroy()V

    .line 1049
    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    .line 1052
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->resetPreview()V

    .line 1054
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1055
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1058
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1061
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1062
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 1063
    const/4 v8, 0x1

    .line 1064
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 1056
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1066
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->set2depth()V

    goto :goto_0
.end method

.method private doDone()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1072
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    if-eqz v0, :cond_0

    .line 1073
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->applyPreview()V

    .line 1080
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1081
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1083
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1086
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1087
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 1081
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1089
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->applyPreview()V

    .line 1090
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    invoke-direct {v4, v0}, Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;)V

    .line 1092
    .local v4, "effectInfo":Lcom/sec/android/mimage/photoretouching/Core/EffectInfo;
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    .line 1094
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1095
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v2

    .line 1096
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 1097
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mHistoryManager:Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->getEnhance()Z

    move-result v5

    .line 1094
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/mimage/photoretouching/Interface/HistoryManager;->addHistory([IIILcom/sec/android/mimage/photoretouching/Core/EffectInfo;Z)V

    .line 1100
    return-void
.end method

.method private init2DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 884
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 886
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v4, v4, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 887
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 888
    const/4 v2, 0x1

    .line 890
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$12;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$12;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 887
    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 919
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 922
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$13;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$13;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 919
    invoke-virtual {v0, v1, v4, v4, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 948
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 950
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->setActionBarBtnVisibility()V

    .line 951
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->unableDone()V

    .line 953
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->buttonGone(I)V

    .line 954
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 957
    :cond_0
    return-void
.end method

.method private init3DepthActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 960
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v0, :cond_0

    .line 962
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->initActionBarLayout(ZIILcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)V

    .line 963
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x4

    .line 966
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$14;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$14;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 963
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 990
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    const/4 v1, 0x3

    .line 993
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$15;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$15;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 990
    invoke-virtual {v0, v1, v4, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->registCustomizedActionBar(IZZLcom/sec/android/mimage/photoretouching/Gui/listener/ActionbarButtonsListener$TouchInterface;)Z

    .line 1036
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->ableDone()V

    .line 1039
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeDoneCancelLayout()V

    .line 1041
    :cond_0
    return-void
.end method

.method private initGesture()V
    .locals 4

    .prologue
    .line 1614
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$16;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$16;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 1658
    .local v0, "gesture":Landroid/view/GestureDetector$OnGestureListener;
    new-instance v1, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    .line 1659
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$MultiTouchScaleGestureListener;

    invoke-direct {v3, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$MultiTouchScaleGestureListener;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    invoke-direct {v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector$SimpleOnMultiTouchGestureListener;)V

    .line 1658
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mGestureDetector:Lcom/sec/android/mimage/photoretouching/util/MultiTouchGestureDetector;

    .line 1660
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mGesture:Landroid/view/GestureDetector;

    .line 1661
    return-void
.end method

.method private initSaveOptionDialog()V
    .locals 11

    .prologue
    const v10, 0x7f0601f0

    const v9, 0x7f0601cb

    const/16 v2, 0x9

    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 607
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660
    :goto_0
    return-void

    .line 612
    :cond_0
    new-instance v8, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$3;

    invoke-direct {v8, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 626
    .local v8, "saveListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 628
    const v3, 0x7f0601cd

    .line 630
    const/4 v5, 0x1

    .line 632
    const v7, 0x103012e

    .line 626
    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZZLandroid/graphics/Point;I)V

    .line 634
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 635
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x810

    move v3, v9

    move v5, v9

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 639
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v2, 0x820

    move v3, v10

    move v5, v10

    move-object v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addDialogButton(IIIILandroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 641
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$4;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 648
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f06000d

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$5;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 659
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    goto :goto_0
.end method

.method private initSetAsDialog()V
    .locals 7

    .prologue
    .line 1529
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1531
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1532
    const v2, 0x7f090158

    .line 1533
    const v3, 0x7f0600a3

    .line 1534
    const/4 v4, 0x0

    .line 1535
    const/4 v5, 0x0

    .line 1536
    const v6, 0x103012e

    .line 1531
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1537
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1539
    :cond_0
    return-void
.end method

.method private initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isShareVia"    # Z

    .prologue
    .line 1280
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1281
    .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    const/4 v2, 0x0

    .line 1282
    .local v2, "shareChooser":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 1283
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    const v4, 0x7f06000f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 1287
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/content/pm/LabeledIntent;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/pm/LabeledIntent;

    .line 1288
    .local v0, "extraIntents":[Landroid/content/pm/LabeledIntent;
    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1289
    const/high16 v3, 0x24000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1290
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1471
    return-void

    .line 1285
    .end local v0    # "extraIntents":[Landroid/content/pm/LabeledIntent;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    const v4, 0x7f0600a3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method private initShareViaDialog()V
    .locals 7

    .prologue
    .line 1517
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 1519
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const/16 v1, 0x1000

    .line 1520
    const v2, 0x7f090157

    .line 1521
    const v3, 0x7f06000f

    .line 1522
    const/4 v4, 0x0

    .line 1523
    const/4 v5, 0x0

    .line 1524
    const v6, 0x103012e

    .line 1519
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 1525
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 1527
    :cond_0
    return-void
.end method

.method private initTabLayout()V
    .locals 2

    .prologue
    .line 486
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->initDataBase(Landroid/content/Context;)V

    .line 487
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setDrawerHandleVisibility(I)V

    .line 488
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTab()V

    .line 489
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->onTempStickerCallback:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->addSticker(Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager$OnTempStickerCallback;)V

    .line 490
    return-void
.end method

.method private initUndoRedoAllDialog()V
    .locals 15

    .prologue
    const v6, 0x103012e

    const/16 v1, 0x1000

    const/16 v14, 0x500

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 663
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 665
    const v3, 0x7f0600a6

    move v4, v2

    .line 663
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 670
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 671
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 672
    const v3, 0x7f06009c

    .line 674
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$6;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 671
    invoke-virtual {v0, v14, v3, v5, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 681
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060007

    .line 682
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$7;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 681
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 689
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v3, 0x7f060009

    .line 690
    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$8;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 689
    invoke-virtual {v0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 698
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 700
    iget-object v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 701
    const/4 v9, 0x2

    .line 702
    const v10, 0x7f0600a1

    move v8, v1

    move v11, v2

    move-object v12, v5

    move v13, v6

    .line 700
    invoke-virtual/range {v7 .. v13}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registDialog(IIIZLandroid/graphics/Point;I)V

    .line 707
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->registButtons()V

    .line 708
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 709
    const v1, 0x7f0601ce

    .line 711
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$9;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 708
    invoke-virtual {v0, v14, v1, v5, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->addNoti(IILjava/lang/String;Lcom/sec/android/mimage/photoretouching/Gui/listener/DialogButtonsListener$DialogButtonTouchInterface;)V

    .line 718
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060007

    .line 719
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$10;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 718
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 726
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f060009

    .line 727
    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$11;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$11;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;)V

    .line 726
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    .line 735
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->finishRegistingButtons()V

    .line 736
    return-void
.end method

.method private runOptionItem(I)Z
    .locals 9
    .param p1, "optionItemId"    # I

    .prologue
    const v8, 0x7f090156

    const v7, 0x7a1200

    const/4 v6, 0x7

    const/4 v5, 0x0

    const/16 v4, 0x9

    .line 1162
    const/4 v1, 0x0

    .line 1163
    .local v1, "ret":Z
    const/4 v2, 0x0

    .line 1164
    .local v2, "uri":Landroid/net/Uri;
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mOptionItemId:I

    .line 1165
    sparse-switch p1, :sswitch_data_0

    .line 1276
    :cond_0
    :goto_0
    return v1

    .line 1168
    :sswitch_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1170
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v6}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1177
    :goto_1
    const/4 v1, 0x1

    .line 1178
    goto :goto_0

    .line 1175
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->openCamera(Landroid/content/Context;)V

    goto :goto_1

    .line 1180
    :sswitch_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1197
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mCurrentSaveSize:I

    .line 1198
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1199
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1210
    :cond_2
    :goto_2
    const/4 v1, 0x1

    .line 1211
    goto :goto_0

    .line 1201
    :cond_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090157

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1202
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 1203
    if-nez v2, :cond_4

    .line 1205
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1207
    :cond_4
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeShareViaIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    goto :goto_2

    .line 1213
    :sswitch_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1215
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1217
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getPath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1219
    .local v0, "fileName":Ljava/lang/String;
    if-nez v0, :cond_6

    .line 1220
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->getSimpleDate()Ljava/lang/String;

    .line 1224
    :goto_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v0, v8}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->setTextToDialog(Ljava/lang/String;I)V

    .line 1226
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v4, 0x2e000000

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    .line 1227
    const/4 v1, 0x1

    .line 1228
    goto/16 :goto_0

    .line 1222
    .restart local v0    # "fileName":Ljava/lang/String;
    :cond_6
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1231
    .end local v0    # "fileName":Ljava/lang/String;
    :sswitch_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1248
    iput v7, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mCurrentSaveSize:I

    .line 1249
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->isRegisterd(I)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1250
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 1263
    :cond_7
    :goto_4
    const/4 v1, 0x1

    .line 1264
    goto/16 :goto_0

    .line 1252
    :cond_8
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090158

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->IsDialogShown(I)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1255
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 1256
    if-nez v2, :cond_9

    .line 1258
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1260
    :cond_9
    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/Interface/IntentManager;->makeSetAsIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v3, v5}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->initSetAsOrShareViaLayout(Landroid/content/Intent;Z)V

    .line 1261
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v4, 0x7f090158

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    goto :goto_4

    .line 1267
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1268
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    .line 1269
    :cond_a
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setVisibilityDirectly(I)V

    .line 1270
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 1271
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v4, 0x17000000

    invoke-virtual {v3, v4}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto/16 :goto_0

    .line 1165
    :sswitch_data_0
    .sparse-switch
        0x7f090007 -> :sswitch_2
        0x7f090157 -> :sswitch_1
        0x7f090158 -> :sswitch_3
        0x7f09015a -> :sswitch_0
        0x7f09015b -> :sswitch_4
    .end sparse-switch
.end method

.method private set2depth()V
    .locals 0

    .prologue
    .line 1103
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->init2DepthActionBar()V

    .line 1121
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->invalidateViews()V

    .line 1122
    return-void
.end method

.method private setVisibleTopBottomMenu(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 325
    if-eqz p1, :cond_1

    .line 326
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->show()V

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionbarNBottomButtonAnimation:Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView$ActionbarNBottomButtonAnimation;->hide()V

    goto :goto_0
.end method

.method private showTabLayout(IZZ)V
    .locals 1
    .param p1, "assistantType"    # I
    .param p2, "initByconfig"    # Z
    .param p3, "doOpen"    # Z

    .prologue
    .line 480
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0, p1, p3}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->showTabView(IZ)V

    .line 482
    :cond_0
    return-void
.end method


# virtual methods
.method public OnTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 338
    const/4 v1, 0x1

    .line 340
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    .line 341
    .local v0, "isDrawerOpened":Z
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    if-eqz v2, :cond_0

    .line 342
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    invoke-virtual {v2, p2, v0}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->touch(Landroid/view/MotionEvent;Z)V

    .line 352
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->invalidateViews()V

    .line 353
    return v1
.end method

.method public backPressed()V
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->doCancel()V

    .line 257
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->openDrawer()V

    .line 266
    :goto_0
    return-void

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->closeDrawer()V

    .line 263
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->saveToDB()V

    .line 264
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x31000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public changeImage(I)V
    .locals 0
    .param p1, "trayButtonIdx"    # I

    .prologue
    .line 452
    return-void
.end method

.method public getActionHeight()I
    .locals 1

    .prologue
    .line 464
    const/4 v0, 0x0

    return v0
.end method

.method public getBottomButtonHeight()I
    .locals 1

    .prologue
    .line 468
    const/4 v0, 0x0

    return v0
.end method

.method public getStatusHeight()I
    .locals 1

    .prologue
    .line 475
    const/4 v0, 0x0

    return v0
.end method

.method public getmDecorationMenuLayoutManager()Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    return-object v0
.end method

.method public initActionbar()V
    .locals 0

    .prologue
    .line 270
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->init2DepthActionBar()V

    .line 274
    return-void
.end method

.method public initButtons()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW initButton: mDecorationMenuLayoutManager.getRecentlyList().size()="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getRecentButton()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v2, :cond_0

    .line 239
    const/4 v0, 0x2

    invoke-direct {p0, v0, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->showTabLayout(IZZ)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_TYPE1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    .line 250
    :goto_0
    return-void

    .line 244
    :cond_0
    invoke-direct {p0, v2, v3, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->showTabLayout(IZZ)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    sget v1, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->TAB_STICKER_RECENTLY:I

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    goto :goto_0
.end method

.method public initDialog()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->init()V

    .line 281
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->initUndoRedoAllDialog()V

    .line 283
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->initSaveOptionDialog()V

    .line 284
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->initShareViaDialog()V

    .line 285
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->initSetAsDialog()V

    .line 287
    :cond_0
    return-void
.end method

.method public initEffect()V
    .locals 0

    .prologue
    .line 227
    return-void
.end method

.method public initProgressText()V
    .locals 0

    .prologue
    .line 309
    return-void
.end method

.method public initSubView()V
    .locals 0

    .prologue
    .line 440
    return-void
.end method

.method public initTrayLayout()V
    .locals 0

    .prologue
    .line 447
    return-void
.end method

.method public initView()V
    .locals 0

    .prologue
    .line 232
    return-void
.end method

.method public newIntent(Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .prologue
    .line 382
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 383
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->isEdited()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    const v1, 0x7f0601cd

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->showDialog(I)Z

    .line 397
    :goto_0
    return-void

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    if-eqz v0, :cond_1

    .line 391
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->deleteCurrentButton()V

    .line 392
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    invoke-interface {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;->getImage()V

    .line 393
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 395
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->changeViewStatus(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 359
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 360
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPaint:Landroid/graphics/Paint;

    .line 362
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->destroy()V

    .line 363
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    .line 365
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 366
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    .line 368
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    .line 369
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->destroy()V

    .line 371
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mNewIntentCallback:Lcom/sec/android/mimage/photoretouching/PhotoRetouching$NewIntentCallback;

    .line 374
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 375
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 378
    :cond_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 295
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 296
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPaint:Landroid/graphics/Paint;

    .line 293
    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->drawImage(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Lcom/sec/android/mimage/photoretouching/Interface/ImageDataInterface;Landroid/graphics/Paint;)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->drawStickerBdry(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 303
    :cond_0
    return-void
.end method

.method public onFrameKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 400
    const/16 v1, 0x17

    if-eq p1, v1, :cond_0

    const/16 v1, 0x42

    if-ne p1, v1, :cond_3

    .line 402
    :cond_0
    const/4 v0, 0x0

    .line 403
    .local v0, "isFocused":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    if-eqz v1, :cond_1

    .line 404
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onEnter()Z

    move-result v0

    .line 410
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    if-eqz v1, :cond_2

    .line 411
    if-nez v0, :cond_2

    .line 412
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onkey_main_Enter()V

    .line 434
    .end local v0    # "isFocused":Z
    :cond_2
    :goto_0
    return v2

    .line 416
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 418
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->backPressed()V

    goto :goto_0
.end method

.method public onLayout()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 850
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->setViewWidth(I)V

    .line 851
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->changeLayoutSize(I)V

    .line 855
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->getImageEditViewWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->getImageEditViewHeight()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 856
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 858
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 860
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 863
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 864
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 858
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 865
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JW onLayout: mImageData.getPreviewHeight()="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 866
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    if-eqz v0, :cond_0

    .line 868
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPinchZoomEffect:Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/PinchZoomEffect;->configurationChange()V

    .line 882
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(I)V
    .locals 0
    .param p1, "viewId"    # I

    .prologue
    .line 460
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->runOptionItem(I)Z

    .line 461
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 456
    return-void
.end method

.method public refreshView()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1593
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->getImageEditViewWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->getImageEditViewHeight()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setViewSize(II)V

    .line 1595
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    .line 1597
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewOutputBuffer()[I

    move-result-object v1

    .line 1598
    .local v1, "output":[I
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mViewBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1601
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 1604
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 1605
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    .line 1606
    const/4 v8, 0x1

    .line 1607
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mPaint:Landroid/graphics/Paint;

    move v4, v2

    move v5, v2

    .line 1599
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V

    .line 1609
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->invalidateViews()V

    .line 1610
    return-void
.end method

.method public setConfigurationChanged()V
    .locals 3

    .prologue
    .line 313
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mTrayManager:Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;->onConfigurationChanged()V

    .line 314
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mActionBarManager:Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;->onConfigurationChanged()V

    .line 315
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mButtonsManager:Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;->onConfigurationChanged()V

    .line 316
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDialogsManager:Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;->changeLanguage()V

    .line 317
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->configurationChanged()V

    .line 319
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getCurrnetType()I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->isDrawerOpened()Z

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->showTabLayout(IZZ)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mDecorationMenuLayoutManager:Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->getCurrnetType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/DecorationMenuLayoutManager;->setTabIndex(I)V

    .line 322
    return-void
.end method

.method public setStickerMode(ILandroid/graphics/Rect;)V
    .locals 4
    .param p1, "resId"    # I
    .param p2, "targetOrgRoi"    # Landroid/graphics/Rect;

    .prologue
    .line 740
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    if-nez v0, :cond_0

    .line 741
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->stickerCallback:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Core/StickerEffect$OnMyStickerCallback;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    .line 742
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->mStickerEffect:Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/mimage/photoretouching/Core/StickerEffect;->setStickerMode(ILandroid/graphics/Rect;)V

    .line 744
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->init3DepthActionBar()V

    .line 745
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/view/StickerView;->invalidateViews()V

    .line 746
    return-void
.end method
