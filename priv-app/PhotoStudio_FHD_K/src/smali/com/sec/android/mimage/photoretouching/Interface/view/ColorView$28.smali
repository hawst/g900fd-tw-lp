.class Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$28;
.super Ljava/lang/Object;
.source "ColorView.java"

# interfaces
.implements Lcom/sec/android/mimage/photoretouching/Interface/SeekbarThreadManager$OnCallBack;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->initColorView(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Interface/TrayManager;Lcom/sec/android/mimage/photoretouching/Interface/ActionBarManager;Lcom/sec/android/mimage/photoretouching/Interface/DialogsManager;Lcom/sec/android/mimage/photoretouching/Interface/ViewButtonsManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$28;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    .line 2214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public applyEffect(I)V
    .locals 2
    .param p1, "step"    # I

    .prologue
    .line 2223
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$28;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mCurrentEffectType:I
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$12(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)I

    move-result v0

    const v1, 0x15001506

    if-eq v0, v1, :cond_0

    .line 2224
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$28;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    # getter for: Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->mColorEffect:Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->access$11(Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;)Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ColorEffect;->addEvent(I)V

    .line 2225
    :cond_0
    return-void
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 2218
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView$28;->this$0:Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/view/ColorView;->postInvalidateViews()V

    .line 2219
    return-void
.end method
