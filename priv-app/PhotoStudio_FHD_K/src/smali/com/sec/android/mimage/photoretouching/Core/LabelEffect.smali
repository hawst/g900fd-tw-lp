.class public Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;
.super Ljava/lang/Object;
.source "LabelEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;,
        Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;
    }
.end annotation


# static fields
.field public static final LABEL_MAX_NUM:I = 0x5


# instance fields
.field private mBm_delete:Landroid/graphics/Bitmap;

.field private mBm_delete_press:Landroid/graphics/Bitmap;

.field private mBm_lrtb:Landroid/graphics/Bitmap;

.field private mBm_lrtb_press:Landroid/graphics/Bitmap;

.field private mBm_rotate:Landroid/graphics/Bitmap;

.field private mBm_rotate_press:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mCurrentLabelCount:I

.field private mCurrentLabelIndex:I

.field mDeletePressed:Z

.field private mGreyPaint:Landroid/graphics/Paint;

.field private mHandlerMaxSize:I

.field private mHandler_LB:Landroid/graphics/Bitmap;

.field private mHandler_RB:Landroid/graphics/Bitmap;

.field private mHandler_delete:Landroid/graphics/Bitmap;

.field private mHandler_rotate:Landroid/graphics/Bitmap;

.field private mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

.field private mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

.field private mLabelBuilder:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;

.field private mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

.field private mRectPaint:Landroid/graphics/Paint;

.field mTouchPressed:Z

.field private mTouchType:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    .line 42
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 43
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mGreyPaint:Landroid/graphics/Paint;

    .line 45
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 47
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    .line 49
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    .line 50
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    .line 53
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mDeletePressed:Z

    .line 54
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchPressed:Z

    .line 56
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete:Landroid/graphics/Bitmap;

    .line 57
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    .line 58
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 59
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 60
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 61
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 63
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandlerMaxSize:I

    .line 65
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelBuilder:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;

    .line 76
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    .line 124
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imgInfo"    # Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .param p3, "labelCallback"    # Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    .line 42
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 43
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mGreyPaint:Landroid/graphics/Paint;

    .line 45
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 47
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    .line 49
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    .line 50
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    .line 53
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mDeletePressed:Z

    .line 54
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchPressed:Z

    .line 56
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete:Landroid/graphics/Bitmap;

    .line 57
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    .line 58
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 59
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 60
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 61
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 63
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandlerMaxSize:I

    .line 65
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelBuilder:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;

    .line 76
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    .line 80
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    .line 81
    iput-object p2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 83
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020374

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    .line 84
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020375

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete:Landroid/graphics/Bitmap;

    .line 85
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020378

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    .line 87
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020377

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    .line 88
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020376

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    .line 89
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020379

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    .line 91
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 92
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 93
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 94
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 96
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 97
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mRectPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mRectPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mRectPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 101
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mGreyPaint:Landroid/graphics/Paint;

    .line 102
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mGreyPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mGreyPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mGreyPaint:Landroid/graphics/Paint;

    const/16 v1, 0x4b

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/mimage/photoretouching/Core/Label;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    .line 115
    :cond_0
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;-><init>(Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelBuilder:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;

    .line 117
    iput-object p3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->getHandlerMaxSize()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandlerMaxSize:I

    .line 120
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;)Lcom/sec/android/mimage/photoretouching/Core/ImageData;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    return-object v0
.end method

.method private setHandlerIconPressed(I)V
    .locals 1
    .param p1, "touchType"    # I

    .prologue
    .line 343
    packed-switch p1, :pswitch_data_0

    .line 371
    :goto_0
    return-void

    .line 345
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchPressed:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mDeletePressed:Z

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 351
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchPressed:Z

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 354
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 357
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchPressed:Z

    if-eqz v0, :cond_2

    .line 358
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 360
    :cond_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 363
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchPressed:Z

    if-eqz v0, :cond_3

    .line 364
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 366
    :cond_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 343
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public Destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 300
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    if-eqz v1, :cond_0

    .line 302
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 305
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    .line 311
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 312
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 313
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 315
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 316
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 317
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 319
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 320
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 321
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 322
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 324
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mRectPaint:Landroid/graphics/Paint;

    .line 325
    iput-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mGreyPaint:Landroid/graphics/Paint;

    .line 326
    return-void

    .line 303
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 304
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Label;->destory()V

    .line 302
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public applyOriginal()[I
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 765
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v10

    .line 766
    .local v10, "ret":[I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v1

    .line 767
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v3

    .line 768
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 766
    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 769
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalInputData()[I

    move-result-object v1

    .line 771
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    .line 774
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    .line 775
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 769
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 777
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    if-lt v8, v1, :cond_0

    .line 786
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v3

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v6

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v7

    move-object v1, v10

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 788
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 789
    return-object v10

    .line 779
    :cond_0
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->getLabelOrderPosition(I)I

    move-result v9

    .line 780
    .local v9, "idx":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v9

    if-eqz v1, :cond_1

    .line 782
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v9

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->applyOriginal(Landroid/graphics/Bitmap;)V

    .line 777
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public applyPreview()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 740
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v1

    .line 741
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v3

    .line 742
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 740
    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 743
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewInputBuffer()[I

    move-result-object v1

    .line 745
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v3

    .line 748
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v6

    .line 749
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v7

    move v4, v2

    move v5, v2

    .line 743
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 751
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    if-lt v8, v1, :cond_0

    .line 759
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updatePreviewBuffer(Landroid/graphics/Bitmap;)V

    .line 760
    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 761
    return-void

    .line 753
    :cond_0
    invoke-virtual {p0, v8}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->getLabelOrderPosition(I)I

    move-result v9

    .line 754
    .local v9, "idx":I
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v9

    if-eqz v1, :cond_1

    .line 756
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v9

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->applyPreview(Landroid/graphics/Bitmap;)V

    .line 751
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public configurationChanged()V
    .locals 2

    .prologue
    .line 330
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    if-eqz v1, :cond_0

    .line 332
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    if-lt v0, v1, :cond_1

    .line 340
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 334
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 336
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Label;->configurationChanged()V

    .line 332
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public copy(Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;)V
    .locals 1
    .param p1, "label"    # Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;

    .prologue
    .line 136
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 137
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    .line 138
    iget-object v0, p1, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    .line 139
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    .line 140
    iget v0, p1, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    .line 141
    return-void
.end method

.method public deleteLabel()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 229
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    if-eq v2, v5, :cond_0

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    if-lez v2, :cond_0

    .line 231
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    .line 233
    .local v0, "curLabelIdx":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->destory()V

    .line 234
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    iget v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aput-object v6, v2, v3

    .line 236
    const/4 v1, 0x0

    .line 237
    .local v1, "i":I
    move v1, v0

    :goto_0
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_2

    .line 246
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    .line 247
    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    .line 249
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    if-gtz v2, :cond_4

    .line 250
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;->actionBarUnableSave()V

    .line 256
    .end local v0    # "curLabelIdx":I
    .end local v1    # "i":I
    :cond_0
    :goto_1
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    if-nez v2, :cond_1

    .line 258
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    if-eqz v2, :cond_1

    .line 260
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;->set2DepthActionBar()V

    .line 264
    :cond_1
    return-void

    .line 239
    .restart local v0    # "curLabelIdx":I
    .restart local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v1

    if-nez v2, :cond_3

    .line 241
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    add-int/lit8 v4, v1, 0x1

    aget-object v3, v3, v4

    aput-object v3, v2, v1

    .line 242
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    add-int/lit8 v3, v1, 0x1

    aput-object v6, v2, v3

    .line 237
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 252
    :cond_4
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    invoke-interface {v2}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;->actionBarAbleSave()V

    goto :goto_1
.end method

.method public deleteLabelAll()V
    .locals 3

    .prologue
    .line 268
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    if-lt v0, v1, :cond_0

    .line 275
    return-void

    .line 269
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Label;->destory()V

    .line 270
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 272
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    .line 273
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    .line 268
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public deleteLabelOrdering(I)V
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 701
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    if-lt v0, v1, :cond_0

    .line 706
    move v0, p1

    :goto_1
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_2

    .line 710
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    add-int/lit8 v0, v1, -0x1

    :goto_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    array-length v1, v1

    if-lt v0, v1, :cond_3

    .line 713
    return-void

    .line 702
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getZOrder()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getZOrder()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 703
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getZOrder()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->setZOrder(I)V

    .line 701
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 707
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    add-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getZOrder()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->setZOrder(I)V

    .line 706
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 711
    :cond_3
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->setZOrder(I)V

    .line 710
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public drawLabelBdry(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 24
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 511
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    if-nez v2, :cond_1

    .line 670
    :cond_0
    :goto_0
    return-void

    .line 514
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v22

    .line 517
    .local v22, "viewTransform":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getSupMatrixBasedOnViewTransform()Landroid/graphics/Matrix;

    move-result-object v21

    .line 518
    .local v21, "supportViewtransMatrix":Landroid/graphics/Matrix;
    new-instance v15, Landroid/graphics/RectF;

    invoke-direct {v15}, Landroid/graphics/RectF;-><init>()V

    .line 519
    .local v15, "previewRect":Landroid/graphics/RectF;
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v4}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewWidth()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v15, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 520
    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 523
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    .line 524
    .local v8, "drawRoi":Landroid/graphics/RectF;
    new-instance v20, Landroid/graphics/RectF;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/RectF;-><init>()V

    .line 526
    .local v20, "src":Landroid/graphics/RectF;
    const/4 v2, 0x2

    new-array v14, v2, [F

    .line 528
    .local v14, "point":[F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 530
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    array-length v2, v2

    if-lt v11, v2, :cond_6

    .line 617
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 618
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 620
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    aput v3, v14, v2

    .line 621
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->y:F

    aput v3, v14, v2

    .line 622
    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 624
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getDrawBdry()Landroid/graphics/RectF;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 625
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 627
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 628
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getAngle()I

    move-result v2

    int-to-float v2, v2

    .line 629
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 630
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 628
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 632
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 633
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 632
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 634
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 635
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 634
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 636
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 637
    iget v2, v8, Landroid/graphics/RectF;->right:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 636
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 638
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float v3, v2, v3

    iget v2, v8, Landroid/graphics/RectF;->top:F

    const/high16 v4, 0x40000000    # 2.0f

    sub-float v4, v2, v4

    .line 639
    iget v2, v8, Landroid/graphics/RectF;->left:F

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v5, v2

    iget v2, v8, Landroid/graphics/RectF;->bottom:F

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mRectPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    .line 638
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 641
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    .line 642
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 643
    iget v3, v8, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 644
    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 642
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 648
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 649
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 650
    iget v3, v8, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 651
    iget v4, v8, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 649
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 654
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_4

    .line 655
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 656
    iget v3, v8, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 657
    iget v4, v8, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 655
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 661
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_5

    .line 662
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 663
    iget v3, v8, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 664
    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 662
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 668
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0

    .line 532
    :cond_6
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    if-lt v12, v2, :cond_7

    .line 530
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 534
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    if-eqz v2, :cond_8

    .line 535
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getZOrder()I

    move-result v2

    if-ne v2, v11, :cond_8

    .line 537
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 539
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v3, v3, v12

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    aput v3, v14, v2

    .line 540
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v3, v3, v12

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getDrawCenterPT()Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->y:F

    aput v3, v14, v2

    .line 541
    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 543
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getDrawBdry()Landroid/graphics/RectF;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 544
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 546
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 547
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getAngle()I

    move-result v2

    int-to-float v2, v2

    .line 548
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 549
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 547
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 551
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, LE - drawLabelBdry() - viewTransform : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 552
    const-string v3, " / mLabel[j].getDrawBdry() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v3, v3, v12

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getDrawBdry()Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 553
    const-string v3, " / drawRoi : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 551
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 555
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v23

    .line 556
    .local v23, "width":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    .line 557
    .local v10, "height":I
    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v0, v2

    move/from16 v17, v0

    .line 558
    .local v17, "resizeWidth":I
    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v0, v2

    move/from16 v16, v0

    .line 559
    .local v16, "resizeHeight":I
    move/from16 v0, v17

    int-to-float v2, v0

    move/from16 v0, v23

    int-to-float v3, v0

    div-float v19, v2, v3

    .line 560
    .local v19, "scaleWidth":F
    move/from16 v0, v16

    int-to-float v2, v0

    int-to-float v3, v10

    div-float v18, v2, v3

    .line 562
    .local v18, "scaleHeight":F
    new-instance v13, Landroid/graphics/Matrix;

    invoke-direct {v13}, Landroid/graphics/Matrix;-><init>()V

    .line 563
    .local v13, "matrix":Landroid/graphics/Matrix;
    move/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 564
    iget v2, v8, Landroid/graphics/RectF;->left:F

    iget v3, v8, Landroid/graphics/RectF;->top:F

    invoke-virtual {v13, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 566
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, LE - drawLabelBdry() - matrix : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / scaleWidth : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 567
    const-string v3, " / scaleHeight : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / drawRoi : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / srcRoi : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 566
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 570
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v19, v2

    if-eqz v2, :cond_9

    .line 573
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mGreyPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 585
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 586
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getAngle()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    .line 587
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 588
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 586
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 590
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 591
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getAngle()I

    move-result v2

    int-to-float v2, v2

    .line 592
    const/4 v3, 0x0

    aget v3, v14, v3

    .line 593
    const/4 v4, 0x1

    aget v4, v14, v4

    .line 591
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 594
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v19, v2

    if-eqz v2, :cond_a

    .line 597
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v13, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 607
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 610
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 532
    .end local v10    # "height":I
    .end local v13    # "matrix":Landroid/graphics/Matrix;
    .end local v16    # "resizeHeight":I
    .end local v17    # "resizeWidth":I
    .end local v18    # "scaleHeight":F
    .end local v19    # "scaleWidth":F
    .end local v23    # "width":I
    :cond_8
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2

    .line 575
    .restart local v10    # "height":I
    .restart local v13    # "matrix":Landroid/graphics/Matrix;
    .restart local v16    # "resizeHeight":I
    .restart local v17    # "resizeWidth":I
    .restart local v18    # "scaleHeight":F
    .restart local v19    # "scaleWidth":F
    .restart local v23    # "width":I
    :catch_0
    move-exception v9

    .line 576
    .local v9, "e":Ljava/lang/Exception;
    goto/16 :goto_0

    .line 581
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iget v3, v8, Landroid/graphics/RectF;->left:F

    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mGreyPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_3

    .line 599
    :catch_1
    move-exception v9

    .line 600
    .restart local v9    # "e":Ljava/lang/Exception;
    goto/16 :goto_0

    .line 605
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v12

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->GetObjectBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iget v3, v8, Landroid/graphics/RectF;->left:F

    iget v4, v8, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_4
.end method

.method public freeResource()V
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 280
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 281
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 283
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb_press:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 284
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate_press:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 285
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete_press:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 287
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 288
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 289
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 290
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->recycleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 291
    return-void
.end method

.method public getCurLabelCount()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    return v0
.end method

.method public getCurLabelIdx()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    return v0
.end method

.method public getHandlerMaxSize()I
    .locals 5

    .prologue
    .line 145
    const/4 v0, 0x0

    .line 147
    .local v0, "size":I
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 149
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 150
    if-lez v0, :cond_0

    move v1, v0

    .line 160
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 153
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 154
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 155
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 156
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 157
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_3

    .line 158
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_3
    move v1, v0

    .line 160
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalHeight()I

    move-result v0

    .line 133
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLabel()[Lcom/sec/android/mimage/photoretouching/Core/Label;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    return-object v0
.end method

.method public getLabelOrderPosition(I)I
    .locals 2
    .param p1, "orderNum"    # I

    .prologue
    .line 683
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 690
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    add-int/lit8 v0, v1, -0x1

    .end local v0    # "i":I
    :goto_1
    return v0

    .line 684
    .restart local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    .line 683
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 686
    :cond_2
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getZOrder()I

    move-result v1

    if-ne v1, p1, :cond_1

    goto :goto_1
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getOriginalWidth()I

    move-result v0

    .line 128
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetHandlerIconPressed()V
    .locals 1

    .prologue
    .line 375
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchPressed:Z

    .line 376
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_delete:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_delete:Landroid/graphics/Bitmap;

    .line 377
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_rotate:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_rotate:Landroid/graphics/Bitmap;

    .line 378
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_LB:Landroid/graphics/Bitmap;

    .line 379
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mBm_lrtb:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandler_RB:Landroid/graphics/Bitmap;

    .line 380
    return-void
.end method

.method public resetLabelOrdering()V
    .locals 2

    .prologue
    .line 694
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    .line 698
    return-void

    .line 695
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 696
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Core/Label;->setZOrder(I)V

    .line 694
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setLabelMode(ILandroid/graphics/Rect;Ljava/lang/String;I)V
    .locals 18
    .param p1, "resId"    # I
    .param p2, "targetOrgRoi"    # Landroid/graphics/Rect;
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "styleNum"    # I

    .prologue
    .line 176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    .line 177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->destory()V

    .line 178
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    const/4 v4, 0x0

    aput-object v4, v2, v3

    .line 179
    const v2, 0x1000100d

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/ViewStatus$SubMode;->setMode(I)V

    .line 180
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelBuilder:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$LabelBuilder;->getLabelBitmap(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 182
    .local v9, "labelBitmap":Landroid/graphics/Bitmap;
    if-nez v9, :cond_1

    .line 225
    :goto_0
    return-void

    .line 185
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mContext:Landroid/content/Context;

    .line 186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    .line 188
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    .line 189
    const/4 v7, 0x2

    .line 190
    const/4 v8, 0x0

    .line 192
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mHandlerMaxSize:I

    move/from16 v5, p1

    invoke-direct/range {v2 .. v10}, Lcom/sec/android/mimage/photoretouching/Core/Label;-><init>(Landroid/content/Context;Lcom/sec/android/mimage/photoretouching/Core/ImageData;IIIZLandroid/graphics/Bitmap;I)V

    .line 185
    aput-object v2, v11, v12

    .line 194
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sj, LabelEffect - setLabelMode() - mCurrentLabelCount : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 195
    const-string v3, " / mLabel[mCurrentLabelCount].getZOrder() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getZOrder()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 196
    const-string v3, " / orgRoi : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getOrgDestROI()Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 194
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 213
    if-eqz p2, :cond_2

    .line 215
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    aget-object v10, v2, v3

    new-instance v11, Landroid/graphics/RectF;

    move-object/from16 v0, p2

    invoke-direct {v11, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 216
    new-instance v12, Landroid/graphics/PointF;

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v12, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 217
    new-instance v13, Landroid/graphics/PointF;

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-direct {v13, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 218
    new-instance v14, Landroid/graphics/PointF;

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-direct {v14, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 219
    new-instance v15, Landroid/graphics/PointF;

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    invoke-direct {v15, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 220
    new-instance v16, Landroid/graphics/PointF;

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 221
    const/16 v17, 0x0

    .line 215
    invoke-virtual/range {v10 .. v17}, Lcom/sec/android/mimage/photoretouching/Core/Label;->setOrgDestROI(Landroid/graphics/RectF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;I)V

    .line 223
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    .line 224
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    goto/16 :goto_0
.end method

.method public setLabelOrdering(I)V
    .locals 3
    .param p1, "curIdx"    # I

    .prologue
    .line 673
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    if-ge v0, v1, :cond_0

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    array-length v2, v2

    if-le v1, v2, :cond_1

    .line 679
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, p1

    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->setZOrder(I)V

    .line 680
    return-void

    .line 674
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getZOrder()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getZOrder()I

    move-result v2

    if-le v1, v2, :cond_2

    .line 675
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->getZOrder()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Core/Label;->setZOrder(I)V

    .line 676
    :cond_2
    if-ne v0, p1, :cond_3

    .line 673
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public touch(Landroid/view/MotionEvent;Z)V
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "isDrawerOpened"    # Z

    .prologue
    const/4 v10, 0x6

    const/4 v9, -0x1

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 384
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getViewTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/sec/android/mimage/photoretouching/util/QuramDrawUtil;->transformVirtualPreviewCoordinate(Landroid/view/MotionEvent;Landroid/graphics/Matrix;)V

    .line 387
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingX()I

    move-result v6

    int-to-float v6, v6

    sub-float v3, v5, v6

    .line 388
    .local v3, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v6}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->getPreviewPaddingY()I

    move-result v6

    int-to-float v6, v6

    sub-float v4, v5, v6

    .line 389
    .local v4, "y":F
    const/4 v1, 0x1

    .line 390
    .local v1, "ret":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 507
    :goto_0
    return-void

    .line 393
    :pswitch_0
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchPressed:Z

    .line 395
    iput v8, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    .line 396
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    if-eq v5, v9, :cond_0

    .line 398
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aget-object v5, v5, v6

    invoke-virtual {v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/Label;->InitMoveObject(FF)I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    .line 399
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    if-nez v5, :cond_0

    .line 400
    iput v9, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    .line 403
    :cond_0
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    if-ge v5, v7, :cond_4

    .line 405
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    invoke-interface {v5, v7}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;->setVisibleMenu(Z)V

    .line 407
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelCount:I

    add-int/lit8 v0, v5, -0x1

    .local v0, "i":I
    :goto_1
    if-gez v0, :cond_2

    .line 459
    .end local v0    # "i":I
    :cond_1
    :goto_2
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->setHandlerIconPressed(I)V

    goto :goto_0

    .line 409
    .restart local v0    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    aget-object v5, v5, v0

    invoke-virtual {v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/Label;->InitMoveObject(FF)I

    move-result v5

    iput v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    .line 410
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    if-lt v5, v7, :cond_3

    .line 412
    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    .line 414
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->setLabelOrdering(I)V

    .line 416
    if-eqz p2, :cond_1

    .line 417
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;->closeDecorationDrawer()V

    goto :goto_2

    .line 407
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 430
    .end local v0    # "i":I
    :cond_4
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    invoke-virtual {p0, v5}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->setLabelOrdering(I)V

    .line 432
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    invoke-interface {v5, v8}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;->setVisibleMenu(Z)V

    .line 434
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    if-ne v5, v10, :cond_5

    .line 436
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mDeletePressed:Z

    goto :goto_2

    .line 441
    :cond_5
    if-eqz p2, :cond_1

    .line 442
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    invoke-interface {v5}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;->closeDecorationDrawer()V

    goto :goto_2

    .line 463
    :pswitch_1
    iput-boolean v7, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchPressed:Z

    .line 465
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    if-eq v5, v9, :cond_6

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    if-eq v5, v10, :cond_6

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aget-object v5, v5, v6

    if-eqz v5, :cond_6

    .line 467
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    invoke-interface {v5, v8}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;->setVisibleMenu(Z)V

    .line 468
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aget-object v5, v5, v6

    invoke-virtual {v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/Label;->StartMoveObject(FF)V

    .line 471
    :cond_6
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    .line 472
    .local v2, "touchType":I
    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mDeletePressed:Z

    if-eqz v5, :cond_7

    .line 475
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    if-eq v5, v9, :cond_7

    .line 476
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aget-object v5, v5, v6

    invoke-virtual {v5, v3, v4}, Lcom/sec/android/mimage/photoretouching/Core/Label;->InitMoveObject(FF)I

    move-result v2

    .line 477
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sj, SE - touch() - MOVE - touchType : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogD(Ljava/lang/String;)V

    .line 478
    if-eq v2, v10, :cond_7

    .line 479
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mDeletePressed:Z

    .line 485
    :cond_7
    invoke-direct {p0, v2}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->setHandlerIconPressed(I)V

    goto/16 :goto_0

    .line 490
    .end local v2    # "touchType":I
    :pswitch_2
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchPressed:Z

    .line 492
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabelCallbak:Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;

    invoke-interface {v5, v7}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect$OnMyLabelCallback;->setVisibleMenu(Z)V

    .line 493
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    if-eq v5, v9, :cond_8

    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    if-eq v5, v10, :cond_8

    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aget-object v5, v5, v6

    if-eqz v5, :cond_8

    .line 495
    iget-object v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mLabel:[Lcom/sec/android/mimage/photoretouching/Core/Label;

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mCurrentLabelIndex:I

    aget-object v5, v5, v6

    invoke-virtual {v5}, Lcom/sec/android/mimage/photoretouching/Core/Label;->EndMoveObject()V

    .line 498
    :cond_8
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    if-ne v5, v10, :cond_9

    iget-boolean v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mDeletePressed:Z

    if-eqz v5, :cond_9

    .line 500
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->deleteLabel()V

    .line 502
    :cond_9
    iput-boolean v8, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mDeletePressed:Z

    .line 503
    iget v5, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mTouchType:I

    invoke-direct {p0, v5}, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->setHandlerIconPressed(I)V

    goto/16 :goto_0

    .line 390
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public updateOriginalBuffer([I)V
    .locals 1
    .param p1, "prevOriginalBuf"    # [I

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->updateOriginalBuffer([I)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Core/LabelEffect;->mImageData:Lcom/sec/android/mimage/photoretouching/Core/ImageData;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Core/ImageData;->setPreviewBuffer()V

    .line 296
    return-void
.end method
