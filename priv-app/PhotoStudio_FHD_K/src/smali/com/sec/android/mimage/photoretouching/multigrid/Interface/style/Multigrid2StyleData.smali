.class public Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;
.super Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;
.source "Multigrid2StyleData.java"


# static fields
.field private static final mCollagePreset0:[[F

.field private static final mCollagePreset1:[[F

.field private static final mCollagePreset2:[[F

.field private static final mCollagePreset3:[[F

.field private static final mCollagePreset4:[[F

.field private static final mCollagePreset5:[[F

.field private static final mPilePreset0_1to1:[[F

.field private static final mPilePreset0_9to16:[[F

.field private static final mPilePreset1_1to1:[[F

.field private static final mPilePreset1_9to16:[[F

.field private static final mPilePreset2_1to1:[[F

.field private static final mPilePreset2_9to16:[[F

.field private static final mPilePreset3_1to1:[[F

.field private static final mPilePreset3_9to16:[[F


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 10
    new-array v0, v6, [[F

    .line 11
    new-array v1, v3, [F

    .line 12
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p50:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 14
    new-array v1, v3, [F

    .line 15
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p50:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 8
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mCollagePreset0:[[F

    .line 21
    new-array v0, v6, [[F

    .line 22
    new-array v1, v3, [F

    .line 23
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p375:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 25
    new-array v1, v3, [F

    .line 26
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p375:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 19
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mCollagePreset2:[[F

    .line 32
    new-array v0, v6, [[F

    .line 33
    new-array v1, v3, [F

    .line 34
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p625:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 36
    new-array v1, v3, [F

    .line 37
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p625:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 30
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mCollagePreset4:[[F

    .line 43
    new-array v0, v6, [[F

    .line 44
    new-array v1, v3, [F

    .line 45
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p50:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 47
    new-array v1, v3, [F

    .line 48
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p50:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 41
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mCollagePreset1:[[F

    .line 54
    new-array v0, v6, [[F

    .line 55
    new-array v1, v3, [F

    .line 56
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p375:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 58
    new-array v1, v3, [F

    .line 59
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p375:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 52
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mCollagePreset3:[[F

    .line 65
    new-array v0, v6, [[F

    .line 66
    new-array v1, v3, [F

    .line 67
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p625:F

    aput v2, v1, v7

    aput-object v1, v0, v4

    .line 69
    new-array v1, v3, [F

    .line 70
    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p0:F

    aput v2, v1, v4

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p625:F

    aput v2, v1, v5

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v6

    sget v2, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->p100:F

    aput v2, v1, v7

    aput-object v1, v0, v5

    .line 63
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mCollagePreset5:[[F

    .line 75
    new-array v0, v6, [[F

    .line 76
    new-array v1, v3, [F

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    .line 77
    new-array v1, v3, [F

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    .line 74
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset0_1to1:[[F

    .line 81
    new-array v0, v6, [[F

    .line 82
    new-array v1, v3, [F

    fill-array-data v1, :array_2

    aput-object v1, v0, v4

    .line 83
    new-array v1, v3, [F

    fill-array-data v1, :array_3

    aput-object v1, v0, v5

    .line 80
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset1_1to1:[[F

    .line 87
    new-array v0, v6, [[F

    .line 88
    new-array v1, v3, [F

    fill-array-data v1, :array_4

    aput-object v1, v0, v4

    .line 89
    new-array v1, v3, [F

    fill-array-data v1, :array_5

    aput-object v1, v0, v5

    .line 86
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset2_1to1:[[F

    .line 93
    new-array v0, v6, [[F

    .line 94
    new-array v1, v3, [F

    fill-array-data v1, :array_6

    aput-object v1, v0, v4

    .line 95
    new-array v1, v3, [F

    fill-array-data v1, :array_7

    aput-object v1, v0, v5

    .line 92
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset3_1to1:[[F

    .line 99
    new-array v0, v6, [[F

    .line 100
    new-array v1, v3, [F

    fill-array-data v1, :array_8

    aput-object v1, v0, v4

    .line 101
    new-array v1, v3, [F

    fill-array-data v1, :array_9

    aput-object v1, v0, v5

    .line 98
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset0_9to16:[[F

    .line 105
    new-array v0, v6, [[F

    .line 106
    new-array v1, v3, [F

    fill-array-data v1, :array_a

    aput-object v1, v0, v4

    .line 107
    new-array v1, v3, [F

    fill-array-data v1, :array_b

    aput-object v1, v0, v5

    .line 104
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset1_9to16:[[F

    .line 111
    new-array v0, v6, [[F

    .line 112
    new-array v1, v3, [F

    fill-array-data v1, :array_c

    aput-object v1, v0, v4

    .line 113
    new-array v1, v3, [F

    fill-array-data v1, :array_d

    aput-object v1, v0, v5

    .line 110
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset2_9to16:[[F

    .line 117
    new-array v0, v6, [[F

    .line 118
    new-array v1, v3, [F

    fill-array-data v1, :array_e

    aput-object v1, v0, v4

    .line 119
    new-array v1, v3, [F

    fill-array-data v1, :array_f

    aput-object v1, v0, v5

    .line 116
    sput-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset3_9to16:[[F

    .line 120
    return-void

    .line 76
    :array_0
    .array-data 4
        0x3ec2de01    # 0.3806f
        0x3d4ccccd    # 0.05f
        0x3f8aa993    # 1.0833f
        0x3f5b089a    # 0.8556f
    .end array-data

    .line 77
    :array_1
    .array-data 4
        -0x4271de6a    # -0.0694f
        0x3e16bb99    # 0.1472f
        0x3f1a511a    # 0.6028f
        0x3f6eecc0    # 0.9333f
    .end array-data

    .line 82
    :array_2
    .array-data 4
        0x3eed844d    # 0.4639f
        0x3e85aee6    # 0.2611f
        0x3f7de00d    # 0.9917f
        0x3f2eecc0    # 0.6833f
    .end array-data

    .line 83
    :array_3
    .array-data 4
        0x3d889a02    # 0.0667f
        0x3e554c98    # 0.2083f
        0x3f02d773    # 0.5111f
        0x3f47d567    # 0.7806f
    .end array-data

    .line 88
    :array_4
    .array-data 4
        0x3d2acd9f    # 0.0417f
        0x3e82de01    # 0.2556f
        0x3ef05532    # 0.4694f
        0x3f4aacda    # 0.7917f
    .end array-data

    .line 89
    :array_5
    .array-data 4
        0x3efa511a    # 0.4889f
        0x3e871de7    # 0.2639f
        0x3f7779a7    # 0.9667f
        0x3f244674    # 0.6417f
    .end array-data

    .line 94
    :array_6
    .array-data 4
        0x3e000000    # 0.125f
        0x3ea66666    # 0.325f
        0x3ebe9100    # 0.3722f
        0x3f271de7    # 0.6528f
    .end array-data

    .line 95
    :array_7
    .array-data 4
        0x3f1c710d    # 0.6111f
        0x3ea66666    # 0.325f
        0x3f5c710d    # 0.8611f
        0x3f271de7    # 0.6528f
    .end array-data

    .line 100
    :array_8
    .array-data 4
        0x3e9f4880    # 0.3111f
        0x3ea594af    # 0.3234f
        0x3f99f55a    # 1.2028f
        0x3f679a6b    # 0.9047f
    .end array-data

    .line 101
    :array_9
    .array-data 4
        -0x4205bc02    # -0.1222f
        0x3d734d6a    # 0.0594f
        0x3f36c227    # 0.7139f
        0x3f1ecbfb    # 0.6203f
    .end array-data

    .line 106
    :array_a
    .array-data 4
        0x3ea38866    # 0.3194f
        0x3f053261    # 0.5203f
        0x3f85aee6    # 1.0444f
        0x3f59999a    # 0.85f
    .end array-data

    .line 107
    :array_b
    .array-data 4
        0x3b378034    # 0.0028f
        0x3e200d1b    # 0.1563f
        0x3f2aacda    # 0.6667f
        0x3f1ecbfb    # 0.6203f
    .end array-data

    .line 112
    :array_c
    .array-data 4
        0x3c63bcd3    # 0.0139f
        0x3e973190    # 0.2953f
        0x3f0b5dcc    # 0.5444f
        0x3f2c63f1    # 0.6734f
    .end array-data

    .line 113
    :array_d
    .array-data 4
        0x3efd21ff    # 0.4944f
        0x3e933333    # 0.2875f
        0x3f760aa6    # 0.9611f
        0x3f2068dc    # 0.6266f
    .end array-data

    .line 118
    :array_e
    .array-data 4
        0x3e888ce7    # 0.2667f
        0x3e39a6b5    # 0.1813f
        0x3f3bb98c    # 0.7333f
        0x3ec339c1    # 0.3813f
    .end array-data

    .line 119
    :array_f
    .array-data 4
        0x3e888ce7    # 0.2667f
        0x3f1ccccd    # 0.6125f
        0x3f3bb98c    # 0.7333f
        0x3f500000    # 0.8125f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;-><init>()V

    return-void
.end method

.method public static getDrawRects(II)[[F
    .locals 4
    .param p0, "style"    # I
    .param p1, "proportion"    # I

    .prologue
    const/high16 v3, 0x1e500000

    .line 126
    const/4 v0, 0x0

    .line 128
    .local v0, "returnPreset":[[F
    const/high16 v1, 0x1e200000

    const/4 v2, 0x2

    invoke-static {v2}, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/MultigridStyleData;->getMaxStyleNum(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    move-result p0

    .line 130
    packed-switch p0, :pswitch_data_0

    .line 201
    const/4 v0, 0x0

    .line 205
    :goto_0
    return-object v0

    .line 133
    :pswitch_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mCollagePreset0:[[F

    .line 134
    goto :goto_0

    .line 137
    :pswitch_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mCollagePreset1:[[F

    .line 138
    goto :goto_0

    .line 141
    :pswitch_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mCollagePreset2:[[F

    .line 142
    goto :goto_0

    .line 145
    :pswitch_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mCollagePreset3:[[F

    .line 146
    goto :goto_0

    .line 149
    :pswitch_4
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mCollagePreset4:[[F

    .line 150
    goto :goto_0

    .line 153
    :pswitch_5
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mCollagePreset5:[[F

    .line 154
    goto :goto_0

    .line 158
    :pswitch_6
    if-ne p1, v3, :cond_0

    .line 160
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset0_1to1:[[F

    .line 161
    goto :goto_0

    .line 164
    :cond_0
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset0_9to16:[[F

    .line 166
    goto :goto_0

    .line 169
    :pswitch_7
    if-ne p1, v3, :cond_1

    .line 171
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset1_1to1:[[F

    .line 172
    goto :goto_0

    .line 175
    :cond_1
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset1_9to16:[[F

    .line 177
    goto :goto_0

    .line 180
    :pswitch_8
    if-ne p1, v3, :cond_2

    .line 182
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset2_1to1:[[F

    .line 183
    goto :goto_0

    .line 186
    :cond_2
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset2_9to16:[[F

    .line 188
    goto :goto_0

    .line 191
    :pswitch_9
    if-ne p1, v3, :cond_3

    .line 193
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset3_1to1:[[F

    .line 194
    goto :goto_0

    .line 197
    :cond_3
    sget-object v0, Lcom/sec/android/mimage/photoretouching/multigrid/Interface/style/Multigrid2StyleData;->mPilePreset3_9to16:[[F

    .line 199
    goto :goto_0

    .line 130
    :pswitch_data_0
    .packed-switch 0x1e200000
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public getRotate(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 212
    const/4 v0, 0x0

    return v0
.end method
