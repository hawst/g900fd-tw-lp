.class public Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;
.super Ljava/lang/Object;
.source "AssistantLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;,
        Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantStickerInfo;,
        Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;,
        Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;,
        Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnStickerCallback;
    }
.end annotation


# static fields
.field public static final INIT_STEP:I = 0xf

.field public static final INIT_STEP_B:I = 0xf

.field public static final INIT_STEP_COLOR_USING_SEEKBAR:I = 0xf

.field public static final INIT_STEP_G:I = 0xa

.field public static final INIT_STEP_R:I = 0xf

.field public static final INIT_STEP_TWIRL_SPHERICITY:I = 0xf

.field public static final SEEKBAR_B:I = 0x6

.field public static final SEEKBAR_COLLAGE:I = 0x10

.field public static final SEEKBAR_COLLAGE_ROUNDNESS:I = 0x11

.field public static final SEEKBAR_COLLAGE_SPACEING:I = 0x12

.field public static final SEEKBAR_G:I = 0x5

.field public static final SEEKBAR_HUE:I = 0x2

.field public static final SEEKBAR_MAX:I = 0x14

.field public static final SEEKBAR_NONE:I = 0x0

.field public static final SEEKBAR_NORMAL:I = 0x1

.field public static final SEEKBAR_R:I = 0x4

.field public static final SEEKBAR_RGB:I = 0x3

.field public static final STICKER_BRUSH:I = 0x1001

.field public static final STICKER_COMIC:I = 0x1002

.field public static final STICKER_ICON:I = 0x1004

.field public static final STICKER_NONE:I = 0x1000

.field public static final STICKER_PAPER:I = 0x1003


# instance fields
.field private final DURATION:I

.field private mAnimationSet:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

.field private mAssistantLayout:Landroid/view/ViewGroup;

.field private mAssistantSeekbarList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantStickerInfo;

.field private mContext:Landroid/content/Context;

.field private mCurrentType:I

.field private mCurrentlayout:Landroid/view/View;

.field public mPageImage:[Landroid/widget/ImageView;

.field public mPageLayout:[Landroid/widget/LinearLayout;

.field private mSeekbar_b:Landroid/widget/SeekBar;

.field private mSeekbar_g:Landroid/widget/SeekBar;

.field private mSeekbar_hue:Landroid/widget/SeekBar;

.field private mSeekbar_normal:Landroid/widget/SeekBar;

.field private mSeekbar_r:Landroid/widget/SeekBar;

.field private mSeekbar_roundness:Landroid/widget/SeekBar;

.field private mSeekbar_spacing:Landroid/widget/SeekBar;

.field private mShowLayout:Z

.field private mStep:I

.field private mStep_b:I

.field private mStep_g:I

.field private mStickerAdapter:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

.field private mViewPager:Landroid/support/v4/view/ViewPager;

.field private seekbar_layout_collage:Landroid/widget/RelativeLayout;

.field private seekbar_layout_hue:Landroid/widget/LinearLayout;

.field private seekbar_layout_normal:Landroid/widget/LinearLayout;

.field private seekbar_layout_rgb:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1196
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    .line 1197
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantStickerInfo;

    .line 1198
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    .line 1199
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentType:I

    .line 1201
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    .line 1202
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    .line 1203
    const/16 v0, 0x258

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->DURATION:I

    .line 1207
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_collage:Landroid/widget/RelativeLayout;

    .line 1214
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_roundness:Landroid/widget/SeekBar;

    .line 1215
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_spacing:Landroid/widget/SeekBar;

    .line 1217
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep:I

    .line 1218
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep_g:I

    .line 1219
    iput v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep_b:I

    .line 1248
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 1249
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStickerAdapter:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    .line 1250
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    .line 1251
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    .line 1252
    iput-boolean v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mShowLayout:Z

    .line 1253
    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAnimationSet:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    .line 47
    iput-object p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    .line 48
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 1202
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)Z
    .locals 1

    .prologue
    .line 1252
    iget-boolean v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mShowLayout:Z

    return v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)I
    .locals 1

    .prologue
    .line 1199
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentType:I

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;I)V
    .locals 0

    .prologue
    .line 1056
    invoke-direct {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setIndicator(I)V

    return-void
.end method

.method private setIndicator(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 1057
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 1058
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 1063
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    aget-object v1, v1, p1

    if-eqz v1, :cond_0

    .line 1064
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    aget-object v1, v1, p1

    const v2, 0x7f020354

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1066
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 1059
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 1060
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    const v2, 0x7f020353

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1058
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setSeekbarContext(IIIIIII)V
    .locals 5
    .param p1, "type"    # I
    .param p2, "pval"    # I
    .param p3, "max"    # I
    .param p4, "pval1"    # I
    .param p5, "max1"    # I
    .param p6, "pval2"    # I
    .param p7, "max2"    # I

    .prologue
    const/16 v4, 0x8

    const v3, 0x7f020524

    const/4 v2, 0x0

    .line 556
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentType:I

    .line 558
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    .line 559
    const v1, 0x7f090020

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 558
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_normal:Landroid/widget/LinearLayout;

    .line 560
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    .line 561
    const v1, 0x7f09002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 560
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_rgb:Landroid/widget/LinearLayout;

    .line 562
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    .line 563
    const v1, 0x7f090028

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 562
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_hue:Landroid/widget/LinearLayout;

    .line 564
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    .line 565
    const v1, 0x7f090045

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 564
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_collage:Landroid/widget/RelativeLayout;

    .line 567
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_normal:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 568
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_hue:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 569
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_rgb:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 570
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_collage:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 572
    sparse-switch p1, :sswitch_data_0

    .line 634
    :cond_0
    :goto_0
    invoke-virtual {p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setVisibility(I)V

    .line 635
    return-void

    .line 574
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_normal:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 576
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    .line 577
    const v1, 0x7f090024

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    .line 576
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_normal:Landroid/widget/SeekBar;

    .line 578
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_normal:Landroid/widget/SeekBar;

    invoke-virtual {v0, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 579
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_normal:Landroid/widget/SeekBar;

    invoke-virtual {v0, p3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 580
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_normal:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 584
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_hue:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 586
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    .line 587
    const v1, 0x7f09002a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    .line 586
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_hue:Landroid/widget/SeekBar;

    .line 588
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_hue:Landroid/widget/SeekBar;

    invoke-virtual {v0, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 589
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_hue:Landroid/widget/SeekBar;

    invoke-virtual {v0, p3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 590
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_hue:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 594
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_rgb:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 595
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v1, 0x7f090031

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_r:Landroid/widget/SeekBar;

    .line 596
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_r:Landroid/widget/SeekBar;

    invoke-virtual {v0, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 597
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_r:Landroid/widget/SeekBar;

    invoke-virtual {v0, p3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 598
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_r:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 601
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v1, 0x7f090039

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_g:Landroid/widget/SeekBar;

    .line 602
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_g:Landroid/widget/SeekBar;

    invoke-virtual {v0, p4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 603
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_g:Landroid/widget/SeekBar;

    invoke-virtual {v0, p5}, Landroid/widget/SeekBar;->setMax(I)V

    .line 604
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_g:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 607
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v1, 0x7f090041

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_b:Landroid/widget/SeekBar;

    .line 608
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_b:Landroid/widget/SeekBar;

    invoke-virtual {v0, p6}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 609
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_b:Landroid/widget/SeekBar;

    invoke-virtual {v0, p7}, Landroid/widget/SeekBar;->setMax(I)V

    .line 610
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_b:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 615
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_collage:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 616
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v1, 0x7f09004a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_roundness:Landroid/widget/SeekBar;

    .line 617
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_roundness:Landroid/widget/SeekBar;

    if-eqz v0, :cond_1

    .line 619
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_roundness:Landroid/widget/SeekBar;

    invoke-virtual {v0, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 620
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_roundness:Landroid/widget/SeekBar;

    invoke-virtual {v0, p3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 625
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v1, 0x7f09004f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_spacing:Landroid/widget/SeekBar;

    .line 626
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_spacing:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 628
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_spacing:Landroid/widget/SeekBar;

    invoke-virtual {v0, p4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 629
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_spacing:Landroid/widget/SeekBar;

    invoke-virtual {v0, p5}, Landroid/widget/SeekBar;->setMax(I)V

    goto/16 :goto_0

    .line 572
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x10 -> :sswitch_3
    .end sparse-switch
.end method

.method private setSeekbarLeftButton(IILandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 11
    .param p1, "iconId"    # I
    .param p2, "type"    # I
    .param p3, "listener"    # Landroid/view/View$OnClickListener;
    .param p4, "rlistener"    # Landroid/view/View$OnClickListener;
    .param p5, "glistener"    # Landroid/view/View$OnClickListener;
    .param p6, "blistener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 640
    const/4 v3, 0x0

    .line 642
    .local v3, "icon":Landroid/widget/LinearLayout;
    const/4 v9, 0x2

    if-ne p2, v9, :cond_3

    .line 643
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f090029

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "icon":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 668
    .restart local v3    # "icon":Landroid/widget/LinearLayout;
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 669
    invoke-virtual {v3, p1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 670
    invoke-virtual {v3, p3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 672
    :cond_1
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f090021

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout;

    .line 673
    .local v4, "layout":Landroid/widget/FrameLayout;
    if-eqz v4, :cond_2

    .line 675
    invoke-virtual {v4, p3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 677
    :cond_2
    return-void

    .line 644
    .end local v4    # "layout":Landroid/widget/FrameLayout;
    :cond_3
    const/4 v9, 0x1

    if-ne p2, v9, :cond_4

    .line 646
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f090023

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "icon":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 647
    .restart local v3    # "icon":Landroid/widget/LinearLayout;
    goto :goto_0

    .line 649
    :cond_4
    const/4 v9, 0x3

    if-ne p2, v9, :cond_0

    .line 650
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    .line 651
    const v10, 0x7f090030

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 650
    check-cast v8, Landroid/widget/LinearLayout;

    .line 652
    .local v8, "rIcon":Landroid/widget/LinearLayout;
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    .line 653
    const v10, 0x7f090038

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 652
    check-cast v2, Landroid/widget/LinearLayout;

    .line 654
    .local v2, "gIcon":Landroid/widget/LinearLayout;
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    .line 655
    const v10, 0x7f090040

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 654
    check-cast v1, Landroid/widget/LinearLayout;

    .line 656
    .local v1, "bIcon":Landroid/widget/LinearLayout;
    invoke-virtual {v8, p1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 657
    invoke-virtual {v2, p1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 658
    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 660
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f09002e

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout;

    .line 661
    .local v7, "layout_r":Landroid/widget/FrameLayout;
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f090036

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout;

    .line 662
    .local v6, "layout_g":Landroid/widget/FrameLayout;
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f09003e

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout;

    .line 664
    .local v5, "layout_b":Landroid/widget/FrameLayout;
    invoke-virtual {v7, p4}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 665
    move-object/from16 v0, p5

    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 666
    move-object/from16 v0, p6

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method private setSeekbarListener(ILcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "seekbar_listener"    # Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;
    .param p3, "seekbar_listener1"    # Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;
    .param p4, "seekbar_listener2"    # Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .prologue
    .line 723
    sparse-switch p1, :sswitch_data_0

    .line 900
    :cond_0
    :goto_0
    return-void

    .line 725
    :sswitch_0
    if-eqz p2, :cond_0

    .line 727
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_normal:Landroid/widget/SeekBar;

    .line 728
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$3;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$3;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 749
    const-string v0, "setSeekbarListener"

    invoke-static {v0}, Lcom/sec/android/mimage/photoretouching/util/QuramUtil;->LogI(Ljava/lang/String;)V

    goto :goto_0

    .line 753
    :sswitch_1
    if-eqz p2, :cond_0

    .line 754
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_hue:Landroid/widget/SeekBar;

    .line 755
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$4;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$4;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    goto :goto_0

    .line 777
    :sswitch_2
    if-eqz p2, :cond_1

    .line 778
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_r:Landroid/widget/SeekBar;

    .line 779
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$5;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$5;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 799
    :cond_1
    if-eqz p3, :cond_2

    .line 800
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_g:Landroid/widget/SeekBar;

    .line 801
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$6;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$6;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 822
    :cond_2
    if-eqz p4, :cond_0

    .line 823
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_b:Landroid/widget/SeekBar;

    .line 824
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$7;

    invoke-direct {v1, p0, p4}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$7;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    goto :goto_0

    .line 848
    :sswitch_3
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_roundness:Landroid/widget/SeekBar;

    if-eqz v0, :cond_3

    .line 850
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_roundness:Landroid/widget/SeekBar;

    if-eqz v0, :cond_3

    .line 851
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_roundness:Landroid/widget/SeekBar;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$8;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$8;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 875
    :cond_3
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_spacing:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 877
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_spacing:Landroid/widget/SeekBar;

    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$9;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$9;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    goto :goto_0

    .line 723
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x10 -> :sswitch_3
    .end sparse-switch
.end method

.method private setSeekbarRightButton(IILandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 11
    .param p1, "iconId"    # I
    .param p2, "type"    # I
    .param p3, "listener"    # Landroid/view/View$OnClickListener;
    .param p4, "rlistener"    # Landroid/view/View$OnClickListener;
    .param p5, "glistener"    # Landroid/view/View$OnClickListener;
    .param p6, "blistener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 682
    const/4 v3, 0x0

    .line 684
    .local v3, "icon":Landroid/widget/LinearLayout;
    const/4 v9, 0x2

    if-ne p2, v9, :cond_3

    .line 685
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f09002b

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "icon":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 708
    .restart local v3    # "icon":Landroid/widget/LinearLayout;
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 709
    invoke-virtual {v3, p1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 710
    invoke-virtual {v3, p3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 712
    :cond_1
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f090025

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/FrameLayout;

    .line 713
    .local v4, "layout":Landroid/widget/FrameLayout;
    if-eqz v4, :cond_2

    .line 715
    invoke-virtual {v4, p3}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 717
    :cond_2
    return-void

    .line 686
    .end local v4    # "layout":Landroid/widget/FrameLayout;
    :cond_3
    const/4 v9, 0x1

    if-ne p2, v9, :cond_4

    .line 688
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f090027

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "icon":Landroid/widget/LinearLayout;
    check-cast v3, Landroid/widget/LinearLayout;

    .line 689
    .restart local v3    # "icon":Landroid/widget/LinearLayout;
    goto :goto_0

    .line 691
    :cond_4
    const/4 v9, 0x3

    if-ne p2, v9, :cond_0

    .line 692
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f090034

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 693
    .local v8, "rIcon":Landroid/widget/LinearLayout;
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f09003c

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 694
    .local v2, "gIcon":Landroid/widget/LinearLayout;
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f090044

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 696
    .local v1, "bIcon":Landroid/widget/LinearLayout;
    invoke-virtual {v8, p1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 697
    invoke-virtual {v2, p1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 698
    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 700
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f090032

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout;

    .line 701
    .local v7, "layout_r":Landroid/widget/FrameLayout;
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f09003a

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout;

    .line 702
    .local v6, "layout_g":Landroid/widget/FrameLayout;
    iget-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v10, 0x7f090042

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout;

    .line 704
    .local v5, "layout_b":Landroid/widget/FrameLayout;
    invoke-virtual {v7, p4}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 705
    move-object/from16 v0, p5

    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 706
    move-object/from16 v0, p6

    invoke-virtual {v5, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public addCollageBorderSeekbar(IIIILcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V
    .locals 2
    .param p1, "startPosRoundness"    # I
    .param p2, "maxRoundness"    # I
    .param p3, "startPosSpacing"    # I
    .param p4, "maxSpacing"    # I
    .param p5, "listenerRoundness"    # Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;
    .param p6, "listenerSpacing"    # Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .prologue
    .line 345
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)V

    .line 346
    .local v0, "aInfo":Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;
    const/16 v1, 0x10

    iput v1, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->assistantType:I

    .line 347
    iput p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarStartPos:I

    .line 348
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax:I

    .line 349
    iput-object p5, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 351
    iput p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarStartPos1:I

    .line 352
    iput p4, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax1:I

    .line 353
    iput-object p6, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 354
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    return-void
.end method

.method public addSeekbar(IIILandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;IILcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V
    .locals 2
    .param p1, "assistantType"    # I
    .param p2, "leftIconId"    # I
    .param p3, "rightIconId"    # I
    .param p4, "leftListener"    # Landroid/view/View$OnClickListener;
    .param p5, "rightListener"    # Landroid/view/View$OnClickListener;
    .param p6, "startPos"    # I
    .param p7, "seekBarMax"    # I
    .param p8, "listener"    # Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .prologue
    .line 289
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)V

    .line 290
    .local v0, "aInfo":Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;
    iput p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->assistantType:I

    .line 291
    iput p2, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->iconId:I

    .line 292
    iput p3, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->iconId2:I

    .line 293
    iput p6, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarStartPos:I

    .line 294
    iput p7, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax:I

    .line 295
    iput-object p8, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 296
    iput-object p4, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarLeftListener:Landroid/view/View$OnClickListener;

    .line 297
    iput-object p5, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarRightListener:Landroid/view/View$OnClickListener;

    .line 299
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 300
    return-void
.end method

.method public addSeekbarRGB(IIIIIIIILandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V
    .locals 3
    .param p1, "startPosr"    # I
    .param p2, "seekBarMaxr"    # I
    .param p3, "startPosg"    # I
    .param p4, "seekBarMaxg"    # I
    .param p5, "startPosb"    # I
    .param p6, "seekBarMaxb"    # I
    .param p7, "leftIconId"    # I
    .param p8, "rightIconId"    # I
    .param p9, "leftRListener"    # Landroid/view/View$OnClickListener;
    .param p10, "rightRListener"    # Landroid/view/View$OnClickListener;
    .param p11, "leftGListener"    # Landroid/view/View$OnClickListener;
    .param p12, "rightGListener"    # Landroid/view/View$OnClickListener;
    .param p13, "leftBListener"    # Landroid/view/View$OnClickListener;
    .param p14, "rightBListener"    # Landroid/view/View$OnClickListener;
    .param p15, "listener_r"    # Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;
    .param p16, "listener_g"    # Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;
    .param p17, "listener_b"    # Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .prologue
    .line 317
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;

    invoke-direct {v1, p0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)V

    .line 318
    .local v1, "aInfo":Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;
    const/4 v2, 0x3

    iput v2, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->assistantType:I

    .line 319
    iput p1, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarStartPos:I

    .line 320
    iput p2, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax:I

    .line 321
    iput p3, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarStartPos1:I

    .line 322
    iput p4, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax1:I

    .line 323
    iput p5, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarStartPos2:I

    .line 324
    iput p6, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax2:I

    .line 325
    iput p7, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->iconId:I

    .line 326
    iput p8, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->iconId2:I

    .line 327
    iput-object p9, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarRLeftListener:Landroid/view/View$OnClickListener;

    .line 328
    iput-object p10, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarRRightListener:Landroid/view/View$OnClickListener;

    .line 329
    iput-object p11, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarGLeftListener:Landroid/view/View$OnClickListener;

    .line 330
    iput-object p12, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarGRightListener:Landroid/view/View$OnClickListener;

    .line 331
    move-object/from16 v0, p13

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarBLeftListener:Landroid/view/View$OnClickListener;

    .line 332
    move-object/from16 v0, p14

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarBRightListener:Landroid/view/View$OnClickListener;

    .line 333
    move-object/from16 v0, p15

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 334
    move-object/from16 v0, p16

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 335
    move-object/from16 v0, p17

    iput-object v0, v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener2:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 337
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    return-void
.end method

.method public addSticker(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnStickerCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnStickerCallback;

    .prologue
    .line 303
    new-instance v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantStickerInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantStickerInfo;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)V

    .line 304
    .local v0, "aInfo":Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantStickerInfo;
    iput-object p1, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantStickerInfo;->stickerListener:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnStickerCallback;

    .line 306
    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantStickerInfo;

    .line 307
    return-void
.end method

.method public configurationChanged()V
    .locals 3

    .prologue
    .line 359
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const v2, 0x7f0900bb

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    .line 360
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 361
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 362
    .local v0, "vGroup":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 363
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 364
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 365
    return-void
.end method

.method public configurationChangedForCollage()V
    .locals 3

    .prologue
    .line 368
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const v2, 0x7f0900bb

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    .line 369
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 370
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 371
    .local v0, "vGroup":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 372
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 373
    :cond_0
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 374
    return-void
.end method

.method public destroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 252
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 253
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 261
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 262
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    .line 264
    .end local v1    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    .line 266
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    check-cast v2, Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 267
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 268
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    .line 270
    :cond_1
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_normal:Landroid/widget/LinearLayout;

    .line 271
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_hue:Landroid/widget/LinearLayout;

    .line 272
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_rgb:Landroid/widget/LinearLayout;

    .line 273
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_normal:Landroid/widget/SeekBar;

    .line 274
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_hue:Landroid/widget/SeekBar;

    .line 275
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_r:Landroid/widget/SeekBar;

    .line 276
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_g:Landroid/widget/SeekBar;

    .line 277
    iput-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_b:Landroid/widget/SeekBar;

    .line 278
    return-void

    .line 254
    .restart local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;

    .line 256
    .local v0, "aInfo":Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;
    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarLeftListener:Landroid/view/View$OnClickListener;

    .line 257
    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 258
    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarRightListener:Landroid/view/View$OnClickListener;

    .line 259
    iput-object v4, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->thumbnail:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getCurrnetType()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentType:I

    return v0
.end method

.method public incrementProgressBy(II)V
    .locals 1
    .param p1, "pval"    # I
    .param p2, "assistantType"    # I

    .prologue
    .line 498
    sparse-switch p2, :sswitch_data_0

    .line 531
    :cond_0
    :goto_0
    return-void

    .line 500
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_normal:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_normal:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    goto :goto_0

    .line 504
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_hue:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_hue:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    goto :goto_0

    .line 508
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_r:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_r:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    goto :goto_0

    .line 512
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_g:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_g:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    goto :goto_0

    .line 516
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_b:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 517
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_b:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    goto :goto_0

    .line 521
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_roundness:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 522
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_roundness:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    goto :goto_0

    .line 526
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_spacing:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_spacing:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    goto :goto_0

    .line 498
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x11 -> :sswitch_5
        0x12 -> :sswitch_6
    .end sparse-switch
.end method

.method public init()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    const v1, 0x7f0900bb

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    .line 52
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    .line 54
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 67
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->initStep()V

    .line 68
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 69
    return-void

    .line 59
    :sswitch_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentType:I

    .line 60
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03001b

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    goto :goto_0

    .line 63
    :sswitch_1
    const/16 v0, 0x1000

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentType:I

    .line 64
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/PhotoRetouching;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03001d

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    goto :goto_0

    .line 54
    nop

    :sswitch_data_0
    .sparse-switch
        0x15000000 -> :sswitch_0
        0x16000000 -> :sswitch_0
        0x18000000 -> :sswitch_0
        0x31100000 -> :sswitch_1
    .end sparse-switch
.end method

.method public initForCollage()V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;

    const v1, 0x7f0900bb

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/photoretouching/multigrid/MultiGridActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    .line 72
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentType:I

    .line 75
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03001b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->initStep()V

    .line 77
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 78
    return-void
.end method

.method public initSeekbar(I)V
    .locals 10
    .param p1, "assistantType"    # I

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 378
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v9, v0, :cond_1

    .line 403
    .end local v9    # "i":I
    :cond_0
    :goto_1
    return-void

    .line 380
    .restart local v9    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;

    .line 381
    .local v8, "aInfo":Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;
    iget v0, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->assistantType:I

    if-ne v0, p1, :cond_2

    .line 383
    iget v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep:I

    .line 384
    iget v3, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax:I

    iget v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep_g:I

    .line 385
    iget v5, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax1:I

    iget v6, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep_b:I

    .line 386
    iget v7, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax2:I

    move-object v0, p0

    move v1, p1

    .line 383
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setSeekbarContext(IIIIIII)V

    .line 387
    iget v0, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->assistantType:I

    .line 388
    iget-object v1, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    iget-object v2, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 389
    iget-object v3, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener2:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 387
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setSeekbarListener(ILcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V

    .line 390
    iget v1, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->iconId:I

    iget v2, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->assistantType:I

    .line 391
    iget-object v3, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarLeftListener:Landroid/view/View$OnClickListener;

    .line 392
    iget-object v4, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarRLeftListener:Landroid/view/View$OnClickListener;

    .line 393
    iget-object v5, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarGLeftListener:Landroid/view/View$OnClickListener;

    .line 394
    iget-object v6, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarBLeftListener:Landroid/view/View$OnClickListener;

    move-object v0, p0

    .line 390
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setSeekbarLeftButton(IILandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 395
    iget v1, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->iconId2:I

    iget v2, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->assistantType:I

    .line 396
    iget-object v3, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarRightListener:Landroid/view/View$OnClickListener;

    .line 397
    iget-object v4, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarRRightListener:Landroid/view/View$OnClickListener;

    .line 398
    iget-object v5, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarGRightListener:Landroid/view/View$OnClickListener;

    .line 399
    iget-object v6, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarBRightListener:Landroid/view/View$OnClickListener;

    move-object v0, p0

    .line 395
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setSeekbarRightButton(IILandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 378
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_0
.end method

.method public initSeekbar(IIII)V
    .locals 11
    .param p1, "assistantType"    # I
    .param p2, "step_1"    # I
    .param p3, "step_2"    # I
    .param p4, "step_3"    # I

    .prologue
    const/4 v10, 0x0

    .line 412
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 413
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v9, v0, :cond_1

    .line 433
    .end local v9    # "i":I
    :cond_0
    :goto_1
    return-void

    .line 414
    .restart local v9    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantSeekbarList:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;

    .line 415
    .local v8, "aInfo":Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;
    iget v0, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->assistantType:I

    if-ne v0, p1, :cond_2

    .line 416
    iget v3, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax:I

    .line 417
    iget v5, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax1:I

    .line 418
    iget v7, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarMax2:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, p3

    move v6, p4

    .line 416
    invoke-direct/range {v0 .. v7}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setSeekbarContext(IIIIIII)V

    .line 419
    iget v0, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->assistantType:I

    .line 420
    iget-object v1, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    iget-object v2, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener1:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 421
    iget-object v3, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarListener2:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;

    .line 419
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setSeekbarListener(ILcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnSeekbarCallback;)V

    .line 422
    iget v1, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->iconId:I

    iget v2, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->assistantType:I

    .line 423
    iget-object v4, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarRLeftListener:Landroid/view/View$OnClickListener;

    .line 424
    iget-object v5, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarGLeftListener:Landroid/view/View$OnClickListener;

    .line 425
    iget-object v6, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarBLeftListener:Landroid/view/View$OnClickListener;

    move-object v0, p0

    move-object v3, v10

    .line 422
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setSeekbarLeftButton(IILandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 426
    iget v1, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->iconId2:I

    iget v2, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->assistantType:I

    .line 427
    iget-object v4, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarRRightListener:Landroid/view/View$OnClickListener;

    .line 428
    iget-object v5, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarGRightListener:Landroid/view/View$OnClickListener;

    .line 429
    iget-object v6, v8, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantSeekbarInfo;->seekBarBRightListener:Landroid/view/View$OnClickListener;

    move-object v0, p0

    move-object v3, v10

    .line 426
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setSeekbarRightButton(IILandroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 413
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_0
.end method

.method public initStep()V
    .locals 2

    .prologue
    const/16 v1, 0xf

    .line 81
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep:I

    .line 82
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep_g:I

    .line 83
    iput v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep_b:I

    .line 84
    return-void
.end method

.method public isShown()Z
    .locals 2

    .prologue
    .line 146
    const/4 v0, 0x0

    .line 147
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 149
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 150
    const/4 v0, 0x1

    .line 152
    :cond_0
    return v0
.end method

.method public isVisibility()I
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    .line 142
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setCollageArrowPos()V
    .locals 4

    .prologue
    .line 1174
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_collage:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    .line 1176
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->seekbar_layout_collage:Landroid/widget/RelativeLayout;

    const v3, 0x7f090050

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1177
    .local v0, "arrowView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1179
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1181
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->isLandscape()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1183
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050016

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1190
    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1193
    .end local v0    # "arrowView":Landroid/view/View;
    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    return-void

    .line 1187
    .restart local v0    # "arrowView":Landroid/view/View;
    .restart local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    goto :goto_0
.end method

.method public setCurrentSeekbarProgress(II)V
    .locals 1
    .param p1, "pval"    # I
    .param p2, "assistantType"    # I

    .prologue
    .line 462
    sparse-switch p2, :sswitch_data_0

    .line 495
    :cond_0
    :goto_0
    return-void

    .line 464
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_normal:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_normal:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 469
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_hue:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 470
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_hue:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 473
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_r:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_r:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 477
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_g:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_g:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 481
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_b:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_b:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 486
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_roundness:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_roundness:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 491
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_spacing:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mSeekbar_spacing:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 462
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x11 -> :sswitch_5
        0x12 -> :sswitch_6
    .end sparse-switch
.end method

.method public setStickerContext(I)V
    .locals 10
    .param p1, "type"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 909
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentType:I

    .line 911
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStickerAdapter:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    if-eqz v3, :cond_0

    .line 912
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStickerAdapter:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->destroy()V

    .line 913
    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStickerAdapter:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    .line 916
    :cond_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v3, :cond_1

    .line 917
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->removeAllViews()V

    .line 918
    iput-object v9, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 921
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v4, 0x7f09006c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 922
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentlayout:Landroid/view/View;

    const v4, 0x7f09006d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 923
    .local v0, "iconLayout":Landroid/widget/FrameLayout;
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 925
    new-instance v3, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4, p1, p0}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;-><init>(Landroid/content/Context;ILcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)V

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStickerAdapter:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    .line 927
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStickerAdapter:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 929
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStickerAdapter:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->getCount()I

    move-result v3

    new-array v3, v3, [Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    .line 930
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStickerAdapter:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->getCount()I

    move-result v3

    new-array v3, v3, [Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    .line 932
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;

    invoke-direct {v1}, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;-><init>()V

    .line 933
    .local v1, "indicatorListener":Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;
    const/4 v2, 0x0

    .line 935
    .local v2, "temp":Landroid/widget/LinearLayout;
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStickerAdapter:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->getCount()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 1028
    :goto_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/mimage/photoretouching/Gui/listener/IndicatorListener;->initIndicatorListener([Landroid/widget/LinearLayout;Landroid/support/v4/view/ViewPager;)V

    .line 1030
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v4, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$10;

    invoke-direct {v4, p0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$10;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 1049
    invoke-direct {p0, v6}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setIndicator(I)V

    .line 1051
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStickerAdapter:Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;

    invoke-virtual {v3}, Lcom/sec/android/mimage/photoretouching/Core/StickerAdapter;->getCount()I

    move-result v3

    if-eqz v3, :cond_2

    .line 1052
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1054
    :cond_2
    return-void

    .line 937
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 938
    const v4, 0x7f030049

    .line 937
    invoke-virtual {v3, v4, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .end local v2    # "temp":Landroid/widget/LinearLayout;
    check-cast v2, Landroid/widget/LinearLayout;

    .line 939
    .restart local v2    # "temp":Landroid/widget/LinearLayout;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v6

    .line 940
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v6

    if-eqz v3, :cond_3

    .line 941
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v6

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 942
    :cond_3
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900d4

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v6

    goto :goto_0

    .line 945
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 946
    const v4, 0x7f03004a

    .line 945
    invoke-virtual {v3, v4, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .end local v2    # "temp":Landroid/widget/LinearLayout;
    check-cast v2, Landroid/widget/LinearLayout;

    .line 947
    .restart local v2    # "temp":Landroid/widget/LinearLayout;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v6

    .line 948
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v6

    if-eqz v3, :cond_4

    .line 949
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v6

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 950
    :cond_4
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900d4

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v6

    .line 952
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d5

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v5

    .line 953
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v5

    if-eqz v3, :cond_5

    .line 954
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v5

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 955
    :cond_5
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900d6

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v5

    goto/16 :goto_0

    .line 958
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 959
    const v4, 0x7f03004b

    .line 958
    invoke-virtual {v3, v4, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .end local v2    # "temp":Landroid/widget/LinearLayout;
    check-cast v2, Landroid/widget/LinearLayout;

    .line 960
    .restart local v2    # "temp":Landroid/widget/LinearLayout;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v6

    .line 961
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v6

    if-eqz v3, :cond_6

    .line 962
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v6

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 963
    :cond_6
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900d4

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v6

    .line 965
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d5

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v5

    .line 966
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v5

    if-eqz v3, :cond_7

    .line 967
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v5

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 968
    :cond_7
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900d6

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v5

    .line 970
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d7

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v7

    .line 971
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v7

    if-eqz v3, :cond_8

    .line 972
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v7

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 973
    :cond_8
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900d8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v7

    goto/16 :goto_0

    .line 976
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 977
    const v4, 0x7f03004c

    .line 976
    invoke-virtual {v3, v4, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .end local v2    # "temp":Landroid/widget/LinearLayout;
    check-cast v2, Landroid/widget/LinearLayout;

    .line 978
    .restart local v2    # "temp":Landroid/widget/LinearLayout;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v6

    .line 979
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v6

    if-eqz v3, :cond_9

    .line 980
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v6

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 981
    :cond_9
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900d4

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v6

    .line 983
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d5

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v5

    .line 984
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v5

    if-eqz v3, :cond_a

    .line 985
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v5

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 986
    :cond_a
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900d6

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v5

    .line 988
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d7

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v7

    .line 989
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v7

    if-eqz v3, :cond_b

    .line 990
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v7

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 991
    :cond_b
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900d8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v7

    .line 993
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d9

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v8

    .line 994
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v8

    if-eqz v3, :cond_c

    .line 995
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v8

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 996
    :cond_c
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900da

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v8

    goto/16 :goto_0

    .line 999
    :pswitch_4
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 1000
    const v4, 0x7f03004d

    .line 999
    invoke-virtual {v3, v4, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .end local v2    # "temp":Landroid/widget/LinearLayout;
    check-cast v2, Landroid/widget/LinearLayout;

    .line 1001
    .restart local v2    # "temp":Landroid/widget/LinearLayout;
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v6

    .line 1002
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v6

    if-eqz v3, :cond_d

    .line 1003
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v6

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1004
    :cond_d
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900d4

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v6

    .line 1006
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d5

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v5

    .line 1007
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v5

    if-eqz v3, :cond_e

    .line 1008
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v5

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1009
    :cond_e
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900d6

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v5

    .line 1011
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d7

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v7

    .line 1012
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v7

    if-eqz v3, :cond_f

    .line 1013
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v7

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1014
    :cond_f
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900d8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v7

    .line 1016
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const v3, 0x7f0900d9

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v8

    .line 1017
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v8

    if-eqz v3, :cond_10

    .line 1018
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v8

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1019
    :cond_10
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const v3, 0x7f0900da

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v8

    .line 1021
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const/4 v5, 0x4

    const v3, 0x7f0900db

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    aput-object v3, v4, v5

    .line 1022
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const/4 v4, 0x4

    aget-object v3, v3, v4

    if-eqz v3, :cond_11

    .line 1023
    iget-object v3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageLayout:[Landroid/widget/LinearLayout;

    const/4 v4, 0x4

    aget-object v3, v3, v4

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1024
    :cond_11
    iget-object v4, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mPageImage:[Landroid/widget/ImageView;

    const/4 v5, 0x4

    const v3, 0x7f0900dc

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    aput-object v3, v4, v5

    goto/16 :goto_0

    .line 935
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setStickerLongMode(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 551
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantStickerInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantStickerInfo;->stickerListener:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnStickerCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnStickerCallback;->setLongClickSticker(I)V

    .line 552
    return-void
.end method

.method public setStickerMode(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantStickerInfo:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantStickerInfo;

    iget-object v0, v0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$AssistantStickerInfo;->stickerListener:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnStickerCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$OnStickerCallback;->setSticker(I)V

    .line 548
    return-void
.end method

.method public setVisibility(I)V
    .locals 9
    .param p1, "visibility"    # I

    .prologue
    .line 156
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    .line 238
    :goto_0
    return-void

    .line 158
    :cond_0
    const/4 v0, 0x0

    .line 160
    .local v0, "ta":Landroid/view/animation/TranslateAnimation;
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 164
    :sswitch_0
    iget v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentType:I

    const/16 v2, 0x10

    if-ne v1, v2, :cond_1

    .line 166
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mShowLayout:Z

    .line 167
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 169
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$1;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$1;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    .line 182
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mShowLayout:Z

    .line 183
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 185
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;Z)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAnimationSet:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    .line 186
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAnimationSet:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 188
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .line 189
    .end local v0    # "ta":Landroid/view/animation/TranslateAnimation;
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 190
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 191
    const/4 v5, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    .line 192
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 188
    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 193
    .restart local v0    # "ta":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 194
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAnimationSet:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 195
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAnimationSet:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 196
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    new-instance v2, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$2;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$2;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    .line 223
    :sswitch_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mShowLayout:Z

    .line 224
    new-instance v1, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;-><init>(Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;Z)V

    iput-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAnimationSet:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    .line 225
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAnimationSet:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 227
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .line 228
    .end local v0    # "ta":Landroid/view/animation/TranslateAnimation;
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 229
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 230
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 231
    const/4 v7, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    .line 227
    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 232
    .restart local v0    # "ta":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 234
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAnimationSet:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    invoke-virtual {v1, v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 235
    iget-object v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAnimationSet:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 160
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public setVisibilityQuickly(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 245
    :goto_0
    return-void

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAssistantLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public showAssistantLayout(I)V
    .locals 2
    .param p1, "assistantType"    # I

    .prologue
    const/4 v1, 0x0

    .line 103
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mCurrentType:I

    .line 104
    invoke-static {}, Lcom/sec/android/mimage/photoretouching/ViewStatus;->getCurrentMode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 117
    :goto_0
    if-nez p1, :cond_1

    .line 119
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAnimationSet:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAnimationSet:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->getIsRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mAnimationSet:Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;

    invoke-virtual {v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager$LayoutAnimationSet;->cancel()V

    .line 124
    iput-boolean v1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mShowLayout:Z

    .line 127
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setVisibilityQuickly(I)V

    .line 133
    :goto_1
    return-void

    .line 110
    :sswitch_0
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->initSeekbar(I)V

    goto :goto_0

    .line 113
    :sswitch_1
    invoke-virtual {p0, p1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setStickerContext(I)V

    goto :goto_0

    .line 131
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->setVisibility(I)V

    goto :goto_1

    .line 104
    nop

    :sswitch_data_0
    .sparse-switch
        0x15000000 -> :sswitch_0
        0x16000000 -> :sswitch_0
        0x1e110000 -> :sswitch_0
        0x2c000000 -> :sswitch_0
        0x31100000 -> :sswitch_1
    .end sparse-switch
.end method

.method public updateCurrentCollageStep(II)V
    .locals 0
    .param p1, "currentRoundStep"    # I
    .param p2, "currentFrameStep"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep:I

    .line 98
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep_g:I

    .line 99
    return-void
.end method

.method public updateCurrentRGBStep(III)V
    .locals 0
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep:I

    .line 92
    iput p2, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep_g:I

    .line 93
    iput p3, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep_b:I

    .line 94
    return-void
.end method

.method public updateCurrentStep(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/sec/android/mimage/photoretouching/Interface/AssistantLayoutManager;->mStep:I

    .line 88
    return-void
.end method
