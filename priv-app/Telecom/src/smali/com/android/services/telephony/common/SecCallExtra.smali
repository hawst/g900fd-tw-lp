.class public Lcom/android/services/telephony/common/SecCallExtra;
.super Ljava/lang/Object;
.source "SecCallExtra.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/services/telephony/common/SecCallExtra;",
            ">;"
        }
    .end annotation
.end field

.field private static final DOMAIN_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final RESOLUTION_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TYPE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAssisted:I

.field private mCdnipNumber:Ljava/lang/String;

.field private mConnectionCallId:Ljava/lang/String;

.field private mDisconnectForCallBarring:I

.field private mDomain:I

.field private mEndEarly:I

.field private mFirstCallerForIMSConference:Ljava/lang/String;

.field private mGetAliveParticipantNumber:Ljava/lang/String;

.field private mHDIcon:I

.field private mHasBeenVideoCall:I

.field private mIMSConferenceCall:B

.field private mIMSConferenceStateChanged:I

.field private mIsAutorejectCall:I

.field private mIsEpdgCall:I

.field private mIsEpdgW2L:I

.field private mIsIpCall:I

.field private mIsRadNumberConverted:I

.field private mIsRedialCall:I

.field private mIsWaitingCall:I

.field private mLineCtrl:I

.field private mLogSeviceType:I

.field private mModifiedLogType:I

.field private mModifiedTimeMillis:J

.field private mNameForIMSConference:Ljava/lang/String;

.field private mNumberForIMSConference:Ljava/lang/String;

.field private mOutgoingCallMessage:Ljava/lang/String;

.field private mRadOriginalNumber:Ljava/lang/String;

.field private mReceivedCall:B

.field private mSubId:J

.field private mTelecomCallId:Ljava/lang/String;

.field private mType:I

.field private mVideoResolution:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 131
    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->builder()Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "VOICE"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "VIDEO_SHARE_TX"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "VIDEO_SHARE_RX"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "VIDEO"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "MODIFY_REQUEST"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "MODIFY_RECEIVE"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/android/services/telephony/common/SecCallExtra;->TYPE_MAP:Ljava/util/Map;

    .line 140
    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->builder()Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "CS"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "PS"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/android/services/telephony/common/SecCallExtra;->DOMAIN_MAP:Ljava/util/Map;

    .line 145
    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->builder()Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "QCIF"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "QVGA"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "VGA"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "VGALAND"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "CIF"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "HD720"

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/android/services/telephony/common/SecCallExtra;->RESOLUTION_MAP:Ljava/util/Map;

    .line 565
    new-instance v0, Lcom/android/services/telephony/common/SecCallExtra$1;

    invoke-direct {v0}, Lcom/android/services/telephony/common/SecCallExtra$1;-><init>()V

    sput-object v0, Lcom/android/services/telephony/common/SecCallExtra;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mTelecomCallId:Ljava/lang/String;

    .line 36
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mConnectionCallId:Ljava/lang/String;

    .line 38
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRedialCall:I

    .line 40
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsAutorejectCall:I

    .line 44
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mType:I

    .line 46
    iput v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDomain:I

    .line 48
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHDIcon:I

    .line 50
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgCall:I

    .line 52
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgW2L:I

    .line 54
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHasBeenVideoCall:I

    .line 56
    iput v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mVideoResolution:I

    .line 58
    iput-byte v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceCall:B

    .line 60
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mOutgoingCallMessage:Ljava/lang/String;

    .line 62
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mRadOriginalNumber:Ljava/lang/String;

    .line 64
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRadNumberConverted:I

    .line 66
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mCdnipNumber:Ljava/lang/String;

    .line 68
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNameForIMSConference:Ljava/lang/String;

    .line 70
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNumberForIMSConference:Ljava/lang/String;

    .line 72
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mFirstCallerForIMSConference:Ljava/lang/String;

    .line 74
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mGetAliveParticipantNumber:Ljava/lang/String;

    .line 76
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceStateChanged:I

    .line 78
    iput-wide v4, p0, Lcom/android/services/telephony/common/SecCallExtra;->mSubId:J

    .line 84
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsWaitingCall:I

    .line 86
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsIpCall:I

    .line 88
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLineCtrl:I

    .line 90
    iput-wide v4, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedTimeMillis:J

    .line 92
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLogSeviceType:I

    .line 94
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedLogType:I

    .line 96
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mEndEarly:I

    .line 98
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mAssisted:I

    .line 100
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDisconnectForCallBarring:I

    .line 187
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mTelecomCallId:Ljava/lang/String;

    .line 36
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mConnectionCallId:Ljava/lang/String;

    .line 38
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRedialCall:I

    .line 40
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsAutorejectCall:I

    .line 44
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mType:I

    .line 46
    iput v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDomain:I

    .line 48
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHDIcon:I

    .line 50
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgCall:I

    .line 52
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgW2L:I

    .line 54
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHasBeenVideoCall:I

    .line 56
    iput v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mVideoResolution:I

    .line 58
    iput-byte v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceCall:B

    .line 60
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mOutgoingCallMessage:Ljava/lang/String;

    .line 62
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mRadOriginalNumber:Ljava/lang/String;

    .line 64
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRadNumberConverted:I

    .line 66
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mCdnipNumber:Ljava/lang/String;

    .line 68
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNameForIMSConference:Ljava/lang/String;

    .line 70
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNumberForIMSConference:Ljava/lang/String;

    .line 72
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mFirstCallerForIMSConference:Ljava/lang/String;

    .line 74
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mGetAliveParticipantNumber:Ljava/lang/String;

    .line 76
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceStateChanged:I

    .line 78
    iput-wide v4, p0, Lcom/android/services/telephony/common/SecCallExtra;->mSubId:J

    .line 84
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsWaitingCall:I

    .line 86
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsIpCall:I

    .line 88
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLineCtrl:I

    .line 90
    iput-wide v4, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedTimeMillis:J

    .line 92
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLogSeviceType:I

    .line 94
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedLogType:I

    .line 96
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mEndEarly:I

    .line 98
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mAssisted:I

    .line 100
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDisconnectForCallBarring:I

    .line 554
    invoke-virtual {p0, p1}, Lcom/android/services/telephony/common/SecCallExtra;->readFromParcel(Landroid/os/Parcel;)V

    .line 555
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/android/services/telephony/common/SecCallExtra;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/android/services/telephony/common/SecCallExtra;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mTelecomCallId:Ljava/lang/String;

    .line 36
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mConnectionCallId:Ljava/lang/String;

    .line 38
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRedialCall:I

    .line 40
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsAutorejectCall:I

    .line 44
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mType:I

    .line 46
    iput v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDomain:I

    .line 48
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHDIcon:I

    .line 50
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgCall:I

    .line 52
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgW2L:I

    .line 54
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHasBeenVideoCall:I

    .line 56
    iput v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mVideoResolution:I

    .line 58
    iput-byte v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceCall:B

    .line 60
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mOutgoingCallMessage:Ljava/lang/String;

    .line 62
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mRadOriginalNumber:Ljava/lang/String;

    .line 64
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRadNumberConverted:I

    .line 66
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mCdnipNumber:Ljava/lang/String;

    .line 68
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNameForIMSConference:Ljava/lang/String;

    .line 70
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNumberForIMSConference:Ljava/lang/String;

    .line 72
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mFirstCallerForIMSConference:Ljava/lang/String;

    .line 74
    iput-object v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mGetAliveParticipantNumber:Ljava/lang/String;

    .line 76
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceStateChanged:I

    .line 78
    iput-wide v4, p0, Lcom/android/services/telephony/common/SecCallExtra;->mSubId:J

    .line 84
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsWaitingCall:I

    .line 86
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsIpCall:I

    .line 88
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLineCtrl:I

    .line 90
    iput-wide v4, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedTimeMillis:J

    .line 92
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLogSeviceType:I

    .line 94
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedLogType:I

    .line 96
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mEndEarly:I

    .line 98
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mAssisted:I

    .line 100
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDisconnectForCallBarring:I

    .line 190
    iget-object v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mTelecomCallId:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mTelecomCallId:Ljava/lang/String;

    .line 191
    iget-object v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mConnectionCallId:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mConnectionCallId:Ljava/lang/String;

    .line 192
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mIsRedialCall:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRedialCall:I

    .line 193
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mIsAutorejectCall:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsAutorejectCall:I

    .line 194
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mType:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mType:I

    .line 195
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mDomain:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDomain:I

    .line 196
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mHDIcon:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHDIcon:I

    .line 197
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgCall:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgCall:I

    .line 198
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgW2L:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgW2L:I

    .line 199
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mVideoResolution:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mVideoResolution:I

    .line 200
    iget-byte v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceCall:B

    iput-byte v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceCall:B

    .line 201
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceStateChanged:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceStateChanged:I

    .line 202
    iget-object v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mNameForIMSConference:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNameForIMSConference:Ljava/lang/String;

    .line 203
    iget-object v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mNumberForIMSConference:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNumberForIMSConference:Ljava/lang/String;

    .line 204
    iget-object v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mGetAliveParticipantNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mGetAliveParticipantNumber:Ljava/lang/String;

    .line 205
    iget-object v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mFirstCallerForIMSConference:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mFirstCallerForIMSConference:Ljava/lang/String;

    .line 206
    iget-byte v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mReceivedCall:B

    iput-byte v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mReceivedCall:B

    .line 207
    iget-object v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mOutgoingCallMessage:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mOutgoingCallMessage:Ljava/lang/String;

    .line 208
    iget-object v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mRadOriginalNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mRadOriginalNumber:Ljava/lang/String;

    .line 209
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mIsRadNumberConverted:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRadNumberConverted:I

    .line 210
    iget-object v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mCdnipNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mCdnipNumber:Ljava/lang/String;

    .line 211
    iget-wide v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mSubId:J

    iput-wide v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mSubId:J

    .line 212
    iget-wide v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedTimeMillis:J

    iput-wide v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedTimeMillis:J

    .line 213
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mLogSeviceType:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLogSeviceType:I

    .line 214
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedLogType:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedLogType:I

    .line 215
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mEndEarly:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mEndEarly:I

    .line 216
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mDisconnectForCallBarring:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDisconnectForCallBarring:I

    .line 217
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mHasBeenVideoCall:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHasBeenVideoCall:I

    .line 218
    iget v0, p1, Lcom/android/services/telephony/common/SecCallExtra;->mAssisted:I

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mAssisted:I

    .line 219
    return-void
.end method

.method public static createSecCallExtraBundle(Lcom/android/services/telephony/common/SecCallExtra;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 156
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 157
    const-string v1, "sec_call_extra"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 158
    return-object v0
.end method

.method public static getSecCallExtra(Landroid/os/Bundle;)Lcom/android/services/telephony/common/SecCallExtra;
    .locals 1

    .prologue
    .line 179
    if-nez p0, :cond_0

    .line 180
    const/4 v0, 0x0

    .line 183
    :goto_0
    return-object v0

    .line 182
    :cond_0
    const-class v0, Lcom/android/services/telephony/common/SecCallExtra;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 183
    const-string v0, "sec_call_extra"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/common/SecCallExtra;

    goto :goto_0
.end method

.method public static setSecCallExtraToBundle(Landroid/os/Bundle;Lcom/android/services/telephony/common/SecCallExtra;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 168
    if-nez p1, :cond_0

    .line 169
    const/4 p0, 0x0

    .line 175
    :goto_0
    return-object p0

    .line 171
    :cond_0
    if-nez p0, :cond_1

    .line 172
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 174
    :cond_1
    const-string v0, "sec_call_extra"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method


# virtual methods
.method public final IsIPCall()Z
    .locals 1

    .prologue
    .line 265
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsIpCall:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 559
    const/4 v0, 0x0

    return v0
.end method

.method public final getCdnipNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mCdnipNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final getDomain()I
    .locals 1

    .prologue
    .line 286
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDomain:I

    return v0
.end method

.method public final getEpdgCall()Z
    .locals 1

    .prologue
    .line 301
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgCall:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getEpdgW2L()Z
    .locals 1

    .prologue
    .line 309
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgW2L:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getHDIcon()I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHDIcon:I

    return v0
.end method

.method public final getHasBeenVideoCall()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 467
    iget v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHasBeenVideoCall:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getLogServiceType()I
    .locals 1

    .prologue
    .line 435
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLogSeviceType:I

    return v0
.end method

.method public final getModifiedLogType()I
    .locals 1

    .prologue
    .line 443
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedLogType:I

    return v0
.end method

.method public final getModifiedTimeMillis()J
    .locals 2

    .prologue
    .line 427
    iget-wide v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedTimeMillis:J

    return-wide v0
.end method

.method public final getRadOriginalNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mRadOriginalNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final getTelecomCallId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mTelecomCallId:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()I
    .locals 1

    .prologue
    .line 279
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mType:I

    return v0
.end method

.method public final isAutoRejectCall()Z
    .locals 1

    .prologue
    .line 246
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsAutorejectCall:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final isIMSConferenceCall()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 328
    iget-byte v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceCall:B

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isLineCtrl()Z
    .locals 1

    .prologue
    .line 269
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLineCtrl:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final isRadConvertNumber()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 363
    iget v1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRadNumberConverted:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final readFromParcel(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 519
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mTelecomCallId:Ljava/lang/String;

    .line 520
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mConnectionCallId:Ljava/lang/String;

    .line 521
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRedialCall:I

    .line 522
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsAutorejectCall:I

    .line 523
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsWaitingCall:I

    .line 524
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsIpCall:I

    .line 525
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLineCtrl:I

    .line 526
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mType:I

    .line 527
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDomain:I

    .line 528
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHDIcon:I

    .line 529
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgCall:I

    .line 530
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgW2L:I

    .line 531
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mVideoResolution:I

    .line 532
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceCall:B

    .line 533
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceStateChanged:I

    .line 534
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNameForIMSConference:Ljava/lang/String;

    .line 535
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNumberForIMSConference:Ljava/lang/String;

    .line 536
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mGetAliveParticipantNumber:Ljava/lang/String;

    .line 537
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mFirstCallerForIMSConference:Ljava/lang/String;

    .line 538
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mReceivedCall:B

    .line 539
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mOutgoingCallMessage:Ljava/lang/String;

    .line 540
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mRadOriginalNumber:Ljava/lang/String;

    .line 541
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRadNumberConverted:I

    .line 542
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mCdnipNumber:Ljava/lang/String;

    .line 543
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mSubId:J

    .line 544
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedTimeMillis:J

    .line 545
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLogSeviceType:I

    .line 546
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedLogType:I

    .line 547
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mEndEarly:I

    .line 548
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDisconnectForCallBarring:I

    .line 549
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHasBeenVideoCall:I

    .line 550
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mAssisted:I

    .line 551
    return-void
.end method

.method public final setAssisted(I)V
    .locals 0

    .prologue
    .line 475
    iput p1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mAssisted:I

    .line 476
    return-void
.end method

.method public final setConnectionCallId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mConnectionCallId:Ljava/lang/String;

    .line 231
    return-void
.end method

.method public final setDisconnectForCallBarring(Z)V
    .locals 1

    .prologue
    .line 455
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDisconnectForCallBarring:I

    .line 456
    return-void
.end method

.method public final setDomain(I)V
    .locals 0

    .prologue
    .line 283
    iput p1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDomain:I

    .line 284
    return-void
.end method

.method public final setEndEarly(Z)V
    .locals 1

    .prologue
    .line 447
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mEndEarly:I

    .line 448
    return-void
.end method

.method public final setHDIcon(I)V
    .locals 0

    .prologue
    .line 290
    iput p1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHDIcon:I

    .line 291
    return-void
.end method

.method public final setIMSConferenceCall(B)V
    .locals 0

    .prologue
    .line 324
    iput-byte p1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceCall:B

    .line 325
    return-void
.end method

.method public final setIPCall(Z)V
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsIpCall:I

    .line 263
    return-void
.end method

.method public final setLineCtrl(Z)V
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLineCtrl:I

    .line 273
    return-void
.end method

.method public final setLogServiceType(I)V
    .locals 0

    .prologue
    .line 431
    iput p1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLogSeviceType:I

    .line 432
    return-void
.end method

.method public final setModifiedLogType(I)V
    .locals 0

    .prologue
    .line 439
    iput p1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedLogType:I

    .line 440
    return-void
.end method

.method public final setModifiedTimeMillis(J)V
    .locals 1

    .prologue
    .line 423
    iput-wide p1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedTimeMillis:J

    .line 424
    return-void
.end method

.method public final setOutgoingCallMessage(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mOutgoingCallMessage:Ljava/lang/String;

    .line 345
    return-void
.end method

.method public final setRadConvertNumber(Z)V
    .locals 1

    .prologue
    .line 359
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRadNumberConverted:I

    .line 360
    return-void

    .line 359
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setRadOriginalNumber(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mRadOriginalNumber:Ljava/lang/String;

    .line 352
    return-void
.end method

.method public final setReceivedCall(B)V
    .locals 1

    .prologue
    .line 336
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mReceivedCall:B

    .line 337
    return-void
.end method

.method public final setRedialCall(Z)V
    .locals 1

    .prologue
    .line 242
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRedialCall:I

    .line 243
    return-void
.end method

.method public final setSubId(J)V
    .locals 1

    .prologue
    .line 415
    iput-wide p1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mSubId:J

    .line 416
    return-void
.end method

.method public final setTelecomCallId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/android/services/telephony/common/SecCallExtra;->mTelecomCallId:Ljava/lang/String;

    .line 223
    return-void
.end method

.method public final setType(I)V
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mType:I

    .line 277
    return-void
.end method

.method public final setVideoResolution(I)V
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mVideoResolution:I

    .line 314
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 580
    invoke-static {p0}, Lcom/google/common/base/Equivalences;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mTelecomCallId"

    iget-object v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mTelecomCallId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mConnectionCallId"

    iget-object v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mConnectionCallId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mIsRedialCall"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRedialCall:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mIsAutorejectCall"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsAutorejectCall:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mIsWaitingCall"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsWaitingCall:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mIsIpCall"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsIpCall:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mLineCtrl"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLineCtrl:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mType"

    sget-object v2, Lcom/android/services/telephony/common/SecCallExtra;->TYPE_MAP:Ljava/util/Map;

    iget v3, p0, Lcom/android/services/telephony/common/SecCallExtra;->mType:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mDomain"

    sget-object v2, Lcom/android/services/telephony/common/SecCallExtra;->DOMAIN_MAP:Ljava/util/Map;

    iget v3, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDomain:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mHDIcon"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHDIcon:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mIsEpdgCall"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgCall:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mIsEpdgW2L"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgW2L:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mVideoResolution"

    sget-object v2, Lcom/android/services/telephony/common/SecCallExtra;->RESOLUTION_MAP:Ljava/util/Map;

    iget v3, p0, Lcom/android/services/telephony/common/SecCallExtra;->mVideoResolution:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mIMSConferenceCall"

    iget-byte v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceCall:B

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mIMSConferenceStateChanged"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceStateChanged:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mNameForIMSConference"

    iget-object v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNameForIMSConference:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mNumberForIMSConference"

    iget-object v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNumberForIMSConference:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mGetAliveParticipantNumber"

    iget-object v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mGetAliveParticipantNumber:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mFirstCallerForIMSConference"

    iget-object v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mFirstCallerForIMSConference:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mReceivedCall"

    iget-byte v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mReceivedCall:B

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mOutgoingCallMessage"

    iget-object v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mOutgoingCallMessage:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mRadOriginalNumber"

    iget-object v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mRadOriginalNumber:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mIsRadNumberConverted"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRadNumberConverted:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mCdnipNumber"

    iget-object v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mCdnipNumber:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mSubId"

    iget-wide v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mSubId:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mModifiedTimeMillis"

    iget-wide v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedTimeMillis:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mLogSeviceType"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLogSeviceType:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mModifiedLogType"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedLogType:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mEndEarly"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mEndEarly:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mDisconnectForCallBarring"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDisconnectForCallBarring:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mHasBeenVideoCall"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHasBeenVideoCall:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mAssisted"

    iget v2, p0, Lcom/android/services/telephony/common/SecCallExtra;->mAssisted:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 484
    iget-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mTelecomCallId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 485
    iget-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mConnectionCallId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 486
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRedialCall:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 487
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsAutorejectCall:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 488
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsWaitingCall:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 489
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsIpCall:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 490
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLineCtrl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 491
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 492
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDomain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 493
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHDIcon:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 494
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgCall:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 495
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsEpdgW2L:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 496
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mVideoResolution:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 497
    iget-byte v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceCall:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 498
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIMSConferenceStateChanged:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 499
    iget-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNameForIMSConference:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 500
    iget-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mNumberForIMSConference:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 501
    iget-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mGetAliveParticipantNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 502
    iget-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mFirstCallerForIMSConference:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 503
    iget-byte v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mReceivedCall:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 504
    iget-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mOutgoingCallMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 505
    iget-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mRadOriginalNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 506
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mIsRadNumberConverted:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 507
    iget-object v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mCdnipNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 508
    iget-wide v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mSubId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 509
    iget-wide v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedTimeMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 510
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mLogSeviceType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 511
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mModifiedLogType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 512
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mEndEarly:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 513
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mDisconnectForCallBarring:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 514
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mHasBeenVideoCall:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 515
    iget v0, p0, Lcom/android/services/telephony/common/SecCallExtra;->mAssisted:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 516
    return-void
.end method
