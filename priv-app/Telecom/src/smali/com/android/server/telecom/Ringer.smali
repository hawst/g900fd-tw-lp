.class public final Lcom/android/server/telecom/Ringer;
.super Lcom/android/server/telecom/CallsManager$CallsManagerListener;
.source "Ringer.java"

# interfaces
.implements Lcom/android/server/telecom/WiredHeadsetManager$Listener;


# instance fields
.field private final LIVE_CALL_STATES:[I

.field private flashHandler:Landroid/os/Handler;

.field private final mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

.field private mCallMotionMgr:Lcom/android/server/telecom/secutils/CallMotionMgr;

.field private mCallWaitingPlayer:Lcom/android/server/telecom/InCallTonePlayer;

.field private final mCallsManager:Lcom/android/server/telecom/CallsManager;

.field private final mContext:Landroid/content/Context;

.field private mCustomVibration:Lcom/android/server/telecom/secutils/CustomVibration;

.field public mFlashNoti:Lcom/android/server/telecom/secutils/FlashNoti;

.field private mHeadsetRingtonePlayer:Lcom/android/server/telecom/InCallTonePlayer;

.field private mIsVibrating:Z

.field private final mPlayerFactory:Lcom/android/server/telecom/InCallTonePlayer$Factory;

.field private final mRingingCalls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/server/telecom/Call;",
            ">;"
        }
    .end annotation
.end field

.field private mRingtonePlayer:Lcom/android/server/telecom/secutils/SecAsyncRingtonePlayer;

.field private final mVibrator:Landroid/os/Vibrator;

.field private final mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 53
    const/4 v0, 0x3

    new-array v0, v0, [J

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    aput-wide v2, v0, v1

    const/4 v1, 0x1

    aput-wide v4, v0, v1

    const/4 v1, 0x2

    aput-wide v4, v0, v1

    .line 59
    new-instance v0, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v0}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/media/AudioAttributes$Builder;->setContentType(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    return-void
.end method

.method constructor <init>(Lcom/android/server/telecom/CallAudioManager;Lcom/android/server/telecom/CallsManager;Lcom/android/server/telecom/InCallTonePlayer$Factory;Landroid/content/Context;Lcom/android/server/telecom/WiredHeadsetManager;)V
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;-><init>()V

    .line 75
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingingCalls:Ljava/util/List;

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/telecom/Ringer;->mIsVibrating:Z

    .line 97
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/server/telecom/Ringer;->LIVE_CALL_STATES:[I

    .line 122
    new-instance v0, Lcom/android/server/telecom/Ringer$1;

    invoke-direct {v0, p0}, Lcom/android/server/telecom/Ringer$1;-><init>(Lcom/android/server/telecom/Ringer;)V

    iput-object v0, p0, Lcom/android/server/telecom/Ringer;->flashHandler:Landroid/os/Handler;

    .line 108
    iput-object p1, p0, Lcom/android/server/telecom/Ringer;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    .line 109
    iput-object p2, p0, Lcom/android/server/telecom/Ringer;->mCallsManager:Lcom/android/server/telecom/CallsManager;

    .line 110
    iput-object p3, p0, Lcom/android/server/telecom/Ringer;->mPlayerFactory:Lcom/android/server/telecom/InCallTonePlayer$Factory;

    .line 111
    iput-object p4, p0, Lcom/android/server/telecom/Ringer;->mContext:Landroid/content/Context;

    .line 112
    iput-object p5, p0, Lcom/android/server/telecom/Ringer;->mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

    .line 113
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/WiredHeadsetManager;->addListener(Lcom/android/server/telecom/WiredHeadsetManager$Listener;)V

    .line 116
    new-instance v0, Landroid/os/SystemVibrator;

    invoke-direct {v0, p4}, Landroid/os/SystemVibrator;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/Ringer;->mVibrator:Landroid/os/Vibrator;

    .line 117
    new-instance v0, Lcom/android/server/telecom/secutils/SecAsyncRingtonePlayer;

    invoke-direct {v0, p4}, Lcom/android/server/telecom/secutils/SecAsyncRingtonePlayer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingtonePlayer:Lcom/android/server/telecom/secutils/SecAsyncRingtonePlayer;

    .line 118
    new-instance v0, Lcom/android/server/telecom/secutils/FlashNoti;

    invoke-direct {v0, p4}, Lcom/android/server/telecom/secutils/FlashNoti;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/Ringer;->mFlashNoti:Lcom/android/server/telecom/secutils/FlashNoti;

    .line 119
    new-instance v0, Lcom/android/server/telecom/secutils/CallMotionMgr;

    invoke-direct {v0, p4, p0}, Lcom/android/server/telecom/secutils/CallMotionMgr;-><init>(Landroid/content/Context;Lcom/android/server/telecom/Ringer;)V

    iput-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallMotionMgr:Lcom/android/server/telecom/secutils/CallMotionMgr;

    .line 120
    invoke-static {p4}, Lcom/android/server/telecom/secutils/CustomVibration;->getInstance(Landroid/content/Context;)Lcom/android/server/telecom/secutils/CustomVibration;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/telecom/Ringer;->mCustomVibration:Lcom/android/server/telecom/secutils/CustomVibration;

    .line 121
    return-void

    .line 97
    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x5
    .end array-data
.end method

.method private getTopMostUnansweredCall()Lcom/android/server/telecom/Call;
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingingCalls:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingingCalls:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    goto :goto_0
.end method

.method private onRespondedToIncomingCall(Lcom/android/server/telecom/Call;)V
    .locals 1

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->getTopMostUnansweredCall()Lcom/android/server/telecom/Call;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 206
    invoke-direct {p0, p1}, Lcom/android/server/telecom/Ringer;->removeFromUnansweredCall(Lcom/android/server/telecom/Call;)V

    .line 208
    :cond_0
    return-void
.end method

.method private removeFromUnansweredCall(Lcom/android/server/telecom/Call;)V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingingCalls:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 221
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->updateRinging()V

    .line 222
    return-void
.end method

.method private startHeadsetRingtone()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 441
    const-string v0, "start HeadsetRingtone."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 443
    invoke-static {}, Lcom/android/services/telephony/common/SystemDBInterface;->isTPhoneMode()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/services/telephony/common/SystemDBInterface;->isTPhoneRelaxMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 444
    const-string v0, "TPhone RelaxMode: Don\'t Start the headsetRington."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 457
    :cond_0
    :goto_0
    return-void

    .line 448
    :cond_1
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mHeadsetRingtonePlayer:Lcom/android/server/telecom/InCallTonePlayer;

    if-nez v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 451
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mPlayerFactory:Lcom/android/server/telecom/InCallTonePlayer$Factory;

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lcom/android/server/telecom/InCallTonePlayer$Factory;->createPlayer(I)Lcom/android/server/telecom/InCallTonePlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/telecom/Ringer;->mHeadsetRingtonePlayer:Lcom/android/server/telecom/InCallTonePlayer;

    .line 454
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mHeadsetRingtonePlayer:Lcom/android/server/telecom/InCallTonePlayer;

    invoke-virtual {v0}, Lcom/android/server/telecom/InCallTonePlayer;->startTone()V

    goto :goto_0
.end method

.method private startRingingOrCallWaiting()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 247
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallsManager:Lcom/android/server/telecom/CallsManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/CallsManager;->getForegroundCall()Lcom/android/server/telecom/Call;

    move-result-object v1

    .line 248
    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/telecom/CallsManager;->getCalls()Lcom/google/common/collect/ImmutableCollection;

    move-result-object v0

    .line 249
    const-string v2, "startRingingOrCallWaiting, foregroundCall: %s."

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-static {p0, v2, v3}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    invoke-static {}, Lcom/android/services/telephony/common/SystemDBInterface;->isTPhoneMode()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/android/services/telephony/common/SystemDBInterface;->isTPhoneRelaxMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 252
    const-string v0, "TPhone RelaxMode: Don\'t Start the ringtone and/or vibrator."

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    const-string v2, "usa_cdma_smc_fac_req"

    invoke-static {v2}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "ctc_cdma_smc_fac_req"

    invoke-static {v2}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 258
    :cond_2
    invoke-static {}, Lcom/android/server/telecom/operator/usa/TelecomExtension;->getSilence()Z

    move-result v2

    if-nez v2, :cond_0

    .line 260
    :cond_3
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->updateFlashNotification()V

    .line 262
    const-string v2, "voip_interworking"

    invoke-static {v2}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->isVoIPActivated()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 264
    const-string v0, "voip is activated - starting call waiting tone..."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p0, v0, v2}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 265
    if-eqz v1, :cond_0

    .line 266
    const-string v0, "Playing call-waiting tone."

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 269
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->stopCallWaiting()V

    .line 271
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallWaitingPlayer:Lcom/android/server/telecom/InCallTonePlayer;

    if-nez v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mPlayerFactory:Lcom/android/server/telecom/InCallTonePlayer$Factory;

    invoke-static {v0}, Lcom/android/server/telecom/secutils/TelecomUtils;->getWaitingTonePlayer(Lcom/android/server/telecom/InCallTonePlayer$Factory;)Lcom/android/server/telecom/InCallTonePlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallWaitingPlayer:Lcom/android/server/telecom/InCallTonePlayer;

    .line 273
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallWaitingPlayer:Lcom/android/server/telecom/InCallTonePlayer;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallWaitingPlayer:Lcom/android/server/telecom/InCallTonePlayer;

    invoke-virtual {v0}, Lcom/android/server/telecom/InCallTonePlayer;->startTone()V

    .line 275
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->startWaitingTone()V

    goto :goto_0

    .line 282
    :cond_4
    iget-object v2, p0, Lcom/android/server/telecom/Ringer;->mRingingCalls:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    const-string v2, "feature_multisim_dsda"

    invoke-static {v2}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/telecom/Ringer;->LIVE_CALL_STATES:[I

    invoke-virtual {v2, v3}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState([I)Lcom/android/server/telecom/Call;

    move-result-object v2

    if-nez v2, :cond_f

    .line 286
    :cond_5
    const-string v2, "feature_multisim_dsda"

    invoke-static {v2}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {v0}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->isDualCallRinging(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 289
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->stopRinging()V

    .line 290
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->updateFlashNotification()V

    .line 293
    :cond_6
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->stopCallWaiting()V

    .line 295
    invoke-virtual {v1}, Lcom/android/server/telecom/Call;->getContactUri()Landroid/net/Uri;

    move-result-object v2

    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mContext:Landroid/content/Context;

    const-string v3, "notification"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    if-eqz v2, :cond_7

    const-string v4, "android.people"

    new-array v5, v7, [Ljava/lang/String;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->matchesCallFilter(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 296
    const-string v0, "do not start ringtone for Zen mode"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 301
    :cond_8
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/telecom/secutils/RingTTS;->getInstance(Landroid/content/Context;)Lcom/android/server/telecom/secutils/RingTTS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/RingTTS;->checkTTS()V

    .line 303
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 306
    iget-object v2, p0, Lcom/android/server/telecom/Ringer;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    invoke-virtual {v2, v7}, Lcom/android/server/telecom/CallAudioManager;->setIsRinging(Z)V

    .line 307
    iget-object v2, p0, Lcom/android/server/telecom/Ringer;->mRingtonePlayer:Lcom/android/server/telecom/secutils/SecAsyncRingtonePlayer;

    invoke-virtual {v2}, Lcom/android/server/telecom/secutils/SecAsyncRingtonePlayer;->checkingRecoding()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 308
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    .line 309
    if-eq v1, v7, :cond_9

    if-nez v1, :cond_9

    invoke-virtual {v0, v8}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-lez v0, :cond_b

    :cond_9
    iget-boolean v0, p0, Lcom/android/server/telecom/Ringer;->mIsVibrating:Z

    if-nez v0, :cond_b

    .line 313
    const-string v0, "- starting vibrator...for voice recording"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCustomVibration:Lcom/android/server/telecom/secutils/CustomVibration;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCustomVibration:Lcom/android/server/telecom/secutils/CustomVibration;

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/CustomVibration;->isVibrating()Z

    move-result v0

    if-nez v0, :cond_a

    .line 317
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCustomVibration:Lcom/android/server/telecom/secutils/CustomVibration;

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/CustomVibration;->startVibration()V

    .line 319
    :cond_a
    iput-boolean v7, p0, Lcom/android/server/telecom/Ringer;->mIsVibrating:Z

    goto/16 :goto_0

    .line 321
    :cond_b
    const-string v0, "skipping ring because volume is zero for voice recording"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 326
    :cond_c
    invoke-virtual {v0, v8}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-lez v0, :cond_d

    .line 327
    const-string v0, "startRingingOrCallWaiting"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p0, v0, v2}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 334
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingtonePlayer:Lcom/android/server/telecom/secutils/SecAsyncRingtonePlayer;

    invoke-virtual {v1}, Lcom/android/server/telecom/Call;->getRingtone()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/telecom/secutils/SecAsyncRingtonePlayer;->play(Landroid/net/Uri;)V

    .line 350
    :goto_1
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCustomVibration:Lcom/android/server/telecom/secutils/CustomVibration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCustomVibration:Lcom/android/server/telecom/secutils/CustomVibration;

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/CustomVibration;->isVibrating()Z

    move-result v0

    if-nez v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCustomVibration:Lcom/android/server/telecom/secutils/CustomVibration;

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/CustomVibration;->startVibration()V

    goto/16 :goto_0

    .line 335
    :cond_d
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/WiredHeadsetManager;->isPluggedIn()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 337
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->startHeadsetRingtone()V

    goto :goto_1

    .line 339
    :cond_e
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    invoke-virtual {v0, v7}, Lcom/android/server/telecom/CallAudioManager;->setIsRinging(Z)V

    .line 340
    const-string v0, "startRingingOrCallWaiting, skipping because volume is 0"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 353
    :cond_f
    if-eqz v1, :cond_0

    .line 358
    if-eqz v1, :cond_0

    .line 359
    const-string v0, "Playing call-waiting tone."

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 362
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->stopRinging()V

    .line 364
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->isPlayingWaitingToneByNetwork()Z

    move-result v0

    if-nez v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallWaitingPlayer:Lcom/android/server/telecom/InCallTonePlayer;

    if-nez v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mPlayerFactory:Lcom/android/server/telecom/InCallTonePlayer$Factory;

    invoke-static {v0}, Lcom/android/server/telecom/secutils/TelecomUtils;->getWaitingTonePlayer(Lcom/android/server/telecom/InCallTonePlayer$Factory;)Lcom/android/server/telecom/InCallTonePlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallWaitingPlayer:Lcom/android/server/telecom/InCallTonePlayer;

    .line 367
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallWaitingPlayer:Lcom/android/server/telecom/InCallTonePlayer;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallWaitingPlayer:Lcom/android/server/telecom/InCallTonePlayer;

    invoke-virtual {v0}, Lcom/android/server/telecom/InCallTonePlayer;->startTone()V

    .line 369
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->startWaitingTone()V

    goto/16 :goto_0
.end method

.method private stopCallWaiting()V
    .locals 2

    .prologue
    .line 432
    const-string v0, "stop call waiting."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 433
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallWaitingPlayer:Lcom/android/server/telecom/InCallTonePlayer;

    if-eqz v0, :cond_0

    .line 434
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->stopWaitingTone()V

    .line 435
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallWaitingPlayer:Lcom/android/server/telecom/InCallTonePlayer;

    invoke-virtual {v0}, Lcom/android/server/telecom/InCallTonePlayer;->stopTone()V

    .line 436
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallWaitingPlayer:Lcom/android/server/telecom/InCallTonePlayer;

    .line 438
    :cond_0
    return-void
.end method

.method private stopHeadsetRingtone()V
    .locals 2

    .prologue
    .line 460
    const-string v0, "stop HeadsetRingtone."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 461
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mHeadsetRingtonePlayer:Lcom/android/server/telecom/InCallTonePlayer;

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mHeadsetRingtonePlayer:Lcom/android/server/telecom/InCallTonePlayer;

    invoke-virtual {v0}, Lcom/android/server/telecom/InCallTonePlayer;->stopTone()V

    .line 463
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/telecom/Ringer;->mHeadsetRingtonePlayer:Lcom/android/server/telecom/InCallTonePlayer;

    .line 465
    :cond_0
    return-void
.end method

.method private stopRinging()V
    .locals 5

    .prologue
    const/16 v4, 0x14

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 398
    const-string v0, "stopRinging"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 400
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingtonePlayer:Lcom/android/server/telecom/secutils/SecAsyncRingtonePlayer;

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/SecAsyncRingtonePlayer;->stop()V

    .line 402
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->stopHeadsetRingtone()V

    .line 403
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->flashHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->flashHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 405
    :cond_0
    const-string v0, "-stop flash.handler..."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 407
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mFlashNoti:Lcom/android/server/telecom/secutils/FlashNoti;

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/FlashNoti;->stopNotiFlash()V

    .line 408
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallMotionMgr:Lcom/android/server/telecom/secutils/CallMotionMgr;

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/CallMotionMgr;->stopFlashCallMotion()V

    .line 409
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallMotionMgr:Lcom/android/server/telecom/secutils/CallMotionMgr;

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/CallMotionMgr;->stopCallMotion()V

    .line 410
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/telecom/secutils/RingTTS;->getInstance(Landroid/content/Context;)Lcom/android/server/telecom/secutils/RingTTS;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/RingTTS;->stopTTS()V

    .line 411
    iget-boolean v0, p0, Lcom/android/server/telecom/Ringer;->mIsVibrating:Z

    if-eqz v0, :cond_1

    .line 412
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 413
    iput-boolean v3, p0, Lcom/android/server/telecom/Ringer;->mIsVibrating:Z

    .line 415
    :cond_1
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCustomVibration:Lcom/android/server/telecom/secutils/CustomVibration;

    if-eqz v0, :cond_2

    .line 416
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCustomVibration:Lcom/android/server/telecom/secutils/CustomVibration;

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/CustomVibration;->stopVibration()V

    .line 421
    :cond_2
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    invoke-virtual {v0, v3}, Lcom/android/server/telecom/CallAudioManager;->setIsRinging(Z)V

    .line 423
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 424
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-lez v0, :cond_3

    .line 427
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingtonePlayer:Lcom/android/server/telecom/secutils/SecAsyncRingtonePlayer;

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/SecAsyncRingtonePlayer;->returnOriginVolume()V

    .line 429
    :cond_3
    return-void
.end method

.method private updateFlashNotification()V
    .locals 4

    .prologue
    const/16 v1, 0x14

    .line 235
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->flashHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->flashHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->flashHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 239
    iget-object v1, p0, Lcom/android/server/telecom/Ringer;->flashHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 242
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallMotionMgr:Lcom/android/server/telecom/secutils/CallMotionMgr;

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/CallMotionMgr;->checkFlashCallMotion()V

    .line 243
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallMotionMgr:Lcom/android/server/telecom/secutils/CallMotionMgr;

    invoke-virtual {v0}, Lcom/android/server/telecom/secutils/CallMotionMgr;->checkCallMotion()V

    .line 244
    return-void
.end method

.method private updateRinging()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingingCalls:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->stopRinging()V

    .line 227
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->stopCallWaiting()V

    .line 231
    :goto_0
    return-void

    .line 229
    :cond_0
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->startRingingOrCallWaiting()V

    goto :goto_0
.end method


# virtual methods
.method public final onCallAdded(Lcom/android/server/telecom/Call;)V
    .locals 2

    .prologue
    .line 137
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->isIncoming()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 138
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingingCalls:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const-string v0, "New ringing call is already in list of unanswered calls"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->wtf(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingingCalls:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->updateRinging()V

    .line 144
    :cond_1
    return-void
.end method

.method public final onCallRemoved(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/android/server/telecom/Ringer;->removeFromUnansweredCall(Lcom/android/server/telecom/Call;)V

    .line 149
    return-void
.end method

.method public final onCallStateChanged(Lcom/android/server/telecom/Call;II)V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x4

    if-eq p3, v0, :cond_0

    .line 154
    invoke-direct {p0, p1}, Lcom/android/server/telecom/Ringer;->removeFromUnansweredCall(Lcom/android/server/telecom/Call;)V

    .line 156
    :cond_0
    return-void
.end method

.method public final onForegroundCallChanged(Lcom/android/server/telecom/Call;Lcom/android/server/telecom/Call;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingingCalls:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingingCalls:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    :cond_0
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->updateRinging()V

    .line 174
    :cond_1
    return-void
.end method

.method public final onIncomingCallAnswered(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/android/server/telecom/Ringer;->onRespondedToIncomingCall(Lcom/android/server/telecom/Call;)V

    .line 161
    return-void
.end method

.method public final onIncomingCallRejected(Lcom/android/server/telecom/Call;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/android/server/telecom/Ringer;->onRespondedToIncomingCall(Lcom/android/server/telecom/Call;)V

    .line 166
    return-void
.end method

.method public final onWiredHeadsetPluggedInChanged$25decb5(Z)V
    .locals 1

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->getTopMostUnansweredCall()Lcom/android/server/telecom/Call;

    move-result-object v0

    if-nez v0, :cond_0

    .line 185
    :goto_0
    return-void

    .line 180
    :cond_0
    if-eqz p1, :cond_1

    .line 181
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->startHeadsetRingtone()V

    goto :goto_0

    .line 183
    :cond_1
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->stopHeadsetRingtone()V

    goto :goto_0
.end method

.method public final restartRingingOrCallWaiting()V
    .locals 2

    .prologue
    .line 378
    const-string v0, "restartRingingOrCallWaiting"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 379
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->stopRinging()V

    .line 380
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->stopCallWaiting()V

    .line 381
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->stopHeadsetRingtone()V

    .line 382
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingingCalls:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 383
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->startRingingOrCallWaiting()V

    .line 385
    :cond_0
    return-void
.end method

.method public final silence()V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mCallsManager:Lcom/android/server/telecom/CallsManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/CallsManager;->getForegroundCall()Lcom/android/server/telecom/Call;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/android/server/telecom/Ringer;->mRingingCalls:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/android/server/telecom/Ringer;->mRingingCalls:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 197
    const-string v0, "S_RI"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/android/server/telecom/CallIdMapper;->sendSecBluetoothATCommand(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_0
    invoke-direct {p0}, Lcom/android/server/telecom/Ringer;->updateRinging()V

    .line 201
    return-void
.end method
