.class public final Lcom/android/server/telecom/TelecomApp;
.super Landroid/app/Application;
.source "TelecomApp.java"


# static fields
.field private static sInstance:Lcom/android/server/telecom/TelecomApp;


# instance fields
.field private autoCSP:Lcom/android/server/telecom/IAutoCSP;

.field private autoCSPConnection:Landroid/content/ServiceConnection;

.field private mCallsManager:Lcom/android/server/telecom/CallsManager;

.field private mMissedCallNotifier:Lcom/android/server/telecom/MissedCallNotifier;

.field private mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

.field private mRcsCapaObserver:Lcom/android/server/telecom/rcs/RcsCapaObserver;

.field private mRcsContentObserver:Lcom/android/server/telecom/rcs/RcsContentObserver;

.field private mRcsSessionObserver:Landroid/database/ContentObserver;

.field private mTelecomService:Lcom/android/server/telecom/TelecomServiceImpl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsCapaObserver:Lcom/android/server/telecom/rcs/RcsCapaObserver;

    .line 226
    new-instance v0, Lcom/android/server/telecom/TelecomApp$2;

    invoke-direct {v0, p0}, Lcom/android/server/telecom/TelecomApp$2;-><init>(Lcom/android/server/telecom/TelecomApp;)V

    iput-object v0, p0, Lcom/android/server/telecom/TelecomApp;->autoCSPConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$002(Lcom/android/server/telecom/TelecomApp;Lcom/android/server/telecom/IAutoCSP;)Lcom/android/server/telecom/IAutoCSP;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/android/server/telecom/TelecomApp;->autoCSP:Lcom/android/server/telecom/IAutoCSP;

    return-object p1
.end method

.method public static getInstance()Lcom/android/server/telecom/TelecomApp;
    .locals 2

    .prologue
    .line 210
    sget-object v0, Lcom/android/server/telecom/TelecomApp;->sInstance:Lcom/android/server/telecom/TelecomApp;

    if-nez v0, :cond_0

    .line 211
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No TelecomApp running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_0
    sget-object v0, Lcom/android/server/telecom/TelecomApp;->sInstance:Lcom/android/server/telecom/TelecomApp;

    return-object v0
.end method


# virtual methods
.method public final getAutoCSP()Lcom/android/server/telecom/IAutoCSP;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->autoCSP:Lcom/android/server/telecom/IAutoCSP;

    return-object v0
.end method

.method public final getMissedCallNotifier()Lcom/android/server/telecom/MissedCallNotifier;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mMissedCallNotifier:Lcom/android/server/telecom/MissedCallNotifier;

    return-object v0
.end method

.method public final getPhoneAccountRegistrar()Lcom/android/server/telecom/PhoneAccountRegistrar;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

    return-object v0
.end method

.method public final onCreate()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 105
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 107
    sput-object p0, Lcom/android/server/telecom/TelecomApp;->sInstance:Lcom/android/server/telecom/TelecomApp;

    .line 108
    invoke-static {p0}, Lcom/android/services/telephony/common/PhoneFeature;->makeFeature(Landroid/content/Context;)V

    .line 109
    invoke-static {p0}, Lcom/android/services/telephony/common/SystemDBInterface;->initialize(Landroid/content/Context;)V

    .line 111
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Lcom/android/server/telecom/MissedCallNotifier;

    invoke-direct {v0, p0}, Lcom/android/server/telecom/MissedCallNotifier;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mMissedCallNotifier:Lcom/android/server/telecom/MissedCallNotifier;

    .line 117
    new-instance v0, Lcom/android/server/telecom/PhoneAccountRegistrar;

    invoke-direct {v0, p0}, Lcom/android/server/telecom/PhoneAccountRegistrar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

    .line 119
    new-instance v0, Lcom/android/server/telecom/CallsManager;

    iget-object v1, p0, Lcom/android/server/telecom/TelecomApp;->mMissedCallNotifier:Lcom/android/server/telecom/MissedCallNotifier;

    iget-object v2, p0, Lcom/android/server/telecom/TelecomApp;->mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/server/telecom/CallsManager;-><init>(Landroid/content/Context;Lcom/android/server/telecom/MissedCallNotifier;Lcom/android/server/telecom/PhoneAccountRegistrar;)V

    iput-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mCallsManager:Lcom/android/server/telecom/CallsManager;

    .line 120
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mCallsManager:Lcom/android/server/telecom/CallsManager;

    invoke-static {v0}, Lcom/android/server/telecom/CallsManager;->initialize(Lcom/android/server/telecom/CallsManager;)V

    .line 122
    new-instance v0, Lcom/android/server/telecom/TelecomServiceImpl;

    iget-object v1, p0, Lcom/android/server/telecom/TelecomApp;->mMissedCallNotifier:Lcom/android/server/telecom/MissedCallNotifier;

    iget-object v2, p0, Lcom/android/server/telecom/TelecomApp;->mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

    iget-object v3, p0, Lcom/android/server/telecom/TelecomApp;->mCallsManager:Lcom/android/server/telecom/CallsManager;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/android/server/telecom/TelecomServiceImpl;-><init>(Lcom/android/server/telecom/MissedCallNotifier;Lcom/android/server/telecom/PhoneAccountRegistrar;Lcom/android/server/telecom/CallsManager;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mTelecomService:Lcom/android/server/telecom/TelecomServiceImpl;

    .line 124
    const-string v0, "telecom"

    iget-object v1, p0, Lcom/android/server/telecom/TelecomApp;->mTelecomService:Lcom/android/server/telecom/TelecomServiceImpl;

    invoke-static {v0, v1}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 127
    invoke-static {p0}, Lcom/android/server/telecom/BluetoothPhoneService;->start(Landroid/content/Context;)V

    .line 129
    invoke-static {p0}, Lcom/android/server/telecom/BluetoothVoIPService;->start(Landroid/content/Context;)V

    .line 131
    invoke-static {p0}, Lcom/android/server/telecom/LowBatteryManager;->init(Landroid/content/Context;)V

    .line 133
    :cond_0
    const-string v0, "ims_rcs"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 134
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsSessionObserver:Landroid/database/ContentObserver;

    if-nez v0, :cond_1

    .line 135
    new-instance v0, Lcom/android/server/telecom/TelecomApp$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/server/telecom/TelecomApp$1;-><init>(Lcom/android/server/telecom/TelecomApp;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsSessionObserver:Landroid/database/ContentObserver;

    .line 143
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/telecom/TelecomApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/services/telephony/common/RcsTransferConstants$Cs;->ACTIVE_SESSIONS_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/server/telecom/TelecomApp;->mRcsSessionObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 146
    :cond_2
    const-string v0, "usa_spr_roaming_feature"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 147
    invoke-static {}, Lcom/android/server/telecom/operator/usa/TelecomExtensionManager;->getInstance()Lcom/android/server/telecom/operator/usa/TelecomExtensionManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/operator/usa/TelecomExtensionManager;->telephonyManager(Landroid/content/Context;)V

    .line 153
    :cond_3
    const-string v0, "ltn_auto_csp"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 154
    const-string v0, "AutoCSP"

    const-string v1, "Binding to AutoCSP Service "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-virtual {p0}, Lcom/android/server/telecom/TelecomApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/server/telecom/IAutoCSP;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v1, v5}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, v4, :cond_6

    :cond_4
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/server/telecom/TelecomApp;->autoCSPConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v4}, Lcom/android/server/telecom/TelecomApp;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 160
    :cond_5
    return-void

    .line 155
    :cond_6
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v2, v2, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    new-instance v3, Landroid/content/ComponentName;

    invoke-direct {v3, v2, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final processChangeInContent(I)V
    .locals 3

    .prologue
    .line 217
    const-string v0, "TelecommApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processChangeInContent::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mCallsManager:Lcom/android/server/telecom/CallsManager;

    invoke-virtual {v0, p1}, Lcom/android/server/telecom/CallsManager;->onChangeInContent(I)V

    .line 219
    return-void
.end method

.method public final registerRcsObserver(I)V
    .locals 3

    .prologue
    .line 162
    const-string v0, "TelecommApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "registerRcsObserver::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    packed-switch p1, :pswitch_data_0

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 165
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsContentObserver:Lcom/android/server/telecom/rcs/RcsContentObserver;

    if-nez v0, :cond_1

    .line 166
    new-instance v0, Lcom/android/server/telecom/rcs/RcsContentObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1, p0}, Lcom/android/server/telecom/rcs/RcsContentObserver;-><init>(Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsContentObserver:Lcom/android/server/telecom/rcs/RcsContentObserver;

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsContentObserver:Lcom/android/server/telecom/rcs/RcsContentObserver;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsContentObserver:Lcom/android/server/telecom/rcs/RcsContentObserver;

    invoke-virtual {p0}, Lcom/android/server/telecom/TelecomApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/telecom/rcs/RcsContentObserver;->registerSelfAsObserver(Landroid/content/ContentResolver;)V

    goto :goto_0

    .line 173
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsCapaObserver:Lcom/android/server/telecom/rcs/RcsCapaObserver;

    if-nez v0, :cond_2

    .line 174
    new-instance v0, Lcom/android/server/telecom/rcs/RcsCapaObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-static {}, Lcom/android/internal/telephony/CallManager;->getInstance()Lcom/android/internal/telephony/CallManager;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/android/server/telecom/rcs/RcsCapaObserver;-><init>(Landroid/os/Handler;Landroid/content/Context;Lcom/android/internal/telephony/CallManager;)V

    iput-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsCapaObserver:Lcom/android/server/telecom/rcs/RcsCapaObserver;

    .line 177
    :cond_2
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsCapaObserver:Lcom/android/server/telecom/rcs/RcsCapaObserver;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsCapaObserver:Lcom/android/server/telecom/rcs/RcsCapaObserver;

    invoke-virtual {p0}, Lcom/android/server/telecom/TelecomApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/telecom/rcs/RcsCapaObserver;->registerSelfAsObserver(Landroid/content/ContentResolver;)V

    goto :goto_0

    .line 163
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final unRegisterRcsObserver(I)V
    .locals 3

    .prologue
    .line 186
    const-string v0, "TelecommApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unRegisterRcsObserver::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    packed-switch p1, :pswitch_data_0

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 189
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsContentObserver:Lcom/android/server/telecom/rcs/RcsContentObserver;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsContentObserver:Lcom/android/server/telecom/rcs/RcsContentObserver;

    invoke-virtual {v0}, Lcom/android/server/telecom/rcs/RcsContentObserver;->unregisterSelfAsObseverAndCloseCursor()V

    goto :goto_0

    .line 193
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsCapaObserver:Lcom/android/server/telecom/rcs/RcsCapaObserver;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/android/server/telecom/TelecomApp;->mRcsCapaObserver:Lcom/android/server/telecom/rcs/RcsCapaObserver;

    invoke-virtual {v0}, Lcom/android/server/telecom/rcs/RcsCapaObserver;->unregisterSelfAsObseverAndCloseCursor()V

    goto :goto_0

    .line 187
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
