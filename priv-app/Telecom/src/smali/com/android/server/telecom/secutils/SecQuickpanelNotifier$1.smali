.class final Lcom/android/server/telecom/secutils/SecQuickpanelNotifier$1;
.super Landroid/content/AsyncQueryHandler;
.source "SecQuickpanelNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->notifyMissedCall(Lcom/android/server/telecom/Call;Ljava/lang/String;ILjava/lang/String;Landroid/app/Notification$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic this$0:Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;


# direct methods
.method constructor <init>(Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier$1;->this$0:Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;

    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected final onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 75
    # getter for: Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onQueryComplete()..."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    if-eqz p3, :cond_7

    if-eqz p2, :cond_7

    .line 78
    check-cast p2, Landroid/app/Notification$InboxStyle;

    .line 80
    # getter for: Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MISSED_CALL query counts : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 85
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ne v1, v8, :cond_0

    .line 86
    iget-object v1, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier$1;->this$0:Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;

    invoke-static {v1, v8}, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->access$102(Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;Z)Z

    .line 90
    :cond_0
    :try_start_0
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 92
    :goto_0
    add-int/lit8 v2, v0, 0x1

    .line 93
    const-string v0, "count"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 95
    # getter for: Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "MISSED_CALL query duplicatedCount : "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v0, v1, v5}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    const-string v0, "number"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 97
    const-string v0, "name"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 98
    const-string v5, "dormant_set"

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 100
    if-eqz v0, :cond_8

    .line 106
    :goto_1
    if-ne v5, v8, :cond_1

    .line 107
    iget-object v1, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier$1;->this$0:Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;

    # getter for: Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->access$200(Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;)Landroid/content/Context;

    move-result-object v1

    const v5, 0x7f08007b

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v5, "%s"

    invoke-virtual {v1, v5, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 110
    :cond_1
    if-le v4, v8, :cond_3

    .line 111
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 112
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 113
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier$1;->this$0:Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;

    # getter for: Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mAllMissedCallsFromSamePerson:Z
    invoke-static {v0}, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->access$100(Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 114
    const-string v0, " ("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 115
    const-string v0, "%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v6

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 116
    const-string v0, ")"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 118
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 121
    :cond_3
    if-eqz v0, :cond_5

    .line 122
    invoke-virtual {p2, v0}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    .line 123
    if-eq v2, v8, :cond_4

    .line 124
    const-string v1, ", "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 126
    :cond_4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 128
    :cond_5
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_a

    .line 131
    :cond_6
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 134
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier$1;->this$0:Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;

    invoke-virtual {p2}, Landroid/app/Notification$InboxStyle;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->access$300(Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;Landroid/app/Notification;)V

    .line 137
    :cond_7
    return-void

    .line 102
    :cond_8
    :try_start_1
    const-string v0, "feature_vzw"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "feature_kor"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 103
    :cond_9
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 131
    :catchall_0
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_a
    move v0, v2

    goto/16 :goto_0

    :cond_b
    move-object v0, v1

    goto/16 :goto_1
.end method
