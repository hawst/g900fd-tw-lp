.class final Lcom/android/server/telecom/secutils/SecCallLogManager$AddCallArgs;
.super Ljava/lang/Object;
.source "SecCallLogManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/telecom/secutils/SecCallLogManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AddCallArgs"
.end annotation


# instance fields
.field public final accountHandle:Landroid/telecom/PhoneAccountHandle;

.field public final callType:I

.field public final callerInfo:Lcom/android/internal/telephony/CallerInfo;

.field public final context:Landroid/content/Context;

.field public dbUri:Landroid/net/Uri;

.field public final durationInSec:I

.field public final isAutoRejectCall:Z

.field public final number:Ljava/lang/String;

.field public final photoringUrl:Ljava/lang/String;

.field public final presentation:I

.field public service_type:I

.field public final timestamp:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/CallerInfo;Ljava/lang/String;IIILandroid/telecom/PhoneAccountHandle;JJLjava/lang/Long;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/android/server/telecom/secutils/SecCallLogManager$AddCallArgs;->context:Landroid/content/Context;

    .line 96
    iput-object p2, p0, Lcom/android/server/telecom/secutils/SecCallLogManager$AddCallArgs;->callerInfo:Lcom/android/internal/telephony/CallerInfo;

    .line 97
    iput-object p3, p0, Lcom/android/server/telecom/secutils/SecCallLogManager$AddCallArgs;->number:Ljava/lang/String;

    .line 98
    iput p4, p0, Lcom/android/server/telecom/secutils/SecCallLogManager$AddCallArgs;->presentation:I

    .line 99
    iput p5, p0, Lcom/android/server/telecom/secutils/SecCallLogManager$AddCallArgs;->callType:I

    .line 100
    iput-object p7, p0, Lcom/android/server/telecom/secutils/SecCallLogManager$AddCallArgs;->accountHandle:Landroid/telecom/PhoneAccountHandle;

    .line 102
    iput-wide p8, p0, Lcom/android/server/telecom/secutils/SecCallLogManager$AddCallArgs;->timestamp:J

    .line 103
    const-wide/16 v2, 0x3e8

    div-long v2, p10, v2

    long-to-int v2, v2

    iput v2, p0, Lcom/android/server/telecom/secutils/SecCallLogManager$AddCallArgs;->durationInSec:I

    .line 104
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/android/server/telecom/secutils/SecCallLogManager$AddCallArgs;->photoringUrl:Ljava/lang/String;

    .line 106
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/android/server/telecom/secutils/SecCallLogManager$AddCallArgs;->isAutoRejectCall:Z

    .line 107
    return-void
.end method
