.class public Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;
.super Ljava/lang/Object;
.source "SecQuickpanelNotifier.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAllMissedCallsFromSamePerson:Z

.field private mContext:Landroid/content/Context;

.field private mMissedCallCount:I

.field private mMissedCallName:Ljava/lang/String;

.field private final mNotificationManager:Landroid/app/NotificationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mAllMissedCallsFromSamePerson:Z

    .line 51
    iput-object p1, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mContext:Landroid/content/Context;

    .line 52
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mNotificationManager:Landroid/app/NotificationManager;

    .line 54
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mAllMissedCallsFromSamePerson:Z

    return v0
.end method

.method static synthetic access$102(Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;Z)Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mAllMissedCallsFromSamePerson:Z

    return v0
.end method

.method static synthetic access$200(Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;Landroid/app/Notification;)V
    .locals 4

    .prologue
    .line 37
    sget-object v0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->TAG:Ljava/lang/String;

    const-string v1, "registerMissedCallNotification"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget v0, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mMissedCallCount:I

    iput v0, p1, Landroid/app/Notification;->missedCount:I

    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mMissedCallName:Ljava/lang/String;

    iput-object v0, p1, Landroid/app/Notification;->contactCharSeq:Ljava/lang/CharSequence;

    const/4 v0, 0x2

    iput v0, p1, Landroid/app/Notification;->priority:I

    iget v0, p1, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/app/Notification;->flags:I

    iget v0, p1, Landroid/app/Notification;->defaults:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p1, Landroid/app/Notification;->defaults:I

    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mNotificationManager:Landroid/app/NotificationManager;

    const/4 v1, 0x0

    const/4 v2, 0x1

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2, p1, v3}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mMissedCallCount:I

    invoke-static {v0, v1}, Lcom/android/server/telecom/secutils/CallBadge;->updateBadge(Landroid/content/Context;I)V

    return-void
.end method


# virtual methods
.method public final notifyMissedCall(Lcom/android/server/telecom/Call;Ljava/lang/String;ILjava/lang/String;Landroid/app/Notification$Builder;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 59
    sget-object v0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "notifyMissedCall : creationTimeMillis="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getCreationTimeMillis()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " missedCallCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    new-instance v2, Landroid/app/Notification$InboxStyle;

    invoke-direct {v2, p5}, Landroid/app/Notification$InboxStyle;-><init>(Landroid/app/Notification$Builder;)V

    .line 62
    invoke-virtual {v2, p4}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    .line 64
    iput-object p2, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mMissedCallName:Ljava/lang/String;

    .line 66
    iput p3, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mMissedCallCount:I

    .line 68
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    const-string v0, "new = 1  AND "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "type = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 72
    new-instance v0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier$1;

    iget-object v3, p0, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier$1;-><init>(Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;Landroid/content/ContentResolver;)V

    .line 140
    const-string v3, "content://logs/call/group"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v7, "date DESC LIMIT 10"

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    return-void
.end method
