.class public final Lcom/android/server/telecom/CallAudioManager;
.super Lcom/android/server/telecom/CallsManager$CallsManagerListener;
.source "CallAudioManager.java"

# interfaces
.implements Lcom/android/server/telecom/WiredHeadsetManager$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/telecom/CallAudioManager$RouteRequester;
    }
.end annotation


# instance fields
.field private mAudioFocusStreamType:I

.field public final mAudioManager:Landroid/media/AudioManager;

.field public mAudioState:Landroid/telecom/AudioState;

.field private final mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

.field private mContext:Landroid/content/Context;

.field private mIsModifyCall:Z

.field private mIsRinging:Z

.field private mIsTonePlaying:Z

.field private final mStatusBarNotifier:Lcom/android/server/telecom/StatusBarNotifier;

.field private mVTendToneStoped:Z

.field public mWasSpeakerOn:Z

.field private final mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/server/telecom/StatusBarNotifier;Lcom/android/server/telecom/WiredHeadsetManager;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/telecom/CallAudioManager;->mVTendToneStoped:Z

    .line 64
    iput-object p2, p0, Lcom/android/server/telecom/CallAudioManager;->mStatusBarNotifier:Lcom/android/server/telecom/StatusBarNotifier;

    .line 65
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    .line 66
    new-instance v0, Lcom/android/server/telecom/BluetoothManager;

    invoke-direct {v0, p1, p0}, Lcom/android/server/telecom/BluetoothManager;-><init>(Landroid/content/Context;Lcom/android/server/telecom/CallAudioManager;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    .line 67
    iput-object p3, p0, Lcom/android/server/telecom/CallAudioManager;->mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

    .line 68
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/WiredHeadsetManager;->addListener(Lcom/android/server/telecom/WiredHeadsetManager$Listener;)V

    .line 69
    iput-object p1, p0, Lcom/android/server/telecom/CallAudioManager;->mContext:Landroid/content/Context;

    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/telecom/CallAudioManager;->getInitialAudioState(Lcom/android/server/telecom/Call;)Landroid/telecom/AudioState;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/telecom/CallAudioManager;->saveAudioState(Landroid/telecom/AudioState;)V

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioFocusStreamType:I

    .line 73
    return-void
.end method

.method private calculateSupportedRoutes()I
    .locals 2

    .prologue
    .line 721
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/WiredHeadsetManager;->isPluggedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 724
    const/16 v0, 0xc

    .line 729
    :goto_0
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v1}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 730
    or-int/lit8 v0, v0, 0x2

    .line 733
    :cond_0
    return v0

    .line 726
    :cond_1
    const/16 v0, 0x9

    goto :goto_0
.end method

.method private static getForegroundCall()Lcom/android/server/telecom/Call;
    .locals 3

    .prologue
    .line 803
    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/telecom/CallsManager;->getForegroundCall()Lcom/android/server/telecom/Call;

    move-result-object v0

    .line 807
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getState()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 808
    const/4 v0, 0x0

    .line 810
    :cond_0
    return-object v0
.end method

.method private getInitialAudioState(Lcom/android/server/telecom/Call;)Landroid/telecom/AudioState;
    .locals 4

    .prologue
    .line 737
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->calculateSupportedRoutes()I

    move-result v1

    .line 738
    const/4 v0, 0x5

    invoke-direct {p0, v0, v1}, Lcom/android/server/telecom/CallAudioManager;->selectWiredOrEarpiece(II)I

    move-result v0

    .line 750
    iget-object v2, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v2}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothWatchSingleConnected()Z

    move-result v2

    .line 751
    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v3}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAvailable()Z

    move-result v3

    if-eqz v3, :cond_0

    if-nez v2, :cond_0

    .line 752
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getState()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 759
    :cond_0
    :goto_0
    :pswitch_0
    new-instance v2, Landroid/telecom/AudioState;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0, v1}, Landroid/telecom/AudioState;-><init>(ZII)V

    return-object v2

    .line 758
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 752
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private hasFocus()Z
    .locals 2

    .prologue
    .line 819
    iget v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioFocusStreamType:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onCallUpdated(Lcom/android/server/telecom/Call;)V
    .locals 9

    .prologue
    const/4 v8, 0x7

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 380
    iget v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioFocusStreamType:I

    if-eqz v0, :cond_3

    move v0, v1

    .line 381
    :goto_0
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->calculateSupportedRoutes()I

    move-result v4

    .line 383
    if-eqz p1, :cond_8

    .line 384
    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->isCSVideoCall(Lcom/android/server/telecom/Call;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 385
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getState()I

    move-result v3

    if-ne v3, v7, :cond_0

    .line 386
    invoke-direct {p0, v2}, Lcom/android/server/telecom/CallAudioManager;->updateAudioStreamAndMode(Z)V

    .line 392
    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getState()I

    move-result v3

    if-ne v3, v8, :cond_8

    .line 393
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v3

    invoke-static {v3}, Lcom/android/server/telecom/secutils/TelecomUtils;->getTelephonyDisconnectCause(Landroid/telecom/DisconnectCause;)I

    move-result v3

    .line 396
    :goto_2
    iget-object v5, p0, Lcom/android/server/telecom/CallAudioManager;->mContext:Landroid/content/Context;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/android/server/telecom/CallAudioManager;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/server/telecom/secutils/TelecomUtils;->isDriveLinkModeOn(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 397
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    if-eq v0, v7, :cond_1

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->isIncoming()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 398
    :cond_1
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v0, v0, Landroid/telecom/AudioState;->route:I

    if-eq v0, v6, :cond_2

    .line 399
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget-boolean v0, v0, Landroid/telecom/AudioState;->isMuted:Z

    const/16 v1, 0x8

    iget-object v3, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v3, v3, Landroid/telecom/AudioState;->supportedRouteMask:I

    invoke-direct {p0, v0, v1, v3}, Lcom/android/server/telecom/CallAudioManager;->setSystemAudioState(ZII)V

    .line 400
    const-string v0, "set initial audio route to speaker"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 413
    :cond_2
    :goto_3
    return-void

    :cond_3
    move v0, v2

    .line 380
    goto :goto_0

    .line 388
    :cond_4
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getState()I

    move-result v3

    if-eq v3, v6, :cond_0

    .line 389
    invoke-direct {p0, v2}, Lcom/android/server/telecom/CallAudioManager;->updateAudioStreamAndMode(Z)V

    goto :goto_1

    .line 403
    :cond_5
    if-eqz v0, :cond_6

    iget v5, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioFocusStreamType:I

    if-nez v5, :cond_6

    .line 404
    invoke-direct {p0, p1, v1}, Lcom/android/server/telecom/CallAudioManager;->setInitialAudioState(Lcom/android/server/telecom/Call;Z)V

    goto :goto_3

    .line 405
    :cond_6
    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioFocusStreamType:I

    const/16 v5, 0xb

    if-ne v0, v5, :cond_2

    .line 406
    if-eqz p1, :cond_2

    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->isCSVideoCall(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 407
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    if-ne v0, v8, :cond_2

    iget-boolean v0, p0, Lcom/android/server/telecom/CallAudioManager;->mIsTonePlaying:Z

    if-nez v0, :cond_7

    const/4 v0, 0x5

    if-ne v3, v0, :cond_2

    .line 410
    :cond_7
    new-instance v0, Landroid/telecom/AudioState;

    invoke-direct {v0, v2, v1, v4}, Landroid/telecom/AudioState;-><init>(ZII)V

    iput-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    goto :goto_3

    :cond_8
    move v3, v2

    goto :goto_2
.end method

.method private requestAudioFocusAndSetMode(II)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 642
    const-string v0, "requestAudioFocusAndSetMode, stream: %d -> %d"

    new-array v3, v5, [Ljava/lang/Object;

    iget v4, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioFocusStreamType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {p0, v0, v3}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 643
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkState(Z)V

    .line 647
    iget v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioFocusStreamType:I

    if-eq v0, p1, :cond_0

    .line 648
    const-string v0, "requesting audio focus for stream: %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 649
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1, v5}, Landroid/media/AudioManager;->requestAudioFocusForCall(II)V

    .line 652
    :cond_0
    iput p1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioFocusStreamType:I

    .line 653
    invoke-direct {p0, p2}, Lcom/android/server/telecom/CallAudioManager;->setMode(I)V

    .line 654
    return-void

    :cond_1
    move v0, v2

    .line 643
    goto :goto_0
.end method

.method private saveAudioState(Landroid/telecom/AudioState;)V
    .locals 3

    .prologue
    .line 374
    iput-object p1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    .line 375
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mStatusBarNotifier:Lcom/android/server/telecom/StatusBarNotifier;

    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget-boolean v1, v1, Landroid/telecom/AudioState;->isMuted:Z

    invoke-virtual {v0, v1}, Lcom/android/server/telecom/StatusBarNotifier;->notifyMute(Z)V

    .line 376
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mStatusBarNotifier:Lcom/android/server/telecom/StatusBarNotifier;

    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v0, v0, Landroid/telecom/AudioState;->route:I

    const/16 v2, 0x8

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/server/telecom/StatusBarNotifier;->notifySpeakerphone(Z)V

    .line 377
    return-void

    .line 376
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private selectWiredOrEarpiece(II)I
    .locals 2

    .prologue
    .line 709
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 710
    and-int/lit8 p1, p2, 0x5

    .line 711
    if-nez p1, :cond_0

    .line 712
    const-string v0, "One of wired headset or earpiece should always be valid."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->wtf(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 714
    const/4 p1, 0x1

    .line 717
    :cond_0
    return p1
.end method

.method private setInitialAudioState(Lcom/android/server/telecom/Call;Z)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 769
    invoke-direct {p0, p1}, Lcom/android/server/telecom/CallAudioManager;->getInitialAudioState(Lcom/android/server/telecom/Call;)Landroid/telecom/AudioState;

    move-result-object v0

    .line 770
    const-string v1, "setInitialAudioState %s, %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v4

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 772
    if-nez p2, :cond_1

    iget-boolean v1, p0, Lcom/android/server/telecom/CallAudioManager;->mIsTonePlaying:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->playingSecCallEndTone()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 773
    :cond_0
    const-string v0, "trying to initialize audio state while tone is playing, abort."

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 779
    :goto_0
    return-void

    .line 777
    :cond_1
    iget-boolean v2, v0, Landroid/telecom/AudioState;->isMuted:Z

    iget v3, v0, Landroid/telecom/AudioState;->route:I

    iget v4, v0, Landroid/telecom/AudioState;->supportedRouteMask:I

    sget-object v5, Lcom/android/server/telecom/CallAudioManager$RouteRequester;->INTERNAL:Lcom/android/server/telecom/CallAudioManager$RouteRequester;

    move-object v0, p0

    move v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/server/telecom/CallAudioManager;->setSystemAudioState(ZZIILcom/android/server/telecom/CallAudioManager$RouteRequester;)V

    goto :goto_0
.end method

.method private setMode(I)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 673
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->hasFocus()Z

    move-result v0

    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkState(Z)V

    .line 674
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v3

    .line 675
    const-string v0, "Request to change audio mode from %d to %d"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {p0, v0, v4}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 677
    invoke-static {}, Lcom/android/server/telecom/CallAudioManager;->getForegroundCall()Lcom/android/server/telecom/Call;

    move-result-object v4

    .line 679
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {v4, v0, p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->getNeedtoSetMode(Lcom/android/server/telecom/Call;Landroid/media/AudioManager;I)Z

    move-result v5

    .line 681
    if-eqz v4, :cond_4

    const-string v0, "feature_multisim"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 682
    invoke-virtual {v4}, Lcom/android/server/telecom/Call;->isIncoming()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x3

    invoke-virtual {v4}, Lcom/android/server/telecom/Call;->getState()I

    move-result v6

    if-ne v0, v6, :cond_4

    move v0, v1

    .line 686
    :goto_0
    if-ne v3, p1, :cond_0

    iget-boolean v6, p0, Lcom/android/server/telecom/CallAudioManager;->mIsModifyCall:Z

    if-nez v6, :cond_0

    if-nez v5, :cond_0

    if-eqz v0, :cond_3

    .line 687
    :cond_0
    if-ne v3, v7, :cond_1

    if-ne p1, v1, :cond_1

    .line 688
    const-string v0, "Transition from IN_CALL -> RINGTONE. Resetting to NORMAL first."

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v3}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 689
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setMode(I)V

    .line 691
    :cond_1
    const-string v0, "feature_multisim"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 693
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    const/4 v3, 0x6

    if-eq v0, v3, :cond_2

    .line 694
    const-string v0, "call.getState() : %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/android/server/telecom/Call;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 695
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {v4, v0, p1}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->setAudioParameters(Lcom/android/server/telecom/Call;Landroid/media/AudioManager;I)V

    .line 698
    :cond_2
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {v4, v0, p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->setAudioParameters(Lcom/android/server/telecom/Call;Landroid/media/AudioManager;I)V

    .line 699
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setMode(I)V

    .line 700
    :cond_3
    iput-boolean v2, p0, Lcom/android/server/telecom/CallAudioManager;->mIsModifyCall:Z

    .line 703
    return-void

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method private setSystemAudioState(ZII)V
    .locals 6

    .prologue
    .line 440
    const/4 v1, 0x0

    sget-object v5, Lcom/android/server/telecom/CallAudioManager$RouteRequester;->INTERNAL:Lcom/android/server/telecom/CallAudioManager$RouteRequester;

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/server/telecom/CallAudioManager;->setSystemAudioState(ZZIILcom/android/server/telecom/CallAudioManager$RouteRequester;)V

    .line 441
    return-void
.end method

.method private setSystemAudioState(ZIILcom/android/server/telecom/CallAudioManager$RouteRequester;)V
    .locals 6

    .prologue
    .line 436
    const/4 v1, 0x0

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/server/telecom/CallAudioManager;->setSystemAudioState(ZZIILcom/android/server/telecom/CallAudioManager$RouteRequester;)V

    .line 437
    return-void
.end method

.method private setSystemAudioState(ZZIILcom/android/server/telecom/CallAudioManager$RouteRequester;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 450
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_1

    .line 451
    const-string v0, "force_initial_audiostate"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/telecom/CallsManager;->getCalls()Lcom/google/common/collect/ImmutableCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableCollection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 505
    :cond_0
    :goto_0
    return-void

    .line 457
    :cond_1
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    .line 458
    new-instance v1, Landroid/telecom/AudioState;

    invoke-direct {v1, p2, p3, p4}, Landroid/telecom/AudioState;-><init>(ZII)V

    invoke-direct {p0, v1}, Lcom/android/server/telecom/CallAudioManager;->saveAudioState(Landroid/telecom/AudioState;)V

    .line 459
    if-nez p1, :cond_2

    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 462
    :cond_2
    const-string v1, "changing audio state from %s to %s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v4

    iget-object v3, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    aput-object v3, v2, v5

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 465
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget-boolean v1, v1, Landroid/telecom/AudioState;->isMuted:Z

    iget-object v2, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v2

    if-eq v1, v2, :cond_3

    .line 466
    const-string v1, "changing microphone mute state to: %b"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget-boolean v3, v3, Landroid/telecom/AudioState;->isMuted:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 467
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget-boolean v2, v2, Landroid/telecom/AudioState;->isMuted:Z

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    .line 471
    :cond_3
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v1, v1, Landroid/telecom/AudioState;->route:I

    if-ne v1, v6, :cond_6

    .line 472
    invoke-direct {p0, v4}, Lcom/android/server/telecom/CallAudioManager;->turnOnSpeaker(Z)V

    .line 475
    sget-object v1, Lcom/android/server/telecom/CallAudioManager$RouteRequester;->EXTERNAL:Lcom/android/server/telecom/CallAudioManager$RouteRequester;

    if-ne p5, v1, :cond_5

    .line 476
    invoke-virtual {p0, v5}, Lcom/android/server/telecom/CallAudioManager;->turnOnSecBluetoothForSec(Z)V

    .line 501
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    invoke-virtual {v0, v1}, Landroid/telecom/AudioState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 502
    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    invoke-virtual {v1, v0, v2}, Lcom/android/server/telecom/CallsManager;->onAudioStateChanged(Landroid/telecom/AudioState;Landroid/telecom/AudioState;)V

    .line 503
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->updateAudioForForegroundCall()V

    goto :goto_0

    .line 478
    :cond_5
    invoke-direct {p0, v5}, Lcom/android/server/telecom/CallAudioManager;->turnOnBluetooth(Z)V

    goto :goto_1

    .line 480
    :cond_6
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v1, v1, Landroid/telecom/AudioState;->route:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_8

    .line 482
    sget-object v1, Lcom/android/server/telecom/CallAudioManager$RouteRequester;->EXTERNAL:Lcom/android/server/telecom/CallAudioManager$RouteRequester;

    if-ne p5, v1, :cond_7

    .line 483
    invoke-virtual {p0, v4}, Lcom/android/server/telecom/CallAudioManager;->turnOnSecBluetoothForSec(Z)V

    .line 488
    :goto_2
    invoke-direct {p0, v5}, Lcom/android/server/telecom/CallAudioManager;->turnOnSpeaker(Z)V

    goto :goto_1

    .line 485
    :cond_7
    invoke-direct {p0, v4}, Lcom/android/server/telecom/CallAudioManager;->turnOnBluetooth(Z)V

    goto :goto_2

    .line 489
    :cond_8
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v1, v1, Landroid/telecom/AudioState;->route:I

    if-eq v1, v5, :cond_9

    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v1, v1, Landroid/telecom/AudioState;->route:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    .line 492
    :cond_9
    sget-object v1, Lcom/android/server/telecom/CallAudioManager$RouteRequester;->EXTERNAL:Lcom/android/server/telecom/CallAudioManager$RouteRequester;

    if-ne p5, v1, :cond_a

    .line 493
    invoke-virtual {p0, v4}, Lcom/android/server/telecom/CallAudioManager;->turnOnSecBluetoothForSec(Z)V

    .line 498
    :goto_3
    invoke-direct {p0, v4}, Lcom/android/server/telecom/CallAudioManager;->turnOnSpeaker(Z)V

    goto :goto_1

    .line 495
    :cond_a
    invoke-direct {p0, v4}, Lcom/android/server/telecom/CallAudioManager;->turnOnBluetooth(Z)V

    goto :goto_3
.end method

.method private turnOnBluetooth(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 527
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAudioConnectedOrPending()Z

    move-result v0

    .line 529
    if-eq p1, v0, :cond_0

    .line 530
    const-string v0, "connecting bluetooth %s"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 531
    const-string v0, "but ignore that for gear concept"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 532
    :cond_0
    return-void
.end method

.method private turnOnSpeaker(Z)V
    .locals 4

    .prologue
    .line 509
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 510
    const-string v0, "turning speaker phone %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 511
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 514
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {v0}, Lcom/android/server/telecom/secutils/TelecomUtils;->updateRingbackToneVolume(Landroid/media/AudioManager;)V

    .line 515
    return-void
.end method

.method private updateAudioForForegroundCall()V
    .locals 3

    .prologue
    .line 793
    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/telecom/CallsManager;->getForegroundCall()Lcom/android/server/telecom/Call;

    move-result-object v0

    .line 794
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getConnectionService()Lcom/android/server/telecom/ConnectionServiceWrapper;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 795
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getConnectionService()Lcom/android/server/telecom/ConnectionServiceWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    invoke-virtual {v1, v0, v2}, Lcom/android/server/telecom/ConnectionServiceWrapper;->onAudioStateChanged(Lcom/android/server/telecom/Call;Landroid/telecom/AudioState;)V

    .line 797
    :cond_0
    return-void
.end method

.method private updateAudioStreamAndMode(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 583
    const-string v0, "updateAudioStreamAndMode, mIsRinging: %b, mIsTonePlaying: %b removeCase: %b"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-boolean v4, p0, Lcom/android/server/telecom/CallAudioManager;->mIsRinging:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v1

    iget-boolean v4, p0, Lcom/android/server/telecom/CallAudioManager;->mIsTonePlaying:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p0, v0, v3}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 586
    invoke-static {}, Lcom/android/server/telecom/TelecomApp;->getInstance()Lcom/android/server/telecom/TelecomApp;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/telecom/secutils/TelecomUtils;->needPlaySecCallEndTone(Landroid/content/Context;)Z

    move-result v0

    .line 587
    if-eqz p1, :cond_0

    move v0, v1

    .line 590
    :cond_0
    iget-boolean v3, p0, Lcom/android/server/telecom/CallAudioManager;->mIsRinging:Z

    if-eqz v3, :cond_2

    .line 591
    invoke-direct {p0, v5, v2}, Lcom/android/server/telecom/CallAudioManager;->requestAudioFocusAndSetMode(II)V

    .line 639
    :cond_1
    :goto_0
    return-void

    .line 593
    :cond_2
    invoke-static {}, Lcom/android/server/telecom/CallAudioManager;->getForegroundCall()Lcom/android/server/telecom/Call;

    move-result-object v3

    .line 594
    if-eqz v3, :cond_3

    .line 595
    const-string v0, "- Foreground call is exist"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 600
    invoke-static {v3}, Lcom/android/server/telecom/secutils/TelecomUtils;->getAudioStream(Lcom/android/server/telecom/Call;)I

    move-result v0

    invoke-static {v3}, Lcom/android/server/telecom/secutils/TelecomUtils;->getAudioMode(Lcom/android/server/telecom/Call;)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/server/telecom/CallAudioManager;->requestAudioFocusAndSetMode(II)V

    goto :goto_0

    .line 601
    :cond_3
    iget-boolean v4, p0, Lcom/android/server/telecom/CallAudioManager;->mIsTonePlaying:Z

    if-eqz v4, :cond_6

    .line 615
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    .line 617
    if-eqz v3, :cond_4

    const-string v0, "voip_interworking"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->isVoIPActivated()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 620
    :cond_4
    const-string v0, "call is null, or voip is activated"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v2}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 621
    invoke-direct {p0, v1, v1}, Lcom/android/server/telecom/CallAudioManager;->requestAudioFocusAndSetMode(II)V

    goto :goto_0

    .line 623
    :cond_5
    iget v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioFocusStreamType:I

    invoke-direct {p0, v0, v1}, Lcom/android/server/telecom/CallAudioManager;->requestAudioFocusAndSetMode(II)V

    goto :goto_0

    .line 626
    :cond_6
    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/telecom/CallsManager;->getForegroundCall()Lcom/android/server/telecom/Call;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/android/server/telecom/Call;->getState()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_7

    :goto_1
    if-nez v2, :cond_1

    if-nez v0, :cond_1

    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->playingSecCallEndTone()Z

    move-result v0

    if-nez v0, :cond_1

    .line 629
    const-string v0, "- abandonAudioFocus"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v2}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 630
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    invoke-direct {p0, v1}, Lcom/android/server/telecom/CallAudioManager;->setMode(I)V

    const-string v0, "abandoning audio focus"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->abandonAudioFocusForCall()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioFocusStreamType:I

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 626
    goto :goto_1
.end method


# virtual methods
.method public final getBluetoothManager()Lcom/android/server/telecom/BluetoothManager;
    .locals 1

    .prologue
    .line 827
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    return-object v0
.end method

.method public final isBluetoothAudioOn()Z
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAudioConnected()Z

    move-result v0

    return v0
.end method

.method public final isBluetoothDeviceAvailable()Z
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAvailable()Z

    move-result v0

    return v0
.end method

.method public final isWiredHeadsetPluggedIn()Z
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/WiredHeadsetManager;->isPluggedIn()Z

    move-result v0

    return v0
.end method

.method public final mute(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 204
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    const-string v1, "mute, shouldMute: %b"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211
    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/telecom/CallsManager;->hasEmergencyCall()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 213
    const-string v1, "ignoring mute for emergency call"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    move p1, v0

    .line 216
    :cond_2
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget-boolean v1, v1, Landroid/telecom/AudioState;->isMuted:Z

    if-eq v1, p1, :cond_3

    .line 217
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v0, v0, Landroid/telecom/AudioState;->route:I

    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v1, v1, Landroid/telecom/AudioState;->supportedRouteMask:I

    invoke-direct {p0, p1, v0, v1}, Lcom/android/server/telecom/CallAudioManager;->setSystemAudioState(ZII)V

    goto :goto_0

    .line 218
    :cond_3
    const-string v1, "feature_multisim_dsda"

    invoke-static {v1}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    const-string v1, "force mute, shouldMute: %b for DSDA"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 221
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    goto :goto_0
.end method

.method final onBluetoothStateChange(Lcom/android/server/telecom/BluetoothManager;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x2

    .line 305
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 332
    :goto_0
    return-void

    .line 309
    :cond_0
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->calculateSupportedRoutes()I

    move-result v2

    .line 310
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v0, v0, Landroid/telecom/AudioState;->route:I

    .line 311
    invoke-virtual {p1}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAudioConnectedOrPending()Z

    move-result v3

    if-nez v3, :cond_1

    iget-boolean v3, p1, Lcom/android/server/telecom/BluetoothManager;->mBluetoothConnected:Z

    if-eqz v3, :cond_3

    :cond_1
    move v0, v1

    .line 331
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget-boolean v1, v1, Landroid/telecom/AudioState;->isMuted:Z

    invoke-direct {p0, v1, v0, v2}, Lcom/android/server/telecom/CallAudioManager;->setSystemAudioState(ZII)V

    goto :goto_0

    .line 313
    :cond_3
    iget-object v3, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v3, v3, Landroid/telecom/AudioState;->route:I

    if-ne v3, v1, :cond_2

    .line 314
    invoke-static {}, Lcom/android/server/telecom/CallAudioManager;->getForegroundCall()Lcom/android/server/telecom/Call;

    move-result-object v0

    .line 315
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {v0}, Lcom/android/server/telecom/secutils/TelecomUtils;->isVideoCall(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/server/telecom/CallAudioManager;->mWasSpeakerOn:Z

    if-eqz v0, :cond_5

    .line 316
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/WiredHeadsetManager;->isPluggedIn()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 317
    invoke-direct {p0, v4, v2}, Lcom/android/server/telecom/CallAudioManager;->selectWiredOrEarpiece(II)I

    move-result v0

    goto :goto_1

    .line 320
    :cond_4
    const/16 v0, 0x8

    goto :goto_1

    .line 323
    :cond_5
    invoke-direct {p0, v4, v2}, Lcom/android/server/telecom/CallAudioManager;->selectWiredOrEarpiece(II)I

    move-result v0

    .line 326
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/telecom/CallAudioManager;->mWasSpeakerOn:Z

    goto :goto_1
.end method

.method public final onCallAdded(Lcom/android/server/telecom/Call;)V
    .locals 3

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/android/server/telecom/CallAudioManager;->mIsTonePlaying:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->isPSVideoCall(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/telecom/CallsManager;->mInCallToneMonitor:Lcom/android/server/telecom/InCallToneMonitor;

    invoke-virtual {v0}, Lcom/android/server/telecom/InCallToneMonitor;->stopCallEndTone()V

    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->isVideoCall(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/telecom/CallAudioManager;->mVTendToneStoped:Z

    .line 82
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/telecom/CallAudioManager;->onCallUpdated(Lcom/android/server/telecom/Call;)V

    .line 84
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/server/telecom/CallAudioManager;->getForegroundCall()Lcom/android/server/telecom/Call;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 85
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->isIncoming()Z

    move-result v0

    if-nez v0, :cond_1

    .line 87
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v1, v1, Landroid/telecom/AudioState;->route:I

    iget-object v2, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v2, v2, Landroid/telecom/AudioState;->supportedRouteMask:I

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/telecom/CallAudioManager;->setSystemAudioState(ZII)V

    .line 90
    :cond_1
    return-void
.end method

.method public final onCallRemoved(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/android/server/telecom/CallAudioManager;->resetAudioStateAfterDisconnect()V

    .line 95
    return-void
.end method

.method public final onCallStateChanged(Lcom/android/server/telecom/Call;II)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/android/server/telecom/CallAudioManager;->onCallUpdated(Lcom/android/server/telecom/Call;)V

    .line 120
    return-void
.end method

.method public final onEpdgStateChanged()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 369
    const-string v0, "onEpdgStateChanged"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 370
    invoke-direct {p0, v2}, Lcom/android/server/telecom/CallAudioManager;->updateAudioStreamAndMode(Z)V

    .line 371
    return-void
.end method

.method public final onForegroundCallChanged(Lcom/android/server/telecom/Call;Lcom/android/server/telecom/Call;)V
    .locals 2

    .prologue
    .line 146
    invoke-direct {p0, p2}, Lcom/android/server/telecom/CallAudioManager;->onCallUpdated(Lcom/android/server/telecom/Call;)V

    .line 148
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->calculateSupportedRoutes()I

    move-result v0

    .line 149
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v1, v1, Landroid/telecom/AudioState;->supportedRouteMask:I

    if-eq v0, v1, :cond_0

    .line 151
    invoke-direct {p0, p2}, Lcom/android/server/telecom/CallAudioManager;->getInitialAudioState(Lcom/android/server/telecom/Call;)Landroid/telecom/AudioState;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onForegroundCallChanged -changed mAudioState.supportedRouteMask "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v1, v1, Landroid/telecom/AudioState;->supportedRouteMask:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    :cond_0
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->updateAudioForForegroundCall()V

    .line 155
    return-void
.end method

.method public final onIncomingCallAnswered(Lcom/android/server/telecom/Call;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 124
    iget-object v2, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v2, v2, Landroid/telecom/AudioState;->route:I

    .line 129
    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/telecom/CallsManager;->getCalls()Lcom/google/common/collect/ImmutableCollection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableCollection;->size()I

    move-result v3

    if-ne v3, v0, :cond_0

    .line 133
    :goto_0
    iget-object v3, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v3}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothWatchSingleConnected()Z

    move-result v3

    .line 134
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez v3, :cond_1

    .line 138
    const/4 v0, 0x2

    .line 141
    :goto_1
    iget-object v2, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v2, v2, Landroid/telecom/AudioState;->supportedRouteMask:I

    invoke-direct {p0, v1, v0, v2}, Lcom/android/server/telecom/CallAudioManager;->setSystemAudioState(ZII)V

    .line 142
    return-void

    :cond_0
    move v0, v1

    .line 129
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public final onIsVoipAudioModeChanged$57920a58()V
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/telecom/CallAudioManager;->updateAudioStreamAndMode(Z)V

    .line 160
    return-void
.end method

.method public final onModifiedCall()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 347
    const-string v0, "onModifiedCall"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 348
    iput-boolean v3, p0, Lcom/android/server/telecom/CallAudioManager;->mIsModifyCall:Z

    .line 349
    invoke-direct {p0, v2}, Lcom/android/server/telecom/CallAudioManager;->updateAudioStreamAndMode(Z)V

    .line 351
    const-string v0, "common_volte_hk"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 352
    invoke-static {}, Lcom/android/server/telecom/CallAudioManager;->getForegroundCall()Lcom/android/server/telecom/Call;

    move-result-object v0

    .line 353
    invoke-static {v0}, Lcom/android/server/telecom/secutils/TelecomUtils;->isVideoCall(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v0, v0, Landroid/telecom/AudioState;->route:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAudioConnected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/WiredHeadsetManager;->isPluggedIn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 359
    :cond_0
    iput-boolean v3, p0, Lcom/android/server/telecom/CallAudioManager;->mWasSpeakerOn:Z

    .line 366
    :cond_1
    :goto_0
    return-void

    .line 362
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v2}, Lcom/android/server/telecom/CallAudioManager;->setAudioRoute(IZ)V

    goto :goto_0
.end method

.method public final onWiredHeadsetPluggedInChanged$25decb5(Z)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    .line 169
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    const/4 v1, 0x1

    .line 174
    if-eqz p1, :cond_3

    .line 175
    const/4 v0, 0x4

    .line 194
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {v1, p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->updateRingbackToneVolume(Landroid/media/AudioManager;Z)V

    .line 196
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget-boolean v1, v1, Landroid/telecom/AudioState;->isMuted:Z

    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->calculateSupportedRoutes()I

    move-result v2

    sget-object v3, Lcom/android/server/telecom/CallAudioManager$RouteRequester;->EXTERNAL:Lcom/android/server/telecom/CallAudioManager$RouteRequester;

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/android/server/telecom/CallAudioManager;->setSystemAudioState(ZIILcom/android/server/telecom/CallAudioManager$RouteRequester;)V

    goto :goto_0

    .line 176
    :cond_3
    iget-object v2, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v2, v2, Landroid/telecom/AudioState;->route:I

    if-ne v2, v0, :cond_4

    .line 177
    const-string v0, "onWiredHeadsetPluggedInChanged : keep ROUTE_BLUETOOTH"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 179
    :cond_4
    iget-object v2, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v2}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAvailable()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 180
    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v1}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAudioConnectedOrPending()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    iget-boolean v1, v1, Lcom/android/server/telecom/BluetoothManager;->mBluetoothConnected:Z

    if-eqz v1, :cond_2

    goto :goto_0

    .line 185
    :cond_5
    iget-boolean v0, p0, Lcom/android/server/telecom/CallAudioManager;->mWasSpeakerOn:Z

    if-eqz v0, :cond_6

    .line 186
    invoke-static {}, Lcom/android/server/telecom/CallAudioManager;->getForegroundCall()Lcom/android/server/telecom/Call;

    move-result-object v0

    .line 187
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 189
    const/16 v0, 0x8

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final resetAudioStateAfterDisconnect()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 98
    const-string v0, "resetAudioStateAfterDisconnect..."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 101
    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/telecom/CallsManager;->getCalls()Lcom/google/common/collect/ImmutableCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const-string v0, "all calls removed, reseting system audio to default state"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    invoke-direct {p0, v4, v2}, Lcom/android/server/telecom/CallAudioManager;->setInitialAudioState(Lcom/android/server/telecom/Call;Z)V

    .line 104
    iput-boolean v2, p0, Lcom/android/server/telecom/CallAudioManager;->mWasSpeakerOn:Z

    .line 106
    :cond_0
    invoke-direct {p0, v3}, Lcom/android/server/telecom/CallAudioManager;->updateAudioStreamAndMode(Z)V

    .line 115
    :cond_1
    :goto_0
    return-void

    .line 107
    :cond_2
    const-string v0, "force_initial_audiostate"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/telecom/CallsManager;->getCalls()Lcom/google/common/collect/ImmutableCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 109
    const-string v0, "all calls removed, reseting system audio to default state - for sec"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    invoke-direct {p0, v4, v3}, Lcom/android/server/telecom/CallAudioManager;->setInitialAudioState(Lcom/android/server/telecom/Call;Z)V

    .line 111
    iput-boolean v2, p0, Lcom/android/server/telecom/CallAudioManager;->mWasSpeakerOn:Z

    .line 113
    :cond_3
    invoke-direct {p0, v3}, Lcom/android/server/telecom/CallAudioManager;->updateAudioStreamAndMode(Z)V

    goto :goto_0
.end method

.method public final setAudioRoute(I)V
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/server/telecom/CallAudioManager;->setAudioRoute(IZ)V

    .line 232
    return-void
.end method

.method public final setAudioRoute(IZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 236
    invoke-direct {p0}, Lcom/android/server/telecom/CallAudioManager;->hasFocus()Z

    move-result v2

    if-nez v2, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    const-string v2, "setAudioRoute, route: %s"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p1}, Landroid/telecom/AudioState;->audioRouteToString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {p0, v2, v3}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 243
    iget-object v2, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v2, v2, Landroid/telecom/AudioState;->supportedRouteMask:I

    invoke-direct {p0, p1, v2}, Lcom/android/server/telecom/CallAudioManager;->selectWiredOrEarpiece(II)I

    move-result v2

    .line 246
    iget-object v3, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v3, v3, Landroid/telecom/AudioState;->supportedRouteMask:I

    or-int/2addr v3, v2

    if-nez v3, :cond_2

    .line 247
    const-string v3, "Asking to set to a route that is unsupported: %d"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p0, v3, v0}, Lcom/android/server/telecom/Log;->wtf(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 251
    :cond_2
    iget-object v3, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v3, v3, Landroid/telecom/AudioState;->route:I

    if-eq v3, v2, :cond_0

    .line 254
    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    :goto_1
    iput-boolean v0, p0, Lcom/android/server/telecom/CallAudioManager;->mWasSpeakerOn:Z

    .line 257
    if-eqz p2, :cond_4

    .line 258
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget-boolean v0, v0, Landroid/telecom/AudioState;->isMuted:Z

    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v1, v1, Landroid/telecom/AudioState;->supportedRouteMask:I

    sget-object v3, Lcom/android/server/telecom/CallAudioManager$RouteRequester;->EXTERNAL:Lcom/android/server/telecom/CallAudioManager$RouteRequester;

    invoke-direct {p0, v0, v2, v1, v3}, Lcom/android/server/telecom/CallAudioManager;->setSystemAudioState(ZIILcom/android/server/telecom/CallAudioManager$RouteRequester;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 254
    goto :goto_1

    .line 261
    :cond_4
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget-boolean v0, v0, Landroid/telecom/AudioState;->isMuted:Z

    iget-object v1, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v1, v1, Landroid/telecom/AudioState;->supportedRouteMask:I

    invoke-direct {p0, v0, v2, v1}, Lcom/android/server/telecom/CallAudioManager;->setSystemAudioState(ZII)V

    goto :goto_0
.end method

.method final setIsRinging(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 268
    iget-boolean v0, p0, Lcom/android/server/telecom/CallAudioManager;->mIsRinging:Z

    if-eq v0, p1, :cond_0

    .line 269
    const-string v0, "setIsRinging %b -> %b"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-boolean v2, p0, Lcom/android/server/telecom/CallAudioManager;->mIsRinging:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    iput-boolean p1, p0, Lcom/android/server/telecom/CallAudioManager;->mIsRinging:Z

    .line 271
    invoke-direct {p0, v4}, Lcom/android/server/telecom/CallAudioManager;->updateAudioStreamAndMode(Z)V

    .line 273
    :cond_0
    return-void
.end method

.method final setIsTonePlaying(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 283
    invoke-static {}, Lcom/android/server/telecom/ThreadUtil;->checkOnMainThread()V

    .line 285
    iget-boolean v0, p0, Lcom/android/server/telecom/CallAudioManager;->mIsTonePlaying:Z

    if-eq v0, p1, :cond_1

    .line 286
    const-string v0, "mIsTonePlaying %b -> %b."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-boolean v2, p0, Lcom/android/server/telecom/CallAudioManager;->mIsTonePlaying:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    iput-boolean p1, p0, Lcom/android/server/telecom/CallAudioManager;->mIsTonePlaying:Z

    .line 288
    iget-boolean v0, p0, Lcom/android/server/telecom/CallAudioManager;->mIsTonePlaying:Z

    if-nez v0, :cond_2

    .line 290
    iget-boolean v0, p0, Lcom/android/server/telecom/CallAudioManager;->mVTendToneStoped:Z

    if-ne v0, v3, :cond_0

    .line 291
    const/4 v0, 0x0

    invoke-direct {p0, v0, v3}, Lcom/android/server/telecom/CallAudioManager;->setInitialAudioState(Lcom/android/server/telecom/Call;Z)V

    .line 292
    iput-boolean v4, p0, Lcom/android/server/telecom/CallAudioManager;->mVTendToneStoped:Z

    .line 294
    :cond_0
    invoke-direct {p0, v3}, Lcom/android/server/telecom/CallAudioManager;->updateAudioStreamAndMode(Z)V

    .line 298
    :cond_1
    :goto_0
    return-void

    .line 296
    :cond_2
    invoke-direct {p0, v4}, Lcom/android/server/telecom/CallAudioManager;->updateAudioStreamAndMode(Z)V

    goto :goto_0
.end method

.method public final toggleBluetooth(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 560
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 561
    const-string v0, "toggleBluetooth(): bluetooth is available"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 562
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    iget v0, v0, Landroid/telecom/AudioState;->route:I

    if-ne v0, v3, :cond_0

    .line 563
    invoke-virtual {p0, v4, v2}, Lcom/android/server/telecom/CallAudioManager;->setAudioRoute(IZ)V

    .line 576
    :goto_0
    return-void

    .line 565
    :cond_0
    invoke-virtual {p0, v3, v2}, Lcom/android/server/telecom/CallAudioManager;->setAudioRoute(IZ)V

    goto :goto_0

    .line 568
    :cond_1
    const-string v0, "toggleBluetooth(): bluetooth is unavailable"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 569
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.bluetooth.devicepicker.action.LAUNCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 570
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 571
    const-string v1, "android.bluetooth.devicepicker.extra.NEED_AUTH"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 572
    const-string v1, "android.bluetooth.devicepicker.extra.FILTER_TYPE"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 573
    const-string v1, "android.bluetooth.FromHeadsetActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 574
    invoke-static {}, Lcom/android/server/telecom/TelecomApp;->getInstance()Lcom/android/server/telecom/TelecomApp;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/telecom/TelecomApp;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final turnOnSecBluetoothForSec(Z)V
    .locals 4

    .prologue
    .line 545
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/BluetoothManager;->isBluetoothAudioConnectedOrPending()Z

    move-result v0

    .line 547
    if-eq p1, v0, :cond_0

    .line 548
    const-string v0, "connecting turnOnSecBluetoothForSec %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 549
    if-eqz p1, :cond_1

    .line 550
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/BluetoothManager;->connectBluetoothAudio()V

    .line 556
    :cond_0
    :goto_0
    return-void

    .line 552
    :cond_1
    iget-object v0, p0, Lcom/android/server/telecom/CallAudioManager;->mBluetoothManager:Lcom/android/server/telecom/BluetoothManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/BluetoothManager;->disconnectBluetoothAudio()V

    goto :goto_0
.end method
