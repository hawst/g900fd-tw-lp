.class public final Lcom/android/server/telecom/secutils/SecCallsManagerListener;
.super Lcom/android/server/telecom/CallsManager$CallsManagerListener;
.source "SecCallsManagerListener.java"


# instance fields
.field private callBarringPass:Z

.field private mAutoRedial:Lcom/android/server/telecom/secutils/AutoRedial;

.field private mAutoRedialTimeDelay:Lcom/android/server/telecom/secutils/AutoRedialTimeDelay;

.field private mCallBargeIn:Lcom/android/server/telecom/CallBargeIn;

.field private final mCallsManager:Lcom/android/server/telecom/CallsManager;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/telecom/CallsManager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;-><init>()V

    .line 41
    iput-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mCallBargeIn:Lcom/android/server/telecom/CallBargeIn;

    .line 42
    iput-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mAutoRedial:Lcom/android/server/telecom/secutils/AutoRedial;

    .line 43
    iput-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mAutoRedialTimeDelay:Lcom/android/server/telecom/secutils/AutoRedialTimeDelay;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->callBarringPass:Z

    .line 48
    iput-object p1, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mContext:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mCallsManager:Lcom/android/server/telecom/CallsManager;

    .line 50
    const-string v0, "auto_redial_time_delay"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    new-instance v0, Lcom/android/server/telecom/secutils/AutoRedialTimeDelay;

    invoke-direct {v0, p1}, Lcom/android/server/telecom/secutils/AutoRedialTimeDelay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mAutoRedialTimeDelay:Lcom/android/server/telecom/secutils/AutoRedialTimeDelay;

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    new-instance v0, Lcom/android/server/telecom/secutils/AutoRedial;

    invoke-direct {v0, p1}, Lcom/android/server/telecom/secutils/AutoRedial;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mAutoRedial:Lcom/android/server/telecom/secutils/AutoRedial;

    goto :goto_0
.end method


# virtual methods
.method public final onCallAdded(Lcom/android/server/telecom/Call;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onCallAdded : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    invoke-static {}, Lcom/android/server/telecom/CallIdMapper;->sendIncomingVT()V

    .line 62
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mCallBargeIn:Lcom/android/server/telecom/CallBargeIn;

    if-nez v0, :cond_4

    new-instance v0, Lcom/android/server/telecom/CallBargeIn;

    iget-object v1, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/telecom/CallBargeIn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mCallBargeIn:Lcom/android/server/telecom/CallBargeIn;

    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mCallBargeIn:Lcom/android/server/telecom/CallBargeIn;

    invoke-virtual {v0}, Lcom/android/server/telecom/CallBargeIn;->init()V

    .line 63
    :goto_0
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mCallsManager:Lcom/android/server/telecom/CallsManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/CallsManager;->getForegroundCall()Lcom/android/server/telecom/Call;

    move-result-object v0

    .line 64
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->isIncoming()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getState()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/telecom/secutils/AutoAnswer;->checkAutoAnsweringMode(Landroid/content/Context;)V

    .line 67
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 68
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mCallBargeIn:Lcom/android/server/telecom/CallBargeIn;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/telecom/CallBargeIn;->stopBargeIn(I)V

    .line 70
    :cond_1
    const-string v0, "ctc_call_time_duration"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 71
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/android/services/telephony/common/SecCallExtra;->getSecCallExtra(Landroid/os/Bundle;)Lcom/android/services/telephony/common/SecCallExtra;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->isIncoming()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->getPhoneTypeFromCall(Lcom/android/server/telecom/Call;)I

    move-result v1

    if-eq v1, v4, :cond_2

    invoke-static {v3}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->getActiveCall(I)Lcom/android/server/telecom/Call;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 75
    :cond_2
    const-string v1, "setLineCtrl to set call time title"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    invoke-virtual {v0, v4}, Lcom/android/services/telephony/common/SecCallExtra;->setLineCtrl(Z)V

    .line 77
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/services/telephony/common/SecCallExtra;->setSecCallExtraToBundle(Landroid/os/Bundle;Lcom/android/services/telephony/common/SecCallExtra;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/server/telecom/Call;->setExtras(Landroid/os/Bundle;)V

    .line 80
    :cond_3
    return-void

    .line 62
    :cond_4
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mCallBargeIn:Lcom/android/server/telecom/CallBargeIn;

    invoke-virtual {v0}, Lcom/android/server/telecom/CallBargeIn;->updateBargeInState()V

    goto :goto_0
.end method

.method public final onCallRemoved(Lcom/android/server/telecom/Call;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 83
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCallRemoved : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getCallerInfo()Lcom/android/internal/telephony/CallerInfo;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    :cond_0
    invoke-static {v0}, Lcom/android/server/telecom/CallIdMapper;->sendOnDisconnected(Ljava/lang/String;)V

    .line 86
    return-void

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public final onCallStateChanged(Lcom/android/server/telecom/Call;II)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x7

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 90
    const-string v0, "onCallStateChanged %s -> %s, call: %s"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {p2}, Landroid/telecom/CallState;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {p3}, Landroid/telecom/CallState;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    aput-object p1, v1, v6

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    const/4 v0, 0x5

    if-ne p3, v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mCallBargeIn:Lcom/android/server/telecom/CallBargeIn;

    invoke-virtual {v0, v6}, Lcom/android/server/telecom/CallBargeIn;->stopBargeIn(I)V

    .line 96
    :cond_0
    if-eq p3, v8, :cond_1

    const/16 v0, 0x8

    if-ne p3, v0, :cond_2

    .line 97
    :cond_1
    invoke-static {}, Lcom/android/services/telephony/common/SystemDBInterface;->isTPhoneMode()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 98
    const-string v0, "do not wakeUpScreen in TPhoneMode"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    :goto_0
    invoke-static {}, Lcom/android/server/telecom/secutils/MinuteMinder;->stopMinuteMinderAlarm()V

    .line 103
    const-string v0, "auto_redial_time_delay"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 104
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mAutoRedialTimeDelay:Lcom/android/server/telecom/secutils/AutoRedialTimeDelay;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/telecom/secutils/AutoRedialTimeDelay;->startAutoRedialTimeDelay(Lcom/android/server/telecom/Call;I)V

    .line 111
    :goto_1
    const-string v0, "att_volte_ui"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/telecom/secutils/TelecomUtils;->mVolteRedialNumber:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 113
    const-string v0, "VoLTE tc 14.2, redialing..."

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CALL_PRIVILEGED"

    const-string v2, "tel"

    sget-object v3, Lcom/android/server/telecom/secutils/TelecomUtils;->mVolteRedialNumber:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 116
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 117
    iget-object v1, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 118
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/telecom/secutils/TelecomUtils;->mVolteRedialNumber:Ljava/lang/String;

    .line 121
    :cond_2
    const/4 v0, 0x5

    if-ne p3, v0, :cond_3

    .line 122
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/telecom/secutils/MinuteMinder;->startMinuteMinderAlarm(Landroid/content/Context;)V

    .line 124
    :cond_3
    if-ne p2, v7, :cond_5

    const/4 v0, 0x5

    if-ne p3, v0, :cond_5

    .line 125
    const-string v0, "feature_ctc"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->getPhoneTypeFromCall(Lcom/android/server/telecom/Call;)I

    move-result v0

    if-eq v0, v6, :cond_5

    .line 127
    :cond_4
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mContext:Landroid/content/Context;

    const-string v1, "call_answer_vib"

    invoke-static {v0, v1}, Lcom/android/server/telecom/secutils/TelecomUtils;->startCallVibration(Landroid/content/Context;Ljava/lang/String;)Z

    .line 131
    :cond_5
    if-ne p3, v7, :cond_6

    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->getPhoneTypeFromCall(Lcom/android/server/telecom/Call;)I

    move-result v0

    if-ne v0, v6, :cond_6

    .line 132
    iput-boolean v9, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->callBarringPass:Z

    .line 134
    :cond_6
    if-eq p2, v8, :cond_8

    if-ne p3, v8, :cond_8

    .line 135
    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->isSplitFromConference(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "feature_ctc"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->callBarringPass:Z

    if-nez v0, :cond_10

    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->getPhoneTypeFromCall(Lcom/android/server/telecom/Call;)I

    move-result v0

    if-ne v0, v6, :cond_10

    .line 138
    :cond_7
    const-string v0, "if conference call, not vibration "

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    :cond_8
    :goto_2
    if-eq p3, v8, :cond_9

    if-ne p3, v9, :cond_a

    :cond_9
    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->getPhoneTypeFromCall(Lcom/android/server/telecom/Call;)I

    move-result v0

    if-ne v0, v6, :cond_a

    .line 145
    iput-boolean v5, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->callBarringPass:Z

    .line 147
    :cond_a
    const/4 v0, 0x4

    if-eq p2, v0, :cond_b

    if-ne p2, v7, :cond_c

    .line 148
    :cond_b
    const-string v0, "ims_support_multimedia_caller_id"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 149
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_photoring_mt_content_url"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "key_photoring_mt_content_url"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 151
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCallStateChanged url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 152
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 153
    if-eqz v1, :cond_c

    .line 154
    const-string v2, "key_photoring_mt_content_url"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 158
    :cond_c
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mCallBargeIn:Lcom/android/server/telecom/CallBargeIn;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mCallBargeIn:Lcom/android/server/telecom/CallBargeIn;

    invoke-virtual {v0}, Lcom/android/server/telecom/CallBargeIn;->hideBargeInNotification()V

    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mCallBargeIn:Lcom/android/server/telecom/CallBargeIn;

    invoke-virtual {v0}, Lcom/android/server/telecom/CallBargeIn;->updateBargeInState()V

    .line 159
    :cond_d
    return-void

    .line 100
    :cond_e
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/telecom/secutils/TelecomUtils;->wakeUpScreen(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 107
    :cond_f
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mAutoRedial:Lcom/android/server/telecom/secutils/AutoRedial;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/telecom/secutils/AutoRedial;->startAutoRedial(Lcom/android/server/telecom/Call;I)V

    goto/16 :goto_1

    .line 140
    :cond_10
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mContext:Landroid/content/Context;

    const-string v1, "call_end_vib"

    invoke-static {v0, v1}, Lcom/android/server/telecom/secutils/TelecomUtils;->startCallVibration(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_2
.end method

.method public final onIncomingCallAnswered(Lcom/android/server/telecom/Call;)V
    .locals 2

    .prologue
    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onIncomingCallAnswered: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 164
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mCallBargeIn:Lcom/android/server/telecom/CallBargeIn;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/telecom/CallBargeIn;->stopBargeIn(I)V

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/telecom/secutils/TelecomUtils;->sendAcceptIntentForRecorder(Landroid/content/Context;)V

    .line 167
    return-void
.end method
