.class public final Lcom/android/server/telecom/Call;
.super Ljava/lang/Object;
.source "Call.java"

# interfaces
.implements Lcom/android/server/telecom/CreateConnectionResponse;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/telecom/Call$Listener;
    }
.end annotation


# static fields
.field private static final sCallerInfoQueryListener:Lcom/android/internal/telephony/CallerInfoAsyncQuery$OnQueryCompleteListener;

.field private static final sPhotoLoadListener:Lcom/android/server/telecom/ContactsAsyncHelper$OnImageLoadCompleteListener;


# instance fields
.field private mCallCapabilities:I

.field private mCallerDisplayName:Ljava/lang/String;

.field private mCallerDisplayNamePresentation:I

.field private mCallerDualPhoneNumber:Ljava/lang/String;

.field private mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

.field private mCannedSmsResponses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCannedSmsResponsesLoadingStarted:Z

.field private mChildCalls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/server/telecom/Call;",
            ">;"
        }
    .end annotation
.end field

.field private mConferenceLevelActiveCall:Lcom/android/server/telecom/Call;

.field private final mConferenceableCalls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/server/telecom/Call;",
            ">;"
        }
    .end annotation
.end field

.field private mConnectTimeMillis:J

.field private mConnectionManagerPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

.field private mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

.field private final mContext:Landroid/content/Context;

.field private mCreateConnectionProcessor:Lcom/android/server/telecom/CreateConnectionProcessor;

.field private mCreationTimeMillis:J

.field private mDirectToVoiceMailProcessed:Z

.field private mDirectToVoicemailQueryCompleted:Z

.field private final mDirectToVoicemailRunnable:Ljava/lang/Runnable;

.field private mDisconnectCause:Landroid/telecom/DisconnectCause;

.field private mExtras:Landroid/os/Bundle;

.field private mGatewayInfo:Landroid/telecom/GatewayInfo;

.field private mHandle:Landroid/net/Uri;

.field private mHandlePresentation:I

.field private final mHandler:Landroid/os/Handler;

.field private mIncomingVerifyedByConnectionService:Z

.field private mIsConference:Z

.field private mIsEmergencyCall:Z

.field private final mIsIncoming:Z

.field private mIsLocallyDisconnecting:Z

.field private mIsUnknown:Z

.field private mIsVoipAudioMode:Z

.field private final mListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/android/server/telecom/Call$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private mParentCall:Lcom/android/server/telecom/Call;

.field private mQueryToken:I

.field private final mRepository$f45da5c:Lcom/android/server/telecom/ServiceBinder$Listener;

.field private mRingbackRequested:Z

.field private mSpeakerphoneOn:Z

.field private mState:I

.field private mStatusHints:Landroid/telecom/StatusHints;

.field private mTargetPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

.field private mVideoProvider:Lcom/android/internal/telecom/IVideoProvider;

.field private mVideoState:I

.field private mVideoStateHistory:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 159
    new-instance v0, Lcom/android/server/telecom/Call$1;

    invoke-direct {v0}, Lcom/android/server/telecom/Call$1;-><init>()V

    sput-object v0, Lcom/android/server/telecom/Call;->sCallerInfoQueryListener:Lcom/android/internal/telephony/CallerInfoAsyncQuery$OnQueryCompleteListener;

    .line 170
    new-instance v0, Lcom/android/server/telecom/ContactsAsyncHelper$OnImageLoadCompleteListener;

    invoke-direct {v0}, Lcom/android/server/telecom/ContactsAsyncHelper$OnImageLoadCompleteListener;-><init>()V

    sput-object v0, Lcom/android/server/telecom/Call;->sPhotoLoadListener:Lcom/android/server/telecom/ContactsAsyncHelper$OnImageLoadCompleteListener;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/server/telecom/ServiceBinder$Listener;Landroid/net/Uri;Landroid/telecom/GatewayInfo;Landroid/telecom/PhoneAccountHandle;Landroid/telecom/PhoneAccountHandle;ZZ)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    new-instance v1, Lcom/android/server/telecom/Call$3;

    invoke-direct {v1, p0}, Lcom/android/server/telecom/Call$3;-><init>(Lcom/android/server/telecom/Call;)V

    iput-object v1, p0, Lcom/android/server/telecom/Call;->mDirectToVoicemailRunnable:Ljava/lang/Runnable;

    .line 203
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/server/telecom/Call;->mCreationTimeMillis:J

    .line 215
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/android/server/telecom/Call;->mHandler:Landroid/os/Handler;

    .line 217
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/server/telecom/Call;->mConferenceableCalls:Ljava/util/List;

    .line 219
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/server/telecom/Call;->mConnectTimeMillis:J

    .line 261
    new-instance v1, Landroid/telecom/DisconnectCause;

    invoke-direct {v1, v0}, Landroid/telecom/DisconnectCause;-><init>(I)V

    iput-object v1, p0, Lcom/android/server/telecom/Call;->mDisconnectCause:Landroid/telecom/DisconnectCause;

    .line 264
    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iput-object v1, p0, Lcom/android/server/telecom/Call;->mExtras:Landroid/os/Bundle;

    .line 272
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v2, 0x8

    const v3, 0x3f666666    # 0.9f

    invoke-direct {v1, v2, v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    invoke-static {v1}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    .line 281
    iput v0, p0, Lcom/android/server/telecom/Call;->mQueryToken:I

    .line 284
    iput-boolean v0, p0, Lcom/android/server/telecom/Call;->mRingbackRequested:Z

    .line 290
    iput-boolean v0, p0, Lcom/android/server/telecom/Call;->mDirectToVoiceMailProcessed:Z

    .line 292
    iput-boolean v0, p0, Lcom/android/server/telecom/Call;->mIncomingVerifyedByConnectionService:Z

    .line 294
    iput-boolean v0, p0, Lcom/android/server/telecom/Call;->mDirectToVoicemailQueryCompleted:Z

    .line 298
    iput-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsConference:Z

    .line 300
    iput-object v5, p0, Lcom/android/server/telecom/Call;->mParentCall:Lcom/android/server/telecom/Call;

    .line 302
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/android/server/telecom/Call;->mChildCalls:Ljava/util/List;

    .line 305
    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    iput-object v1, p0, Lcom/android/server/telecom/Call;->mCannedSmsResponses:Ljava/util/List;

    .line 308
    iput-boolean v0, p0, Lcom/android/server/telecom/Call;->mCannedSmsResponsesLoadingStarted:Z

    .line 317
    iput-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsLocallyDisconnecting:Z

    .line 321
    iput-object v5, p0, Lcom/android/server/telecom/Call;->mConferenceLevelActiveCall:Lcom/android/server/telecom/Call;

    .line 352
    if-eqz p8, :cond_0

    const/4 v0, 0x5

    :cond_0
    iput v0, p0, Lcom/android/server/telecom/Call;->mState:I

    .line 353
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mContext:Landroid/content/Context;

    .line 354
    iput-object p2, p0, Lcom/android/server/telecom/Call;->mRepository$f45da5c:Lcom/android/server/telecom/ServiceBinder$Listener;

    .line 355
    invoke-virtual {p0, p3}, Lcom/android/server/telecom/Call;->setHandle(Landroid/net/Uri;)V

    .line 356
    invoke-virtual {p0, p3, v4}, Lcom/android/server/telecom/Call;->setHandle(Landroid/net/Uri;I)V

    .line 357
    iput-object p4, p0, Lcom/android/server/telecom/Call;->mGatewayInfo:Landroid/telecom/GatewayInfo;

    .line 358
    invoke-virtual {p0, p5}, Lcom/android/server/telecom/Call;->setConnectionManagerPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    .line 359
    invoke-virtual {p0, p6}, Lcom/android/server/telecom/Call;->setTargetPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    .line 360
    iput-boolean p7, p0, Lcom/android/server/telecom/Call;->mIsIncoming:Z

    .line 361
    iput-boolean p8, p0, Lcom/android/server/telecom/Call;->mIsConference:Z

    .line 362
    invoke-direct {p0}, Lcom/android/server/telecom/Call;->maybeLoadCannedSmsResponses()V

    .line 363
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/server/telecom/ServiceBinder$Listener;Landroid/net/Uri;Landroid/telecom/GatewayInfo;Landroid/telecom/PhoneAccountHandle;Landroid/telecom/PhoneAccountHandle;ZZJ)V
    .locals 11

    .prologue
    .line 374
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v2 .. v10}, Lcom/android/server/telecom/Call;-><init>(Landroid/content/Context;Lcom/android/server/telecom/ServiceBinder$Listener;Landroid/net/Uri;Landroid/telecom/GatewayInfo;Landroid/telecom/PhoneAccountHandle;Landroid/telecom/PhoneAccountHandle;ZZ)V

    .line 375
    move-wide/from16 v0, p9

    iput-wide v0, p0, Lcom/android/server/telecom/Call;->mCreationTimeMillis:J

    .line 376
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/telecom/Call;Lcom/android/internal/telephony/CallerInfo;I)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 74
    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lcom/android/server/telecom/Call;->mQueryToken:I

    if-ne v0, p2, :cond_1

    iput-object p1, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iput-boolean v3, p0, Lcom/android/server/telecom/Call;->mDirectToVoicemailQueryCompleted:Z

    invoke-static {}, Lcom/android/server/telecom/TelecomApp;->getInstance()Lcom/android/server/telecom/TelecomApp;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/server/telecom/secutils/RingingCallAdditionalAsyncQuery;->startAdditionalQuery(Landroid/content/Context;Lcom/android/internal/telephony/CallerInfo;)V

    const-string v0, "CallerInfo received for %s: %s"

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    invoke-static {v2}, Lcom/android/server/telecom/Log;->piiHandle(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    aput-object p1, v1, v3

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iget-object v0, v0, Lcom/android/internal/telephony/CallerInfo;->contactDisplayPhotoUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    const-string v0, "Searching person uri %s for call %s"

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iget-object v2, v2, Lcom/android/internal/telephony/CallerInfo;->contactDisplayPhotoUri:Landroid/net/Uri;

    aput-object v2, v1, v4

    aput-object p0, v1, v3

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/server/telecom/Call;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iget-object v1, v1, Lcom/android/internal/telephony/CallerInfo;->contactDisplayPhotoUri:Landroid/net/Uri;

    sget-object v2, Lcom/android/server/telecom/Call;->sPhotoLoadListener:Lcom/android/server/telecom/ContactsAsyncHelper$OnImageLoadCompleteListener;

    invoke-static {p2, v0, v1, v2, p0}, Lcom/android/server/telecom/ContactsAsyncHelper;->startObtainPhotoAsync(ILandroid/content/Context;Landroid/net/Uri;Lcom/android/server/telecom/ContactsAsyncHelper$OnImageLoadCompleteListener;Ljava/lang/Object;)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mIncomingVerifyedByConnectionService:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/server/telecom/Call;->processDirectToVoicemail()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onCallerInfoChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/android/server/telecom/Call;Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;I)V
    .locals 2

    .prologue
    .line 74
    iget v0, p0, Lcom/android/server/telecom/Call;->mQueryToken:I

    if-ne v0, p3, :cond_0

    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iput-object p1, v0, Lcom/android/internal/telephony/CallerInfo;->cachedPhoto:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iput-object p2, v0, Lcom/android/internal/telephony/CallerInfo;->cachedPhotoIcon:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onCallerInfoChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic access$202(Lcom/android/server/telecom/Call;Z)Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/telecom/Call;->mDirectToVoicemailQueryCompleted:Z

    return v0
.end method

.method static synthetic access$300(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/android/server/telecom/Call;->processDirectToVoicemail()V

    return-void
.end method

.method static synthetic access$402(Lcom/android/server/telecom/Call;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mCannedSmsResponses:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lcom/android/server/telecom/Call;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    return-object v0
.end method

.method private addChildCall(Lcom/android/server/telecom/Call;)V
    .locals 2

    .prologue
    .line 1068
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mChildCalls:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1071
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mConferenceLevelActiveCall:Lcom/android/server/telecom/Call;

    .line 1072
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mChildCalls:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1074
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 1075
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onChildrenChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 1078
    :cond_0
    return-void
.end method

.method static getStateFromConnectionState(I)I
    .locals 1

    .prologue
    const/4 v0, 0x7

    .line 1425
    packed-switch p0, :pswitch_data_0

    .line 1441
    :goto_0
    :pswitch_0
    return v0

    .line 1427
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1429
    :pswitch_2
    const/4 v0, 0x5

    goto :goto_0

    .line 1431
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 1435
    :pswitch_4
    const/4 v0, 0x6

    goto :goto_0

    .line 1437
    :pswitch_5
    const/4 v0, 0x0

    goto :goto_0

    .line 1439
    :pswitch_6
    const/4 v0, 0x4

    goto :goto_0

    .line 1425
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method private isRinging(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1166
    iget v2, p0, Lcom/android/server/telecom/Call;->mState:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 1171
    :goto_0
    return v0

    .line 1170
    :cond_0
    const-string v2, "Request to %s a non-ringing call %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v1

    aput-object p0, v3, v0

    invoke-static {p0, v2, v3}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 1171
    goto :goto_0
.end method

.method private maybeLoadCannedSmsResponses()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1263
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsIncoming:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/server/telecom/Call;->isRespondViaSmsCapable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mCannedSmsResponsesLoadingStarted:Z

    if-nez v0, :cond_0

    .line 1264
    const-string v0, "maybeLoadCannedSmsResponses: starting task to load messages"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1265
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/telecom/Call;->mCannedSmsResponsesLoadingStarted:Z

    .line 1267
    invoke-static {}, Lcom/android/server/telecom/rejectcallwithmessage/SecRespondViaSmsManager;->getInstance()Lcom/android/server/telecom/rejectcallwithmessage/SecRespondViaSmsManager;

    move-result-object v0

    new-instance v1, Lcom/android/server/telecom/Call$4;

    invoke-direct {v1, p0}, Lcom/android/server/telecom/Call$4;-><init>(Lcom/android/server/telecom/Call;)V

    iget-object v2, p0, Lcom/android/server/telecom/Call;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/telecom/rejectcallwithmessage/SecRespondViaSmsManager;->loadCannedTextMessages$499a44e3(Landroid/telecom/Response;)V

    .line 1291
    :goto_0
    return-void

    .line 1289
    :cond_0
    const-string v0, "maybeLoadCannedSmsResponses: doing nothing"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private processDirectToVoicemail()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 690
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mDirectToVoiceMailProcessed:Z

    if-nez v0, :cond_1

    .line 691
    const-string v0, "processDirectToVoicemail"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 692
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iget-boolean v0, v0, Lcom/android/internal/telephony/CallerInfo;->shouldSendToVoicemail:Z

    if-eqz v0, :cond_2

    .line 693
    const-string v0, "Directing call to voicemail: %s."

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 696
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/server/telecom/Call;->setState(I)V

    .line 697
    invoke-virtual {p0}, Lcom/android/server/telecom/Call;->reject$2598ce09()V

    .line 710
    :cond_0
    :goto_0
    iput-boolean v3, p0, Lcom/android/server/telecom/Call;->mDirectToVoiceMailProcessed:Z

    .line 712
    :cond_1
    return-void

    .line 698
    :cond_2
    invoke-static {p0}, Lcom/android/server/telecom/secutils/TelecomUtils;->rejectCallIfShouldBe(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 699
    const-string v0, "this is rejected : %s."

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 705
    :cond_3
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 706
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onSuccessfulIncomingCall(Lcom/android/server/telecom/Call;)V

    goto :goto_1
.end method

.method private removeChildCall(Lcom/android/server/telecom/Call;)V
    .locals 2

    .prologue
    .line 1081
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mChildCalls:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1082
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 1083
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onChildrenChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 1086
    :cond_0
    return-void
.end method


# virtual methods
.method final addListener(Lcom/android/server/telecom/Call$Listener;)V
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 380
    return-void
.end method

.method public final answer(I)V
    .locals 1

    .prologue
    .line 871
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 875
    const-string v0, "answer"

    invoke-direct {p0, v0}, Lcom/android/server/telecom/Call;->isRinging(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 880
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0, p0, p1}, Lcom/android/server/telecom/ConnectionServiceWrapper;->answer(Lcom/android/server/telecom/Call;I)V

    .line 882
    :cond_0
    return-void
.end method

.method final can(I)Z
    .locals 1

    .prologue
    .line 1064
    iget v0, p0, Lcom/android/server/telecom/Call;->mCallCapabilities:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final clearConnectionService()V
    .locals 3

    .prologue
    .line 673
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    if-eqz v0, :cond_0

    .line 674
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    .line 675
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    .line 676
    new-instance v1, Landroid/telecom/DisconnectCause;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v0, p0, v1}, Lcom/android/server/telecom/ConnectionServiceWrapper;->removeCall(Lcom/android/server/telecom/Call;Landroid/telecom/DisconnectCause;)V

    .line 684
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/telecom/ServiceBinder;->decrementAssociatedCallCount()V

    .line 686
    :cond_0
    return-void
.end method

.method final conferenceWith(Lcom/android/server/telecom/Call;)V
    .locals 2

    .prologue
    .line 979
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    if-nez v0, :cond_0

    .line 980
    const-string v0, "conference requested on a call without a connection service."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 984
    :goto_0
    return-void

    .line 982
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0, p0, p1}, Lcom/android/server/telecom/ConnectionServiceWrapper;->conference(Lcom/android/server/telecom/Call;Lcom/android/server/telecom/Call;)V

    goto :goto_0
.end method

.method public final disconnect()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 833
    iput-boolean v2, p0, Lcom/android/server/telecom/Call;->mIsLocallyDisconnecting:Z

    .line 835
    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    if-ne v0, v2, :cond_5

    .line 837
    :cond_0
    const-string v0, "Aborting call %s"

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p0, v1, v3

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 838
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCreateConnectionProcessor:Lcom/android/server/telecom/CreateConnectionProcessor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCreateConnectionProcessor:Lcom/android/server/telecom/CreateConnectionProcessor;

    invoke-virtual {v0}, Lcom/android/server/telecom/CreateConnectionProcessor;->abort()V

    .line 852
    :cond_1
    :goto_0
    return-void

    .line 838
    :cond_2
    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    if-eq v0, v4, :cond_3

    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    if-ne v0, v2, :cond_4

    :cond_3
    new-instance v0, Landroid/telecom/DisconnectCause;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/android/server/telecom/Call;->handleCreateConnectionFailure(Landroid/telecom/DisconnectCause;)V

    goto :goto_0

    :cond_4
    const-string v0, "Cannot abort a call which isn\'t either PRE_DIAL_WAIT or CONNECTING"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 839
    :cond_5
    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_1

    .line 840
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    if-nez v0, :cond_6

    .line 841
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    const-string v1, "disconnect() request on a call without a connection service."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/android/server/telecom/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 844
    :cond_6
    const-string v0, "Send disconnect to connection service for call: %s"

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p0, v1, v3

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 849
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/ConnectionServiceWrapper;->disconnect(Lcom/android/server/telecom/Call;)V

    goto :goto_0
.end method

.method public final getCallCapabilities()I
    .locals 1

    .prologue
    .line 613
    iget v0, p0, Lcom/android/server/telecom/Call;->mCallCapabilities:I

    return v0
.end method

.method public final getCallerDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method final getCallerDisplayNamePresentation()I
    .locals 1

    .prologue
    .line 476
    iget v0, p0, Lcom/android/server/telecom/Call;->mCallerDisplayNamePresentation:I

    return v0
.end method

.method public final getCallerDualPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerDualPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final getCallerInfo()Lcom/android/internal/telephony/CallerInfo;
    .locals 1

    .prologue
    .line 1241
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    return-object v0
.end method

.method final getCannedSmsResponses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1149
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCannedSmsResponses:Ljava/util/List;

    return-object v0
.end method

.method public final getChildCalls()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/server/telecom/Call;",
            ">;"
        }
    .end annotation

    .prologue
    .line 635
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mChildCalls:Ljava/util/List;

    return-object v0
.end method

.method final getConferenceableCalls()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/server/telecom/Call;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConferenceableCalls:Ljava/util/List;

    return-object v0
.end method

.method public final getConnectTimeMillis()J
    .locals 2

    .prologue
    .line 605
    iget-wide v0, p0, Lcom/android/server/telecom/Call;->mConnectTimeMillis:J

    return-wide v0
.end method

.method final getConnectionManagerPhoneAccount()Landroid/telecom/PhoneAccountHandle;
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionManagerPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    return-object v0
.end method

.method final getConnectionService()Lcom/android/server/telecom/ConnectionServiceWrapper;
    .locals 1

    .prologue
    .line 647
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    return-object v0
.end method

.method final getContactUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 958
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iget-boolean v0, v0, Lcom/android/internal/telephony/CallerInfo;->contactExists:Z

    if-nez v0, :cond_1

    .line 959
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    .line 961
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iget-wide v0, v0, Lcom/android/internal/telephony/CallerInfo;->contactIdOrZero:J

    iget-object v2, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iget-object v2, v2, Lcom/android/internal/telephony/CallerInfo;->lookupKey:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public final getCreationTimeMillis()J
    .locals 2

    .prologue
    .line 601
    iget-wide v0, p0, Lcom/android/server/telecom/Call;->mCreationTimeMillis:J

    return-wide v0
.end method

.method public final getDisconnectCause()Landroid/telecom/DisconnectCause;
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mDisconnectCause:Landroid/telecom/DisconnectCause;

    return-object v0
.end method

.method public final getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 940
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mExtras:Landroid/os/Bundle;

    return-object v0
.end method

.method final getGatewayInfo()Landroid/telecom/GatewayInfo;
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mGatewayInfo:Landroid/telecom/GatewayInfo;

    return-object v0
.end method

.method public final getHandle()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    return-object v0
.end method

.method public final getHandlePresentation()I
    .locals 1

    .prologue
    .line 450
    iget v0, p0, Lcom/android/server/telecom/Call;->mHandlePresentation:I

    return v0
.end method

.method public final getIsVoipAudioMode()Z
    .locals 1

    .prologue
    .line 1362
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsVoipAudioMode:Z

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iget-object v0, v0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public final getOriginalHandle()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mGatewayInfo:Landroid/telecom/GatewayInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/telecom/Call;->mGatewayInfo:Landroid/telecom/GatewayInfo;

    invoke-virtual {v0}, Landroid/telecom/GatewayInfo;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mGatewayInfo:Landroid/telecom/GatewayInfo;

    invoke-virtual {v0}, Landroid/telecom/GatewayInfo;->getOriginalAddress()Landroid/net/Uri;

    move-result-object v0

    .line 544
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    goto :goto_0
.end method

.method public final getParentCall()Lcom/android/server/telecom/Call;
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mParentCall:Lcom/android/server/telecom/Call;

    return-object v0
.end method

.method public final getPhoneId()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1385
    const-wide/16 v0, 0x0

    .line 1386
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkMultiSimDsda()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1387
    iget-object v3, p0, Lcom/android/server/telecom/Call;->mTargetPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/server/telecom/Call;->mTargetPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v3}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1388
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mTargetPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v0}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/telecom/secutils/TelecomUtils;->getAccoutSubId(Ljava/lang/String;)J

    move-result-wide v0

    .line 1390
    :cond_0
    invoke-static {v0, v1}, Landroid/telephony/SubscriptionManager;->getPhoneId(J)I

    move-result v0

    .line 1391
    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->isValidPhoneId(I)Z

    move-result v1

    .line 1392
    if-nez v1, :cond_1

    move v0, v2

    .line 1395
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method final getPhoto()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iget-object v0, v0, Lcom/android/internal/telephony/CallerInfo;->cachedPhoto:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method final getPhotoIcon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iget-object v0, v0, Lcom/android/internal/telephony/CallerInfo;->cachedPhotoIcon:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public final getRingtone()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iget-object v0, v0, Lcom/android/internal/telephony/CallerInfo;->contactRingtoneUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method public final getSecConnectionServiceWrapper()Lcom/android/server/telecom/secutils/SecConnectionServiceWrapper;
    .locals 1

    .prologue
    .line 1445
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0}, Lcom/android/server/telecom/ConnectionServiceWrapper;->getSecConnectionServiceWrapper()Lcom/android/server/telecom/secutils/SecConnectionServiceWrapper;

    move-result-object v0

    return-object v0
.end method

.method public final getStartWithSpeakerphoneOn()Z
    .locals 1

    .prologue
    .line 1306
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mSpeakerphoneOn:Z

    return v0
.end method

.method public final getState()I
    .locals 1

    .prologue
    .line 408
    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    return v0
.end method

.method public final getStatusHints()Landroid/telecom/StatusHints;
    .locals 1

    .prologue
    .line 1373
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mStatusHints:Landroid/telecom/StatusHints;

    return-object v0
.end method

.method public final getTargetPhoneAccount()Landroid/telecom/PhoneAccountHandle;
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mTargetPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    return-object v0
.end method

.method public final getVideoProvider()Lcom/android/internal/telecom/IVideoProvider;
    .locals 1

    .prologue
    .line 1323
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mVideoProvider:Lcom/android/internal/telecom/IVideoProvider;

    return-object v0
.end method

.method public final getVideoState()I
    .locals 1

    .prologue
    .line 1331
    iget v0, p0, Lcom/android/server/telecom/Call;->mVideoState:I

    return v0
.end method

.method public final getVideoStateHistory()I
    .locals 1

    .prologue
    .line 1341
    iget v0, p0, Lcom/android/server/telecom/Call;->mVideoStateHistory:I

    return v0
.end method

.method public final handleCreateConnectionFailure(Landroid/telecom/DisconnectCause;)V
    .locals 2

    .prologue
    .line 784
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/telecom/Call;->mCreateConnectionProcessor:Lcom/android/server/telecom/CreateConnectionProcessor;

    .line 785
    invoke-virtual {p0}, Lcom/android/server/telecom/Call;->clearConnectionService()V

    .line 786
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mDisconnectCause:Landroid/telecom/DisconnectCause;

    .line 787
    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/android/server/telecom/CallsManager;->markCallAsDisconnected(Lcom/android/server/telecom/Call;Landroid/telecom/DisconnectCause;)V

    .line 789
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsUnknown:Z

    if-eqz v0, :cond_0

    .line 790
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 791
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onFailedUnknownCall(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 793
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsIncoming:Z

    if-eqz v0, :cond_1

    .line 794
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 795
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onFailedIncomingCall(Lcom/android/server/telecom/Call;)V

    goto :goto_1

    .line 798
    :cond_1
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 799
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onFailedOutgoingCall$47ced4ac(Lcom/android/server/telecom/Call;)V

    goto :goto_2

    .line 802
    :cond_2
    return-void
.end method

.method public final handleCreateConnectionSuccess(Lcom/android/server/telecom/CallIdMapper;Landroid/telecom/ParcelableConnection;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 732
    const-string v0, "handleCreateConnectionSuccessful %s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 733
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/telecom/Call;->mCreateConnectionProcessor:Lcom/android/server/telecom/CreateConnectionProcessor;

    .line 734
    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getPhoneAccount()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/telecom/Call;->setTargetPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    .line 735
    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getHandle()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getHandlePresentation()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/telecom/Call;->setHandle(Landroid/net/Uri;I)V

    .line 736
    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getCallerDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getCallerDisplayNamePresentation()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/telecom/Call;->setCallerDisplayName(Ljava/lang/String;I)V

    .line 738
    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getCallerDualPhoneNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/telecom/Call;->setCallerDualPhoneNumber(Ljava/lang/String;)V

    .line 739
    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getCapabilities()I

    move-result v0

    invoke-virtual {p0, v0, v2}, Lcom/android/server/telecom/Call;->setCallCapabilities(IZ)V

    .line 740
    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getVideoProvider()Lcom/android/internal/telecom/IVideoProvider;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/telecom/Call;->setVideoProvider(Lcom/android/internal/telecom/IVideoProvider;)V

    .line 741
    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getVideoState()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/telecom/Call;->setVideoState(I)V

    .line 742
    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->isRingbackRequested()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/telecom/Call;->setRingbackRequested(Z)V

    .line 743
    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getStatusHints()Landroid/telecom/StatusHints;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/telecom/Call;->setStatusHints(Landroid/telecom/StatusHints;)V

    .line 745
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConferenceableCalls:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 746
    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getConferenceableConnectionIds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 747
    iget-object v2, p0, Lcom/android/server/telecom/Call;->mConferenceableCalls:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/android/server/telecom/CallIdMapper;->getCall(Ljava/lang/Object;)Lcom/android/server/telecom/Call;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 750
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsUnknown:Z

    if-eqz v0, :cond_1

    .line 751
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 752
    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getState()I

    move-result v2

    invoke-static {v2}, Lcom/android/server/telecom/Call;->getStateFromConnectionState(I)I

    move-result v2

    invoke-virtual {v0, p0, v2}, Lcom/android/server/telecom/Call$Listener;->onSuccessfulUnknownCall(Lcom/android/server/telecom/Call;I)V

    goto :goto_1

    .line 754
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsIncoming:Z

    if-eqz v0, :cond_4

    .line 761
    iput-boolean v3, p0, Lcom/android/server/telecom/Call;->mIncomingVerifyedByConnectionService:Z

    .line 764
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mDirectToVoicemailQueryCompleted:Z

    if-eqz v0, :cond_3

    .line 765
    invoke-direct {p0}, Lcom/android/server/telecom/Call;->processDirectToVoicemail()V

    .line 779
    :cond_2
    :goto_2
    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getIsVoipAudioMode()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/telecom/Call;->setIsVoipAudioMode(Z)V

    .line 780
    return-void

    .line 769
    :cond_3
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/telecom/Call;->mDirectToVoicemailRunnable:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/android/server/telecom/Call;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/telecom/BluetoothManager;->getDirectToVoicemailMillis(Landroid/content/ContentResolver;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    .line 773
    :cond_4
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 774
    invoke-virtual {p2}, Landroid/telecom/ParcelableConnection;->getState()I

    move-result v2

    invoke-static {v2}, Lcom/android/server/telecom/Call;->getStateFromConnectionState(I)I

    move-result v2

    invoke-virtual {v0, p0, v2}, Lcom/android/server/telecom/Call$Listener;->onSuccessfulOutgoingCall(Lcom/android/server/telecom/Call;I)V

    goto :goto_3
.end method

.method public final hold()V
    .locals 2

    .prologue
    .line 904
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 906
    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 907
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/ConnectionServiceWrapper;->hold(Lcom/android/server/telecom/Call;)V

    .line 909
    :cond_0
    return-void
.end method

.method final isActive()Z
    .locals 2

    .prologue
    .line 936
    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isAlive()Z
    .locals 1

    .prologue
    .line 924
    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    packed-switch v0, :pswitch_data_0

    .line 931
    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 929
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 924
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final isConference()Z
    .locals 1

    .prologue
    .line 442
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsConference:Z

    return v0
.end method

.method public final isEmergencyCall()Z
    .locals 1

    .prologue
    .line 533
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsEmergencyCall:Z

    return v0
.end method

.method public final isIncoming()Z
    .locals 1

    .prologue
    .line 584
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsIncoming:Z

    return v0
.end method

.method public final isLocallyDisconnecting()Z
    .locals 1

    .prologue
    .line 1412
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsLocallyDisconnecting:Z

    return v0
.end method

.method final isRespondViaSmsCapable()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1109
    iget v2, p0, Lcom/android/server/telecom/Call;->mState:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_1

    .line 1145
    :cond_0
    :goto_0
    return v0

    .line 1113
    :cond_1
    iget-object v2, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    if-eqz v2, :cond_0

    .line 1119
    iget-object v2, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isUriNumber(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1129
    iget-object v2, p0, Lcom/android/server/telecom/Call;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/android/internal/telephony/SmsApplication;->getDefaultRespondViaMessageApplication(Landroid/content/Context;Z)Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1134
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 1135
    invoke-static {}, Lcom/android/server/telecom/rejectcallwithmessage/SecRespondViaSmsManager;->getInstance()Lcom/android/server/telecom/rejectcallwithmessage/SecRespondViaSmsManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/android/server/telecom/Call;->mHandlePresentation:I

    invoke-virtual {v0, v1, v2}, Lcom/android/server/telecom/rejectcallwithmessage/SecRespondViaSmsManager;->isRespondViaSmsCapable(Ljava/lang/String;I)Z

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1145
    goto :goto_0
.end method

.method final isRingbackRequested()Z
    .locals 1

    .prologue
    .line 438
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mRingbackRequested:Z

    return v0
.end method

.method public final isUnknown()Z
    .locals 1

    .prologue
    .line 1399
    iget-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsUnknown:Z

    return v0
.end method

.method final mergeConference()V
    .locals 2

    .prologue
    .line 995
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    if-nez v0, :cond_1

    .line 996
    const-string v0, "merging conference calls without a connection service."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 999
    :cond_0
    :goto_0
    return-void

    .line 997
    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/server/telecom/Call;->can(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 998
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/ConnectionServiceWrapper;->mergeConference(Lcom/android/server/telecom/Call;)V

    goto :goto_0
.end method

.method final onPostDialWait(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 969
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 970
    invoke-virtual {v0, p0, p1}, Lcom/android/server/telecom/Call$Listener;->onPostDialWait(Lcom/android/server/telecom/Call;Ljava/lang/String;)V

    goto :goto_0

    .line 972
    :cond_0
    return-void
.end method

.method final playDtmfTone(C)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 808
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    if-nez v0, :cond_0

    .line 809
    const-string v0, "playDtmfTone() request on a call without a connection service."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 814
    :goto_0
    return-void

    .line 811
    :cond_0
    const-string v0, "Send playDtmfTone to connection service for call %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 812
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0, p0, p1}, Lcom/android/server/telecom/ConnectionServiceWrapper;->playDtmfTone(Lcom/android/server/telecom/Call;C)V

    goto :goto_0
.end method

.method final postDialContinue(Z)V
    .locals 1

    .prologue
    .line 975
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0, p0, p1}, Lcom/android/server/telecom/ConnectionServiceWrapper;->onPostDialContinue(Lcom/android/server/telecom/Call;Z)V

    .line 976
    return-void
.end method

.method public final reject$2598ce09()V
    .locals 1

    .prologue
    .line 891
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 895
    const-string v0, "reject"

    invoke-direct {p0, v0}, Lcom/android/server/telecom/Call;->isRinging(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 896
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/ConnectionServiceWrapper;->reject(Lcom/android/server/telecom/Call;)V

    .line 898
    :cond_0
    return-void
.end method

.method final removeListener(Lcom/android/server/telecom/Call$Listener;)V
    .locals 1

    .prologue
    .line 383
    if-eqz p1, :cond_0

    .line 384
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 386
    :cond_0
    return-void
.end method

.method final setCallCapabilities(I)V
    .locals 1

    .prologue
    .line 617
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/server/telecom/Call;->setCallCapabilities(IZ)V

    .line 618
    return-void
.end method

.method final setCallCapabilities(IZ)V
    .locals 4

    .prologue
    .line 621
    const-string v0, "setCallCapabilities: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/telecom/PhoneCapabilities;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 622
    if-nez p2, :cond_0

    iget v0, p0, Lcom/android/server/telecom/Call;->mCallCapabilities:I

    if-eq v0, p1, :cond_1

    .line 623
    :cond_0
    iput p1, p0, Lcom/android/server/telecom/Call;->mCallCapabilities:I

    .line 624
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 625
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onCallCapabilitiesChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 628
    :cond_1
    return-void
.end method

.method final setCallerDisplayName(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 480
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerDisplayName:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/telecom/Call;->mCallerDisplayNamePresentation:I

    if-eq p2, v0, :cond_2

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 483
    :goto_0
    invoke-static {v0, p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->getOperatorCallerDisplayName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 485
    iput-object v0, p0, Lcom/android/server/telecom/Call;->mCallerDisplayName:Ljava/lang/String;

    .line 486
    iput p2, p0, Lcom/android/server/telecom/Call;->mCallerDisplayNamePresentation:I

    .line 487
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 488
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onCallerDisplayNameChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_1

    .line 482
    :cond_1
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 491
    :cond_2
    return-void
.end method

.method final setCallerDualPhoneNumber(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 498
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCallerDualPhoneNumber:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 499
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mCallerDualPhoneNumber:Ljava/lang/String;

    .line 500
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 501
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onCallerDualPhoneNumberChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 504
    :cond_0
    return-void
.end method

.method final setConferenceableCalls(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/server/telecom/Call;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1051
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConferenceableCalls:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1052
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConferenceableCalls:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1054
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 1055
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onConferenceableCallsChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 1057
    :cond_0
    return-void
.end method

.method public final setConnectTimeMillis(J)V
    .locals 1

    .prologue
    .line 609
    iput-wide p1, p0, Lcom/android/server/telecom/Call;->mConnectTimeMillis:J

    .line 610
    return-void
.end method

.method final setConnectionManagerPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V
    .locals 2

    .prologue
    .line 560
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionManagerPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 561
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mConnectionManagerPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    .line 562
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 567
    :cond_0
    return-void
.end method

.method final setConnectionService(Lcom/android/server/telecom/ConnectionServiceWrapper;)V
    .locals 1

    .prologue
    .line 660
    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 662
    invoke-virtual {p0}, Lcom/android/server/telecom/Call;->clearConnectionService()V

    .line 664
    invoke-virtual {p1}, Lcom/android/server/telecom/ConnectionServiceWrapper;->incrementAssociatedCallCount()V

    .line 665
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    .line 666
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/ConnectionServiceWrapper;->addCall(Lcom/android/server/telecom/Call;)V

    .line 667
    return-void
.end method

.method final setDisconnectCause(Landroid/telecom/DisconnectCause;)V
    .locals 0

    .prologue
    .line 525
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mDisconnectCause:Landroid/telecom/DisconnectCause;

    .line 526
    return-void
.end method

.method public final setExtras(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 944
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mExtras:Landroid/os/Bundle;

    .line 945
    return-void
.end method

.method final setGatewayInfo(Landroid/telecom/GatewayInfo;)V
    .locals 0

    .prologue
    .line 552
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mGatewayInfo:Landroid/telecom/GatewayInfo;

    .line 553
    return-void
.end method

.method final setHandle(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 455
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/server/telecom/Call;->setHandle(Landroid/net/Uri;I)V

    .line 456
    return-void
.end method

.method final setHandle(Landroid/net/Uri;I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 459
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    invoke-static {p1, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/telecom/Call;->mHandlePresentation:I

    if-eq p2, v0, :cond_4

    .line 460
    :cond_0
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    .line 461
    iput p2, p0, Lcom/android/server/telecom/Call;->mHandlePresentation:I

    .line 462
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/telecom/Call;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumber(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsEmergencyCall:Z

    .line 464
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    if-nez v0, :cond_3

    move-object v0, v3

    :goto_1
    iget v4, p0, Lcom/android/server/telecom/Call;->mQueryToken:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/android/server/telecom/Call;->mQueryToken:I

    iput-object v3, p0, Lcom/android/server/telecom/Call;->mCallerInfo:Lcom/android/internal/telephony/CallerInfo;

    iput-boolean v2, p0, Lcom/android/server/telecom/Call;->mDirectToVoicemailQueryCompleted:Z

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "Looking up information for: %s."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/server/telecom/Log;->piiHandle(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {p0, v3, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    iget v1, p0, Lcom/android/server/telecom/Call;->mQueryToken:I

    iget-object v2, p0, Lcom/android/server/telecom/Call;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/server/telecom/Call;->sCallerInfoQueryListener:Lcom/android/internal/telephony/CallerInfoAsyncQuery$OnQueryCompleteListener;

    invoke-static {v1, v2, v0, v3, p0}, Lcom/android/internal/telephony/CallerInfoAsyncQuery;->startQuery(ILandroid/content/Context;Ljava/lang/String;Lcom/android/internal/telephony/CallerInfoAsyncQuery$OnQueryCompleteListener;Ljava/lang/Object;)Lcom/android/internal/telephony/CallerInfoAsyncQuery;

    .line 465
    :cond_1
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 466
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onHandleChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_2

    :cond_2
    move v0, v2

    .line 462
    goto :goto_0

    .line 464
    :cond_3
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 469
    :cond_4
    return-void
.end method

.method public final setIsUnknown(Z)V
    .locals 1

    .prologue
    .line 1403
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/telecom/Call;->mIsUnknown:Z

    .line 1404
    return-void
.end method

.method public final setIsVoipAudioMode(Z)V
    .locals 2

    .prologue
    .line 1366
    iput-boolean p1, p0, Lcom/android/server/telecom/Call;->mIsVoipAudioMode:Z

    .line 1367
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 1368
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onIsVoipAudioModeChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 1370
    :cond_0
    return-void
.end method

.method final setParentCall(Lcom/android/server/telecom/Call;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1026
    if-ne p1, p0, :cond_1

    .line 1027
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    const-string v2, "setting the parent to self"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v0}, Lcom/android/server/telecom/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1048
    :cond_0
    return-void

    .line 1030
    :cond_1
    iget-object v1, p0, Lcom/android/server/telecom/Call;->mParentCall:Lcom/android/server/telecom/Call;

    if-eq p1, v1, :cond_0

    .line 1034
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/android/server/telecom/Call;->mParentCall:Lcom/android/server/telecom/Call;

    if-nez v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkState(Z)V

    .line 1036
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mParentCall:Lcom/android/server/telecom/Call;

    .line 1037
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mParentCall:Lcom/android/server/telecom/Call;

    if-eqz v0, :cond_4

    .line 1038
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mParentCall:Lcom/android/server/telecom/Call;

    invoke-direct {v0, p0}, Lcom/android/server/telecom/Call;->removeChildCall(Lcom/android/server/telecom/Call;)V

    .line 1040
    :cond_4
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mParentCall:Lcom/android/server/telecom/Call;

    .line 1041
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mParentCall:Lcom/android/server/telecom/Call;

    if-eqz v0, :cond_5

    .line 1042
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mParentCall:Lcom/android/server/telecom/Call;

    invoke-direct {v0, p0}, Lcom/android/server/telecom/Call;->addChildCall(Lcom/android/server/telecom/Call;)V

    .line 1045
    :cond_5
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 1046
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onParentChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0
.end method

.method final setRingbackRequested(Z)V
    .locals 3

    .prologue
    .line 431
    iput-boolean p1, p0, Lcom/android/server/telecom/Call;->mRingbackRequested:Z

    .line 432
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 433
    iget-boolean v2, p0, Lcom/android/server/telecom/Call;->mRingbackRequested:Z

    invoke-virtual {v0, p0, v2}, Lcom/android/server/telecom/Call$Listener;->onRingbackRequested(Lcom/android/server/telecom/Call;Z)V

    goto :goto_0

    .line 435
    :cond_0
    return-void
.end method

.method public final setStartWithSpeakerphoneOn(Z)V
    .locals 0

    .prologue
    .line 1297
    iput-boolean p1, p0, Lcom/android/server/telecom/Call;->mSpeakerphoneOn:Z

    .line 1298
    return-void
.end method

.method public final setState(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 418
    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    if-eq v0, p1, :cond_0

    .line 419
    const-string v0, "setState %s -> %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/server/telecom/Call;->mState:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 420
    iput p1, p0, Lcom/android/server/telecom/Call;->mState:I

    .line 421
    invoke-direct {p0}, Lcom/android/server/telecom/Call;->maybeLoadCannedSmsResponses()V

    .line 423
    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 424
    iput-boolean v4, p0, Lcom/android/server/telecom/Call;->mIsLocallyDisconnecting:Z

    .line 425
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/telecom/Call;->setParentCall(Lcom/android/server/telecom/Call;)V

    .line 428
    :cond_0
    return-void
.end method

.method public final setStatusHints(Landroid/telecom/StatusHints;)V
    .locals 2

    .prologue
    .line 1377
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mStatusHints:Landroid/telecom/StatusHints;

    .line 1378
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 1379
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onStatusHintsChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 1381
    :cond_0
    return-void
.end method

.method final setTargetPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V
    .locals 2

    .prologue
    .line 574
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setTargetPhoneAccount() accountHandle = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 575
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mTargetPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 576
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mTargetPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    .line 577
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 578
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onTargetPhoneAccountChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 581
    :cond_0
    return-void
.end method

.method public final setVideoProvider(Lcom/android/internal/telecom/IVideoProvider;)V
    .locals 2

    .prologue
    .line 1313
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mVideoProvider:Lcom/android/internal/telecom/IVideoProvider;

    .line 1314
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 1315
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onVideoCallProviderChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 1317
    :cond_0
    return-void
.end method

.method public final setVideoState(I)V
    .locals 2

    .prologue
    .line 1353
    iget v0, p0, Lcom/android/server/telecom/Call;->mVideoStateHistory:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/android/server/telecom/Call;->mVideoStateHistory:I

    .line 1355
    iput p1, p0, Lcom/android/server/telecom/Call;->mVideoState:I

    .line 1356
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 1357
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onVideoStateChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 1359
    :cond_0
    return-void
.end method

.method final splitFromConference()V
    .locals 2

    .prologue
    .line 987
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    if-nez v0, :cond_0

    .line 988
    const-string v0, "splitting from conference call without a connection service"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 992
    :goto_0
    return-void

    .line 990
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/ConnectionServiceWrapper;->splitFromConference(Lcom/android/server/telecom/Call;)V

    goto :goto_0
.end method

.method final startCreateConnection(Lcom/android/server/telecom/PhoneAccountRegistrar;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 721
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startCreateConnection() phoneAccountRegistrar = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 722
    iget-object v1, p0, Lcom/android/server/telecom/Call;->mCreateConnectionProcessor:Lcom/android/server/telecom/CreateConnectionProcessor;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkState(Z)V

    .line 723
    new-instance v0, Lcom/android/server/telecom/CreateConnectionProcessor;

    iget-object v2, p0, Lcom/android/server/telecom/Call;->mRepository$f45da5c:Lcom/android/server/telecom/ServiceBinder$Listener;

    iget-object v5, p0, Lcom/android/server/telecom/Call;->mContext:Landroid/content/Context;

    move-object v1, p0

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/server/telecom/CreateConnectionProcessor;-><init>(Lcom/android/server/telecom/Call;Lcom/android/server/telecom/ServiceBinder$Listener;Lcom/android/server/telecom/CreateConnectionResponse;Lcom/android/server/telecom/PhoneAccountRegistrar;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/Call;->mCreateConnectionProcessor:Lcom/android/server/telecom/CreateConnectionProcessor;

    .line 725
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mCreateConnectionProcessor:Lcom/android/server/telecom/CreateConnectionProcessor;

    invoke-virtual {v0}, Lcom/android/server/telecom/CreateConnectionProcessor;->process()V

    .line 726
    return-void
.end method

.method final stopDtmfTone()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 820
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    if-nez v0, :cond_0

    .line 821
    const-string v0, "stopDtmfTone() request on a call without a connection service."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 826
    :goto_0
    return-void

    .line 823
    :cond_0
    const-string v0, "Send stopDtmfTone to connection service for call %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 824
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/ConnectionServiceWrapper;->stopDtmfTone(Lcom/android/server/telecom/Call;)V

    goto :goto_0
.end method

.method public final swapConference()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1004
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    if-nez v0, :cond_1

    .line 1005
    const-string v0, "swapping conference calls without a connection service."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1023
    :cond_0
    :goto_0
    return-void

    .line 1006
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/server/telecom/Call;->can(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1007
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/ConnectionServiceWrapper;->swapConference(Lcom/android/server/telecom/Call;)V

    .line 1008
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mChildCalls:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1019
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/telecom/Call;->mConferenceLevelActiveCall:Lcom/android/server/telecom/Call;

    goto :goto_0

    .line 1010
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mChildCalls:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    iput-object v0, p0, Lcom/android/server/telecom/Call;->mConferenceLevelActiveCall:Lcom/android/server/telecom/Call;

    goto :goto_0

    .line 1014
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mChildCalls:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/telecom/Call;->mConferenceLevelActiveCall:Lcom/android/server/telecom/Call;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/server/telecom/Call;->mChildCalls:Ljava/util/List;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    :goto_1
    iput-object v0, p0, Lcom/android/server/telecom/Call;->mConferenceLevelActiveCall:Lcom/android/server/telecom/Call;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mChildCalls:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    goto :goto_1

    .line 1008
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 391
    const/4 v0, 0x0

    .line 392
    iget-object v3, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v3}, Lcom/android/server/telecom/ConnectionServiceWrapper;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 393
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0}, Lcom/android/server/telecom/ConnectionServiceWrapper;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    .line 396
    :cond_0
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "[%s, %s, %s, %s, %d, childs(%d), has_parent(%b), [%s]"

    const/16 v5, 0x8

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    iget v6, p0, Lcom/android/server/telecom/Call;->mState:I

    invoke-static {v6}, Landroid/telecom/CallState;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v6, 0x2

    aput-object v0, v5, v6

    const/4 v0, 0x3

    iget-object v6, p0, Lcom/android/server/telecom/Call;->mHandle:Landroid/net/Uri;

    invoke-static {v6}, Lcom/android/server/telecom/Log;->piiHandle(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x4

    iget v6, p0, Lcom/android/server/telecom/Call;->mVideoState:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x5

    iget-object v6, p0, Lcom/android/server/telecom/Call;->mChildCalls:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x6

    iget-object v0, p0, Lcom/android/server/telecom/Call;->mParentCall:Lcom/android/server/telecom/Call;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x7

    iget v1, p0, Lcom/android/server/telecom/Call;->mCallCapabilities:I

    invoke-static {v1}, Landroid/telecom/PhoneCapabilities;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method final unhold()V
    .locals 2

    .prologue
    .line 915
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 917
    iget v0, p0, Lcom/android/server/telecom/Call;->mState:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 918
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mConnectionService:Lcom/android/server/telecom/ConnectionServiceWrapper;

    invoke-virtual {v0, p0}, Lcom/android/server/telecom/ConnectionServiceWrapper;->unhold(Lcom/android/server/telecom/Call;)V

    .line 920
    :cond_0
    return-void
.end method

.method public final updateExtras(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 948
    iput-object p1, p0, Lcom/android/server/telecom/Call;->mExtras:Landroid/os/Bundle;

    .line 949
    iget-object v0, p0, Lcom/android/server/telecom/Call;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call$Listener;

    .line 950
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call$Listener;->onExtraChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 952
    :cond_0
    return-void
.end method
