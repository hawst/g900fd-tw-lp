.class public final Lcom/android/server/telecom/CallsManager;
.super Lcom/android/server/telecom/Call$Listener;
.source "CallsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/telecom/CallsManager$CallsManagerListener;
    }
.end annotation


# static fields
.field private static INSTANCE:Lcom/android/server/telecom/CallsManager;

.field private static final LIVE_CALL_STATES:[I

.field private static final OUTGOING_CALL_STATES:[I


# instance fields
.field private defaultSubscriptionChanged:Z

.field private isNeedAddLog:Z

.field public final mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

.field private final mCalls:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/android/server/telecom/Call;",
            ">;"
        }
    .end annotation
.end field

.field private final mConnectionServiceRepository$f45da5c:Lcom/android/server/telecom/ServiceBinder$Listener;

.field private final mContext:Landroid/content/Context;

.field private final mDtmfLocalTonePlayer:Lcom/android/server/telecom/DtmfLocalTonePlayer;

.field private mForegroundCall:Lcom/android/server/telecom/Call;

.field private final mHeadsetMediaButton:Lcom/android/server/telecom/HeadsetMediaButton;

.field private final mInCallController:Lcom/android/server/telecom/InCallController;

.field public final mInCallToneMonitor:Lcom/android/server/telecom/InCallToneMonitor;

.field private final mListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/android/server/telecom/CallsManager$CallsManagerListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mLocallyDisconnectingCalls:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/android/server/telecom/Call;",
            ">;"
        }
    .end annotation
.end field

.field private final mMissedCallNotifier:Lcom/android/server/telecom/MissedCallNotifier;

.field private final mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

.field private mPhoneState:I

.field private final mPhoneStateBroadcaster:Lcom/android/server/telecom/PhoneStateBroadcaster;

.field private final mProximitySensorManager:Lcom/android/server/telecom/ProximitySensorManager;

.field private final mRinger:Lcom/android/server/telecom/Ringer;

.field public final mSecCallLogManager:Lcom/android/server/telecom/secutils/SecCallLogManager;

.field private final mSecCallsManagerListener:Lcom/android/server/telecom/secutils/SecCallsManagerListener;

.field private final mSecPhoneStateListener:Lcom/android/server/telecom/secutils/SecPhoneStateListener;

.field private final mTtyManager:Lcom/android/server/telecom/TtyManager;

.field private mWB_AMR_state:Z

.field public final mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

.field private prevSubId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/telecom/CallsManager;->INSTANCE:Lcom/android/server/telecom/CallsManager;

    .line 119
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/server/telecom/CallsManager;->LIVE_CALL_STATES:[I

    .line 122
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/server/telecom/CallsManager;->OUTGOING_CALL_STATES:[I

    return-void

    .line 119
    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x5
    .end array-data

    .line 122
    :array_1
    .array-data 4
        0x1
        0x2
        0x3
    .end array-data
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/server/telecom/MissedCallNotifier;Lcom/android/server/telecom/PhoneAccountRegistrar;)V
    .locals 7

    .prologue
    const v4, 0x3f666666    # 0.9f

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 188
    invoke-direct {p0}, Lcom/android/server/telecom/Call$Listener;-><init>()V

    .line 115
    iput-boolean v2, p0, Lcom/android/server/telecom/CallsManager;->defaultSubscriptionChanged:Z

    .line 117
    iput-boolean v3, p0, Lcom/android/server/telecom/CallsManager;->isNeedAddLog:Z

    .line 135
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x8

    invoke-direct {v0, v1, v4, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    .line 145
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x10

    invoke-direct {v0, v1, v4, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    .line 158
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mLocallyDisconnectingCalls:Ljava/util/Set;

    .line 166
    iput v2, p0, Lcom/android/server/telecom/CallsManager;->mPhoneState:I

    .line 168
    iput-boolean v2, p0, Lcom/android/server/telecom/CallsManager;->mWB_AMR_state:Z

    .line 189
    iput-object p1, p0, Lcom/android/server/telecom/CallsManager;->mContext:Landroid/content/Context;

    .line 190
    iput-object p3, p0, Lcom/android/server/telecom/CallsManager;->mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

    .line 191
    iput-object p2, p0, Lcom/android/server/telecom/CallsManager;->mMissedCallNotifier:Lcom/android/server/telecom/MissedCallNotifier;

    .line 192
    new-instance v6, Lcom/android/server/telecom/StatusBarNotifier;

    invoke-direct {v6, p1, p0}, Lcom/android/server/telecom/StatusBarNotifier;-><init>(Landroid/content/Context;Lcom/android/server/telecom/CallsManager;)V

    .line 193
    new-instance v0, Lcom/android/server/telecom/WiredHeadsetManager;

    invoke-direct {v0, p1}, Lcom/android/server/telecom/WiredHeadsetManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

    .line 194
    new-instance v0, Lcom/android/server/telecom/CallAudioManager;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

    invoke-direct {v0, p1, v6, v1}, Lcom/android/server/telecom/CallAudioManager;-><init>(Landroid/content/Context;Lcom/android/server/telecom/StatusBarNotifier;Lcom/android/server/telecom/WiredHeadsetManager;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    .line 195
    new-instance v3, Lcom/android/server/telecom/InCallTonePlayer$Factory;

    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    invoke-direct {v3, v0}, Lcom/android/server/telecom/InCallTonePlayer$Factory;-><init>(Lcom/android/server/telecom/CallAudioManager;)V

    .line 196
    new-instance v0, Lcom/android/server/telecom/Ringer;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    iget-object v5, p0, Lcom/android/server/telecom/CallsManager;->mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

    move-object v2, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/server/telecom/Ringer;-><init>(Lcom/android/server/telecom/CallAudioManager;Lcom/android/server/telecom/CallsManager;Lcom/android/server/telecom/InCallTonePlayer$Factory;Landroid/content/Context;Lcom/android/server/telecom/WiredHeadsetManager;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mRinger:Lcom/android/server/telecom/Ringer;

    .line 197
    new-instance v0, Lcom/android/server/telecom/HeadsetMediaButton;

    invoke-direct {v0, p1, p0}, Lcom/android/server/telecom/HeadsetMediaButton;-><init>(Landroid/content/Context;Lcom/android/server/telecom/CallsManager;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mHeadsetMediaButton:Lcom/android/server/telecom/HeadsetMediaButton;

    .line 198
    new-instance v0, Lcom/android/server/telecom/TtyManager;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mWiredHeadsetManager:Lcom/android/server/telecom/WiredHeadsetManager;

    invoke-direct {v0, p1, v1}, Lcom/android/server/telecom/TtyManager;-><init>(Landroid/content/Context;Lcom/android/server/telecom/WiredHeadsetManager;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mTtyManager:Lcom/android/server/telecom/TtyManager;

    .line 199
    new-instance v0, Lcom/android/server/telecom/ProximitySensorManager;

    invoke-direct {v0, p1}, Lcom/android/server/telecom/ProximitySensorManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mProximitySensorManager:Lcom/android/server/telecom/ProximitySensorManager;

    .line 200
    new-instance v0, Lcom/android/server/telecom/PhoneStateBroadcaster;

    invoke-direct {v0}, Lcom/android/server/telecom/PhoneStateBroadcaster;-><init>()V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mPhoneStateBroadcaster:Lcom/android/server/telecom/PhoneStateBroadcaster;

    .line 201
    new-instance v0, Lcom/android/server/telecom/secutils/SecPhoneStateListener;

    invoke-direct {v0, p1}, Lcom/android/server/telecom/secutils/SecPhoneStateListener;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mSecPhoneStateListener:Lcom/android/server/telecom/secutils/SecPhoneStateListener;

    .line 202
    new-instance v0, Lcom/android/server/telecom/secutils/SecCallLogManager;

    invoke-direct {v0, p1}, Lcom/android/server/telecom/secutils/SecCallLogManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mSecCallLogManager:Lcom/android/server/telecom/secutils/SecCallLogManager;

    .line 203
    new-instance v0, Lcom/android/server/telecom/secutils/SecCallsManagerListener;

    invoke-direct {v0, p1, p0}, Lcom/android/server/telecom/secutils/SecCallsManagerListener;-><init>(Landroid/content/Context;Lcom/android/server/telecom/CallsManager;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mSecCallsManagerListener:Lcom/android/server/telecom/secutils/SecCallsManagerListener;

    .line 204
    new-instance v0, Lcom/android/server/telecom/InCallController;

    invoke-direct {v0, p1}, Lcom/android/server/telecom/InCallController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mInCallController:Lcom/android/server/telecom/InCallController;

    .line 205
    new-instance v0, Lcom/android/server/telecom/DtmfLocalTonePlayer;

    invoke-direct {v0, p1}, Lcom/android/server/telecom/DtmfLocalTonePlayer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mDtmfLocalTonePlayer:Lcom/android/server/telecom/DtmfLocalTonePlayer;

    .line 206
    new-instance v0, Lcom/android/server/telecom/ServiceBinder$Listener;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

    invoke-direct {v0, v1, p1}, Lcom/android/server/telecom/ServiceBinder$Listener;-><init>(Lcom/android/server/telecom/PhoneAccountRegistrar;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mConnectionServiceRepository$f45da5c:Lcom/android/server/telecom/ServiceBinder$Listener;

    .line 208
    new-instance v0, Lcom/android/server/telecom/InCallToneMonitor;

    invoke-direct {v0, v3, p0}, Lcom/android/server/telecom/InCallToneMonitor;-><init>(Lcom/android/server/telecom/InCallTonePlayer$Factory;Lcom/android/server/telecom/CallsManager;)V

    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mInCallToneMonitor:Lcom/android/server/telecom/InCallToneMonitor;

    .line 210
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mSecCallLogManager:Lcom/android/server/telecom/secutils/SecCallLogManager;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mPhoneStateBroadcaster:Lcom/android/server/telecom/PhoneStateBroadcaster;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 213
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mInCallController:Lcom/android/server/telecom/InCallController;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 214
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mRinger:Lcom/android/server/telecom/Ringer;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 215
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    new-instance v1, Lcom/android/server/telecom/RingbackPlayer;

    invoke-direct {v1, p0, v3}, Lcom/android/server/telecom/RingbackPlayer;-><init>(Lcom/android/server/telecom/CallsManager;Lcom/android/server/telecom/InCallTonePlayer$Factory;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 216
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mInCallToneMonitor:Lcom/android/server/telecom/InCallToneMonitor;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 217
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 218
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 219
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mDtmfLocalTonePlayer:Lcom/android/server/telecom/DtmfLocalTonePlayer;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 220
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mHeadsetMediaButton:Lcom/android/server/telecom/HeadsetMediaButton;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 222
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-static {}, Lcom/android/server/telecom/rejectcallwithmessage/SecRespondViaSmsManager;->getInstance()Lcom/android/server/telecom/rejectcallwithmessage/SecRespondViaSmsManager;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 223
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mProximitySensorManager:Lcom/android/server/telecom/ProximitySensorManager;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 224
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mSecCallsManagerListener:Lcom/android/server/telecom/secutils/SecCallsManagerListener;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 225
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mSecPhoneStateListener:Lcom/android/server/telecom/secutils/SecPhoneStateListener;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 226
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-static {}, Lcom/android/server/telecom/operator/usa/TelecomExtensionManager;->getInstance()Lcom/android/server/telecom/operator/usa/TelecomExtensionManager;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 227
    return-void
.end method

.method private addCall(Lcom/android/server/telecom/Call;)V
    .locals 3

    .prologue
    .line 1233
    const-string v0, "addCall(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1235
    invoke-virtual {p1, p0}, Lcom/android/server/telecom/Call;->addListener(Lcom/android/server/telecom/Call$Listener;)V

    .line 1236
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1240
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/CallsManager$CallsManagerListener;

    .line 1241
    invoke-virtual {v0, p1}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;->onCallAdded(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 1243
    :cond_0
    invoke-direct {p0}, Lcom/android/server/telecom/CallsManager;->updateForegroundCall()V

    .line 1244
    return-void
.end method

.method static conference(Lcom/android/server/telecom/Call;Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 678
    invoke-virtual {p0, p1}, Lcom/android/server/telecom/Call;->conferenceWith(Lcom/android/server/telecom/Call;)V

    .line 679
    return-void
.end method

.method private varargs getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;
    .locals 7

    .prologue
    .line 1142
    array-length v2, p2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_5

    aget v3, p2, v1

    .line 1144
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 1145
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkMultiSimDsda()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1146
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getPhoneId()I

    move-result v0

    iget-object v4, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    invoke-virtual {v4}, Lcom/android/server/telecom/Call;->getPhoneId()I

    move-result v4

    if-ne v0, v4, :cond_1

    .line 1147
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    .line 1174
    :goto_1
    return-object v0

    .line 1149
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    goto :goto_1

    .line 1153
    :cond_1
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    .line 1154
    invoke-static {p1, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1155
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getParentCall()Lcom/android/server/telecom/Call;

    move-result-object v5

    if-nez v5, :cond_2

    .line 1160
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkMultiSimDsda()Z

    move-result v5

    if-eqz v5, :cond_3

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getPhoneId()I

    move-result v5

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getPhoneId()I

    move-result v6

    if-ne v5, v6, :cond_2

    .line 1166
    :cond_3
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getState()I

    move-result v5

    if-ne v3, v5, :cond_2

    goto :goto_1

    .line 1142
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1174
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static getInstance()Lcom/android/server/telecom/CallsManager;
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lcom/android/server/telecom/CallsManager;->INSTANCE:Lcom/android/server/telecom/CallsManager;

    return-object v0
.end method

.method private varargs getNumCallsWithState([I)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1446
    array-length v3, p1

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    aget v4, p1, v2

    .line 1447
    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    .line 1448
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 1449
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 1451
    goto :goto_1

    .line 1446
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v1

    goto :goto_0

    .line 1453
    :cond_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method static initialize(Lcom/android/server/telecom/CallsManager;)V
    .locals 0

    .prologue
    .line 181
    sput-object p0, Lcom/android/server/telecom/CallsManager;->INSTANCE:Lcom/android/server/telecom/CallsManager;

    .line 182
    return-void
.end method

.method private makeRoomForOutgoingCall(Lcom/android/server/telecom/Call;Z)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1473
    sget-object v0, Lcom/android/server/telecom/CallsManager;->LIVE_CALL_STATES:[I

    invoke-direct {p0, v0}, Lcom/android/server/telecom/CallsManager;->getNumCallsWithState([I)I

    move-result v0

    if-gt v1, v0, :cond_2

    move v0, v1

    .line 1474
    :goto_0
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkMultiSimDsda()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1478
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-static {v0, p1}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->hasMultiSimMaximumLiveCalls(Ljava/util/Collection;Lcom/android/server/telecom/Call;)Z

    move-result v0

    .line 1480
    :cond_0
    if-eqz v0, :cond_d

    .line 1483
    sget-object v0, Lcom/android/server/telecom/CallsManager;->LIVE_CALL_STATES:[I

    invoke-direct {p0, p1, v0}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v3

    .line 1485
    if-ne p1, v3, :cond_3

    .line 1557
    :cond_1
    :goto_1
    return v1

    :cond_2
    move v0, v2

    .line 1473
    goto :goto_0

    .line 1492
    :cond_3
    sget-object v0, Lcom/android/server/telecom/CallsManager;->OUTGOING_CALL_STATES:[I

    invoke-direct {p0, v0}, Lcom/android/server/telecom/CallsManager;->getNumCallsWithState([I)I

    move-result v0

    if-gt v1, v0, :cond_5

    move v0, v1

    .line 1493
    :goto_2
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkMultiSimDsda()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1494
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-static {v0, p1}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->hasMultiSimMaximumOutgoingCalls(Ljava/util/Collection;Lcom/android/server/telecom/Call;)Z

    move-result v0

    .line 1496
    :cond_4
    if-eqz v0, :cond_7

    .line 1500
    if-eqz p2, :cond_6

    .line 1501
    sget-object v0, Lcom/android/server/telecom/CallsManager;->OUTGOING_CALL_STATES:[I

    const/4 v3, 0x0

    invoke-direct {p0, v3, v0}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v0

    .line 1502
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->isEmergencyCall()Z

    move-result v3

    if-nez v3, :cond_6

    .line 1503
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->disconnect()V

    goto :goto_1

    :cond_5
    move v0, v2

    .line 1492
    goto :goto_2

    :cond_6
    move v1, v2

    .line 1507
    goto :goto_1

    .line 1510
    :cond_7
    new-array v0, v1, [I

    const/4 v4, 0x6

    aput v4, v0, v2

    invoke-direct {p0, v0}, Lcom/android/server/telecom/CallsManager;->getNumCallsWithState([I)I

    move-result v0

    if-gt v1, v0, :cond_9

    move v0, v1

    .line 1511
    :goto_3
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkMultiSimDsda()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1512
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-static {v0, p1}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->hasMultiSimMaximumHoldingCalls(Ljava/util/Collection;Lcom/android/server/telecom/Call;)Z

    move-result v0

    .line 1514
    :cond_8
    if-eqz v0, :cond_b

    .line 1516
    if-eqz p2, :cond_a

    .line 1519
    invoke-virtual {v3}, Lcom/android/server/telecom/Call;->disconnect()V

    goto :goto_1

    :cond_9
    move v0, v2

    .line 1510
    goto :goto_3

    :cond_a
    move v1, v2

    .line 1522
    goto :goto_1

    .line 1530
    :cond_b
    invoke-virtual {v3}, Lcom/android/server/telecom/Call;->getTargetPhoneAccount()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getTargetPhoneAccount()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-static {v0, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1532
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getTargetPhoneAccount()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1543
    invoke-virtual {v3, v1}, Lcom/android/server/telecom/Call;->can(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1544
    invoke-virtual {v3}, Lcom/android/server/telecom/Call;->hold()V

    goto :goto_1

    :cond_c
    move v1, v2

    .line 1549
    goto :goto_1

    .line 1552
    :cond_d
    const-string v0, "feature_ctc"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkMultiSimDsda()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1553
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    sget-object v2, Lcom/android/server/telecom/CallsManager;->LIVE_CALL_STATES:[I

    invoke-static {v0, p1, v2}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->getOtherSlotFirstCallWithState(Ljava/util/Collection;Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v0

    .line 1554
    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Lcom/android/server/telecom/Call;->can(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1555
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->hold()V

    goto/16 :goto_1
.end method

.method private setCallState(Lcom/android/server/telecom/Call;I)V
    .locals 5

    .prologue
    .line 1288
    if-nez p1, :cond_1

    .line 1319
    :cond_0
    :goto_0
    return-void

    .line 1291
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getState()I

    move-result v1

    .line 1292
    const-string v0, "setCallState %s -> %s, call: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v1}, Landroid/telecom/CallState;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Landroid/telecom/CallState;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    invoke-static {p0, v0, v2}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1294
    if-eq p2, v1, :cond_0

    .line 1302
    invoke-virtual {p1, p2}, Lcom/android/server/telecom/Call;->setState(I)V

    .line 1303
    invoke-direct {p0, p2}, Lcom/android/server/telecom/CallsManager;->updatePhoneState(I)V

    .line 1305
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1306
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/CallsManager$CallsManagerListener;

    .line 1307
    invoke-virtual {v0, p1, v1, p2}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;->onCallStateChanged(Lcom/android/server/telecom/Call;II)V

    goto :goto_1

    .line 1309
    :cond_2
    invoke-direct {p0}, Lcom/android/server/telecom/CallsManager;->updateForegroundCall()V

    goto :goto_0

    .line 1314
    :cond_3
    iget-boolean v0, p0, Lcom/android/server/telecom/CallsManager;->isNeedAddLog:Z

    if-eqz v0, :cond_0

    .line 1315
    invoke-static {p1, p2}, Lcom/android/server/telecom/secutils/TelecomUtils;->addCallLogForNotRegisteredCall$44b8cb48(Lcom/android/server/telecom/Call;I)V

    goto :goto_0
.end method

.method private updateForegroundCall()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1364
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v2

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    .line 1371
    const-string v4, "feature_multisim_dsda"

    invoke-static {v4}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, Lcom/android/server/telecom/CallsManager;->LIVE_CALL_STATES:[I

    invoke-direct {p0, v2, v4}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-static {v4}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->isDualCallRinging(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->isDualSim()Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getPhoneId()I

    move-result v4

    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->getMultiSimDSDAForegoundPhoneId()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 1375
    :cond_2
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getParentCall()Lcom/android/server/telecom/Call;

    move-result-object v4

    if-nez v4, :cond_0

    .line 1380
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->isActive()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getState()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_5

    .line 1397
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    if-eq v0, v1, :cond_7

    .line 1398
    const-string v1, "Updating foreground call, %s -> %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1399
    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    .line 1400
    iput-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    .line 1402
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    if-eqz v0, :cond_4

    .line 1403
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    iget-boolean v2, p0, Lcom/android/server/telecom/CallsManager;->mWB_AMR_state:Z

    invoke-static {v0, v2}, Lcom/android/server/telecom/secutils/TelecomUtils;->updateWideBand(Lcom/android/server/telecom/Call;Z)V

    .line 1406
    :cond_4
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/CallsManager$CallsManagerListener;

    .line 1407
    iget-object v3, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    invoke-virtual {v0, v1, v3}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;->onForegroundCallChanged(Lcom/android/server/telecom/Call;Lcom/android/server/telecom/Call;)V

    goto :goto_2

    .line 1391
    :cond_5
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->isAlive()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getState()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_8

    :cond_6
    :goto_3
    move-object v1, v0

    .line 1395
    goto/16 :goto_0

    .line 1410
    :cond_7
    return-void

    :cond_8
    move-object v0, v1

    goto :goto_3

    :cond_9
    move-object v0, v1

    goto :goto_1
.end method

.method private updatePhoneState(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1322
    packed-switch p1, :pswitch_data_0

    .line 1334
    iput v2, p0, Lcom/android/server/telecom/CallsManager;->mPhoneState:I

    .line 1337
    :goto_0
    iget v0, p0, Lcom/android/server/telecom/CallsManager;->mPhoneState:I

    if-nez v0, :cond_2

    .line 1339
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-direct {p0, v5, v0}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1340
    iput v3, p0, Lcom/android/server/telecom/CallsManager;->mPhoneState:I

    .line 1345
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    if-ne v0, v4, :cond_1

    const-string v0, "Ringing"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v3}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 1346
    iput v1, p0, Lcom/android/server/telecom/CallsManager;->mPhoneState:I

    .line 1349
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "updatePhoneState: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/server/telecom/CallsManager;->mPhoneState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1350
    return-void

    .line 1326
    :pswitch_0
    iput v3, p0, Lcom/android/server/telecom/CallsManager;->mPhoneState:I

    goto :goto_0

    .line 1329
    :pswitch_1
    iput v1, p0, Lcom/android/server/telecom/CallsManager;->mPhoneState:I

    goto :goto_0

    .line 1341
    :cond_3
    new-array v0, v1, [I

    aput v4, v0, v2

    invoke-direct {p0, v5, v0}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1342
    const-string v0, "foreground call is ringing"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v3}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1343
    iput v1, p0, Lcom/android/server/telecom/CallsManager;->mPhoneState:I

    goto :goto_1

    .line 1345
    :cond_4
    const-string v0, "isRinging()...false"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v3}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    goto :goto_2

    .line 1322
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1339
    :array_0
    .array-data 4
        0x3
        0x5
        0x6
    .end array-data
.end method


# virtual methods
.method final addListener(Lcom/android/server/telecom/CallsManager$CallsManagerListener;)V
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 414
    return-void
.end method

.method final addNewUnknownCall(Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 456
    const-string v0, "android.telecom.extra.UNKNOWN_CALL_HANDLE"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 457
    const-string v0, "addNewUnknownCall with handle: %s"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v3}, Lcom/android/server/telecom/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 458
    new-instance v0, Lcom/android/server/telecom/Call;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/telecom/CallsManager;->mConnectionServiceRepository$f45da5c:Lcom/android/server/telecom/ServiceBinder$Listener;

    move-object v5, v4

    move-object v6, p1

    invoke-direct/range {v0 .. v8}, Lcom/android/server/telecom/Call;-><init>(Landroid/content/Context;Lcom/android/server/telecom/ServiceBinder$Listener;Landroid/net/Uri;Landroid/telecom/GatewayInfo;Landroid/telecom/PhoneAccountHandle;Landroid/telecom/PhoneAccountHandle;ZZ)V

    .line 469
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/android/server/telecom/Call;->setConnectTimeMillis(J)V

    .line 470
    invoke-virtual {v0, v7}, Lcom/android/server/telecom/Call;->setIsUnknown(Z)V

    .line 471
    invoke-virtual {v0, p2}, Lcom/android/server/telecom/Call;->setExtras(Landroid/os/Bundle;)V

    .line 472
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call;->addListener(Lcom/android/server/telecom/Call$Listener;)V

    .line 473
    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

    invoke-virtual {v0, v1}, Lcom/android/server/telecom/Call;->startCreateConnection(Lcom/android/server/telecom/PhoneAccountRegistrar;)V

    .line 474
    return-void
.end method

.method public final answerCall(Lcom/android/server/telecom/Call;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 690
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 691
    const-string v0, "Request to answer a non-existent call %s"

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 726
    :goto_0
    return-void

    .line 695
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    if-eq v0, p1, :cond_2

    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->isActive()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 698
    :cond_1
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getCallCapabilities()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_3

    .line 702
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getConnectionService()Lcom/android/server/telecom/ConnectionServiceWrapper;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getConnectionService()Lcom/android/server/telecom/ConnectionServiceWrapper;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 703
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->disconnect()V

    .line 715
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/CallsManager$CallsManagerListener;

    .line 716
    invoke-virtual {v0, p1}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;->onIncomingCallAnswered(Lcom/android/server/telecom/Call;)V

    goto :goto_2

    .line 706
    :cond_3
    const-string v0, "Holding active/dialing call %s before answering incoming call %s."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 708
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->hold()V

    goto :goto_1

    .line 719
    :cond_4
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkMultiSimDsda()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 720
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getPhoneId()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->setAudioPath(I)V

    .line 724
    :cond_5
    invoke-virtual {p1, p2}, Lcom/android/server/telecom/Call;->answer(I)V

    goto :goto_0
.end method

.method final createConferenceCall(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ParcelableConference;)Lcom/android/server/telecom/Call;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 1180
    new-instance v0, Lcom/android/server/telecom/Call;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/telecom/CallsManager;->mConnectionServiceRepository$f45da5c:Lcom/android/server/telecom/ServiceBinder$Listener;

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v8}, Lcom/android/server/telecom/Call;-><init>(Landroid/content/Context;Lcom/android/server/telecom/ServiceBinder$Listener;Landroid/net/Uri;Landroid/telecom/GatewayInfo;Landroid/telecom/PhoneAccountHandle;Landroid/telecom/PhoneAccountHandle;ZZ)V

    .line 1190
    invoke-virtual {p2}, Landroid/telecom/ParcelableConference;->getState()I

    move-result v1

    invoke-static {v1}, Lcom/android/server/telecom/Call;->getStateFromConnectionState(I)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/server/telecom/CallsManager;->setCallState(Lcom/android/server/telecom/Call;I)V

    .line 1191
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getState()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 1192
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/android/server/telecom/Call;->setConnectTimeMillis(J)V

    .line 1194
    :cond_0
    invoke-virtual {p2}, Landroid/telecom/ParcelableConference;->getCapabilities()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/telecom/Call;->setCallCapabilities(I)V

    .line 1197
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call;->addListener(Lcom/android/server/telecom/Call$Listener;)V

    .line 1198
    invoke-direct {p0, v0}, Lcom/android/server/telecom/CallsManager;->addCall(Lcom/android/server/telecom/Call;)V

    .line 1199
    return-object v0
.end method

.method public final disconnectAllCalls()V
    .locals 2

    .prologue
    .line 817
    const-string v0, "disconnectAllCalls"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 819
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    .line 820
    invoke-virtual {p0, v0}, Lcom/android/server/telecom/CallsManager;->disconnectCall(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 822
    :cond_0
    return-void
.end method

.method final disconnectCall(Lcom/android/server/telecom/Call;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 803
    const-string v0, "disconnectCall %s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 805
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 806
    const-string v0, "Unknown call (%s) asked to disconnect"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 811
    :goto_0
    return-void

    .line 808
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mLocallyDisconnectingCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 809
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->disconnect()V

    goto :goto_0
.end method

.method public final dump(Lcom/android/internal/util/IndentingPrintWriter;)V
    .locals 3

    .prologue
    .line 1599
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.DUMP"

    const-string v2, "CallsManager"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 1600
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    .line 1601
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 1602
    const-string v0, "mCalls: "

    invoke-virtual {p1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 1603
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    .line 1604
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    .line 1605
    invoke-virtual {p1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 1607
    :cond_0
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    .line 1609
    :cond_1
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    .line 1610
    return-void
.end method

.method public final getActiveCall()Lcom/android/server/telecom/Call;
    .locals 3

    .prologue
    .line 1109
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x5

    aput v2, v0, v1

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v0

    return-object v0
.end method

.method public final getAudioState()Landroid/telecom/AudioState;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    iget-object v0, v0, Lcom/android/server/telecom/CallAudioManager;->mAudioState:Landroid/telecom/AudioState;

    return-object v0
.end method

.method public final getCallState()I
    .locals 1

    .prologue
    .line 1207
    iget v0, p0, Lcom/android/server/telecom/CallsManager;->mPhoneState:I

    return v0
.end method

.method public final getCalls()Lcom/google/common/collect/ImmutableCollection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableCollection",
            "<",
            "Lcom/android/server/telecom/Call;",
            ">;"
        }
    .end annotation

    .prologue
    .line 376
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method final getCurrentTtyMode()I
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mTtyManager:Lcom/android/server/telecom/TtyManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/TtyManager;->getCurrentTtyMode()I

    move-result v0

    return v0
.end method

.method final getDialingCall()Lcom/android/server/telecom/Call;
    .locals 3

    .prologue
    .line 1113
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x3

    aput v2, v0, v1

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v0

    return-object v0
.end method

.method public final varargs getFirstCallWithState([I)Lcom/android/server/telecom/Call;
    .locals 1

    .prologue
    .line 1131
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v0

    return-object v0
.end method

.method public final getForegroundCall()Lcom/android/server/telecom/Call;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    return-object v0
.end method

.method public final getHeldCall()Lcom/android/server/telecom/Call;
    .locals 3

    .prologue
    .line 1117
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x6

    aput v2, v0, v1

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v0

    return-object v0
.end method

.method final getInCallController()Lcom/android/server/telecom/InCallController;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mInCallController:Lcom/android/server/telecom/InCallController;

    return-object v0
.end method

.method public final getMissedCallNotifier()Lcom/android/server/telecom/MissedCallNotifier;
    .locals 1

    .prologue
    .line 1224
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mMissedCallNotifier:Lcom/android/server/telecom/MissedCallNotifier;

    return-object v0
.end method

.method final getNumHeldCalls()I
    .locals 4

    .prologue
    .line 1121
    const/4 v0, 0x0

    .line 1122
    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    .line 1123
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getParentCall()Lcom/android/server/telecom/Call;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    const/4 v3, 0x6

    if-ne v0, v3, :cond_1

    .line 1124
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    .line 1126
    goto :goto_0

    .line 1127
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method final getPhoneAccountRegistrar()Lcom/android/server/telecom/PhoneAccountRegistrar;
    .locals 1

    .prologue
    .line 1216
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

    return-object v0
.end method

.method public final getRinger()Lcom/android/server/telecom/Ringer;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mRinger:Lcom/android/server/telecom/Ringer;

    return-object v0
.end method

.method public final getRingingCall()Lcom/android/server/telecom/Call;
    .locals 3

    .prologue
    .line 1105
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x4

    aput v2, v0, v1

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v0

    return-object v0
.end method

.method public final getSecCallLogManager()Lcom/android/server/telecom/secutils/SecCallLogManager;
    .locals 1

    .prologue
    .line 1568
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mSecCallLogManager:Lcom/android/server/telecom/secutils/SecCallLogManager;

    return-object v0
.end method

.method public final getSecInCallController()Lcom/android/server/telecom/secutils/SecInCallController;
    .locals 1

    .prologue
    .line 1561
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mInCallController:Lcom/android/server/telecom/InCallController;

    if-eqz v0, :cond_0

    .line 1562
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mInCallController:Lcom/android/server/telecom/InCallController;

    invoke-virtual {v0}, Lcom/android/server/telecom/InCallController;->getSecInCallController()Lcom/android/server/telecom/secutils/SecInCallController;

    move-result-object v0

    .line 1564
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final handleConnectionServiceDeath(Lcom/android/server/telecom/ConnectionServiceWrapper;)V
    .locals 4

    .prologue
    .line 1015
    if-eqz p1, :cond_1

    .line 1016
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    .line 1017
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getConnectionService()Lcom/android/server/telecom/ConnectionServiceWrapper;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 1018
    new-instance v2, Landroid/telecom/DisconnectCause;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {p0, v0, v2}, Lcom/android/server/telecom/CallsManager;->markCallAsDisconnected(Lcom/android/server/telecom/Call;Landroid/telecom/DisconnectCause;)V

    goto :goto_0

    .line 1022
    :cond_1
    return-void
.end method

.method public final hasActiveOrHoldingCall()Z
    .locals 2

    .prologue
    .line 1029
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x5
        0x6
    .end array-data
.end method

.method public final hasAnyCalls()Z
    .locals 1

    .prologue
    .line 1025
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final hasEmergencyCall()Z
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    .line 393
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->isEmergencyCall()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    const/4 v0, 0x1

    .line 397
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasRingingCall()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1033
    new-array v2, v0, [I

    const/4 v3, 0x4

    aput v3, v2, v1

    const/4 v3, 0x0

    invoke-direct {p0, v3, v2}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method final holdCall(Lcom/android/server/telecom/Call;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 831
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 832
    const-string v0, "Unknown call (%s) asked to be put on hold"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 837
    :goto_0
    return-void

    .line 834
    :cond_0
    const-string v0, "Putting call on hold: (%s)"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 835
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->hold()V

    goto :goto_0
.end method

.method protected final isAddCallCapable(Lcom/android/server/telecom/Call;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1068
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getParentCall()Lcom/android/server/telecom/Call;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1101
    :cond_0
    :goto_0
    return v2

    .line 1075
    :cond_1
    const/16 v0, 0x80

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getCallCapabilities()I

    move-result v3

    and-int/lit16 v3, v3, 0x80

    if-ne v0, v3, :cond_4

    move v0, v1

    .line 1077
    :goto_1
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->isConference()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_0

    .line 1083
    :cond_2
    const-string v0, "show_add_call_menu_for_3rd_call"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1084
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    .line 1085
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkMultiSimDsda()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1086
    if-eq p1, v0, :cond_3

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getParentCall()Lcom/android/server/telecom/Call;

    move-result-object v4

    if-nez v4, :cond_3

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getPhoneId()I

    move-result v4

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getPhoneId()I

    move-result v0

    if-ne v4, v0, :cond_3

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1075
    goto :goto_1

    .line 1091
    :cond_5
    if-eq p1, v0, :cond_3

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getParentCall()Lcom/android/server/telecom/Call;

    move-result-object v0

    if-nez v0, :cond_3

    goto :goto_0

    .line 1097
    :cond_6
    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkOperatorAddCallCapable(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v1

    .line 1101
    goto :goto_0
.end method

.method public final isInCall()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 591
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    .line 592
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->isActive()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 593
    const-string v0, "isInCall()...Active"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v2}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 602
    :goto_0
    return v0

    .line 596
    :cond_1
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->isAlive()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    const/4 v4, 0x4

    if-ne v0, v4, :cond_0

    .line 597
    :cond_2
    const-string v0, "isInCall()...Alive or Ringing"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v2}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 598
    goto :goto_0

    .line 601
    :cond_3
    const-string v0, "isInCall()...false"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 602
    goto :goto_0
.end method

.method final isTtySupported()Z
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mTtyManager:Lcom/android/server/telecom/TtyManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/TtyManager;->isTtySupported()Z

    move-result v0

    return v0
.end method

.method final markCallAsActive(Lcom/android/server/telecom/Call;)V
    .locals 4

    .prologue
    .line 966
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getConnectTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 967
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/android/server/telecom/Call;->setConnectTimeMillis(J)V

    .line 969
    :cond_0
    const/4 v0, 0x5

    invoke-direct {p0, p1, v0}, Lcom/android/server/telecom/CallsManager;->setCallState(Lcom/android/server/telecom/Call;I)V

    .line 971
    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->setRouteSpeakerWithStartphoneOn(Lcom/android/server/telecom/Call;)V

    .line 972
    return-void
.end method

.method final markCallAsDialing(Lcom/android/server/telecom/Call;)V
    .locals 1

    .prologue
    .line 960
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/android/server/telecom/CallsManager;->setCallState(Lcom/android/server/telecom/Call;I)V

    .line 962
    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->setRouteSpeakerWithStartphoneOn(Lcom/android/server/telecom/Call;)V

    .line 963
    return-void
.end method

.method final markCallAsDisconnected(Lcom/android/server/telecom/Call;Landroid/telecom/DisconnectCause;)V
    .locals 1

    .prologue
    .line 985
    invoke-virtual {p1, p2}, Lcom/android/server/telecom/Call;->setDisconnectCause(Landroid/telecom/DisconnectCause;)V

    .line 986
    const/4 v0, 0x7

    invoke-direct {p0, p1, v0}, Lcom/android/server/telecom/CallsManager;->setCallState(Lcom/android/server/telecom/Call;I)V

    .line 987
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->sendRequestLastCall()V

    .line 989
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/server/telecom/secutils/TelecomUtils;->setIMSPreRegState(Z)V

    .line 990
    return-void
.end method

.method final markCallAsOnHold(Lcom/android/server/telecom/Call;)V
    .locals 1

    .prologue
    .line 975
    const/4 v0, 0x6

    invoke-direct {p0, p1, v0}, Lcom/android/server/telecom/CallsManager;->setCallState(Lcom/android/server/telecom/Call;I)V

    .line 976
    return-void
.end method

.method final markCallAsRemoved(Lcom/android/server/telecom/Call;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 996
    const-string v0, "removeCall(%s)"

    new-array v3, v1, [Ljava/lang/Object;

    aput-object p1, v3, v2

    invoke-static {p0, v0, v3}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/server/telecom/Call;->setParentCall(Lcom/android/server/telecom/Call;)V

    invoke-virtual {p1, p0}, Lcom/android/server/telecom/Call;->removeListener(Lcom/android/server/telecom/Call$Listener;)V

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->clearConnectionService()V

    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/CallsManager$CallsManagerListener;

    invoke-virtual {v0, p1}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;->onCallRemoved(Lcom/android/server/telecom/Call;)V

    goto :goto_1

    :cond_0
    invoke-direct {p0}, Lcom/android/server/telecom/CallsManager;->updateForegroundCall()V

    :cond_1
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/telecom/CallsManager;->updatePhoneState(I)V

    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getCallCapabilities()I

    move-result v4

    invoke-virtual {v0, v4, v1}, Lcom/android/server/telecom/Call;->setCallCapabilities(IZ)V

    goto :goto_2

    :cond_2
    iget-boolean v0, p0, Lcom/android/server/telecom/CallsManager;->defaultSubscriptionChanged:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lcom/android/server/telecom/CallsManager;->prevSubId:J

    invoke-static {v0, v1}, Landroid/telephony/SubscriptionManager;->setDefaultVoiceSubId(J)V

    iput-boolean v2, p0, Lcom/android/server/telecom/CallsManager;->defaultSubscriptionChanged:Z

    .line 997
    :cond_3
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mLocallyDisconnectingCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 998
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mLocallyDisconnectingCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 999
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getState()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_4

    .line 1000
    const-string v0, "markCallAsRemoved don\'t call unhold "

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1006
    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method final markCallAsRinging(Lcom/android/server/telecom/Call;)V
    .locals 1

    .prologue
    .line 956
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/android/server/telecom/CallsManager;->setCallState(Lcom/android/server/telecom/Call;I)V

    .line 957
    return-void
.end method

.method final mute(Z)V
    .locals 1

    .prologue
    .line 866
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    invoke-virtual {v0, p1}, Lcom/android/server/telecom/CallAudioManager;->mute(Z)V

    .line 867
    return-void
.end method

.method final onAudioStateChanged(Landroid/telecom/AudioState;Landroid/telecom/AudioState;)V
    .locals 3

    .prologue
    .line 941
    const-string v0, "onAudioStateChanged, audioState: %s -> %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 942
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/CallsManager$CallsManagerListener;

    .line 943
    invoke-virtual {v0, p1, p2}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;->onAudioStateChanged(Landroid/telecom/AudioState;Landroid/telecom/AudioState;)V

    goto :goto_0

    .line 945
    :cond_0
    return-void
.end method

.method public final bridge synthetic onCallCapabilitiesChanged(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/android/server/telecom/Call$Listener;->onCallCapabilitiesChanged(Lcom/android/server/telecom/Call;)V

    return-void
.end method

.method public final bridge synthetic onCallerDisplayNameChanged(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/android/server/telecom/Call$Listener;->onCallerDisplayNameChanged(Lcom/android/server/telecom/Call;)V

    return-void
.end method

.method public final bridge synthetic onCallerDualPhoneNumberChanged(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/android/server/telecom/Call$Listener;->onCallerDualPhoneNumberChanged(Lcom/android/server/telecom/Call;)V

    return-void
.end method

.method public final bridge synthetic onCallerInfoChanged(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/android/server/telecom/Call$Listener;->onCallerInfoChanged(Lcom/android/server/telecom/Call;)V

    return-void
.end method

.method public final bridge synthetic onCannedSmsResponsesLoaded(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/android/server/telecom/Call$Listener;->onCannedSmsResponsesLoaded(Lcom/android/server/telecom/Call;)V

    return-void
.end method

.method final onChangeInContent(I)V
    .locals 2

    .prologue
    .line 949
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onChangeInContent::"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 950
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/CallsManager$CallsManagerListener;

    .line 951
    invoke-virtual {v0, p1}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;->onChangeInContent(I)V

    goto :goto_0

    .line 953
    :cond_0
    return-void
.end method

.method public final onChildrenChanged(Lcom/android/server/telecom/Call;)V
    .locals 2

    .prologue
    .line 355
    invoke-direct {p0}, Lcom/android/server/telecom/CallsManager;->updateForegroundCall()V

    .line 356
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/CallsManager$CallsManagerListener;

    .line 357
    invoke-virtual {v0, p1}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;->onIsConferencedChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 359
    :cond_0
    return-void
.end method

.method public final bridge synthetic onConferenceableCallsChanged(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/android/server/telecom/Call$Listener;->onConferenceableCallsChanged(Lcom/android/server/telecom/Call;)V

    return-void
.end method

.method public final bridge synthetic onExtraChanged(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/android/server/telecom/Call$Listener;->onExtraChanged(Lcom/android/server/telecom/Call;)V

    return-void
.end method

.method public final onFailedIncomingCall(Lcom/android/server/telecom/Call;)V
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x7

    invoke-direct {p0, p1, v0}, Lcom/android/server/telecom/CallsManager;->setCallState(Lcom/android/server/telecom/Call;I)V

    .line 314
    invoke-virtual {p1, p0}, Lcom/android/server/telecom/Call;->removeListener(Lcom/android/server/telecom/Call$Listener;)V

    .line 315
    return-void
.end method

.method public final onFailedOutgoingCall$47ced4ac(Lcom/android/server/telecom/Call;)V
    .locals 3

    .prologue
    .line 255
    const-string v0, "onFailedOutgoingCall, call: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 256
    invoke-virtual {p0, p1}, Lcom/android/server/telecom/CallsManager;->markCallAsRemoved(Lcom/android/server/telecom/Call;)V

    .line 257
    return-void
.end method

.method public final onFailedUnknownCall(Lcom/android/server/telecom/Call;)V
    .locals 3

    .prologue
    .line 326
    const-string v0, "onFailedUnknownCall for call %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 327
    const/4 v0, 0x7

    invoke-direct {p0, p1, v0}, Lcom/android/server/telecom/CallsManager;->setCallState(Lcom/android/server/telecom/Call;I)V

    .line 328
    invoke-virtual {p1, p0}, Lcom/android/server/telecom/Call;->removeListener(Lcom/android/server/telecom/Call$Listener;)V

    .line 329
    return-void
.end method

.method public final bridge synthetic onHandleChanged(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/android/server/telecom/Call$Listener;->onHandleChanged(Lcom/android/server/telecom/Call;)V

    return-void
.end method

.method public final onIsVoipAudioModeChanged(Lcom/android/server/telecom/Call;)V
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/CallsManager$CallsManagerListener;

    .line 364
    invoke-virtual {v0}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;->onIsVoipAudioModeChanged$57920a58()V

    goto :goto_0

    .line 366
    :cond_0
    return-void
.end method

.method public final onParentChanged(Lcom/android/server/telecom/Call;)V
    .locals 2

    .prologue
    .line 346
    invoke-direct {p0}, Lcom/android/server/telecom/CallsManager;->updateForegroundCall()V

    .line 347
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/CallsManager$CallsManagerListener;

    .line 348
    invoke-virtual {v0, p1}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;->onIsConferencedChanged(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 350
    :cond_0
    return-void
.end method

.method public final onPostDialWait(Lcom/android/server/telecom/Call;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mInCallController:Lcom/android/server/telecom/InCallController;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/telecom/InCallController;->onPostDialWait(Lcom/android/server/telecom/Call;Ljava/lang/String;)V

    .line 341
    return-void
.end method

.method public final onRingbackRequested(Lcom/android/server/telecom/Call;Z)V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/CallsManager$CallsManagerListener;

    .line 334
    invoke-virtual {v0, p1}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;->onRingbackRequested$65500c84(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 336
    :cond_0
    return-void
.end method

.method public final bridge synthetic onStatusHintsChanged(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/android/server/telecom/Call$Listener;->onStatusHintsChanged(Lcom/android/server/telecom/Call;)V

    return-void
.end method

.method public final onSuccessfulIncomingCall(Lcom/android/server/telecom/Call;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 261
    const-string v2, "onSuccessfulIncomingCall"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 262
    iput-boolean v0, p0, Lcom/android/server/telecom/CallsManager;->isNeedAddLog:Z

    .line 263
    invoke-direct {p0, p1, v5}, Lcom/android/server/telecom/CallsManager;->setCallState(Lcom/android/server/telecom/Call;I)V

    .line 266
    invoke-static {}, Lcom/android/server/telecom/TelecomApp;->getInstance()Lcom/android/server/telecom/TelecomApp;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkDriveLink$304ff7b3(Landroid/content/Context;Z)V

    .line 267
    iget-object v2, p0, Lcom/android/server/telecom/CallsManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/telecom/secutils/TelecomUtils;->isDriveLinkModeOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->isVideoCall(Lcom/android/server/telecom/Call;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 268
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->reject$2598ce09()V

    .line 269
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mSecCallLogManager:Lcom/android/server/telecom/secutils/SecCallLogManager;

    invoke-virtual {v0, p1, v4}, Lcom/android/server/telecom/secutils/SecCallLogManager;->logCall(Lcom/android/server/telecom/Call;I)V

    .line 270
    invoke-static {}, Lcom/android/server/telecom/TelecomApp;->getInstance()Lcom/android/server/telecom/TelecomApp;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->launchDriveLinkDialogActivity(Landroid/content/Context;Lcom/android/server/telecom/Call;)V

    .line 309
    :goto_0
    return-void

    .line 274
    :cond_0
    sget-object v2, Lcom/android/server/telecom/CallsManager;->OUTGOING_CALL_STATES:[I

    const/4 v3, 0x0

    invoke-direct {p0, v3, v2}, Lcom/android/server/telecom/CallsManager;->getFirstCallWithState(Lcom/android/server/telecom/Call;[I)Lcom/android/server/telecom/Call;

    move-result-object v2

    .line 275
    if-eqz v2, :cond_1

    .line 276
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->reject$2598ce09()V

    .line 277
    iput-boolean v1, p0, Lcom/android/server/telecom/CallsManager;->isNeedAddLog:Z

    .line 280
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mSecCallLogManager:Lcom/android/server/telecom/secutils/SecCallLogManager;

    const/4 v1, 0x5

    invoke-virtual {v0, p1, v1}, Lcom/android/server/telecom/secutils/SecCallLogManager;->logCall(Lcom/android/server/telecom/Call;I)V

    goto :goto_0

    .line 284
    :cond_1
    new-array v2, v0, [I

    aput v5, v2, v1

    invoke-direct {p0, v2}, Lcom/android/server/telecom/CallsManager;->getNumCallsWithState([I)I

    move-result v2

    if-gt v0, v2, :cond_3

    .line 285
    :goto_1
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkMultiSimDsda()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 286
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-static {v0, p1}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->hasMultiSimMaximumRingingCalls(Ljava/util/Collection;Lcom/android/server/telecom/Call;)Z

    move-result v0

    .line 288
    :cond_2
    if-eqz v0, :cond_4

    .line 289
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->reject$2598ce09()V

    .line 292
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mMissedCallNotifier:Lcom/android/server/telecom/MissedCallNotifier;

    invoke-virtual {v0, p1}, Lcom/android/server/telecom/MissedCallNotifier;->showMissedCallNotification(Lcom/android/server/telecom/Call;)V

    .line 293
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mSecCallLogManager:Lcom/android/server/telecom/secutils/SecCallLogManager;

    invoke-virtual {v0, p1, v4}, Lcom/android/server/telecom/secutils/SecCallLogManager;->logCall(Lcom/android/server/telecom/Call;I)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 284
    goto :goto_1

    .line 298
    :cond_4
    invoke-static {}, Lcom/android/services/telephony/common/SystemDBInterface;->isTPhoneMode()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/android/services/telephony/common/SystemDBInterface;->isTPhoneRelaxMode()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->isVideoCall(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 301
    :cond_5
    :try_start_0
    const-string v0, "launch incoming call for Tphone"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 302
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/android/server/telecom/secutils/TelecomUtils;->requestTPhoneStart(Lcom/android/server/telecom/Call;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 308
    :cond_6
    :goto_2
    invoke-direct {p0, p1}, Lcom/android/server/telecom/CallsManager;->addCall(Lcom/android/server/telecom/Call;)V

    goto :goto_0

    .line 303
    :catch_0
    move-exception v0

    .line 304
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public final onSuccessfulOutgoingCall(Lcom/android/server/telecom/Call;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 231
    const-string v0, "onSuccessfulOutgoingCall, %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 233
    invoke-direct {p0, p1, p2}, Lcom/android/server/telecom/CallsManager;->setCallState(Lcom/android/server/telecom/Call;I)V

    .line 234
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    invoke-direct {p0, p1}, Lcom/android/server/telecom/CallsManager;->addCall(Lcom/android/server/telecom/Call;)V

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/CallsManager$CallsManagerListener;

    .line 242
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getConnectionService()Lcom/android/server/telecom/ConnectionServiceWrapper;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;->onConnectionServiceChanged$382ed448(Lcom/android/server/telecom/Call;Lcom/android/server/telecom/ConnectionServiceWrapper;)V

    goto :goto_0

    .line 245
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/server/telecom/CallsManager;->markCallAsDialing(Lcom/android/server/telecom/Call;)V

    .line 247
    const-string v0, "feature_ctc"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->getPhoneTypeFromCall(Lcom/android/server/telecom/Call;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 249
    invoke-virtual {p0, v3}, Lcom/android/server/telecom/CallsManager;->mute(Z)V

    .line 251
    :cond_2
    return-void
.end method

.method public final onSuccessfulUnknownCall(Lcom/android/server/telecom/Call;I)V
    .locals 3

    .prologue
    .line 319
    invoke-direct {p0, p1, p2}, Lcom/android/server/telecom/CallsManager;->setCallState(Lcom/android/server/telecom/Call;I)V

    .line 320
    const-string v0, "onSuccessfulUnknownCall for call %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 321
    invoke-direct {p0, p1}, Lcom/android/server/telecom/CallsManager;->addCall(Lcom/android/server/telecom/Call;)V

    .line 322
    return-void
.end method

.method public final bridge synthetic onTargetPhoneAccountChanged(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/android/server/telecom/Call$Listener;->onTargetPhoneAccountChanged(Lcom/android/server/telecom/Call;)V

    return-void
.end method

.method public final bridge synthetic onVideoCallProviderChanged(Lcom/android/server/telecom/Call;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/android/server/telecom/Call$Listener;->onVideoCallProviderChanged(Lcom/android/server/telecom/Call;)V

    return-void
.end method

.method public final onVideoStateChanged(Lcom/android/server/telecom/Call;)V
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 373
    :cond_0
    return-void
.end method

.method final phoneAccountSelected(Lcom/android/server/telecom/Call;Landroid/telecom/PhoneAccountHandle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 913
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 914
    const-string v0, "Attempted to add account to unknown call %s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 937
    :cond_0
    :goto_0
    return-void

    .line 922
    :cond_1
    invoke-virtual {p1, p2}, Lcom/android/server/telecom/Call;->setTargetPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    .line 926
    invoke-direct {p0, p1, v2}, Lcom/android/server/telecom/CallsManager;->makeRoomForOutgoingCall(Lcom/android/server/telecom/Call;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 927
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

    invoke-virtual {p1, v0}, Lcom/android/server/telecom/Call;->startCreateConnection(Lcom/android/server/telecom/PhoneAccountRegistrar;)V

    .line 928
    const-string v0, "feature_multisim_dsds"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 929
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultVoiceSubId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/telecom/CallsManager;->prevSubId:J

    .line 930
    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->setDefaultSubscriptionId(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 931
    iput-boolean v3, p0, Lcom/android/server/telecom/CallsManager;->defaultSubscriptionChanged:Z

    goto :goto_0

    .line 934
    :cond_2
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->disconnect()V

    goto :goto_0
.end method

.method final placeOutgoingCall(Lcom/android/server/telecom/Call;Landroid/net/Uri;Landroid/telecom/GatewayInfo;ZI)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 626
    invoke-static {}, Landroid/util/GateConfig;->isGateEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627
    const-string v0, "GATE"

    const-string v1, "<GATE-M>MO_CALL</GATE-M>"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/android/server/telecom/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 628
    :cond_0
    if-nez p1, :cond_2

    .line 630
    const-string v0, "Canceling unknown call."

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 669
    :cond_1
    :goto_0
    return-void

    .line 634
    :cond_2
    if-nez p3, :cond_6

    move-object v0, p2

    .line 636
    :goto_1
    if-nez p3, :cond_7

    .line 637
    const-string v1, "Creating a new outgoing call with handle: %s"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/server/telecom/Log;->piiHandle(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 643
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "placeOutgoingCall() setStartWithSpeakerphoneOn = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 645
    invoke-virtual {p1, v0}, Lcom/android/server/telecom/Call;->setHandle(Landroid/net/Uri;)V

    .line 646
    invoke-virtual {p1, p3}, Lcom/android/server/telecom/Call;->setGatewayInfo(Landroid/telecom/GatewayInfo;)V

    .line 647
    invoke-virtual {p1, p4}, Lcom/android/server/telecom/Call;->setStartWithSpeakerphoneOn(Z)V

    .line 648
    invoke-virtual {p1, p5}, Lcom/android/server/telecom/Call;->setVideoState(I)V

    .line 650
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getHandle()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/telecom/TelephonyUtil;->shouldProcessAsEmergency(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v1

    .line 653
    invoke-static {}, Lcom/android/server/telecom/TelecomApp;->getInstance()Lcom/android/server/telecom/TelecomApp;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkDriveLink$304ff7b3(Landroid/content/Context;Z)V

    .line 654
    if-eqz v1, :cond_3

    const-string v0, "feature_multisim"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 656
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/server/telecom/Call;->setTargetPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    .line 658
    :cond_3
    const-string v0, "feature_multisim"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 659
    const-string v2, "call.emergency.dial"

    if-eqz v1, :cond_8

    const-string v0, "true"

    :goto_3
    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "placeOutgoingCall() call.getTargetPhoneAccount() = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getTargetPhoneAccount()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v2}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 662
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "isEmergencyCall = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v2}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 664
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getTargetPhoneAccount()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    if-nez v0, :cond_5

    if-eqz v1, :cond_1

    .line 667
    :cond_5
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

    invoke-virtual {p1, v0}, Lcom/android/server/telecom/Call;->startCreateConnection(Lcom/android/server/telecom/PhoneAccountRegistrar;)V

    goto/16 :goto_0

    .line 634
    :cond_6
    invoke-virtual {p3}, Landroid/telecom/GatewayInfo;->getGatewayAddress()Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_1

    .line 639
    :cond_7
    const-string v1, "Creating a new outgoing call with gateway handle: %s, original handle: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/server/telecom/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p2}, Lcom/android/server/telecom/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 659
    :cond_8
    const-string v0, "false"

    goto :goto_3
.end method

.method final playDtmfTone(Lcom/android/server/telecom/Call;C)V
    .locals 3

    .prologue
    .line 750
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 751
    const-string v0, "Request to play DTMF in a non-existent call %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 756
    :goto_0
    return-void

    .line 753
    :cond_0
    invoke-virtual {p1, p2}, Lcom/android/server/telecom/Call;->playDtmfTone(C)V

    .line 754
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mDtmfLocalTonePlayer:Lcom/android/server/telecom/DtmfLocalTonePlayer;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/telecom/DtmfLocalTonePlayer;->playTone(Lcom/android/server/telecom/Call;C)V

    goto :goto_0
.end method

.method public final playDtmfTonePlayer(Lcom/android/server/telecom/Call;C)V
    .locals 3

    .prologue
    .line 759
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 760
    const-string v0, "Request to play DTMF in a non-existent call %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 764
    :goto_0
    return-void

    .line 762
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mDtmfLocalTonePlayer:Lcom/android/server/telecom/DtmfLocalTonePlayer;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/telecom/DtmfLocalTonePlayer;->playTone(Lcom/android/server/telecom/Call;C)V

    goto :goto_0
.end method

.method final postDialContinue(Lcom/android/server/telecom/Call;Z)V
    .locals 3

    .prologue
    .line 790
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 791
    const-string v0, "Request to continue post-dial string in a non-existent call %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 795
    :goto_0
    return-void

    .line 793
    :cond_0
    invoke-virtual {p1, p2}, Lcom/android/server/telecom/Call;->postDialContinue(Z)V

    goto :goto_0
.end method

.method final processIncomingCallIntent(Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 428
    const-string v0, "processIncomingCallIntent"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 429
    invoke-static {}, Landroid/util/GateConfig;->isGateEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    const-string v0, "GATE"

    const-string v1, "<GATE-M>INCOMING_CALL</GATE-M>"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v1, v2}, Lcom/android/server/telecom/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 431
    :cond_0
    const-string v0, "incoming_number"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 432
    new-instance v0, Lcom/android/server/telecom/Call;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/telecom/CallsManager;->mConnectionServiceRepository$f45da5c:Lcom/android/server/telecom/ServiceBinder$Listener;

    move-object v5, v4

    move-object v6, p1

    invoke-direct/range {v0 .. v8}, Lcom/android/server/telecom/Call;-><init>(Landroid/content/Context;Lcom/android/server/telecom/ServiceBinder$Listener;Landroid/net/Uri;Landroid/telecom/GatewayInfo;Landroid/telecom/PhoneAccountHandle;Landroid/telecom/PhoneAccountHandle;ZZ)V

    invoke-static {v0, p2}, Lcom/android/server/telecom/secutils/TelecomUtils;->makeSecCall(Lcom/android/server/telecom/Call;Landroid/os/Bundle;)Lcom/android/server/telecom/Call;

    move-result-object v0

    .line 442
    invoke-virtual {v0, p2}, Lcom/android/server/telecom/Call;->setExtras(Landroid/os/Bundle;)V

    .line 444
    invoke-virtual {v0, p0}, Lcom/android/server/telecom/Call;->addListener(Lcom/android/server/telecom/Call$Listener;)V

    .line 445
    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

    invoke-virtual {v0, v1}, Lcom/android/server/telecom/Call;->startCreateConnection(Lcom/android/server/telecom/PhoneAccountRegistrar;)V

    .line 448
    const-string v1, "feature_multisim_dsds"

    invoke-static {v1}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 449
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultVoiceSubId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/server/telecom/CallsManager;->prevSubId:J

    .line 450
    invoke-static {v0}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->setDefaultSubscriptionId(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 451
    iput-boolean v7, p0, Lcom/android/server/telecom/CallsManager;->defaultSubscriptionChanged:Z

    .line 453
    :cond_1
    return-void
.end method

.method final rejectCall(Lcom/android/server/telecom/Call;ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 734
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 735
    const-string v0, "Request to reject a non-existent call %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 742
    :goto_0
    return-void

    .line 737
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/CallsManager$CallsManagerListener;

    .line 738
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;->onIncomingCallRejected(Lcom/android/server/telecom/Call;ZLjava/lang/String;)V

    goto :goto_1

    .line 740
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->reject$2598ce09()V

    goto :goto_0
.end method

.method final removeListener(Lcom/android/server/telecom/CallsManager$CallsManagerListener;)V
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 418
    return-void
.end method

.method final requestRcsObserver(II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 870
    const-string v0, "requestRcsObserver (%d,%d)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 871
    packed-switch p2, :pswitch_data_0

    .line 879
    :goto_0
    return-void

    .line 873
    :pswitch_0
    const-string v0, "REGISTER"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 874
    invoke-static {}, Lcom/android/server/telecom/TelecomApp;->getInstance()Lcom/android/server/telecom/TelecomApp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/telecom/TelecomApp;->registerRcsObserver(I)V

    goto :goto_0

    .line 877
    :pswitch_1
    const-string v0, "UNREGISTER"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 878
    invoke-static {}, Lcom/android/server/telecom/TelecomApp;->getInstance()Lcom/android/server/telecom/TelecomApp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/telecom/TelecomApp;->unRegisterRcsObserver(I)V

    goto :goto_0

    .line 871
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final resetAudioStateAfterDisconnect()V
    .locals 2

    .prologue
    .line 1589
    const-string v0, "resetAudioStateAfterDisconnect()..."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1590
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/CallAudioManager;->resetAudioStateAfterDisconnect()V

    .line 1591
    return-void
.end method

.method public final setAudioRoute(I)V
    .locals 2

    .prologue
    .line 890
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/server/telecom/CallAudioManager;->setAudioRoute(I)V

    .line 891
    return-void
.end method

.method public final setAudioRoute(IZ)V
    .locals 2

    .prologue
    .line 895
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/android/server/telecom/CallAudioManager;->setAudioRoute(IZ)V

    .line 896
    return-void
.end method

.method public final setWBAMRstate(Z)V
    .locals 0

    .prologue
    .line 1572
    iput-boolean p1, p0, Lcom/android/server/telecom/CallsManager;->mWB_AMR_state:Z

    .line 1573
    return-void
.end method

.method final startOutgoingCall(Landroid/net/Uri;Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)Lcom/android/server/telecom/Call;
    .locals 9

    .prologue
    .line 487
    new-instance v0, Lcom/android/server/telecom/Call;

    iget-object v1, p0, Lcom/android/server/telecom/CallsManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/telecom/CallsManager;->mConnectionServiceRepository$f45da5c:Lcom/android/server/telecom/ServiceBinder$Listener;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p1

    invoke-direct/range {v0 .. v8}, Lcom/android/server/telecom/Call;-><init>(Landroid/content/Context;Lcom/android/server/telecom/ServiceBinder$Listener;Landroid/net/Uri;Landroid/telecom/GatewayInfo;Landroid/telecom/PhoneAccountHandle;Landroid/telecom/PhoneAccountHandle;ZZ)V

    invoke-static {v0, p3}, Lcom/android/server/telecom/secutils/TelecomUtils;->makeSecCall(Lcom/android/server/telecom/Call;Landroid/os/Bundle;)Lcom/android/server/telecom/Call;

    move-result-object v1

    .line 497
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/server/telecom/PhoneAccountRegistrar;->getCallCapablePhoneAccounts(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 500
    invoke-static {p3}, Lcom/android/server/telecom/secutils/TelecomUtils;->isVideoCall(Landroid/os/Bundle;)Z

    move-result v0

    .line 505
    if-eqz p2, :cond_14

    .line 506
    invoke-interface {v4, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    .line 507
    const/4 p2, 0x0

    move-object v2, p2

    .line 511
    :goto_0
    if-nez v2, :cond_8

    .line 512
    const-string v3, "feature_multisim_preferred_sim"

    invoke-static {v3}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 513
    invoke-static {p1, v0}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->selectPhoneAccountHandle(Landroid/net/Uri;Z)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 524
    :cond_0
    :goto_1
    if-nez v0, :cond_1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    iget v2, p0, Lcom/android/server/telecom/CallsManager;->mPhoneState:I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    if-eqz v2, :cond_1

    .line 526
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mForegroundCall:Lcom/android/server/telecom/Call;

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->getTargetPhoneAccount()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 530
    :cond_1
    if-nez v0, :cond_3

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_3

    invoke-static {p3}, Lcom/android/server/telecom/secutils/TelecomUtils;->isVideoCall(Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p3}, Lcom/android/server/telecom/secutils/TelecomUtils;->isCSVideoCall(Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 531
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultVoiceSubId()J

    move-result-wide v2

    invoke-static {v2, v3}, Landroid/telephony/SubscriptionManager;->getPhoneId(J)I

    move-result v0

    .line 532
    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->isValidPhoneId(I)Z

    move-result v2

    .line 533
    if-nez v2, :cond_2

    .line 534
    const/4 v0, 0x0

    .line 535
    :cond_2
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 539
    :cond_3
    const-string v2, "CallsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "startOutgoingCall() phoneAccountHandle = "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v5}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 541
    invoke-virtual {v1, v0}, Lcom/android/server/telecom/Call;->setTargetPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    .line 543
    iget-object v2, p0, Lcom/android/server/telecom/CallsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/android/server/telecom/Call;->getHandle()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/telecom/TelephonyUtil;->shouldProcessAsEmergency(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v5

    .line 546
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "tel"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {p1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v6, 0x2

    if-le v3, v6, :cond_6

    :cond_4
    const-string v3, "2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v6, 0x2

    if-le v3, v6, :cond_6

    :cond_5
    const-string v3, "3"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "4"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "5"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_6
    const/4 v2, 0x1

    move v3, v2

    .line 550
    :goto_2
    if-nez v3, :cond_b

    invoke-direct {p0, v1, v5}, Lcom/android/server/telecom/CallsManager;->makeRoomForOutgoingCall(Lcom/android/server/telecom/Call;Z)Z

    move-result v2

    if-nez v2, :cond_b

    .line 552
    const/4 v0, 0x0

    .line 587
    :goto_3
    return-object v0

    .line 516
    :cond_7
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mPhoneAccountRegistrar:Lcom/android/server/telecom/PhoneAccountRegistrar;

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/server/telecom/PhoneAccountRegistrar;->getDefaultOutgoingPhoneAccount(Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 517
    if-nez v0, :cond_0

    :cond_8
    move-object v0, v2

    goto/16 :goto_1

    .line 546
    :cond_9
    const/4 v2, 0x0

    move v3, v2

    goto :goto_2

    :cond_a
    const/4 v2, 0x0

    move v3, v2

    goto :goto_2

    .line 555
    :cond_b
    if-nez v0, :cond_d

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    const/4 v6, 0x1

    if-le v2, v6, :cond_d

    if-nez v5, :cond_d

    .line 557
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/android/server/telecom/Call;->setState(I)V

    .line 558
    if-eqz p3, :cond_c

    .line 559
    const-string v2, "selectPhoneAccountAccounts"

    invoke-virtual {p3, v2, v4}, Landroid/os/Bundle;->putParcelableList(Ljava/lang/String;Ljava/util/List;)V

    .line 563
    :cond_c
    :goto_4
    invoke-virtual {v1, p3}, Lcom/android/server/telecom/Call;->setExtras(Landroid/os/Bundle;)V

    .line 564
    const-string v2, "CallsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "startOutgoingCall() extras = "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2, v4, v6}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 565
    invoke-static {v1}, Lcom/android/server/telecom/LowBatteryManager;->supportLowBatteryManager(Lcom/android/server/telecom/Call;)Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-static {}, Lcom/android/server/telecom/LowBatteryManager;->getInstance()Lcom/android/server/telecom/LowBatteryManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/telecom/LowBatteryManager;->isLowBatt()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 566
    invoke-static {}, Lcom/android/server/telecom/LowBatteryManager;->getInstance()Lcom/android/server/telecom/LowBatteryManager;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/android/server/telecom/LowBatteryManager;->performBatteryLowProcess(Z)V

    .line 567
    invoke-virtual {v1}, Lcom/android/server/telecom/Call;->disconnect()V

    .line 568
    const/4 v0, 0x0

    goto :goto_3

    .line 561
    :cond_d
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/server/telecom/Call;->setState(I)V

    goto :goto_4

    .line 572
    :cond_e
    if-eqz p1, :cond_12

    invoke-virtual {p1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_12

    invoke-virtual {p1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    const-string v4, "#"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    invoke-virtual {p1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    const-string v4, "+"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    :cond_f
    const/4 v2, 0x1

    :goto_5
    if-nez v2, :cond_10

    if-eqz v3, :cond_13

    .line 573
    :cond_10
    invoke-virtual {v1, p0}, Lcom/android/server/telecom/Call;->addListener(Lcom/android/server/telecom/Call$Listener;)V

    .line 579
    :goto_6
    const-string v2, "feature_multisim_dsds"

    invoke-static {v2}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 580
    if-eqz v0, :cond_11

    if-nez v5, :cond_11

    .line 581
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultVoiceSubId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/server/telecom/CallsManager;->prevSubId:J

    .line 582
    invoke-static {v1}, Lcom/android/server/telecom/secutils/TelecomUtilsMultiSIM;->setDefaultSubscriptionId(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 583
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/telecom/CallsManager;->defaultSubscriptionChanged:Z

    :cond_11
    move-object v0, v1

    .line 587
    goto/16 :goto_3

    .line 572
    :cond_12
    const/4 v2, 0x0

    goto :goto_5

    .line 575
    :cond_13
    invoke-direct {p0, v1}, Lcom/android/server/telecom/CallsManager;->addCall(Lcom/android/server/telecom/Call;)V

    goto :goto_6

    :cond_14
    move-object v2, p2

    goto/16 :goto_0
.end method

.method final stopDtmfTone(Lcom/android/server/telecom/Call;)V
    .locals 3

    .prologue
    .line 770
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 771
    const-string v0, "Request to stop DTMF in a non-existent call %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 776
    :goto_0
    return-void

    .line 773
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->stopDtmfTone()V

    .line 774
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mDtmfLocalTonePlayer:Lcom/android/server/telecom/DtmfLocalTonePlayer;

    invoke-virtual {v0, p1}, Lcom/android/server/telecom/DtmfLocalTonePlayer;->stopTone(Lcom/android/server/telecom/Call;)V

    goto :goto_0
.end method

.method public final stopDtmfTonePlayer(Lcom/android/server/telecom/Call;)V
    .locals 3

    .prologue
    .line 779
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 780
    const-string v0, "Request to stop DTMF in a non-existent call %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 784
    :goto_0
    return-void

    .line 782
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mDtmfLocalTonePlayer:Lcom/android/server/telecom/DtmfLocalTonePlayer;

    invoke-virtual {v0, p1}, Lcom/android/server/telecom/DtmfLocalTonePlayer;->stopTone(Lcom/android/server/telecom/Call;)V

    goto :goto_0
.end method

.method public final storeSpeakerState(Z)V
    .locals 1

    .prologue
    .line 1580
    invoke-virtual {p0}, Lcom/android/server/telecom/CallsManager;->getRingingCall()Lcom/android/server/telecom/Call;

    move-result-object v0

    .line 1581
    if-eqz v0, :cond_0

    .line 1582
    invoke-virtual {v0, p1}, Lcom/android/server/telecom/Call;->setStartWithSpeakerphoneOn(Z)V

    .line 1586
    :goto_0
    return-void

    .line 1584
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCallAudioManager:Lcom/android/server/telecom/CallAudioManager;

    iput-boolean p1, v0, Lcom/android/server/telecom/CallAudioManager;->mWasSpeakerOn:Z

    goto :goto_0
.end method

.method final turnOffProximitySensor(Z)V
    .locals 1

    .prologue
    .line 909
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mProximitySensorManager:Lcom/android/server/telecom/ProximitySensorManager;

    invoke-virtual {v0, p1}, Lcom/android/server/telecom/ProximitySensorManager;->turnOff(Z)V

    .line 910
    return-void
.end method

.method final turnOnProximitySensor()V
    .locals 1

    .prologue
    .line 900
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mProximitySensorManager:Lcom/android/server/telecom/ProximitySensorManager;

    invoke-virtual {v0}, Lcom/android/server/telecom/ProximitySensorManager;->turnOn()V

    .line 901
    return-void
.end method

.method final unholdCall(Lcom/android/server/telecom/Call;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 845
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 846
    const-string v0, "Unknown call (%s) asked to be removed from hold"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 862
    :goto_0
    return-void

    .line 848
    :cond_0
    const-string v0, "unholding call: (%s)"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 849
    iget-object v0, p0, Lcom/android/server/telecom/CallsManager;->mCalls:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/telecom/Call;

    .line 850
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_1

    if-eq v0, p1, :cond_1

    .line 853
    :try_start_0
    invoke-virtual {v0}, Lcom/android/server/telecom/Call;->hold()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 854
    :catch_0
    move-exception v0

    .line 855
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 860
    :cond_2
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->unhold()V

    goto :goto_0
.end method

.method public final updateForegroundCallForMulSimDsda()V
    .locals 0

    .prologue
    .line 1356
    invoke-direct {p0}, Lcom/android/server/telecom/CallsManager;->updateForegroundCall()V

    .line 1357
    return-void
.end method
