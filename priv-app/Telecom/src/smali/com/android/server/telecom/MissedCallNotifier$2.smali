.class final Lcom/android/server/telecom/MissedCallNotifier$2;
.super Landroid/os/Handler;
.source "MissedCallNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/telecom/MissedCallNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic this$0:Lcom/android/server/telecom/MissedCallNotifier;


# direct methods
.method constructor <init>(Lcom/android/server/telecom/MissedCallNotifier;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/android/server/telecom/MissedCallNotifier$2;->this$0:Lcom/android/server/telecom/MissedCallNotifier;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Handler : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mHandler: unexpected message: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    :goto_0
    return-void

    .line 122
    :pswitch_0
    const-string v0, "START_QUERY_FOR_OTHER_DEVICES "

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier$2;->this$0:Lcom/android/server/telecom/MissedCallNotifier;

    new-instance v1, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;

    iget-object v2, p0, Lcom/android/server/telecom/MissedCallNotifier$2;->this$0:Lcom/android/server/telecom/MissedCallNotifier;

    # getter for: Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/server/telecom/MissedCallNotifier;->access$300(Lcom/android/server/telecom/MissedCallNotifier;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/android/server/telecom/MissedCallNotifier;->quickpanelNotifier:Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;
    invoke-static {v0, v1}, Lcom/android/server/telecom/MissedCallNotifier;->access$202(Lcom/android/server/telecom/MissedCallNotifier;Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;)Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;

    .line 124
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier$2;->this$0:Lcom/android/server/telecom/MissedCallNotifier;

    # getter for: Lcom/android/server/telecom/MissedCallNotifier;->quickpanelNotifier:Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;
    invoke-static {v0}, Lcom/android/server/telecom/MissedCallNotifier;->access$200(Lcom/android/server/telecom/MissedCallNotifier;)Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/telecom/MissedCallNotifier$2;->this$0:Lcom/android/server/telecom/MissedCallNotifier;

    # getter for: Lcom/android/server/telecom/MissedCallNotifier;->mCall:Lcom/android/server/telecom/Call;
    invoke-static {v1}, Lcom/android/server/telecom/MissedCallNotifier;->access$400(Lcom/android/server/telecom/MissedCallNotifier;)Lcom/android/server/telecom/Call;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/telecom/MissedCallNotifier$2;->this$0:Lcom/android/server/telecom/MissedCallNotifier;

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier$2;->this$0:Lcom/android/server/telecom/MissedCallNotifier;

    # getter for: Lcom/android/server/telecom/MissedCallNotifier;->mCall:Lcom/android/server/telecom/Call;
    invoke-static {v3}, Lcom/android/server/telecom/MissedCallNotifier;->access$400(Lcom/android/server/telecom/MissedCallNotifier;)Lcom/android/server/telecom/Call;

    move-result-object v3

    # invokes: Lcom/android/server/telecom/MissedCallNotifier;->getNameForCall(Lcom/android/server/telecom/Call;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/android/server/telecom/MissedCallNotifier;->access$500(Lcom/android/server/telecom/MissedCallNotifier;Lcom/android/server/telecom/Call;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier$2;->this$0:Lcom/android/server/telecom/MissedCallNotifier;

    # getter for: Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I
    invoke-static {v3}, Lcom/android/server/telecom/MissedCallNotifier;->access$600(Lcom/android/server/telecom/MissedCallNotifier;)I

    move-result v3

    iget-object v4, p0, Lcom/android/server/telecom/MissedCallNotifier$2;->this$0:Lcom/android/server/telecom/MissedCallNotifier;

    # getter for: Lcom/android/server/telecom/MissedCallNotifier;->mExpandedText:Ljava/lang/String;
    invoke-static {v4}, Lcom/android/server/telecom/MissedCallNotifier;->access$700(Lcom/android/server/telecom/MissedCallNotifier;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/telecom/MissedCallNotifier$2;->this$0:Lcom/android/server/telecom/MissedCallNotifier;

    iget-object v5, v5, Lcom/android/server/telecom/MissedCallNotifier;->mNotiBuilder:Landroid/app/Notification$Builder;

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;->notifyMissedCall(Lcom/android/server/telecom/Call;Ljava/lang/String;ILjava/lang/String;Landroid/app/Notification$Builder;)V

    goto :goto_0

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method
