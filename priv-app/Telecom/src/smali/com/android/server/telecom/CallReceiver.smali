.class public Lcom/android/server/telecom/CallReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CallReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/android/server/telecom/CallReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/telecom/CallReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static processIncomingCallIntent(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 205
    const-string v0, "android.telecom.extra.PHONE_ACCOUNT_HANDLE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 208
    if-nez v0, :cond_0

    .line 209
    sget-object v0, Lcom/android/server/telecom/CallReceiver;->TAG:Ljava/lang/String;

    const-string v1, "Rejecting incoming call due to null phone account"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/server/telecom/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    :goto_0
    return-void

    .line 212
    :cond_0
    invoke-virtual {v0}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_1

    .line 213
    sget-object v0, Lcom/android/server/telecom/CallReceiver;->TAG:Ljava/lang/String;

    const-string v1, "Rejecting incoming call due to null component name"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/server/telecom/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 217
    :cond_1
    const/4 v1, 0x0

    .line 218
    const-string v2, "android.telecom.extra.INCOMING_CALL_EXTRAS"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 219
    const-string v1, "android.telecom.extra.INCOMING_CALL_EXTRAS"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 221
    :cond_2
    sget-object v2, Lcom/android/server/telecom/CallReceiver;->TAG:Ljava/lang/String;

    const-string v3, "Processing incoming call from connection service [%s]"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 228
    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/android/server/telecom/CallsManager;->processIncomingCallIntent(Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public static processOutgoingCallIntent(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 13

    .prologue
    const v8, 0x7f08001e

    const/4 v9, -0x1

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 71
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 72
    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    .line 73
    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 76
    const-string v1, "feature_ctc"

    invoke-static {v1}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 77
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 79
    :goto_0
    sget-object v0, Lcom/android/server/telecom/CallReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, "processOutgoingCallIntent() scheme = "

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v10, v6, [Ljava/lang/Object;

    invoke-static {v0, v3, v10}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    sget-object v0, Lcom/android/server/telecom/CallReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, "uriString = "

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v10, v6, [Ljava/lang/Object;

    invoke-static {v0, v3, v10}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    const-string v0, "voicemail"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isUriNumber(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "sip"

    :goto_1
    invoke-static {v0, v1, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v2, v0

    .line 86
    :cond_0
    sget-object v0, Lcom/android/server/telecom/CallReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, "handle = "

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v10, v6, [Ljava/lang/Object;

    invoke-static {v0, v3, v10}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    invoke-static {p0, p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->isAvailableInternationCall(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 89
    const v0, 0x7f080039

    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 201
    :cond_1
    :goto_2
    return v6

    .line 83
    :cond_2
    const-string v0, "tel"

    goto :goto_1

    .line 94
    :cond_3
    const-string v0, "android.telecom.extra.PHONE_ACCOUNT_HANDLE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 97
    if-nez v0, :cond_4

    .line 98
    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->selectAccountBySimSlotIDExtra(Landroid/content/Intent;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 102
    :cond_4
    const-string v3, "android.telecom.extra.OUTGOING_CALL_EXTRAS"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 103
    const-string v3, "android.telecom.extra.OUTGOING_CALL_EXTRAS"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 105
    :goto_3
    const-string v10, "voicemail"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 112
    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->getVoiceCallSubId(Landroid/content/Intent;)J

    move-result-wide v10

    .line 113
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v12

    invoke-virtual {v12, v10, v11}, Landroid/telephony/TelephonyManager;->getVoiceMailNumber(J)Ljava/lang/String;

    move-result-object v10

    .line 114
    if-eqz v10, :cond_5

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 115
    :cond_5
    sget-object v0, Lcom/android/server/telecom/CallReceiver;->TAG:Ljava/lang/String;

    const-string v1, "processOutgoingCallIntent no voice mail number "

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    const/4 v0, 0x5

    invoke-static {p0, v0}, Lcom/android/server/telecom/CallReceiver;->showErrorDialog(Landroid/content/Context;I)V

    goto :goto_2

    .line 121
    :cond_6
    const-string v10, "tel"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 122
    invoke-static {p0, p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->checkToInitiateOutgoingCall(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v5

    .line 123
    sget-object v10, Lcom/android/server/telecom/CallReceiver;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "checkToInitiateOutgoingCall = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    new-array v12, v6, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    const/4 v10, 0x4

    if-ne v5, v10, :cond_b

    .line 125
    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->removeVideoCallExtra(Landroid/content/Intent;)V

    .line 132
    :cond_7
    invoke-static {p0, v2, p1, v3}, Lcom/android/server/telecom/secutils/TelecomUtils;->makeOutgoingSecCallExtra(Landroid/content/Context;Landroid/net/Uri;Landroid/content/Intent;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v3

    .line 133
    const-string v5, "is_default_dialer"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 135
    const-string v10, "ip_call"

    invoke-static {v10}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 136
    const-string v10, "outbarringpwright"

    invoke-virtual {p1, v10, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 137
    invoke-static {v3}, Lcom/android/services/telephony/common/SecCallExtra;->getSecCallExtra(Landroid/os/Bundle;)Lcom/android/services/telephony/common/SecCallExtra;

    move-result-object v11

    .line 138
    if-eqz v11, :cond_8

    invoke-virtual {v11}, Lcom/android/services/telephony/common/SecCallExtra;->IsIPCall()Z

    move-result v11

    if-eqz v11, :cond_8

    if-nez v10, :cond_8

    .line 139
    invoke-static {v1}, Lcom/android/server/telecom/secutils/TelecomUtils;->makeIpCallNum(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 140
    const-string v2, "tel"

    invoke-static {v2, v1, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 141
    invoke-virtual {p1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 146
    :cond_8
    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v1

    invoke-virtual {v1, v2, v0, v3}, Lcom/android/server/telecom/CallsManager;->startOutgoingCall(Landroid/net/Uri;Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)Lcom/android/server/telecom/Call;

    move-result-object v3

    .line 147
    if-eqz v3, :cond_11

    .line 153
    new-instance v0, Lcom/android/server/telecom/NewOutgoingCallIntentBroadcaster;

    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v2

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/server/telecom/NewOutgoingCallIntentBroadcaster;-><init>(Landroid/content/Context;Lcom/android/server/telecom/CallsManager;Lcom/android/server/telecom/Call;Landroid/content/Intent;Z)V

    .line 155
    invoke-virtual {v0}, Lcom/android/server/telecom/NewOutgoingCallIntentBroadcaster;->processIntent()I

    move-result v1

    .line 156
    if-nez v1, :cond_c

    move v0, v7

    .line 158
    :goto_4
    if-nez v0, :cond_e

    if-eqz v3, :cond_e

    .line 159
    invoke-virtual {v3}, Lcom/android/server/telecom/Call;->disconnect()V

    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/android/server/telecom/ErrorDialogActivity;

    invoke-direct {v2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sparse-switch v1, :sswitch_data_0

    move v0, v6

    move v1, v9

    :goto_5
    if-eq v1, v9, :cond_9

    const-string v1, "error_message_id"

    invoke-virtual {v2, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_9
    if-eq v0, v7, :cond_a

    const/high16 v0, 0x10000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget-object v0, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p0, v2, v0}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 160
    :cond_a
    invoke-static {}, Lcom/android/server/telecom/ATCommandReceiver;->isATcommandExecuting()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    const-string v0, "videocall"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 163
    if-nez v1, :cond_17

    move v0, v6

    .line 166
    :goto_6
    sget-object v2, Lcom/android/server/telecom/CallReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Call is not success, ATCommandReceiver.isATcommandExecuting() "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/server/telecom/ATCommandReceiver;->isATcommandExecuting()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isVideoCallType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", callType : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 167
    if-nez v0, :cond_d

    const-string v0, "atd"

    :goto_7
    const-string v1, "NO CARRIER"

    invoke-static {v0, v1}, Lcom/android/server/telecom/ATCommandReceiver;->sendResponse(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 126
    :cond_b
    if-eqz v5, :cond_7

    .line 127
    invoke-static {p0, v5}, Lcom/android/server/telecom/CallReceiver;->showErrorDialog(Landroid/content/Context;I)V

    goto/16 :goto_2

    :cond_c
    move v0, v6

    .line 156
    goto :goto_4

    :sswitch_0
    move v0, v6

    move v1, v8

    .line 159
    goto :goto_5

    :sswitch_1
    move v0, v7

    move v1, v9

    goto :goto_5

    .line 167
    :cond_d
    const-string v0, "atdvideo"

    goto :goto_7

    .line 171
    :cond_e
    if-eqz v0, :cond_f

    .line 172
    invoke-static {}, Lcom/android/server/telecom/ATCommandReceiver;->isATcommandExecuting()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 173
    const-string v0, "videocall"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 175
    if-nez v1, :cond_16

    move v0, v6

    .line 178
    :goto_8
    sget-object v2, Lcom/android/server/telecom/CallReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Call is success, ATCommandReceiver.isATcommandExecuting() "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/server/telecom/ATCommandReceiver;->isATcommandExecuting()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isVideoCallType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", callType : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 179
    if-nez v0, :cond_10

    const-string v0, "atd"

    :goto_9
    const-string v1, "OK"

    invoke-static {v0, v1}, Lcom/android/server/telecom/ATCommandReceiver;->sendResponse(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    :goto_a
    move v6, v7

    .line 201
    goto/16 :goto_2

    .line 179
    :cond_10
    const-string v0, "atdvideo"

    goto :goto_9

    .line 184
    :cond_11
    const-string v0, "videocall"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 186
    if-eqz v1, :cond_12

    invoke-static {}, Lcom/android/server/telecom/LowBatteryManager;->getInstance()Lcom/android/server/telecom/LowBatteryManager;

    move-result-object v0

    if-eqz v0, :cond_12

    invoke-static {}, Lcom/android/server/telecom/LowBatteryManager;->getInstance()Lcom/android/server/telecom/LowBatteryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/telecom/LowBatteryManager;->isLowBatt()Z

    move-result v0

    if-nez v0, :cond_13

    .line 190
    :cond_12
    invoke-static {p0, v7}, Lcom/android/server/telecom/CallReceiver;->showErrorDialog(Landroid/content/Context;I)V

    .line 192
    :cond_13
    invoke-static {}, Lcom/android/server/telecom/ATCommandReceiver;->isATcommandExecuting()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 193
    if-nez v1, :cond_15

    move v0, v6

    .line 196
    :goto_b
    sget-object v2, Lcom/android/server/telecom/CallReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Call is failed, ATCommandReceiver.isATcommandExecuting() "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/server/telecom/ATCommandReceiver;->isATcommandExecuting()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isVideoCallType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", callType : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Lcom/android/server/telecom/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    if-nez v0, :cond_14

    const-string v0, "atd"

    :goto_c
    const-string v1, "NO CARRIER"

    invoke-static {v0, v1}, Lcom/android/server/telecom/ATCommandReceiver;->sendResponse(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :cond_14
    const-string v0, "atdvideo"

    goto :goto_c

    :cond_15
    move v0, v7

    goto :goto_b

    :cond_16
    move v0, v7

    goto/16 :goto_8

    :cond_17
    move v0, v7

    goto/16 :goto_6

    :cond_18
    move-object v3, v4

    goto/16 :goto_3

    :cond_19
    move-object v1, v0

    goto/16 :goto_0

    .line 159
    nop

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x26 -> :sswitch_0
        0x2c -> :sswitch_1
    .end sparse-switch
.end method

.method private static showErrorDialog(Landroid/content/Context;I)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v2, -0x1

    const v1, 0x7f08002e

    .line 276
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/android/server/telecom/ErrorDialogActivity;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 278
    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    move v0, v2

    .line 298
    :goto_1
    if-ne v0, v1, :cond_2

    .line 299
    invoke-static {v1}, Lcom/android/server/telecom/secutils/TelecomUtils;->displayToast(I)V

    .line 307
    :goto_2
    return-void

    :pswitch_1
    move v0, v1

    .line 281
    goto :goto_1

    .line 283
    :pswitch_2
    invoke-static {}, Lcom/android/server/telecom/TelecomApp;->getInstance()Lcom/android/server/telecom/TelecomApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/telecom/TelecomApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "videocall_settings"

    invoke-static {v5, v6, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-nez v5, :cond_0

    .line 284
    :goto_3
    if-eqz v0, :cond_1

    .line 285
    const v0, 0x7f080089

    goto :goto_1

    :cond_0
    move v0, v3

    .line 283
    goto :goto_3

    .line 287
    :cond_1
    const v0, 0x7f08008a

    .line 289
    goto :goto_1

    .line 291
    :pswitch_3
    const v0, 0x7f08008b

    .line 292
    goto :goto_1

    .line 294
    :pswitch_4
    const-string v0, "show_missing_voicemail"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 302
    :cond_2
    if-eq v0, v2, :cond_3

    .line 303
    const-string v1, "error_message_id"

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 305
    :cond_3
    const/high16 v0, 0x10000000

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 306
    sget-object v0, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p0, v4, v0}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_2

    .line 278
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 51
    const-string v0, "is_unknown_call"

    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 52
    const-string v1, "is_incoming_call"

    invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 53
    const-string v2, "onReceive - isIncomingCall: %s isUnknownCall: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p0, v2, v3}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    if-eqz v0, :cond_2

    .line 57
    const-string v0, "android.telecom.extra.PHONE_ACCOUNT_HANDLE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    if-nez v0, :cond_0

    const-string v0, "Rejecting unknown call due to null phone account"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-virtual {v0}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v0, "Rejecting unknown call due to null component name"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/android/server/telecom/CallsManager;->getInstance()Lcom/android/server/telecom/CallsManager;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/android/server/telecom/CallsManager;->addNewUnknownCall(Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    goto :goto_0

    .line 58
    :cond_2
    if-eqz v1, :cond_3

    .line 59
    invoke-static {p2}, Lcom/android/server/telecom/CallReceiver;->processIncomingCallIntent(Landroid/content/Intent;)V

    goto :goto_0

    .line 61
    :cond_3
    invoke-static {p1, p2}, Lcom/android/server/telecom/CallReceiver;->processOutgoingCallIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    goto :goto_0
.end method
