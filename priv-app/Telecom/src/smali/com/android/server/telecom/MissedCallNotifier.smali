.class public final Lcom/android/server/telecom/MissedCallNotifier;
.super Lcom/android/server/telecom/CallsManager$CallsManagerListener;
.source "MissedCallNotifier.java"


# static fields
.field private static final CALL_LOG_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mCall:Lcom/android/server/telecom/Call;

.field private final mContext:Landroid/content/Context;

.field private mExpandedText:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mMissedCallCount:I

.field private mMissedCallName:Ljava/lang/String;

.field public mNotiBuilder:Landroid/app/Notification$Builder;

.field private final mNotificationManager:Landroid/app/NotificationManager;

.field private quickpanelNotifier:Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 66
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "number"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "presentation"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/telecom/MissedCallNotifier;->CALL_LOG_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Lcom/android/server/telecom/CallsManager$CallsManagerListener;-><init>()V

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    .line 81
    iput-object v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    .line 84
    iput-object v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mExpandedText:Ljava/lang/String;

    .line 89
    iput-object v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->quickpanelNotifier:Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;

    .line 91
    iput-object v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mCall:Lcom/android/server/telecom/Call;

    .line 105
    new-instance v0, Lcom/android/server/telecom/MissedCallNotifier$1;

    invoke-direct {v0, p0}, Lcom/android/server/telecom/MissedCallNotifier$1;-><init>(Lcom/android/server/telecom/MissedCallNotifier;)V

    iput-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 115
    new-instance v0, Lcom/android/server/telecom/MissedCallNotifier$2;

    invoke-direct {v0, p0}, Lcom/android/server/telecom/MissedCallNotifier$2;-><init>(Lcom/android/server/telecom/MissedCallNotifier;)V

    iput-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mHandler:Landroid/os/Handler;

    .line 94
    iput-object p1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    .line 95
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mNotificationManager:Landroid/app/NotificationManager;

    .line 98
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 99
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 100
    iget-object v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/telecom/MissedCallNotifier;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 102
    invoke-direct {p0}, Lcom/android/server/telecom/MissedCallNotifier;->updateOnStartup()V

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/telecom/MissedCallNotifier;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/android/server/telecom/MissedCallNotifier;->cancelMissedCallNotification()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/server/telecom/MissedCallNotifier;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/android/server/telecom/MissedCallNotifier;->updateOnStartup()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/server/telecom/MissedCallNotifier;)Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->quickpanelNotifier:Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;

    return-object v0
.end method

.method static synthetic access$202(Lcom/android/server/telecom/MissedCallNotifier;Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;)Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/android/server/telecom/MissedCallNotifier;->quickpanelNotifier:Lcom/android/server/telecom/secutils/SecQuickpanelNotifier;

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/server/telecom/MissedCallNotifier;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/telecom/MissedCallNotifier;)Lcom/android/server/telecom/Call;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mCall:Lcom/android/server/telecom/Call;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/telecom/MissedCallNotifier;Lcom/android/server/telecom/Call;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/android/server/telecom/MissedCallNotifier;->getNameForCall(Lcom/android/server/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/telecom/MissedCallNotifier;)I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    return v0
.end method

.method static synthetic access$700(Lcom/android/server/telecom/MissedCallNotifier;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mExpandedText:Ljava/lang/String;

    return-object v0
.end method

.method private cancelMissedCallNotification()V
    .locals 2

    .prologue
    .line 388
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    .line 389
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mNotificationManager:Landroid/app/NotificationManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 390
    return-void
.end method

.method private static configureLedOnNotification(Landroid/app/Notification;)V
    .locals 1

    .prologue
    .line 521
    iget v0, p0, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/app/Notification;->flags:I

    .line 522
    iget v0, p0, Landroid/app/Notification;->defaults:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Landroid/app/Notification;->defaults:I

    .line 523
    return-void
.end method

.method private createCallLogPendingIntent()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 467
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 468
    const-string v1, "vnd.android.cursor.dir/calls"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 470
    iget-object v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v1

    .line 471
    invoke-virtual {v1, v0}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    .line 473
    invoke-virtual {v1, v3, v3}, Landroid/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private createSendSmsFromNotificationPendingIntent(Landroid/net/Uri;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 500
    const-string v0, "com.android.server.telecom.ACTION_SEND_SMS_FROM_NOTIFICATION"

    const-string v1, "smsto"

    invoke-virtual {p1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/server/telecom/MissedCallNotifier;->createTelecomPendingIntent(Ljava/lang/String;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private createTelecomPendingIntent(Ljava/lang/String;Landroid/net/Uri;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 513
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/server/telecom/TelecomBroadcastReceiver;

    invoke-direct {v0, p1, p2, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 514
    iget-object v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private getNameForCall(Lcom/android/server/telecom/Call;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x3

    .line 396
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getHandle()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 397
    :goto_0
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getName()Ljava/lang/String;

    move-result-object v2

    .line 399
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 424
    :goto_1
    return-object v0

    .line 396
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getHandle()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 401
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 406
    const-string v2, "ecid_enable"

    invoke-static {v2}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 407
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getHandle()Landroid/net/Uri;

    move-result-object v2

    if-nez v2, :cond_2

    .line 408
    :goto_2
    if-eqz v1, :cond_3

    .line 409
    iget-object v2, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-static {v2, v1, v4}, Lcom/android/server/telecom/EcidContact;->doLookup(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/server/telecom/EcidContact;

    move-result-object v1

    .line 410
    if-eqz v1, :cond_3

    .line 411
    invoke-virtual {v1, v4}, Lcom/android/server/telecom/EcidContact;->getContactName(I)Ljava/lang/String;

    move-result-object v1

    .line 412
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 413
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Ecid name added as "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v2}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 414
    goto :goto_1

    .line 407
    :cond_2
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getHandle()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 420
    :cond_3
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v1

    .line 421
    sget-object v2, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    invoke-virtual {v1, v0, v2}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 424
    :cond_4
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const v1, 0x7f080008

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private updateOnStartup()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 529
    const-string v0, "updateOnStartup()..."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v3}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 532
    new-instance v0, Lcom/android/server/telecom/MissedCallNotifier$3;

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lcom/android/server/telecom/MissedCallNotifier$3;-><init>(Lcom/android/server/telecom/MissedCallNotifier;Landroid/content/ContentResolver;)V

    .line 590
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v3, "type="

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 591
    const/4 v3, 0x3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 592
    const-string v3, " AND new=1"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    const-string v3, "content://logs/call"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/android/server/telecom/MissedCallNotifier;->CALL_LOG_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v7, "date DESC"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    return-void
.end method


# virtual methods
.method public final SecShowMissedCallNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 10

    .prologue
    .line 300
    const-string v0, "SecShowMissedCallNotification"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 302
    iget v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    .line 303
    new-instance v1, Lcom/android/server/telecom/secutils/SViewCoverNotifier;

    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/android/server/telecom/secutils/SViewCoverNotifier;-><init>(Landroid/content/Context;)V

    .line 313
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p1}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p1

    :goto_0
    iput-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    .line 314
    invoke-static {p2}, Lcom/android/server/telecom/secutils/TelecomUtils;->isDormantOn(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const v2, 0x7f08007b

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "%s"

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    .line 318
    :cond_0
    iget v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_6

    .line 319
    const v2, 0x7f08000a

    .line 320
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    .line 328
    :goto_1
    new-instance v7, Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-direct {v7, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 329
    const v3, 0x108007f

    invoke-virtual {v7, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const v5, 0x7f08000d

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    aput-object v9, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f090000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/server/telecom/MissedCallNotifier;->createCallLogPendingIntent()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 343
    const-string v0, "tel"

    const/4 v2, 0x0

    invoke-static {v0, p2, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 344
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    .line 347
    :cond_1
    iget v2, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    .line 348
    const-string v2, "Add actions with number %s."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {p0, v2, v3}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const v3, 0x7f080008

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 352
    const/high16 v2, 0x7f020000

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const v4, 0x7f08000e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.android.server.telecom.ACTION_CALL_BACK_FROM_NOTIFICATION"

    invoke-direct {p0, v4, v0}, Lcom/android/server/telecom/MissedCallNotifier;->createTelecomPendingIntent(Ljava/lang/String;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v7, v2, v3, v4}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 356
    const v2, 0x7f020001

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const v4, 0x7f08000f

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0}, Lcom/android/server/telecom/MissedCallNotifier;->createSendSmsFromNotificationPendingIntent(Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v7, v2, v3, v0}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 366
    :cond_2
    :goto_2
    const-string v0, "Notifying %d missed calls to SViewCover"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p0, v0, v2}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 367
    iget v6, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p4

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/telecom/secutils/SViewCoverNotifier;->notifyMissedCall$6409b06e(Ljava/lang/String;Ljava/lang/String;JI)V

    .line 369
    const-string v0, "support_tphone"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Lcom/android/services/telephony/common/SystemDBInterface;->isTPhoneEnabled()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 370
    const-string v0, "missed call notification by TPhone."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 383
    :goto_3
    return-void

    .line 313
    :cond_3
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ecid_enable"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p2, :cond_4

    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const/4 v2, 0x3

    invoke-static {v0, p2, v2}, Lcom/android/server/telecom/EcidContact;->doLookup(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/server/telecom/EcidContact;

    move-result-object v0

    if-eqz v0, :cond_4

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/android/server/telecom/EcidContact;->getContactName(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ecid name added as "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v0

    sget-object v2, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    invoke-virtual {v0, p2, v2}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const v2, 0x7f080008

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 322
    :cond_6
    const v2, 0x7f08000b

    .line 323
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const v3, 0x7f08000c

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 362
    :cond_7
    const-string v0, "Suppress actions. missedCall number: %d."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {p0, v0, v2}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 374
    :cond_8
    invoke-virtual {v7}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 375
    iget v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    iput v1, v0, Landroid/app/Notification;->missedCount:I

    .line 376
    invoke-static {v0}, Lcom/android/server/telecom/MissedCallNotifier;->configureLedOnNotification(Landroid/app/Notification;)V

    .line 378
    const-string v1, "Adding missed call notification for %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {p0, v1, v2}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 379
    iget-object v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mNotificationManager:Landroid/app/NotificationManager;

    const/4 v2, 0x0

    const/4 v3, 0x1

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    .line 382
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    invoke-static {v0, v1}, Lcom/android/server/telecom/secutils/CallBadge;->updateBadge(Landroid/content/Context;I)V

    goto/16 :goto_3
.end method

.method final clearMissedCalls()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 147
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 148
    const-string v1, "new"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 149
    const-string v1, "is_read"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 150
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 151
    const-string v2, "new"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    const-string v2, " = 1 AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    const-string v2, "type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    const-string v2, " = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    iget-object v2, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 158
    invoke-direct {p0}, Lcom/android/server/telecom/MissedCallNotifier;->cancelMissedCallNotification()V

    .line 159
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    invoke-static {v0, v1}, Lcom/android/server/telecom/secutils/CallBadge;->updateBadge(Landroid/content/Context;I)V

    .line 160
    return-void
.end method

.method public final onCallStateChanged(Lcom/android/server/telecom/Call;II)V
    .locals 2

    .prologue
    .line 138
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    const/4 v0, 0x7

    if-ne p3, v0, :cond_0

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/DisconnectCause;->getCode()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 140
    invoke-virtual {p0, p1}, Lcom/android/server/telecom/MissedCallNotifier;->showMissedCallNotification(Lcom/android/server/telecom/Call;)V

    .line 142
    :cond_0
    return-void
.end method

.method public final showMissedCallNotification(Lcom/android/server/telecom/Call;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const v5, 0x7f08007b

    const v7, 0x7f08000c

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 168
    const-string v0, "showMissedCallNotification"

    new-array v1, v11, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    iget v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    .line 171
    new-instance v4, Lcom/android/server/telecom/secutils/SViewCoverNotifier;

    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-direct {v4, v0}, Lcom/android/server/telecom/secutils/SViewCoverNotifier;-><init>(Landroid/content/Context;)V

    .line 180
    invoke-direct {p0, p1}, Lcom/android/server/telecom/MissedCallNotifier;->getNameForCall(Lcom/android/server/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    .line 181
    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->isDormantOn(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "%s"

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getHandlePresentation()I

    move-result v1

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/android/server/telecom/secutils/TelecomUtils;->getPresentationString(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    .line 186
    invoke-static {}, Lcom/android/server/telecom/secutils/TelecomUtils;->isPrayModeOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "%s"

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    .line 190
    :cond_1
    iget v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    if-ne v0, v10, :cond_4

    .line 191
    const v1, 0x7f08000a

    .line 192
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    move-object v12, v0

    move v0, v1

    move-object v1, v12

    .line 205
    :goto_0
    new-instance v5, Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-direct {v5, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 206
    const v3, 0x108007f

    invoke-virtual {v5, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v3

    iget-object v6, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const v7, 0x7f08000d

    new-array v8, v10, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    aput-object v9, v8, v11

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    iget-object v6, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f090000

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getCreationTimeMillis()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v3

    iget-object v6, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/server/telecom/MissedCallNotifier;->createCallLogPendingIntent()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 220
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getHandle()Landroid/net/Uri;

    move-result-object v6

    .line 221
    if-nez v6, :cond_6

    move-object v0, v2

    .line 224
    :goto_1
    iget v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    if-ne v3, v10, :cond_9

    .line 225
    const-string v3, "Add actions with number %s."

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/server/telecom/Log;->piiHandle(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {p0, v3, v7}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 228
    const-string v3, "usa_cdma_smc_fac_req"

    invoke-static {v3}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 229
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getHandlePresentation()I

    move-result v3

    invoke-static {v3, p1}, Lcom/android/server/telecom/operator/usa/TelecomExtension;->getPresentation(ILcom/android/server/telecom/Call;)I

    move-result v3

    invoke-static {v3}, Lcom/android/server/telecom/operator/usa/TelecomExtension;->isPresentationAllowed(I)Z

    move-result v3

    .line 233
    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const v7, 0x7f08002a

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 235
    const/high16 v0, 0x7f020000

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const v7, 0x7f08000e

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v7, "com.android.server.telecom.ACTION_CALL_BACK_FROM_NOTIFICATION"

    invoke-direct {p0, v7, v6}, Lcom/android/server/telecom/MissedCallNotifier;->createTelecomPendingIntent(Ljava/lang/String;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v7

    invoke-virtual {v5, v0, v3, v7}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 239
    const v0, 0x7f020001

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    const v7, 0x7f08000f

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v6}, Lcom/android/server/telecom/MissedCallNotifier;->createSendSmsFromNotificationPendingIntent(Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v5, v0, v3, v6}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 244
    :cond_2
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getPhotoIcon()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 245
    if-eqz v0, :cond_8

    .line 246
    invoke-virtual {v5, v0}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    .line 247
    invoke-virtual {v4, v0}, Lcom/android/server/telecom/secutils/SViewCoverNotifier;->setPhoto(Landroid/graphics/Bitmap;)V

    .line 262
    :cond_3
    :goto_3
    const-string v0, "Notifying %d missed calls to SViewCover"

    new-array v3, v10, [Ljava/lang/Object;

    iget v6, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v11

    invoke-static {p0, v0, v3}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 263
    invoke-direct {p0, p1}, Lcom/android/server/telecom/MissedCallNotifier;->getNameForCall(Lcom/android/server/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    iget v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    invoke-virtual {v4, p1, v0, v3}, Lcom/android/server/telecom/secutils/SViewCoverNotifier;->notifyMissedCall(Lcom/android/server/telecom/Call;Ljava/lang/String;I)V

    .line 265
    const-string v0, "support_tphone"

    invoke-static {v0}, Lcom/android/services/telephony/common/PhoneFeature;->hasFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {}, Lcom/android/services/telephony/common/SystemDBInterface;->isTPhoneEnabled()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 266
    const-string v0, "missed call notification by TPhone."

    new-array v1, v11, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/server/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    :goto_4
    return-void

    .line 194
    :cond_4
    const v1, 0x7f08000b

    .line 195
    invoke-static {p1}, Lcom/android/server/telecom/secutils/TelecomUtils;->isDormantOn(Lcom/android/server/telecom/Call;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    new-array v5, v10, [Ljava/lang/Object;

    iget v6, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v11

    invoke-virtual {v3, v7, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v12, v0

    move v0, v1

    move-object v1, v12

    goto/16 :goto_0

    .line 199
    :cond_5
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    new-array v3, v10, [Ljava/lang/Object;

    iget v5, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v11

    invoke-virtual {v0, v7, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v12, v0

    move v0, v1

    move-object v1, v12

    goto/16 :goto_0

    .line 221
    :cond_6
    invoke-virtual {v6}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 231
    :cond_7
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getHandlePresentation()I

    move-result v3

    invoke-static {v3}, Lcom/android/server/telecom/secutils/TelecomUtils;->isNumberExists(I)Z

    move-result v3

    goto/16 :goto_2

    .line 249
    :cond_8
    invoke-virtual {p1}, Lcom/android/server/telecom/Call;->getPhoto()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 250
    if-eqz v0, :cond_3

    instance-of v3, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_3

    .line 251
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 252
    invoke-virtual {v5, v0}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    .line 253
    invoke-virtual {v4, v0}, Lcom/android/server/telecom/secutils/SViewCoverNotifier;->setPhoto(Landroid/graphics/Bitmap;)V

    goto/16 :goto_3

    .line 257
    :cond_9
    const-string v3, "Suppress actions. handle: %s, missedCalls: %d."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/server/telecom/Log;->piiHandle(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v11

    iget v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v10

    invoke-static {p0, v3, v6}, Lcom/android/server/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 270
    :cond_a
    iget v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    if-le v0, v10, :cond_b

    .line 271
    iput-object v5, p0, Lcom/android/server/telecom/MissedCallNotifier;->mNotiBuilder:Landroid/app/Notification$Builder;

    .line 272
    iput-object v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mExpandedText:Ljava/lang/String;

    .line 273
    iput-object p1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mCall:Lcom/android/server/telecom/Call;

    .line 274
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_4

    .line 278
    :cond_b
    invoke-virtual {v5}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 279
    iget v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    iput v1, v0, Landroid/app/Notification;->missedCount:I

    .line 280
    invoke-static {v0}, Lcom/android/server/telecom/MissedCallNotifier;->configureLedOnNotification(Landroid/app/Notification;)V

    .line 282
    const-string v1, "Adding missed call notification for %s."

    new-array v3, v10, [Ljava/lang/Object;

    aput-object p1, v3, v11

    invoke-static {p0, v1, v3}, Lcom/android/server/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 283
    iget-object v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mNotificationManager:Landroid/app/NotificationManager;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v2, v10, v0, v3}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    .line 286
    iget-object v0, p0, Lcom/android/server/telecom/MissedCallNotifier;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/server/telecom/MissedCallNotifier;->mMissedCallCount:I

    invoke-static {v0, v1}, Lcom/android/server/telecom/secutils/CallBadge;->updateBadge(Landroid/content/Context;I)V

    goto/16 :goto_4
.end method
