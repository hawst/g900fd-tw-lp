.class public Lcom/android/providers/downloads/saveas/DownloadFileInfo;
.super Ljava/lang/Object;
.source "DownloadFileInfo.java"


# instance fields
.field mFileName:Ljava/lang/String;

.field mStatus:I

.field mStream:Ljava/io/FileOutputStream;

.field mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/io/FileOutputStream;I)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "stream"    # Ljava/io/FileOutputStream;
    .param p3, "status"    # I

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->mFileName:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->mStream:Ljava/io/FileOutputStream;

    .line 33
    iput p3, p0, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->mStatus:I

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->mTitle:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/FileOutputStream;ILjava/lang/String;)V
    .locals 0
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "stream"    # Ljava/io/FileOutputStream;
    .param p3, "status"    # I
    .param p4, "title"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->mFileName:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->mStream:Ljava/io/FileOutputStream;

    .line 40
    iput p3, p0, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->mStatus:I

    .line 41
    iput-object p4, p0, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->mTitle:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public getFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->mTitle:Ljava/lang/String;

    return-object v0
.end method
