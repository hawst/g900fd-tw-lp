.class public Lcom/android/providers/downloads/saveas/DownloadSaveasManager;
.super Ljava/lang/Object;
.source "DownloadSaveasManager.java"


# static fields
.field public static final DOWNLOAD_SAVEAS_URI:Landroid/net/Uri;

.field private static RENAME_CANCELED:I

.field private static RENAME_CONFIRMED:I

.field private static RENAME_PENDING:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-string v0, "content://downloadsaveas/sdownloadsaveas"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->DOWNLOAD_SAVEAS_URI:Landroid/net/Uri;

    .line 58
    const/4 v0, 0x0

    sput v0, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->RENAME_PENDING:I

    .line 59
    const/4 v0, 0x1

    sput v0, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->RENAME_CANCELED:I

    .line 60
    const/4 v0, 0x2

    sput v0, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->RENAME_CONFIRMED:I

    return-void
.end method

.method public static checkRenameStatus(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/StringBuilder;)I
    .locals 8
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "renameCursor"    # Landroid/database/Cursor;
    .param p2, "strBuild"    # Ljava/lang/StringBuilder;

    .prologue
    .line 72
    sget v2, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->RENAME_PENDING:I

    .line 74
    .local v2, "ret":I
    invoke-interface {p1}, Landroid/database/Cursor;->requery()Z

    .line 76
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 78
    const-string v5, "status"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 79
    .local v0, "columnId":I
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 80
    .local v4, "status":I
    const-string v5, "_id"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 81
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 82
    .local v3, "rid":I
    const-string v5, "hint"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 83
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "renamedFilename":Ljava/lang/String;
    sget-boolean v5, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v5, :cond_0

    .line 86
    const-string v5, "DownloadManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DownloadSaveasManager [saveas] id : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " status "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " filename "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_0
    const/16 v5, 0x1ea

    if-ne v4, v5, :cond_3

    .line 91
    sget-boolean v5, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v5, :cond_1

    .line 92
    const-string v5, "DownloadManager"

    const-string v6, "DownloadSaveasManager [saveas] rename(download) is canceled. This is ERROR case. This could not be called"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_1
    sget v2, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->RENAME_CANCELED:I

    .line 115
    .end local v0    # "columnId":I
    .end local v1    # "renamedFilename":Ljava/lang/String;
    .end local v3    # "rid":I
    .end local v4    # "status":I
    :cond_2
    :goto_0
    return v2

    .line 96
    .restart local v0    # "columnId":I
    .restart local v1    # "renamedFilename":Ljava/lang/String;
    .restart local v3    # "rid":I
    .restart local v4    # "status":I
    :cond_3
    const/16 v5, 0xc0

    if-ne v4, v5, :cond_5

    .line 98
    sget-boolean v5, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v5, :cond_4

    .line 99
    const-string v5, "DownloadManager"

    const-string v6, "DownloadSaveasManager [saveas] rename is confirmed"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_4
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    sget v2, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->RENAME_CONFIRMED:I

    goto :goto_0

    .line 105
    :cond_5
    const/16 v5, 0xbf

    if-ne v4, v5, :cond_2

    .line 107
    sget-boolean v5, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v5, :cond_6

    .line 108
    const-string v5, "DownloadManager"

    const-string v6, "DownloadSaveasManager [saveas] rename is still pending"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_6
    sget v2, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->RENAME_PENDING:I

    goto :goto_0
.end method

.method private static chooseUniqueFilename(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 10
    .param p0, "destination"    # I
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "extension"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;
    .param p4, "recoveryDir"    # Z

    .prologue
    .line 591
    new-instance v4, Ljava/util/Random;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    invoke-direct {v4, v8, v9}, Ljava/util/Random;-><init>(J)V

    .line 593
    .local v4, "sRandom":Ljava/util/Random;
    const/4 v6, 0x0

    .line 594
    .local v6, "temp_extension":Ljava/lang/String;
    const/4 v0, 0x0

    .line 597
    .local v0, "fullFilename":Ljava/lang/String;
    if-eqz p3, :cond_1

    const-string v7, "application/vnd.oma.dd+xml"

    invoke-virtual {p3, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 598
    const-string v6, ".dd"

    .line 602
    :goto_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 603
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    if-eqz p4, :cond_0

    const/4 v7, 0x1

    if-eq p0, v7, :cond_2

    const/4 v7, 0x5

    if-eq p0, v7, :cond_2

    const/4 v7, 0x2

    if-eq p0, v7, :cond_2

    const/4 v7, 0x3

    if-eq p0, v7, :cond_2

    :cond_0
    move-object v1, v0

    .line 631
    .end local v0    # "fullFilename":Ljava/lang/String;
    .local v1, "fullFilename":Ljava/lang/String;
    :goto_1
    return-object v1

    .line 600
    .end local v1    # "fullFilename":Ljava/lang/String;
    .restart local v0    # "fullFilename":Ljava/lang/String;
    :cond_1
    move-object v6, p2

    goto :goto_0

    .line 611
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 626
    const/4 v5, 0x1

    .line 627
    .local v5, "sequence":I
    const/4 v3, 0x1

    .local v3, "magnitude":I
    :goto_2
    const v7, 0x3b9aca00

    if-ge v3, v7, :cond_6

    .line 628
    const/4 v2, 0x0

    .local v2, "iteration":I
    :goto_3
    const/16 v7, 0x9

    if-ge v2, v7, :cond_5

    .line 629
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 630
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_3

    move-object v1, v0

    .line 631
    .end local v0    # "fullFilename":Ljava/lang/String;
    .restart local v1    # "fullFilename":Ljava/lang/String;
    goto :goto_1

    .line 633
    .end local v1    # "fullFilename":Ljava/lang/String;
    .restart local v0    # "fullFilename":Ljava/lang/String;
    :cond_3
    sget-boolean v7, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v7, :cond_4

    .line 634
    const-string v7, "DownloadManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file with sequence number "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " exists"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    :cond_4
    invoke-virtual {v4, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    add-int/2addr v5, v7

    .line 628
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 627
    :cond_5
    mul-int/lit8 v3, v3, 0xa

    goto :goto_2

    .line 639
    .end local v2    # "iteration":I
    :cond_6
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "failed to generate an unused filename on internal download storage"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7
.end method

.method public static getSaveAsFilename(Landroid/content/Context;Lcom/android/providers/downloads/DownloadInfo;Ljava/lang/String;Ljava/io/FileOutputStream;Ljava/lang/String;[I)Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .locals 54
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "info"    # Lcom/android/providers/downloads/DownloadInfo;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "stream"    # Ljava/io/FileOutputStream;
    .param p4, "mimeType"    # Ljava/lang/String;
    .param p5, "paramStatus"    # [I

    .prologue
    .line 182
    const/16 v49, 0x0

    .line 183
    .local v49, "tempFilenameBeforeExtension":Ljava/lang/String;
    const/16 v47, 0x0

    .line 184
    .local v47, "tempFileExtension":Ljava/lang/String;
    move-object/from16 v48, p2

    .line 186
    .local v48, "tempFilename":Ljava/lang/String;
    const/16 v42, 0x0

    .line 188
    .local v42, "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    const/4 v14, 0x0

    .line 189
    .local v14, "breakLoop":Z
    const/16 v21, 0x0

    .line 191
    .local v21, "isOMA":Z
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_0

    .line 192
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] getSaveAsFilename "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :cond_0
    const-string v4, "DownloadManager"

    const-string v7, "DownloadSaveasManager : [saveas] skipSaveAsCheck"

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    move-object/from16 v0, p0

    invoke-static {v0, v10, v11}, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->skipSaveAsCheck(Landroid/content/Context;J)Z

    move-result v4

    const/4 v7, 0x1

    if-ne v4, v7, :cond_1

    .line 198
    new-instance v4, Lcom/android/providers/downloads/saveas/DownloadFileInfo;

    const/4 v7, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {v4, v0, v1, v7}, Lcom/android/providers/downloads/saveas/DownloadFileInfo;-><init>(Ljava/lang/String;Ljava/io/FileOutputStream;I)V

    .line 569
    :goto_0
    return-object v4

    .line 202
    :cond_1
    sget-object v4, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->DOWNLOAD_SAVEAS_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v35

    .line 204
    .local v35, "renameUri":Landroid/net/Uri;
    if-eqz v35, :cond_2

    .line 205
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v35

    invoke-virtual {v4, v0, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 208
    :cond_2
    new-instance v53, Landroid/content/ContentValues;

    invoke-direct/range {v53 .. v53}, Landroid/content/ContentValues;-><init>()V

    .line 211
    .local v53, "values":Landroid/content/ContentValues;
    const-string v4, "_id"

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 212
    const-string v4, "hint"

    move-object/from16 v0, v53

    move-object/from16 v1, p2

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const-string v4, "mimetype"

    move-object/from16 v0, v53

    move-object/from16 v1, p4

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v4, "status"

    const/16 v7, 0xbf

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 216
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_3

    .line 217
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] mimeType "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p4

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    const-string v4, "DownloadManager"

    const-string v7, "DownloadSaveasManager : [saveas] Status 191"

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :cond_3
    sget-object v4, Landroid/provider/Downloads$Impl;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    .line 223
    .local v5, "downloadUri":Landroid/net/Uri;
    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "_id"

    aput-object v7, v6, v4

    .line 224
    .local v6, "downloadProjection":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 228
    .local v15, "downloadCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    .line 230
    const-string v4, "downloadmethod"

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v25

    .line 231
    .local v25, "mMethodColumn":I
    move/from16 v0, v25

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 233
    .local v13, "DownloadMethod":I
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] DownloadMethod "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    if-eqz v13, :cond_4

    .line 236
    const/16 v21, 0x1

    .line 239
    :cond_4
    if-eqz v21, :cond_6

    .line 240
    const-string v4, "dd_primaryMimeType"

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v24

    .line 241
    .local v24, "mDDTypeId":I
    const-string v4, "dd_fileName"

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v23

    .line 243
    .local v23, "mDDFilenameColumnId":I
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] DD mimetype "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v24

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] DD filename "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v23

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v28, v4, 0x1

    .line 247
    .local v28, "namePartIndex":I
    const/16 v32, 0x0

    .line 248
    .local v32, "pathPart":Ljava/lang/String;
    const/16 v27, 0x0

    .line 249
    .local v27, "namePart":Ljava/lang/String;
    if-lez v28, :cond_5

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v4

    move/from16 v0, v28

    if-ge v0, v4, :cond_5

    .line 250
    const/4 v4, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v32

    .line 251
    move-object/from16 v0, p2

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v27

    .line 255
    :cond_5
    const-string v4, "hint"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v23

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v4, "mimetype"

    move/from16 v0, v24

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    .end local v23    # "mDDFilenameColumnId":I
    .end local v24    # "mDDTypeId":I
    .end local v27    # "namePart":Ljava/lang/String;
    .end local v28    # "namePartIndex":I
    .end local v32    # "pathPart":Ljava/lang/String;
    :cond_6
    if-eqz v15, :cond_7

    :try_start_1
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8

    .line 276
    .end local v13    # "DownloadMethod":I
    .end local v25    # "mMethodColumn":I
    :cond_7
    :goto_1
    const/16 v4, 0x2e

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v17

    .line 277
    .local v17, "extensionPartIndex":I
    if-lez v17, :cond_8

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v4

    move/from16 v0, v17

    if-ge v0, v4, :cond_8

    .line 278
    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v47

    .line 282
    :cond_8
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v7, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->DOWNLOAD_SAVEAS_URI:Landroid/net/Uri;

    move-object/from16 v0, v53

    invoke-virtual {v4, v7, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 284
    if-eqz v21, :cond_15

    .line 286
    new-instance v20, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW_DOWNLOADS"

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 287
    .local v20, "intent":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 288
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 299
    :goto_2
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_9

    .line 300
    const-string v4, "DownloadManager"

    const-string v7, "DownloadSaveasManager : [saveas] downloads view started"

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    :cond_9
    const/4 v4, 0x3

    new-array v9, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "_id"

    aput-object v7, v9, v4

    const/4 v4, 0x1

    const-string v7, "hint"

    aput-object v7, v9, v4

    const/4 v4, 0x2

    const-string v7, "status"

    aput-object v7, v9, v4

    .line 305
    .local v9, "projection":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v8, v35

    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v34

    .line 307
    .local v34, "renameCursor":Landroid/database/Cursor;
    if-nez v34, :cond_16

    .line 309
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_a

    .line 310
    const-string v4, "DownloadManager"

    const-string v7, "DownloadSaveasManager : [saveas] rename info is null. continue download"

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    :cond_a
    :goto_3
    if-eqz p3, :cond_b

    :try_start_2
    invoke-virtual/range {p3 .. p3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    .line 505
    :cond_b
    :goto_4
    const-string v4, "recovery"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v49

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v47

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v33

    .line 507
    .local v33, "recoveryDir":Z
    const-class v7, Lcom/android/providers/downloads/Helpers;

    monitor-enter v7

    .line 509
    :try_start_3
    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/providers/downloads/DownloadInfo;->mDestination:I

    move-object/from16 v0, v49

    move-object/from16 v1, v47

    move-object/from16 v2, p4

    move/from16 v3, v33

    invoke-static {v4, v0, v1, v2, v3}, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->chooseUniqueFilename(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v19

    .line 510
    .local v19, "fullFilename":Ljava/lang/String;
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_c

    .line 511
    const-string v4, "DownloadManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "DownloadSaveasManager : [saveas] fullFilename "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    :cond_c
    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    const/16 v8, 0x1ea

    if-ne v4, v8, :cond_2e

    .line 515
    new-instance v43, Lcom/android/providers/downloads/saveas/DownloadFileInfo;

    const/4 v4, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1ea

    move-object/from16 v0, v43

    invoke-direct {v0, v4, v8, v10}, Lcom/android/providers/downloads/saveas/DownloadFileInfo;-><init>(Ljava/lang/String;Ljava/io/FileOutputStream;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 539
    .end local v42    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .local v43, "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    :cond_d
    :goto_5
    if-nez v43, :cond_31

    .line 540
    :try_start_4
    new-instance v42, Lcom/android/providers/downloads/saveas/DownloadFileInfo;

    const/4 v4, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1ec

    move-object/from16 v0, v42

    invoke-direct {v0, v4, v8, v10}, Lcom/android/providers/downloads/saveas/DownloadFileInfo;-><init>(Ljava/lang/String;Ljava/io/FileOutputStream;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 542
    .end local v43    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .restart local v42    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    :goto_6
    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 544
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_e

    .line 545
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] retInfo.mFilename "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v42

    iget-object v8, v0, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->mFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    :cond_e
    const/16 v41, 0x0

    .line 550
    .local v41, "retFile":Ljava/io/File;
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->mFileName:Ljava/lang/String;

    if-eqz v4, :cond_f

    .line 551
    new-instance v41, Ljava/io/File;

    .end local v41    # "retFile":Ljava/io/File;
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->mFileName:Ljava/lang/String;

    move-object/from16 v0, v41

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 553
    .restart local v41    # "retFile":Ljava/io/File;
    :cond_f
    if-eqz v41, :cond_12

    .line 554
    invoke-virtual/range {v41 .. v41}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v31

    .line 555
    .local v31, "parentPath":Ljava/lang/String;
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_10

    .line 556
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] Parent folder is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    :cond_10
    if-eqz v31, :cond_12

    .line 559
    new-instance v30, Ljava/io/File;

    invoke-direct/range {v30 .. v31}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 560
    .local v30, "parentFile":Ljava/io/File;
    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_12

    .line 561
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_11

    .line 562
    const-string v4, "DownloadManager"

    const-string v7, "DownloadSaveasManager : [saveas] make folder"

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    :cond_11
    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->mkdirs()Z

    .end local v30    # "parentFile":Ljava/io/File;
    .end local v31    # "parentPath":Ljava/lang/String;
    :cond_12
    move-object/from16 v4, v42

    .line 569
    goto/16 :goto_0

    .line 260
    .end local v9    # "projection":[Ljava/lang/String;
    .end local v17    # "extensionPartIndex":I
    .end local v19    # "fullFilename":Ljava/lang/String;
    .end local v20    # "intent":Landroid/content/Intent;
    .end local v33    # "recoveryDir":Z
    .end local v34    # "renameCursor":Landroid/database/Cursor;
    .end local v41    # "retFile":Ljava/io/File;
    :catch_0
    move-exception v16

    .line 262
    .local v16, "e":Ljava/lang/Exception;
    :try_start_6
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_13

    .line 263
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] exception"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 269
    :cond_13
    if-eqz v15, :cond_7

    :try_start_7
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_1

    .line 270
    :catch_1
    move-exception v4

    goto/16 :goto_1

    .line 268
    .end local v16    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 269
    if-eqz v15, :cond_14

    :try_start_8
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_9

    .line 272
    :cond_14
    :goto_7
    throw v4

    .line 292
    .restart local v17    # "extensionPartIndex":I
    :cond_15
    new-instance v20, Landroid/content/Intent;

    const-string v4, "android.intent.action.DIRECT_SAVEAS_SCREEN"

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 293
    .restart local v20    # "intent":Landroid/content/Intent;
    const-string v4, "direct_saveas_id"

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 294
    const-string v4, "sbrowser"

    const/4 v7, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 295
    const/high16 v4, 0x10800000

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 296
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 315
    .restart local v9    # "projection":[Ljava/lang/String;
    .restart local v34    # "renameCursor":Landroid/database/Cursor;
    :cond_16
    new-instance v44, Ljava/util/concurrent/Semaphore;

    const/4 v4, 0x0

    move-object/from16 v0, v44

    invoke-direct {v0, v4}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 316
    .local v44, "sem":Ljava/util/concurrent/Semaphore;
    new-instance v29, Lcom/android/providers/downloads/saveas/DownloadSaveAsContentObserver;

    move-object/from16 v0, v29

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/android/providers/downloads/saveas/DownloadSaveAsContentObserver;-><init>(Ljava/util/concurrent/Semaphore;)V

    .line 320
    .local v29, "observer":Lcom/android/providers/downloads/saveas/DownloadSaveAsContentObserver;
    :try_start_9
    new-instance v46, Ljava/lang/StringBuilder;

    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    .line 321
    .local v46, "strBuild":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v7, 0x1

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v4, v0, v7, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 323
    const/16 v52, 0x1

    .line 327
    .local v52, "tryOnce":Z
    :cond_17
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SecDownloadSaveasManager : [saveas][exp] sem wait id "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    const/4 v4, 0x1

    move/from16 v0, v52

    if-ne v0, v4, :cond_1e

    .line 330
    const/16 v52, 0x0

    .line 331
    const-string v4, "DownloadManager"

    const-string v7, "SecDownloadSaveasManager : [saveas][exp] try once"

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    :goto_8
    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->checkRenameStatus(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/StringBuilder;)I

    move-result v45

    .line 339
    .local v45, "status":I
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_18

    .line 340
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[saveas] status "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v45

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    :cond_18
    sget v4, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->RENAME_PENDING:I

    move/from16 v0, v45

    if-ne v0, v4, :cond_19

    .line 344
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_19

    .line 345
    const-string v4, "DownloadManager"

    const-string v7, "[saveas] RENAME_PENDING "

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :cond_19
    sget v4, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->RENAME_CONFIRMED:I

    move/from16 v0, v45

    if-ne v0, v4, :cond_2a

    .line 350
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_1a

    .line 351
    const-string v4, "DownloadManager"

    const-string v7, "[saveas] RENAME_CONFIRMED "

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :cond_1a
    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v28, v4, 0x1

    .line 356
    .restart local v28    # "namePartIndex":I
    const/16 v32, 0x0

    .line 357
    .restart local v32    # "pathPart":Ljava/lang/String;
    const/16 v27, 0x0

    .line 358
    .restart local v27    # "namePart":Ljava/lang/String;
    if-lez v28, :cond_1b

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v4

    move/from16 v0, v28

    if-ge v0, v4, :cond_1b

    .line 359
    const/4 v4, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v32

    .line 360
    move-object/from16 v0, p2

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v27

    .line 364
    :cond_1b
    if-nez v27, :cond_1c

    .line 365
    move-object/from16 v27, p2

    .line 368
    :cond_1c
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_1d

    .line 369
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] filename "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] namePartIndex "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] pathPart "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v32

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] namePart "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v27

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    :cond_1d
    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    .line 377
    .local v36, "renamedFullFilename":Ljava/lang/String;
    if-nez v36, :cond_20

    .line 378
    new-instance v4, Lcom/android/providers/downloads/saveas/DownloadFileInfo;

    const/4 v7, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {v4, v0, v1, v7}, Lcom/android/providers/downloads/saveas/DownloadFileInfo;-><init>(Ljava/lang/String;Ljava/io/FileOutputStream;I)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 479
    :try_start_a
    invoke-interface/range {v34 .. v34}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_a

    .line 485
    :goto_9
    :try_start_b
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v7, v0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2

    goto/16 :goto_0

    .line 486
    :catch_2
    move-exception v7

    goto/16 :goto_0

    .line 334
    .end local v27    # "namePart":Ljava/lang/String;
    .end local v28    # "namePartIndex":I
    .end local v32    # "pathPart":Ljava/lang/String;
    .end local v36    # "renamedFullFilename":Ljava/lang/String;
    .end local v45    # "status":I
    :cond_1e
    :try_start_c
    invoke-virtual/range {v44 .. v44}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_8

    .line 470
    .end local v46    # "strBuild":Ljava/lang/StringBuilder;
    .end local v52    # "tryOnce":Z
    :catch_3
    move-exception v16

    .line 472
    .restart local v16    # "e":Ljava/lang/Exception;
    :try_start_d
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_1f

    .line 473
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] exception"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 479
    :cond_1f
    :try_start_e
    invoke-interface/range {v34 .. v34}, Landroid/database/Cursor;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_c

    .line 485
    :goto_a
    :try_start_f
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_4

    goto/16 :goto_3

    .line 486
    :catch_4
    move-exception v4

    goto/16 :goto_3

    .line 380
    .end local v16    # "e":Ljava/lang/Exception;
    .restart local v27    # "namePart":Ljava/lang/String;
    .restart local v28    # "namePartIndex":I
    .restart local v32    # "pathPart":Ljava/lang/String;
    .restart local v36    # "renamedFullFilename":Ljava/lang/String;
    .restart local v45    # "status":I
    .restart local v46    # "strBuild":Ljava/lang/StringBuilder;
    .restart local v52    # "tryOnce":Z
    :cond_20
    :try_start_10
    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v28, v4, 0x1

    .line 381
    const/16 v39, 0x0

    .line 382
    .local v39, "renamedPathPart":Ljava/lang/String;
    const/16 v37, 0x0

    .line 383
    .local v37, "renamedNamePart":Ljava/lang/String;
    if-lez v28, :cond_21

    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->length()I

    move-result v4

    move/from16 v0, v28

    if-ge v0, v4, :cond_21

    .line 384
    const/4 v4, 0x0

    move-object/from16 v0, v36

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v39

    .line 385
    move-object/from16 v0, v36

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v37

    .line 389
    :cond_21
    if-nez v37, :cond_22

    .line 390
    move-object/from16 v37, v36

    .line 393
    :cond_22
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_23

    .line 394
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] renamedFullFilename "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] namePartIndex "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v28

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] renamedPathPart "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v39

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] renamedNamePart "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    :cond_23
    const/16 v4, 0x2e

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v17

    .line 402
    const/16 v38, 0x0

    .line 403
    .local v38, "renamedNamePartBeforeExtension":Ljava/lang/String;
    if-lez v17, :cond_24

    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->length()I

    move-result v4

    move/from16 v0, v17

    if-ge v0, v4, :cond_24

    .line 404
    const/4 v4, 0x0

    move-object/from16 v0, v37

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v38

    .line 408
    :cond_24
    const/16 v50, 0x0

    .line 409
    .local v50, "tempPath":Ljava/lang/String;
    if-eqz v50, :cond_25

    .line 410
    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 411
    .local v22, "mBaseDir":Ljava/io/File;
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_25

    .line 412
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->mkdirs()Z

    .line 417
    .end local v22    # "mBaseDir":Ljava/io/File;
    :cond_25
    const/16 v48, 0x0

    .line 419
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_26

    .line 420
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] renamedNamePartBeforeExtension "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v38

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] tempFilename "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v48

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    :cond_26
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_27

    .line 426
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "info.mTotalBytes "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/providers/downloads/DownloadInfo;->mTotalBytes:J

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    :cond_27
    new-instance v26, Landroid/content/ContentValues;

    invoke-direct/range {v26 .. v26}, Landroid/content/ContentValues;-><init>()V

    .line 432
    .local v26, "mRunningValues":Landroid/content/ContentValues;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v7, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->DOWNLOAD_SAVEAS_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-static {v7, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v4, v7, v0, v8, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 435
    const/16 v4, 0x2e

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v17

    .line 436
    if-lez v17, :cond_28

    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->length()I

    move-result v4

    move/from16 v0, v17

    if-ge v0, v4, :cond_28

    .line 437
    const/4 v4, 0x0

    move-object/from16 v0, v36

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v49

    .line 440
    :cond_28
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_29

    .line 441
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] tempFilenameBeforeExtension "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v49

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] set temp file path -> _DATA "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v49

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v47

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    :cond_29
    const-string v4, "_data"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v49

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v47

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v7, Landroid/provider/Downloads$Impl;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-static {v7, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v4, v7, v0, v8, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 449
    const/4 v14, 0x1

    .line 452
    .end local v26    # "mRunningValues":Landroid/content/ContentValues;
    .end local v27    # "namePart":Ljava/lang/String;
    .end local v28    # "namePartIndex":I
    .end local v32    # "pathPart":Ljava/lang/String;
    .end local v36    # "renamedFullFilename":Ljava/lang/String;
    .end local v37    # "renamedNamePart":Ljava/lang/String;
    .end local v38    # "renamedNamePartBeforeExtension":Ljava/lang/String;
    .end local v39    # "renamedPathPart":Ljava/lang/String;
    .end local v50    # "tempPath":Ljava/lang/String;
    :cond_2a
    sget v4, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->RENAME_CANCELED:I

    move/from16 v0, v45

    if-ne v0, v4, :cond_2c

    .line 453
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_2b

    .line 454
    const-string v4, "DownloadManager"

    const-string v7, "[saveas] RENAME_CANCELED "

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    :cond_2b
    const/16 v4, 0x1ea

    move-object/from16 v0, p1

    iput v4, v0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    .line 459
    const/4 v4, 0x0

    const/16 v7, 0x1ea

    aput v7, p5, v4

    .line 460
    const/4 v14, 0x1

    .line 463
    :cond_2c
    if-eqz v14, :cond_17

    .line 465
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_2d

    .line 466
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] continue download with renamed filename "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v48

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_3
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 479
    :cond_2d
    :try_start_11
    invoke-interface/range {v34 .. v34}, Landroid/database/Cursor;->close()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_b

    .line 485
    :goto_b
    :try_start_12
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_5

    goto/16 :goto_3

    .line 486
    :catch_5
    move-exception v4

    goto/16 :goto_3

    .line 478
    .end local v45    # "status":I
    .end local v46    # "strBuild":Ljava/lang/StringBuilder;
    .end local v52    # "tryOnce":Z
    :catchall_1
    move-exception v4

    .line 479
    :try_start_13
    invoke-interface/range {v34 .. v34}, Landroid/database/Cursor;->close()V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_d

    .line 485
    :goto_c
    :try_start_14
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v7, v0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_f

    .line 488
    :goto_d
    throw v4

    .line 496
    .end local v29    # "observer":Lcom/android/providers/downloads/saveas/DownloadSaveAsContentObserver;
    .end local v44    # "sem":Ljava/util/concurrent/Semaphore;
    :catch_6
    move-exception v16

    .line 498
    .restart local v16    # "e":Ljava/lang/Exception;
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_b

    .line 499
    const-string v4, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DownloadSaveasManager : [saveas] stream close exception"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 516
    .end local v16    # "e":Ljava/lang/Exception;
    .restart local v19    # "fullFilename":Ljava/lang/String;
    .restart local v33    # "recoveryDir":Z
    :cond_2e
    if-eqz v19, :cond_30

    .line 519
    :try_start_15
    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v51, v4, 0x1

    .line 520
    .local v51, "titleIndex":I
    move-object/from16 v0, v19

    move/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v40

    .line 521
    .local v40, "renamedTitle":Ljava/lang/String;
    new-instance v43, Lcom/android/providers/downloads/saveas/DownloadFileInfo;

    const/4 v4, 0x0

    const/16 v8, 0xc8

    move-object/from16 v0, v43

    move-object/from16 v1, v19

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v4, v8, v2}, Lcom/android/providers/downloads/saveas/DownloadFileInfo;-><init>(Ljava/lang/String;Ljava/io/FileOutputStream;ILjava/lang/String;)V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_7
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    .line 523
    .end local v42    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .restart local v43    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    :try_start_16
    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 524
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 525
    .local v18, "f":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2f

    .line 526
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->delete()Z
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_e
    .catchall {:try_start_16 .. :try_end_16} :catchall_3

    .line 528
    :cond_2f
    const/16 p2, 0x0

    goto/16 :goto_5

    .line 531
    .end local v18    # "f":Ljava/io/File;
    .end local v40    # "renamedTitle":Ljava/lang/String;
    .end local v43    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .end local v51    # "titleIndex":I
    .restart local v42    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    :catch_7
    move-exception v16

    .line 533
    .restart local v16    # "e":Ljava/lang/Exception;
    :goto_e
    :try_start_17
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_30

    .line 534
    const-string v4, "DownloadManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "DownloadSaveasManager : [saveas] open output stream"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .end local v16    # "e":Ljava/lang/Exception;
    :cond_30
    move-object/from16 v43, v42

    .end local v42    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .restart local v43    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    goto/16 :goto_5

    .line 542
    .end local v19    # "fullFilename":Ljava/lang/String;
    .end local v43    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .restart local v42    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    :catchall_2
    move-exception v4

    :goto_f
    monitor-exit v7
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    throw v4

    .line 270
    .end local v9    # "projection":[Ljava/lang/String;
    .end local v17    # "extensionPartIndex":I
    .end local v20    # "intent":Landroid/content/Intent;
    .end local v33    # "recoveryDir":Z
    .end local v34    # "renameCursor":Landroid/database/Cursor;
    .restart local v13    # "DownloadMethod":I
    .restart local v25    # "mMethodColumn":I
    :catch_8
    move-exception v4

    goto/16 :goto_1

    .end local v13    # "DownloadMethod":I
    .end local v25    # "mMethodColumn":I
    :catch_9
    move-exception v7

    goto/16 :goto_7

    .line 480
    .restart local v9    # "projection":[Ljava/lang/String;
    .restart local v17    # "extensionPartIndex":I
    .restart local v20    # "intent":Landroid/content/Intent;
    .restart local v27    # "namePart":Ljava/lang/String;
    .restart local v28    # "namePartIndex":I
    .restart local v29    # "observer":Lcom/android/providers/downloads/saveas/DownloadSaveAsContentObserver;
    .restart local v32    # "pathPart":Ljava/lang/String;
    .restart local v34    # "renameCursor":Landroid/database/Cursor;
    .restart local v36    # "renamedFullFilename":Ljava/lang/String;
    .restart local v44    # "sem":Ljava/util/concurrent/Semaphore;
    .restart local v45    # "status":I
    .restart local v46    # "strBuild":Ljava/lang/StringBuilder;
    .restart local v52    # "tryOnce":Z
    :catch_a
    move-exception v7

    goto/16 :goto_9

    .end local v27    # "namePart":Ljava/lang/String;
    .end local v28    # "namePartIndex":I
    .end local v32    # "pathPart":Ljava/lang/String;
    .end local v36    # "renamedFullFilename":Ljava/lang/String;
    :catch_b
    move-exception v4

    goto/16 :goto_b

    .end local v45    # "status":I
    .end local v46    # "strBuild":Ljava/lang/StringBuilder;
    .end local v52    # "tryOnce":Z
    .restart local v16    # "e":Ljava/lang/Exception;
    :catch_c
    move-exception v4

    goto/16 :goto_a

    .end local v16    # "e":Ljava/lang/Exception;
    :catch_d
    move-exception v7

    goto/16 :goto_c

    .line 542
    .end local v29    # "observer":Lcom/android/providers/downloads/saveas/DownloadSaveAsContentObserver;
    .end local v42    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .end local v44    # "sem":Ljava/util/concurrent/Semaphore;
    .restart local v19    # "fullFilename":Ljava/lang/String;
    .restart local v33    # "recoveryDir":Z
    .restart local v43    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    :catchall_3
    move-exception v4

    move-object/from16 v42, v43

    .end local v43    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .restart local v42    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    goto :goto_f

    .line 531
    .end local v42    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .restart local v40    # "renamedTitle":Ljava/lang/String;
    .restart local v43    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .restart local v51    # "titleIndex":I
    :catch_e
    move-exception v16

    move-object/from16 v42, v43

    .end local v43    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .restart local v42    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    goto :goto_e

    .line 486
    .end local v19    # "fullFilename":Ljava/lang/String;
    .end local v33    # "recoveryDir":Z
    .end local v40    # "renamedTitle":Ljava/lang/String;
    .end local v51    # "titleIndex":I
    .restart local v29    # "observer":Lcom/android/providers/downloads/saveas/DownloadSaveAsContentObserver;
    .restart local v44    # "sem":Ljava/util/concurrent/Semaphore;
    :catch_f
    move-exception v7

    goto/16 :goto_d

    .end local v29    # "observer":Lcom/android/providers/downloads/saveas/DownloadSaveAsContentObserver;
    .end local v42    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .end local v44    # "sem":Ljava/util/concurrent/Semaphore;
    .restart local v19    # "fullFilename":Ljava/lang/String;
    .restart local v33    # "recoveryDir":Z
    .restart local v43    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    :cond_31
    move-object/from16 v42, v43

    .end local v43    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .restart local v42    # "retInfo":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    goto/16 :goto_6
.end method

.method public static skipSaveAsCheck(Landroid/content/Context;J)Z
    .locals 11
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "lId"    # J

    .prologue
    const/4 v3, 0x0

    .line 121
    const/4 v9, 0x0

    .line 123
    .local v9, "ret":Z
    sget-object v0, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->DOWNLOAD_SAVEAS_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 124
    .local v1, "renameUri":Landroid/net/Uri;
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    const-string v4, "hint"

    aput-object v4, v2, v0

    const/4 v0, 0x2

    const-string v4, "status"

    aput-object v4, v2, v0

    .line 125
    .local v2, "projection":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 127
    .local v8, "renameCursor":Landroid/database/Cursor;
    if-nez v8, :cond_1

    .line 129
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_0

    const-string v0, "DownloadManager"

    const-string v3, "DownloadSaveasManager : [saveas] rename info is null. continue download"

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_0
    const/4 v9, 0x1

    .line 175
    :goto_0
    return v9

    .line 134
    :cond_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 136
    const-string v0, "status"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 137
    .local v6, "columnId":I
    invoke-interface {v8, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 139
    .local v10, "status":I
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_2

    const-string v0, "DownloadManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DownloadSaveasManager [saveas] status "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_2
    const/16 v0, 0x1ea

    if-ne v10, v0, :cond_5

    .line 143
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_3

    const-string v0, "DownloadManager"

    const-string v3, "DownloadSaveasManager [saveas] rename(download) is canceled. skip SaveAs check"

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    :cond_3
    const/4 v9, 0x1

    .line 168
    .end local v6    # "columnId":I
    .end local v10    # "status":I
    :cond_4
    :goto_1
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 169
    :catch_0
    move-exception v0

    goto :goto_0

    .line 146
    .restart local v6    # "columnId":I
    .restart local v10    # "status":I
    :cond_5
    const/16 v0, 0xc0

    if-ne v10, v0, :cond_7

    .line 148
    :try_start_2
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_6

    const-string v0, "DownloadManager"

    const-string v3, "DownloadSaveasManager [saveas] rename is confirmed. skip SaveAs check"

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    :cond_6
    const/4 v9, 0x1

    goto :goto_1

    .line 151
    :cond_7
    const/16 v0, 0xbe

    if-ne v10, v0, :cond_9

    .line 153
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_4

    const-string v0, "DownloadManager"

    const-string v3, "DownloadSaveasOperations [saveas] rename is still pending (insert)"

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 160
    .end local v6    # "columnId":I
    .end local v10    # "status":I
    :catch_1
    move-exception v7

    .line 162
    .local v7, "e":Ljava/lang/Exception;
    :try_start_3
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_8

    const-string v0, "DownloadManager"

    const-string v3, "DownloadSaveasOperations [saveas] rename check in update download"

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 163
    :cond_8
    const/4 v9, 0x1

    .line 168
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 169
    :catch_2
    move-exception v0

    goto :goto_0

    .line 155
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v6    # "columnId":I
    .restart local v10    # "status":I
    :cond_9
    const/16 v0, 0xbf

    if-ne v10, v0, :cond_4

    .line 157
    :try_start_5
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_4

    const-string v0, "DownloadManager"

    const-string v3, "DownloadSaveasOperations [saveas] rename is still pending (update)"

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 167
    .end local v6    # "columnId":I
    .end local v10    # "status":I
    :catchall_0
    move-exception v0

    .line 168
    :try_start_6
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 171
    :goto_2
    throw v0

    .line 169
    :catch_3
    move-exception v3

    goto :goto_2
.end method
