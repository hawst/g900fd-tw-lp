.class Lcom/android/providers/downloads/DownloadProvider$1;
.super Ljava/lang/Object;
.source "DownloadProvider.java"

# interfaces
.implements Landroid/os/ParcelFileDescriptor$OnCloseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/providers/downloads/DownloadProvider;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/providers/downloads/DownloadProvider;

.field final synthetic val$file:Ljava/io/File;

.field final synthetic val$shouldScan:Z

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/android/providers/downloads/DownloadProvider;Ljava/io/File;Landroid/net/Uri;Z)V
    .locals 0

    .prologue
    .line 1436
    iput-object p1, p0, Lcom/android/providers/downloads/DownloadProvider$1;->this$0:Lcom/android/providers/downloads/DownloadProvider;

    iput-object p2, p0, Lcom/android/providers/downloads/DownloadProvider$1;->val$file:Ljava/io/File;

    iput-object p3, p0, Lcom/android/providers/downloads/DownloadProvider$1;->val$uri:Landroid/net/Uri;

    iput-boolean p4, p0, Lcom/android/providers/downloads/DownloadProvider$1;->val$shouldScan:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClose(Ljava/io/IOException;)V
    .locals 7
    .param p1, "e"    # Ljava/io/IOException;

    .prologue
    const/4 v6, 0x0

    .line 1439
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1440
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "total_bytes"

    iget-object v3, p0, Lcom/android/providers/downloads/DownloadProvider$1;->val$file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1441
    const-string v2, "lastmod"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1443
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadProvider$1;->this$0:Lcom/android/providers/downloads/DownloadProvider;

    iget-object v3, p0, Lcom/android/providers/downloads/DownloadProvider$1;->val$uri:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1, v6, v6}, Lcom/android/providers/downloads/DownloadProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1445
    iget-boolean v2, p0, Lcom/android/providers/downloads/DownloadProvider$1;->val$shouldScan:Z

    if-eqz v2, :cond_0

    .line 1446
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1448
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadProvider$1;->val$file:Ljava/io/File;

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1449
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadProvider$1;->this$0:Lcom/android/providers/downloads/DownloadProvider;

    invoke-virtual {v2}, Lcom/android/providers/downloads/DownloadProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1451
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
