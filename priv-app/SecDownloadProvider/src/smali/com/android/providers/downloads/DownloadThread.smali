.class public Lcom/android/providers/downloads/DownloadThread;
.super Ljava/lang/Thread;
.source "DownloadThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/downloads/DownloadThread$SyncThread;,
        Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    }
.end annotation


# instance fields
.field private final bConcurrentWriting:Z

.field private bSBEnabledForThisSession:Z

.field private final bSetupWifiOnly:Z

.field private mCAC:Z

.field private mCACfinished:Z

.field private final mContext:Landroid/content/Context;

.field private final mId:J

.field private final mInfo:Lcom/android/providers/downloads/DownloadInfo;

.field private final mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

.field private mLastUpdateBytes:J

.field private mLastUpdateTime:J

.field private mMadeProgress:Z

.field private mNetworkType:I

.field private final mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

.field private volatile mPolicyDirty:Z

.field private mPolicyListener:Landroid/net/INetworkPolicyListener;

.field private mSkipDequeueDownload:Z

.field private mSpeed:J

.field private mSpeedSampleBytes:J

.field private mSpeedSampleStart:J

.field private final mSystemFacade:Lcom/android/providers/downloads/SystemFacade;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/providers/downloads/SystemFacade;Lcom/android/providers/downloads/DownloadNotifier;Lcom/android/providers/downloads/DownloadInfo;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "systemFacade"    # Lcom/android/providers/downloads/SystemFacade;
    .param p3, "notifier"    # Lcom/android/providers/downloads/DownloadNotifier;
    .param p4, "info"    # Lcom/android/providers/downloads/DownloadInfo;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 368
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 158
    iput-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread;->mCAC:Z

    .line 159
    iput-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread;->mCACfinished:Z

    .line 160
    iput-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread;->mSkipDequeueDownload:Z

    .line 334
    iput-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread;->mMadeProgress:Z

    .line 339
    iput-wide v4, p0, Lcom/android/providers/downloads/DownloadThread;->mLastUpdateBytes:J

    .line 340
    iput-wide v4, p0, Lcom/android/providers/downloads/DownloadThread;->mLastUpdateTime:J

    .line 342
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/providers/downloads/DownloadThread;->mNetworkType:I

    .line 353
    iput-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    .line 357
    iput-boolean v2, p0, Lcom/android/providers/downloads/DownloadThread;->bSetupWifiOnly:Z

    .line 358
    iput-boolean v2, p0, Lcom/android/providers/downloads/DownloadThread;->bConcurrentWriting:Z

    .line 2049
    new-instance v0, Lcom/android/providers/downloads/DownloadThread$1;

    invoke-direct {v0, p0}, Lcom/android/providers/downloads/DownloadThread$1;-><init>(Lcom/android/providers/downloads/DownloadThread;)V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mPolicyListener:Landroid/net/INetworkPolicyListener;

    .line 369
    iput-object p1, p0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    .line 370
    iput-object p2, p0, Lcom/android/providers/downloads/DownloadThread;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    .line 371
    iput-object p3, p0, Lcom/android/providers/downloads/DownloadThread;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    .line 373
    iget-wide v0, p4, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    iput-wide v0, p0, Lcom/android/providers/downloads/DownloadThread;->mId:J

    .line 374
    iput-object p4, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    .line 375
    new-instance v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-direct {v0, p0, p4}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;-><init>(Lcom/android/providers/downloads/DownloadThread;Lcom/android/providers/downloads/DownloadInfo;)V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    .line 377
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_0

    .line 378
    const-string v0, "DownloadManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DownloadThread created for uri "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/providers/downloads/SystemFacade;Lcom/android/providers/downloads/DownloadNotifier;Lcom/android/providers/downloads/DownloadInfo;Z)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "systemFacade"    # Lcom/android/providers/downloads/SystemFacade;
    .param p3, "notifier"    # Lcom/android/providers/downloads/DownloadNotifier;
    .param p4, "info"    # Lcom/android/providers/downloads/DownloadInfo;
    .param p5, "bCACUse"    # Z

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 385
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 158
    iput-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread;->mCAC:Z

    .line 159
    iput-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread;->mCACfinished:Z

    .line 160
    iput-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread;->mSkipDequeueDownload:Z

    .line 334
    iput-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread;->mMadeProgress:Z

    .line 339
    iput-wide v4, p0, Lcom/android/providers/downloads/DownloadThread;->mLastUpdateBytes:J

    .line 340
    iput-wide v4, p0, Lcom/android/providers/downloads/DownloadThread;->mLastUpdateTime:J

    .line 342
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/providers/downloads/DownloadThread;->mNetworkType:I

    .line 353
    iput-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    .line 357
    iput-boolean v2, p0, Lcom/android/providers/downloads/DownloadThread;->bSetupWifiOnly:Z

    .line 358
    iput-boolean v2, p0, Lcom/android/providers/downloads/DownloadThread;->bConcurrentWriting:Z

    .line 2049
    new-instance v0, Lcom/android/providers/downloads/DownloadThread$1;

    invoke-direct {v0, p0}, Lcom/android/providers/downloads/DownloadThread$1;-><init>(Lcom/android/providers/downloads/DownloadThread;)V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mPolicyListener:Landroid/net/INetworkPolicyListener;

    .line 386
    iput-object p1, p0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    .line 387
    iput-object p2, p0, Lcom/android/providers/downloads/DownloadThread;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    .line 388
    iput-object p3, p0, Lcom/android/providers/downloads/DownloadThread;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    .line 390
    iget-wide v0, p4, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    iput-wide v0, p0, Lcom/android/providers/downloads/DownloadThread;->mId:J

    .line 391
    iput-object p4, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    .line 392
    new-instance v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-direct {v0, p0, p4}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;-><init>(Lcom/android/providers/downloads/DownloadThread;Lcom/android/providers/downloads/DownloadInfo;)V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    .line 394
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_0

    .line 395
    const-string v0, "DownloadManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DownloadThread created for uri "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", bCACUse="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :cond_0
    return-void
.end method

.method private CACAvaiable()Z
    .locals 5

    .prologue
    .line 407
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v2, v2, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/android/providers/downloads/DownloadThread;->isHttpsUrl(Ljava/lang/String;)Z

    move-result v1

    .line 408
    .local v1, "isHttpsUrl":Z
    const/4 v0, 0x0

    .line 409
    .local v0, "isCACPolicyEnabled":Z
    if-eqz v1, :cond_0

    .line 410
    sget-boolean v2, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v2, :cond_0

    .line 411
    const-string v2, "DownloadManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CACAvaiable() - URL:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    :cond_0
    if-eqz v1, :cond_1

    .line 415
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/providers/downloads/DownloadCAC;->isSmartCardAuthenticationAvailable(Landroid/content/Context;)Z

    move-result v0

    .line 416
    if-eqz v0, :cond_1

    .line 417
    sget-boolean v2, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v2, :cond_1

    .line 418
    const-string v2, "DownloadManager"

    const-string v3, "CACAvaiable() - CACPolicyEnabled"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    :cond_1
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private declared-synchronized InstallDrmMessage(Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;)Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    .locals 27
    .param p1, "state"    # Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    .prologue
    .line 2097
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 2098
    .local v17, "mimeType":Ljava/lang/String;
    const/4 v8, 0x0

    .line 2099
    .local v8, "destFilenameTemp":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v23, v0

    if-eqz v23, :cond_0

    .line 2100
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/android/providers/downloads/DownloadThread;->chooseUniqueFilenameDCF(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2102
    new-instance v19, Ljava/io/File;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2103
    .local v19, "srcFile":Ljava/io/File;
    move-object v7, v8

    .line 2104
    .local v7, "destFileName":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "application/vnd.oma.drm.content"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 2105
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ".dcf"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2109
    :goto_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v7, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v23

    if-eqz v23, :cond_0

    .line 2111
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2112
    .local v6, "destFile":Ljava/io/File;
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 2113
    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    .line 2117
    .end local v6    # "destFile":Ljava/io/File;
    .end local v7    # "destFileName":Ljava/lang/String;
    .end local v19    # "srcFile":Ljava/io/File;
    :cond_0
    new-instance v15, Landroid/drm/DrmManagerClient;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-direct {v15, v0}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    .line 2120
    .local v15, "mDrmManager":Landroid/drm/DrmManagerClient;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "application/vnd.oma.drm.content"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 2121
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/drm/DrmManagerClient;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    .line 2122
    const-string v23, "DownloadManager"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "SecHandlerThread : InstallDrmContentORDcf : ActualMimeType "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2124
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v23, v0

    if-eqz v23, :cond_1

    .line 2125
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 2126
    .local v22, "values":Landroid/content/ContentValues;
    new-instance v23, Ljava/io/File;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-direct/range {v23 .. v24}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    .line 2128
    .local v16, "mTitle":Ljava/lang/String;
    const-string v23, "title"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2129
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/android/providers/downloads/DownloadInfo;->getAllDownloadsUri()Landroid/net/Uri;

    move-result-object v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    move-object/from16 v3, v25

    move-object/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2176
    .end local v16    # "mTitle":Ljava/lang/String;
    .end local v22    # "values":Landroid/content/ContentValues;
    :cond_1
    :goto_1
    new-instance v12, Landroid/drm/DrmInfoRequest;

    const/16 v23, 0xe

    const-string v24, "application/vnd.oma.drm.content"

    move/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v12, v0, v1}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 2177
    .local v12, "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    const-string v23, "drm_path"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v12, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2178
    const-string v23, "DownloadManager"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "DownloadThread : InstallDrmMessage TYPE_GET_DRMFILE_INFO  Drm Info Request created with mimetype "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v12}, Landroid/drm/DrmInfoRequest;->getMimeType()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2179
    invoke-virtual {v15, v12}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v10

    .line 2180
    .local v10, "drmInfo2":Landroid/drm/DrmInfo;
    const-string v23, "status"

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v21

    .line 2181
    .local v21, "status_req2":Ljava/lang/String;
    const-string v23, "DownloadManager"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "DownloadThread : InstallDrmMessage  processRequest status is "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2182
    const-string v23, "success"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_8

    .line 2183
    const-string v23, "DownloadManager"

    const-string v24, "DownloadThread : InstallDrmMessage : TYPE_GET_DRMFILE_INFO acquireDrmInfo Success"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2189
    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v5, v0, [I

    .line 2190
    .local v5, "DrmFileInfo":[I
    const/16 v23, 0x0

    new-instance v24, Ljava/lang/String;

    const-string v25, "version"

    invoke-direct/range {v24 .. v25}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    aput v24, v5, v23

    .line 2191
    const/16 v23, 0x1

    new-instance v24, Ljava/lang/String;

    const-string v25, "type"

    invoke-direct/range {v24 .. v25}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    aput v24, v5, v23

    .line 2193
    const-string v23, "DownloadManager"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "DownloadThread : InstallDrmMessage Drm Version: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const/16 v25, 0x0

    aget v25, v5, v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2194
    const-string v23, "DownloadManager"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "DownloadThread : InstallDrmMessage Drm Type : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const/16 v25, 0x1

    aget v25, v5, v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2197
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v23

    const-string v24, "CscFeature_Web_BlockSDCDDownload"

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_2

    .line 2198
    const/16 v23, 0x1

    aget v23, v5, v23

    if-eqz v23, :cond_2

    .line 2199
    const-string v23, "DownloadManager"

    const-string v24, "DownloadThread : Not Supported Drm Type: "

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2211
    const-string v23, "DownloadManager"

    const-string v24, "DownloadThread : Deleting Not Supported Drm Type file: "

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2212
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    .line 2213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    const/16 v24, 0x196

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214
    const/16 p1, 0x0

    .line 2222
    .end local v5    # "DrmFileInfo":[I
    .end local v8    # "destFilenameTemp":Ljava/lang/String;
    .end local v10    # "drmInfo2":Landroid/drm/DrmInfo;
    .end local v12    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .end local v15    # "mDrmManager":Landroid/drm/DrmManagerClient;
    .end local v17    # "mimeType":Ljava/lang/String;
    .end local v21    # "status_req2":Ljava/lang/String;
    .end local p1    # "state":Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    :cond_2
    :goto_2
    monitor-exit p0

    return-object p1

    .line 2107
    .restart local v7    # "destFileName":Ljava/lang/String;
    .restart local v8    # "destFilenameTemp":Ljava/lang/String;
    .restart local v17    # "mimeType":Ljava/lang/String;
    .restart local v19    # "srcFile":Ljava/io/File;
    .restart local p1    # "state":Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    :cond_3
    :try_start_1
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ".dm"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 2132
    .end local v7    # "destFileName":Ljava/lang/String;
    .end local v19    # "srcFile":Ljava/io/File;
    .restart local v15    # "mDrmManager":Landroid/drm/DrmManagerClient;
    :cond_4
    new-instance v11, Landroid/drm/DrmInfoRequest;

    const/16 v23, 0x7

    const-string v24, "application/vnd.oma.drm.content"

    move/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v11, v0, v1}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 2133
    .local v11, "drmInfoRequest_convert":Landroid/drm/DrmInfoRequest;
    const-string v23, "drm_path"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v11, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2134
    const-string v23, "status"

    const-string v24, "fail"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v11, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2135
    const-string v23, "DownloadManager"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "DownloadThread : InstallDrmMessage: Drm Info Request created with mimetype "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v11}, Landroid/drm/DrmInfoRequest;->getMimeType()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2136
    invoke-virtual {v15, v11}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v9

    .line 2137
    .local v9, "drmInfo":Landroid/drm/DrmInfo;
    const-string v23, "status"

    move-object/from16 v0, v23

    invoke-virtual {v9, v0}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    .line 2138
    .local v20, "status_req1":Ljava/lang/String;
    const-string v23, "DownloadManager"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "DownloadThread : InstallDrmMessage: processRequest status is "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2139
    const-string v23, "success"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 2140
    const-string v23, "DownloadManager"

    const-string v24, "DownloadThread : InstallDrmMessage: acquireDrmInfo Success"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2147
    const-string v23, "drm_path"

    move-object/from16 v0, v23

    invoke-virtual {v9, v0}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    .line 2148
    .local v18, "pathname_dest":Ljava/lang/Object;
    if-eqz v18, :cond_7

    .line 2149
    sget-boolean v23, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v23, :cond_5

    .line 2150
    const-string v23, "DownloadManager"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "DownloadThread : InstallDrmMessage: acquireDrmInfo output filepath is"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2152
    :cond_5
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2153
    .local v14, "file2":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->delete()Z

    .line 2154
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    .line 2160
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/drm/DrmManagerClient;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    .line 2161
    const-string v23, "DownloadManager"

    const-string v24, "DownloadThread : InstallDrmMessage: acquireDrmInfo_register Success"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2163
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v23, v0

    if-eqz v23, :cond_1

    .line 2164
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 2165
    .restart local v22    # "values":Landroid/content/ContentValues;
    new-instance v23, Ljava/io/File;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-direct/range {v23 .. v24}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    .line 2167
    .restart local v16    # "mTitle":Ljava/lang/String;
    const-string v23, "title"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2168
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/android/providers/downloads/DownloadInfo;->getAllDownloadsUri()Landroid/net/Uri;

    move-result-object v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    move-object/from16 v3, v25

    move-object/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 2217
    .end local v8    # "destFilenameTemp":Ljava/lang/String;
    .end local v9    # "drmInfo":Landroid/drm/DrmInfo;
    .end local v11    # "drmInfoRequest_convert":Landroid/drm/DrmInfoRequest;
    .end local v14    # "file2":Ljava/io/File;
    .end local v15    # "mDrmManager":Landroid/drm/DrmManagerClient;
    .end local v16    # "mTitle":Ljava/lang/String;
    .end local v17    # "mimeType":Ljava/lang/String;
    .end local v18    # "pathname_dest":Ljava/lang/Object;
    .end local v20    # "status_req1":Ljava/lang/String;
    .end local v22    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v13

    .line 2218
    .local v13, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v23, "DownloadManager"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "DownloadThread : InstallDrmMessage : Handle Exception"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v13}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    const/16 v24, 0x1eb

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2220
    const/16 p1, 0x0

    goto/16 :goto_2

    .line 2142
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v8    # "destFilenameTemp":Ljava/lang/String;
    .restart local v9    # "drmInfo":Landroid/drm/DrmInfo;
    .restart local v11    # "drmInfoRequest_convert":Landroid/drm/DrmInfoRequest;
    .restart local v15    # "mDrmManager":Landroid/drm/DrmManagerClient;
    .restart local v17    # "mimeType":Ljava/lang/String;
    .restart local v20    # "status_req1":Ljava/lang/String;
    :cond_6
    :try_start_3
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    .line 2143
    const-string v23, "DownloadManager"

    const-string v24, "DownloadThread : InstallDrmMessage: acquireDrmInfo Fail"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    const/16 v24, 0x1eb

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    .line 2145
    const/16 p1, 0x0

    goto/16 :goto_2

    .line 2171
    .restart local v18    # "pathname_dest":Ljava/lang/Object;
    :cond_7
    const-string v23, "DownloadManager"

    const-string v24, "DownloadThread : InstallDrmMessage: acquireDrmInfo_convert no file path"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 2097
    .end local v8    # "destFilenameTemp":Ljava/lang/String;
    .end local v9    # "drmInfo":Landroid/drm/DrmInfo;
    .end local v11    # "drmInfoRequest_convert":Landroid/drm/DrmInfoRequest;
    .end local v15    # "mDrmManager":Landroid/drm/DrmManagerClient;
    .end local v17    # "mimeType":Ljava/lang/String;
    .end local v18    # "pathname_dest":Ljava/lang/Object;
    .end local v20    # "status_req1":Ljava/lang/String;
    :catchall_0
    move-exception v23

    monitor-exit p0

    throw v23

    .line 2185
    .restart local v8    # "destFilenameTemp":Ljava/lang/String;
    .restart local v10    # "drmInfo2":Landroid/drm/DrmInfo;
    .restart local v12    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .restart local v15    # "mDrmManager":Landroid/drm/DrmManagerClient;
    .restart local v17    # "mimeType":Ljava/lang/String;
    .restart local v21    # "status_req2":Ljava/lang/String;
    :cond_8
    :try_start_4
    const-string v23, "DownloadManager"

    const-string v24, "DownloadThread : InstallDrmMessage : TYPE_GET_DRMFILE_INFO acquireDrmInfo Fail"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    const/16 v24, 0x1eb

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2187
    const/16 p1, 0x0

    goto/16 :goto_2
.end method

.method private ValidateOMAHeaderData(Ljava/lang/String;)I
    .locals 6
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    const/16 v1, 0x26d

    .line 1947
    const/4 v0, 0x0

    .line 1949
    .local v0, "StatusResult":I
    if-nez p1, :cond_1

    .line 1951
    const/16 v1, 0x262

    .line 1980
    :cond_0
    :goto_0
    return v1

    .line 1954
    :cond_1
    const/4 v2, 0x1

    invoke-static {p1}, Lcom/android/providers/downloads/Helpers;->isDrmMime(Ljava/lang/String;)Z

    move-result v3

    if-ne v2, v3, :cond_3

    .line 1955
    const/4 v0, 0x1

    :cond_2
    move v1, v0

    .line 1980
    goto :goto_0

    .line 1958
    :cond_3
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v2, v2, Lcom/android/providers/downloads/DownloadInfo;->mPrimaryMimeType:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v2, v2, Lcom/android/providers/downloads/DownloadInfo;->mPrimaryMimeType:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1959
    const/4 v0, 0x1

    .line 1972
    :goto_1
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v2, v2, Lcom/android/providers/downloads/DownloadInfo;->mTotalBytes:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v2, v2, Lcom/android/providers/downloads/DownloadInfo;->mContentSize:J

    iget-object v4, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v4, v4, Lcom/android/providers/downloads/DownloadInfo;->mTotalBytes:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 1973
    sget-boolean v2, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v2, :cond_0

    .line 1974
    const-string v2, "DownloadManager"

    const-string v3, "DownloadThread : ValidateHeaderData : MisMatch Download Content Size "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1961
    :cond_4
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v2, v2, Lcom/android/providers/downloads/DownloadInfo;->mSecMimeType1:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v2, v2, Lcom/android/providers/downloads/DownloadInfo;->mSecMimeType1:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1962
    const/4 v0, 0x1

    goto :goto_1

    .line 1963
    :cond_5
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v2, v2, Lcom/android/providers/downloads/DownloadInfo;->mSecMimeType2:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v2, v2, Lcom/android/providers/downloads/DownloadInfo;->mSecMimeType2:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1964
    const/4 v0, 0x1

    goto :goto_1

    .line 1966
    :cond_6
    sget-boolean v2, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v2, :cond_0

    .line 1967
    const-string v2, "DownloadManager"

    const-string v3, "DownloadThread : ValidateHeaderData : MisMatch Download Mime Type "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/providers/downloads/DownloadThread;)Lcom/android/providers/downloads/DownloadInfo;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadThread;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/providers/downloads/DownloadThread;)Lcom/android/providers/downloads/SystemFacade;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadThread;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/providers/downloads/DownloadThread;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadThread;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/providers/downloads/DownloadThread;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadThread;
    .param p1, "x1"    # Z

    .prologue
    .line 128
    iput-boolean p1, p0, Lcom/android/providers/downloads/DownloadThread;->mPolicyDirty:Z

    return p1
.end method

.method private addRequestHeaders(Ljava/net/HttpURLConnection;Z)V
    .locals 8
    .param p1, "conn"    # Ljava/net/HttpURLConnection;
    .param p2, "resuming"    # Z

    .prologue
    .line 2003
    iget-object v3, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    invoke-virtual {v3}, Lcom/android/providers/downloads/DownloadInfo;->getHeaders()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2004
    .local v0, "header":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2008
    .end local v0    # "header":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    const-string v3, "User-Agent"

    invoke-virtual {p1, v3}, Ljava/net/HttpURLConnection;->getRequestProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 2009
    const-string v3, "User-Agent"

    iget-object v4, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    invoke-virtual {v4}, Lcom/android/providers/downloads/DownloadInfo;->getUserAgent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2014
    :cond_1
    const-string v3, "Accept-Encoding"

    const-string v4, "identity"

    invoke-virtual {p1, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2016
    if-eqz p2, :cond_5

    .line 2017
    iget-object v3, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v3, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mETag:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 2018
    const-string v3, "If-Match"

    iget-object v4, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mETag:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2022
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bytes="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2023
    .local v2, "range":Ljava/lang/String;
    iget-boolean v3, p0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v3, :cond_4

    .line 2024
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bytes="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2025
    iget-object v3, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v4, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    iget-object v3, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    .line 2026
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2028
    :cond_3
    sget-boolean v3, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v3, :cond_4

    .line 2029
    const-string v3, "DownloadManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "range for the request: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2032
    :cond_4
    const-string v3, "Range"

    invoke-virtual {p1, v3, v2}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2035
    .end local v2    # "range":Ljava/lang/String;
    :cond_5
    return-void
.end method

.method private checkConnectivity()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/downloads/StopRequestException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1772
    iput-boolean v6, p0, Lcom/android/providers/downloads/DownloadThread;->mPolicyDirty:Z

    .line 1774
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v3, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v4, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    invoke-virtual {v2, v4, v5}, Lcom/android/providers/downloads/DownloadInfo;->checkCanUseNetwork(J)Lcom/android/providers/downloads/DownloadInfo$NetworkState;

    move-result-object v0

    .line 1775
    .local v0, "networkUsable":Lcom/android/providers/downloads/DownloadInfo$NetworkState;
    sget-object v2, Lcom/android/providers/downloads/DownloadInfo$NetworkState;->OK:Lcom/android/providers/downloads/DownloadInfo$NetworkState;

    if-eq v0, v2, :cond_2

    .line 1776
    const/16 v1, 0xc3

    .line 1777
    .local v1, "status":I
    sget-object v2, Lcom/android/providers/downloads/DownloadInfo$NetworkState;->UNUSABLE_DUE_TO_SIZE:Lcom/android/providers/downloads/DownloadInfo$NetworkState;

    if-ne v0, v2, :cond_1

    .line 1778
    const/16 v1, 0xc4

    .line 1779
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/providers/downloads/DownloadInfo;->notifyPauseDueToSize(Z)V

    .line 1784
    :cond_0
    :goto_0
    new-instance v2, Lcom/android/providers/downloads/StopRequestException;

    invoke-virtual {v0}, Lcom/android/providers/downloads/DownloadInfo$NetworkState;->name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v2

    .line 1780
    :cond_1
    sget-object v2, Lcom/android/providers/downloads/DownloadInfo$NetworkState;->RECOMMENDED_UNUSABLE_DUE_TO_SIZE:Lcom/android/providers/downloads/DownloadInfo$NetworkState;

    if-ne v0, v2, :cond_0

    .line 1781
    const/16 v1, 0xc4

    .line 1782
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    invoke-virtual {v2, v6}, Lcom/android/providers/downloads/DownloadInfo;->notifyPauseDueToSize(Z)V

    goto :goto_0

    .line 1786
    .end local v1    # "status":I
    :cond_2
    return-void
.end method

.method private checkOMADD()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/downloads/StopRequestException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 1908
    iget-object v3, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v3, v3, Lcom/android/providers/downloads/DownloadInfo;->mMethod:I

    if-ne v3, v8, :cond_4

    .line 1910
    iget-object v3, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v3, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 1911
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1912
    .local v2, "values":Landroid/content/ContentValues;
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1914
    .local v1, "mTitle":Ljava/lang/String;
    iget-boolean v3, p0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v3, :cond_0

    .line 1915
    sget-boolean v3, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v3, :cond_0

    .line 1916
    const-string v3, "DownloadManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "new File "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1920
    :cond_0
    const-string v3, "title"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1921
    const-string v3, "total_bytes"

    iget-object v4, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v4, v4, Lcom/android/providers/downloads/DownloadInfo;->mTotalBytes:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1923
    iget-boolean v3, p0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v3, :cond_1

    .line 1924
    sget-boolean v3, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v3, :cond_1

    .line 1925
    const-string v3, "DownloadManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "now total bytes is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1929
    :cond_1
    iget-object v3, p0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    invoke-virtual {v4}, Lcom/android/providers/downloads/DownloadInfo;->getAllDownloadsUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v2, v9, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1932
    .end local v1    # "mTitle":Ljava/lang/String;
    .end local v2    # "values":Landroid/content/ContentValues;
    :cond_2
    iget-object v3, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v3, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/android/providers/downloads/DownloadThread;->ValidateOMAHeaderData(Ljava/lang/String;)I

    move-result v0

    .line 1934
    .local v0, "VaildateResult":I
    if-eq v8, v0, :cond_4

    .line 1935
    sget-boolean v3, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v3, :cond_3

    .line 1936
    const-string v3, "DownloadManager"

    const-string v4, "SecDownloadThread : run : Mismatch Download Content"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1939
    :cond_3
    new-instance v3, Lcom/android/providers/downloads/StopRequestException;

    invoke-static {v0}, Lcom/android/providers/downloads/Helpers;->getStatusText(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v3

    .line 1943
    .end local v0    # "VaildateResult":I
    :cond_4
    return-void
.end method

.method private checkPausedOrCanceled()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/downloads/StopRequestException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x1ea

    .line 1793
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    monitor-enter v1

    .line 1794
    :try_start_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mControl:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 1795
    new-instance v0, Lcom/android/providers/downloads/StopRequestException;

    const/16 v2, 0xc1

    const-string v3, "download paused by owner"

    invoke-direct {v0, v2, v3}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1801
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1798
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-boolean v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mDeleted:Z

    if-eqz v0, :cond_2

    .line 1799
    :cond_1
    new-instance v0, Lcom/android/providers/downloads/StopRequestException;

    const/16 v2, 0x1ea

    const-string v3, "download canceled"

    invoke-direct {v0, v2, v3}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1801
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1804
    iget-boolean v0, p0, Lcom/android/providers/downloads/DownloadThread;->mPolicyDirty:Z

    if-eqz v0, :cond_3

    .line 1805
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadThread;->checkConnectivity()V

    .line 1807
    :cond_3
    return-void
.end method

.method private static chooseUniqueFilenameDCF(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "filepath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/downloads/StopRequestException;
        }
    .end annotation

    .prologue
    .line 2226
    const/4 v2, 0x0

    .line 2227
    .local v2, "filename":Ljava/lang/String;
    const/4 v3, 0x0

    .line 2229
    .local v3, "fullFilename":Ljava/lang/String;
    const/16 v7, 0x2e

    invoke-virtual {p0, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 2230
    .local v0, "dotIndex":I
    const-string v1, ".dcf"

    .line 2232
    .local v1, "extension":Ljava/lang/String;
    const/4 v7, 0x0

    invoke-virtual {p0, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 2233
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2235
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    move-object v7, v2

    .line 2263
    :goto_0
    return-object v7

    .line 2239
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2254
    const/4 v6, 0x1

    .line 2255
    .local v6, "sequence":I
    const/4 v5, 0x1

    .local v5, "magnitude":I
    :goto_1
    const v7, 0x3b9aca00

    if-ge v5, v7, :cond_4

    .line 2256
    const/4 v4, 0x0

    .local v4, "iteration":I
    :goto_2
    const/16 v7, 0x9

    if-ge v4, v7, :cond_3

    .line 2258
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2259
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 2261
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v7, v3

    .line 2263
    goto :goto_0

    .line 2265
    :cond_1
    sget-boolean v7, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v7, :cond_2

    .line 2266
    const-string v7, "DownloadManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file with sequence number "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " exists"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2268
    :cond_2
    sget-object v7, Lcom/android/providers/downloads/Helpers;->sRandom:Ljava/util/Random;

    invoke-virtual {v7, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    add-int/2addr v6, v7

    .line 2256
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2255
    :cond_3
    mul-int/lit8 v5, v5, 0xa

    goto :goto_1

    .line 2271
    .end local v4    # "iteration":I
    :cond_4
    new-instance v7, Lcom/android/providers/downloads/StopRequestException;

    const/16 v8, 0x1ec

    const-string v9, "failed to generate an unused filename on internal download storage"

    invoke-direct {v7, v8, v9}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v7
.end method

.method private convertFromBytes([BII)J
    .locals 10
    .param p1, "b"    # [B
    .param p2, "s"    # I
    .param p3, "e"    # I

    .prologue
    const-wide/16 v6, 0x0

    .line 1171
    if-eqz p1, :cond_0

    array-length v1, p1

    if-lt v1, p3, :cond_0

    sub-int v1, p3, p2

    const/16 v8, 0x8

    if-eq v1, v8, :cond_2

    :cond_0
    move-wide v2, v6

    .line 1181
    :cond_1
    return-wide v2

    .line 1174
    :cond_2
    const-wide/16 v2, 0x0

    .line 1175
    .local v2, "ret":J
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_1

    .line 1176
    aget-byte v1, p1, v0

    int-to-long v4, v1

    .line 1177
    .local v4, "tmp":J
    cmp-long v1, v4, v6

    if-gez v1, :cond_3

    const-wide/16 v8, 0x100

    add-long/2addr v4, v8

    .line 1178
    :cond_3
    sub-int v1, v0, p2

    mul-int/lit8 v1, v1, 0x8

    shl-long/2addr v4, v1

    .line 1179
    add-long/2addr v2, v4

    .line 1175
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private executeDownload()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/downloads/StopRequestException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const-wide/16 v12, 0x0

    .line 662
    iput-boolean v4, p0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    .line 663
    iget-boolean v0, p0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v0, :cond_0

    .line 664
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_0

    .line 665
    const-string v0, "DownloadManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start executeDownload "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v2, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", 0-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v2, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v2, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v2, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v2, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    const-string v0, "DownloadManager"

    const-string v1, "SB Params: SB_CMD_FILENAME(-100), SB_RET_FILENAME(-101), SB_CMD_RANGE(-102), SB_RET_RANGE(-103), SB_INTERVAL_GETRANGE(1500)"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    :cond_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v10, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    .line 676
    .local v10, "secStart":J
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    .line 679
    .local v8, "secEnd":J
    cmp-long v0, v8, v10

    if-lez v0, :cond_1

    cmp-long v0, v8, v12

    if-lez v0, :cond_1

    cmp-long v0, v10, v12

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    cmp-long v0, v0, v8

    if-ltz v0, :cond_3

    .line 680
    :cond_1
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_2

    .line 681
    const-string v0, "DownloadManager"

    const-string v1, "just download one chunk for remaining bytes"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    :cond_2
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    .line 684
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    .line 685
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mChunkSize:J

    .line 686
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    .line 687
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadThread;->executeDownload_OneChunk()V

    .line 732
    :goto_0
    return-void

    .line 692
    :cond_3
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    cmp-long v0, v0, v10

    if-ltz v0, :cond_5

    .line 693
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_4

    .line 694
    const-string v0, "DownloadManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "update current bytes from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v2, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 696
    :cond_4
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v8, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    .line 697
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    .line 698
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    .line 699
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mChunkSize:J

    .line 700
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    .line 701
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadThread;->executeDownload_OneChunk()V

    goto :goto_0

    .line 708
    :cond_5
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_6

    .line 709
    const-string v0, "DownloadManager"

    const-string v1, "need to download twice, now 1st time "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    :cond_6
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v10, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    .line 712
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v8, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    .line 713
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v2, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    sub-long v2, v10, v2

    iput-wide v2, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mChunkSize:J

    .line 714
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    .line 715
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-boolean v4, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->bFirstChunkOfMultiChunk:Z

    .line 716
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v2, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v4, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/android/providers/downloads/DownloadThread;->getCombinedLength(JJJ)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    .line 717
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadThread;->executeDownload_OneChunk()V

    .line 718
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_7

    .line 719
    const-string v0, "DownloadManager"

    const-string v1, "need to download twice, now 2nd time "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    :cond_7
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v8, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    .line 722
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    .line 723
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    .line 724
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mChunkSize:J

    .line 725
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    .line 726
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->bFirstChunkOfMultiChunk:Z

    .line 727
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadThread;->executeDownload_OneChunk()V

    goto/16 :goto_0
.end method

.method private executeDownload_OneChunk()V
    .locals 32
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/downloads/StopRequestException;
        }
    .end annotation

    .prologue
    .line 743
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_e

    const/16 v24, 0x1

    .line 746
    .local v24, "resuming":Z
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v4, :cond_3

    .line 747
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_0

    .line 748
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "start executeDownload_OneChunk(before setting) "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", 0-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", continue is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    :cond_0
    if-nez v24, :cond_1

    .line 753
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const-wide/16 v6, 0x0

    iput-wide v6, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    .line 754
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const-wide/16 v6, 0x0

    iput-wide v6, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    .line 755
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const-wide/16 v6, 0x0

    iput-wide v6, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    .line 756
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const-wide/16 v6, 0x0

    iput-wide v6, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    .line 758
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_2

    .line 759
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    iput-wide v6, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    .line 761
    :cond_2
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_3

    .line 762
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "start executeDownload_OneChunk(after setting) "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", 0-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", continue is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    :cond_3
    :try_start_0
    new-instance v29, Ljava/net/URL;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mUri:Ljava/lang/String;

    move-object/from16 v0, v29

    invoke-direct {v0, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 775
    .local v29, "url":Ljava/net/URL;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v4, :cond_4

    .line 776
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_4

    .line 777
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "start to download for url "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v29 .. v29}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 785
    :cond_4
    const/16 v21, 0x0

    .line 788
    .local v21, "redirectionCount":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadInfo;->mPackage:Ljava/lang/String;

    if-eqz v4, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadInfo;->mPackage:Ljava/lang/String;

    const-string v5, "com.sec.android.app.sbrowser"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->getMDMProxyServer()Ljava/net/Proxy;

    move-result-object v19

    .local v19, "mdmProxy":Ljava/net/Proxy;
    :goto_1
    move/from16 v22, v21

    .end local v21    # "redirectionCount":I
    .local v22, "redirectionCount":I
    move-object/from16 v30, v29

    .line 791
    .end local v29    # "url":Ljava/net/URL;
    .local v30, "url":Ljava/net/URL;
    :goto_2
    add-int/lit8 v21, v22, 0x1

    .end local v22    # "redirectionCount":I
    .restart local v21    # "redirectionCount":I
    const/4 v4, 0x5

    move/from16 v0, v22

    if-ge v0, v4, :cond_2b

    .line 794
    const/4 v14, 0x0

    .line 796
    .local v14, "conn":Ljava/net/HttpURLConnection;
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->checkConnectivity()V

    .line 798
    if-eqz v19, :cond_10

    .line 799
    move-object/from16 v0, v30

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v14, v0

    .line 807
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/providers/downloads/DownloadThread;->mCAC:Z

    if-eqz v4, :cond_6

    .line 808
    move-object v0, v14

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    move-object/from16 v25, v0

    .line 809
    .local v25, "sconn":Ljavax/net/ssl/HttpsURLConnection;
    new-instance v11, Lcom/android/providers/downloads/DownloadCACKeyManager;

    invoke-direct {v11}, Lcom/android/providers/downloads/DownloadCACKeyManager;-><init>()V

    .line 810
    .local v11, "cacKeyMgr":Lcom/android/providers/downloads/DownloadCACKeyManager;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    move-result-object v4

    invoke-virtual/range {v30 .. v30}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v30 .. v30}, Ljava/net/URL;->getPort()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->getClientCertificateAlias(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v10

    .line 811
    .local v10, "alias":Ljava/lang/String;
    if-eqz v10, :cond_5

    .line 812
    invoke-virtual {v11, v10}, Lcom/android/providers/downloads/DownloadCACKeyManager;->setKeyStoreClientAlias(Ljava/lang/String;)V

    .line 814
    :cond_5
    const/4 v4, 0x1

    new-array v0, v4, [Ljavax/net/ssl/KeyManager;

    move-object/from16 v17, v0

    const/4 v4, 0x0

    aput-object v11, v17, v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 816
    .local v17, "km":[Ljavax/net/ssl/KeyManager;
    :try_start_2
    const-string v4, "TLS"

    invoke-static {v4}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v26

    .line 817
    .local v26, "sslContext":Ljavax/net/ssl/SSLContext;
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v4, v5}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 818
    invoke-virtual/range {v26 .. v26}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v27

    .line 819
    .local v27, "sslSocketFactory":Ljavax/net/ssl/SSLSocketFactory;
    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 827
    .end local v10    # "alias":Ljava/lang/String;
    .end local v11    # "cacKeyMgr":Lcom/android/providers/downloads/DownloadCACKeyManager;
    .end local v17    # "km":[Ljavax/net/ssl/KeyManager;
    .end local v25    # "sconn":Ljavax/net/ssl/HttpsURLConnection;
    .end local v26    # "sslContext":Ljavax/net/ssl/SSLContext;
    .end local v27    # "sslSocketFactory":Ljavax/net/ssl/SSLSocketFactory;
    :cond_6
    :goto_4
    const/4 v4, 0x0

    :try_start_3
    invoke-virtual {v14, v4}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 828
    const/16 v4, 0x4e20

    invoke-virtual {v14, v4}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 829
    const/16 v4, 0x4e20

    invoke-virtual {v14, v4}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 831
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v14, v1}, Lcom/android/providers/downloads/DownloadThread;->addRequestHeaders(Ljava/net/HttpURLConnection;Z)V

    .line 834
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v4, :cond_b

    .line 835
    const-string v4, "DownloadManager"

    const-string v5, "SB enabled "

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "-255:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v5, v5, Lcom/android/providers/downloads/DownloadInfo;->mUid:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 842
    .local v28, "taguid":Ljava/lang/String;
    const-string v4, "NETWORKBOOSTER_LOCAL_FILE_TAG_UID"

    move-object/from16 v0, v28

    invoke-virtual {v14, v4, v0}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_7

    .line 844
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "(HttpLog) setup tag and uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    :cond_7
    const/4 v13, 0x0

    .line 852
    .local v13, "cannotUseWiFi":Z
    const/4 v12, 0x0

    .line 853
    .local v12, "cannotUseMobile":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v4, v4, Lcom/android/providers/downloads/DownloadInfo;->mAllowedNetworkTypes:I

    if-eqz v4, :cond_8

    .line 854
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    invoke-virtual {v4, v5, v6, v7}, Lcom/android/providers/downloads/DownloadInfo;->checkIsNetworkTypeAllowed_NB(IJ)Lcom/android/providers/downloads/DownloadInfo$NetworkState;

    move-result-object v4

    sget-object v5, Lcom/android/providers/downloads/DownloadInfo$NetworkState;->TYPE_DISALLOWED_BY_REQUESTOR:Lcom/android/providers/downloads/DownloadInfo$NetworkState;

    if-ne v4, v5, :cond_11

    const/4 v13, 0x1

    .line 855
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    invoke-virtual {v4, v5, v6, v7}, Lcom/android/providers/downloads/DownloadInfo;->checkIsNetworkTypeAllowed_NB(IJ)Lcom/android/providers/downloads/DownloadInfo$NetworkState;

    move-result-object v4

    sget-object v5, Lcom/android/providers/downloads/DownloadInfo$NetworkState;->TYPE_DISALLOWED_BY_REQUESTOR:Lcom/android/providers/downloads/DownloadInfo$NetworkState;

    if-ne v4, v5, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    const/4 v5, 0x5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    invoke-virtual {v4, v5, v6, v7}, Lcom/android/providers/downloads/DownloadInfo;->checkIsNetworkTypeAllowed_NB(IJ)Lcom/android/providers/downloads/DownloadInfo$NetworkState;

    move-result-object v4

    sget-object v5, Lcom/android/providers/downloads/DownloadInfo$NetworkState;->TYPE_DISALLOWED_BY_REQUESTOR:Lcom/android/providers/downloads/DownloadInfo$NetworkState;

    if-ne v4, v5, :cond_12

    const/4 v12, 0x1

    .line 858
    :cond_8
    :goto_6
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_9

    .line 859
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cannotUseWiFi "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", cannotUseMobile "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    :cond_9
    if-nez v13, :cond_a

    if-eqz v12, :cond_b

    .line 862
    :cond_a
    const-string v4, "NETWORKBOOSTER_CANNOT_USE_WIFIORMOBILE"

    const-string v5, "TRUE"

    invoke-virtual {v14, v4, v5}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    .end local v12    # "cannotUseMobile":Z
    .end local v13    # "cannotUseWiFi":Z
    .end local v28    # "taguid":Ljava/lang/String;
    :cond_b
    invoke-virtual {v14}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v23

    .line 870
    .local v23, "responseCode":I
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v4, :cond_c

    .line 871
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_c

    .line 872
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get response code "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    :cond_c
    sparse-switch v23, :sswitch_data_0

    .line 1015
    invoke-virtual {v14}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v4

    move/from16 v0, v23

    invoke-static {v0, v4}, Lcom/android/providers/downloads/StopRequestException;->throwUnhandledHttpError(ILjava/lang/String;)Lcom/android/providers/downloads/StopRequestException;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1035
    if-eqz v14, :cond_d

    invoke-virtual {v14}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_d
    move/from16 v22, v21

    .line 1037
    .end local v21    # "redirectionCount":I
    .restart local v22    # "redirectionCount":I
    goto/16 :goto_2

    .line 743
    .end local v14    # "conn":Ljava/net/HttpURLConnection;
    .end local v19    # "mdmProxy":Ljava/net/Proxy;
    .end local v22    # "redirectionCount":I
    .end local v23    # "responseCode":I
    .end local v24    # "resuming":Z
    .end local v30    # "url":Ljava/net/URL;
    :cond_e
    const/16 v24, 0x0

    goto/16 :goto_0

    .line 781
    .restart local v24    # "resuming":Z
    :catch_0
    move-exception v15

    .line 782
    .local v15, "e":Ljava/net/MalformedURLException;
    new-instance v4, Lcom/android/providers/downloads/StopRequestException;

    const/16 v5, 0x190

    invoke-direct {v4, v5, v15}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/Throwable;)V

    throw v4

    .line 788
    .end local v15    # "e":Ljava/net/MalformedURLException;
    .restart local v21    # "redirectionCount":I
    .restart local v29    # "url":Ljava/net/URL;
    :cond_f
    const/16 v19, 0x0

    goto/16 :goto_1

    .line 803
    .end local v29    # "url":Ljava/net/URL;
    .restart local v14    # "conn":Ljava/net/HttpURLConnection;
    .restart local v19    # "mdmProxy":Ljava/net/Proxy;
    .restart local v30    # "url":Ljava/net/URL;
    :cond_10
    :try_start_4
    invoke-virtual/range {v30 .. v30}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v14, v0

    goto/16 :goto_3

    .line 854
    .restart local v12    # "cannotUseMobile":Z
    .restart local v13    # "cannotUseWiFi":Z
    .restart local v28    # "taguid":Ljava/lang/String;
    :cond_11
    const/4 v13, 0x0

    goto/16 :goto_5

    .line 855
    :cond_12
    const/4 v12, 0x0

    goto/16 :goto_6

    .line 878
    .end local v12    # "cannotUseMobile":Z
    .end local v13    # "cannotUseWiFi":Z
    .end local v28    # "taguid":Ljava/lang/String;
    .restart local v23    # "responseCode":I
    :sswitch_0
    if-eqz v24, :cond_14

    .line 879
    new-instance v4, Lcom/android/providers/downloads/StopRequestException;

    const/16 v5, 0x1e9

    const-string v6, "Expected partial, but received OK"

    invoke-direct {v4, v5, v6}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v4
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1019
    .end local v23    # "responseCode":I
    :catch_1
    move-exception v15

    move-object/from16 v29, v30

    .line 1020
    .end local v30    # "url":Ljava/net/URL;
    .local v15, "e":Ljava/io/IOException;
    .restart local v29    # "url":Ljava/net/URL;
    :goto_7
    :try_start_5
    instance-of v4, v15, Ljava/net/ProtocolException;

    if-eqz v4, :cond_29

    invoke-virtual {v15}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Unexpected status line"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 1022
    new-instance v4, Lcom/android/providers/downloads/StopRequestException;

    const/16 v5, 0x1ee

    invoke-direct {v4, v5, v15}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/Throwable;)V

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1035
    .end local v15    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_8
    if-eqz v14, :cond_13

    invoke-virtual {v14}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_13
    throw v4

    .line 882
    .end local v29    # "url":Ljava/net/URL;
    .restart local v23    # "responseCode":I
    .restart local v30    # "url":Ljava/net/URL;
    :cond_14
    :try_start_6
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/providers/downloads/DownloadThread;->parseOkHeaders(Ljava/net/HttpURLConnection;)V

    .line 885
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Web_SupportDownloadSaveAs"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_20

    .line 887
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    if-eqz v4, :cond_15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    const-string v5, "application/vnd.oma.dd+xml"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_20

    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadInfo;->mPackage:Ljava/lang/String;

    if-eqz v4, :cond_20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadInfo;->mPackage:Ljava/lang/String;

    const-string v5, "com.sec.android.app.sbrowser"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_16

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadInfo;->mPackage:Ljava/lang/String;

    const-string v5, "com.android.browser"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_20

    .line 891
    :cond_16
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_17

    .line 892
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mContext = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mInfo = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 894
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mInfoDelta.mFilename = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 896
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mInfoDelta .mMimeType = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    :cond_17
    const/4 v4, 0x1

    new-array v9, v4, [I

    const/4 v4, 0x0

    const/16 v5, 0xc8

    aput v5, v9, v4

    .line 899
    .local v9, "paramStatus":[I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v8, v8, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-static/range {v4 .. v9}, Lcom/android/providers/downloads/saveas/DownloadSaveasManager;->getSaveAsFilename(Landroid/content/Context;Lcom/android/providers/downloads/DownloadInfo;Ljava/lang/String;Ljava/io/FileOutputStream;Ljava/lang/String;[I)Lcom/android/providers/downloads/saveas/DownloadFileInfo;

    move-result-object v16

    .line 901
    .local v16, "info":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    const/4 v4, 0x0

    aget v20, v9, v4

    .line 902
    .local v20, "pStatus":I
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DownloadThread : [saveas] update download end "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pStatus "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "result "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Downloads.Impl.STATUS_SUCCESS "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0xc8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 904
    const/16 v4, 0xc8

    move/from16 v0, v20

    if-le v0, v4, :cond_1a

    .line 905
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_18

    .line 906
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DownloadService : [saveas] failed "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 908
    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move/from16 v0, v20

    iput v0, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    .line 910
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->checkPausedOrCanceled()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1035
    if-eqz v14, :cond_19

    invoke-virtual {v14}, Ljava/net/HttpURLConnection;->disconnect()V

    .end local v9    # "paramStatus":[I
    .end local v16    # "info":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .end local v20    # "pStatus":I
    :cond_19
    :goto_9
    return-void

    .line 914
    .restart local v9    # "paramStatus":[I
    .restart local v16    # "info":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .restart local v20    # "pStatus":I
    :cond_1a
    :try_start_7
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_1b

    .line 915
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DownloadThread : [saveas] info.mFileName "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 917
    :cond_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual/range {v16 .. v16}, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->getFilename()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    .line 920
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_1c

    .line 921
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "*mInfoDelta.mFilename = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 923
    :cond_1c
    new-instance v31, Landroid/content/ContentValues;

    invoke-direct/range {v31 .. v31}, Landroid/content/ContentValues;-><init>()V

    .line 924
    .local v31, "values":Landroid/content/ContentValues;
    const-string v4, "_data"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mETag:Ljava/lang/String;

    if-eqz v4, :cond_1d

    .line 926
    const-string v4, "etag"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mETag:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    :cond_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    if-eqz v4, :cond_1e

    .line 929
    const-string v4, "mimetype"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 931
    :cond_1e
    const-string v4, "total_bytes"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v6, v5, Lcom/android/providers/downloads/DownloadInfo;->mTotalBytes:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 932
    invoke-virtual/range {v16 .. v16}, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->getTitle()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1f

    .line 933
    const-string v4, "title"

    invoke-virtual/range {v16 .. v16}, Lcom/android/providers/downloads/saveas/DownloadFileInfo;->getTitle()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    :cond_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    invoke-virtual {v5}, Lcom/android/providers/downloads/DownloadInfo;->getAllDownloadsUri()Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 941
    .end local v9    # "paramStatus":[I
    .end local v16    # "info":Lcom/android/providers/downloads/saveas/DownloadFileInfo;
    .end local v20    # "pStatus":I
    .end local v31    # "values":Landroid/content/ContentValues;
    :cond_20
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->checkOMADD()V

    .line 945
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v4, :cond_21

    .line 946
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_21

    .line 947
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mimetype is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", method is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v6, v6, Lcom/android/providers/downloads/DownloadInfo;->mMethod:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 950
    :cond_21
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v4, :cond_23

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/providers/downloads/DownloadDrmHelper;->isDrmConvertNeeded(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_23

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    const-wide/32 v6, 0x200000

    cmp-long v4, v4, v6

    if-lez v4, :cond_23

    .line 952
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_22

    .line 953
    const-string v4, "DownloadManager"

    const-string v5, "(HttpLog) download with transferData_NB"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 955
    :cond_22
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/providers/downloads/DownloadThread;->transferData_NB(Ljava/net/HttpURLConnection;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1035
    :goto_a
    if-eqz v14, :cond_19

    invoke-virtual {v14}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_9

    .line 959
    :cond_23
    :try_start_8
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/providers/downloads/DownloadThread;->transferData(Ljava/net/HttpURLConnection;)V

    goto :goto_a

    .line 1035
    .end local v23    # "responseCode":I
    :catchall_1
    move-exception v4

    move-object/from16 v29, v30

    .end local v30    # "url":Ljava/net/URL;
    .restart local v29    # "url":Ljava/net/URL;
    goto/16 :goto_8

    .line 963
    .end local v29    # "url":Ljava/net/URL;
    .restart local v23    # "responseCode":I
    .restart local v30    # "url":Ljava/net/URL;
    :sswitch_1
    if-nez v24, :cond_24

    .line 964
    new-instance v4, Lcom/android/providers/downloads/StopRequestException;

    const/16 v5, 0x1e9

    const-string v6, "Expected OK, but received partial"

    invoke-direct {v4, v5, v6}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v4

    .line 968
    :cond_24
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v4, :cond_25

    .line 969
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_25

    .line 970
    const-string v4, "DownloadManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mimetype is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", method is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v6, v6, Lcom/android/providers/downloads/DownloadInfo;->mMethod:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 973
    :cond_25
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v4, :cond_27

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/providers/downloads/DownloadDrmHelper;->isDrmConvertNeeded(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_27

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v4, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    const-wide/32 v6, 0x200000

    cmp-long v4, v4, v6

    if-lez v4, :cond_27

    .line 975
    sget-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v4, :cond_26

    .line 976
    const-string v4, "DownloadManager"

    const-string v5, "(HttpLog) download with transferData_NB"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    :cond_26
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/providers/downloads/DownloadThread;->transferData_NB(Ljava/net/HttpURLConnection;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1035
    :goto_b
    if-eqz v14, :cond_19

    invoke-virtual {v14}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_9

    .line 982
    :cond_27
    :try_start_9
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/providers/downloads/DownloadThread;->transferData(Ljava/net/HttpURLConnection;)V

    goto :goto_b

    .line 989
    :sswitch_2
    const-string v4, "Location"

    invoke-virtual {v14, v4}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 990
    .local v18, "location":Ljava/lang/String;
    new-instance v29, Ljava/net/URL;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 991
    .end local v30    # "url":Ljava/net/URL;
    .restart local v29    # "url":Ljava/net/URL;
    const/16 v4, 0x12d

    move/from16 v0, v23

    if-ne v0, v4, :cond_28

    .line 993
    :try_start_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual/range {v29 .. v29}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mUri:Ljava/lang/String;
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1035
    :cond_28
    if-eqz v14, :cond_2c

    invoke-virtual {v14}, Ljava/net/HttpURLConnection;->disconnect()V

    move/from16 v22, v21

    .end local v21    # "redirectionCount":I
    .restart local v22    # "redirectionCount":I
    move-object/from16 v30, v29

    .end local v29    # "url":Ljava/net/URL;
    .restart local v30    # "url":Ljava/net/URL;
    goto/16 :goto_2

    .line 998
    .end local v18    # "location":Ljava/lang/String;
    .end local v22    # "redirectionCount":I
    .restart local v21    # "redirectionCount":I
    :sswitch_3
    :try_start_b
    new-instance v4, Lcom/android/providers/downloads/StopRequestException;

    const/16 v5, 0x1e9

    const-string v6, "Precondition failed"

    invoke-direct {v4, v5, v6}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v4

    .line 1002
    :sswitch_4
    new-instance v4, Lcom/android/providers/downloads/StopRequestException;

    const/16 v5, 0x1e9

    const-string v6, "Requested range not satisfiable"

    invoke-direct {v4, v5, v6}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v4

    .line 1006
    :sswitch_5
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/providers/downloads/DownloadThread;->parseUnavailableHeaders(Ljava/net/HttpURLConnection;)V

    .line 1007
    new-instance v4, Lcom/android/providers/downloads/StopRequestException;

    const/16 v5, 0x1f7

    invoke-virtual {v14}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v4

    .line 1011
    :sswitch_6
    new-instance v4, Lcom/android/providers/downloads/StopRequestException;

    const/16 v5, 0x1f4

    invoke-virtual {v14}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v4
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 1025
    .end local v23    # "responseCode":I
    .end local v30    # "url":Ljava/net/URL;
    .restart local v15    # "e":Ljava/io/IOException;
    .restart local v29    # "url":Ljava/net/URL;
    :cond_29
    :try_start_c
    instance-of v4, v15, Ljavax/net/ssl/SSLException;

    if-eqz v4, :cond_2a

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/providers/downloads/DownloadThread;->mCAC:Z

    if-nez v4, :cond_2a

    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->CACAvaiable()Z

    move-result v4

    if-eqz v4, :cond_2a

    .line 1026
    new-instance v4, Lcom/android/providers/downloads/StopRequestException;

    const/16 v5, 0x193

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "while trying to execute request: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v15}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6, v15}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 1031
    :cond_2a
    new-instance v4, Lcom/android/providers/downloads/StopRequestException;

    const/16 v5, 0x1ef

    invoke-direct {v4, v5, v15}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/Throwable;)V

    throw v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 1039
    .end local v14    # "conn":Ljava/net/HttpURLConnection;
    .end local v15    # "e":Ljava/io/IOException;
    .end local v29    # "url":Ljava/net/URL;
    .restart local v30    # "url":Ljava/net/URL;
    :cond_2b
    new-instance v4, Lcom/android/providers/downloads/StopRequestException;

    const/16 v5, 0x1f1

    const-string v6, "Too many redirects"

    invoke-direct {v4, v5, v6}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v4

    .line 1019
    .end local v30    # "url":Ljava/net/URL;
    .restart local v14    # "conn":Ljava/net/HttpURLConnection;
    .restart local v18    # "location":Ljava/lang/String;
    .restart local v23    # "responseCode":I
    .restart local v29    # "url":Ljava/net/URL;
    :catch_2
    move-exception v15

    goto/16 :goto_7

    .line 820
    .end local v18    # "location":Ljava/lang/String;
    .end local v23    # "responseCode":I
    .end local v29    # "url":Ljava/net/URL;
    .restart local v10    # "alias":Ljava/lang/String;
    .restart local v11    # "cacKeyMgr":Lcom/android/providers/downloads/DownloadCACKeyManager;
    .restart local v17    # "km":[Ljavax/net/ssl/KeyManager;
    .restart local v25    # "sconn":Ljavax/net/ssl/HttpsURLConnection;
    .restart local v30    # "url":Ljava/net/URL;
    :catch_3
    move-exception v4

    goto/16 :goto_4

    .end local v10    # "alias":Ljava/lang/String;
    .end local v11    # "cacKeyMgr":Lcom/android/providers/downloads/DownloadCACKeyManager;
    .end local v17    # "km":[Ljavax/net/ssl/KeyManager;
    .end local v25    # "sconn":Ljavax/net/ssl/HttpsURLConnection;
    .end local v30    # "url":Ljava/net/URL;
    .restart local v18    # "location":Ljava/lang/String;
    .restart local v23    # "responseCode":I
    .restart local v29    # "url":Ljava/net/URL;
    :cond_2c
    move/from16 v22, v21

    .end local v21    # "redirectionCount":I
    .restart local v22    # "redirectionCount":I
    move-object/from16 v30, v29

    .end local v29    # "url":Ljava/net/URL;
    .restart local v30    # "url":Ljava/net/URL;
    goto/16 :goto_2

    .line 876
    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xce -> :sswitch_1
        0x12d -> :sswitch_2
        0x12e -> :sswitch_2
        0x12f -> :sswitch_2
        0x133 -> :sswitch_2
        0x19c -> :sswitch_3
        0x1a0 -> :sswitch_4
        0x1f4 -> :sswitch_6
        0x1f7 -> :sswitch_5
    .end sparse-switch
.end method

.method private finalizeDestination()V
    .locals 8

    .prologue
    .line 1716
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v5}, Landroid/provider/Downloads$Impl;->isStatusError(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1719
    :try_start_0
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v6, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    invoke-virtual {v6}, Lcom/android/providers/downloads/DownloadInfo;->getAllDownloadsUri()Landroid/net/Uri;

    move-result-object v6

    const-string v7, "rw"

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 1722
    .local v4, "target":Landroid/os/ParcelFileDescriptor;
    :try_start_1
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v5

    const-wide/16 v6, 0x0

    invoke-static {v5, v6, v7}, Landroid/system/Os;->ftruncate(Ljava/io/FileDescriptor;J)V
    :try_end_1
    .catch Landroid/system/ErrnoException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1725
    :try_start_2
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1731
    .end local v4    # "target":Landroid/os/ParcelFileDescriptor;
    :goto_0
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 1732
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v6, v6, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 1733
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const/4 v6, 0x0

    iput-object v6, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    .line 1765
    :cond_0
    :goto_1
    return-void

    .line 1723
    .restart local v4    # "target":Landroid/os/ParcelFileDescriptor;
    :catch_0
    move-exception v5

    .line 1725
    :try_start_3
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_0

    .line 1727
    .end local v4    # "target":Landroid/os/ParcelFileDescriptor;
    :catch_1
    move-exception v5

    goto :goto_0

    .line 1725
    .restart local v4    # "target":Landroid/os/ParcelFileDescriptor;
    :catchall_0
    move-exception v5

    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    throw v5
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1736
    .end local v4    # "target":Landroid/os/ParcelFileDescriptor;
    :cond_1
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v5}, Landroid/provider/Downloads$Impl;->isStatusSuccess(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1738
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 1741
    :try_start_4
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    const/16 v6, 0x1a4

    invoke-static {v5, v6}, Landroid/system/Os;->chmod(Ljava/lang/String;I)V
    :try_end_4
    .catch Landroid/system/ErrnoException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1745
    :goto_2
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v5, v5, Lcom/android/providers/downloads/DownloadInfo;->mDestination:I

    const/4 v6, 0x4

    if-eq v5, v6, :cond_0

    .line 1748
    :try_start_5
    new-instance v2, Ljava/io/File;

    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1749
    .local v2, "before":Ljava/io/File;
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v6, v6, Lcom/android/providers/downloads/DownloadInfo;->mDestination:I

    invoke-static {v5, v6}, Lcom/android/providers/downloads/Helpers;->getRunningDestinationDirectory(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v3

    .line 1751
    .local v3, "beforeDir":Ljava/io/File;
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v6, v6, Lcom/android/providers/downloads/DownloadInfo;->mDestination:I

    invoke-static {v5, v6}, Lcom/android/providers/downloads/Helpers;->getSuccessDestinationDirectory(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v1

    .line 1753
    .local v1, "afterDir":Ljava/io/File;
    invoke-virtual {v3, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1755
    new-instance v0, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1756
    .local v0, "after":Ljava/io/File;
    invoke-virtual {v2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1757
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 1760
    .end local v0    # "after":Ljava/io/File;
    .end local v1    # "afterDir":Ljava/io/File;
    .end local v2    # "before":Ljava/io/File;
    .end local v3    # "beforeDir":Ljava/io/File;
    :catch_2
    move-exception v5

    goto :goto_1

    .line 1742
    :catch_3
    move-exception v5

    goto :goto_2
.end method

.method private finishCACDownload()V
    .locals 1

    .prologue
    .line 426
    iget-boolean v0, p0, Lcom/android/providers/downloads/DownloadThread;->mCAC:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/providers/downloads/DownloadThread;->mCACfinished:Z

    if-nez v0, :cond_0

    .line 427
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/providers/downloads/DownloadThread;->mCACfinished:Z

    .line 428
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/providers/downloads/DownloadCAC;->getInstance(Landroid/content/Context;)Lcom/android/providers/downloads/DownloadCAC;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/providers/downloads/DownloadCAC;->getInstance(Landroid/content/Context;)Lcom/android/providers/downloads/DownloadCAC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/providers/downloads/DownloadCAC;->finishedDownload()V

    .line 431
    :cond_0
    return-void
.end method

.method private getCombinedLength(JJJ)J
    .locals 5
    .param p1, "firstEnd"    # J
    .param p3, "secStart"    # J
    .param p5, "secEnd"    # J

    .prologue
    .line 1388
    const-wide/16 v0, 0x0

    .line 1389
    .local v0, "combinedLen":J
    cmp-long v2, p1, p3

    if-gtz v2, :cond_0

    .line 1390
    sub-long v2, p5, p3

    add-long v0, p1, v2

    .line 1396
    :goto_0
    return-wide v0

    .line 1391
    :cond_0
    cmp-long v2, p1, p5

    if-gtz v2, :cond_1

    .line 1392
    move-wide v0, p5

    goto :goto_0

    .line 1394
    :cond_1
    move-wide v0, p1

    goto :goto_0
.end method

.method private static getHeaderFieldLong(Ljava/net/URLConnection;Ljava/lang/String;J)J
    .locals 2
    .param p0, "conn"    # Ljava/net/URLConnection;
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "defaultValue"    # J

    .prologue
    .line 2073
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/net/URLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    .line 2075
    .end local p2    # "defaultValue":J
    :goto_0
    return-wide p2

    .line 2074
    .restart local p2    # "defaultValue":J
    :catch_0
    move-exception v0

    .line 2075
    .local v0, "e":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method private isHttpsUrl(Ljava/lang/String;)Z
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 401
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x7

    if-le v1, v2, :cond_0

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static isStatusRetryable(I)Z
    .locals 1
    .param p0, "status"    # I

    .prologue
    .line 2084
    sparse-switch p0, :sswitch_data_0

    .line 2090
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2088
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2084
    nop

    :sswitch_data_0
    .sparse-switch
        0x1ef -> :sswitch_0
        0x1f4 -> :sswitch_0
        0x1f7 -> :sswitch_0
    .end sparse-switch
.end method

.method private logDebug(Ljava/lang/String;)V
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 2038
    const-string v0, "DownloadManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2039
    return-void
.end method

.method private logError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 2046
    const-string v0, "DownloadManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2047
    return-void
.end method

.method private logWarning(Ljava/lang/String;)V
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 2042
    const-string v0, "DownloadManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2043
    return-void
.end method

.method private parseOkHeaders(Ljava/net/HttpURLConnection;)V
    .locals 14
    .param p1, "conn"    # Ljava/net/HttpURLConnection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/downloads/StopRequestException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, -0x1

    .line 1856
    iget-boolean v0, p0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v0, :cond_0

    .line 1857
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_0

    .line 1858
    const-string v0, "DownloadManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file name before parseOkHeaders "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v2, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1862
    :cond_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mMethod:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1863
    :cond_1
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/content/Intent;->normalizeMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    .line 1867
    :cond_2
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 1868
    const-string v0, "Content-Disposition"

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1869
    .local v3, "contentDisposition":Ljava/lang/String;
    const-string v0, "Content-Location"

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1872
    .local v4, "contentLocation":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mPackage:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/providers/downloads/Helpers;->ignoreDestinationUri(Ljava/lang/String;)Z

    move-result v7

    .line 1873
    .local v7, "ignoreDestinationUri":Z
    iget-object v10, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v1, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mUri:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v2, v2, Lcom/android/providers/downloads/DownloadInfo;->mHint:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v6, v6, Lcom/android/providers/downloads/DownloadInfo;->mDestination:I

    invoke-static/range {v0 .. v7}, Lcom/android/providers/downloads/Helpers;->generateSaveFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1883
    .end local v3    # "contentDisposition":Ljava/lang/String;
    .end local v4    # "contentLocation":Ljava/lang/String;
    .end local v7    # "ignoreDestinationUri":Z
    :cond_3
    iget-boolean v0, p0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    if-eqz v0, :cond_4

    .line 1884
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_4

    .line 1885
    const-string v0, "DownloadManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file name after parseOkHeaders "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v2, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1891
    :cond_4
    const-string v0, "Transfer-Encoding"

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1892
    .local v9, "transferEncoding":Ljava/lang/String;
    if-nez v9, :cond_5

    .line 1893
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const-string v1, "Content-Length"

    invoke-static {p1, v1, v12, v13}, Lcom/android/providers/downloads/DownloadThread;->getHeaderFieldLong(Ljava/net/URLConnection;Ljava/lang/String;J)J

    move-result-wide v10

    iput-wide v10, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    .line 1898
    :goto_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const-string v1, "ETag"

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mETag:Ljava/lang/String;

    .line 1900
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual {v0}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->writeToDatabase()V

    .line 1903
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadThread;->checkConnectivity()V

    .line 1904
    return-void

    .line 1876
    .end local v9    # "transferEncoding":Ljava/lang/String;
    .restart local v3    # "contentDisposition":Ljava/lang/String;
    .restart local v4    # "contentLocation":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 1877
    .local v8, "e":Ljava/io/IOException;
    new-instance v0, Lcom/android/providers/downloads/StopRequestException;

    const/16 v1, 0x1ec

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to generate filename: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1895
    .end local v3    # "contentDisposition":Ljava/lang/String;
    .end local v4    # "contentLocation":Ljava/lang/String;
    .end local v8    # "e":Ljava/io/IOException;
    .restart local v9    # "transferEncoding":Ljava/lang/String;
    :cond_5
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iput-wide v12, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    goto :goto_0
.end method

.method private parseUnavailableHeaders(Ljava/net/HttpURLConnection;)V
    .locals 6
    .param p1, "conn"    # Ljava/net/HttpURLConnection;

    .prologue
    .line 1984
    const-string v2, "Retry-After"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Ljava/net/HttpURLConnection;->getHeaderFieldInt(Ljava/lang/String;I)I

    move-result v2

    int-to-long v0, v2

    .line 1985
    .local v0, "retryAfter":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 1986
    const-wide/16 v0, 0x0

    .line 1996
    :goto_0
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, v0

    long-to-int v3, v4

    iput v3, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRetryAfter:I

    .line 1997
    return-void

    .line 1988
    :cond_0
    const-wide/16 v2, 0x1e

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    .line 1989
    const-wide/16 v0, 0x1e

    .line 1993
    :cond_1
    :goto_1
    sget-object v2, Lcom/android/providers/downloads/Helpers;->sRandom:Ljava/util/Random;

    const/16 v3, 0x1f

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_0

    .line 1990
    :cond_2
    const-wide/32 v2, 0x15180

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 1991
    const-wide/32 v0, 0x15180

    goto :goto_1
.end method

.method private readFromResponse_NB([BIILjava/io/InputStream;)I
    .locals 5
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .param p4, "entityStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/downloads/StopRequestException;
        }
    .end annotation

    .prologue
    .line 1380
    :try_start_0
    invoke-virtual {p4, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 1381
    :catch_0
    move-exception v0

    .line 1382
    .local v0, "ex":Ljava/io/IOException;
    new-instance v1, Lcom/android/providers/downloads/StopRequestException;

    const/16 v2, 0x1ef

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed reading response: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private reportProgress_NB(Ljava/io/RandomAccessFile;Z)V
    .locals 26
    .param p1, "raf"    # Ljava/io/RandomAccessFile;
    .param p2, "bFinalSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1400
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 1402
    .local v14, "now":J
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeedSampleStart:J

    sub-long v18, v14, v6

    .line 1403
    .local v18, "sampleDelta":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v0, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    move-wide/from16 v22, v0

    .line 1405
    .local v22, "sparseReceivedBytes":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v6, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v10, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v11}, Lcom/android/providers/downloads/DownloadThread;->getCombinedLength(JJJ)J

    move-result-wide v22

    .line 1406
    const-wide/16 v6, 0x1f4

    cmp-long v5, v18, v6

    if-gtz v5, :cond_0

    if-eqz p2, :cond_2

    .line 1407
    :cond_0
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeedSampleBytes:J

    sub-long v6, v22, v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    div-long v20, v6, v18

    .line 1410
    .local v20, "sampleSpeed":J
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeed:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_a

    .line 1411
    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread;->mSpeed:J

    .line 1417
    :goto_0
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeedSampleStart:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    .line 1418
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v6, v6, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeed:J

    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/android/providers/downloads/DownloadNotifier;->notifyDownloadSpeed(JJ)V

    .line 1421
    :cond_1
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeedSampleStart:J

    .line 1422
    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread;->mSpeedSampleBytes:J

    .line 1425
    .end local v20    # "sampleSpeed":J
    :cond_2
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/providers/downloads/DownloadThread;->mLastUpdateBytes:J

    sub-long v12, v22, v6

    .line 1426
    .local v12, "bytesDelta":J
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/providers/downloads/DownloadThread;->mLastUpdateTime:J

    sub-long v24, v14, v6

    .line 1427
    .local v24, "timeDelta":J
    const-wide/16 v6, 0x1000

    cmp-long v5, v12, v6

    if-lez v5, :cond_3

    const-wide/16 v6, 0x5dc

    cmp-long v5, v24, v6

    if-gtz v5, :cond_4

    :cond_3
    if-eqz p2, :cond_9

    .line 1429
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-wide/from16 v0, v22

    iput-wide v0, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    .line 1434
    const-wide/16 v16, 0x0

    .line 1435
    .local v16, "sTime":J
    sget-boolean v5, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v5, :cond_5

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v16

    .line 1436
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    if-nez v5, :cond_b

    .line 1437
    if-eqz p1, :cond_6

    .line 1438
    invoke-virtual/range {p1 .. p1}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/FileDescriptor;->sync()V

    .line 1440
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual {v5}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->writeToDatabase()V

    .line 1464
    :cond_7
    :goto_1
    sget-boolean v5, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v5, :cond_8

    const-string v5, "DownloadManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sync data setup range in report progress 0-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v7, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v7, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v7, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", CurrentBytes="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v7, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", BytesNotified="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/providers/downloads/DownloadThread;->mLastUpdateBytes:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", time to update progress : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long v8, v8, v16

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1468
    :cond_8
    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread;->mLastUpdateBytes:J

    .line 1469
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/android/providers/downloads/DownloadThread;->mLastUpdateTime:J

    .line 1471
    .end local v16    # "sTime":J
    :cond_9
    return-void

    .line 1413
    .end local v12    # "bytesDelta":J
    .end local v24    # "timeDelta":J
    .restart local v20    # "sampleSpeed":J
    :cond_a
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeed:J

    const-wide/16 v8, 0x3

    mul-long/2addr v6, v8

    add-long v6, v6, v20

    const-wide/16 v8, 0x4

    div-long/2addr v6, v8

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeed:J

    goto/16 :goto_0

    .line 1442
    .end local v20    # "sampleSpeed":J
    .restart local v12    # "bytesDelta":J
    .restart local v16    # "sTime":J
    .restart local v24    # "timeDelta":J
    :cond_b
    if-nez p2, :cond_c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    # getter for: Lcom/android/providers/downloads/DownloadThread$SyncThread;->bRunning:Z
    invoke-static {v5}, Lcom/android/providers/downloads/DownloadThread$SyncThread;->access$300(Lcom/android/providers/downloads/DownloadThread$SyncThread;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 1443
    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    iget-object v6, v5, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    monitor-enter v6

    .line 1444
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->clear()V

    .line 1445
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1446
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    iget-object v6, v5, Lcom/android/providers/downloads/DownloadThread$SyncThread;->finalSyncLocker:Ljava/lang/Object;

    monitor-enter v6

    .line 1447
    if-eqz p1, :cond_d

    .line 1448
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/FileDescriptor;->sync()V

    .line 1450
    :cond_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual {v5}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->writeToDatabase()V

    .line 1451
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    invoke-virtual {v5}, Lcom/android/providers/downloads/DownloadThread$SyncThread;->stop()V

    .line 1452
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1453
    sget-boolean v5, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v5, :cond_7

    const-string v5, "DownloadManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "final sync data to disc 0-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v7, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v7, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v7, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", CurrentBytes="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v7, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1445
    :catchall_0
    move-exception v5

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 1452
    :catchall_1
    move-exception v5

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v5

    .line 1458
    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    invoke-virtual {v5, v6}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->copyMe(Lcom/android/providers/downloads/DownloadInfo;)Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-result-object v4

    .line 1459
    .local v4, "newInfoDelta":Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    iget-object v6, v5, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    monitor-enter v6

    .line 1460
    :try_start_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    invoke-virtual {v5, v4}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 1461
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/lang/Object;->notify()V

    .line 1462
    monitor-exit v6

    goto/16 :goto_1

    :catchall_2
    move-exception v5

    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v5
.end method

.method private transferData(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/io/FileDescriptor;)V
    .locals 12
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "out"    # Ljava/io/OutputStream;
    .param p3, "outFd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/downloads/StopRequestException;
        }
    .end annotation

    .prologue
    .line 1666
    const/16 v5, 0x2000

    new-array v0, v5, [B

    .line 1668
    .local v0, "buffer":[B
    :goto_0
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadThread;->checkPausedOrCanceled()V

    .line 1670
    const/4 v4, -0x1

    .line 1672
    .local v4, "len":I
    :try_start_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 1678
    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 1706
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    const-wide/16 v10, -0x1

    cmp-long v5, v8, v10

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v10, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    cmp-long v5, v8, v10

    if-eqz v5, :cond_2

    .line 1707
    new-instance v5, Lcom/android/providers/downloads/StopRequestException;

    const/16 v8, 0x1ef

    const-string v9, "Content length mismatch"

    invoke-direct {v5, v8, v9}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v5

    .line 1673
    :catch_0
    move-exception v1

    .line 1674
    .local v1, "e":Ljava/io/IOException;
    new-instance v5, Lcom/android/providers/downloads/StopRequestException;

    const/16 v8, 0x1ef

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed reading response: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v9, v1}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 1684
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    :try_start_1
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    const-wide/16 v10, -0x1

    cmp-long v5, v8, v10

    if-nez v5, :cond_1

    .line 1685
    invoke-static {p3}, Landroid/system/Os;->fstat(Ljava/io/FileDescriptor;)Landroid/system/StructStat;

    move-result-object v5

    iget-wide v2, v5, Landroid/system/StructStat;->st_size:J

    .line 1686
    .local v2, "curSize":J
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    int-to-long v10, v4

    add-long/2addr v8, v10

    sub-long v6, v8, v2

    .line 1688
    .local v6, "newBytes":J
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-static {v5, p3, v6, v7}, Lcom/android/providers/downloads/StorageUtils;->ensureAvailableSpace(Landroid/content/Context;Ljava/io/FileDescriptor;J)V

    .line 1691
    .end local v2    # "curSize":J
    .end local v6    # "newBytes":J
    :cond_1
    const/4 v5, 0x0

    invoke-virtual {p2, v0, v5, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 1693
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/android/providers/downloads/DownloadThread;->mMadeProgress:Z

    .line 1694
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v8, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    int-to-long v10, v4

    add-long/2addr v8, v10

    iput-wide v8, v5, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    .line 1696
    invoke-direct {p0, p3}, Lcom/android/providers/downloads/DownloadThread;->updateProgress(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Landroid/system/ErrnoException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 1698
    :catch_1
    move-exception v1

    .line 1699
    .local v1, "e":Landroid/system/ErrnoException;
    new-instance v5, Lcom/android/providers/downloads/StopRequestException;

    const/16 v8, 0x1ec

    invoke-direct {v5, v8, v1}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/Throwable;)V

    throw v5

    .line 1700
    .end local v1    # "e":Landroid/system/ErrnoException;
    :catch_2
    move-exception v1

    .line 1701
    .local v1, "e":Ljava/io/IOException;
    new-instance v5, Lcom/android/providers/downloads/StopRequestException;

    const/16 v8, 0x1ec

    invoke-direct {v5, v8, v1}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/Throwable;)V

    throw v5

    .line 1709
    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    return-void
.end method

.method private transferData(Ljava/net/HttpURLConnection;)V
    .locals 22
    .param p1, "conn"    # Ljava/net/HttpURLConnection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/downloads/StopRequestException;
        }
    .end annotation

    .prologue
    .line 1563
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v18, v0

    const-wide/16 v20, -0x1

    cmp-long v18, v18, v20

    if-eqz v18, :cond_0

    const/4 v8, 0x1

    .line 1564
    .local v8, "hasLength":Z
    :goto_0
    const-string v18, "Transfer-Encoding"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 1565
    .local v17, "transferEncoding":Ljava/lang/String;
    const-string v18, "chunked"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    .line 1566
    .local v10, "isChunked":Z
    if-nez v8, :cond_1

    if-nez v10, :cond_1

    .line 1567
    new-instance v18, Lcom/android/providers/downloads/StopRequestException;

    const/16 v19, 0x1e9

    const-string v20, "can\'t know size of download, giving up"

    invoke-direct/range {v18 .. v20}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v18

    .line 1563
    .end local v8    # "hasLength":Z
    .end local v10    # "isChunked":Z
    .end local v17    # "transferEncoding":Ljava/lang/String;
    :cond_0
    const/4 v8, 0x0

    goto :goto_0

    .line 1571
    .restart local v8    # "hasLength":Z
    .restart local v10    # "isChunked":Z
    .restart local v17    # "transferEncoding":Ljava/lang/String;
    :cond_1
    const/4 v6, 0x0

    .line 1572
    .local v6, "drmClient":Landroid/drm/DrmManagerClient;
    const/16 v16, 0x0

    .line 1573
    .local v16, "outPfd":Landroid/os/ParcelFileDescriptor;
    const/4 v15, 0x0

    .line 1574
    .local v15, "outFd":Ljava/io/FileDescriptor;
    const/4 v9, 0x0

    .line 1575
    .local v9, "in":Ljava/io/InputStream;
    const/4 v11, 0x0

    .line 1578
    .local v11, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 1585
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/downloads/DownloadThread;->bSBEnabledForThisSession:Z

    move/from16 v18, v0

    if-eqz v18, :cond_2

    .line 1586
    sget-boolean v18, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v18, :cond_2

    .line 1587
    const-string v18, "DownloadManager"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "start transferData, "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/providers/downloads/DownloadInfo;->getAllDownloadsUri()Landroid/net/Uri;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1591
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/providers/downloads/DownloadInfo;->getAllDownloadsUri()Landroid/net/Uri;

    move-result-object v19

    const-string v20, "rw"

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v16

    .line 1593
    invoke-virtual/range {v16 .. v16}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v15

    .line 1599
    new-instance v14, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V
    :try_end_1
    .catch Landroid/system/ErrnoException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1603
    .end local v11    # "out":Ljava/io/OutputStream;
    .local v14, "out":Ljava/io/OutputStream;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-lez v18, :cond_3

    .line 1604
    invoke-static {v15}, Landroid/system/Os;->fstat(Ljava/io/FileDescriptor;)Landroid/system/StructStat;

    move-result-object v18

    move-object/from16 v0, v18

    iget-wide v4, v0, Landroid/system/StructStat;->st_size:J

    .line 1605
    .local v4, "curSize":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v18, v0

    sub-long v12, v18, v4

    .line 1607
    .local v12, "newBytes":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v15, v12, v13}, Lcom/android/providers/downloads/StorageUtils;->ensureAvailableSpace(Landroid/content/Context;Ljava/io/FileDescriptor;J)V
    :try_end_2
    .catch Landroid/system/ErrnoException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1611
    const-wide/16 v18, 0x0

    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v18

    move-wide/from16 v2, v20

    invoke-static {v15, v0, v1, v2, v3}, Landroid/system/Os;->posix_fallocate(Ljava/io/FileDescriptor;JJ)V
    :try_end_3
    .catch Landroid/system/ErrnoException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1623
    .end local v4    # "curSize":J
    .end local v12    # "newBytes":J
    :cond_3
    :goto_1
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    move-wide/from16 v18, v0

    sget v20, Landroid/system/OsConstants;->SEEK_SET:I

    move-wide/from16 v0, v18

    move/from16 v2, v20

    invoke-static {v15, v0, v1, v2}, Landroid/system/Os;->lseek(Ljava/io/FileDescriptor;JI)J
    :try_end_4
    .catch Landroid/system/ErrnoException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1633
    :try_start_5
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v14, v15}, Lcom/android/providers/downloads/DownloadThread;->transferData(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/io/FileDescriptor;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1636
    :try_start_6
    instance-of v0, v14, Landroid/drm/DrmOutputStream;

    move/from16 v18, v0

    if-eqz v18, :cond_4

    .line 1637
    move-object v0, v14

    check-cast v0, Landroid/drm/DrmOutputStream;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/drm/DrmOutputStream;->finish()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1644
    :cond_4
    if-eqz v6, :cond_5

    .line 1645
    invoke-virtual {v6}, Landroid/drm/DrmManagerClient;->release()V

    .line 1648
    :cond_5
    invoke-static {v9}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 1651
    if-eqz v14, :cond_6

    :try_start_7
    invoke-virtual {v14}, Ljava/io/OutputStream;->flush()V

    .line 1652
    :cond_6
    if-eqz v15, :cond_7

    invoke-virtual {v15}, Ljava/io/FileDescriptor;->sync()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1655
    :cond_7
    invoke-static {v14}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 1658
    :goto_2
    return-void

    .line 1579
    .end local v14    # "out":Ljava/io/OutputStream;
    .restart local v11    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v7

    .line 1580
    .local v7, "e":Ljava/io/IOException;
    :try_start_8
    new-instance v18, Lcom/android/providers/downloads/StopRequestException;

    const/16 v19, 0x1ef

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v1, v7}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/Throwable;)V

    throw v18
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1644
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v18

    :goto_3
    if-eqz v6, :cond_8

    .line 1645
    invoke-virtual {v6}, Landroid/drm/DrmManagerClient;->release()V

    .line 1648
    :cond_8
    invoke-static {v9}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 1651
    if-eqz v11, :cond_9

    :try_start_9
    invoke-virtual {v11}, Ljava/io/OutputStream;->flush()V

    .line 1652
    :cond_9
    if-eqz v15, :cond_a

    invoke-virtual {v15}, Ljava/io/FileDescriptor;->sync()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 1655
    :cond_a
    invoke-static {v11}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    :goto_4
    throw v18

    .line 1612
    .end local v11    # "out":Ljava/io/OutputStream;
    .restart local v4    # "curSize":J
    .restart local v12    # "newBytes":J
    .restart local v14    # "out":Ljava/io/OutputStream;
    :catch_1
    move-exception v7

    .line 1613
    .local v7, "e":Landroid/system/ErrnoException;
    :try_start_a
    iget v0, v7, Landroid/system/ErrnoException;->errno:I

    move/from16 v18, v0

    sget v19, Landroid/system/OsConstants;->ENOTSUP:I

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_b

    .line 1614
    const-string v18, "DownloadManager"

    const-string v19, "fallocate() said ENOTSUP; falling back to ftruncate()"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1615
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-static {v15, v0, v1}, Landroid/system/Os;->ftruncate(Ljava/io/FileDescriptor;J)V
    :try_end_a
    .catch Landroid/system/ErrnoException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_1

    .line 1625
    .end local v4    # "curSize":J
    .end local v7    # "e":Landroid/system/ErrnoException;
    .end local v12    # "newBytes":J
    :catch_2
    move-exception v7

    move-object v11, v14

    .line 1626
    .end local v14    # "out":Ljava/io/OutputStream;
    .restart local v7    # "e":Landroid/system/ErrnoException;
    .restart local v11    # "out":Ljava/io/OutputStream;
    :goto_5
    :try_start_b
    new-instance v18, Lcom/android/providers/downloads/StopRequestException;

    const/16 v19, 0x1ec

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v1, v7}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/Throwable;)V

    throw v18
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 1617
    .end local v11    # "out":Ljava/io/OutputStream;
    .restart local v4    # "curSize":J
    .restart local v12    # "newBytes":J
    .restart local v14    # "out":Ljava/io/OutputStream;
    :cond_b
    :try_start_c
    throw v7
    :try_end_c
    .catch Landroid/system/ErrnoException; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 1627
    .end local v4    # "curSize":J
    .end local v7    # "e":Landroid/system/ErrnoException;
    .end local v12    # "newBytes":J
    :catch_3
    move-exception v7

    move-object v11, v14

    .line 1628
    .end local v14    # "out":Ljava/io/OutputStream;
    .local v7, "e":Ljava/io/IOException;
    .restart local v11    # "out":Ljava/io/OutputStream;
    :goto_6
    :try_start_d
    new-instance v18, Lcom/android/providers/downloads/StopRequestException;

    const/16 v19, 0x1ec

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v1, v7}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/Throwable;)V

    throw v18
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 1639
    .end local v7    # "e":Ljava/io/IOException;
    .end local v11    # "out":Ljava/io/OutputStream;
    .restart local v14    # "out":Ljava/io/OutputStream;
    :catch_4
    move-exception v7

    .line 1640
    .restart local v7    # "e":Ljava/io/IOException;
    :try_start_e
    new-instance v18, Lcom/android/providers/downloads/StopRequestException;

    const/16 v19, 0x1ec

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v1, v7}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/Throwable;)V

    throw v18
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 1644
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v18

    move-object v11, v14

    .end local v14    # "out":Ljava/io/OutputStream;
    .restart local v11    # "out":Ljava/io/OutputStream;
    goto :goto_3

    .line 1653
    .end local v11    # "out":Ljava/io/OutputStream;
    .restart local v14    # "out":Ljava/io/OutputStream;
    :catch_5
    move-exception v18

    .line 1655
    invoke-static {v14}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_2

    :catchall_2
    move-exception v18

    invoke-static {v14}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    throw v18

    .line 1653
    .end local v14    # "out":Ljava/io/OutputStream;
    .restart local v11    # "out":Ljava/io/OutputStream;
    :catch_6
    move-exception v19

    .line 1655
    invoke-static {v11}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_4

    :catchall_3
    move-exception v18

    invoke-static {v11}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    throw v18

    .line 1627
    :catch_7
    move-exception v7

    goto :goto_6

    .line 1625
    :catch_8
    move-exception v7

    goto :goto_5
.end method

.method private transferData_NB(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/RandomAccessFile;)V
    .locals 26
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "fName"    # Ljava/lang/String;
    .param p3, "raf"    # Ljava/io/RandomAccessFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/downloads/StopRequestException;
        }
    .end annotation

    .prologue
    .line 1186
    const/high16 v18, 0x10000

    .line 1187
    .local v18, "size_limit":I
    const/4 v11, 0x0

    .line 1188
    .local v11, "pendig_data_size":I
    const/high16 v19, 0x10000

    move/from16 v0, v19

    new-array v12, v0, [B

    .line 1189
    .local v12, "pending_data":[B
    const/16 v19, 0x20

    move/from16 v0, v19

    new-array v13, v0, [B

    .line 1190
    .local v13, "secChunkLen":[B
    const/16 v19, 0x2000

    const/high16 v22, 0x10000

    move/from16 v0, v19

    move/from16 v1, v22

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 1191
    .local v10, "nextReadLen":I
    const/4 v5, 0x0

    .line 1192
    .local v5, "bFileNameSent":Z
    const/4 v6, 0x1

    .line 1193
    .local v6, "bReportProgressForSecChunk":Z
    if-nez p2, :cond_0

    .line 1194
    const/4 v5, 0x1

    .line 1195
    const/4 v6, 0x0

    .line 1197
    :cond_0
    sget-boolean v19, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v19, :cond_1

    .line 1198
    const-string v19, "DownloadManager"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "InputStream is "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", class name is "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1200
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v19

    const-string v22, "okhttp.internal.http.MultiSocketInputStream"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v19

    if-gez v19, :cond_2

    .line 1201
    const/4 v5, 0x1

    .line 1202
    const/4 v6, 0x0

    .line 1204
    :cond_2
    sget-boolean v19, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v19, :cond_3

    .line 1205
    const-string v19, "DownloadManager"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "task download starts : bFileNameSent="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", bReportProgressForSecChunk="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1208
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 1210
    .local v20, "startTime":J
    :cond_4
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->checkPausedOrCanceled()V

    .line 1215
    if-nez v5, :cond_b

    .line 1216
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    .line 1217
    .local v7, "byteFName":[B
    const/4 v5, 0x1

    .line 1218
    const/16 v19, -0x64

    array-length v0, v7

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v22

    move-object/from16 v3, p1

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/android/providers/downloads/DownloadThread;->readFromResponse_NB([BIILjava/io/InputStream;)I

    move-result v8

    .line 1219
    .local v8, "bytesRead":I
    const/16 v19, -0x65

    move/from16 v0, v19

    if-ne v8, v0, :cond_5

    .line 1220
    sget-boolean v19, Lcom/android/providers/downloads/Constants;->LOGVV:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v19, :cond_4

    .line 1222
    :try_start_1
    const-string v19, "DownloadManager"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "[fileIO][fileNameNotify] "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " now file pointer "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p3 .. p3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1224
    :catch_0
    move-exception v19

    goto :goto_0

    .line 1230
    :cond_5
    :try_start_2
    sget-boolean v19, Lcom/android/providers/downloads/Constants;->LOGVV:Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v19, :cond_6

    .line 1232
    :try_start_3
    const-string v19, "DownloadManager"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "[fileIO][fileNameNotifyFail] "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " now file pointer "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p3 .. p3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7

    .line 1281
    .end local v7    # "byteFName":[B
    :cond_6
    :goto_1
    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v8, v0, :cond_11

    .line 1282
    if-lez v11, :cond_8

    .line 1283
    const/16 v19, 0x0

    :try_start_4
    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-virtual {v0, v12, v1, v11}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 1284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    move-wide/from16 v22, v0

    int-to-long v0, v11

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    .line 1285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    move-wide/from16 v22, v0

    int-to-long v0, v11

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    .line 1286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    move-wide/from16 v22, v0

    int-to-long v0, v11

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    .line 1287
    sget-boolean v19, Lcom/android/providers/downloads/Constants;->LOGVV:Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    if-eqz v19, :cond_7

    .line 1289
    :try_start_5
    const-string v19, "DownloadManager"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "[fileIO][write1] "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " now file pointer "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p3 .. p3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 1295
    :cond_7
    :goto_2
    const/4 v11, 0x0

    .line 1298
    :cond_8
    const/16 v19, 0x1

    :try_start_6
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/android/providers/downloads/DownloadThread;->reportProgress_NB(Ljava/io/RandomAccessFile;Z)V

    .line 1299
    sget-boolean v19, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v19, :cond_9

    .line 1300
    const-string v19, "DownloadManager"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "downloaded "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " for "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 1367
    :cond_9
    sget-boolean v19, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v19, :cond_a

    .line 1368
    const-string v19, "DownloadManager"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "one chunk finished, mInfoDelta.mTotalBytes="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", mInfoDelta.mCurrentBytes="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", mInfoDelta.bFirstChunkOfMultiChunk="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-boolean v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->bFirstChunkOfMultiChunk:Z

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1372
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v22, v0

    const-wide/16 v24, -0x1

    cmp-long v19, v22, v24

    if-eqz v19, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v24, v0

    cmp-long v19, v22, v24

    if-eqz v19, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->bFirstChunkOfMultiChunk:Z

    move/from16 v19, v0

    if-nez v19, :cond_17

    .line 1373
    new-instance v19, Lcom/android/providers/downloads/StopRequestException;

    const/16 v22, 0x1ef

    const-string v23, "Content length mismatch"

    move-object/from16 v0, v19

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v19

    .line 1241
    .end local v8    # "bytesRead":I
    :cond_b
    if-eqz v6, :cond_10

    :try_start_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    sub-long v22, v22, v20

    const-wide/16 v24, 0x5dc

    cmp-long v19, v22, v24

    if-ltz v19, :cond_10

    .line 1242
    const/16 v19, -0x66

    array-length v0, v13

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v22

    move-object/from16 v3, p1

    invoke-direct {v0, v13, v1, v2, v3}, Lcom/android/providers/downloads/DownloadThread;->readFromResponse_NB([BIILjava/io/InputStream;)I

    move-result v8

    .line 1243
    .restart local v8    # "bytesRead":I
    const/16 v19, -0x67

    move/from16 v0, v19

    if-ne v8, v0, :cond_f

    .line 1244
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 1246
    const/16 v19, 0x0

    const/16 v22, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v22

    invoke-direct {v0, v13, v1, v2}, Lcom/android/providers/downloads/DownloadThread;->convertFromBytes([BII)J

    move-result-wide v16

    .line 1247
    .local v16, "secStart":J
    const-wide/16 v22, 0x0

    cmp-long v19, v16, v22

    if-lez v19, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-wide/from16 v0, v16

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    .line 1248
    :cond_c
    const/16 v19, 0x8

    const/16 v22, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v22

    invoke-direct {v0, v13, v1, v2}, Lcom/android/providers/downloads/DownloadThread;->convertFromBytes([BII)J

    move-result-wide v14

    .line 1249
    .local v14, "secEnd":J
    const-wide/16 v22, 0x0

    cmp-long v19, v14, v22

    if-lez v19, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iput-wide v14, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    .line 1250
    :cond_d
    sget-boolean v19, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v19, :cond_e

    .line 1251
    const-string v19, "DownloadManager"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "[fileIO][secwrite] 0-"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " - "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", current bytes="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " file "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1256
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x0

    cmp-long v19, v22, v24

    if-ltz v19, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x0

    cmp-long v19, v22, v24

    if-ltz v19, :cond_4

    .line 1257
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/android/providers/downloads/DownloadThread;->reportProgress_NB(Ljava/io/RandomAccessFile;Z)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_0

    .line 1275
    .end local v8    # "bytesRead":I
    .end local v14    # "secEnd":J
    .end local v16    # "secStart":J
    :catch_1
    move-exception v9

    .line 1276
    .local v9, "e":Ljava/io/IOException;
    new-instance v19, Lcom/android/providers/downloads/StopRequestException;

    const/16 v22, 0x1ef

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Failed reading response: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v19

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2, v9}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    throw v19

    .line 1262
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v8    # "bytesRead":I
    :cond_f
    const-wide v20, 0x7fffffffffffffffL

    .line 1263
    const/4 v6, 0x0

    .line 1264
    :try_start_8
    sget-boolean v19, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v19, :cond_6

    .line 1265
    const-string v19, "DownloadManager"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "[fileIO][secwriteFail] 0-"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " - "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", current bytes="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " file "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1272
    .end local v8    # "bytesRead":I
    :cond_10
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v12, v11, v10, v1}, Lcom/android/providers/downloads/DownloadThread;->readFromResponse_NB([BIILjava/io/InputStream;)I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    move-result v8

    .restart local v8    # "bytesRead":I
    goto/16 :goto_1

    .line 1305
    :cond_11
    move v4, v8

    .line 1306
    .local v4, "actRead":I
    :try_start_9
    array-length v0, v12

    move/from16 v19, v0

    move/from16 v0, v19

    if-le v4, v0, :cond_15

    .line 1307
    if-lez v11, :cond_12

    .line 1308
    const/16 v19, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-virtual {v0, v12, v1, v11}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 1309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    move-wide/from16 v22, v0

    int-to-long v0, v11

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    .line 1310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    move-wide/from16 v22, v0

    int-to-long v0, v11

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    .line 1311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    move-wide/from16 v22, v0

    int-to-long v0, v11

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    .line 1313
    sget-boolean v19, Lcom/android/providers/downloads/Constants;->LOGVV:Z
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    if-eqz v19, :cond_12

    .line 1315
    :try_start_a
    const-string v19, "DownloadManager"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "[fileIO][write2] "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " now file pointer "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p3 .. p3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 1322
    :cond_12
    :goto_3
    :try_start_b
    array-length v0, v12

    move/from16 v19, v0

    sub-int v4, v4, v19

    .line 1323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    move-wide/from16 v22, v0

    int-to-long v0, v4

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    .line 1324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    move-wide/from16 v22, v0

    int-to-long v0, v4

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    .line 1325
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    move-wide/from16 v22, v0

    int-to-long v0, v4

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    .line 1326
    const/4 v11, 0x0

    .line 1327
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p3

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 1328
    sget-boolean v19, Lcom/android/providers/downloads/Constants;->LOGVV:Z
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2

    if-eqz v19, :cond_13

    .line 1330
    :try_start_c
    const-string v19, "DownloadManager"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "[fileIO][seek] "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " now file pointer "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p3 .. p3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4

    .line 1336
    :cond_13
    :goto_4
    const/16 v19, 0x0

    :try_start_d
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/android/providers/downloads/DownloadThread;->reportProgress_NB(Ljava/io/RandomAccessFile;Z)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_2

    .line 1362
    :cond_14
    :goto_5
    const/16 v19, 0x2000

    const/high16 v22, 0x10000

    sub-int v22, v22, v11

    move/from16 v0, v19

    move/from16 v1, v22

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 1364
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/downloads/DownloadThread;->mMadeProgress:Z

    goto/16 :goto_0

    .line 1338
    :cond_15
    add-int/2addr v11, v4

    .line 1339
    const v19, 0xe000

    move/from16 v0, v19

    if-lt v11, v0, :cond_14

    .line 1340
    const/16 v19, 0x0

    :try_start_e
    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-virtual {v0, v12, v1, v11}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 1341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    move-wide/from16 v22, v0

    int-to-long v0, v11

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    .line 1342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    move-wide/from16 v22, v0

    int-to-long v0, v11

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    .line 1343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    move-wide/from16 v22, v0

    int-to-long v0, v11

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    .line 1345
    sget-boolean v19, Lcom/android/providers/downloads/Constants;->LOGVV:Z
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2

    if-eqz v19, :cond_16

    .line 1347
    :try_start_f
    const-string v19, "DownloadManager"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "[fileIO][write3] "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " now file pointer "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p3 .. p3}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_3

    .line 1353
    :cond_16
    :goto_6
    const/4 v11, 0x0

    .line 1354
    const/16 v19, 0x0

    :try_start_10
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/android/providers/downloads/DownloadThread;->reportProgress_NB(Ljava/io/RandomAccessFile;Z)V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_2

    goto/16 :goto_5

    .line 1358
    .end local v4    # "actRead":I
    :catch_2
    move-exception v9

    .line 1359
    .restart local v9    # "e":Ljava/io/IOException;
    new-instance v19, Lcom/android/providers/downloads/StopRequestException;

    const/16 v22, 0x1ec

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-direct {v0, v1, v9}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/Throwable;)V

    throw v19

    .line 1375
    .end local v9    # "e":Ljava/io/IOException;
    :cond_17
    return-void

    .line 1349
    .restart local v4    # "actRead":I
    :catch_3
    move-exception v19

    goto :goto_6

    .line 1332
    :catch_4
    move-exception v19

    goto/16 :goto_4

    .line 1317
    :catch_5
    move-exception v19

    goto/16 :goto_3

    .line 1291
    .end local v4    # "actRead":I
    :catch_6
    move-exception v19

    goto/16 :goto_2

    .line 1234
    .restart local v7    # "byteFName":[B
    :catch_7
    move-exception v19

    goto/16 :goto_1
.end method

.method private transferData_NB(Ljava/net/HttpURLConnection;)V
    .locals 26
    .param p1, "conn"    # Ljava/net/HttpURLConnection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/downloads/StopRequestException;
        }
    .end annotation

    .prologue
    .line 1047
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v20, v0

    const-wide/16 v22, -0x1

    cmp-long v20, v20, v22

    if-eqz v20, :cond_0

    const/4 v10, 0x1

    .line 1048
    .local v10, "hasLength":Z
    :goto_0
    const-string v20, "Transfer-Encoding"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 1049
    .local v19, "transferEncoding":Ljava/lang/String;
    const-string v20, "chunked"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    .line 1050
    .local v12, "isChunked":Z
    if-nez v10, :cond_1

    if-nez v12, :cond_1

    .line 1051
    new-instance v20, Lcom/android/providers/downloads/StopRequestException;

    const/16 v21, 0x1e9

    const-string v22, "can\'t know size of download, giving up"

    invoke-direct/range {v20 .. v22}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v20

    .line 1047
    .end local v10    # "hasLength":Z
    .end local v12    # "isChunked":Z
    .end local v19    # "transferEncoding":Ljava/lang/String;
    :cond_0
    const/4 v10, 0x0

    goto :goto_0

    .line 1055
    .restart local v10    # "hasLength":Z
    .restart local v12    # "isChunked":Z
    .restart local v19    # "transferEncoding":Ljava/lang/String;
    :cond_1
    const/4 v11, 0x0

    .line 1056
    .local v11, "in":Ljava/io/InputStream;
    const/16 v16, 0x0

    .line 1057
    .local v16, "raf":Ljava/io/RandomAccessFile;
    const/4 v8, 0x0

    .line 1060
    .local v8, "fName":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 1070
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v8, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    .line 1071
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v20, :cond_2

    .line 1072
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "mInfoDelta.mFileName = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1074
    :cond_2
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1075
    .local v9, "file":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v20

    if-nez v20, :cond_10

    .line 1076
    invoke-virtual {v9}, Ljava/io/File;->createNewFile()Z

    .line 1077
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v20, :cond_3

    .line 1078
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "[fileIO][create] "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1089
    :cond_3
    :goto_1
    new-instance v17, Ljava/io/RandomAccessFile;

    const-string v20, "rw"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-direct {v0, v8, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1091
    .end local v16    # "raf":Ljava/io/RandomAccessFile;
    .local v17, "raf":Ljava/io/RandomAccessFile;
    :try_start_2
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v20, :cond_4

    .line 1092
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "[fileIO][create] try to attemp for file storage: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v22, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v24

    sub-long v22, v22, v24

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-lez v20, :cond_5

    .line 1096
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v22

    sub-long v14, v20, v22

    .line 1097
    .local v14, "newBytes":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v14, v15}, Lcom/android/providers/downloads/StorageUtils;->ensureAvailableSpace(Landroid/content/Context;Ljava/io/FileDescriptor;J)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1101
    :try_start_3
    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v20

    const-wide/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    move-wide/from16 v3, v24

    invoke-static {v0, v1, v2, v3, v4}, Landroid/system/Os;->posix_fallocate(Ljava/io/FileDescriptor;JJ)V
    :try_end_3
    .catch Landroid/system/ErrnoException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1113
    .end local v14    # "newBytes":J
    :cond_5
    :goto_2
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 1114
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v20, :cond_6

    .line 1115
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "[fileIO][seek] "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", current file length="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "[fileIO][create] file FD is "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1118
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v20, v0

    new-instance v21, Lcom/android/providers/downloads/DownloadThread$SyncThread;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    move-wide/from16 v3, v22

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/downloads/DownloadThread$SyncThread;-><init>(Lcom/android/providers/downloads/DownloadThread;Ljava/io/RandomAccessFile;J)V

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    .line 1119
    new-instance v18, Ljava/lang/Thread;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1120
    .local v18, "t":Ljava/lang/Thread;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Thread;->start()V

    .line 1121
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v20, :cond_7

    .line 1122
    const-string v20, "DownloadManager"

    const-string v21, "[fileIO][create] file sync thread is started"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1131
    :cond_7
    :try_start_5
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v20, :cond_8

    .line 1132
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "start to receive data from "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " and write data to "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1140
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v11, v8, v1}, Lcom/android/providers/downloads/DownloadThread;->transferData_NB(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/RandomAccessFile;)V

    .line 1142
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v20, :cond_9

    .line 1143
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "finish to receive data from "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " and write data to "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1147
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    move-object/from16 v20, v0

    if-eqz v20, :cond_a

    .line 1148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/providers/downloads/DownloadThread$SyncThread;->stop()V

    .line 1150
    :cond_a
    invoke-static {v11}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 1152
    if-eqz v17, :cond_b

    .line 1153
    :try_start_6
    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v13

    .line 1154
    .local v13, "outFd":Ljava/io/FileDescriptor;
    if-eqz v13, :cond_b

    .line 1155
    invoke-virtual {v13}, Ljava/io/FileDescriptor;->sync()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_7

    .line 1161
    .end local v13    # "outFd":Ljava/io/FileDescriptor;
    :cond_b
    :goto_3
    if-eqz v17, :cond_c

    .line 1163
    :try_start_7
    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4

    .line 1168
    :cond_c
    :goto_4
    return-void

    .line 1061
    .end local v9    # "file":Ljava/io/File;
    .end local v17    # "raf":Ljava/io/RandomAccessFile;
    .end local v18    # "t":Ljava/lang/Thread;
    .restart local v16    # "raf":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v6

    .line 1062
    .local v6, "e":Ljava/io/IOException;
    :try_start_8
    new-instance v20, Lcom/android/providers/downloads/StopRequestException;

    const/16 v21, 0x1ef

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v0, v1, v6}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/Throwable;)V

    throw v20
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1147
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v20

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    move-object/from16 v21, v0

    if-eqz v21, :cond_d

    .line 1148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/android/providers/downloads/DownloadThread$SyncThread;->stop()V

    .line 1150
    :cond_d
    invoke-static {v11}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 1152
    if-eqz v16, :cond_e

    .line 1153
    :try_start_9
    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v13

    .line 1154
    .restart local v13    # "outFd":Ljava/io/FileDescriptor;
    if-eqz v13, :cond_e

    .line 1155
    invoke-virtual {v13}, Ljava/io/FileDescriptor;->sync()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_6

    .line 1161
    .end local v13    # "outFd":Ljava/io/FileDescriptor;
    :cond_e
    :goto_6
    if-eqz v16, :cond_f

    .line 1163
    :try_start_a
    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_5

    .line 1165
    :cond_f
    :goto_7
    throw v20

    .line 1081
    .restart local v9    # "file":Ljava/io/File;
    :cond_10
    :try_start_b
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v20, :cond_3

    .line 1082
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "[fileIO][exist file] "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_1

    .line 1125
    .end local v9    # "file":Ljava/io/File;
    :catch_1
    move-exception v7

    .line 1126
    .local v7, "ex":Ljava/lang/Throwable;
    :goto_8
    :try_start_c
    const-string v20, "DownloadManager"

    move-object/from16 v0, v20

    invoke-static {v0, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1127
    new-instance v20, Lcom/android/providers/downloads/StopRequestException;

    const/16 v21, 0x1ec

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v0, v1, v7}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/Throwable;)V

    throw v20
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 1102
    .end local v7    # "ex":Ljava/lang/Throwable;
    .end local v16    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "file":Ljava/io/File;
    .restart local v14    # "newBytes":J
    .restart local v17    # "raf":Ljava/io/RandomAccessFile;
    :catch_2
    move-exception v6

    .line 1103
    .local v6, "e":Landroid/system/ErrnoException;
    :try_start_d
    iget v0, v6, Landroid/system/ErrnoException;->errno:I

    move/from16 v20, v0

    sget v21, Landroid/system/OsConstants;->ENOTSUP:I

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_11

    .line 1104
    const-string v20, "DownloadManager"

    const-string v21, "fallocate() said ENOTSUP; falling back to ftruncate()"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1105
    invoke-virtual/range {v17 .. v17}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Landroid/system/Os;->ftruncate(Ljava/io/FileDescriptor;J)V

    goto/16 :goto_2

    .line 1125
    .end local v6    # "e":Landroid/system/ErrnoException;
    .end local v14    # "newBytes":J
    :catch_3
    move-exception v7

    move-object/from16 v16, v17

    .end local v17    # "raf":Ljava/io/RandomAccessFile;
    .restart local v16    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_8

    .line 1107
    .end local v16    # "raf":Ljava/io/RandomAccessFile;
    .restart local v6    # "e":Landroid/system/ErrnoException;
    .restart local v14    # "newBytes":J
    .restart local v17    # "raf":Ljava/io/RandomAccessFile;
    :cond_11
    throw v6
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 1147
    .end local v6    # "e":Landroid/system/ErrnoException;
    .end local v14    # "newBytes":J
    :catchall_1
    move-exception v20

    move-object/from16 v16, v17

    .end local v17    # "raf":Ljava/io/RandomAccessFile;
    .restart local v16    # "raf":Ljava/io/RandomAccessFile;
    goto/16 :goto_5

    .line 1164
    .end local v16    # "raf":Ljava/io/RandomAccessFile;
    .restart local v17    # "raf":Ljava/io/RandomAccessFile;
    .restart local v18    # "t":Ljava/lang/Thread;
    :catch_4
    move-exception v20

    goto/16 :goto_4

    .end local v9    # "file":Ljava/io/File;
    .end local v17    # "raf":Ljava/io/RandomAccessFile;
    .end local v18    # "t":Ljava/lang/Thread;
    .restart local v16    # "raf":Ljava/io/RandomAccessFile;
    :catch_5
    move-exception v21

    goto :goto_7

    .line 1158
    :catch_6
    move-exception v21

    goto :goto_6

    .end local v16    # "raf":Ljava/io/RandomAccessFile;
    .restart local v9    # "file":Ljava/io/File;
    .restart local v17    # "raf":Ljava/io/RandomAccessFile;
    .restart local v18    # "t":Ljava/lang/Thread;
    :catch_7
    move-exception v20

    goto/16 :goto_3
.end method

.method private updateProgress(Ljava/io/FileDescriptor;)V
    .locals 24
    .param p1, "outFd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1813
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 1814
    .local v10, "now":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v8, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    .line 1816
    .local v8, "currentBytes":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeedSampleStart:J

    move-wide/from16 v18, v0

    sub-long v12, v10, v18

    .line 1817
    .local v12, "sampleDelta":J
    const-wide/16 v18, 0x1f4

    cmp-long v18, v12, v18

    if-lez v18, :cond_1

    .line 1818
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeedSampleBytes:J

    move-wide/from16 v18, v0

    sub-long v18, v8, v18

    const-wide/16 v20, 0x3e8

    mul-long v18, v18, v20

    div-long v14, v18, v12

    .line 1821
    .local v14, "sampleSpeed":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeed:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-nez v18, :cond_3

    .line 1822
    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeed:J

    .line 1828
    :goto_0
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeedSampleStart:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-eqz v18, :cond_0

    .line 1829
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread;->mId:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeed:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    move-wide/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/providers/downloads/DownloadNotifier;->notifyDownloadSpeed(JJ)V

    .line 1832
    :cond_0
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeedSampleStart:J

    .line 1833
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeedSampleBytes:J

    .line 1836
    .end local v14    # "sampleSpeed":J
    :cond_1
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread;->mLastUpdateBytes:J

    move-wide/from16 v18, v0

    sub-long v6, v8, v18

    .line 1837
    .local v6, "bytesDelta":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread;->mLastUpdateTime:J

    move-wide/from16 v18, v0

    sub-long v16, v10, v18

    .line 1838
    .local v16, "timeDelta":J
    const-wide/16 v18, 0x1000

    cmp-long v18, v6, v18

    if-lez v18, :cond_2

    const-wide/16 v18, 0x5dc

    cmp-long v18, v16, v18

    if-lez v18, :cond_2

    .line 1841
    invoke-virtual/range {p1 .. p1}, Ljava/io/FileDescriptor;->sync()V

    .line 1843
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->writeToDatabase()V

    .line 1845
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/android/providers/downloads/DownloadThread;->mLastUpdateBytes:J

    .line 1846
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/android/providers/downloads/DownloadThread;->mLastUpdateTime:J

    .line 1848
    :cond_2
    return-void

    .line 1824
    .end local v6    # "bytesDelta":J
    .end local v16    # "timeDelta":J
    .restart local v14    # "sampleSpeed":J
    :cond_3
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadThread;->mSpeed:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x3

    mul-long v18, v18, v20

    add-long v18, v18, v14

    const-wide/16 v20, 0x4

    div-long v18, v18, v20

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/providers/downloads/DownloadThread;->mSpeed:J

    goto :goto_0
.end method


# virtual methods
.method public getMDMProxyServer()Ljava/net/Proxy;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 469
    const-string v0, "content://com.sec.knox.provider/BrowserPolicy"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 470
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "getHttpProxy"

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 471
    .local v6, "cr":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 473
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 474
    const-string v0, "getHttpProxy"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 475
    .local v8, "mdmProxyStr":Ljava/lang/String;
    if-eqz v8, :cond_1

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 476
    const/16 v0, 0x3a

    invoke-virtual {v8, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    .line 477
    .local v7, "i":I
    const/4 v0, 0x0

    invoke-virtual {v8, v0, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 479
    .local v9, "proxyHost":Ljava/lang/String;
    add-int/lit8 v0, v7, 0x1

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v8, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 480
    .local v11, "tempProxyPort":Ljava/lang/String;
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 481
    .local v10, "proxyPort":I
    new-instance v2, Ljava/net/Proxy;

    sget-object v0, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    new-instance v3, Ljava/net/InetSocketAddress;

    invoke-direct {v3, v9, v10}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-direct {v2, v0, v3}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 487
    .end local v7    # "i":I
    .end local v8    # "mdmProxyStr":Ljava/lang/String;
    .end local v9    # "proxyHost":Ljava/lang/String;
    .end local v10    # "proxyPort":I
    .end local v11    # "tempProxyPort":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 484
    .restart local v8    # "mdmProxyStr":Ljava/lang/String;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v8    # "mdmProxyStr":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public run()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 437
    :try_start_0
    invoke-virtual {p0}, Lcom/android/providers/downloads/DownloadThread;->runInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 440
    iget-boolean v0, p0, Lcom/android/providers/downloads/DownloadThread;->mSkipDequeueDownload:Z

    if-eq v0, v2, :cond_0

    .line 442
    invoke-static {}, Lcom/android/providers/downloads/DownloadHandler;->getInstance()Lcom/android/providers/downloads/DownloadHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v2, v1, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v0, v2, v3}, Lcom/android/providers/downloads/DownloadHandler;->dequeueDownload(J)V

    .line 445
    :cond_0
    return-void

    .line 440
    :catchall_0
    move-exception v0

    iget-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread;->mSkipDequeueDownload:Z

    if-eq v1, v2, :cond_1

    .line 442
    invoke-static {}, Lcom/android/providers/downloads/DownloadHandler;->getInstance()Lcom/android/providers/downloads/DownloadHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v2, v2, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v1, v2, v3}, Lcom/android/providers/downloads/DownloadHandler;->dequeueDownload(J)V

    :cond_1
    throw v0
.end method

.method public runInternal()V
    .locals 18

    .prologue
    .line 494
    const/16 v11, 0xa

    invoke-static {v11}, Landroid/os/Process;->setThreadPriority(I)V

    .line 498
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/providers/downloads/DownloadThread;->mId:J

    invoke-static {v11, v12, v13}, Lcom/android/providers/downloads/DownloadInfo;->queryDownloadStatus(Landroid/content/ContentResolver;J)I

    move-result v11

    const/16 v12, 0xc8

    if-ne v11, v12, :cond_1

    .line 500
    const-string v11, "Already finished; skipping"

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/providers/downloads/DownloadThread;->logDebug(Ljava/lang/String;)V

    .line 502
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->finishCACDownload()V

    .line 653
    :cond_0
    :goto_0
    return-void

    .line 507
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-static {v11}, Landroid/net/NetworkPolicyManager;->from(Landroid/content/Context;)Landroid/net/NetworkPolicyManager;

    move-result-object v6

    .line 508
    .local v6, "netPolicy":Landroid/net/NetworkPolicyManager;
    const/4 v10, 0x0

    .line 509
    .local v10, "wakeLock":Landroid/os/PowerManager$WakeLock;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    const-string v12, "power"

    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/PowerManager;

    .line 512
    .local v7, "pm":Landroid/os/PowerManager;
    const/4 v11, 0x1

    :try_start_0
    const-string v12, "DownloadManager"

    invoke-virtual {v7, v11, v12}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v10

    .line 513
    new-instance v11, Landroid/os/WorkSource;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v12, v12, Lcom/android/providers/downloads/DownloadInfo;->mUid:I

    invoke-direct {v11, v12}, Landroid/os/WorkSource;-><init>(I)V

    invoke-virtual {v10, v11}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    .line 514
    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 517
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mPolicyListener:Landroid/net/INetworkPolicyListener;

    invoke-virtual {v6, v11}, Landroid/net/NetworkPolicyManager;->registerListener(Landroid/net/INetworkPolicyListener;)V

    .line 519
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Starting "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v12, v12, Lcom/android/providers/downloads/DownloadInfo;->mPackage:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/providers/downloads/DownloadThread;->logDebug(Ljava/lang/String;)V

    .line 523
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v12, v12, Lcom/android/providers/downloads/DownloadInfo;->mUid:I

    invoke-interface {v11, v12}, Lcom/android/providers/downloads/SystemFacade;->getActiveNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 524
    .local v4, "info":Landroid/net/NetworkInfo;
    if-eqz v4, :cond_2

    .line 525
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v11

    move-object/from16 v0, p0

    iput v11, v0, Lcom/android/providers/downloads/DownloadThread;->mNetworkType:I

    .line 530
    :cond_2
    const/16 v11, -0xff

    invoke-static {v11}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 531
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v11, v11, Lcom/android/providers/downloads/DownloadInfo;->mUid:I

    invoke-static {v11}, Landroid/net/TrafficStats;->setThreadStatsUid(I)V

    .line 533
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->executeDownload()V

    .line 535
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const/16 v12, 0xc8

    iput v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    .line 536
    const/4 v11, 0x1

    invoke-static {v11}, Landroid/net/TrafficStats;->incrementOperationCount(I)V

    .line 539
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    const-wide/16 v14, -0x1

    cmp-long v11, v12, v14

    if-nez v11, :cond_3

    .line 540
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-wide v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    iput-wide v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J
    :try_end_0
    .catch Lcom/android/providers/downloads/StopRequestException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 592
    :cond_3
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Finished with status "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v12}, Landroid/provider/Downloads$Impl;->statusToString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/providers/downloads/DownloadThread;->logDebug(Ljava/lang/String;)V

    .line 594
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/providers/downloads/DownloadThread;->mId:J

    const-wide/16 v14, 0x0

    invoke-virtual {v11, v12, v13, v14, v15}, Lcom/android/providers/downloads/DownloadNotifier;->notifyDownloadSpeed(JJ)V

    .line 596
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->finalizeDestination()V

    .line 598
    const-string v11, "DownloadManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "MIME Type = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v13, v13, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    if-eqz v11, :cond_6

    .line 600
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    const/16 v12, 0xc8

    if-ne v11, v12, :cond_6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    const-string v12, "application/vnd.oma.drm.message"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    const-string v12, "application/vnd.oma.drm.content"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 602
    :cond_4
    const-string v11, "DownloadManager"

    const-string v12, "DRM Converting."

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/providers/downloads/DownloadThread;->InstallDrmMessage(Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;)Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-result-object v9

    .line 604
    .local v9, "tempState":Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    const/16 v12, 0xc8

    if-eq v11, v12, :cond_5

    .line 605
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    if-eqz v11, :cond_5

    .line 606
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    invoke-direct {v2, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 607
    .local v2, "DrmFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 608
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 612
    .end local v2    # "DrmFile":Ljava/io/File;
    :cond_5
    if-eqz v9, :cond_6

    .line 613
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v12, v9, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    iput-object v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    .line 614
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v12, v9, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    iput-object v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    .line 623
    .end local v9    # "tempState":Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->finishCACDownload()V

    .line 625
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    const/16 v12, 0x193

    if-ne v11, v12, :cond_8

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/android/providers/downloads/DownloadThread;->mCAC:Z

    if-nez v11, :cond_8

    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->CACAvaiable()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 626
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/android/providers/downloads/DownloadCAC;->getInstance(Landroid/content/Context;)Lcom/android/providers/downloads/DownloadCAC;

    move-result-object v5

    .line 627
    .local v5, "mCACManager":Lcom/android/providers/downloads/DownloadCAC;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/providers/downloads/DownloadThread;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    invoke-virtual {v5, v11, v12, v13, v14}, Lcom/android/providers/downloads/DownloadCAC;->addDownload(Landroid/content/Context;Lcom/android/providers/downloads/DownloadInfo;Lcom/android/providers/downloads/SystemFacade;Lcom/android/providers/downloads/DownloadNotifier;)V

    .line 628
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/android/providers/downloads/DownloadThread;->mSkipDequeueDownload:Z

    .line 629
    const-string v11, "DownloadManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Try CAC for download id = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v14, v13, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    .end local v5    # "mCACManager":Lcom/android/providers/downloads/DownloadCAC;
    :goto_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v11}, Landroid/provider/Downloads$Impl;->isStatusCompleted(I)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 636
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    invoke-virtual {v11}, Lcom/android/providers/downloads/DownloadInfo;->sendIntentIfRequested()V

    .line 639
    :cond_7
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 640
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 643
    const-string v11, "DownloadManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Download "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v14, v13, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " finished with status "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v13, v13, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v13}, Landroid/provider/Downloads$Impl;->statusToString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mPolicyListener:Landroid/net/INetworkPolicyListener;

    invoke-virtual {v6, v11}, Landroid/net/NetworkPolicyManager;->unregisterListener(Landroid/net/INetworkPolicyListener;)V

    .line 648
    if-eqz v10, :cond_0

    .line 649
    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 650
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 633
    :cond_8
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual {v11}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->writeToDatabase()V

    goto :goto_1

    .line 543
    .end local v4    # "info":Landroid/net/NetworkInfo;
    :catch_0
    move-exception v3

    .line 544
    .local v3, "e":Lcom/android/providers/downloads/StopRequestException;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual {v3}, Lcom/android/providers/downloads/StopRequestException;->getFinalStatus()I

    move-result v12

    iput v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    .line 545
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual {v3}, Lcom/android/providers/downloads/StopRequestException;->getMessage()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mErrorMsg:Ljava/lang/String;

    .line 547
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Stop requested with status "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v12}, Landroid/provider/Downloads$Impl;->statusToString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ": "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/providers/downloads/DownloadThread;->logWarning(Ljava/lang/String;)V

    .line 553
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    const/16 v12, 0xc2

    if-ne v11, v12, :cond_e

    .line 554
    new-instance v11, Ljava/lang/IllegalStateException;

    const-string v12, "Execution should always throw final error codes"

    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 592
    .end local v3    # "e":Lcom/android/providers/downloads/StopRequestException;
    :catchall_0
    move-exception v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Finished with status "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v13, v13, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v13}, Landroid/provider/Downloads$Impl;->statusToString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/providers/downloads/DownloadThread;->logDebug(Ljava/lang/String;)V

    .line 594
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/providers/downloads/DownloadThread;->mId:J

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-virtual {v12, v14, v15, v0, v1}, Lcom/android/providers/downloads/DownloadNotifier;->notifyDownloadSpeed(JJ)V

    .line 596
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->finalizeDestination()V

    .line 598
    const-string v12, "DownloadManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "MIME Type = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v14, v14, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    if-eqz v12, :cond_b

    .line 600
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    const/16 v13, 0xc8

    if-ne v12, v13, :cond_b

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    const-string v13, "application/vnd.oma.drm.message"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_9

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    const-string v13, "application/vnd.oma.drm.content"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 602
    :cond_9
    const-string v12, "DownloadManager"

    const-string v13, "DRM Converting."

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/providers/downloads/DownloadThread;->InstallDrmMessage(Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;)Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-result-object v9

    .line 604
    .restart local v9    # "tempState":Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    const/16 v13, 0xc8

    if-eq v12, v13, :cond_a

    .line 605
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    if-eqz v12, :cond_a

    .line 606
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    invoke-direct {v2, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 607
    .restart local v2    # "DrmFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_a

    .line 608
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 612
    .end local v2    # "DrmFile":Ljava/io/File;
    :cond_a
    if-eqz v9, :cond_b

    .line 613
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v13, v9, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    iput-object v13, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    .line 614
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v13, v9, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    iput-object v13, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    .line 623
    .end local v9    # "tempState":Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->finishCACDownload()V

    .line 625
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    const/16 v13, 0x193

    if-ne v12, v13, :cond_1c

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/android/providers/downloads/DownloadThread;->mCAC:Z

    if-nez v12, :cond_1c

    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->CACAvaiable()Z

    move-result v12

    if-eqz v12, :cond_1c

    .line 626
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/android/providers/downloads/DownloadCAC;->getInstance(Landroid/content/Context;)Lcom/android/providers/downloads/DownloadCAC;

    move-result-object v5

    .line 627
    .restart local v5    # "mCACManager":Lcom/android/providers/downloads/DownloadCAC;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/providers/downloads/DownloadThread;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/providers/downloads/DownloadThread;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    invoke-virtual {v5, v12, v13, v14, v15}, Lcom/android/providers/downloads/DownloadCAC;->addDownload(Landroid/content/Context;Lcom/android/providers/downloads/DownloadInfo;Lcom/android/providers/downloads/SystemFacade;Lcom/android/providers/downloads/DownloadNotifier;)V

    .line 628
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/android/providers/downloads/DownloadThread;->mSkipDequeueDownload:Z

    .line 629
    const-string v12, "DownloadManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Try CAC for download id = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v14, v14, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    .end local v5    # "mCACManager":Lcom/android/providers/downloads/DownloadCAC;
    :goto_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v12}, Landroid/provider/Downloads$Impl;->isStatusCompleted(I)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 636
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    invoke-virtual {v12}, Lcom/android/providers/downloads/DownloadInfo;->sendIntentIfRequested()V

    .line 639
    :cond_c
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 640
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 643
    const-string v12, "DownloadManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Download "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v14, v14, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " finished with status "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v14, v14, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v14}, Landroid/provider/Downloads$Impl;->statusToString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mPolicyListener:Landroid/net/INetworkPolicyListener;

    invoke-virtual {v6, v12}, Landroid/net/NetworkPolicyManager;->unregisterListener(Landroid/net/INetworkPolicyListener;)V

    .line 648
    if-eqz v10, :cond_d

    .line 649
    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 650
    const/4 v10, 0x0

    :cond_d
    throw v11

    .line 558
    .restart local v3    # "e":Lcom/android/providers/downloads/StopRequestException;
    :cond_e
    :try_start_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v11}, Lcom/android/providers/downloads/DownloadThread;->isStatusRetryable(I)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 559
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/android/providers/downloads/DownloadThread;->mMadeProgress:Z

    if-eqz v11, :cond_14

    .line 560
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const/4 v12, 0x1

    iput v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mNumFailed:I

    .line 565
    :goto_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mNumFailed:I

    const/4 v12, 0x5

    if-ge v11, v12, :cond_f

    .line 566
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v12, v12, Lcom/android/providers/downloads/DownloadInfo;->mUid:I

    invoke-interface {v11, v12}, Lcom/android/providers/downloads/SystemFacade;->getActiveNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 567
    .restart local v4    # "info":Landroid/net/NetworkInfo;
    if-eqz v4, :cond_15

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/providers/downloads/DownloadThread;->mNetworkType:I

    if-ne v11, v12, :cond_15

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v11

    if-eqz v11, :cond_15

    .line 569
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const/16 v12, 0xc2

    iput v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    .line 577
    :goto_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-static {v11}, Lcom/android/providers/downloads/DownloadDrmHelper;->isDrmConvertNeeded(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 580
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const/16 v12, 0x1e9

    iput v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 592
    .end local v4    # "info":Landroid/net/NetworkInfo;
    :cond_f
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Finished with status "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v12}, Landroid/provider/Downloads$Impl;->statusToString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/providers/downloads/DownloadThread;->logDebug(Ljava/lang/String;)V

    .line 594
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/providers/downloads/DownloadThread;->mId:J

    const-wide/16 v14, 0x0

    invoke-virtual {v11, v12, v13, v14, v15}, Lcom/android/providers/downloads/DownloadNotifier;->notifyDownloadSpeed(JJ)V

    .line 596
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->finalizeDestination()V

    .line 598
    const-string v11, "DownloadManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "MIME Type = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v13, v13, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    if-eqz v11, :cond_12

    .line 600
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    const/16 v12, 0xc8

    if-ne v11, v12, :cond_12

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    const-string v12, "application/vnd.oma.drm.message"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    const-string v12, "application/vnd.oma.drm.content"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 602
    :cond_10
    const-string v11, "DownloadManager"

    const-string v12, "DRM Converting."

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/providers/downloads/DownloadThread;->InstallDrmMessage(Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;)Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-result-object v9

    .line 604
    .restart local v9    # "tempState":Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    const/16 v12, 0xc8

    if-eq v11, v12, :cond_11

    .line 605
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    if-eqz v11, :cond_11

    .line 606
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    invoke-direct {v2, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 607
    .restart local v2    # "DrmFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_11

    .line 608
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 612
    .end local v2    # "DrmFile":Ljava/io/File;
    :cond_11
    if-eqz v9, :cond_12

    .line 613
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v12, v9, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    iput-object v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    .line 614
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v12, v9, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    iput-object v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    .line 623
    .end local v9    # "tempState":Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    :cond_12
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->finishCACDownload()V

    .line 625
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    const/16 v12, 0x193

    if-ne v11, v12, :cond_16

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/android/providers/downloads/DownloadThread;->mCAC:Z

    if-nez v11, :cond_16

    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->CACAvaiable()Z

    move-result v11

    if-eqz v11, :cond_16

    .line 626
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/android/providers/downloads/DownloadCAC;->getInstance(Landroid/content/Context;)Lcom/android/providers/downloads/DownloadCAC;

    move-result-object v5

    .line 627
    .restart local v5    # "mCACManager":Lcom/android/providers/downloads/DownloadCAC;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/providers/downloads/DownloadThread;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    invoke-virtual {v5, v11, v12, v13, v14}, Lcom/android/providers/downloads/DownloadCAC;->addDownload(Landroid/content/Context;Lcom/android/providers/downloads/DownloadInfo;Lcom/android/providers/downloads/SystemFacade;Lcom/android/providers/downloads/DownloadNotifier;)V

    .line 628
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/android/providers/downloads/DownloadThread;->mSkipDequeueDownload:Z

    .line 629
    const-string v11, "DownloadManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Try CAC for download id = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v14, v13, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    .end local v5    # "mCACManager":Lcom/android/providers/downloads/DownloadCAC;
    :goto_5
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v11}, Landroid/provider/Downloads$Impl;->isStatusCompleted(I)Z

    move-result v11

    if-eqz v11, :cond_13

    .line 636
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    invoke-virtual {v11}, Lcom/android/providers/downloads/DownloadInfo;->sendIntentIfRequested()V

    .line 639
    :cond_13
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 640
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 643
    const-string v11, "DownloadManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Download "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v14, v13, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " finished with status "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v13, v13, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v13}, Landroid/provider/Downloads$Impl;->statusToString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mPolicyListener:Landroid/net/INetworkPolicyListener;

    invoke-virtual {v6, v11}, Landroid/net/NetworkPolicyManager;->unregisterListener(Landroid/net/INetworkPolicyListener;)V

    .line 648
    if-eqz v10, :cond_0

    .line 649
    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 650
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 562
    :cond_14
    :try_start_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mNumFailed:I

    add-int/lit8 v12, v12, 0x1

    iput v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mNumFailed:I

    goto/16 :goto_3

    .line 572
    .restart local v4    # "info":Landroid/net/NetworkInfo;
    :cond_15
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const/16 v12, 0xc3

    iput v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_4

    .line 633
    .end local v4    # "info":Landroid/net/NetworkInfo;
    :cond_16
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual {v11}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->writeToDatabase()V

    goto :goto_5

    .line 585
    .end local v3    # "e":Lcom/android/providers/downloads/StopRequestException;
    :catch_1
    move-exception v8

    .line 586
    .local v8, "t":Ljava/lang/Throwable;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    const/16 v12, 0x1eb

    iput v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    .line 587
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual {v8}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mErrorMsg:Ljava/lang/String;

    .line 589
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v8}, Lcom/android/providers/downloads/DownloadThread;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 592
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Finished with status "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v12, v12, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v12}, Landroid/provider/Downloads$Impl;->statusToString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/providers/downloads/DownloadThread;->logDebug(Ljava/lang/String;)V

    .line 594
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/providers/downloads/DownloadThread;->mId:J

    const-wide/16 v14, 0x0

    invoke-virtual {v11, v12, v13, v14, v15}, Lcom/android/providers/downloads/DownloadNotifier;->notifyDownloadSpeed(JJ)V

    .line 596
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->finalizeDestination()V

    .line 598
    const-string v11, "DownloadManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "MIME Type = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v13, v13, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    if-eqz v11, :cond_19

    .line 600
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    const/16 v12, 0xc8

    if-ne v11, v12, :cond_19

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    const-string v12, "application/vnd.oma.drm.message"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_17

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    const-string v12, "application/vnd.oma.drm.content"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_19

    .line 602
    :cond_17
    const-string v11, "DownloadManager"

    const-string v12, "DRM Converting."

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/providers/downloads/DownloadThread;->InstallDrmMessage(Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;)Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-result-object v9

    .line 604
    .restart local v9    # "tempState":Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    const/16 v12, 0xc8

    if-eq v11, v12, :cond_18

    .line 605
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    if-eqz v11, :cond_18

    .line 606
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    invoke-direct {v2, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 607
    .restart local v2    # "DrmFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_18

    .line 608
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 612
    .end local v2    # "DrmFile":Ljava/io/File;
    :cond_18
    if-eqz v9, :cond_19

    .line 613
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v12, v9, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    iput-object v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    .line 614
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v12, v9, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    iput-object v12, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    .line 623
    .end local v9    # "tempState":Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    :cond_19
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->finishCACDownload()V

    .line 625
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    const/16 v12, 0x193

    if-ne v11, v12, :cond_1b

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/android/providers/downloads/DownloadThread;->mCAC:Z

    if-nez v11, :cond_1b

    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadThread;->CACAvaiable()Z

    move-result v11

    if-eqz v11, :cond_1b

    .line 626
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/android/providers/downloads/DownloadCAC;->getInstance(Landroid/content/Context;)Lcom/android/providers/downloads/DownloadCAC;

    move-result-object v5

    .line 627
    .restart local v5    # "mCACManager":Lcom/android/providers/downloads/DownloadCAC;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/providers/downloads/DownloadThread;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    invoke-virtual {v5, v11, v12, v13, v14}, Lcom/android/providers/downloads/DownloadCAC;->addDownload(Landroid/content/Context;Lcom/android/providers/downloads/DownloadInfo;Lcom/android/providers/downloads/SystemFacade;Lcom/android/providers/downloads/DownloadNotifier;)V

    .line 628
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/android/providers/downloads/DownloadThread;->mSkipDequeueDownload:Z

    .line 629
    const-string v11, "DownloadManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Try CAC for download id = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v14, v13, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    .end local v5    # "mCACManager":Lcom/android/providers/downloads/DownloadCAC;
    :goto_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v11, v11, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v11}, Landroid/provider/Downloads$Impl;->isStatusCompleted(I)Z

    move-result v11

    if-eqz v11, :cond_1a

    .line 636
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    invoke-virtual {v11}, Lcom/android/providers/downloads/DownloadInfo;->sendIntentIfRequested()V

    .line 639
    :cond_1a
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 640
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 643
    const-string v11, "DownloadManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Download "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-wide v14, v13, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " finished with status "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget v13, v13, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v13}, Landroid/provider/Downloads$Impl;->statusToString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mPolicyListener:Landroid/net/INetworkPolicyListener;

    invoke-virtual {v6, v11}, Landroid/net/NetworkPolicyManager;->unregisterListener(Landroid/net/INetworkPolicyListener;)V

    .line 648
    if-eqz v10, :cond_0

    .line 649
    invoke-virtual {v10}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 650
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 633
    :cond_1b
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual {v11}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->writeToDatabase()V

    goto :goto_6

    .end local v8    # "t":Ljava/lang/Throwable;
    :cond_1c
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/providers/downloads/DownloadThread;->mInfoDelta:Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    invoke-virtual {v12}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->writeToDatabase()V

    goto/16 :goto_2
.end method
