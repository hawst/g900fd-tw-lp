.class public Lcom/android/providers/downloads/OpenHelper;
.super Ljava/lang/Object;
.source "OpenHelper.java"


# direct methods
.method private static buildViewIntent(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    .line 67
    const-string v15, "download"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/DownloadManager;

    .line 69
    .local v3, "downManager":Landroid/app/DownloadManager;
    const/4 v15, 0x1

    invoke-virtual {v3, v15}, Landroid/app/DownloadManager;->setAccessAllDownloads(Z)V

    .line 71
    new-instance v15, Landroid/app/DownloadManager$Query;

    invoke-direct {v15}, Landroid/app/DownloadManager$Query;-><init>()V

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-wide p1, v16, v17

    invoke-virtual/range {v15 .. v16}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    move-result-object v15

    invoke-virtual {v3, v15}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v2

    .line 73
    .local v2, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v15

    if-nez v15, :cond_0

    .line 74
    const/4 v7, 0x0

    .line 141
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v7

    .line 77
    :cond_0
    :try_start_1
    const-string v15, "local_uri"

    invoke-static {v2, v15}, Lcom/android/providers/downloads/OpenHelper;->getCursorUri(Landroid/database/Cursor;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 78
    .local v8, "localUri":Landroid/net/Uri;
    const-string v15, "local_filename"

    invoke-static {v2, v15}, Lcom/android/providers/downloads/OpenHelper;->getCursorFile(Landroid/database/Cursor;Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    .line 79
    .local v5, "file":Ljava/io/File;
    const-string v15, "media_type"

    invoke-static {v2, v15}, Lcom/android/providers/downloads/OpenHelper;->getCursorString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 80
    .local v12, "mimeType":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-static {v0, v5, v12}, Lcom/android/providers/downloads/DownloadDrmHelper;->getOriginalMimeType(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 82
    new-instance v7, Landroid/content/Intent;

    const-string v15, "android.intent.action.VIEW"

    invoke-direct {v7, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 84
    .local v7, "intent":Landroid/content/Intent;
    const-string v15, "application/vnd.android.package-archive"

    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 86
    invoke-virtual {v7, v8, v12}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    const-string v15, "uri"

    invoke-static {v2, v15}, Lcom/android/providers/downloads/OpenHelper;->getCursorUri(Landroid/database/Cursor;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 90
    .local v14, "remoteUri":Landroid/net/Uri;
    const-string v15, "android.intent.extra.ORIGINATING_URI"

    invoke-virtual {v7, v15, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 91
    const-string v15, "android.intent.extra.REFERRER"

    invoke-static/range {p0 .. p2}, Lcom/android/providers/downloads/OpenHelper;->getRefererUri(Landroid/content/Context;J)Landroid/net/Uri;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 92
    const-string v15, "android.intent.extra.ORIGINATING_UID"

    invoke-static/range {p0 .. p2}, Lcom/android/providers/downloads/OpenHelper;->getOriginatingUid(Landroid/content/Context;J)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 141
    .end local v14    # "remoteUri":Landroid/net/Uri;
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 93
    :cond_1
    :try_start_2
    const-string v15, "file"

    invoke-virtual {v8}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 94
    const/4 v15, 0x3

    invoke-virtual {v7, v15}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 96
    if-eqz v12, :cond_2

    const-string v15, "image/"

    invoke-virtual {v12, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 97
    :cond_2
    const-string v15, "mediaprovider_uri"

    invoke-interface {v2, v15}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    invoke-interface {v2, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 100
    .local v9, "mediaProviderString":Ljava/lang/String;
    if-eqz v9, :cond_5

    .line 101
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 102
    .local v10, "mediaProviderUri":Landroid/net/Uri;
    if-eqz v10, :cond_4

    .line 103
    sget-boolean v15, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v15, :cond_3

    .line 104
    const-string v15, "DownloadManager"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "openCurrentDownload() mediaProviderUri = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :cond_3
    invoke-virtual {v7, v10, v12}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 141
    .end local v5    # "file":Ljava/io/File;
    .end local v7    # "intent":Landroid/content/Intent;
    .end local v8    # "localUri":Landroid/net/Uri;
    .end local v9    # "mediaProviderString":Ljava/lang/String;
    .end local v10    # "mediaProviderUri":Landroid/net/Uri;
    .end local v12    # "mimeType":Ljava/lang/String;
    :catchall_0
    move-exception v15

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v15

    .line 108
    .restart local v5    # "file":Ljava/io/File;
    .restart local v7    # "intent":Landroid/content/Intent;
    .restart local v8    # "localUri":Landroid/net/Uri;
    .restart local v9    # "mediaProviderString":Ljava/lang/String;
    .restart local v10    # "mediaProviderUri":Landroid/net/Uri;
    .restart local v12    # "mimeType":Ljava/lang/String;
    :cond_4
    :try_start_3
    sget-object v15, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, p1

    invoke-static {v15, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v7, v15, v12}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 112
    .end local v10    # "mediaProviderUri":Landroid/net/Uri;
    :cond_5
    sget-object v15, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, p1

    invoke-static {v15, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v7, v15, v12}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 115
    .end local v9    # "mediaProviderString":Ljava/lang/String;
    :cond_6
    const-string v15, "0"

    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_7

    const-string v15, "application/"

    invoke-virtual {v12, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_9

    :cond_7
    if-eqz v5, :cond_9

    .line 116
    const/4 v13, 0x0

    .line 117
    .local v13, "newMimeType":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 118
    .local v6, "fileName":Ljava/lang/String;
    invoke-static {v6}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 120
    .local v4, "extensionFromeFileName":Ljava/lang/String;
    if-eqz v4, :cond_8

    .line 121
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v11

    .line 122
    .local v11, "mime":Landroid/webkit/MimeTypeMap;
    invoke-virtual {v11, v4}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 123
    if-eqz v13, :cond_8

    .line 124
    move-object v12, v13

    .line 125
    const-string v15, "DownloadManager"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "newMimeType from extention : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    .end local v11    # "mime":Landroid/webkit/MimeTypeMap;
    :cond_8
    invoke-virtual {v7, v8, v12}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 131
    .end local v4    # "extensionFromeFileName":Ljava/lang/String;
    .end local v6    # "fileName":Ljava/lang/String;
    .end local v13    # "newMimeType":Ljava/lang/String;
    :cond_9
    invoke-virtual {v7, v8, v12}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 135
    :cond_a
    const/4 v15, 0x1

    invoke-virtual {v7, v15}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 136
    invoke-virtual {v7, v8, v12}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1
.end method

.method private static getCursorFile(Landroid/database/Cursor;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "column"    # Ljava/lang/String;

    .prologue
    .line 189
    new-instance v0, Ljava/io/File;

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static getCursorString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "column"    # Ljava/lang/String;

    .prologue
    .line 181
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getCursorUri(Landroid/database/Cursor;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "column"    # Ljava/lang/String;

    .prologue
    .line 185
    invoke-static {p0, p1}, Lcom/android/providers/downloads/OpenHelper;->getCursorString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getOriginatingUid(Landroid/content/Context;J)I
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    const/4 v3, 0x0

    .line 165
    sget-object v0, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 166
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "uid"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 168
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 170
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    const-string v0, "uid"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 174
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 177
    :goto_0
    return v0

    .line 174
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 177
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 174
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getRefererUri(Landroid/content/Context;J)Landroid/net/Uri;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 146
    sget-object v0, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    const-string v3, "headers"

    invoke-static {v0, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 149
    .local v1, "headersUri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 152
    .local v7, "headers":Landroid/database/Cursor;
    :cond_0
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    const-string v0, "header"

    invoke-static {v7, v0}, Lcom/android/providers/downloads/OpenHelper;->getCursorString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 154
    .local v6, "header":Ljava/lang/String;
    const-string v0, "Referer"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const-string v0, "value"

    invoke-static {v7, v0}, Lcom/android/providers/downloads/OpenHelper;->getCursorUri(Landroid/database/Cursor;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 159
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 161
    .end local v6    # "header":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 159
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static startViewIntent(Landroid/content/Context;JI)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .param p3, "intentFlags"    # I

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-static {p0, p1, p2}, Lcom/android/providers/downloads/OpenHelper;->buildViewIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v1

    .line 47
    .local v1, "intent":Landroid/content/Intent;
    if-nez v1, :cond_0

    .line 48
    const-string v3, "DownloadManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No intent built for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :goto_0
    return v2

    .line 52
    :cond_0
    invoke-virtual {v1, p3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 54
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    const/4 v2, 0x1

    goto :goto_0

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "DownloadManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to start "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
