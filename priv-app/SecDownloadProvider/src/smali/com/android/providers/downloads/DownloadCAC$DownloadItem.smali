.class Lcom/android/providers/downloads/DownloadCAC$DownloadItem;
.super Ljava/lang/Object;
.source "DownloadCAC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/downloads/DownloadCAC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadItem"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInfo:Lcom/android/providers/downloads/DownloadInfo;

.field private mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

.field private mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

.field final synthetic this$0:Lcom/android/providers/downloads/DownloadCAC;


# direct methods
.method public constructor <init>(Lcom/android/providers/downloads/DownloadCAC;Landroid/content/Context;Lcom/android/providers/downloads/SystemFacade;Lcom/android/providers/downloads/DownloadNotifier;Lcom/android/providers/downloads/DownloadInfo;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "systemFacade"    # Lcom/android/providers/downloads/SystemFacade;
    .param p4, "notifier"    # Lcom/android/providers/downloads/DownloadNotifier;
    .param p5, "info"    # Lcom/android/providers/downloads/DownloadInfo;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->this$0:Lcom/android/providers/downloads/DownloadCAC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p2, p0, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mContext:Landroid/content/Context;

    .line 72
    iput-object p3, p0, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    .line 73
    iput-object p4, p0, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    .line 74
    iput-object p5, p0, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    .line 75
    return-void
.end method

.method static synthetic access$300(Lcom/android/providers/downloads/DownloadCAC$DownloadItem;)Lcom/android/providers/downloads/DownloadInfo;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadCAC$DownloadItem;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    return-object v0
.end method


# virtual methods
.method public GetContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public GetInfo()Lcom/android/providers/downloads/DownloadInfo;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    return-object v0
.end method

.method public GetNotifier()Lcom/android/providers/downloads/DownloadNotifier;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    return-object v0
.end method

.method public GetSystemFacade()Lcom/android/providers/downloads/SystemFacade;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    return-object v0
.end method
