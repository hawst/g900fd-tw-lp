.class Lcom/android/providers/downloads/saveas/DownloadSaveAsContentObserver;
.super Landroid/database/ContentObserver;
.source "DownloadSaveasManager.java"


# instance fields
.field mSem:Ljava/util/concurrent/Semaphore;


# direct methods
.method constructor <init>(Ljava/util/concurrent/Semaphore;)V
    .locals 1
    .param p1, "sem"    # Ljava/util/concurrent/Semaphore;

    .prologue
    .line 746
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 747
    iput-object p1, p0, Lcom/android/providers/downloads/saveas/DownloadSaveAsContentObserver;->mSem:Ljava/util/concurrent/Semaphore;

    .line 748
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 750
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_0

    .line 751
    const-string v0, "DownloadManager"

    const-string v1, "DownloadSaveasManager : [saveas] DownloadSaveAsContentObserver on changed"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 753
    :cond_0
    iget-object v0, p0, Lcom/android/providers/downloads/saveas/DownloadSaveAsContentObserver;->mSem:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 754
    return-void
.end method
