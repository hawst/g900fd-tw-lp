.class public Lcom/android/providers/downloads/StorageUtils;
.super Ljava/lang/Object;
.source "StorageUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/downloads/StorageUtils$ObserverLatch;,
        Lcom/android/providers/downloads/StorageUtils$ConcreteFile;
    }
.end annotation


# static fields
.field static sForceFullEviction:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/providers/downloads/StorageUtils;->sForceFullEviction:Z

    return-void
.end method

.method public static ensureAvailableSpace(Landroid/content/Context;Ljava/io/FileDescriptor;J)V
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "bytes"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/providers/downloads/StopRequestException;
        }
    .end annotation

    .prologue
    .line 93
    invoke-static/range {p1 .. p1}, Lcom/android/providers/downloads/StorageUtils;->getAvailableBytes(Ljava/io/FileDescriptor;)J

    move-result-wide v4

    .line 94
    .local v4, "availBytes":J
    cmp-long v17, v4, p2

    if-ltz v17, :cond_1

    .line 138
    :cond_0
    return-void

    .line 103
    :cond_1
    :try_start_0
    invoke-static/range {p1 .. p1}, Landroid/system/Os;->fstat(Ljava/io/FileDescriptor;)Landroid/system/StructStat;

    move-result-object v17

    move-object/from16 v0, v17

    iget-wide v10, v0, Landroid/system/StructStat;->st_dev:J
    :try_end_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_1

    .line 108
    .local v10, "dev":J
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/android/providers/downloads/StorageUtils;->getDeviceId(Ljava/io/File;)J

    move-result-wide v8

    .line 109
    .local v8, "dataDev":J
    invoke-static {}, Landroid/os/Environment;->getDownloadCacheDirectory()Ljava/io/File;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/android/providers/downloads/StorageUtils;->getDeviceId(Ljava/io/File;)J

    move-result-wide v6

    .line 110
    .local v6, "cacheDev":J
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/android/providers/downloads/StorageUtils;->getDeviceId(Ljava/io/File;)J

    move-result-wide v14

    .line 112
    .local v14, "externalDev":J
    cmp-long v17, v10, v8

    if-eqz v17, :cond_2

    cmp-long v17, v10, v14

    if-nez v17, :cond_5

    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    move-result v17

    if-eqz v17, :cond_5

    .line 115
    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v16

    .line 116
    .local v16, "pm":Landroid/content/pm/PackageManager;
    new-instance v13, Lcom/android/providers/downloads/StorageUtils$ObserverLatch;

    invoke-direct {v13}, Lcom/android/providers/downloads/StorageUtils$ObserverLatch;-><init>()V

    .line 117
    .local v13, "observer":Lcom/android/providers/downloads/StorageUtils$ObserverLatch;
    sget-boolean v17, Lcom/android/providers/downloads/StorageUtils;->sForceFullEviction:Z

    if-eqz v17, :cond_4

    const-wide v18, 0x7fffffffffffffffL

    :goto_0
    move-object/from16 v0, v16

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2, v13}, Landroid/content/pm/PackageManager;->freeStorageAndNotify(JLandroid/content/pm/IPackageDataObserver;)V

    .line 120
    :try_start_1
    iget-object v0, v13, Lcom/android/providers/downloads/StorageUtils$ObserverLatch;->latch:Ljava/util/concurrent/CountDownLatch;

    move-object/from16 v17, v0

    const-wide/16 v18, 0x1e

    sget-object v20, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v17 .. v20}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v17

    if-nez v17, :cond_3

    .line 121
    new-instance v17, Ljava/io/IOException;

    const-string v18, "Timeout while freeing disk space"

    invoke-direct/range {v17 .. v18}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v17
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 123
    :catch_0
    move-exception v12

    .line 124
    .local v12, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Thread;->interrupt()V

    .line 133
    .end local v12    # "e":Ljava/lang/InterruptedException;
    .end local v13    # "observer":Lcom/android/providers/downloads/StorageUtils$ObserverLatch;
    .end local v16    # "pm":Landroid/content/pm/PackageManager;
    :cond_3
    :goto_1
    invoke-static/range {p1 .. p1}, Lcom/android/providers/downloads/StorageUtils;->getAvailableBytes(Ljava/io/FileDescriptor;)J

    move-result-wide v4

    .line 134
    cmp-long v17, v4, p2

    if-gez v17, :cond_0

    .line 135
    new-instance v17, Lcom/android/providers/downloads/StopRequestException;

    const/16 v18, 0xc6

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Not enough free space; "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " requested, "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " available"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v17 .. v19}, Lcom/android/providers/downloads/StopRequestException;-><init>(ILjava/lang/String;)V

    throw v17

    .line 104
    .end local v6    # "cacheDev":J
    .end local v8    # "dataDev":J
    .end local v10    # "dev":J
    .end local v14    # "externalDev":J
    :catch_1
    move-exception v12

    .line 105
    .local v12, "e":Landroid/system/ErrnoException;
    invoke-virtual {v12}, Landroid/system/ErrnoException;->rethrowAsIOException()Ljava/io/IOException;

    move-result-object v17

    throw v17

    .end local v12    # "e":Landroid/system/ErrnoException;
    .restart local v6    # "cacheDev":J
    .restart local v8    # "dataDev":J
    .restart local v10    # "dev":J
    .restart local v13    # "observer":Lcom/android/providers/downloads/StorageUtils$ObserverLatch;
    .restart local v14    # "externalDev":J
    .restart local v16    # "pm":Landroid/content/pm/PackageManager;
    :cond_4
    move-wide/from16 v18, p2

    .line 117
    goto :goto_0

    .line 127
    .end local v13    # "observer":Lcom/android/providers/downloads/StorageUtils$ObserverLatch;
    .end local v16    # "pm":Landroid/content/pm/PackageManager;
    :cond_5
    cmp-long v17, v10, v6

    if-nez v17, :cond_3

    .line 129
    invoke-static/range {p2 .. p3}, Lcom/android/providers/downloads/StorageUtils;->freeCacheStorage(J)V

    goto :goto_1
.end method

.method private static freeCacheStorage(J)V
    .locals 12
    .param p0, "bytes"    # J

    .prologue
    .line 147
    invoke-static {}, Landroid/os/Environment;->getDownloadCacheDirectory()Ljava/io/File;

    move-result-object v3

    const-string v8, "partial_downloads"

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v9

    invoke-static {v3, v8, v9}, Lcom/android/providers/downloads/StorageUtils;->listFilesRecursive(Ljava/io/File;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v1

    .line 151
    .local v1, "files":Ljava/util/List;, "Ljava/util/List<Lcom/android/providers/downloads/StorageUtils$ConcreteFile;>;"
    const-string v3, "DownloadManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Found "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " downloads on cache"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    new-instance v3, Lcom/android/providers/downloads/StorageUtils$1;

    invoke-direct {v3}, Lcom/android/providers/downloads/StorageUtils$1;-><init>()V

    invoke-static {v1, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 160
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 161
    .local v6, "now":J
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/providers/downloads/StorageUtils$ConcreteFile;

    .line 162
    .local v0, "file":Lcom/android/providers/downloads/StorageUtils$ConcreteFile;
    const-wide/16 v8, 0x0

    cmp-long v3, p0, v8

    if-gtz v3, :cond_1

    .line 173
    .end local v0    # "file":Lcom/android/providers/downloads/StorageUtils$ConcreteFile;
    :cond_0
    return-void

    .line 164
    .restart local v0    # "file":Lcom/android/providers/downloads/StorageUtils$ConcreteFile;
    :cond_1
    iget-object v3, v0, Lcom/android/providers/downloads/StorageUtils$ConcreteFile;->file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    sub-long v8, v6, v8

    const-wide/32 v10, 0x5265c00

    cmp-long v3, v8, v10

    if-gez v3, :cond_2

    .line 165
    const-string v3, "DownloadManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Skipping recently modified "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v0, Lcom/android/providers/downloads/StorageUtils$ConcreteFile;->file:Ljava/io/File;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 167
    :cond_2
    iget-object v3, v0, Lcom/android/providers/downloads/StorageUtils$ConcreteFile;->file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 168
    .local v4, "len":J
    const-string v3, "DownloadManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Deleting "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v0, Lcom/android/providers/downloads/StorageUtils$ConcreteFile;->file:Ljava/io/File;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to reclaim "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    sub-long/2addr p0, v4

    .line 170
    iget-object v3, v0, Lcom/android/providers/downloads/StorageUtils$ConcreteFile;->file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private static getAvailableBytes(Ljava/io/FileDescriptor;)J
    .locals 6
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    :try_start_0
    invoke-static {p0}, Landroid/system/Os;->fstatvfs(Ljava/io/FileDescriptor;)Landroid/system/StructStatVfs;

    move-result-object v1

    .line 182
    .local v1, "stat":Landroid/system/StructStatVfs;
    iget-wide v2, v1, Landroid/system/StructStatVfs;->f_bavail:J

    iget-wide v4, v1, Landroid/system/StructStatVfs;->f_bsize:J
    :try_end_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_0

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x2000000

    sub-long/2addr v2, v4

    return-wide v2

    .line 183
    .end local v1    # "stat":Landroid/system/StructStatVfs;
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Landroid/system/ErrnoException;
    invoke-virtual {v0}, Landroid/system/ErrnoException;->rethrowAsIOException()Ljava/io/IOException;

    move-result-object v2

    throw v2
.end method

.method private static getDeviceId(Ljava/io/File;)J
    .locals 4
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 190
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/system/Os;->stat(Ljava/lang/String;)Landroid/system/StructStat;

    move-result-object v1

    iget-wide v2, v1, Landroid/system/StructStat;->st_dev:J
    :try_end_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :goto_0
    return-wide v2

    .line 191
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Landroid/system/ErrnoException;
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method static listFilesRecursive(Ljava/io/File;Ljava/lang/String;I)Ljava/util/List;
    .locals 10
    .param p0, "startDir"    # Ljava/io/File;
    .param p1, "exclude"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/android/providers/downloads/StorageUtils$ConcreteFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 206
    .local v6, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/providers/downloads/StorageUtils$ConcreteFile;>;"
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 207
    .local v4, "dirs":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/io/File;>;"
    invoke-virtual {v4, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 208
    :cond_0
    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_4

    .line 209
    invoke-virtual {v4}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    .line 210
    .local v3, "dir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 212
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 213
    .local v2, "children":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 215
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_0

    aget-object v1, v0, v7

    .line 216
    .local v1, "child":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 217
    invoke-virtual {v4, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 215
    :cond_1
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 218
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 220
    :try_start_0
    new-instance v5, Lcom/android/providers/downloads/StorageUtils$ConcreteFile;

    invoke-direct {v5, v1}, Lcom/android/providers/downloads/StorageUtils$ConcreteFile;-><init>(Ljava/io/File;)V

    .line 221
    .local v5, "file":Lcom/android/providers/downloads/StorageUtils$ConcreteFile;
    const/4 v9, -0x1

    if-eq p2, v9, :cond_3

    iget-object v9, v5, Lcom/android/providers/downloads/StorageUtils$ConcreteFile;->stat:Landroid/system/StructStat;

    iget v9, v9, Landroid/system/StructStat;->st_uid:I

    if-ne v9, p2, :cond_1

    .line 222
    :cond_3
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 224
    .end local v5    # "file":Lcom/android/providers/downloads/StorageUtils$ConcreteFile;
    :catch_0
    move-exception v9

    goto :goto_1

    .line 229
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "child":Ljava/io/File;
    .end local v2    # "children":[Ljava/io/File;
    .end local v3    # "dir":Ljava/io/File;
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    :cond_4
    return-object v6
.end method
