.class public Lcom/android/providers/downloads/DownloadNotifier;
.super Ljava/lang/Object;
.source "DownloadNotifier.java"


# instance fields
.field private final mActiveNotifs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mDownloadSpeed:Landroid/util/LongSparseLongArray;

.field private final mDownloadTouch:Landroid/util/LongSparseLongArray;

.field private final mNotifManager:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadNotifier;->mActiveNotifs:Ljava/util/HashMap;

    .line 81
    new-instance v0, Landroid/util/LongSparseLongArray;

    invoke-direct {v0}, Landroid/util/LongSparseLongArray;-><init>()V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadSpeed:Landroid/util/LongSparseLongArray;

    .line 88
    new-instance v0, Landroid/util/LongSparseLongArray;

    invoke-direct {v0}, Landroid/util/LongSparseLongArray;-><init>()V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadTouch:Landroid/util/LongSparseLongArray;

    .line 92
    iput-object p1, p0, Lcom/android/providers/downloads/DownloadNotifier;->mContext:Landroid/content/Context;

    .line 93
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadNotifier;->mNotifManager:Landroid/app/NotificationManager;

    .line 95
    return-void
.end method

.method private static buildNotificationTag(Lcom/android/providers/downloads/DownloadInfo;)Ljava/lang/String;
    .locals 4
    .param p0, "info"    # Lcom/android/providers/downloads/DownloadInfo;

    .prologue
    .line 370
    iget v0, p0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    const/16 v1, 0xc4

    if-ne v0, v1, :cond_0

    .line 371
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadInfo;->mPackage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 378
    :goto_0
    return-object v0

    .line 372
    :cond_0
    invoke-static {p0}, Lcom/android/providers/downloads/DownloadNotifier;->isActiveAndVisible(Lcom/android/providers/downloads/DownloadInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "1:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadInfo;->mPackage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 374
    :cond_1
    invoke-static {p0}, Lcom/android/providers/downloads/DownloadNotifier;->isCompleteAndVisible(Lcom/android/providers/downloads/DownloadInfo;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "3:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 378
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getDownloadIds(Ljava/util/Collection;)[J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/providers/downloads/DownloadInfo;",
            ">;)[J"
        }
    .end annotation

    .prologue
    .line 346
    .local p1, "infos":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/providers/downloads/DownloadInfo;>;"
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v5

    new-array v3, v5, [J

    .line 347
    .local v3, "ids":[J
    const/4 v0, 0x0

    .line 348
    .local v0, "i":I
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/providers/downloads/DownloadInfo;

    .line 349
    .local v4, "info":Lcom/android/providers/downloads/DownloadInfo;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    iget-wide v6, v4, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    aput-wide v6, v3, v0

    move v0, v1

    .line 350
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 351
    .end local v4    # "info":Lcom/android/providers/downloads/DownloadInfo;
    :cond_0
    return-object v3
.end method

.method private static getDownloadTitle(Landroid/content/res/Resources;Lcom/android/providers/downloads/DownloadInfo;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "info"    # Lcom/android/providers/downloads/DownloadInfo;

    .prologue
    .line 338
    iget-object v0, p1, Lcom/android/providers/downloads/DownloadInfo;->mTitle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    iget-object v0, p1, Lcom/android/providers/downloads/DownloadInfo;->mTitle:Ljava/lang/String;

    .line 341
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f04000f

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getNotificationTagType(Ljava/lang/String;)I
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;

    .prologue
    .line 387
    const/4 v0, 0x0

    const/16 v1, 0x3a

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static isActiveAndVisible(Lcom/android/providers/downloads/DownloadInfo;)Z
    .locals 3
    .param p0, "download"    # Lcom/android/providers/downloads/DownloadInfo;

    .prologue
    const/4 v0, 0x1

    .line 391
    iget v1, p0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    const/16 v2, 0xc0

    if-ne v1, v2, :cond_1

    iget v1, p0, Lcom/android/providers/downloads/DownloadInfo;->mVisibility:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/providers/downloads/DownloadInfo;->mVisibility:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isCompleteAndVisible(Lcom/android/providers/downloads/DownloadInfo;)Z
    .locals 3
    .param p0, "download"    # Lcom/android/providers/downloads/DownloadInfo;

    .prologue
    const/4 v0, 0x1

    .line 397
    iget v1, p0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    invoke-static {v1}, Landroid/provider/Downloads$Impl;->isStatusCompleted(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    const/16 v2, 0x1ea

    if-ne v1, v2, :cond_1

    :cond_0
    iget v1, p0, Lcom/android/providers/downloads/DownloadInfo;->mState:I

    const/16 v2, 0x9

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/android/providers/downloads/DownloadInfo;->mState:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_2

    :cond_1
    iget v1, p0, Lcom/android/providers/downloads/DownloadInfo;->mVisibility:I

    if-ne v1, v0, :cond_2

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateWithLocked(Ljava/util/Collection;)V
    .locals 46
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/providers/downloads/DownloadInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 128
    .local p1, "downloads":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/providers/downloads/DownloadInfo;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mContext:Landroid/content/Context;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    .line 131
    .local v31, "res":Landroid/content/res/Resources;
    invoke-static {}, Lcom/google/common/collect/ArrayListMultimap;->create()Lcom/google/common/collect/ArrayListMultimap;

    move-result-object v12

    .line 132
    .local v12, "clustered":Lcom/google/common/collect/Multimap;, "Lcom/google/common/collect/Multimap<Ljava/lang/String;Lcom/android/providers/downloads/DownloadInfo;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v39

    if-eqz v39, :cond_1

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/providers/downloads/DownloadInfo;

    .line 133
    .local v22, "info":Lcom/android/providers/downloads/DownloadInfo;
    invoke-static/range {v22 .. v22}, Lcom/android/providers/downloads/DownloadNotifier;->buildNotificationTag(Lcom/android/providers/downloads/DownloadInfo;)Ljava/lang/String;

    move-result-object v34

    .line 134
    .local v34, "tag":Ljava/lang/String;
    if-eqz v34, :cond_0

    move-object/from16 v0, v22

    iget-boolean v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mDeleted:Z

    move/from16 v39, v0

    if-nez v39, :cond_0

    .line 135
    move-object/from16 v0, v34

    move-object/from16 v1, v22

    invoke-interface {v12, v0, v1}, Lcom/google/common/collect/Multimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 140
    .end local v22    # "info":Lcom/android/providers/downloads/DownloadInfo;
    .end local v34    # "tag":Ljava/lang/String;
    :cond_1
    invoke-interface {v12}, Lcom/google/common/collect/Multimap;->keySet()Ljava/util/Set;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .end local v19    # "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v39

    if-eqz v39, :cond_1d

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    .line 141
    .restart local v34    # "tag":Ljava/lang/String;
    invoke-static/range {v34 .. v34}, Lcom/android/providers/downloads/DownloadNotifier;->getNotificationTagType(Ljava/lang/String;)I

    move-result v35

    .line 142
    .local v35, "type":I
    move-object/from16 v0, v34

    invoke-interface {v12, v0}, Lcom/google/common/collect/Multimap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v11

    .line 144
    .local v11, "cluster":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/providers/downloads/DownloadInfo;>;"
    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/providers/downloads/DownloadInfo;

    .line 145
    .local v13, "downInfo":Lcom/android/providers/downloads/DownloadInfo;
    iget-boolean v0, v13, Lcom/android/providers/downloads/DownloadInfo;->mDownloadCompleteNotified:Z

    move/from16 v39, v0

    if-nez v39, :cond_2

    .line 148
    new-instance v9, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mContext:Landroid/content/Context;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-direct {v9, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 152
    .local v9, "builder":Landroid/app/Notification$Builder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mActiveNotifs:Ljava/util/HashMap;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_7

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mActiveNotifs:Ljava/util/HashMap;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/lang/Long;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 158
    .local v16, "firstShown":J
    :goto_2
    move-wide/from16 v0, v16

    invoke-virtual {v9, v0, v1}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 161
    const/16 v39, 0x1

    move/from16 v0, v35

    move/from16 v1, v39

    if-ne v0, v1, :cond_8

    .line 162
    const/high16 v39, 0x7f020000

    move/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 170
    :cond_3
    :goto_3
    const/16 v39, 0x1

    move/from16 v0, v35

    move/from16 v1, v39

    if-eq v0, v1, :cond_4

    const/16 v39, 0x2

    move/from16 v0, v35

    move/from16 v1, v39

    if-ne v0, v1, :cond_a

    .line 172
    :cond_4
    new-instance v39, Landroid/net/Uri$Builder;

    invoke-direct/range {v39 .. v39}, Landroid/net/Uri$Builder;-><init>()V

    const-string v40, "active-dl"

    invoke-virtual/range {v39 .. v40}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v38

    .line 173
    .local v38, "uri":Landroid/net/Uri;
    new-instance v23, Landroid/content/Intent;

    const-string v39, "android.intent.action.DOWNLOAD_LIST"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mContext:Landroid/content/Context;

    move-object/from16 v40, v0

    const-class v41, Lcom/android/providers/downloads/DownloadReceiver;

    move-object/from16 v0, v23

    move-object/from16 v1, v39

    move-object/from16 v2, v38

    move-object/from16 v3, v40

    move-object/from16 v4, v41

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 184
    .local v23, "intent":Landroid/content/Intent;
    const-string v39, "extra_click_download_ids"

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/providers/downloads/DownloadNotifier;->getDownloadIds(Ljava/util/Collection;)[J

    move-result-object v40

    move-object/from16 v0, v23

    move-object/from16 v1, v39

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mContext:Landroid/content/Context;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    const/high16 v41, 0x8000000

    move-object/from16 v0, v39

    move/from16 v1, v40

    move-object/from16 v2, v23

    move/from16 v3, v41

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 188
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 220
    .end local v23    # "intent":Landroid/content/Intent;
    .end local v38    # "uri":Landroid/net/Uri;
    :cond_5
    :goto_4
    const/16 v30, 0x0

    .line 221
    .local v30, "remainingText":Ljava/lang/String;
    const/16 v27, 0x0

    .line 222
    .local v27, "percentText":Ljava/lang/String;
    const/16 v39, 0x1

    move/from16 v0, v35

    move/from16 v1, v39

    if-ne v0, v1, :cond_10

    .line 223
    const-wide/16 v14, 0x0

    .line 224
    .local v14, "current":J
    const-wide/16 v36, 0x0

    .line 225
    .local v36, "total":J
    const-wide/16 v32, 0x0

    .line 226
    .local v32, "speed":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadSpeed:Landroid/util/LongSparseLongArray;

    move-object/from16 v40, v0

    monitor-enter v40

    .line 227
    :try_start_0
    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .local v20, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_5
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v39

    if-eqz v39, :cond_d

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/providers/downloads/DownloadInfo;

    .line 228
    .restart local v22    # "info":Lcom/android/providers/downloads/DownloadInfo;
    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mTotalBytes:J

    move-wide/from16 v42, v0

    const-wide/16 v44, -0x1

    cmp-long v39, v42, v44

    if-eqz v39, :cond_6

    .line 229
    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mCurrentBytes:J

    move-wide/from16 v42, v0

    add-long v14, v14, v42

    .line 230
    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mTotalBytes:J

    move-wide/from16 v42, v0

    add-long v36, v36, v42

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadSpeed:Landroid/util/LongSparseLongArray;

    move-object/from16 v39, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    move-wide/from16 v42, v0

    move-object/from16 v0, v39

    move-wide/from16 v1, v42

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseLongArray;->get(J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v42

    add-long v32, v32, v42

    goto :goto_5

    .line 155
    .end local v14    # "current":J
    .end local v16    # "firstShown":J
    .end local v20    # "i$":Ljava/util/Iterator;
    .end local v22    # "info":Lcom/android/providers/downloads/DownloadInfo;
    .end local v27    # "percentText":Ljava/lang/String;
    .end local v30    # "remainingText":Ljava/lang/String;
    .end local v32    # "speed":J
    .end local v36    # "total":J
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 156
    .restart local v16    # "firstShown":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mActiveNotifs:Ljava/util/HashMap;

    move-object/from16 v39, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v40

    move-object/from16 v0, v39

    move-object/from16 v1, v34

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 163
    :cond_8
    const/16 v39, 0x2

    move/from16 v0, v35

    move/from16 v1, v39

    if-ne v0, v1, :cond_9

    .line 164
    const v39, 0x108008a

    move/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    goto/16 :goto_3

    .line 165
    :cond_9
    const/16 v39, 0x3

    move/from16 v0, v35

    move/from16 v1, v39

    if-ne v0, v1, :cond_3

    .line 166
    const v39, 0x7f020007

    move/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    goto/16 :goto_3

    .line 190
    :cond_a
    const/16 v39, 0x3

    move/from16 v0, v35

    move/from16 v1, v39

    if-ne v0, v1, :cond_5

    .line 191
    move-object/from16 v22, v13

    .line 192
    .restart local v22    # "info":Lcom/android/providers/downloads/DownloadInfo;
    sget-object v39, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    move-wide/from16 v40, v0

    invoke-static/range {v39 .. v41}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v38

    .line 194
    .restart local v38    # "uri":Landroid/net/Uri;
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 197
    move-object/from16 v0, v22

    iget v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    move/from16 v39, v0

    invoke-static/range {v39 .. v39}, Landroid/provider/Downloads$Impl;->isStatusError(I)Z

    move-result v39

    if-eqz v39, :cond_b

    .line 198
    const-string v8, "android.intent.action.DOWNLOAD_LIST"

    .line 207
    .local v8, "action":Ljava/lang/String;
    :goto_6
    new-instance v23, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mContext:Landroid/content/Context;

    move-object/from16 v39, v0

    const-class v40, Lcom/android/providers/downloads/DownloadReceiver;

    move-object/from16 v0, v23

    move-object/from16 v1, v38

    move-object/from16 v2, v39

    move-object/from16 v3, v40

    invoke-direct {v0, v8, v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 208
    .restart local v23    # "intent":Landroid/content/Intent;
    const-string v39, "extra_click_download_ids"

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/android/providers/downloads/DownloadNotifier;->getDownloadIds(Ljava/util/Collection;)[J

    move-result-object v40

    move-object/from16 v0, v23

    move-object/from16 v1, v39

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mContext:Landroid/content/Context;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    const/high16 v41, 0x8000000

    move-object/from16 v0, v39

    move/from16 v1, v40

    move-object/from16 v2, v23

    move/from16 v3, v41

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 213
    new-instance v18, Landroid/content/Intent;

    const-string v39, "android.intent.action.DOWNLOAD_HIDE"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mContext:Landroid/content/Context;

    move-object/from16 v40, v0

    const-class v41, Lcom/android/providers/downloads/DownloadReceiver;

    move-object/from16 v0, v18

    move-object/from16 v1, v39

    move-object/from16 v2, v38

    move-object/from16 v3, v40

    move-object/from16 v4, v41

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 215
    .local v18, "hideIntent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mContext:Landroid/content/Context;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    const/16 v41, 0x0

    move-object/from16 v0, v39

    move/from16 v1, v40

    move-object/from16 v2, v18

    move/from16 v3, v41

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 216
    const/16 v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, v22

    iput-boolean v0, v1, Lcom/android/providers/downloads/DownloadInfo;->mDownloadCompleteNotified:Z

    goto/16 :goto_4

    .line 200
    .end local v8    # "action":Ljava/lang/String;
    .end local v18    # "hideIntent":Landroid/content/Intent;
    .end local v23    # "intent":Landroid/content/Intent;
    :cond_b
    move-object/from16 v0, v22

    iget v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mDestination:I

    move/from16 v39, v0

    const/16 v40, 0x5

    move/from16 v0, v39

    move/from16 v1, v40

    if-eq v0, v1, :cond_c

    .line 201
    const-string v8, "android.intent.action.DOWNLOAD_OPEN"

    .restart local v8    # "action":Ljava/lang/String;
    goto/16 :goto_6

    .line 203
    .end local v8    # "action":Ljava/lang/String;
    :cond_c
    const-string v8, "android.intent.action.DOWNLOAD_LIST"

    .restart local v8    # "action":Ljava/lang/String;
    goto/16 :goto_6

    .line 234
    .end local v8    # "action":Ljava/lang/String;
    .end local v22    # "info":Lcom/android/providers/downloads/DownloadInfo;
    .end local v38    # "uri":Landroid/net/Uri;
    .restart local v14    # "current":J
    .restart local v20    # "i$":Ljava/util/Iterator;
    .restart local v27    # "percentText":Ljava/lang/String;
    .restart local v30    # "remainingText":Ljava/lang/String;
    .restart local v32    # "speed":J
    .restart local v36    # "total":J
    :cond_d
    :try_start_1
    monitor-exit v40
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    const-wide/16 v40, 0x0

    cmp-long v39, v36, v40

    if-lez v39, :cond_12

    .line 237
    cmp-long v39, v14, v36

    if-lez v39, :cond_e

    .line 238
    move-wide/from16 v14, v36

    .line 240
    :cond_e
    const-wide/16 v40, 0x64

    mul-long v40, v40, v14

    div-long v40, v40, v36

    move-wide/from16 v0, v40

    long-to-int v0, v0

    move/from16 v26, v0

    .line 241
    .local v26, "percent":I
    const v39, 0x7f04001b

    const/16 v40, 0x1

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v42

    aput-object v42, v40, v41

    move-object/from16 v0, v31

    move/from16 v1, v39

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    .line 243
    const-wide/16 v40, 0x0

    cmp-long v39, v32, v40

    if-lez v39, :cond_f

    .line 244
    sub-long v40, v36, v14

    const-wide/16 v42, 0x3e8

    mul-long v40, v40, v42

    div-long v28, v40, v32

    .line 245
    .local v28, "remainingMillis":J
    const v39, 0x7f04001c

    const/16 v40, 0x1

    move/from16 v0, v40

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    invoke-static/range {v28 .. v29}, Landroid/text/format/DateUtils;->formatDuration(J)Ljava/lang/CharSequence;

    move-result-object v42

    aput-object v42, v40, v41

    move-object/from16 v0, v31

    move/from16 v1, v39

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    .line 249
    .end local v28    # "remainingMillis":J
    :cond_f
    const/16 v39, 0x64

    const/16 v40, 0x0

    move/from16 v0, v39

    move/from16 v1, v26

    move/from16 v2, v40

    invoke-virtual {v9, v0, v1, v2}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 257
    .end local v14    # "current":J
    .end local v20    # "i$":Ljava/util/Iterator;
    .end local v26    # "percent":I
    .end local v32    # "speed":J
    .end local v36    # "total":J
    :cond_10
    :goto_7
    invoke-interface {v11}, Ljava/util/Collection;->size()I

    move-result v39

    const/16 v40, 0x1

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_19

    .line 258
    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/providers/downloads/DownloadInfo;

    .line 260
    .restart local v22    # "info":Lcom/android/providers/downloads/DownloadInfo;
    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/android/providers/downloads/DownloadNotifier;->getDownloadTitle(Landroid/content/res/Resources;Lcom/android/providers/downloads/DownloadInfo;)Ljava/lang/CharSequence;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 262
    const/16 v39, 0x1

    move/from16 v0, v35

    move/from16 v1, v39

    if-ne v0, v1, :cond_14

    .line 263
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mDescription:Ljava/lang/String;

    move-object/from16 v39, v0

    invoke-static/range {v39 .. v39}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v39

    if-nez v39, :cond_13

    .line 264
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mDescription:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 268
    :goto_8
    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 295
    :cond_11
    :goto_9
    invoke-virtual {v9}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v25

    .line 323
    .end local v22    # "info":Lcom/android/providers/downloads/DownloadInfo;
    .local v25, "notif":Landroid/app/Notification;
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mNotifManager:Landroid/app/NotificationManager;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    move-object/from16 v0, v39

    move-object/from16 v1, v34

    move/from16 v2, v40

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_1

    .line 234
    .end local v25    # "notif":Landroid/app/Notification;
    .restart local v14    # "current":J
    .restart local v32    # "speed":J
    .restart local v36    # "total":J
    :catchall_0
    move-exception v39

    :try_start_2
    monitor-exit v40
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v39

    .line 251
    .restart local v20    # "i$":Ljava/util/Iterator;
    :cond_12
    const/16 v39, 0x64

    const/16 v40, 0x0

    const/16 v41, 0x1

    move/from16 v0, v39

    move/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v9, v0, v1, v2}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    goto :goto_7

    .line 266
    .end local v14    # "current":J
    .end local v20    # "i$":Ljava/util/Iterator;
    .end local v32    # "speed":J
    .end local v36    # "total":J
    .restart local v22    # "info":Lcom/android/providers/downloads/DownloadInfo;
    :cond_13
    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto :goto_8

    .line 270
    :cond_14
    const/16 v39, 0x2

    move/from16 v0, v35

    move/from16 v1, v39

    if-ne v0, v1, :cond_15

    .line 271
    const v39, 0x7f040012

    move-object/from16 v0, v31

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto :goto_9

    .line 274
    :cond_15
    const/16 v39, 0x3

    move/from16 v0, v35

    move/from16 v1, v39

    if-ne v0, v1, :cond_11

    .line 275
    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mLastMod:J

    move-wide/from16 v40, v0

    move-wide/from16 v0, v40

    invoke-virtual {v9, v0, v1}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 276
    move-object/from16 v0, v22

    iget v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    move/from16 v39, v0

    invoke-static/range {v39 .. v39}, Landroid/provider/Downloads$Impl;->isStatusError(I)Z

    move-result v39

    if-eqz v39, :cond_16

    .line 277
    const v39, 0x7f040011

    move-object/from16 v0, v31

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 278
    const v39, 0x7f020008

    move/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    goto/16 :goto_9

    .line 279
    :cond_16
    move-object/from16 v0, v22

    iget v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    move/from16 v39, v0

    invoke-static/range {v39 .. v39}, Landroid/provider/Downloads$Impl;->isStatusSuccess(I)Z

    move-result v39

    if-eqz v39, :cond_11

    .line 280
    const v39, 0x7f040010

    move-object/from16 v0, v31

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 282
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v39

    const-string v40, "CscFeature_Web_EnableDownloadedFolderInNotificationBar"

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_11

    .line 283
    const-string v6, "/storage/emulated/0/Download"

    .line 284
    .local v6, "DEVICE_PATH":Ljava/lang/String;
    const-string v7, "/storage/extSdCard/Download"

    .line 285
    .local v7, "EXT_PATH":Ljava/lang/String;
    const v39, 0x7f040010

    move-object/from16 v0, v31

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 286
    .local v10, "caption":Ljava/lang/String;
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mFileName:Ljava/lang/String;

    move-object/from16 v39, v0

    if-eqz v39, :cond_18

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mFileName:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "/storage/emulated/0/Download"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_18

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mContext:Landroid/content/Context;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v39

    const v40, 0x7f04001e

    const/16 v41, 0x1

    move/from16 v0, v41

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v41, v0

    const/16 v42, 0x0

    const-string v43, "emulated0/Download"

    aput-object v43, v41, v42

    invoke-virtual/range {v39 .. v41}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 290
    :cond_17
    :goto_b
    invoke-virtual {v9, v10}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto/16 :goto_9

    .line 288
    :cond_18
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mFileName:Ljava/lang/String;

    move-object/from16 v39, v0

    if-eqz v39, :cond_17

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mFileName:Ljava/lang/String;

    move-object/from16 v39, v0

    const-string v40, "/storage/extSdCard/Download"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_17

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mContext:Landroid/content/Context;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v39

    const v40, 0x7f04001e

    const/16 v41, 0x1

    move/from16 v0, v41

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v41, v0

    const/16 v42, 0x0

    const-string v43, "extSdCard/Download"

    aput-object v43, v41, v42

    invoke-virtual/range {v39 .. v41}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto :goto_b

    .line 298
    .end local v6    # "DEVICE_PATH":Ljava/lang/String;
    .end local v7    # "EXT_PATH":Ljava/lang/String;
    .end local v10    # "caption":Ljava/lang/String;
    .end local v22    # "info":Lcom/android/providers/downloads/DownloadInfo;
    :cond_19
    new-instance v21, Landroid/app/Notification$InboxStyle;

    move-object/from16 v0, v21

    invoke-direct {v0, v9}, Landroid/app/Notification$InboxStyle;-><init>(Landroid/app/Notification$Builder;)V

    .line 300
    .local v21, "inboxStyle":Landroid/app/Notification$InboxStyle;
    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .restart local v20    # "i$":Ljava/util/Iterator;
    :goto_c
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v39

    if-eqz v39, :cond_1a

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/providers/downloads/DownloadInfo;

    .line 301
    .restart local v22    # "info":Lcom/android/providers/downloads/DownloadInfo;
    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/android/providers/downloads/DownloadNotifier;->getDownloadTitle(Landroid/content/res/Resources;Lcom/android/providers/downloads/DownloadInfo;)Ljava/lang/CharSequence;

    move-result-object v39

    move-object/from16 v0, v21

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    goto :goto_c

    .line 304
    .end local v22    # "info":Lcom/android/providers/downloads/DownloadInfo;
    :cond_1a
    const/16 v39, 0x1

    move/from16 v0, v35

    move/from16 v1, v39

    if-ne v0, v1, :cond_1c

    .line 305
    const/high16 v39, 0x7f050000

    invoke-interface {v11}, Ljava/util/Collection;->size()I

    move-result v40

    const/16 v41, 0x1

    move/from16 v0, v41

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v41, v0

    const/16 v42, 0x0

    invoke-interface {v11}, Ljava/util/Collection;->size()I

    move-result v43

    invoke-static/range {v43 .. v43}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v43

    aput-object v43, v41, v42

    move-object/from16 v0, v31

    move/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 307
    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 308
    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 309
    move-object/from16 v0, v21

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/app/Notification$InboxStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    .line 320
    :cond_1b
    :goto_d
    invoke-virtual/range {v21 .. v21}, Landroid/app/Notification$InboxStyle;->build()Landroid/app/Notification;

    move-result-object v25

    .restart local v25    # "notif":Landroid/app/Notification;
    goto/16 :goto_a

    .line 311
    .end local v25    # "notif":Landroid/app/Notification;
    :cond_1c
    const/16 v39, 0x2

    move/from16 v0, v35

    move/from16 v1, v39

    if-ne v0, v1, :cond_1b

    .line 312
    const v39, 0x7f050001

    invoke-interface {v11}, Ljava/util/Collection;->size()I

    move-result v40

    const/16 v41, 0x1

    move/from16 v0, v41

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v41, v0

    const/16 v42, 0x0

    invoke-interface {v11}, Ljava/util/Collection;->size()I

    move-result v43

    invoke-static/range {v43 .. v43}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v43

    aput-object v43, v41, v42

    move-object/from16 v0, v31

    move/from16 v1, v39

    move/from16 v2, v40

    move-object/from16 v3, v41

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 314
    const v39, 0x7f040012

    move-object/from16 v0, v31

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 316
    const v39, 0x7f040012

    move-object/from16 v0, v31

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v21

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/app/Notification$InboxStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    goto :goto_d

    .line 327
    .end local v9    # "builder":Landroid/app/Notification$Builder;
    .end local v11    # "cluster":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/providers/downloads/DownloadInfo;>;"
    .end local v13    # "downInfo":Lcom/android/providers/downloads/DownloadInfo;
    .end local v16    # "firstShown":J
    .end local v20    # "i$":Ljava/util/Iterator;
    .end local v21    # "inboxStyle":Landroid/app/Notification$InboxStyle;
    .end local v27    # "percentText":Ljava/lang/String;
    .end local v30    # "remainingText":Ljava/lang/String;
    .end local v34    # "tag":Ljava/lang/String;
    .end local v35    # "type":I
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mActiveNotifs:Ljava/util/HashMap;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v39

    invoke-interface/range {v39 .. v39}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v24

    .line 328
    .local v24, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1e
    :goto_e
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v39

    if-eqz v39, :cond_1f

    .line 329
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    .line 330
    .restart local v34    # "tag":Ljava/lang/String;
    move-object/from16 v0, v34

    invoke-interface {v12, v0}, Lcom/google/common/collect/Multimap;->containsKey(Ljava/lang/Object;)Z

    move-result v39

    if-nez v39, :cond_1e

    .line 331
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadNotifier;->mNotifManager:Landroid/app/NotificationManager;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    move-object/from16 v0, v39

    move-object/from16 v1, v34

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 332
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->remove()V

    goto :goto_e

    .line 335
    .end local v34    # "tag":Ljava/lang/String;
    :cond_1f
    return-void
.end method


# virtual methods
.method public cancelAll()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadNotifier;->mNotifManager:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 99
    return-void
.end method

.method public dumpSpeeds()V
    .locals 12

    .prologue
    .line 355
    iget-object v6, p0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadSpeed:Landroid/util/LongSparseLongArray;

    monitor-enter v6

    .line 356
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadSpeed:Landroid/util/LongSparseLongArray;

    invoke-virtual {v3}, Landroid/util/LongSparseLongArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 357
    iget-object v3, p0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadSpeed:Landroid/util/LongSparseLongArray;

    invoke-virtual {v3, v2}, Landroid/util/LongSparseLongArray;->keyAt(I)J

    move-result-wide v4

    .line 358
    .local v4, "id":J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iget-object v3, p0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadTouch:Landroid/util/LongSparseLongArray;

    invoke-virtual {v3, v4, v5}, Landroid/util/LongSparseLongArray;->get(J)J

    move-result-wide v10

    sub-long v0, v8, v10

    .line 359
    .local v0, "delta":J
    const-string v3, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Download "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " speed "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadSpeed:Landroid/util/LongSparseLongArray;

    invoke-virtual {v8, v2}, Landroid/util/LongSparseLongArray;->valueAt(I)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "bps, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "ms ago"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 362
    .end local v0    # "delta":J
    .end local v4    # "id":J
    :cond_0
    monitor-exit v6

    .line 363
    return-void

    .line 362
    :catchall_0
    move-exception v3

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public notifyDownloadSpeed(JJ)V
    .locals 5
    .param p1, "id"    # J
    .param p3, "bytesPerSecond"    # J

    .prologue
    .line 106
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadSpeed:Landroid/util/LongSparseLongArray;

    monitor-enter v1

    .line 107
    const-wide/16 v2, 0x0

    cmp-long v0, p3, v2

    if-eqz v0, :cond_0

    .line 108
    :try_start_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadSpeed:Landroid/util/LongSparseLongArray;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/util/LongSparseLongArray;->put(JJ)V

    .line 109
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadTouch:Landroid/util/LongSparseLongArray;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, p1, p2, v2, v3}, Landroid/util/LongSparseLongArray;->put(JJ)V

    .line 114
    :goto_0
    monitor-exit v1

    .line 115
    return-void

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadSpeed:Landroid/util/LongSparseLongArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseLongArray;->delete(J)V

    .line 112
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadNotifier;->mDownloadTouch:Landroid/util/LongSparseLongArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseLongArray;->delete(J)V

    goto :goto_0

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public updateWith(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/providers/downloads/DownloadInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    .local p1, "downloads":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/providers/downloads/DownloadInfo;>;"
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadNotifier;->mActiveNotifs:Ljava/util/HashMap;

    monitor-enter v1

    .line 123
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/providers/downloads/DownloadNotifier;->updateWithLocked(Ljava/util/Collection;)V

    .line 124
    monitor-exit v1

    .line 125
    return-void

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
