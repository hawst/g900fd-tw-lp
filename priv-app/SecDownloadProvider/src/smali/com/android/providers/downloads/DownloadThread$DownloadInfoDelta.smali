.class Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
.super Ljava/lang/Object;
.source "DownloadThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/downloads/DownloadThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadInfoDelta"
.end annotation


# instance fields
.field public bFirstChunkOfMultiChunk:Z

.field public mChunkSize:J

.field public mCurrentBytes:J

.field public mETag:Ljava/lang/String;

.field public mErrorMsg:Ljava/lang/String;

.field public mFileName:Ljava/lang/String;

.field public mFirstChunkEnd:J

.field public mHasNotifyURI:Z

.field public mIsVisibleInDownloadsUi:Z

.field public mMimeType:Ljava/lang/String;

.field public mNumFailed:I

.field public mRealDownloadBytes:J

.field public mRetryAfter:I

.field public mSecChunkEnd:J

.field public mSecChunkStart:J

.field public mStatus:I

.field public mTotalBytes:J

.field public mUri:Ljava/lang/String;

.field public syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

.field final synthetic this$0:Lcom/android/providers/downloads/DownloadThread;


# direct methods
.method public constructor <init>(Lcom/android/providers/downloads/DownloadThread;Lcom/android/providers/downloads/DownloadInfo;)V
    .locals 6
    .param p2, "info"    # Lcom/android/providers/downloads/DownloadInfo;

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 206
    iput-object p1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->this$0:Lcom/android/providers/downloads/DownloadThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    iput-wide v4, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    .line 192
    iput-wide v4, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    .line 195
    iput-wide v4, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    .line 198
    iput-wide v4, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mChunkSize:J

    .line 201
    iput-wide v4, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    .line 202
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    .line 207
    iget-object v0, p2, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mUri:Ljava/lang/String;

    .line 208
    iget-object v0, p2, Lcom/android/providers/downloads/DownloadInfo;->mFileName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    .line 209
    iget-object v0, p2, Lcom/android/providers/downloads/DownloadInfo;->mMimeType:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    .line 210
    iget v0, p2, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    iput v0, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    .line 211
    iget v0, p2, Lcom/android/providers/downloads/DownloadInfo;->mNumFailed:I

    iput v0, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mNumFailed:I

    .line 212
    iget v0, p2, Lcom/android/providers/downloads/DownloadInfo;->mRetryAfter:I

    iput v0, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRetryAfter:I

    .line 213
    iget-wide v2, p2, Lcom/android/providers/downloads/DownloadInfo;->mTotalBytes:J

    iput-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    .line 214
    iget-wide v2, p2, Lcom/android/providers/downloads/DownloadInfo;->mCurrentBytes:J

    iput-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    .line 215
    iget-object v0, p2, Lcom/android/providers/downloads/DownloadInfo;->mETag:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mETag:Ljava/lang/String;

    .line 217
    # getter for: Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;
    invoke-static {p1}, Lcom/android/providers/downloads/DownloadThread;->access$000(Lcom/android/providers/downloads/DownloadThread;)Lcom/android/providers/downloads/DownloadInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mNotifyURI:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mHasNotifyURI:Z

    .line 218
    iget-boolean v0, p2, Lcom/android/providers/downloads/DownloadInfo;->mIsVisibleInDownloadsUi:Z

    iput-boolean v0, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mIsVisibleInDownloadsUi:Z

    .line 224
    iget-wide v2, p2, Lcom/android/providers/downloads/DownloadInfo;->mRangeStart:J

    iput-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    .line 225
    iget-wide v2, p2, Lcom/android/providers/downloads/DownloadInfo;->mRangeEnd:J

    iput-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    .line 226
    iget-wide v2, p2, Lcom/android/providers/downloads/DownloadInfo;->mFirstEnd:J

    iput-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    .line 227
    iput-wide v4, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mChunkSize:J

    .line 228
    iput-wide v4, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    .line 229
    iput-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->bFirstChunkOfMultiChunk:Z

    .line 232
    return-void

    :cond_0
    move v0, v1

    .line 217
    goto :goto_0
.end method


# virtual methods
.method public copyMe(Lcom/android/providers/downloads/DownloadInfo;)Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    .locals 4
    .param p1, "info"    # Lcom/android/providers/downloads/DownloadInfo;

    .prologue
    .line 236
    new-instance v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->this$0:Lcom/android/providers/downloads/DownloadThread;

    invoke-direct {v0, v1, p1}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;-><init>(Lcom/android/providers/downloads/DownloadThread;Lcom/android/providers/downloads/DownloadInfo;)V

    .line 237
    .local v0, "newInfoDelta":Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mUri:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mUri:Ljava/lang/String;

    .line 238
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    .line 239
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    .line 240
    iget v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    iput v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    .line 241
    iget v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mNumFailed:I

    iput v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mNumFailed:I

    .line 242
    iget v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRetryAfter:I

    iput v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRetryAfter:I

    .line 243
    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    iput-wide v2, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    .line 244
    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    iput-wide v2, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    .line 245
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mETag:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mETag:Ljava/lang/String;

    .line 246
    iget-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mHasNotifyURI:Z

    iput-boolean v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mHasNotifyURI:Z

    .line 247
    iget-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mIsVisibleInDownloadsUi:Z

    iput-boolean v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mIsVisibleInDownloadsUi:Z

    .line 248
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mErrorMsg:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mErrorMsg:Ljava/lang/String;

    .line 249
    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    iput-wide v2, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    .line 250
    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    iput-wide v2, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    .line 251
    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    iput-wide v2, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    .line 252
    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mChunkSize:J

    iput-wide v2, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mChunkSize:J

    .line 253
    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    iput-wide v2, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRealDownloadBytes:J

    .line 254
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    iput-object v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->syncThread:Lcom/android/providers/downloads/DownloadThread$SyncThread;

    .line 255
    iget-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->bFirstChunkOfMultiChunk:Z

    iput-boolean v1, v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->bFirstChunkOfMultiChunk:Z

    .line 256
    return-object v0
.end method

.method public writeToDatabase()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x6

    .line 280
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 282
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "uri"

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mUri:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const-string v1, "_data"

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string v1, "mimetype"

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const-string v1, "status"

    iget v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 288
    iget v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v1}, Landroid/provider/Downloads$Impl;->isStatusError(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 290
    iget-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mHasNotifyURI:Z

    if-eqz v1, :cond_1

    .line 291
    const-string v1, "state"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 311
    :cond_0
    :goto_0
    const-string v1, "numfailed"

    iget v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mNumFailed:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 312
    const-string v1, "method"

    iget v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mRetryAfter:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 313
    const-string v1, "total_bytes"

    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mTotalBytes:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 314
    const-string v1, "current_bytes"

    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 315
    const-string v1, "etag"

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mETag:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const-string v1, "lastmod"

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->this$0:Lcom/android/providers/downloads/DownloadThread;

    # getter for: Lcom/android/providers/downloads/DownloadThread;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;
    invoke-static {v2}, Lcom/android/providers/downloads/DownloadThread;->access$100(Lcom/android/providers/downloads/DownloadThread;)Lcom/android/providers/downloads/SystemFacade;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/providers/downloads/SystemFacade;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 318
    const-string v1, "errorMsg"

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mErrorMsg:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const-string v1, "range_start"

    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 322
    const-string v1, "range_end"

    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 323
    const-string v1, "range_first_end"

    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 326
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->this$0:Lcom/android/providers/downloads/DownloadThread;

    # getter for: Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/providers/downloads/DownloadThread;->access$200(Lcom/android/providers/downloads/DownloadThread;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->this$0:Lcom/android/providers/downloads/DownloadThread;

    # getter for: Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;
    invoke-static {v2}, Lcom/android/providers/downloads/DownloadThread;->access$000(Lcom/android/providers/downloads/DownloadThread;)Lcom/android/providers/downloads/DownloadInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/providers/downloads/DownloadInfo;->getAllDownloadsUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 327
    return-void

    .line 293
    :cond_1
    const-string v1, "state"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 295
    :cond_2
    iget v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mStatus:I

    invoke-static {v1}, Landroid/provider/Downloads$Impl;->isStatusSuccess(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 296
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    const-string v2, "application/vnd.oma.dd+xml"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mIsVisibleInDownloadsUi:Z

    if-eqz v1, :cond_3

    .line 297
    const-string v1, "status"

    const/16 v2, 0xb5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 298
    const-string v1, "state"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 302
    :cond_3
    iget-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mHasNotifyURI:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mIsVisibleInDownloadsUi:Z

    if-eqz v1, :cond_4

    .line 303
    const-string v1, "state"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 305
    :cond_4
    const-string v1, "state"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0
.end method

.method public writeToDatabase_NB(Z)V
    .locals 5
    .param p1, "bSyncData"    # Z

    .prologue
    const/4 v4, 0x0

    .line 260
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 262
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "uri"

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mUri:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string v1, "_data"

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string v1, "mimetype"

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v1, "current_bytes"

    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 266
    const-string v1, "lastmod"

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->this$0:Lcom/android/providers/downloads/DownloadThread;

    # getter for: Lcom/android/providers/downloads/DownloadThread;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;
    invoke-static {v2}, Lcom/android/providers/downloads/DownloadThread;->access$100(Lcom/android/providers/downloads/DownloadThread;)Lcom/android/providers/downloads/SystemFacade;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/providers/downloads/SystemFacade;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 267
    if-eqz p1, :cond_0

    .line 268
    const-string v1, "range_start"

    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 269
    const-string v1, "range_end"

    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 270
    const-string v1, "range_first_end"

    iget-wide v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 272
    :cond_0
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->this$0:Lcom/android/providers/downloads/DownloadThread;

    # getter for: Lcom/android/providers/downloads/DownloadThread;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/providers/downloads/DownloadThread;->access$200(Lcom/android/providers/downloads/DownloadThread;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->this$0:Lcom/android/providers/downloads/DownloadThread;

    # getter for: Lcom/android/providers/downloads/DownloadThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;
    invoke-static {v2}, Lcom/android/providers/downloads/DownloadThread;->access$000(Lcom/android/providers/downloads/DownloadThread;)Lcom/android/providers/downloads/DownloadInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/providers/downloads/DownloadInfo;->getAllDownloadsUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 273
    return-void
.end method
