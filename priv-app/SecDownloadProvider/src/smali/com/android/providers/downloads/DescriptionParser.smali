.class public Lcom/android/providers/downloads/DescriptionParser;
.super Ljava/lang/Object;
.source "DescriptionParser.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFilePath:Ljava/lang/String;

.field private mInfo:Lcom/android/providers/downloads/DownloadInfo;


# direct methods
.method public constructor <init>(Lcom/android/providers/downloads/DownloadInfo;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0
    .param p1, "info"    # Lcom/android/providers/downloads/DownloadInfo;
    .param p2, "Filepath"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/android/providers/downloads/DescriptionParser;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    .line 49
    iput-object p2, p0, Lcom/android/providers/downloads/DescriptionParser;->mFilePath:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/android/providers/downloads/DescriptionParser;->mContext:Landroid/content/Context;

    .line 52
    return-void
.end method


# virtual methods
.method public HandleOmaData(Landroid/content/Context;Lcom/android/providers/downloads/DescriptionInfo;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mDDInfo"    # Lcom/android/providers/downloads/DescriptionInfo;

    .prologue
    .line 169
    const/4 v0, 0x1

    .line 171
    .local v0, "RStatus":I
    invoke-virtual {p2}, Lcom/android/providers/downloads/DescriptionInfo;->GetRootElement()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 173
    const/16 v1, 0x259

    .line 180
    :goto_0
    return v1

    .line 176
    :cond_0
    invoke-virtual {p0, p2}, Lcom/android/providers/downloads/DescriptionParser;->OMADescriptionDataValidation(Lcom/android/providers/downloads/DescriptionInfo;)I

    move-result v0

    move v1, v0

    .line 180
    goto :goto_0
.end method

.method public OMADescriptionDataValidation(Lcom/android/providers/downloads/DescriptionInfo;)I
    .locals 8
    .param p1, "mDDInfo"    # Lcom/android/providers/downloads/DescriptionInfo;

    .prologue
    const/16 v4, 0x259

    .line 187
    const/4 v0, 0x1

    .line 200
    .local v0, "RStatus":I
    invoke-virtual {p1}, Lcom/android/providers/downloads/DescriptionInfo;->getSize()J

    move-result-wide v2

    .line 201
    .local v2, "size":J
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-gtz v5, :cond_1

    move v0, v4

    .line 228
    .end local v0    # "RStatus":I
    :cond_0
    :goto_0
    return v0

    .line 204
    .restart local v0    # "RStatus":I
    :cond_1
    const-wide/32 v6, 0x7fffffff

    cmp-long v5, v2, v6

    if-lez v5, :cond_2

    .line 205
    const/16 v0, 0x276

    goto :goto_0

    .line 208
    :cond_2
    invoke-virtual {p1}, Lcom/android/providers/downloads/DescriptionInfo;->getMimeType()Ljava/lang/String;

    move-result-object v1

    .line 209
    .local v1, "value":Ljava/lang/String;
    if-nez v1, :cond_3

    move v0, v4

    .line 211
    goto :goto_0

    .line 214
    :cond_3
    invoke-virtual {p1}, Lcom/android/providers/downloads/DescriptionInfo;->GetObjectURI()Ljava/lang/String;

    move-result-object v1

    .line 215
    if-eqz v1, :cond_4

    const-string v5, "\\n"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_4
    move v0, v4

    .line 219
    goto :goto_0

    .line 222
    :cond_5
    invoke-virtual {p1}, Lcom/android/providers/downloads/DescriptionInfo;->getDDVersion()Ljava/lang/String;

    move-result-object v1

    .line 223
    if-eqz v1, :cond_0

    const-string v5, "1"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "1.0"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    move v0, v4

    .line 224
    goto :goto_0
.end method

.method public ParseData(J)Lcom/android/providers/downloads/DescriptionInfo;
    .locals 23
    .param p1, "lID"    # J

    .prologue
    .line 57
    const/4 v7, 0x0

    .line 59
    .local v7, "ddData":Ljava/lang/String;
    const/4 v11, 0x0

    .line 61
    .local v11, "flag":Z
    const/4 v10, 0x0

    .line 63
    .local v10, "fileContent":[B
    const/4 v8, 0x0

    .line 67
    .local v8, "ddFile":Ljava/io/File;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .local v6, "contents":Ljava/lang/StringBuilder;
    new-instance v16, Lcom/android/providers/downloads/DescriptionInfo;

    invoke-direct/range {v16 .. v16}, Lcom/android/providers/downloads/DescriptionInfo;-><init>()V

    .line 71
    .local v16, "mDDInfo":Lcom/android/providers/downloads/DescriptionInfo;
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 74
    .local v17, "values":Landroid/content/ContentValues;
    const/4 v13, 0x0

    .line 77
    .local v13, "input":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v14, Ljava/io/BufferedReader;

    new-instance v18, Ljava/io/FileReader;

    new-instance v19, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DescriptionParser;->mFilePath:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v18 .. v19}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v18

    invoke-direct {v14, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    .end local v13    # "input":Ljava/io/BufferedReader;
    .local v14, "input":Ljava/io/BufferedReader;
    const/4 v15, 0x0

    .line 81
    .local v15, "line":Ljava/lang/String;
    :goto_0
    :try_start_1
    invoke-virtual {v14}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_0

    .line 83
    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    const-string v18, "line.separator"

    invoke-static/range {v18 .. v18}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 91
    :catch_0
    move-exception v9

    move-object v13, v14

    .line 92
    .end local v14    # "input":Ljava/io/BufferedReader;
    .end local v15    # "line":Ljava/lang/String;
    .local v9, "e":Ljava/lang/Exception;
    .restart local v13    # "input":Ljava/io/BufferedReader;
    :goto_1
    :try_start_2
    const-string v18, "status"

    const/16 v19, 0x259

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 93
    const-string v18, "state"

    const/16 v19, 0x9

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 94
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DescriptionParser;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    sget-object v19, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    move-wide/from16 v1, p1

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v19

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    move-object/from16 v3, v20

    move-object/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 98
    const/16 v16, 0x0

    .line 105
    .end local v16    # "mDDInfo":Lcom/android/providers/downloads/DescriptionInfo;
    :try_start_3
    invoke-virtual {v13}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 159
    .end local v9    # "e":Ljava/lang/Exception;
    :goto_2
    return-object v16

    .line 87
    .end local v13    # "input":Ljava/io/BufferedReader;
    .restart local v14    # "input":Ljava/io/BufferedReader;
    .restart local v15    # "line":Ljava/lang/String;
    .restart local v16    # "mDDInfo":Lcom/android/providers/downloads/DescriptionInfo;
    :cond_0
    :try_start_4
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v7

    .line 105
    :try_start_5
    invoke-virtual {v14}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 112
    :goto_3
    const/16 v18, 0x3c

    :try_start_6
    move/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v12

    .line 113
    .local v12, "index":I
    if-lez v12, :cond_1

    .line 114
    invoke-virtual {v7, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/providers/downloads/DescriptionInfo;->parseDD(Ljava/lang/String;)V

    .line 121
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DescriptionParser;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lcom/android/providers/downloads/DescriptionParser;->HandleOmaData(Landroid/content/Context;Lcom/android/providers/downloads/DescriptionInfo;)I

    move-result v5

    .line 123
    .local v5, "RStauts":I
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/android/providers/downloads/DescriptionParser;->UpdateOmaDescriptionData(Lcom/android/providers/downloads/DescriptionInfo;Landroid/content/ContentValues;)Z

    .line 124
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v5, v0, :cond_2

    .line 126
    const-string v18, "status"

    const/16 v19, 0xb6

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DescriptionParser;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    sget-object v19, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    move-wide/from16 v1, p1

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v19

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    move-object/from16 v3, v20

    move-object/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    move-object v13, v14

    .line 129
    .end local v14    # "input":Ljava/io/BufferedReader;
    .restart local v13    # "input":Ljava/io/BufferedReader;
    goto :goto_2

    .line 104
    .end local v5    # "RStauts":I
    .end local v12    # "index":I
    .end local v15    # "line":Ljava/lang/String;
    :catchall_0
    move-exception v18

    .line 105
    :goto_5
    :try_start_7
    invoke-virtual {v13}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 108
    :goto_6
    throw v18

    .line 117
    .end local v13    # "input":Ljava/io/BufferedReader;
    .restart local v12    # "index":I
    .restart local v14    # "input":Ljava/io/BufferedReader;
    .restart local v15    # "line":Ljava/lang/String;
    :cond_1
    :try_start_8
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Lcom/android/providers/downloads/DescriptionInfo;->parseDD(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_4

    .line 143
    .end local v12    # "index":I
    :catch_1
    move-exception v9

    .line 145
    .restart local v9    # "e":Ljava/lang/Exception;
    const-string v18, "status"

    const/16 v19, 0x259

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 146
    invoke-virtual/range {v16 .. v16}, Lcom/android/providers/downloads/DescriptionInfo;->getInstallNotifyURI()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_4

    .line 147
    const-string v18, "state"

    const/16 v19, 0x6

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 151
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DescriptionParser;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    sget-object v19, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    move-wide/from16 v1, p1

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v19

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    move-object/from16 v3, v20

    move-object/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 155
    const/16 v16, 0x0

    move-object v13, v14

    .end local v14    # "input":Ljava/io/BufferedReader;
    .restart local v13    # "input":Ljava/io/BufferedReader;
    goto/16 :goto_2

    .line 132
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v13    # "input":Ljava/io/BufferedReader;
    .restart local v5    # "RStauts":I
    .restart local v12    # "index":I
    .restart local v14    # "input":Ljava/io/BufferedReader;
    :cond_2
    :try_start_9
    const-string v18, "status"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 134
    invoke-virtual/range {v16 .. v16}, Lcom/android/providers/downloads/DescriptionInfo;->getInstallNotifyURI()Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_3

    .line 135
    const-string v18, "state"

    const/16 v19, 0x6

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 139
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/DescriptionParser;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    sget-object v19, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    move-wide/from16 v1, p1

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v19

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    move-object/from16 v3, v20

    move-object/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 159
    const/16 v16, 0x0

    move-object v13, v14

    .end local v14    # "input":Ljava/io/BufferedReader;
    .restart local v13    # "input":Ljava/io/BufferedReader;
    goto/16 :goto_2

    .line 137
    .end local v13    # "input":Ljava/io/BufferedReader;
    .restart local v14    # "input":Ljava/io/BufferedReader;
    :cond_3
    const-string v18, "state"

    const/16 v19, 0x9

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    goto :goto_8

    .line 149
    .end local v5    # "RStauts":I
    .end local v12    # "index":I
    .restart local v9    # "e":Ljava/lang/Exception;
    :cond_4
    const-string v18, "state"

    const/16 v19, 0x9

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_7

    .line 107
    .end local v9    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v18

    goto/16 :goto_3

    .end local v14    # "input":Ljava/io/BufferedReader;
    .end local v15    # "line":Ljava/lang/String;
    .end local v16    # "mDDInfo":Lcom/android/providers/downloads/DescriptionInfo;
    .restart local v9    # "e":Ljava/lang/Exception;
    .restart local v13    # "input":Ljava/io/BufferedReader;
    :catch_3
    move-exception v18

    goto/16 :goto_2

    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v16    # "mDDInfo":Lcom/android/providers/downloads/DescriptionInfo;
    :catch_4
    move-exception v19

    goto/16 :goto_6

    .line 104
    .end local v13    # "input":Ljava/io/BufferedReader;
    .restart local v14    # "input":Ljava/io/BufferedReader;
    .restart local v15    # "line":Ljava/lang/String;
    :catchall_1
    move-exception v18

    move-object v13, v14

    .end local v14    # "input":Ljava/io/BufferedReader;
    .restart local v13    # "input":Ljava/io/BufferedReader;
    goto/16 :goto_5

    .line 91
    .end local v15    # "line":Ljava/lang/String;
    :catch_5
    move-exception v9

    goto/16 :goto_1
.end method

.method public UpdateOmaDescriptionData(Lcom/android/providers/downloads/DescriptionInfo;Landroid/content/ContentValues;)Z
    .locals 18
    .param p1, "mDDInfo"    # Lcom/android/providers/downloads/DescriptionInfo;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 237
    const/4 v15, 0x1

    .line 240
    .local v15, "result":Z
    const-string v3, "dd_contentSize"

    invoke-virtual/range {p1 .. p1}, Lcom/android/providers/downloads/DescriptionInfo;->getSize()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 242
    invoke-virtual/range {p1 .. p1}, Lcom/android/providers/downloads/DescriptionInfo;->getMimeType()Ljava/lang/String;

    move-result-object v17

    .line 243
    .local v17, "value":Ljava/lang/String;
    if-eqz v17, :cond_0

    .line 244
    const-string v3, "dd_primaryMimeType"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    const-string v3, "mimetype"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/providers/downloads/DescriptionInfo;->getSecMimeType1()Ljava/lang/String;

    move-result-object v17

    .line 248
    if-eqz v17, :cond_1

    .line 249
    const-string v3, "dd_SecondaryMimeType1"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/android/providers/downloads/DescriptionInfo;->getSecMimeType2()Ljava/lang/String;

    move-result-object v17

    .line 252
    if-eqz v17, :cond_2

    .line 253
    const-string v3, "dd_SecondaryMimeType2"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/android/providers/downloads/DescriptionInfo;->GetObjectURI()Ljava/lang/String;

    move-result-object v17

    .line 258
    if-eqz v17, :cond_5

    .line 261
    :try_start_0
    sget-boolean v3, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v3, :cond_3

    .line 262
    const-string v3, "DownloadManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DescriptionParser : UpdateOmaDescriptionData : Obj Url:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const-string v3, "DownloadManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DescriptionParser : UpdateOmaDescriptionData : Main Url:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DescriptionParser;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :cond_3
    new-instance v11, Ljava/net/URI;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/downloads/DescriptionParser;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v3, v3, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    invoke-direct {v11, v3}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 266
    .local v11, "MainUrl":Ljava/net/URI;
    invoke-static/range {v17 .. v17}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 268
    .local v14, "objUri":Landroid/net/Uri;
    const/4 v3, 0x1

    invoke-virtual {v14}, Landroid/net/Uri;->isRelative()Z

    move-result v4

    if-ne v3, v4, :cond_a

    .line 270
    invoke-static/range {v17 .. v17}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 271
    const-string v3, "/"

    move-object/from16 v0, v17

    invoke-static {v0, v3}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 272
    sget-boolean v3, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v3, :cond_4

    .line 273
    const-string v3, "DownloadManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DescriptionParser : UpdateOmaDescriptionData : Obj Url After Encoding:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :cond_4
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/net/URI;->resolve(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v10

    .line 276
    .local v10, "AbsoluteUri":Ljava/net/URI;
    invoke-virtual {v10}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v12

    .line 283
    .end local v10    # "AbsoluteUri":Ljava/net/URI;
    .local v12, "RequestUrl":Ljava/lang/String;
    :goto_0
    const-string v3, "uri"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string v3, "dd_objUrl"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    sget-boolean v3, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v3, :cond_5

    .line 286
    const-string v3, "DownloadManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DescriptionParser : UpdateOmaDescriptionData : Absolute and Request Url:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 298
    .end local v11    # "MainUrl":Ljava/net/URI;
    .end local v12    # "RequestUrl":Ljava/lang/String;
    .end local v14    # "objUri":Landroid/net/Uri;
    :cond_5
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/android/providers/downloads/DescriptionInfo;->getContentName()Ljava/lang/String;

    move-result-object v17

    .line 299
    if-eqz v17, :cond_6

    .line 300
    const-string v3, "dd_fileName"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/android/providers/downloads/DescriptionInfo;->getVendor()Ljava/lang/String;

    move-result-object v17

    .line 304
    if-eqz v17, :cond_7

    .line 305
    const-string v3, "dd_vendor"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/android/providers/downloads/DescriptionInfo;->getDescription()Ljava/lang/String;

    move-result-object v17

    .line 308
    if-eqz v17, :cond_8

    .line 309
    const-string v3, "dd_description"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/android/providers/downloads/DescriptionInfo;->getInstallNotifyURI()Ljava/lang/String;

    move-result-object v17

    .line 312
    if-eqz v17, :cond_9

    .line 313
    const-string v3, "dd_notifyurl"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_9
    const-string v3, "downloadmethod"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 318
    return v15

    .line 278
    .restart local v11    # "MainUrl":Ljava/net/URI;
    .restart local v14    # "objUri":Landroid/net/Uri;
    :cond_a
    move-object/from16 v12, v17

    .line 279
    .restart local v12    # "RequestUrl":Ljava/lang/String;
    :try_start_1
    invoke-static/range {v17 .. v17}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 280
    .local v16, "temp":Landroid/net/Uri;
    new-instance v2, Ljava/net/URI;

    invoke-virtual/range {v16 .. v16}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Landroid/net/Uri;->getUserInfo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Landroid/net/Uri;->getPort()I

    move-result v6

    invoke-virtual/range {v16 .. v16}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v16 .. v16}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v16 .. v16}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v2 .. v9}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    .local v2, "finalUri":Ljava/net/URI;
    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v12

    goto/16 :goto_0

    .line 288
    .end local v2    # "finalUri":Ljava/net/URI;
    .end local v11    # "MainUrl":Ljava/net/URI;
    .end local v12    # "RequestUrl":Ljava/lang/String;
    .end local v14    # "objUri":Landroid/net/Uri;
    .end local v16    # "temp":Landroid/net/Uri;
    :catch_0
    move-exception v13

    .line 289
    .local v13, "e":Ljava/net/URISyntaxException;
    const-string v3, "DownloadManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DescriptionParser : UpdateOmaDescriptionData : URI Syntax Error:Main URL:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/downloads/DescriptionParser;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget-object v5, v5, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    const/4 v15, 0x0

    .line 295
    goto/16 :goto_1

    .line 292
    .end local v13    # "e":Ljava/net/URISyntaxException;
    :catch_1
    move-exception v13

    .line 293
    .local v13, "e":Ljava/lang/Exception;
    const-string v3, "DownloadManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DescriptionParser : UpdateOmaDescriptionData : Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/4 v15, 0x0

    goto/16 :goto_1
.end method
