.class public Lcom/android/providers/downloads/DescriptionInfo;
.super Ljava/lang/Object;
.source "DescriptionInfo.java"


# instance fields
.field holderForStartAndLength:[I

.field private mDDVersion:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private mIconURI:Ljava/lang/String;

.field private mInfoURL:Ljava/lang/String;

.field private mInstallNotifyURI:Ljava/lang/String;

.field private mInstallParam:Ljava/lang/String;

.field public mKeyname:Ljava/lang/String;

.field public mKeyvalue:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mNameSpace:Ljava/lang/String;

.field private mNextURL:Ljava/lang/String;

.field private mObjectURI:Ljava/lang/String;

.field private mRootElement:Ljava/lang/String;

.field private mSize:J

.field private mType:Ljava/lang/String;

.field private mType1:Ljava/lang/String;

.field private mType2:Ljava/lang/String;

.field private mVendor:Ljava/lang/String;

.field mXpp:Lorg/xmlpull/v1/XmlPullParser;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mObjectURI:Ljava/lang/String;

    .line 59
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mType:Ljava/lang/String;

    .line 61
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mType1:Ljava/lang/String;

    .line 63
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mType2:Ljava/lang/String;

    .line 67
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mInstallNotifyURI:Ljava/lang/String;

    .line 69
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mName:Ljava/lang/String;

    .line 71
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mDDVersion:Ljava/lang/String;

    .line 73
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mVendor:Ljava/lang/String;

    .line 75
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mDescription:Ljava/lang/String;

    .line 77
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mNextURL:Ljava/lang/String;

    .line 79
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mInfoURL:Ljava/lang/String;

    .line 81
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mIconURI:Ljava/lang/String;

    .line 83
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mInstallParam:Ljava/lang/String;

    .line 85
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mRootElement:Ljava/lang/String;

    .line 87
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mNameSpace:Ljava/lang/String;

    .line 91
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mXpp:Lorg/xmlpull/v1/XmlPullParser;

    .line 93
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    .line 95
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    .line 273
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->holderForStartAndLength:[I

    return-void
.end method


# virtual methods
.method public GetObjectURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mObjectURI:Ljava/lang/String;

    return-object v0
.end method

.method public GetRootElement()Ljava/lang/String;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mRootElement:Ljava/lang/String;

    return-object v0
.end method

.method public UpdateDDinfo()V
    .locals 4

    .prologue
    .line 350
    const-string v1, "objectURI"

    iget-object v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 351
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mObjectURI:Ljava/lang/String;

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    const-string v1, "type"

    iget-object v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 353
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mType:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 354
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mType:Ljava/lang/String;

    goto :goto_0

    .line 355
    :cond_2
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mType1:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 356
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mType1:Ljava/lang/String;

    goto :goto_0

    .line 357
    :cond_3
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mType2:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 358
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mType2:Ljava/lang/String;

    goto :goto_0

    .line 360
    :cond_4
    const-string v1, "size"

    iget-object v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 362
    :try_start_0
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mSize:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 363
    :catch_0
    move-exception v0

    .line 364
    .local v0, "e":Ljava/lang/Exception;
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mSize:J

    goto :goto_0

    .line 367
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    const-string v1, "installNotifyURI"

    iget-object v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 368
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mInstallNotifyURI:Ljava/lang/String;

    goto :goto_0

    .line 369
    :cond_6
    const-string v1, "name"

    iget-object v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 370
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mName:Ljava/lang/String;

    goto :goto_0

    .line 371
    :cond_7
    const-string v1, "DDVersion"

    iget-object v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 372
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mDDVersion:Ljava/lang/String;

    goto :goto_0

    .line 373
    :cond_8
    const-string v1, "vendor"

    iget-object v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 374
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mVendor:Ljava/lang/String;

    goto :goto_0

    .line 375
    :cond_9
    const-string v1, "description"

    iget-object v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 376
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mDescription:Ljava/lang/String;

    goto/16 :goto_0

    .line 377
    :cond_a
    const-string v1, "nextURL"

    iget-object v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 378
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mNextURL:Ljava/lang/String;

    goto/16 :goto_0

    .line 380
    :cond_b
    const-string v1, "infoURL"

    iget-object v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 381
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mInfoURL:Ljava/lang/String;

    goto/16 :goto_0

    .line 383
    :cond_c
    const-string v1, "iconURI"

    iget-object v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 384
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mIconURI:Ljava/lang/String;

    goto/16 :goto_0

    .line 386
    :cond_d
    const-string v1, "installParam"

    iget-object v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 387
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mInstallParam:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public getContentName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getDDVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mDDVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getInstallNotifyURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mInstallNotifyURI:Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public getSecMimeType1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mType1:Ljava/lang/String;

    return-object v0
.end method

.method public getSecMimeType2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mType2:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 436
    iget-wide v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mSize:J

    return-wide v0
.end method

.method public getVendor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mVendor:Ljava/lang/String;

    return-object v0
.end method

.method public parseDD(Ljava/lang/String;)V
    .locals 4
    .param p1, "xmlData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    const-string v1, "org.xmlpull.v1.XmlPullParserFactory"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance(Ljava/lang/String;Ljava/lang/Class;)Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    .line 157
    .local v0, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const-string v1, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->setFeature(Ljava/lang/String;Z)V

    .line 161
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mXpp:Lorg/xmlpull/v1/XmlPullParser;

    .line 164
    const-string v1, "Downloader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SecDescriptionInfo : parseDD: parser implementation class is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/providers/downloads/DescriptionInfo;->mXpp:Lorg/xmlpull/v1/XmlPullParser;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mXpp:Lorg/xmlpull/v1/XmlPullParser;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 175
    invoke-virtual {p0}, Lcom/android/providers/downloads/DescriptionInfo;->processDocument()V

    .line 177
    return-void
.end method

.method public processDocument()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 183
    iget-object v3, p0, Lcom/android/providers/downloads/DescriptionInfo;->mXpp:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 185
    .local v0, "eventType":I
    const/4 v1, 0x1

    .line 187
    .local v1, "flag":Z
    const/4 v2, 0x0

    .line 191
    .local v2, "rootvalidation":Z
    :cond_0
    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    .line 193
    iget-object v3, p0, Lcom/android/providers/downloads/DescriptionInfo;->mXpp:Lorg/xmlpull/v1/XmlPullParser;

    invoke-virtual {p0, v3}, Lcom/android/providers/downloads/DescriptionInfo;->processStartElement(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 195
    const/4 v1, 0x1

    .line 231
    :cond_1
    iget-object v3, p0, Lcom/android/providers/downloads/DescriptionInfo;->mXpp:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 233
    if-ne v0, v5, :cond_0

    .line 237
    :goto_0
    return-void

    .line 197
    :cond_2
    const/4 v3, 0x3

    if-eq v0, v3, :cond_1

    .line 201
    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    .line 203
    iget-object v3, p0, Lcom/android/providers/downloads/DescriptionInfo;->mXpp:Lorg/xmlpull/v1/XmlPullParser;

    invoke-virtual {p0, v3}, Lcom/android/providers/downloads/DescriptionInfo;->processText(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 205
    if-ne v1, v5, :cond_1

    .line 207
    invoke-virtual {p0}, Lcom/android/providers/downloads/DescriptionInfo;->UpdateDDinfo()V

    .line 209
    const/4 v1, 0x0

    .line 211
    if-nez v2, :cond_1

    .line 213
    const/4 v2, 0x1

    .line 215
    iget-object v3, p0, Lcom/android/providers/downloads/DescriptionInfo;->mRootElement:Ljava/lang/String;

    const-string v4, "media"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/providers/downloads/DescriptionInfo;->mNameSpace:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 219
    :cond_3
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/providers/downloads/DescriptionInfo;->mRootElement:Ljava/lang/String;

    goto :goto_0
.end method

.method public processStartElement(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3
    .param p1, "mXpp"    # Lorg/xmlpull/v1/XmlPullParser;

    .prologue
    .line 242
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 243
    .local v0, "name":Ljava/lang/String;
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v1

    .line 251
    .local v1, "uri":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/providers/downloads/DescriptionInfo;->mRootElement:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 252
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mRootElement:Ljava/lang/String;

    .line 253
    iput-object v1, p0, Lcom/android/providers/downloads/DescriptionInfo;->mNameSpace:Ljava/lang/String;

    .line 256
    :cond_0
    iput-object v0, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyname:Ljava/lang/String;

    .line 257
    return-void
.end method

.method public processText(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 7
    .param p1, "mXpp"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 278
    iget-object v5, p0, Lcom/android/providers/downloads/DescriptionInfo;->holderForStartAndLength:[I

    invoke-interface {p1, v5}, Lorg/xmlpull/v1/XmlPullParser;->getTextCharacters([I)[C

    move-result-object v1

    .line 279
    .local v1, "ch":[C
    if-eqz v1, :cond_1

    .line 281
    iget-object v5, p0, Lcom/android/providers/downloads/DescriptionInfo;->holderForStartAndLength:[I

    const/4 v6, 0x0

    aget v4, v5, v6

    .line 282
    .local v4, "start":I
    iget-object v5, p0, Lcom/android/providers/downloads/DescriptionInfo;->holderForStartAndLength:[I

    const/4 v6, 0x1

    aget v3, v5, v6

    .line 284
    .local v3, "length":I
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 286
    .local v0, "buf":Ljava/lang/StringBuffer;
    move v2, v4

    .local v2, "i":I
    :goto_0
    add-int v5, v4, v3

    if-ge v2, v5, :cond_0

    .line 289
    aget-char v5, v1, v2

    sparse-switch v5, :sswitch_data_0

    .line 329
    aget-char v5, v1, v2

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 286
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 294
    :sswitch_0
    const-string v5, "\\\\"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 301
    :sswitch_1
    const-string v5, "\\\""

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 308
    :sswitch_2
    const-string v5, "\\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 315
    :sswitch_3
    const-string v5, "\\r"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 322
    :sswitch_4
    const-string v5, "\\t"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 340
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/providers/downloads/DescriptionInfo;->mKeyvalue:Ljava/lang/String;

    .line 343
    .end local v0    # "buf":Ljava/lang/StringBuffer;
    .end local v2    # "i":I
    .end local v3    # "length":I
    .end local v4    # "start":I
    :cond_1
    return-void

    .line 289
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_4
        0xa -> :sswitch_2
        0xd -> :sswitch_3
        0x22 -> :sswitch_1
        0x5c -> :sswitch_0
    .end sparse-switch
.end method
