.class public Lcom/android/providers/downloads/DownloadService;
.super Landroid/app/Service;
.source "DownloadService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/downloads/DownloadService$DownloadManagerContentObserver;
    }
.end annotation


# static fields
.field private static sCleanupServiceName:Landroid/content/ComponentName;


# instance fields
.field private mAlarmManager:Landroid/app/AlarmManager;

.field private final mDownloads:Ljava/util/Map;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "mDownloads"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/providers/downloads/DownloadInfo;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mLastStartId:I

.field private mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

.field private mObserver:Lcom/android/providers/downloads/DownloadService$DownloadManagerContentObserver;

.field private mScanner:Lcom/android/providers/downloads/DownloadScanner;

.field mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

.field private mUpdateCallback:Landroid/os/Handler$Callback;

.field private mUpdateHandler:Landroid/os/Handler;

.field private mUpdateThread:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 110
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/android/providers/downloads/DownloadIdleService;

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/android/providers/downloads/DownloadIdleService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/providers/downloads/DownloadService;->sCleanupServiceName:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 120
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mDownloads:Ljava/util/Map;

    .line 317
    new-instance v0, Lcom/android/providers/downloads/DownloadService$1;

    invoke-direct {v0, p0}, Lcom/android/providers/downloads/DownloadService$1;-><init>(Lcom/android/providers/downloads/DownloadService;)V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateCallback:Landroid/os/Handler$Callback;

    return-void
.end method

.method static synthetic access$000(Lcom/android/providers/downloads/DownloadService;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadService;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mDownloads:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/providers/downloads/DownloadService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadService;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadService;->updateLocked()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/android/providers/downloads/DownloadService;)Lcom/android/providers/downloads/DownloadNotifier;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadService;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/providers/downloads/DownloadService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadService;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadService;->enqueueFinalUpdate()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/providers/downloads/DownloadService;)Lcom/android/providers/downloads/DownloadService$DownloadManagerContentObserver;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadService;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mObserver:Lcom/android/providers/downloads/DownloadService$DownloadManagerContentObserver;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/providers/downloads/DownloadService;)Lcom/android/providers/downloads/DownloadScanner;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadService;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mScanner:Lcom/android/providers/downloads/DownloadScanner;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/providers/downloads/DownloadService;)Landroid/os/HandlerThread;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadService;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateThread:Landroid/os/HandlerThread;

    return-object v0
.end method

.method private deleteDownloadLocked(J)V
    .locals 7
    .param p1, "id"    # J

    .prologue
    const/16 v6, 0x1ea

    .line 575
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadService;->mDownloads:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/providers/downloads/DownloadInfo;

    .line 578
    .local v1, "info":Lcom/android/providers/downloads/DownloadInfo;
    if-nez v1, :cond_0

    .line 579
    const-string v2, "DownloadManager"

    const-string v3, "couldn\'t get DownloadInfo"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    :goto_0
    return-void

    .line 583
    :cond_0
    sget-boolean v2, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v2, :cond_1

    .line 584
    const-string v2, "DownloadManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteDownload() info.mStatus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  info.mId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v1, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    :cond_1
    const/16 v2, 0xb6

    iget v3, v1, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    if-ne v2, v3, :cond_2

    .line 588
    iput v6, v1, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    .line 592
    :cond_2
    iget v2, v1, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    const/16 v3, 0xc0

    if-ne v2, v3, :cond_3

    .line 593
    iput v6, v1, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    .line 595
    :cond_3
    iget v2, v1, Lcom/android/providers/downloads/DownloadInfo;->mDestination:I

    if-eqz v2, :cond_4

    iget-object v2, v1, Lcom/android/providers/downloads/DownloadInfo;->mFileName:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 596
    sget-boolean v2, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v2, :cond_4

    .line 597
    const-string v2, "DownloadManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteDownloadLocked() deleting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/android/providers/downloads/DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    :cond_4
    iget v2, v1, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    if-ne v2, v6, :cond_5

    iget-object v2, v1, Lcom/android/providers/downloads/DownloadInfo;->mNotifyURI:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 603
    const/4 v2, 0x7

    iput v2, v1, Lcom/android/providers/downloads/DownloadInfo;->mState:I

    .line 604
    new-instance v0, Lcom/android/providers/downloads/NotifyThread;

    invoke-direct {v0, v1, p0}, Lcom/android/providers/downloads/NotifyThread;-><init>(Lcom/android/providers/downloads/DownloadInfo;Landroid/content/Context;)V

    .line 605
    .local v0, "NotifyThread":Lcom/android/providers/downloads/NotifyThread;
    invoke-virtual {v0}, Lcom/android/providers/downloads/NotifyThread;->start()V

    .line 606
    sget-boolean v2, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v2, :cond_5

    .line 607
    const-string v2, "DownloadManager"

    const-string v3, "DownloadService : deleteDownload : Deleted a download and Notify thread Strated"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    .end local v0    # "NotifyThread":Lcom/android/providers/downloads/NotifyThread;
    :cond_5
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadService;->mDownloads:Ljava/util/Map;

    iget-wide v4, v1, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private enqueueFinalUpdate()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 308
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 309
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateHandler:Landroid/os/Handler;

    iget v2, p0, Lcom/android/providers/downloads/DownloadService;->mLastStartId:I

    const/4 v3, -0x1

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 312
    return-void
.end method

.method private insertDownloadLocked(Lcom/android/providers/downloads/DownloadInfo$Reader;J)Lcom/android/providers/downloads/DownloadInfo;
    .locals 6
    .param p1, "reader"    # Lcom/android/providers/downloads/DownloadInfo$Reader;
    .param p2, "now"    # J

    .prologue
    .line 493
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadService;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadService;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    invoke-virtual {p1, p0, v1, v2}, Lcom/android/providers/downloads/DownloadInfo$Reader;->newDownloadInfo(Landroid/content/Context;Lcom/android/providers/downloads/SystemFacade;Lcom/android/providers/downloads/DownloadNotifier;)Lcom/android/providers/downloads/DownloadInfo;

    move-result-object v0

    .line 494
    .local v0, "info":Lcom/android/providers/downloads/DownloadInfo;
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadService;->mDownloads:Ljava/util/Map;

    iget-wide v2, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    sget-boolean v1, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v1, :cond_0

    .line 497
    const-string v1, "DownloadManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processing inserted download "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    :cond_0
    return-object v0
.end method

.method private isDownloadRunningListExist()Z
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 640
    invoke-virtual {p0}, Lcom/android/providers/downloads/DownloadService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 643
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 644
    .local v7, "idColumn":I
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 645
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 646
    .local v8, "id":J
    const-string v0, "status"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 647
    .local v10, "status":I
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v0, :cond_1

    .line 648
    const-string v0, "DownloadManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isDownloadRunningListExist Download status : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 650
    :cond_1
    const/16 v0, 0xc0

    if-ne v10, v0, :cond_0

    .line 651
    const/4 v0, 0x1

    .line 655
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 657
    .end local v8    # "id":J
    .end local v10    # "status":I
    :goto_0
    return v0

    .line 655
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 657
    const/4 v0, 0x0

    goto :goto_0

    .line 655
    .end local v7    # "idColumn":I
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private updateDownload(Lcom/android/providers/downloads/DownloadInfo$Reader;Lcom/android/providers/downloads/DownloadInfo;J)V
    .locals 11
    .param p1, "reader"    # Lcom/android/providers/downloads/DownloadInfo$Reader;
    .param p2, "info"    # Lcom/android/providers/downloads/DownloadInfo;
    .param p3, "now"    # J

    .prologue
    const/4 v10, 0x0

    .line 507
    invoke-virtual {p1, p2}, Lcom/android/providers/downloads/DownloadInfo$Reader;->updateFromDatabase(Lcom/android/providers/downloads/DownloadInfo;)V

    .line 508
    sget-boolean v6, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v6, :cond_0

    .line 509
    const-string v6, "DownloadManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "processing updated download "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p2, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", status: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p2, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    :cond_0
    iget v6, p2, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    const/16 v7, 0xb5

    if-ne v6, v7, :cond_4

    .line 514
    new-instance v3, Lcom/android/providers/downloads/DescriptionParser;

    iget-object v6, p2, Lcom/android/providers/downloads/DownloadInfo;->mFileName:Ljava/lang/String;

    invoke-direct {v3, p2, v6, p0}, Lcom/android/providers/downloads/DescriptionParser;-><init>(Lcom/android/providers/downloads/DownloadInfo;Ljava/lang/String;Landroid/content/Context;)V

    .line 515
    .local v3, "handle":Lcom/android/providers/downloads/DescriptionParser;
    iget-wide v6, p2, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v3, v6, v7}, Lcom/android/providers/downloads/DescriptionParser;->ParseData(J)Lcom/android/providers/downloads/DescriptionInfo;

    move-result-object v2

    .line 516
    .local v2, "ddInfo":Lcom/android/providers/downloads/DescriptionInfo;
    if-eqz v2, :cond_1

    .line 517
    new-instance v4, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW_DOWNLOADS"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 519
    .local v4, "intent":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0}, Lcom/android/providers/downloads/DownloadService;->existDownloadUi()Z

    move-result v6

    if-nez v6, :cond_3

    .line 520
    const v6, 0x8000

    invoke-virtual {v4, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 521
    const/high16 v6, 0x10000000

    invoke-virtual {v4, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 529
    :goto_0
    invoke-virtual {p0}, Lcom/android/providers/downloads/DownloadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 530
    const/4 v2, 0x0

    .line 532
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_1
    const/4 v3, 0x0

    .line 552
    .end local v2    # "ddInfo":Lcom/android/providers/downloads/DescriptionInfo;
    .end local v3    # "handle":Lcom/android/providers/downloads/DescriptionParser;
    :cond_2
    :goto_1
    return-void

    .line 523
    .restart local v2    # "ddInfo":Lcom/android/providers/downloads/DescriptionInfo;
    .restart local v3    # "handle":Lcom/android/providers/downloads/DescriptionParser;
    .restart local v4    # "intent":Landroid/content/Intent;
    :cond_3
    const/high16 v6, 0x10000000

    :try_start_1
    invoke-virtual {v4, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 526
    :catch_0
    move-exception v1

    .line 527
    .local v1, "_ex":Landroid/content/ActivityNotFoundException;
    const-string v6, "DownloadManager"

    const-string v7, "DownloadService : updateDownload : ActivityNotFoundException : No application for processing ACTION_VIEW_DOWNLOADS"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 536
    .end local v1    # "_ex":Landroid/content/ActivityNotFoundException;
    .end local v2    # "ddInfo":Lcom/android/providers/downloads/DescriptionInfo;
    .end local v3    # "handle":Lcom/android/providers/downloads/DescriptionParser;
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_4
    const/4 v6, 0x6

    iget v7, p2, Lcom/android/providers/downloads/DownloadInfo;->mState:I

    if-ne v6, v7, :cond_2

    .line 537
    sget-boolean v6, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v6, :cond_5

    .line 538
    const-string v6, "DownloadManager"

    const-string v7, "DownloadService : updateDownload :  set the status to CD_STATUS_NOTIFICATION-->"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    :cond_5
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 541
    .local v5, "values":Landroid/content/ContentValues;
    const-string v6, "state"

    const/4 v7, 0x7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 543
    invoke-virtual {p0}, Lcom/android/providers/downloads/DownloadService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    iget-wide v8, p2, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7, v5, v10, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 544
    new-instance v0, Lcom/android/providers/downloads/NotifyThread;

    invoke-direct {v0, p2, p0}, Lcom/android/providers/downloads/NotifyThread;-><init>(Lcom/android/providers/downloads/DownloadInfo;Landroid/content/Context;)V

    .line 545
    .local v0, "Notification":Lcom/android/providers/downloads/NotifyThread;
    invoke-virtual {v0}, Lcom/android/providers/downloads/NotifyThread;->start()V

    .line 546
    sget-boolean v6, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v6, :cond_2

    .line 547
    const-string v6, "DownloadManager"

    const-string v7, "DownloadService : updateDownload : Successfully Updated a download in Service and Notify thread Started"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private updateLocked()Z
    .locals 30

    .prologue
    .line 391
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadService;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    invoke-interface {v7}, Lcom/android/providers/downloads/SystemFacade;->currentTimeMillis()J

    move-result-wide v26

    .line 393
    .local v26, "now":J
    const/16 v22, 0x0

    .line 394
    .local v22, "isActive":Z
    const-wide v24, 0x7fffffffffffffffL

    .line 396
    .local v24, "nextActionMillis":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadService;->mDownloads:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-static {v7}, Lcom/google/common/collect/Sets;->newHashSet(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v28

    .line 398
    .local v28, "staleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 399
    .local v6, "resolver":Landroid/content/ContentResolver;
    sget-object v7, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 402
    .local v15, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v23, Lcom/android/providers/downloads/DownloadInfo$Reader;

    move-object/from16 v0, v23

    invoke-direct {v0, v6, v15}, Lcom/android/providers/downloads/DownloadInfo$Reader;-><init>(Landroid/content/ContentResolver;Landroid/database/Cursor;)V

    .line 403
    .local v23, "reader":Lcom/android/providers/downloads/DownloadInfo$Reader;
    const-string v7, "_id"

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    .line 404
    .local v17, "idColumn":I
    :goto_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 405
    move/from16 v0, v17

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 406
    .local v18, "id":J
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v28

    invoke-interface {v0, v7}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 408
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadService;->mDownloads:Ljava/util/Map;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/android/providers/downloads/DownloadInfo;

    .line 409
    .local v20, "info":Lcom/android/providers/downloads/DownloadInfo;
    if-eqz v20, :cond_2

    .line 410
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v20

    move-wide/from16 v3, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/downloads/DownloadService;->updateDownload(Lcom/android/providers/downloads/DownloadInfo$Reader;Lcom/android/providers/downloads/DownloadInfo;J)V

    .line 415
    :goto_1
    move-object/from16 v0, v20

    iget-boolean v7, v0, Lcom/android/providers/downloads/DownloadInfo;->mDeleted:Z

    if-eqz v7, :cond_3

    .line 417
    const/16 v7, 0xb6

    move-object/from16 v0, v20

    iget v8, v0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    if-ne v7, v8, :cond_0

    .line 418
    const/16 v7, 0x1ea

    move-object/from16 v0, v20

    iput v7, v0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    .line 421
    :cond_0
    move-object/from16 v0, v20

    iget v7, v0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    const/16 v8, 0x1ea

    if-ne v7, v8, :cond_1

    move-object/from16 v0, v20

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadInfo;->mNotifyURI:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 422
    const/4 v7, 0x7

    move-object/from16 v0, v20

    iput v7, v0, Lcom/android/providers/downloads/DownloadInfo;->mState:I

    .line 423
    new-instance v12, Lcom/android/providers/downloads/NotifyThread;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v12, v0, v1}, Lcom/android/providers/downloads/NotifyThread;-><init>(Lcom/android/providers/downloads/DownloadInfo;Landroid/content/Context;)V

    .line 424
    .local v12, "NotifyThread":Lcom/android/providers/downloads/NotifyThread;
    invoke-virtual {v12}, Lcom/android/providers/downloads/NotifyThread;->start()V

    .line 425
    sget-boolean v7, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v7, :cond_1

    .line 426
    const-string v7, "DownloadManager"

    const-string v8, "DownloadService :  Cancel a download and Notify thread Strated"

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    .end local v12    # "NotifyThread":Lcom/android/providers/downloads/NotifyThread;
    :cond_1
    invoke-virtual/range {v20 .. v20}, Lcom/android/providers/downloads/DownloadInfo;->getAllDownloadsUri()Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 458
    :goto_2
    move-object/from16 v0, v20

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lcom/android/providers/downloads/DownloadInfo;->nextActionMillis(J)J

    move-result-wide v8

    move-wide/from16 v0, v24

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v24

    .line 459
    goto/16 :goto_0

    .line 412
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-wide/from16 v2, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/downloads/DownloadService;->insertDownloadLocked(Lcom/android/providers/downloads/DownloadInfo$Reader;J)Lcom/android/providers/downloads/DownloadInfo;

    move-result-object v20

    goto :goto_1

    .line 443
    :cond_3
    invoke-virtual/range {v20 .. v20}, Lcom/android/providers/downloads/DownloadInfo;->startDownloadIfReady()Z

    move-result v13

    .line 446
    .local v13, "activeDownload":Z
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadService;->mScanner:Lcom/android/providers/downloads/DownloadScanner;

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Lcom/android/providers/downloads/DownloadInfo;->startScanIfReady(Lcom/android/providers/downloads/DownloadScanner;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v14

    .line 453
    .local v14, "activeScan":Z
    or-int v22, v22, v13

    .line 454
    or-int v22, v22, v14

    goto :goto_2

    .line 461
    .end local v13    # "activeDownload":Z
    .end local v14    # "activeScan":Z
    .end local v18    # "id":J
    .end local v20    # "info":Lcom/android/providers/downloads/DownloadInfo;
    :cond_4
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 465
    invoke-interface/range {v28 .. v28}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Long;

    .line 466
    .local v18, "id":Ljava/lang/Long;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/android/providers/downloads/DownloadService;->deleteDownloadLocked(J)V

    goto :goto_3

    .line 461
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v17    # "idColumn":I
    .end local v18    # "id":Ljava/lang/Long;
    .end local v23    # "reader":Lcom/android/providers/downloads/DownloadInfo$Reader;
    :catchall_0
    move-exception v7

    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    throw v7

    .line 470
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v17    # "idColumn":I
    .restart local v23    # "reader":Lcom/android/providers/downloads/DownloadInfo$Reader;
    :cond_5
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadService;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/providers/downloads/DownloadService;->mDownloads:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/providers/downloads/DownloadNotifier;->updateWith(Ljava/util/Collection;)V

    .line 474
    const-wide/16 v8, 0x0

    cmp-long v7, v24, v8

    if-lez v7, :cond_7

    const-wide v8, 0x7fffffffffffffffL

    cmp-long v7, v24, v8

    if-gez v7, :cond_7

    .line 475
    sget-boolean v7, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v7, :cond_6

    .line 476
    const-string v7, "DownloadManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "scheduling start in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-wide/from16 v0, v24

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "ms"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    :cond_6
    new-instance v21, Landroid/content/Intent;

    const-string v7, "android.intent.action.DOWNLOAD_WAKEUP"

    move-object/from16 v0, v21

    invoke-direct {v0, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 480
    .local v21, "intent":Landroid/content/Intent;
    const-class v7, Lcom/android/providers/downloads/DownloadReceiver;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 481
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/providers/downloads/DownloadService;->mAlarmManager:Landroid/app/AlarmManager;

    const/4 v8, 0x0

    add-long v10, v26, v24

    const/4 v9, 0x0

    const/high16 v29, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v29

    invoke-static {v0, v9, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v7, v8, v10, v11, v9}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 485
    .end local v21    # "intent":Landroid/content/Intent;
    :cond_7
    return v22
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 7
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "writer"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 629
    new-instance v4, Lcom/android/internal/util/IndentingPrintWriter;

    const-string v5, "  "

    invoke-direct {v4, p2, v5}, Lcom/android/internal/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    .line 630
    .local v4, "pw":Lcom/android/internal/util/IndentingPrintWriter;
    iget-object v6, p0, Lcom/android/providers/downloads/DownloadService;->mDownloads:Ljava/util/Map;

    monitor-enter v6

    .line 631
    :try_start_0
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadService;->mDownloads:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 632
    .local v2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 633
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 634
    .local v1, "id":Ljava/lang/Long;
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadService;->mDownloads:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/providers/downloads/DownloadInfo;

    .line 635
    .local v3, "info":Lcom/android/providers/downloads/DownloadInfo;
    invoke-virtual {v3, v4}, Lcom/android/providers/downloads/DownloadInfo;->dump(Lcom/android/internal/util/IndentingPrintWriter;)V

    goto :goto_0

    .line 637
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "id":Ljava/lang/Long;
    .end local v2    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v3    # "info":Lcom/android/providers/downloads/DownloadInfo;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :cond_0
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 638
    return-void
.end method

.method public enqueueUpdate()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 297
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 299
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/android/providers/downloads/DownloadService;->mLastStartId:I

    const/4 v2, -0x1

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 301
    :cond_0
    return-void
.end method

.method public existDownloadUi()Z
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 555
    invoke-virtual {p0}, Lcom/android/providers/downloads/DownloadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 556
    .local v5, "mPm":Landroid/content/pm/PackageManager;
    new-instance v3, Landroid/content/Intent;

    const-string v8, "android.intent.action.MAIN"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 557
    .local v3, "intent":Landroid/content/Intent;
    const-string v8, "android.intent.category.INFO"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 558
    invoke-virtual {v5, v3, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    .line 559
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    .line 560
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 562
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 563
    .local v1, "firstInfo":Landroid/content/pm/ResolveInfo;
    iget-object v8, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v8, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 564
    .local v6, "packageName":Ljava/lang/String;
    if-eqz v6, :cond_1

    const-string v8, "com.android.providers.downloads.ui"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 565
    const/4 v7, 0x1

    .line 568
    .end local v1    # "firstInfo":Landroid/content/pm/ResolveInfo;
    .end local v6    # "packageName":Ljava/lang/String;
    :cond_0
    return v7

    .line 560
    .restart local v1    # "firstInfo":Landroid/content/pm/ResolveInfo;
    .restart local v6    # "packageName":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 196
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot bind to Download Manager Service"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 204
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 205
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v0, :cond_0

    .line 206
    const-string v0, "DownloadManager"

    const-string v1, "Service onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    if-nez v0, :cond_1

    .line 210
    new-instance v0, Lcom/android/providers/downloads/RealSystemFacade;

    invoke-direct {v0, p0}, Lcom/android/providers/downloads/RealSystemFacade;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mSystemFacade:Lcom/android/providers/downloads/SystemFacade;

    .line 213
    :cond_1
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/android/providers/downloads/DownloadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mAlarmManager:Landroid/app/AlarmManager;

    .line 215
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "DownloadManager-UpdateThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateThread:Landroid/os/HandlerThread;

    .line 216
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 217
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateCallback:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateHandler:Landroid/os/Handler;

    .line 219
    new-instance v0, Lcom/android/providers/downloads/DownloadScanner;

    invoke-direct {v0, p0}, Lcom/android/providers/downloads/DownloadScanner;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mScanner:Lcom/android/providers/downloads/DownloadScanner;

    .line 220
    new-instance v0, Lcom/android/providers/downloads/DownloadNotifier;

    invoke-direct {v0, p0}, Lcom/android/providers/downloads/DownloadNotifier;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    .line 222
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadService;->isDownloadRunningListExist()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 223
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mNotifier:Lcom/android/providers/downloads/DownloadNotifier;

    invoke-virtual {v0}, Lcom/android/providers/downloads/DownloadNotifier;->cancelAll()V

    .line 225
    :cond_2
    new-instance v0, Lcom/android/providers/downloads/DownloadService$DownloadManagerContentObserver;

    invoke-direct {v0, p0}, Lcom/android/providers/downloads/DownloadService$DownloadManagerContentObserver;-><init>(Lcom/android/providers/downloads/DownloadService;)V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mObserver:Lcom/android/providers/downloads/DownloadService$DownloadManagerContentObserver;

    .line 226
    invoke-virtual {p0}, Lcom/android/providers/downloads/DownloadService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/providers/downloads/DownloadService;->mObserver:Lcom/android/providers/downloads/DownloadService$DownloadManagerContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 240
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/android/providers/downloads/DownloadService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadService;->mObserver:Lcom/android/providers/downloads/DownloadService$DownloadManagerContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 285
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mScanner:Lcom/android/providers/downloads/DownloadScanner;

    invoke-virtual {v0}, Lcom/android/providers/downloads/DownloadScanner;->shutdown()V

    .line 286
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadService;->mUpdateThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 287
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v0, :cond_0

    .line 288
    const-string v0, "DownloadManager"

    const-string v1, "Service onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 291
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 256
    if-nez p1, :cond_0

    .line 257
    const-string v2, "DownloadManager"

    const-string v3, "onStartCommand() : intent is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    const/4 v1, 0x2

    .line 279
    :goto_0
    return v1

    .line 260
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    .line 262
    .local v1, "returnValue":I
    const-string v2, "eng"

    const-string v3, "ro.build.type"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 263
    new-instance v0, Ljava/io/File;

    const-string v2, "/storage/emulated/0/Download/Debug.ini"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 265
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-ne v2, v4, :cond_3

    .line 266
    sput-boolean v4, Lcom/android/providers/downloads/Constants;->LOGV:Z

    .line 267
    sput-boolean v4, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    .line 274
    .end local v0    # "file":Ljava/io/File;
    :cond_1
    :goto_1
    sget-boolean v2, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v2, :cond_2

    .line 275
    const-string v2, "DownloadManager"

    const-string v3, "Service onStart"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :cond_2
    iput p3, p0, Lcom/android/providers/downloads/DownloadService;->mLastStartId:I

    .line 278
    invoke-virtual {p0}, Lcom/android/providers/downloads/DownloadService;->enqueueUpdate()V

    goto :goto_0

    .line 270
    .restart local v0    # "file":Ljava/io/File;
    :cond_3
    sput-boolean v5, Lcom/android/providers/downloads/Constants;->LOGV:Z

    .line 271
    sput-boolean v5, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    goto :goto_1
.end method
