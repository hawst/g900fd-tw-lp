.class Lcom/android/providers/downloads/DownloadCAC$1;
.super Landroid/os/AsyncTask;
.source "DownloadCAC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/providers/downloads/DownloadCAC;->finalize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/providers/downloads/DownloadCAC;


# direct methods
.method constructor <init>(Lcom/android/providers/downloads/DownloadCAC;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/android/providers/downloads/DownloadCAC$1;->this$0:Lcom/android/providers/downloads/DownloadCAC;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 156
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/providers/downloads/DownloadCAC$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "unused"    # [Ljava/lang/Void;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC$1;->this$0:Lcom/android/providers/downloads/DownloadCAC;

    # getter for: Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;
    invoke-static {v0}, Lcom/android/providers/downloads/DownloadCAC;->access$000(Lcom/android/providers/downloads/DownloadCAC;)Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->unregisterOpenSSLEngine()Z

    .line 162
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC$1;->this$0:Lcom/android/providers/downloads/DownloadCAC;

    # getter for: Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;
    invoke-static {v0}, Lcom/android/providers/downloads/DownloadCAC;->access$000(Lcom/android/providers/downloads/DownloadCAC;)Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->unregisterProvider()Z

    .line 163
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC$1;->this$0:Lcom/android/providers/downloads/DownloadCAC;

    # getter for: Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;
    invoke-static {v0}, Lcom/android/providers/downloads/DownloadCAC;->access$000(Lcom/android/providers/downloads/DownloadCAC;)Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->deinitialize()V

    .line 165
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 156
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/providers/downloads/DownloadCAC$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC$1;->this$0:Lcom/android/providers/downloads/DownloadCAC;

    sget-object v1, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_DISCONNECTED:Lcom/android/providers/downloads/DownloadCAC$STATE;

    # setter for: Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;
    invoke-static {v0, v1}, Lcom/android/providers/downloads/DownloadCAC;->access$102(Lcom/android/providers/downloads/DownloadCAC;Lcom/android/providers/downloads/DownloadCAC$STATE;)Lcom/android/providers/downloads/DownloadCAC$STATE;

    .line 170
    return-void
.end method
