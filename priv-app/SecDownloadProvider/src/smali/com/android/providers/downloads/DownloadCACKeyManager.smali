.class public Lcom/android/providers/downloads/DownloadCACKeyManager;
.super Ljavax/net/ssl/X509ExtendedKeyManager;
.source "DownloadCACKeyManager.java"


# instance fields
.field private mClientAlias:Ljava/lang/String;

.field private mEntry:Ljava/security/PrivateKey;

.field private pkcs11KS:Ljava/security/KeyStore;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Ljavax/net/ssl/X509ExtendedKeyManager;-><init>()V

    .line 48
    iput-object v1, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->mEntry:Ljava/security/PrivateKey;

    .line 49
    iput-object v1, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->mClientAlias:Ljava/lang/String;

    .line 54
    sget-boolean v1, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v1, :cond_0

    .line 55
    const-string v1, "DownloadCACManager"

    const-string v2, "DownloadCACKeyManager : DownloadCACKeyManager()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :cond_0
    :try_start_0
    const-string v1, "PKCS11"

    const-string v2, "SECPkcs11"

    invoke-static {v1, v2}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1

    iput-object v1, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->pkcs11KS:Ljava/security/KeyStore;

    .line 59
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->pkcs11KS:Ljava/security/KeyStore;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :cond_1
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/lang/Exception;
    sget-boolean v1, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v1, :cond_1

    .line 62
    const-string v1, "DownloadCACManager"

    const-string v2, "DownloadCACKeyManager : exception : pkcs11 load"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private chooseClientAliasInternal()Ljava/lang/String;
    .locals 17

    .prologue
    .line 144
    const/4 v2, 0x0

    .line 145
    .local v2, "aliasChosen":Ljava/lang/String;
    const/4 v12, 0x0

    .line 146
    .local v12, "preferedAlias":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/providers/downloads/DownloadCACKeyManager;->pkcs11KS:Ljava/security/KeyStore;

    if-eqz v14, :cond_4

    .line 147
    const/4 v13, 0x0

    .line 148
    .local v13, "pubCert":Ljava/security/cert/X509Certificate;
    invoke-direct/range {p0 .. p0}, Lcom/android/providers/downloads/DownloadCACKeyManager;->getClientAliasesInternal()[Ljava/lang/String;

    move-result-object v3

    .line 149
    .local v3, "aliases":[Ljava/lang/String;
    if-nez v3, :cond_0

    .line 150
    const/4 v14, 0x0

    .line 193
    .end local v3    # "aliases":[Ljava/lang/String;
    .end local v13    # "pubCert":Ljava/security/cert/X509Certificate;
    :goto_0
    return-object v14

    .line 152
    .restart local v3    # "aliases":[Ljava/lang/String;
    .restart local v13    # "pubCert":Ljava/security/cert/X509Certificate;
    :cond_0
    move-object v4, v3

    .local v4, "arr$":[Ljava/lang/String;
    :try_start_0
    array-length v10, v4

    .local v10, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_1
    if-ge v8, v10, :cond_3

    aget-object v1, v4, v8

    .line 154
    .local v1, "alias":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/providers/downloads/DownloadCACKeyManager;->pkcs11KS:Ljava/security/KeyStore;

    invoke-virtual {v14, v1}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v14

    move-object v0, v14

    check-cast v0, Ljava/security/cert/X509Certificate;

    move-object v13, v0

    .line 155
    invoke-virtual {v13}, Ljava/security/cert/X509Certificate;->getKeyUsage()[Z

    move-result-object v9

    .line 157
    .local v9, "keyUsages":[Z
    const/4 v14, 0x0

    aget-boolean v14, v9, v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_5

    .line 159
    invoke-virtual {v13}, Ljava/security/cert/X509Certificate;->getExtendedKeyUsage()Ljava/util/List;

    move-result-object v7

    .line 160
    .local v7, "extendedKeyUsages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v7, :cond_2

    .line 161
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    .line 162
    .local v11, "numExtendedKeyUsages":I
    const-string v14, "DownloadCACManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "chooseClientAliasInternal : extendedKeyUsages "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 164
    .local v6, "extKeyUsagesStr":Ljava/lang/String;
    const-string v14, "1.3.6.1.5.5.7.3.2"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_1

    .line 165
    const-string v14, "DownloadCACManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "chooseClientAliasInternal : preferedAlias "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 166
    move-object v12, v1

    goto :goto_2

    .line 170
    .end local v6    # "extKeyUsagesStr":Ljava/lang/String;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v11    # "numExtendedKeyUsages":I
    :cond_2
    move-object v2, v1

    .line 174
    .end local v1    # "alias":Ljava/lang/String;
    .end local v7    # "extendedKeyUsages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9    # "keyUsages":[Z
    :cond_3
    if-eqz v12, :cond_4

    .line 175
    move-object v2, v12

    .line 192
    .end local v3    # "aliases":[Ljava/lang/String;
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v10    # "len$":I
    .end local v13    # "pubCert":Ljava/security/cert/X509Certificate;
    :cond_4
    :goto_3
    const-string v14, "DownloadCACManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "chooseClientAliasInternal :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v14, v2

    .line 193
    goto/16 :goto_0

    .line 152
    .restart local v1    # "alias":Ljava/lang/String;
    .restart local v3    # "aliases":[Ljava/lang/String;
    .restart local v4    # "arr$":[Ljava/lang/String;
    .local v8, "i$":I
    .restart local v9    # "keyUsages":[Z
    .restart local v10    # "len$":I
    .restart local v13    # "pubCert":Ljava/security/cert/X509Certificate;
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 177
    .end local v1    # "alias":Ljava/lang/String;
    .end local v8    # "i$":I
    .end local v9    # "keyUsages":[Z
    .end local v10    # "len$":I
    :catch_0
    move-exception v5

    .line 179
    .local v5, "e":Ljava/security/KeyStoreException;
    invoke-virtual {v5}, Ljava/security/KeyStoreException;->printStackTrace()V

    goto :goto_3

    .line 180
    .end local v5    # "e":Ljava/security/KeyStoreException;
    :catch_1
    move-exception v5

    .line 182
    .local v5, "e":Ljava/security/cert/CertificateException;
    invoke-virtual {v5}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto :goto_3

    .line 183
    .end local v5    # "e":Ljava/security/cert/CertificateException;
    :catch_2
    move-exception v5

    .line 187
    .local v5, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v5}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_3
.end method

.method private getClientAliasesInternal()[Ljava/lang/String;
    .locals 8

    .prologue
    .line 198
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->pkcs11KS:Ljava/security/KeyStore;

    if-eqz v5, :cond_0

    .line 200
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 201
    .local v3, "aliasesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->pkcs11KS:Ljava/security/KeyStore;

    invoke-virtual {v5}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v2

    .line 202
    .local v2, "aliasesEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 203
    .local v0, "alias":Ljava/lang/String;
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 204
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "alias":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 205
    .restart local v0    # "alias":Ljava/lang/String;
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    const-string v5, "DownloadCACManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getClientAliasesInternal :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 214
    .end local v0    # "alias":Ljava/lang/String;
    .end local v2    # "aliasesEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    .end local v3    # "aliasesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v4

    .line 216
    .local v4, "e":Ljava/security/KeyStoreException;
    invoke-virtual {v4}, Ljava/security/KeyStoreException;->printStackTrace()V

    .line 219
    .end local v4    # "e":Ljava/security/KeyStoreException;
    :cond_0
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 209
    .restart local v0    # "alias":Ljava/lang/String;
    .restart local v2    # "aliasesEnum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    .restart local v3    # "aliasesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 210
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v1, v5, [Ljava/lang/String;

    .line 211
    .local v1, "aliases":[Ljava/lang/String;
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "aliases":[Ljava/lang/String;
    check-cast v1, [Ljava/lang/String;
    :try_end_1
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_0

    .line 212
    .restart local v1    # "aliases":[Ljava/lang/String;
    goto :goto_1
.end method


# virtual methods
.method public chooseClientAlias([Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;
    .locals 3
    .param p1, "keyType"    # [Ljava/lang/String;
    .param p2, "issuers"    # [Ljava/security/Principal;
    .param p3, "socket"    # Ljava/net/Socket;

    .prologue
    .line 68
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_0

    .line 69
    const-string v0, "DownloadCACManager"

    const-string v1, "DownloadCACKeyManager : chooseClientAlias"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->mClientAlias:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 72
    const-string v0, "DownloadCACManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "chooseClientAlias "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->mClientAlias:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->mClientAlias:Ljava/lang/String;

    .line 76
    :goto_0
    return-object v0

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->pkcs11KS:Ljava/security/KeyStore;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->mClientAlias:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 75
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadCACKeyManager;->chooseClientAliasInternal()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 76
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public chooseServerAlias(Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;
    .locals 1
    .param p1, "keyType"    # Ljava/lang/String;
    .param p2, "issuers"    # [Ljava/security/Principal;
    .param p3, "socket"    # Ljava/net/Socket;

    .prologue
    .line 81
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .locals 7
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 85
    const/4 v1, 0x0

    .line 87
    .local v1, "certificateChain":[Ljava/security/cert/X509Certificate;
    :try_start_0
    iget-object v4, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->pkcs11KS:Ljava/security/KeyStore;

    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Ljava/security/KeyStore;->getEntry(Ljava/lang/String;Ljava/security/KeyStore$ProtectionParameter;)Ljava/security/KeyStore$Entry;

    move-result-object v3

    check-cast v3, Ljava/security/KeyStore$PrivateKeyEntry;

    .line 89
    .local v3, "keyEntry":Ljava/security/KeyStore$PrivateKeyEntry;
    invoke-virtual {v3}, Ljava/security/KeyStore$PrivateKeyEntry;->getCertificateChain()[Ljava/security/cert/Certificate;

    move-result-object v4

    check-cast v4, [Ljava/security/cert/X509Certificate;

    move-object v0, v4

    check-cast v0, [Ljava/security/cert/X509Certificate;

    move-object v1, v0

    .line 91
    const-string v4, "DownloadCACManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The certificate chain: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/UnrecoverableEntryException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3

    .line 108
    .end local v3    # "keyEntry":Ljava/security/KeyStore$PrivateKeyEntry;
    :goto_0
    return-object v1

    .line 92
    :catch_0
    move-exception v2

    .line 94
    .local v2, "e":Ljava/security/KeyStoreException;
    invoke-virtual {v2}, Ljava/security/KeyStoreException;->printStackTrace()V

    goto :goto_0

    .line 95
    .end local v2    # "e":Ljava/security/KeyStoreException;
    :catch_1
    move-exception v2

    .line 97
    .local v2, "e":Ljava/security/UnrecoverableEntryException;
    invoke-virtual {v2}, Ljava/security/UnrecoverableEntryException;->printStackTrace()V

    goto :goto_0

    .line 98
    .end local v2    # "e":Ljava/security/UnrecoverableEntryException;
    :catch_2
    move-exception v2

    .line 100
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 101
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_3
    move-exception v2

    .line 105
    .local v2, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public getClientAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;
    .locals 3
    .param p1, "keyType"    # Ljava/lang/String;
    .param p2, "issuers"    # [Ljava/security/Principal;

    .prologue
    .line 112
    sget-boolean v1, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v1, :cond_0

    .line 113
    const-string v1, "DownloadCACManager"

    const-string v2, "DownloadCACKeyManager : getClientAliases"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_0
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadCACKeyManager;->getClientAliasesInternal()[Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "aliases":[Ljava/lang/String;
    return-object v0
.end method

.method public getPrivateKey(Ljava/lang/String;)Ljava/security/PrivateKey;
    .locals 4
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 121
    sget-boolean v2, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v2, :cond_0

    .line 122
    const-string v2, "DownloadCACManager"

    const-string v3, "DownloadCACKeyManager : getPrivateKey"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_0
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->mEntry:Ljava/security/PrivateKey;

    if-nez v2, :cond_1

    .line 125
    const-string v2, "secpkcs11"

    invoke-static {v2}, Lcom/android/org/conscrypt/OpenSSLEngine;->getInstance(Ljava/lang/String;)Lcom/android/org/conscrypt/OpenSSLEngine;

    move-result-object v1

    .line 127
    .local v1, "engine":Lcom/android/org/conscrypt/OpenSSLEngine;
    :try_start_0
    invoke-virtual {v1, p1}, Lcom/android/org/conscrypt/OpenSSLEngine;->getPrivateKeyById(Ljava/lang/String;)Ljava/security/PrivateKey;

    move-result-object v2

    iput-object v2, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->mEntry:Ljava/security/PrivateKey;
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    .end local v1    # "engine":Lcom/android/org/conscrypt/OpenSSLEngine;
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->mEntry:Ljava/security/PrivateKey;

    return-object v2

    .line 129
    .restart local v1    # "engine":Lcom/android/org/conscrypt/OpenSSLEngine;
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Ljava/security/InvalidKeyException;
    invoke-virtual {v0}, Ljava/security/InvalidKeyException;->printStackTrace()V

    goto :goto_0
.end method

.method public getServerAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;
    .locals 1
    .param p1, "keyType"    # Ljava/lang/String;
    .param p2, "issuers"    # [Ljava/security/Principal;

    .prologue
    .line 140
    const/4 v0, 0x0

    return-object v0
.end method

.method public setKeyStoreClientAlias(Ljava/lang/String;)V
    .locals 3
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 224
    const-string v0, "DownloadCACManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setKeyStoreClientAlias "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    iput-object p1, p0, Lcom/android/providers/downloads/DownloadCACKeyManager;->mClientAlias:Ljava/lang/String;

    .line 226
    return-void
.end method
