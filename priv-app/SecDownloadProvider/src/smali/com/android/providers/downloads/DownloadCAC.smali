.class public Lcom/android/providers/downloads/DownloadCAC;
.super Ljava/lang/Object;
.source "DownloadCAC.java"

# interfaces
.implements Lcom/sec/enterprise/knox/smartcard/SmartCardHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/downloads/DownloadCAC$DownloadItem;,
        Lcom/android/providers/downloads/DownloadCAC$STATE;
    }
.end annotation


# static fields
.field private static instance_:Lcom/android/providers/downloads/DownloadCAC;

.field private static pendingItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/providers/downloads/DownloadCAC$DownloadItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private downloadCount:I

.field private mContext:Landroid/content/Context;

.field private mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

.field private mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

.field private final timeout_ms:I

.field private timer:Ljava/util/Timer;

.field private timerTask:Ljava/util/TimerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/android/providers/downloads/DownloadCAC;->instance_:Lcom/android/providers/downloads/DownloadCAC;

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/providers/downloads/DownloadCAC;->pendingItems:Ljava/util/ArrayList;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/android/providers/downloads/DownloadCAC;->timeout_ms:I

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/providers/downloads/DownloadCAC;->downloadCount:I

    .line 129
    sget-object v0, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_DISCONNECTED:Lcom/android/providers/downloads/DownloadCAC$STATE;

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    .line 130
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mContext:Landroid/content/Context;

    .line 131
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    .line 132
    return-void
.end method

.method static synthetic access$000(Lcom/android/providers/downloads/DownloadCAC;)Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadCAC;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/providers/downloads/DownloadCAC;Lcom/android/providers/downloads/DownloadCAC$STATE;)Lcom/android/providers/downloads/DownloadCAC$STATE;
    .locals 0
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadCAC;
    .param p1, "x1"    # Lcom/android/providers/downloads/DownloadCAC$STATE;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/providers/downloads/DownloadCAC;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadCAC;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadCAC;->checkConnectedState()V

    return-void
.end method

.method private declared-synchronized checkConnectedState()V
    .locals 3

    .prologue
    .line 184
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_0

    .line 185
    const-string v0, "DownloadCAC"

    const-string v1, "checkConnectedState()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->timer:Ljava/util/Timer;

    .line 188
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    sget-object v1, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_CONNECTED:Lcom/android/providers/downloads/DownloadCAC$STATE;

    if-eq v0, v1, :cond_2

    .line 189
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_1

    .line 190
    const-string v0, "DownloadCAC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkConnectedState() - timeout :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/providers/downloads/DownloadCAC;->onStatusChanged(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :cond_2
    monitor-exit p0

    return-void

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized checkPendingAndStart()V
    .locals 7

    .prologue
    .line 223
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v1, :cond_0

    .line 224
    const-string v1, "DownloadCAC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkPendingAndStart() - pending items : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/providers/downloads/DownloadCAC;->pendingItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_0
    :goto_0
    iget v1, p0, Lcom/android/providers/downloads/DownloadCAC;->downloadCount:I

    if-nez v1, :cond_3

    sget-object v1, Lcom/android/providers/downloads/DownloadCAC;->pendingItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 228
    sget-object v1, Lcom/android/providers/downloads/DownloadCAC;->pendingItems:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;

    .line 229
    .local v6, "item":Lcom/android/providers/downloads/DownloadCAC$DownloadItem;
    sget-object v1, Lcom/android/providers/downloads/DownloadCAC;->pendingItems:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 230
    # getter for: Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mInfo:Lcom/android/providers/downloads/DownloadInfo;
    invoke-static {v6}, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->access$300(Lcom/android/providers/downloads/DownloadCAC$DownloadItem;)Lcom/android/providers/downloads/DownloadInfo;

    move-result-object v1

    iget-boolean v1, v1, Lcom/android/providers/downloads/DownloadInfo;->mDeleted:Z

    if-nez v1, :cond_2

    .line 231
    new-instance v0, Lcom/android/providers/downloads/DownloadThread;

    invoke-virtual {v6}, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->GetContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v6}, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->GetSystemFacade()Lcom/android/providers/downloads/SystemFacade;

    move-result-object v2

    invoke-virtual {v6}, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->GetNotifier()Lcom/android/providers/downloads/DownloadNotifier;

    move-result-object v3

    invoke-virtual {v6}, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->GetInfo()Lcom/android/providers/downloads/DownloadInfo;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/android/providers/downloads/DownloadThread;-><init>(Landroid/content/Context;Lcom/android/providers/downloads/SystemFacade;Lcom/android/providers/downloads/DownloadNotifier;Lcom/android/providers/downloads/DownloadInfo;Z)V

    .line 232
    .local v0, "downloader":Lcom/android/providers/downloads/DownloadThread;
    invoke-virtual {v0}, Lcom/android/providers/downloads/DownloadThread;->start()V

    .line 233
    iget v1, p0, Lcom/android/providers/downloads/DownloadCAC;->downloadCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/providers/downloads/DownloadCAC;->downloadCount:I

    .line 234
    sget-boolean v1, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v1, :cond_1

    .line 235
    const-string v1, "DownloadCAC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkPendingAndStart() - CAC Download Start :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mInfo:Lcom/android/providers/downloads/DownloadInfo;
    invoke-static {v6}, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->access$300(Lcom/android/providers/downloads/DownloadCAC$DownloadItem;)Lcom/android/providers/downloads/DownloadInfo;

    move-result-object v3

    iget-wide v4, v3, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mInfo:Lcom/android/providers/downloads/DownloadInfo;
    invoke-static {v6}, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->access$300(Lcom/android/providers/downloads/DownloadCAC$DownloadItem;)Lcom/android/providers/downloads/DownloadInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    .end local v0    # "downloader":Lcom/android/providers/downloads/DownloadThread;
    .end local v6    # "item":Lcom/android/providers/downloads/DownloadCAC$DownloadItem;
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 239
    .restart local v6    # "item":Lcom/android/providers/downloads/DownloadCAC$DownloadItem;
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/android/providers/downloads/DownloadHandler;->getInstance()Lcom/android/providers/downloads/DownloadHandler;

    move-result-object v1

    # getter for: Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mInfo:Lcom/android/providers/downloads/DownloadInfo;
    invoke-static {v6}, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->access$300(Lcom/android/providers/downloads/DownloadCAC$DownloadItem;)Lcom/android/providers/downloads/DownloadInfo;

    move-result-object v2

    iget-wide v2, v2, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-virtual {v1, v2, v3}, Lcom/android/providers/downloads/DownloadHandler;->dequeueDownload(J)V

    .line 240
    sget-boolean v1, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v1, :cond_0

    .line 241
    const-string v1, "DownloadCAC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkPendingAndStart() - Cancelled :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mInfo:Lcom/android/providers/downloads/DownloadInfo;
    invoke-static {v6}, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->access$300(Lcom/android/providers/downloads/DownloadCAC$DownloadItem;)Lcom/android/providers/downloads/DownloadInfo;

    move-result-object v3

    iget-wide v4, v3, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mInfo:Lcom/android/providers/downloads/DownloadInfo;
    invoke-static {v6}, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->access$300(Lcom/android/providers/downloads/DownloadCAC$DownloadItem;)Lcom/android/providers/downloads/DownloadInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->mInfo:Lcom/android/providers/downloads/DownloadInfo;
    invoke-static {v6}, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;->access$300(Lcom/android/providers/downloads/DownloadCAC$DownloadItem;)Lcom/android/providers/downloads/DownloadInfo;

    move-result-object v3

    iget-boolean v3, v3, Lcom/android/providers/downloads/DownloadInfo;->mDeleted:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 223
    .end local v6    # "item":Lcom/android/providers/downloads/DownloadCAC$DownloadItem;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 246
    :cond_3
    :try_start_2
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    sget-object v2, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_DISCONNECTED:Lcom/android/providers/downloads/DownloadCAC$STATE;

    if-eq v1, v2, :cond_1

    .line 247
    sget-object v1, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_DISCONNECTED:Lcom/android/providers/downloads/DownloadCAC$STATE;

    iput-object v1, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    .line 250
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->unregisterOpenSSLEngine()Z

    .line 254
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->unregisterProvider()Z

    .line 255
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->deinitialize()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/providers/downloads/DownloadCAC;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    const-class v1, Lcom/android/providers/downloads/DownloadCAC;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/providers/downloads/DownloadCAC;->instance_:Lcom/android/providers/downloads/DownloadCAC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 140
    if-nez p0, :cond_0

    .line 141
    const/4 v0, 0x0

    .line 144
    :goto_0
    monitor-exit v1

    return-object v0

    .line 142
    :cond_0
    :try_start_1
    new-instance v0, Lcom/android/providers/downloads/DownloadCAC;

    invoke-direct {v0, p0}, Lcom/android/providers/downloads/DownloadCAC;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/providers/downloads/DownloadCAC;->instance_:Lcom/android/providers/downloads/DownloadCAC;

    .line 144
    :cond_1
    sget-object v0, Lcom/android/providers/downloads/DownloadCAC;->instance_:Lcom/android/providers/downloads/DownloadCAC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static isSmartCardAuthenticationAvailable(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 109
    const/4 v7, 0x0

    .line 111
    .local v7, "defaultValue":Z
    const-string v0, "content://com.sec.knox.provider/SmartCardBrowserPolicy"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 112
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "isAuthenticationEnabled"

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 113
    .local v6, "cr":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 115
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 116
    const-string v0, "isAuthenticationEnabled"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 117
    .local v8, "value":Ljava/lang/String;
    if-eqz v8, :cond_0

    const-string v0, "true"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 120
    .end local v7    # "defaultValue":Z
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 123
    .end local v8    # "value":Ljava/lang/String;
    :cond_1
    return v7

    .line 120
    .restart local v7    # "defaultValue":Z
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private declared-synchronized onCACConnected()V
    .locals 3

    .prologue
    .line 286
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_0

    .line 287
    const-string v0, "DownloadCAC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCACConnected() - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    sget-object v1, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_CONNECTED:Lcom/android/providers/downloads/DownloadCAC$STATE;

    if-eq v0, v1, :cond_1

    .line 291
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->registerProvider()Ljava/lang/String;

    .line 292
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->registerOpenSSLEngine()Z

    .line 295
    :cond_1
    sget-object v0, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_CONNECTED:Lcom/android/providers/downloads/DownloadCAC$STATE;

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    .line 296
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadCAC;->checkPendingAndStart()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    monitor-exit p0

    return-void

    .line 286
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized onCACDisconnected()V
    .locals 3

    .prologue
    .line 300
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_0

    .line 301
    const-string v0, "DownloadCAC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCACDisconnected() - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    sget-object v1, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_RECONNECTING:Lcom/android/providers/downloads/DownloadCAC$STATE;

    if-ne v0, v1, :cond_1

    .line 307
    sget-object v0, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_CONNECTING:Lcom/android/providers/downloads/DownloadCAC$STATE;

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    .line 308
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->initialize(Lcom/sec/enterprise/knox/smartcard/SmartCardHelper$Callback;)V

    .line 309
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadCAC;->setTimer()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    :goto_0
    monitor-exit p0

    return-void

    .line 311
    :cond_1
    :try_start_1
    sget-object v0, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_DISCONNECTED:Lcom/android/providers/downloads/DownloadCAC$STATE;

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    .line 312
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadCAC;->checkPendingAndStart()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 300
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setTimer()V
    .locals 4

    .prologue
    .line 175
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/android/providers/downloads/DownloadCAC$2;

    invoke-direct {v0, p0}, Lcom/android/providers/downloads/DownloadCAC$2;-><init>(Lcom/android/providers/downloads/DownloadCAC;)V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->timerTask:Ljava/util/TimerTask;

    .line 180
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->timer:Ljava/util/Timer;

    .line 181
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/android/providers/downloads/DownloadCAC;->timerTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    monitor-exit p0

    return-void

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized addDownload(Landroid/content/Context;Lcom/android/providers/downloads/DownloadInfo;Lcom/android/providers/downloads/SystemFacade;Lcom/android/providers/downloads/DownloadNotifier;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Lcom/android/providers/downloads/DownloadInfo;
    .param p3, "systemFacade"    # Lcom/android/providers/downloads/SystemFacade;
    .param p4, "downloadNotifier"    # Lcom/android/providers/downloads/DownloadNotifier;

    .prologue
    .line 261
    monitor-enter p0

    const/4 v6, 0x0

    .line 262
    .local v6, "needHelperInit":Z
    :try_start_0
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    sget-object v2, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_DISCONNECTED:Lcom/android/providers/downloads/DownloadCAC$STATE;

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/android/providers/downloads/DownloadCAC;->downloadCount:I

    if-nez v1, :cond_0

    sget-object v1, Lcom/android/providers/downloads/DownloadCAC;->pendingItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 265
    const/4 v6, 0x1

    .line 268
    :cond_0
    sget-boolean v1, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v1, :cond_1

    .line 269
    const-string v1, "DownloadCAC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addDownload() state : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / pending items : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/providers/downloads/DownloadCAC;->pendingItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    :cond_1
    new-instance v0, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/providers/downloads/DownloadCAC$DownloadItem;-><init>(Lcom/android/providers/downloads/DownloadCAC;Landroid/content/Context;Lcom/android/providers/downloads/SystemFacade;Lcom/android/providers/downloads/DownloadNotifier;Lcom/android/providers/downloads/DownloadInfo;)V

    .line 272
    .local v0, "item":Lcom/android/providers/downloads/DownloadCAC$DownloadItem;
    sget-object v1, Lcom/android/providers/downloads/DownloadCAC;->pendingItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 274
    const/4 v1, 0x1

    if-ne v6, v1, :cond_3

    .line 275
    sget-boolean v1, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v1, :cond_2

    .line 276
    const-string v1, "DownloadCAC"

    const-string v2, "call SmartCardHelper.initialize"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :cond_2
    sget-object v1, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_CONNECTING:Lcom/android/providers/downloads/DownloadCAC$STATE;

    iput-object v1, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    .line 280
    iget-object v1, p0, Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v1, p0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->initialize(Lcom/sec/enterprise/knox/smartcard/SmartCardHelper$Callback;)V

    .line 281
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadCAC;->setTimer()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    :cond_3
    monitor-exit p0

    return-void

    .line 261
    .end local v0    # "item":Lcom/android/providers/downloads/DownloadCAC$DownloadItem;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized finalize()V
    .locals 2

    .prologue
    .line 148
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 151
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->timer:Ljava/util/Timer;

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->timerTask:Ljava/util/TimerTask;

    .line 153
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    sget-object v1, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_DISCONNECTED:Lcom/android/providers/downloads/DownloadCAC$STATE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    .line 172
    :goto_0
    monitor-exit p0

    return-void

    .line 156
    :cond_1
    :try_start_1
    new-instance v0, Lcom/android/providers/downloads/DownloadCAC$1;

    invoke-direct {v0, p0}, Lcom/android/providers/downloads/DownloadCAC$1;-><init>(Lcom/android/providers/downloads/DownloadCAC;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/providers/downloads/DownloadCAC$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized finishedDownload()V
    .locals 2

    .prologue
    .line 197
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_0

    .line 198
    const-string v0, "DownloadCAC"

    const-string v1, "finishdDownload()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :cond_0
    iget v0, p0, Lcom/android/providers/downloads/DownloadCAC;->downloadCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/providers/downloads/DownloadCAC;->downloadCount:I

    .line 202
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    sget-object v1, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_CONNECTED:Lcom/android/providers/downloads/DownloadCAC$STATE;

    if-ne v0, v1, :cond_3

    .line 203
    sget-object v0, Lcom/android/providers/downloads/DownloadCAC;->pendingItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 204
    sget-object v0, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_RECONNECTING:Lcom/android/providers/downloads/DownloadCAC$STATE;

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    .line 209
    :goto_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->unregisterProvider()Z

    .line 210
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->deinitialize()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 206
    :cond_2
    :try_start_1
    sget-object v0, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_DISCONNECTED:Lcom/android/providers/downloads/DownloadCAC$STATE;

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 213
    :cond_3
    :try_start_2
    sget-object v0, Lcom/android/providers/downloads/DownloadCAC;->pendingItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 214
    sget-object v0, Lcom/android/providers/downloads/DownloadCAC$STATE;->CAC_STATE_CONNECTING:Lcom/android/providers/downloads/DownloadCAC$STATE;

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mState:Lcom/android/providers/downloads/DownloadCAC$STATE;

    .line 215
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->initialize(Lcom/sec/enterprise/knox/smartcard/SmartCardHelper$Callback;)V

    .line 216
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadCAC;->setTimer()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public onInitComplete()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 319
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 321
    iput-object v1, p0, Lcom/android/providers/downloads/DownloadCAC;->timerTask:Ljava/util/TimerTask;

    .line 322
    iput-object v1, p0, Lcom/android/providers/downloads/DownloadCAC;->timer:Ljava/util/Timer;

    .line 324
    :cond_0
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadCAC;->onCACConnected()V

    .line 325
    return-void
.end method

.method public onStatusChanged(I)V
    .locals 4
    .param p1, "code"    # I

    .prologue
    const/4 v3, 0x0

    .line 329
    sget-boolean v0, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v0, :cond_0

    .line 330
    const-string v0, "DownloadCAC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStatusChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/android/providers/downloads/DownloadCAC;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 335
    iput-object v3, p0, Lcom/android/providers/downloads/DownloadCAC;->timerTask:Ljava/util/TimerTask;

    .line 336
    iput-object v3, p0, Lcom/android/providers/downloads/DownloadCAC;->timer:Ljava/util/Timer;

    .line 339
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 368
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadCAC;->onCACDisconnected()V

    .line 371
    :goto_0
    return-void

    .line 358
    :pswitch_0
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadCAC;->onCACDisconnected()V

    goto :goto_0

    .line 364
    :pswitch_1
    invoke-direct {p0}, Lcom/android/providers/downloads/DownloadCAC;->onCACConnected()V

    goto :goto_0

    .line 339
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
