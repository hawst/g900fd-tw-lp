.class public Lcom/android/providers/downloads/NotifyThread;
.super Ljava/lang/Thread;
.source "NotifyThread.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInfo:Lcom/android/providers/downloads/DownloadInfo;


# direct methods
.method public constructor <init>(Lcom/android/providers/downloads/DownloadInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "info"    # Lcom/android/providers/downloads/DownloadInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    .line 49
    iput-object p2, p0, Lcom/android/providers/downloads/NotifyThread;->mContext:Landroid/content/Context;

    .line 50
    return-void
.end method


# virtual methods
.method public UpdateNotificatonFailStatus(Landroid/net/Uri;Lcom/android/providers/downloads/DownloadHttpClient;)V
    .locals 5
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "client"    # Lcom/android/providers/downloads/DownloadHttpClient;

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0xa

    .line 200
    if-eqz p2, :cond_0

    .line 201
    invoke-virtual {p2}, Lcom/android/providers/downloads/DownloadHttpClient;->close()V

    .line 203
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 206
    .local v0, "values":Landroid/content/ContentValues;
    iget-object v1, p0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v1, v1, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    invoke-static {v1}, Landroid/provider/Downloads$Impl;->isStatusError(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 207
    const-string v1, "state"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 208
    sget-boolean v1, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v1, :cond_1

    .line 209
    const-string v1, "DownloadManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NotifyThread : UpdateNotificatonFailStatus : Notification Fail : Download Fail: status:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v3, v3, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/android/providers/downloads/NotifyThread;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p1, v0, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 226
    return-void

    .line 211
    :cond_2
    iget-object v1, p0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    iget v1, v1, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    invoke-static {v1}, Landroid/provider/Downloads$Impl;->isStatusSuccess(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 213
    sget-boolean v1, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v1, :cond_3

    .line 214
    const-string v1, "DownloadManager"

    const-string v2, "NotifyThread : UpdateNotificatonFailStatus : Notification Fail : Download Install Successfully"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :cond_3
    const-string v1, "state"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 221
    :cond_4
    const-string v1, "DownloadManager"

    const-string v2, "NotifyThread : UpdateNotificatonFailStatus : UpdateHandleStatus : Download Status Incorrect"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    const-string v1, "state"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public run()V
    .locals 24

    .prologue
    .line 53
    const/16 v20, 0xa

    invoke-static/range {v20 .. v20}, Landroid/os/Process;->setThreadPriority(I)V

    .line 54
    const/4 v8, 0x0

    .line 55
    .local v8, "client":Lcom/android/providers/downloads/DownloadHttpClient;
    const/16 v17, 0x1

    .line 56
    .local v17, "statusfail":Z
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v20, :cond_0

    .line 57
    const-string v20, "DownloadManager"

    const-string v21, "NotifyThread : run : Notify Thread Started"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v20, v0

    if-nez v20, :cond_2

    .line 196
    :cond_1
    :goto_0
    return-void

    .line 61
    :cond_2
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 65
    .local v9, "contentUri":Landroid/net/Uri;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/android/providers/downloads/Helpers;->isNetworkAvailable(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v20

    if-nez v20, :cond_3

    .line 190
    const/16 v20, 0x1

    move/from16 v0, v20

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 191
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v8}, Lcom/android/providers/downloads/NotifyThread;->UpdateNotificatonFailStatus(Landroid/net/Uri;Lcom/android/providers/downloads/DownloadHttpClient;)V

    goto :goto_0

    .line 71
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mUserAgent:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 72
    .local v18, "userAgent":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/providers/downloads/DownloadInfo;->getHeaders()Ljava/util/Collection;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/util/Pair;

    .line 73
    .local v12, "header":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, v12, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/String;

    const-string v21, "User-Agent"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 74
    iget-object v0, v12, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v18, v0

    .end local v18    # "userAgent":Ljava/lang/String;
    check-cast v18, Ljava/lang/String;

    .line 78
    .end local v12    # "header":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v18    # "userAgent":Ljava/lang/String;
    :cond_5
    if-nez v18, :cond_6

    .line 79
    sget-object v18, Lcom/android/providers/downloads/Constants;->DEFAULT_USER_AGENT:Ljava/lang/String;

    .line 81
    :cond_6
    invoke-static/range {v18 .. v18}, Lcom/android/providers/downloads/DownloadHttpClient;->newInstance(Ljava/lang/String;)Lcom/android/providers/downloads/DownloadHttpClient;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 87
    :try_start_2
    new-instance v5, Ljava/net/URI;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v5, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 88
    .local v5, "MainUrl":Ljava/net/URI;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mNotifyURI:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/net/URI;->resolve(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v4

    .line 89
    .local v4, "AbsoluteUri":Ljava/net/URI;
    invoke-virtual {v4}, Ljava/net/URI;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v7

    .line 101
    .local v7, "RequestUrl":Ljava/lang/String;
    :try_start_3
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v20, :cond_7

    .line 102
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "NotifyThread : run : Sending Notification to the URL: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_7
    new-instance v14, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v14, v7}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 109
    .local v14, "request":Lorg/apache/http/client/methods/HttpPost;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/android/providers/downloads/Helpers;->getStatusText(I)Ljava/lang/String;

    move-result-object v6

    .line 110
    .local v6, "NotifyMessage":Ljava/lang/String;
    new-instance v20, Lorg/apache/http/entity/StringEntity;

    move-object/from16 v0, v20

    invoke-direct {v0, v6}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 117
    :try_start_5
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v20, :cond_8

    .line 118
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "NotifyThread : run : Sending Notification to the URL: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mNotifyURI:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 125
    :cond_8
    :try_start_6
    invoke-virtual {v8, v14}, Lcom/android/providers/downloads/DownloadHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v15

    .line 138
    .local v15, "response":Lorg/apache/http/HttpResponse;
    :try_start_7
    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v16

    .line 140
    .local v16, "statusCode":I
    if-eqz v8, :cond_9

    .line 141
    invoke-virtual {v8}, Lcom/android/providers/downloads/DownloadHttpClient;->close()V

    .line 142
    const/4 v8, 0x0

    .line 144
    :cond_9
    const/16 v20, 0xc8

    move/from16 v0, v20

    move/from16 v1, v16

    if-ne v0, v1, :cond_11

    .line 145
    const/16 v17, 0x0

    .line 146
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 147
    .local v19, "values":Landroid/content/ContentValues;
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v20, :cond_a

    .line 148
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "NotifyThread : run : Notification URL Success: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mNotifyURI:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/provider/Downloads$Impl;->isStatusError(I)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 152
    const-string v20, "state"

    const/16 v21, 0x9

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 153
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v20, :cond_b

    .line 154
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "NotifyThread : run : Notification Success: Download Fail: status:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :cond_b
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v9, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 190
    const/16 v20, 0x1

    move/from16 v0, v20

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 191
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v8}, Lcom/android/providers/downloads/NotifyThread;->UpdateNotificatonFailStatus(Landroid/net/Uri;Lcom/android/providers/downloads/DownloadHttpClient;)V

    goto/16 :goto_0

    .line 90
    .end local v4    # "AbsoluteUri":Ljava/net/URI;
    .end local v5    # "MainUrl":Ljava/net/URI;
    .end local v6    # "NotifyMessage":Ljava/lang/String;
    .end local v7    # "RequestUrl":Ljava/lang/String;
    .end local v14    # "request":Lorg/apache/http/client/methods/HttpPost;
    .end local v15    # "response":Lorg/apache/http/HttpResponse;
    .end local v16    # "statusCode":I
    .end local v19    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v10

    .line 91
    .local v10, "e":Ljava/net/URISyntaxException;
    :try_start_8
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "NotifyThread : run : URI Syntax Error:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 190
    const/16 v20, 0x1

    move/from16 v0, v20

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 191
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v8}, Lcom/android/providers/downloads/NotifyThread;->UpdateNotificatonFailStatus(Landroid/net/Uri;Lcom/android/providers/downloads/DownloadHttpClient;)V

    goto/16 :goto_0

    .line 95
    .end local v10    # "e":Ljava/net/URISyntaxException;
    :catch_1
    move-exception v10

    .line 96
    .local v10, "e":Ljava/lang/Exception;
    :try_start_9
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "NotifyThread : run : Exception:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mUri:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 190
    const/16 v20, 0x1

    move/from16 v0, v20

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 191
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v8}, Lcom/android/providers/downloads/NotifyThread;->UpdateNotificatonFailStatus(Landroid/net/Uri;Lcom/android/providers/downloads/DownloadHttpClient;)V

    goto/16 :goto_0

    .line 111
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v4    # "AbsoluteUri":Ljava/net/URI;
    .restart local v5    # "MainUrl":Ljava/net/URI;
    .restart local v7    # "RequestUrl":Ljava/lang/String;
    .restart local v14    # "request":Lorg/apache/http/client/methods/HttpPost;
    :catch_2
    move-exception v10

    .line 112
    .local v10, "e":Ljava/io/UnsupportedEncodingException;
    :try_start_a
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "NotifyThread : run : Unsupported Encoding for the URL: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mNotifyURI:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 190
    const/16 v20, 0x1

    move/from16 v0, v20

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 191
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v8}, Lcom/android/providers/downloads/NotifyThread;->UpdateNotificatonFailStatus(Landroid/net/Uri;Lcom/android/providers/downloads/DownloadHttpClient;)V

    goto/16 :goto_0

    .line 126
    .end local v10    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v6    # "NotifyMessage":Ljava/lang/String;
    :catch_3
    move-exception v11

    .line 127
    .local v11, "ex":Ljava/lang/IllegalArgumentException;
    :try_start_b
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "NotifyThread : run : Illegal Arg Exception trying to execute request for: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mNotifyURI:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 190
    const/16 v20, 0x1

    move/from16 v0, v20

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 191
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v8}, Lcom/android/providers/downloads/NotifyThread;->UpdateNotificatonFailStatus(Landroid/net/Uri;Lcom/android/providers/downloads/DownloadHttpClient;)V

    goto/16 :goto_0

    .line 132
    .end local v11    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v11

    .line 133
    .local v11, "ex":Ljava/io/IOException;
    :try_start_c
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "NotifyThread : run : Request Execute Failed: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mId:J

    move-wide/from16 v22, v0

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 190
    const/16 v20, 0x1

    move/from16 v0, v20

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 191
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v8}, Lcom/android/providers/downloads/NotifyThread;->UpdateNotificatonFailStatus(Landroid/net/Uri;Lcom/android/providers/downloads/DownloadHttpClient;)V

    goto/16 :goto_0

    .line 157
    .end local v11    # "ex":Ljava/io/IOException;
    .restart local v15    # "response":Lorg/apache/http/HttpResponse;
    .restart local v16    # "statusCode":I
    .restart local v19    # "values":Landroid/content/ContentValues;
    :cond_c
    :try_start_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mStatus:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/provider/Downloads$Impl;->isStatusSuccess(I)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 158
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v20, :cond_d

    .line 159
    const-string v20, "DownloadManager"

    const-string v21, "NotifyThread : run : Notification Success: Download Install Successfully"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :cond_d
    const-string v20, "state"

    const/16 v21, 0xa

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_1

    .line 184
    .end local v4    # "AbsoluteUri":Ljava/net/URI;
    .end local v5    # "MainUrl":Ljava/net/URI;
    .end local v6    # "NotifyMessage":Ljava/lang/String;
    .end local v7    # "RequestUrl":Ljava/lang/String;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "request":Lorg/apache/http/client/methods/HttpPost;
    .end local v15    # "response":Lorg/apache/http/HttpResponse;
    .end local v16    # "statusCode":I
    .end local v18    # "userAgent":Ljava/lang/String;
    .end local v19    # "values":Landroid/content/ContentValues;
    :catch_5
    move-exception v11

    .line 185
    .local v11, "ex":Ljava/lang/RuntimeException;
    :try_start_e
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "NotifyThread : run : Exception for the URL: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mNotifyURI:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 190
    const/16 v20, 0x1

    move/from16 v0, v20

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 191
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v8}, Lcom/android/providers/downloads/NotifyThread;->UpdateNotificatonFailStatus(Landroid/net/Uri;Lcom/android/providers/downloads/DownloadHttpClient;)V

    goto/16 :goto_0

    .line 165
    .end local v11    # "ex":Ljava/lang/RuntimeException;
    .restart local v4    # "AbsoluteUri":Ljava/net/URI;
    .restart local v5    # "MainUrl":Ljava/net/URI;
    .restart local v6    # "NotifyMessage":Ljava/lang/String;
    .restart local v7    # "RequestUrl":Ljava/lang/String;
    .restart local v13    # "i$":Ljava/util/Iterator;
    .restart local v14    # "request":Lorg/apache/http/client/methods/HttpPost;
    .restart local v15    # "response":Lorg/apache/http/HttpResponse;
    .restart local v16    # "statusCode":I
    .restart local v18    # "userAgent":Ljava/lang/String;
    .restart local v19    # "values":Landroid/content/ContentValues;
    :cond_e
    :try_start_f
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v20, :cond_f

    .line 166
    const-string v20, "DownloadManager"

    const-string v21, "NotifyThread : run : Notification Success: Download State is Invalid"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    :cond_f
    const-string v20, "state"

    const/16 v21, 0xa

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_f
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_5
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_1

    .line 190
    .end local v4    # "AbsoluteUri":Ljava/net/URI;
    .end local v5    # "MainUrl":Ljava/net/URI;
    .end local v6    # "NotifyMessage":Ljava/lang/String;
    .end local v7    # "RequestUrl":Ljava/lang/String;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "request":Lorg/apache/http/client/methods/HttpPost;
    .end local v15    # "response":Lorg/apache/http/HttpResponse;
    .end local v16    # "statusCode":I
    .end local v18    # "userAgent":Ljava/lang/String;
    .end local v19    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v20

    const/16 v21, 0x1

    move/from16 v0, v21

    move/from16 v1, v17

    if-ne v0, v1, :cond_10

    .line 191
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v8}, Lcom/android/providers/downloads/NotifyThread;->UpdateNotificatonFailStatus(Landroid/net/Uri;Lcom/android/providers/downloads/DownloadHttpClient;)V

    :cond_10
    throw v20

    .line 175
    .restart local v4    # "AbsoluteUri":Ljava/net/URI;
    .restart local v5    # "MainUrl":Ljava/net/URI;
    .restart local v6    # "NotifyMessage":Ljava/lang/String;
    .restart local v7    # "RequestUrl":Ljava/lang/String;
    .restart local v13    # "i$":Ljava/util/Iterator;
    .restart local v14    # "request":Lorg/apache/http/client/methods/HttpPost;
    .restart local v15    # "response":Lorg/apache/http/HttpResponse;
    .restart local v16    # "statusCode":I
    .restart local v18    # "userAgent":Ljava/lang/String;
    :cond_11
    :try_start_10
    sget-boolean v20, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v20, :cond_12

    .line 176
    const-string v20, "DownloadManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "NotifyThread : run : Notification Fail with HTTP Status Error: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " For the URL: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/downloads/NotifyThread;->mInfo:Lcom/android/providers/downloads/DownloadInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/providers/downloads/DownloadInfo;->mNotifyURI:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_5
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 190
    :cond_12
    const/16 v20, 0x1

    move/from16 v0, v20

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 191
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v8}, Lcom/android/providers/downloads/NotifyThread;->UpdateNotificatonFailStatus(Landroid/net/Uri;Lcom/android/providers/downloads/DownloadHttpClient;)V

    goto/16 :goto_0
.end method
