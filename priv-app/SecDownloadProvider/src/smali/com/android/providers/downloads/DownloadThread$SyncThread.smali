.class Lcom/android/providers/downloads/DownloadThread$SyncThread;
.super Ljava/lang/Object;
.source "DownloadThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/downloads/DownloadThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SyncThread"
.end annotation


# instance fields
.field private bRunning:Z

.field protected finalSyncLocker:Ljava/lang/Object;

.field private lastUpdatedBytes:J

.field private mRaf:Ljava/io/RandomAccessFile;

.field protected syncList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/providers/downloads/DownloadThread;


# direct methods
.method public constructor <init>(Lcom/android/providers/downloads/DownloadThread;Ljava/io/RandomAccessFile;J)V
    .locals 1
    .param p2, "raf"    # Ljava/io/RandomAccessFile;
    .param p3, "curByte"    # J

    .prologue
    .line 1480
    iput-object p1, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->this$0:Lcom/android/providers/downloads/DownloadThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1474
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    .line 1481
    iput-object p2, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->mRaf:Ljava/io/RandomAccessFile;

    .line 1482
    iput-wide p3, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->lastUpdatedBytes:J

    .line 1483
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->bRunning:Z

    .line 1484
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->finalSyncLocker:Ljava/lang/Object;

    .line 1485
    return-void
.end method

.method static synthetic access$300(Lcom/android/providers/downloads/DownloadThread$SyncThread;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/downloads/DownloadThread$SyncThread;

    .prologue
    .line 1473
    iget-boolean v0, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->bRunning:Z

    return v0
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 1492
    iget-object v8, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->mRaf:Ljava/io/RandomAccessFile;

    if-nez v8, :cond_1

    .line 1550
    :cond_0
    :goto_0
    return-void

    .line 1495
    :cond_1
    sget-boolean v8, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v8, :cond_2

    const-string v8, "DownloadManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sync thread start with current byte "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->lastUpdatedBytes:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1496
    :cond_2
    const-wide/16 v4, 0x0

    .line 1497
    .local v4, "lastUpdatedTime":J
    const/4 v3, 0x0

    .line 1498
    .local v3, "syncData":Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;
    :goto_1
    iget-boolean v8, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->bRunning:Z

    if-eqz v8, :cond_b

    .line 1500
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 1501
    .local v6, "now":J
    const/4 v3, 0x0

    .line 1502
    sub-long v8, v6, v4

    const-wide/16 v10, 0x1770

    cmp-long v8, v8, v10

    if-ltz v8, :cond_7

    .line 1503
    iget-object v9, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    monitor-enter v9
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1504
    :try_start_1
    iget-object v8, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    .line 1505
    iget-object v8, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object v3, v0

    .line 1506
    iget-object v8, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->clear()V

    .line 1508
    :cond_3
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1509
    :try_start_2
    iget-object v9, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->finalSyncLocker:Ljava/lang/Object;

    monitor-enter v9
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 1510
    :try_start_3
    iget-boolean v8, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->bRunning:Z

    if-eqz v8, :cond_5

    if-eqz v3, :cond_5

    .line 1511
    iget-object v8, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->mRaf:Ljava/io/RandomAccessFile;

    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/FileDescriptor;->sync()V

    .line 1512
    iget-wide v10, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    iget-wide v12, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->lastUpdatedBytes:J

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    iput-wide v10, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    .line 1513
    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->writeToDatabase_NB(Z)V

    .line 1514
    sget-boolean v8, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v8, :cond_4

    const-string v8, "DownloadManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "sync data to disc 0-"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mFirstChunkEnd:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkStart:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "-"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mSecChunkEnd:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", CurrentBytes="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", lastUpdatedBytes="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->lastUpdatedBytes:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1517
    :cond_4
    iget-wide v10, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    iput-wide v10, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->lastUpdatedBytes:J

    .line 1518
    move-wide v4, v6

    .line 1520
    :cond_5
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1541
    .end local v6    # "now":J
    :cond_6
    :goto_2
    iget-object v9, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    monitor-enter v9

    .line 1543
    :try_start_4
    iget-object v8, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    const-wide/16 v10, 0x5dc

    invoke-virtual {v8, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1547
    :goto_3
    :try_start_5
    monitor-exit v9

    goto/16 :goto_1

    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v8

    .line 1508
    .restart local v6    # "now":J
    :catchall_1
    move-exception v8

    :try_start_6
    monitor-exit v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v8
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0

    .line 1538
    .end local v6    # "now":J
    :catch_0
    move-exception v2

    .line 1539
    .local v2, "e":Ljava/lang/Throwable;
    sget-boolean v8, Lcom/android/providers/downloads/Constants;->LOGV:Z

    if-eqz v8, :cond_6

    const-string v8, "DownloadManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "exception during sync : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1520
    .end local v2    # "e":Ljava/lang/Throwable;
    .restart local v6    # "now":J
    :catchall_2
    move-exception v8

    :try_start_8
    monitor-exit v9
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v8

    .line 1523
    :cond_7
    iget-object v9, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    monitor-enter v9
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0

    .line 1524
    :try_start_a
    iget-object v8, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_8

    .line 1525
    iget-object v8, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->syncList:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;

    move-object v3, v0

    .line 1527
    :cond_8
    monitor-exit v9
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 1528
    :try_start_b
    iget-object v9, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->finalSyncLocker:Ljava/lang/Object;

    monitor-enter v9
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_0

    .line 1529
    :try_start_c
    iget-boolean v8, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->bRunning:Z

    if-eqz v8, :cond_a

    if-eqz v3, :cond_a

    .line 1530
    iget-wide v10, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    iget-wide v12, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->lastUpdatedBytes:J

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    iput-wide v10, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    .line 1531
    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->writeToDatabase_NB(Z)V

    .line 1532
    sget-boolean v8, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v8, :cond_9

    const-string v8, "DownloadManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "sync data only for progress bar, CurrentBytes="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1533
    :cond_9
    iget-wide v10, v3, Lcom/android/providers/downloads/DownloadThread$DownloadInfoDelta;->mCurrentBytes:J

    iput-wide v10, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->lastUpdatedBytes:J

    .line 1535
    :cond_a
    monitor-exit v9

    goto/16 :goto_2

    :catchall_3
    move-exception v8

    monitor-exit v9
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    throw v8
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_0

    .line 1527
    :catchall_4
    move-exception v8

    :try_start_e
    monitor-exit v9
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    :try_start_f
    throw v8
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_0

    .line 1549
    .end local v6    # "now":J
    :cond_b
    sget-boolean v8, Lcom/android/providers/downloads/Constants;->LOGVV:Z

    if-eqz v8, :cond_0

    const-string v8, "DownloadManager"

    const-string v9, "sync thread exit"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1545
    :catch_1
    move-exception v8

    goto/16 :goto_3
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 1488
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/providers/downloads/DownloadThread$SyncThread;->bRunning:Z

    .line 1489
    return-void
.end method
