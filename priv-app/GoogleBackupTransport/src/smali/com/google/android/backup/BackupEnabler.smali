.class public Lcom/google/android/backup/BackupEnabler;
.super Landroid/app/Service;
.source "BackupEnabler.java"


# instance fields
.field private binder:Landroid/os/Binder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 21
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/google/android/backup/BackupEnabler;->binder:Landroid/os/Binder;

    return-void
.end method


# virtual methods
.method protected enableBackup(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 35
    new-instance v0, Landroid/app/backup/BackupManager;

    invoke-direct {v0, p0}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    .line 36
    .local v0, "bm":Landroid/app/backup/BackupManager;
    invoke-virtual {v0, p1}, Landroid/app/backup/BackupManager;->setBackupEnabled(Z)V

    .line 38
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/google/android/backup/BackupEnabler;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 40
    .local v1, "notificationManager":Landroid/app/NotificationManager;
    const-string v2, "com.google.android.backup.notification.tag"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 43
    const-string v2, "BackupTransportService"

    const-string v3, "enableBackup() succeeded"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/backup/BackupEnabler;->binder:Landroid/os/Binder;

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 25
    const-string v0, "BACKUP_ENABLE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    const-string v0, "BACKUP_ENABLE"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/backup/BackupEnabler;->enableBackup(Z)V

    .line 28
    :cond_0
    const/4 v0, 0x2

    return v0
.end method
