.class Lcom/google/android/backup/BackupTransportService$1;
.super Landroid/app/backup/BackupTransport;
.source "BackupTransportService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/backup/BackupTransportService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private fullBackupCompleteLock:Ljava/lang/Object;

.field private fullBackupDataStream:Lcom/google/android/backup/BlockingFileDataStream;

.field private fullBackupStatusCode:Ljava/util/concurrent/atomic/AtomicInteger;

.field private isFullBackup:Ljava/lang/Boolean;

.field private mDriveAuthToken:Ljava/lang/String;

.field private mFullRestoreConnection:Ljavax/net/ssl/HttpsURLConnection;

.field private mFullRestoreDocURLs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFullRestoreDownloadStream:Ljava/io/InputStream;

.field private mLastRestoreApp:Ljava/lang/String;

.field private mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

.field private mRestoreApp:Ljava/lang/String;

.field private mRestoreData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation
.end field

.field private mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;

.field private mSocketFileDescriptor:Landroid/os/ParcelFileDescriptor;

.field private scottyHttpTransfer:Lcom/google/uploader/client/Transfer;

.field private scottyUploadClient:Lcom/google/uploader/client/UploadClient;

.field final synthetic this$0:Lcom/google/android/backup/BackupTransportService;


# direct methods
.method constructor <init>(Lcom/google/android/backup/BackupTransportService;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 287
    iput-object p1, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-direct {p0}, Landroid/app/backup/BackupTransport;-><init>()V

    .line 289
    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    .line 292
    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;

    .line 293
    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    .line 294
    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mLastRestoreApp:Ljava/lang/String;

    .line 297
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreData:Ljava/util/HashMap;

    .line 299
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreDocURLs:Ljava/util/HashMap;

    .line 301
    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->scottyHttpTransfer:Lcom/google/uploader/client/Transfer;

    .line 302
    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mSocketFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 303
    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupDataStream:Lcom/google/android/backup/BlockingFileDataStream;

    .line 304
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupCompleteLock:Ljava/lang/Object;

    .line 306
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupStatusCode:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 307
    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->scottyUploadClient:Lcom/google/uploader/client/UploadClient;

    .line 308
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->isFullBackup:Ljava/lang/Boolean;

    .line 313
    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreDownloadStream:Ljava/io/InputStream;

    .line 314
    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreConnection:Ljavax/net/ssl/HttpsURLConnection;

    .line 315
    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mDriveAuthToken:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/backup/BackupTransportService$1;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService$1;

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupStatusCode:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/backup/BackupTransportService$1;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService$1;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AccountsException;
        }
    .end annotation

    .prologue
    .line 287
    invoke-direct {p0, p1}, Lcom/google/android/backup/BackupTransportService$1;->invalidAuthToken(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/backup/BackupTransportService$1;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService$1;

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupCompleteLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/backup/BackupTransportService$1;)Lcom/google/android/backup/BlockingFileDataStream;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService$1;

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupDataStream:Lcom/google/android/backup/BlockingFileDataStream;

    return-object v0
.end method

.method private closeFullRestoreHttpConnection()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1059
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreDownloadStream:Ljava/io/InputStream;

    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 1060
    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreDownloadStream:Ljava/io/InputStream;

    .line 1061
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreConnection:Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v0, :cond_0

    .line 1062
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreConnection:Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 1063
    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreConnection:Ljavax/net/ssl/HttpsURLConnection;

    .line 1065
    :cond_0
    return-void
.end method

.method private createFullRestoreHttpConnection()Ljavax/net/ssl/HttpsURLConnection;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/accounts/AccountsException;
        }
    .end annotation

    .prologue
    .line 1071
    iget-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreDocURLs:Ljava/util/HashMap;

    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1072
    .local v2, "docURL":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 1073
    new-instance v7, Ljava/io/IOException;

    invoke-direct {v7}, Ljava/io/IOException;-><init>()V

    throw v7

    .line 1076
    :cond_0
    iget-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->mDriveAuthToken:Ljava/lang/String;

    if-nez v7, :cond_1

    .line 1077
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->generateDriveAuthToken()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->mDriveAuthToken:Ljava/lang/String;

    .line 1080
    :cond_1
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1081
    .local v6, "url":Ljava/net/URL;
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    check-cast v1, Ljavax/net/ssl/HttpsURLConnection;

    .line 1082
    .local v1, "connection":Ljavax/net/ssl/HttpsURLConnection;
    const-string v7, "GET"

    invoke-virtual {v1, v7}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 1084
    const/4 v0, 0x0

    .line 1085
    .local v0, "authTokenValid":Z
    const/4 v4, 0x0

    .line 1087
    .local v4, "retry":I
    :goto_0
    if-nez v0, :cond_2

    const/4 v7, 0x3

    if-gt v4, v7, :cond_2

    .line 1088
    add-int/lit8 v4, v4, 0x1

    .line 1089
    const-string v7, "Authorization"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Bearer "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/backup/BackupTransportService$1;->mDriveAuthToken:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    invoke-virtual {v1}, Ljavax/net/ssl/HttpsURLConnection;->connect()V

    .line 1091
    invoke-virtual {v1}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v5

    .line 1092
    .local v5, "status":I
    const-string v7, "BackupTransportService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Http response status : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    sparse-switch v5, :sswitch_data_0

    .line 1109
    invoke-virtual {v1}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 1110
    new-instance v7, Ljava/io/IOException;

    const-string v8, "HTTP Connection ERROR."

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1096
    :sswitch_0
    invoke-virtual {v1}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    .line 1098
    const-wide/16 v8, 0x2710

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1102
    iget-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->mDriveAuthToken:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/google/android/backup/BackupTransportService$1;->invalidAuthToken(Ljava/lang/String;)V

    .line 1103
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->generateDriveAuthToken()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->mDriveAuthToken:Ljava/lang/String;

    goto :goto_0

    .line 1099
    :catch_0
    move-exception v3

    .line 1100
    .local v3, "e":Ljava/lang/InterruptedException;
    new-instance v7, Landroid/accounts/AccountsException;

    invoke-direct {v7, v3}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 1106
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :sswitch_1
    const/4 v0, 0x1

    .line 1107
    goto :goto_0

    .line 1113
    .end local v5    # "status":I
    :cond_2
    if-nez v0, :cond_3

    .line 1115
    new-instance v7, Landroid/accounts/AccountsException;

    const-string v8, "Cannot get valid auth token."

    invoke-direct {v7, v8}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1118
    :cond_3
    return-object v1

    .line 1093
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_1
        0x191 -> :sswitch_0
    .end sparse-switch
.end method

.method private finishFullBackupInternal()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 962
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupCompleteLock:Ljava/lang/Object;

    monitor-enter v1

    .line 963
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupStatusCode:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 966
    :try_start_1
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupCompleteLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 967
    :catch_0
    move-exception v0

    goto :goto_0

    .line 971
    :cond_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 972
    iput-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupDataStream:Lcom/google/android/backup/BlockingFileDataStream;

    .line 973
    iput-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->scottyHttpTransfer:Lcom/google/uploader/client/Transfer;

    .line 974
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->mSocketFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 975
    iput-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->mSocketFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 976
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->isFullBackup:Ljava/lang/Boolean;

    .line 977
    return-void

    .line 971
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method

.method private generateDriveAuthToken()Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AccountsException;
        }
    .end annotation

    .prologue
    .line 1124
    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 1125
    .local v0, "am":Landroid/accounts/AccountManager;
    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # invokes: Lcom/google/android/backup/BackupTransportService;->validateAndGetBackupAccount()Landroid/accounts/Account;
    invoke-static {v4}, Lcom/google/android/backup/BackupTransportService;->access$000(Lcom/google/android/backup/BackupTransportService;)Landroid/accounts/Account;

    move-result-object v2

    .line 1126
    .local v2, "backupAccount":Landroid/accounts/Account;
    if-nez v2, :cond_0

    .line 1127
    new-instance v4, Landroid/accounts/AccountsException;

    const-string v5, "Missing backup account"

    invoke-direct {v4, v5}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1130
    :cond_0
    :try_start_0
    const-string v4, "oauth2:https://www.googleapis.com/auth/drive"

    const/4 v5, 0x1

    invoke-virtual {v0, v2, v4, v5}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 1131
    .local v1, "authToken":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 1132
    new-instance v4, Landroid/accounts/AccountsException;

    const-string v5, "No auth token available"

    invoke-direct {v4, v5}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1137
    .end local v1    # "authToken":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 1138
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Landroid/accounts/AccountsException;

    const-string v5, "Get auth token error"

    invoke-direct {v4, v5}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1136
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "authToken":Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method private varargs getEligiblePackagesForFullBR(Z[Landroid/content/pm/PackageInfo;)[Ljava/lang/String;
    .locals 15
    .param p1, "whitelistCheck"    # Z
    .param p2, "pkgs"    # [Landroid/content/pm/PackageInfo;

    .prologue
    .line 986
    iget-object v13, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v13}, Lcom/google/android/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 988
    .local v3, "cr":Landroid/content/ContentResolver;
    const-string v13, "backup_restore_blacklist"

    const-string v14, ""

    invoke-static {v3, v13, v14}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 989
    .local v1, "blacklist":Ljava/lang/String;
    const-string v13, ","

    invoke-virtual {v1, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/google/android/collect/Sets;->newArraySet([Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v2

    .line 991
    .local v2, "blacklistedApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v13, "full_backup_restore_whitelist"

    const-string v14, "ALL"

    invoke-static {v3, v13, v14}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 993
    .local v11, "whitelist":Ljava/lang/String;
    const-string v13, ","

    invoke-virtual {v11, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/google/android/collect/Sets;->newArraySet([Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v12

    .line 995
    .local v12, "whitelistedApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v13, "backup_restore_full_minTarget"

    const/4 v14, 0x0

    invoke-static {v3, v13, v14}, Lcom/google/android/gsf/Gservices;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    .line 998
    .local v8, "minSdkVersion":I
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 999
    .local v4, "eligiblePackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 1000
    .local v5, "hasSettingsPackage":Z
    move-object/from16 v0, p2

    .local v0, "arr$":[Landroid/content/pm/PackageInfo;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_5

    aget-object v10, v0, v6

    .line 1001
    .local v10, "pkg":Landroid/content/pm/PackageInfo;
    iget-object v9, v10, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 1004
    .local v9, "packageName":Ljava/lang/String;
    iget-object v13, v10, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v2, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 1000
    :cond_0
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1009
    :cond_1
    if-eqz p1, :cond_2

    const-string v13, "ALL"

    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_2

    invoke-interface {v12, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1015
    :cond_2
    if-eqz v8, :cond_3

    iget-object v13, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v13, v13, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    if-ge v13, v8, :cond_3

    .line 1017
    iget-object v13, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v13, v13, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    if-eqz v13, :cond_0

    .line 1021
    iget-object v13, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v13, v13, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v14, 0x4000000

    and-int/2addr v13, v14

    if-nez v13, :cond_0

    .line 1031
    :cond_3
    const-string v13, "com.android.providers.settings"

    invoke-virtual {v9, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 1032
    const/4 v5, 0x1

    goto :goto_1

    .line 1034
    :cond_4
    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1038
    .end local v9    # "packageName":Ljava/lang/String;
    .end local v10    # "pkg":Landroid/content/pm/PackageInfo;
    :cond_5
    if-eqz v5, :cond_6

    .line 1039
    const-string v13, "com.android.providers.settings"

    invoke-interface {v4, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1041
    :cond_6
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v13

    new-array v13, v13, [Ljava/lang/String;

    invoke-interface {v4, v13}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Ljava/lang/String;

    return-object v13
.end method

.method private getRestoreDataLocked()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1184
    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "lock not held"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1185
    :cond_0
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "restore was never started"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1186
    :cond_1
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreData:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 1187
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;

    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreData:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreDocURLs:Ljava/util/HashMap;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/backup/RestoreRequestProcessor;->getApplicationData(Ljava/util/Map;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    .line 1188
    :goto_0
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 1189
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;

    invoke-virtual {v1}, Lcom/google/android/backup/RestoreRequestProcessor;->nextRequest()Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 1190
    .local v0, "req":Lcom/google/common/io/protocol/ProtoBuf;
    if-nez v0, :cond_3

    .line 1197
    .end local v0    # "req":Lcom/google/common/io/protocol/ProtoBuf;
    :cond_2
    return-void

    .line 1192
    .restart local v0    # "req":Lcom/google/common/io/protocol/ProtoBuf;
    :cond_3
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mRestoreScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v1}, Lcom/google/android/backup/BackupTransportService;->access$800(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    .line 1193
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;

    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mRestoreScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v3}, Lcom/google/android/backup/BackupTransportService;->access$800(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v3

    # invokes: Lcom/google/android/backup/BackupTransportService;->sendRequestLocked(Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    invoke-static {v2, v0, v3}, Lcom/google/android/backup/BackupTransportService;->access$600(Lcom/google/android/backup/BackupTransportService;Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/backup/RestoreRequestProcessor;->handleResponse(Lcom/google/common/io/protocol/ProtoBuf;)V

    .line 1194
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;

    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreData:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreDocURLs:Ljava/util/HashMap;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/backup/RestoreRequestProcessor;->getApplicationData(Ljava/util/Map;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    goto :goto_0
.end method

.method private initRequestGeneratorLocked()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 1160
    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "lock not held"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1161
    :cond_0
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    if-eqz v3, :cond_1

    .line 1173
    :goto_0
    return v2

    .line 1163
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1164
    .local v0, "now":J
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v3}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    .line 1167
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v3}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mSchedulerOptions:Lcom/android/common/OperationScheduler$Options;
    invoke-static {v4}, Lcom/google/android/backup/BackupTransportService;->access$200(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler$Options;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v4

    cmp-long v3, v4, v0

    if-lez v3, :cond_2

    .line 1168
    const-string v2, "BackupTransportService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not ready for backup request right now: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v4}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1169
    const/4 v2, 0x0

    goto :goto_0

    .line 1172
    :cond_2
    new-instance v3, Lcom/google/android/backup/BackupRequestGenerator;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/google/android/backup/BackupRequestGenerator;-><init>(J)V

    iput-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    goto :goto_0
.end method

.method private invalidAuthToken(Ljava/lang/String;)V
    .locals 5
    .param p1, "authToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AccountsException;
        }
    .end annotation

    .prologue
    .line 1143
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 1144
    .local v0, "am":Landroid/accounts/AccountManager;
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # invokes: Lcom/google/android/backup/BackupTransportService;->validateAndGetBackupAccount()Landroid/accounts/Account;
    invoke-static {v3}, Lcom/google/android/backup/BackupTransportService;->access$000(Lcom/google/android/backup/BackupTransportService;)Landroid/accounts/Account;

    move-result-object v1

    .line 1145
    .local v1, "backupAccount":Landroid/accounts/Account;
    if-nez v1, :cond_0

    .line 1146
    new-instance v3, Landroid/accounts/AccountsException;

    const-string v4, "Missing backup account"

    invoke-direct {v3, v4}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1149
    :cond_0
    :try_start_0
    iget-object v3, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v3, p1}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1153
    return-void

    .line 1150
    :catch_0
    move-exception v2

    .line 1151
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Landroid/accounts/AccountsException;

    const-string v4, "Fail to invalid auth token."

    invoke-direct {v3, v4}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private isAppWhitelistedForFullBR(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1048
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v2}, Lcom/google/android/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "full_backup_restore_whitelist"

    const-string v4, "ALL"

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1050
    .local v0, "whitelist":Ljava/lang/String;
    const-string v2, "ALL"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1051
    const/4 v2, 0x1

    .line 1055
    :goto_0
    return v2

    .line 1054
    :cond_0
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/collect/Sets;->newArraySet([Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v1

    .line 1055
    .local v1, "whitelistedApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method


# virtual methods
.method public abortFullRestore()I
    .locals 1

    .prologue
    .line 952
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->closeFullRestoreHttpConnection()V

    .line 953
    const/4 v0, 0x0

    return v0
.end method

.method public cancelFullBackup()V
    .locals 1

    .prologue
    .line 874
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupDataStream:Lcom/google/android/backup/BlockingFileDataStream;

    invoke-virtual {v0}, Lcom/google/android/backup/BlockingFileDataStream;->signalTransferCancelled()V

    .line 875
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->finishFullBackupInternal()V

    .line 876
    return-void
.end method

.method public declared-synchronized clearBackupData(Landroid/content/pm/PackageInfo;)I
    .locals 9
    .param p1, "packageInfo"    # Landroid/content/pm/PackageInfo;

    .prologue
    const/4 v5, 0x0

    .line 489
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->initRequestGeneratorLocked()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-nez v6, :cond_0

    const/16 v5, -0x3e8

    .line 506
    :goto_0
    monitor-exit p0

    return v5

    .line 490
    :cond_0
    const/4 v0, 0x0

    .line 491
    .local v0, "apiKey":Ljava/lang/String;
    :try_start_1
    iget-object v4, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 493
    .local v4, "packageName":Ljava/lang/String;
    :try_start_2
    iget-object v6, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v6}, Lcom/google/android/backup/BackupTransportService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/16 v7, 0x80

    invoke-virtual {v6, v4, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 495
    .local v1, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v6, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v6, :cond_1

    iget-object v6, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v7, "com.google.android.backup.api_key"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 496
    :cond_1
    iget v6, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_4

    const/4 v3, 0x1

    .line 498
    .local v3, "isSystem":Z
    :goto_1
    if-nez v3, :cond_3

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_3

    .line 499
    :cond_2
    const-string v6, "BackupTransportService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IGNORING WIPE without API key: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 502
    .end local v1    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "isSystem":Z
    :catch_0
    move-exception v2

    .line 503
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_3
    const-string v6, "BackupTransportService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown package in wipe request: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_3
    iget-object v6, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    invoke-virtual {v6, v4, v0}, Lcom/google/android/backup/BackupRequestGenerator;->getApplication(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/backup/BackupRequestGenerator$Application;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Lcom/google/android/backup/BackupRequestGenerator$Application;->deleteAll(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 489
    .end local v0    # "apiKey":Ljava/lang/String;
    .end local v4    # "packageName":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .restart local v0    # "apiKey":Ljava/lang/String;
    .restart local v1    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v4    # "packageName":Ljava/lang/String;
    :cond_4
    move v3, v5

    .line 496
    goto :goto_1
.end method

.method public configurationIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 325
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.backup.SetBackupAccountActivity"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public currentDestinationString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 330
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # invokes: Lcom/google/android/backup/BackupTransportService;->validateAndGetBackupAccount()Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/android/backup/BackupTransportService;->access$000(Lcom/google/android/backup/BackupTransportService;)Landroid/accounts/Account;

    move-result-object v0

    .line 331
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 332
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v1}, Lcom/google/android/backup/BackupTransportService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040006

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 335
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v1}, Lcom/google/android/backup/BackupTransportService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public dataManagementIntent()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 342
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # invokes: Lcom/google/android/backup/BackupTransportService;->validateAndGetBackupAccount()Landroid/accounts/Account;
    invoke-static {v3}, Lcom/google/android/backup/BackupTransportService;->access$000(Lcom/google/android/backup/BackupTransportService;)Landroid/accounts/Account;

    move-result-object v0

    .line 343
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 344
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v3}, Lcom/google/android/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "backup_restore_manage_data_url"

    invoke-static {v3, v4}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 346
    .local v2, "url":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 347
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 348
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 352
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "url":Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dataManagementLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v0}, Lcom/google/android/backup/BackupTransportService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f040007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized finishBackup()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/16 v2, -0x3e8

    const/4 v4, -0x1

    .line 511
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mActiveUid:I
    invoke-static {v3}, Lcom/google/android/backup/BackupTransportService;->access$500(Lcom/google/android/backup/BackupTransportService;)I

    move-result v3

    if-eq v3, v4, :cond_0

    .line 512
    invoke-static {}, Landroid/net/TrafficStats;->setThreadStatsTagBackup()V

    .line 513
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mActiveUid:I
    invoke-static {v3}, Lcom/google/android/backup/BackupTransportService;->access$500(Lcom/google/android/backup/BackupTransportService;)I

    move-result v3

    invoke-static {v3}, Landroid/net/TrafficStats;->setThreadStatsUid(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->isFullBackup:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 518
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupDataStream:Lcom/google/android/backup/BlockingFileDataStream;

    invoke-virtual {v3}, Lcom/google/android/backup/BlockingFileDataStream;->finishDataStream()I

    .line 519
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->finishFullBackupInternal()V

    .line 521
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupStatusCode:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
    :try_end_1
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_1

    .line 556
    :try_start_2
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 557
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 559
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    const/4 v3, -0x1

    # setter for: Lcom/google/android/backup/BackupTransportService;->mActiveUid:I
    invoke-static {v2, v3}, Lcom/google/android/backup/BackupTransportService;->access$502(Lcom/google/android/backup/BackupTransportService;I)I

    .line 560
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    monitor-exit p0

    return v1

    .line 526
    :cond_1
    :try_start_3
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupStatusCode:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    div-int/lit8 v1, v1, 0x64
    :try_end_3
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v3, 0x5

    if-ne v1, v3, :cond_2

    .line 556
    :try_start_4
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 557
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 559
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    const/4 v3, -0x1

    # setter for: Lcom/google/android/backup/BackupTransportService;->mActiveUid:I
    invoke-static {v1, v3}, Lcom/google/android/backup/BackupTransportService;->access$502(Lcom/google/android/backup/BackupTransportService;I)I

    .line 560
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    move v1, v2

    goto :goto_0

    .line 531
    :cond_2
    const/16 v1, -0x3ea

    .line 556
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 557
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 559
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    const/4 v3, -0x1

    # setter for: Lcom/google/android/backup/BackupTransportService;->mActiveUid:I
    invoke-static {v2, v3}, Lcom/google/android/backup/BackupTransportService;->access$502(Lcom/google/android/backup/BackupTransportService;I)I

    .line 560
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 511
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 535
    :cond_3
    :try_start_5
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    if-eqz v3, :cond_4

    .line 536
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    invoke-virtual {v4}, Lcom/google/android/backup/BackupRequestGenerator;->makeFinalRequest()Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v5}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v5

    # invokes: Lcom/google/android/backup/BackupTransportService;->sendRequestLocked(Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    invoke-static {v3, v4, v5}, Lcom/google/android/backup/BackupTransportService;->access$600(Lcom/google/android/backup/BackupTransportService;Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    :try_end_5
    .catch Lcom/google/android/backup/BackupRequestGenerator$PolicyException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 556
    :cond_4
    :goto_1
    :try_start_6
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 557
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 559
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    const/4 v3, -0x1

    # setter for: Lcom/google/android/backup/BackupTransportService;->mActiveUid:I
    invoke-static {v2, v3}, Lcom/google/android/backup/BackupTransportService;->access$502(Lcom/google/android/backup/BackupTransportService;I)I

    .line 560
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 538
    :catch_0
    move-exception v0

    .line 539
    .local v0, "e":Lcom/google/android/backup/BackupRequestGenerator$PolicyException;
    :try_start_7
    const-string v3, "BackupTransportService"

    const-string v4, "Server policy rejection: "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 541
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mAbortScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v3}, Lcom/google/android/backup/BackupTransportService;->access$700(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    .line 542
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    invoke-virtual {v4}, Lcom/google/android/backup/BackupRequestGenerator;->makeAbortRequest()Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mAbortScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v5}, Lcom/google/android/backup/BackupTransportService;->access$700(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v5

    # invokes: Lcom/google/android/backup/BackupTransportService;->sendRequestLocked(Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    invoke-static {v3, v4, v5}, Lcom/google/android/backup/BackupTransportService;->access$600(Lcom/google/android/backup/BackupTransportService;Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    :try_end_7
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1

    .line 549
    .end local v0    # "e":Lcom/google/android/backup/BackupRequestGenerator$PolicyException;
    :catch_1
    move-exception v0

    .line 550
    .local v0, "e":Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException;
    :try_start_8
    const-string v1, "BackupTransportService"

    const-string v2, "Backup server requires initialization: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 551
    const/16 v1, -0x3e9

    .line 556
    :try_start_9
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 557
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 559
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    const/4 v3, -0x1

    # setter for: Lcom/google/android/backup/BackupTransportService;->mActiveUid:I
    invoke-static {v2, v3}, Lcom/google/android/backup/BackupTransportService;->access$502(Lcom/google/android/backup/BackupTransportService;I)I

    .line 560
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 552
    .end local v0    # "e":Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException;
    :catch_2
    move-exception v0

    .line 553
    .local v0, "e":Ljava/io/IOException;
    :try_start_a
    const-string v1, "BackupTransportService"

    const-string v3, "Error sending final backup to server: "

    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 556
    :try_start_b
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 557
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 559
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    const/4 v3, -0x1

    # setter for: Lcom/google/android/backup/BackupTransportService;->mActiveUid:I
    invoke-static {v1, v3}, Lcom/google/android/backup/BackupTransportService;->access$502(Lcom/google/android/backup/BackupTransportService;I)I

    .line 560
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    move v1, v2

    goto/16 :goto_0

    .line 556
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v1

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 557
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    .line 559
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    const/4 v3, -0x1

    # setter for: Lcom/google/android/backup/BackupTransportService;->mActiveUid:I
    invoke-static {v2, v3}, Lcom/google/android/backup/BackupTransportService;->access$502(Lcom/google/android/backup/BackupTransportService;I)I

    .line 560
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    throw v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0
.end method

.method public declared-synchronized finishRestore()V
    .locals 1

    .prologue
    .line 692
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;

    .line 693
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    .line 694
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreData:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 695
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreDocURLs:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 697
    monitor-exit p0

    return-void

    .line 692
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAvailableRestoreSets()[Landroid/app/backup/RestoreSet;
    .locals 6

    .prologue
    .line 567
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mRestoreScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v2}, Lcom/google/android/backup/BackupTransportService;->access$800(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    .line 568
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-static {}, Lcom/google/android/backup/RestoreRequestProcessor;->makeDeviceRequest()Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mRestoreScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v4}, Lcom/google/android/backup/BackupTransportService;->access$800(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v4

    # invokes: Lcom/google/android/backup/BackupTransportService;->sendRequestLocked(Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    invoke-static {v2, v3, v4}, Lcom/google/android/backup/BackupTransportService;->access$600(Lcom/google/android/backup/BackupTransportService;Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/backup/RestoreRequestProcessor;->processDeviceResponse(Lcom/google/common/io/protocol/ProtoBuf;)[Landroid/app/backup/RestoreSet;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 574
    :goto_0
    monitor-exit p0

    return-object v1

    .line 572
    :catch_0
    move-exception v0

    .line 573
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    const-string v2, "BackupTransportService"

    const-string v3, "Error getting device list from server: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574
    const/4 v1, 0x0

    goto :goto_0

    .line 567
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized getCurrentRestoreSet()J
    .locals 4

    .prologue
    .line 580
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v0}, Lcom/google/android/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getNextFullRestoreDataChunk(Landroid/os/ParcelFileDescriptor;)I
    .locals 10
    .param p1, "socket"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    const/4 v6, -0x1

    const/16 v5, -0x3e8

    .line 883
    :try_start_0
    iget-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v7}, Lcom/google/android/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "full_restore_require_wifi"

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v3

    .line 885
    .local v3, "fullRestoreNeedsWifi":Z
    if-eqz v3, :cond_0

    .line 886
    iget-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mConnManager:Landroid/net/ConnectivityManager;
    invoke-static {v7}, Lcom/google/android/backup/BackupTransportService;->access$900(Lcom/google/android/backup/BackupTransportService;)Landroid/net/ConnectivityManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/ConnectivityManager;->isActiveNetworkMetered()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 887
    const-string v6, "BackupTransportService"

    const-string v7, "Lost unmetered network; canceling full restore"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->closeFullRestoreHttpConnection()V
    :try_end_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 945
    invoke-static {p1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    move v1, v5

    .end local v3    # "fullRestoreNeedsWifi":Z
    :goto_0
    return v1

    .line 895
    .restart local v3    # "fullRestoreNeedsWifi":Z
    :cond_0
    :try_start_1
    iget-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreConnection:Ljavax/net/ssl/HttpsURLConnection;
    :try_end_1
    .catch Landroid/accounts/AccountsException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v7, :cond_1

    .line 897
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->createFullRestoreHttpConnection()Ljavax/net/ssl/HttpsURLConnection;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreConnection:Ljavax/net/ssl/HttpsURLConnection;

    .line 898
    iget-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreConnection:Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v7}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreDownloadStream:Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 906
    :cond_1
    const v7, 0x8000

    :try_start_3
    new-array v0, v7, [B
    :try_end_3
    .catch Landroid/accounts/AccountsException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 909
    .local v0, "buffer":[B
    :try_start_4
    iget-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreDownloadStream:Ljava/io/InputStream;

    invoke-virtual {v7, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Landroid/accounts/AccountsException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v1

    .line 917
    .local v1, "bytesRead":I
    if-ne v1, v6, :cond_2

    .line 919
    :try_start_5
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->closeFullRestoreHttpConnection()V

    .line 920
    const-string v7, "BackupTransportService"

    const-string v8, "Reach end of http content -- NO MORE DATA"

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Landroid/accounts/AccountsException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 945
    invoke-static {p1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    move v1, v6

    goto :goto_0

    .line 899
    .end local v0    # "buffer":[B
    .end local v1    # "bytesRead":I
    :catch_0
    move-exception v2

    .line 900
    .local v2, "e":Ljava/io/IOException;
    :try_start_6
    const-string v6, "BackupTransportService"

    const-string v7, "Http connection error."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 901
    throw v2
    :try_end_6
    .catch Landroid/accounts/AccountsException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 937
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fullRestoreNeedsWifi":Z
    :catch_1
    move-exception v2

    .line 938
    .local v2, "e":Landroid/accounts/AccountsException;
    :try_start_7
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->closeFullRestoreHttpConnection()V

    .line 939
    const-string v6, "BackupTransportService"

    invoke-virtual {v2}, Landroid/accounts/AccountsException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 945
    invoke-static {p1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    move v1, v5

    goto :goto_0

    .line 910
    .end local v2    # "e":Landroid/accounts/AccountsException;
    .restart local v0    # "buffer":[B
    .restart local v3    # "fullRestoreNeedsWifi":Z
    :catch_2
    move-exception v2

    .line 911
    .local v2, "e":Ljava/io/IOException;
    :try_start_8
    const-string v6, "BackupTransportService"

    const-string v7, "HTTP reading error."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 912
    throw v2
    :try_end_8
    .catch Landroid/accounts/AccountsException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 941
    .end local v0    # "buffer":[B
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fullRestoreNeedsWifi":Z
    :catch_3
    move-exception v2

    .line 942
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_9
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->closeFullRestoreHttpConnection()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 943
    const/16 v1, -0x3ea

    .line 945
    invoke-static {p1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_0

    .line 924
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "buffer":[B
    .restart local v1    # "bytesRead":I
    .restart local v3    # "fullRestoreNeedsWifi":Z
    :cond_2
    :try_start_a
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_a
    .catch Landroid/accounts/AccountsException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 928
    .local v4, "outputStream":Ljava/io/FileOutputStream;
    const/4 v6, 0x0

    :try_start_b
    invoke-virtual {v4, v0, v6, v1}, Ljava/io/FileOutputStream;->write([BII)V

    .line 929
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V

    .line 930
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4
    .catch Landroid/accounts/AccountsException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 945
    invoke-static {p1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_0

    .line 931
    :catch_4
    move-exception v2

    .line 932
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_c
    const-string v6, "BackupTransportService"

    const-string v7, "Fail to write to socket."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 933
    throw v2
    :try_end_c
    .catch Landroid/accounts/AccountsException; {:try_start_c .. :try_end_c} :catch_1
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 945
    .end local v0    # "buffer":[B
    .end local v1    # "bytesRead":I
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fullRestoreNeedsWifi":Z
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v5

    invoke-static {p1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    throw v5
.end method

.method public declared-synchronized getRestoreData(Landroid/os/ParcelFileDescriptor;)I
    .locals 6
    .param p1, "outFd"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    const/16 v3, -0x3e8

    .line 669
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    if-nez v4, :cond_0

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "no package to restore"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 672
    :cond_0
    :try_start_1
    new-instance v0, Landroid/app/backup/BackupDataOutput;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/backup/BackupDataOutput;-><init>(Ljava/io/FileDescriptor;)V

    .line 673
    .local v0, "data":Landroid/app/backup/BackupDataOutput;
    new-instance v2, Lcom/google/android/backup/BackupDataReassembler;

    invoke-direct {v2, v0}, Lcom/google/android/backup/BackupDataReassembler;-><init>(Landroid/app/backup/BackupDataOutput;)V

    .line 674
    .local v2, "reassembler":Lcom/google/android/backup/BackupDataReassembler;
    :goto_0
    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->mLastRestoreApp:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 675
    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreData:Ljava/util/HashMap;

    invoke-virtual {v2, v4}, Lcom/google/android/backup/BackupDataReassembler;->writeSomeData(Ljava/util/Map;)V

    .line 676
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->getRestoreDataLocked()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/backup/BackupDataReassembler$InvalidDataException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 679
    .end local v0    # "data":Landroid/app/backup/BackupDataOutput;
    .end local v2    # "reassembler":Lcom/google/android/backup/BackupDataReassembler;
    :catch_0
    move-exception v1

    .line 680
    .local v1, "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "BackupTransportService"

    const-string v5, "Error getting restore data from server: "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 681
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 686
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    monitor-exit p0

    return v3

    .line 678
    .restart local v0    # "data":Landroid/app/backup/BackupDataOutput;
    .restart local v2    # "reassembler":Lcom/google/android/backup/BackupDataReassembler;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 683
    .end local v0    # "data":Landroid/app/backup/BackupDataOutput;
    .end local v2    # "reassembler":Lcom/google/android/backup/BackupDataReassembler;
    :catch_1
    move-exception v1

    .line 684
    .local v1, "e":Lcom/google/android/backup/BackupDataReassembler$InvalidDataException;
    :try_start_3
    const-string v4, "BackupTransportService"

    const-string v5, "Error in restore data from server"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 685
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized initializeDevice()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/16 v1, -0x3e8

    .line 401
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->initRequestGeneratorLocked()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 421
    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    .line 403
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    invoke-virtual {v3}, Lcom/google/android/backup/BackupRequestGenerator;->initializeDevice()V

    .line 407
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # invokes: Lcom/google/android/backup/BackupTransportService;->getBackupManager()Landroid/app/backup/BackupManager;
    invoke-static {v3}, Lcom/google/android/backup/BackupTransportService;->access$300(Lcom/google/android/backup/BackupTransportService;)Landroid/app/backup/BackupManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/backup/BackupManager;->isBackupEnabled()Z

    move-result v0

    .line 408
    .local v0, "isBackupEnabled":Z
    if-nez v0, :cond_2

    .line 409
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # invokes: Lcom/google/android/backup/BackupTransportService;->cancelSetBackupAccountNotification()V
    invoke-static {v1}, Lcom/google/android/backup/BackupTransportService;->access$400(Lcom/google/android/backup/BackupTransportService;)V

    move v1, v2

    .line 410
    goto :goto_0

    .line 414
    :cond_2
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # invokes: Lcom/google/android/backup/BackupTransportService;->validateAndGetBackupAccount()Landroid/accounts/Account;
    invoke-static {v3}, Lcom/google/android/backup/BackupTransportService;->access$000(Lcom/google/android/backup/BackupTransportService;)Landroid/accounts/Account;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 415
    goto :goto_0

    .line 401
    .end local v0    # "isBackupEnabled":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 320
    const-string v0, "com.google.android.backup/.BackupTransportService"

    return-object v0
.end method

.method public declared-synchronized nextRestorePackage()Landroid/app/backup/RestoreDescription;
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 607
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;

    if-nez v5, :cond_0

    .line 608
    const-string v5, "BackupTransportService"

    const-string v7, "Restore processing aborted, no more packages"

    invoke-static {v5, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v5, v6

    .line 663
    :goto_0
    monitor-exit p0

    return-object v5

    .line 613
    :cond_0
    :try_start_1
    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v5}, Lcom/google/android/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 614
    .local v1, "cr":Landroid/content/ContentResolver;
    const-string v5, "enable_full_restore"

    const/4 v8, 0x1

    invoke-static {v1, v5, v8}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v3

    .line 616
    .local v3, "enableFullRestore":Z
    const-string v5, "full_restore_require_wifi"

    const/4 v8, 0x1

    invoke-static {v1, v5, v8}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v4

    .line 621
    .local v4, "fullRestoreNeedsWifi":Z
    :goto_1
    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$1;->mLastRestoreApp:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 622
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->getRestoreDataLocked()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 660
    .end local v1    # "cr":Landroid/content/ContentResolver;
    .end local v3    # "enableFullRestore":Z
    .end local v4    # "fullRestoreNeedsWifi":Z
    :catch_0
    move-exception v2

    .line 661
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    const-string v5, "BackupTransportService"

    const-string v7, "Error getting restore data from server: "

    invoke-static {v5, v7, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 662
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v5, v6

    .line 663
    goto :goto_0

    .line 624
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "cr":Landroid/content/ContentResolver;
    .restart local v3    # "enableFullRestore":Z
    .restart local v4    # "fullRestoreNeedsWifi":Z
    :cond_1
    :try_start_3
    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    if-nez v5, :cond_2

    .line 625
    sget-object v5, Landroid/app/backup/RestoreDescription;->NO_MORE_PACKAGES:Landroid/app/backup/RestoreDescription;

    goto :goto_0

    .line 627
    :cond_2
    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->mLastRestoreApp:Ljava/lang/String;

    .line 628
    const-string v5, "BackupTransportService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Current restore app : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreDocURLs:Ljava/util/HashMap;

    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 631
    const-string v8, "BackupTransportService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "A full restore : "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->mFullRestoreDocURLs:Ljava/util/HashMap;

    iget-object v10, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    if-eqz v3, :cond_6

    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/google/android/backup/BackupTransportService$1;->isAppWhitelistedForFullBR(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 637
    if-eqz v4, :cond_3

    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mConnManager:Landroid/net/ConnectivityManager;
    invoke-static {v5}, Lcom/google/android/backup/BackupTransportService;->access$900(Lcom/google/android/backup/BackupTransportService;)Landroid/net/ConnectivityManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/ConnectivityManager;->isActiveNetworkMetered()Z

    move-result v5

    if-nez v5, :cond_4

    :cond_3
    move v0, v7

    .line 639
    .local v0, "connectionOkay":Z
    :goto_2
    if-nez v0, :cond_5

    .line 640
    const-string v5, "BackupTransportService"

    const-string v8, "Cannot full restore over metered network"

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 607
    .end local v0    # "connectionOkay":Z
    .end local v1    # "cr":Landroid/content/ContentResolver;
    .end local v3    # "enableFullRestore":Z
    .end local v4    # "fullRestoreNeedsWifi":Z
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 637
    .restart local v1    # "cr":Landroid/content/ContentResolver;
    .restart local v3    # "enableFullRestore":Z
    .restart local v4    # "fullRestoreNeedsWifi":Z
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 643
    .restart local v0    # "connectionOkay":Z
    :cond_5
    :try_start_4
    new-instance v5, Landroid/app/backup/RestoreDescription;

    iget-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    const/4 v8, 0x2

    invoke-direct {v5, v7, v8}, Landroid/app/backup/RestoreDescription;-><init>(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 644
    .end local v0    # "connectionOkay":Z
    :cond_6
    iget-object v5, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreData:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 647
    const-string v5, "BackupTransportService"

    const-string v8, "Full restore feature is disabled by gservice and no key/value restore."

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 657
    :cond_7
    const-string v5, "BackupTransportService"

    const-string v7, "A key/value pairs restore"

    invoke-static {v5, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    new-instance v5, Landroid/app/backup/RestoreDescription;

    iget-object v7, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-direct {v5, v7, v8}, Landroid/app/backup/RestoreDescription;-><init>(Ljava/lang/String;I)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method public declared-synchronized performBackup(Landroid/content/pm/PackageInfo;Landroid/os/ParcelFileDescriptor;)I
    .locals 13
    .param p1, "pkg"    # Landroid/content/pm/PackageInfo;
    .param p2, "inFd"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    const v12, 0x7d000

    const/16 v7, -0x3e8

    const/4 v6, 0x0

    .line 426
    monitor-enter p0

    :try_start_0
    iget-object v5, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 427
    .local v5, "packageName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 430
    .local v0, "apiKey":Ljava/lang/String;
    :try_start_1
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v8}, Lcom/google/android/backup/BackupTransportService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x80

    invoke-virtual {v8, v5, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 433
    .local v1, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v8, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v8, :cond_0

    iget-object v8, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v9, "com.google.android.backup.api_key"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 434
    :cond_0
    iget v8, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_2

    const/4 v4, 0x1

    .line 436
    .local v4, "isSystem":Z
    :goto_0
    if-nez v4, :cond_3

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_3

    .line 437
    :cond_1
    const-string v8, "BackupTransportService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IGNORING BACKUP DATA without API key: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483
    .end local v1    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v4    # "isSystem":Z
    :goto_1
    monitor-exit p0

    return v6

    .restart local v1    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :cond_2
    move v4, v6

    .line 434
    goto :goto_0

    .line 440
    .end local v1    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v2

    .line 441
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_2
    const-string v8, "BackupTransportService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unknown package in backup request: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_3
    iget-object v9, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    iget-object v8, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v8, :cond_4

    iget-object v8, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v8, v8, Landroid/content/pm/ApplicationInfo;->uid:I

    :goto_2
    # setter for: Lcom/google/android/backup/BackupTransportService;->mActiveUid:I
    invoke-static {v9, v8}, Lcom/google/android/backup/BackupTransportService;->access$502(Lcom/google/android/backup/BackupTransportService;I)I

    .line 450
    invoke-static {}, Landroid/net/TrafficStats;->setThreadStatsTagBackup()V

    .line 451
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mActiveUid:I
    invoke-static {v8}, Lcom/google/android/backup/BackupTransportService;->access$500(Lcom/google/android/backup/BackupTransportService;)I

    move-result v8

    invoke-static {v8}, Landroid/net/TrafficStats;->setThreadStatsUid(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 454
    :try_start_3
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->initRequestGeneratorLocked()Z
    :try_end_3
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v8

    if-nez v8, :cond_5

    .line 482
    :try_start_4
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 483
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v6, v7

    goto :goto_1

    .line 447
    :cond_4
    const/16 v8, 0x3e8

    goto :goto_2

    .line 457
    :cond_5
    :try_start_5
    new-instance v3, Lcom/google/android/backup/BackupDataFragmenter;

    new-instance v8, Landroid/app/backup/BackupDataInput;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/app/backup/BackupDataInput;-><init>(Ljava/io/FileDescriptor;)V

    iget-object v9, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    invoke-virtual {v9, v5, v0}, Lcom/google/android/backup/BackupRequestGenerator;->getApplication(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/backup/BackupRequestGenerator$Application;

    move-result-object v9

    invoke-direct {v3, v8, v9}, Lcom/google/android/backup/BackupDataFragmenter;-><init>(Landroid/app/backup/BackupDataInput;Lcom/google/android/backup/BackupRequestGenerator$Application;)V
    :try_end_5
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 461
    .local v3, "fragmenter":Lcom/google/android/backup/BackupDataFragmenter;
    :cond_6
    :goto_3
    const v8, 0x7d000

    :try_start_6
    invoke-virtual {v3, v8}, Lcom/google/android/backup/BackupDataFragmenter;->readSomeData(I)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 462
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    invoke-virtual {v8}, Lcom/google/android/backup/BackupRequestGenerator;->approximateSize()I

    move-result v8

    if-lt v8, v12, :cond_6

    .line 463
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    iget-object v9, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    invoke-virtual {v9}, Lcom/google/android/backup/BackupRequestGenerator;->makePartialRequest()Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v10}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v10

    # invokes: Lcom/google/android/backup/BackupTransportService;->sendRequestLocked(Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    invoke-static {v8, v9, v10}, Lcom/google/android/backup/BackupTransportService;->access$600(Lcom/google/android/backup/BackupTransportService;Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;

    .line 464
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v8}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v8

    const-wide/16 v10, 0x0

    invoke-virtual {v8, v10, v11}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V
    :try_end_6
    .catch Lcom/google/android/backup/BackupRequestGenerator$PolicyException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_3

    .line 467
    :catch_1
    move-exception v2

    .line 468
    .local v2, "e":Lcom/google/android/backup/BackupRequestGenerator$PolicyException;
    :try_start_7
    const-string v8, "BackupTransportService"

    const-string v9, "Server policy rejection: "

    invoke-static {v8, v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 470
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mAbortScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v8}, Lcom/google/android/backup/BackupTransportService;->access$700(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v8

    const-wide/16 v10, 0x0

    invoke-virtual {v8, v10, v11}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    .line 471
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    iget-object v9, p0, Lcom/google/android/backup/BackupTransportService$1;->mRequestGen:Lcom/google/android/backup/BackupRequestGenerator;

    invoke-virtual {v9}, Lcom/google/android/backup/BackupRequestGenerator;->makeAbortRequest()Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mAbortScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v10}, Lcom/google/android/backup/BackupTransportService;->access$700(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v10

    # invokes: Lcom/google/android/backup/BackupTransportService;->sendRequestLocked(Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    invoke-static {v8, v9, v10}, Lcom/google/android/backup/BackupTransportService;->access$600(Lcom/google/android/backup/BackupTransportService;Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    :try_end_7
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 482
    .end local v2    # "e":Lcom/google/android/backup/BackupRequestGenerator$PolicyException;
    :cond_7
    :try_start_8
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 483
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 426
    .end local v0    # "apiKey":Ljava/lang/String;
    .end local v3    # "fragmenter":Lcom/google/android/backup/BackupDataFragmenter;
    .end local v5    # "packageName":Ljava/lang/String;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 475
    .restart local v0    # "apiKey":Ljava/lang/String;
    .restart local v5    # "packageName":Ljava/lang/String;
    :catch_2
    move-exception v2

    .line 476
    .local v2, "e":Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException;
    :try_start_9
    const-string v6, "BackupTransportService"

    const-string v7, "Uninitialized device: "

    invoke-static {v6, v7, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 477
    const/16 v6, -0x3e9

    .line 482
    :try_start_a
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 483
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_1

    .line 478
    .end local v2    # "e":Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException;
    :catch_3
    move-exception v2

    .line 479
    .local v2, "e":Ljava/io/IOException;
    :try_start_b
    const-string v6, "BackupTransportService"

    const-string v8, "Error sending partial backup to server: "

    invoke-static {v6, v8, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 482
    :try_start_c
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 483
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    move v6, v7

    goto/16 :goto_1

    .line 482
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v6

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 483
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    throw v6
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0
.end method

.method public declared-synchronized performFullBackup(Landroid/content/pm/PackageInfo;Landroid/os/ParcelFileDescriptor;)I
    .locals 17
    .param p1, "targetPackage"    # Landroid/content/pm/PackageInfo;
    .param p2, "socket"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 718
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p1

    iget-object v15, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 719
    .local v15, "packageName":Ljava/lang/String;
    const-string v2, "BackupTransportService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Attempt to do full backup on "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    const v2, 0x32898

    const-string v3, "Attempt to do full backup on %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v15, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 723
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v2}, Lcom/google/android/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "enable_full_backup"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v12

    .line 725
    .local v12, "enableFullBackup":Z
    if-nez v12, :cond_0

    .line 726
    const-string v2, "BackupTransportService"

    const-string v3, "Full backup feature is disabled by gservice."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 727
    const/16 v2, -0x3e8

    .line 840
    :goto_0
    monitor-exit p0

    return v2

    .line 730
    :cond_0
    const/4 v2, 0x1

    const/4 v3, 0x1

    :try_start_1
    new-array v3, v3, [Landroid/content/pm/PackageInfo;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/backup/BackupTransportService$1;->getEligiblePackagesForFullBR(Z[Landroid/content/pm/PackageInfo;)[Ljava/lang/String;

    move-result-object v11

    .line 731
    .local v11, "eligiblePackages":[Ljava/lang/String;
    array-length v2, v11

    if-nez v2, :cond_1

    .line 732
    const/16 v2, -0x3ea

    goto :goto_0

    .line 735
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->mSocketFileDescriptor:Landroid/os/ParcelFileDescriptor;

    if-eqz v2, :cond_2

    .line 736
    const-string v2, "BackupTransportService"

    const-string v3, "Attempt to initiate full backup while one is in progress"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    const/16 v2, -0x3ea

    goto :goto_0

    .line 740
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_4

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    :goto_1
    # setter for: Lcom/google/android/backup/BackupTransportService;->mActiveUid:I
    invoke-static {v3, v2}, Lcom/google/android/backup/BackupTransportService;->access$502(Lcom/google/android/backup/BackupTransportService;I)I

    .line 743
    const/16 v2, -0xfd

    invoke-static {v2}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 744
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mActiveUid:I
    invoke-static {v2}, Lcom/google/android/backup/BackupTransportService;->access$500(Lcom/google/android/backup/BackupTransportService;)I

    move-result v2

    invoke-static {v2}, Landroid/net/TrafficStats;->setThreadStatsUid(I)V

    .line 748
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/backup/BackupTransportService$1;->mSocketFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 749
    new-instance v2, Lcom/google/android/backup/BlockingFileDataStream;

    new-instance v3, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/backup/BackupTransportService$1;->mSocketFileDescriptor:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    invoke-direct {v2, v3}, Lcom/google/android/backup/BlockingFileDataStream;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupDataStream:Lcom/google/android/backup/BlockingFileDataStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 752
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v3}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v3

    move-object/from16 v0, p1

    # invokes: Lcom/google/android/backup/BackupTransportService;->generateFullBackupRequest(Landroid/content/pm/PackageInfo;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    invoke-static {v2, v0, v3}, Lcom/google/android/backup/BackupTransportService;->access$1000(Lcom/google/android/backup/BackupTransportService;Landroid/content/pm/PackageInfo;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v13

    .line 754
    .local v13, "fullBackupRequest":Lcom/google/common/io/protocol/ProtoBuf;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->scottyUploadClient:Lcom/google/uploader/client/UploadClient;

    if-nez v2, :cond_3

    .line 755
    new-instance v14, Lcom/google/uploader/client/HttpUrlConnectionHttpClient;

    invoke-direct {v14}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient;-><init>()V

    .line 756
    .local v14, "httpClient":Lcom/google/uploader/client/HttpClient;
    invoke-static {v14}, Lcom/google/uploader/client/UploadClientImpl;->newBuilder(Lcom/google/uploader/client/HttpClient;)Lcom/google/uploader/client/UploadClientImpl$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/uploader/client/UploadClientImpl$Builder;->build()Lcom/google/uploader/client/UploadClient;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->scottyUploadClient:Lcom/google/uploader/client/UploadClient;

    .line 759
    .end local v14    # "httpClient":Lcom/google/uploader/client/HttpClient;
    :cond_3
    new-instance v5, Lcom/google/api/client/http/HttpHeaders;

    invoke-direct {v5}, Lcom/google/api/client/http/HttpHeaders;-><init>()V

    .line 760
    .local v5, "httpHeaders":Lcom/google/api/client/http/HttpHeaders;
    const/16 v2, 0xe

    invoke-virtual {v13, v2}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 761
    .local v9, "authToken":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GoogleLogin auth="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/google/api/client/http/HttpHeaders;->setAuthorization(Ljava/lang/String;)Lcom/google/api/client/http/HttpHeaders;

    .line 763
    invoke-static {}, Lcom/google/uploader/client/TransferOptions;->newBuilder()Lcom/google/uploader/client/TransferOptions$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/uploader/client/TransferOptions$Builder;->build()Lcom/google/uploader/client/TransferOptions;

    move-result-object v8

    .line 764
    .local v8, "options":Lcom/google/uploader/client/TransferOptions;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->scottyUploadClient:Lcom/google/uploader/client/UploadClient;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v3}, Lcom/google/android/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "scotty_server_upload_url"

    const-string v6, "https://android.clients.google.com/backup/upload"

    invoke-static {v3, v4, v6}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "PUT"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupDataStream:Lcom/google/android/backup/BlockingFileDataStream;

    invoke-virtual {v13}, Lcom/google/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v7

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-static {v7, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v7

    invoke-interface/range {v2 .. v8}, Lcom/google/uploader/client/UploadClient;->createTransfer(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;Lcom/google/uploader/client/DataStream;Ljava/lang/String;Lcom/google/uploader/client/TransferOptions;)Lcom/google/uploader/client/Transfer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->scottyHttpTransfer:Lcom/google/uploader/client/Transfer;

    .line 772
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->scottyHttpTransfer:Lcom/google/uploader/client/Transfer;

    new-instance v3, Lcom/google/android/backup/BackupTransportService$1$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v15, v9}, Lcom/google/android/backup/BackupTransportService$1$1;-><init>(Lcom/google/android/backup/BackupTransportService$1;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lcom/google/uploader/client/Transfer;->attachListener(Lcom/google/uploader/client/TransferListener;I)V

    .line 822
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->scottyHttpTransfer:Lcom/google/uploader/client/Transfer;

    invoke-interface {v2}, Lcom/google/uploader/client/Transfer;->send()Ljava/util/concurrent/Future;

    .line 824
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->isFullBackup:Ljava/lang/Boolean;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 825
    const/4 v2, 0x0

    .line 839
    :try_start_3
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 840
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 718
    .end local v5    # "httpHeaders":Lcom/google/api/client/http/HttpHeaders;
    .end local v8    # "options":Lcom/google/uploader/client/TransferOptions;
    .end local v9    # "authToken":Ljava/lang/String;
    .end local v11    # "eligiblePackages":[Ljava/lang/String;
    .end local v12    # "enableFullBackup":Z
    .end local v13    # "fullBackupRequest":Lcom/google/common/io/protocol/ProtoBuf;
    .end local v15    # "packageName":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 740
    .restart local v11    # "eligiblePackages":[Ljava/lang/String;
    .restart local v12    # "enableFullBackup":Z
    .restart local v15    # "packageName":Ljava/lang/String;
    :cond_4
    const/16 v2, 0x3e8

    goto/16 :goto_1

    .line 827
    :catch_0
    move-exception v10

    .line 828
    .local v10, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 829
    const-string v2, "BackupTransportService"

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    :cond_5
    const-string v2, "BackupTransportService"

    const-string v3, "Exception when generating full backup request."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupDataStream:Lcom/google/android/backup/BlockingFileDataStream;

    .line 833
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/backup/BackupTransportService$1;->mSocketFileDescriptor:Landroid/os/ParcelFileDescriptor;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 837
    const/16 v2, -0x3e8

    .line 839
    :try_start_5
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 840
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    goto/16 :goto_0

    .line 839
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 840
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsUid()V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public declared-synchronized requestBackupTime()J
    .locals 6

    .prologue
    .line 386
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v2}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    .line 390
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # invokes: Lcom/google/android/backup/BackupTransportService;->validateAndGetBackupAccount()Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/backup/BackupTransportService;->access$000(Lcom/google/android/backup/BackupTransportService;)Landroid/accounts/Account;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 391
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v2}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mSchedulerOptions:Lcom/android/common/OperationScheduler$Options;
    invoke-static {v3}, Lcom/google/android/backup/BackupTransportService;->access$200(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler$Options;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v0

    .line 392
    .local v0, "next":J
    const-wide/16 v2, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, v0, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 395
    .end local v0    # "next":J
    :goto_0
    monitor-exit p0

    return-wide v2

    :cond_0
    const-wide/32 v2, 0x240c8400

    goto :goto_0

    .line 386
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized requestFullBackupTime()J
    .locals 6

    .prologue
    .line 702
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v2}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    .line 706
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # invokes: Lcom/google/android/backup/BackupTransportService;->validateAndGetBackupAccount()Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/backup/BackupTransportService;->access$000(Lcom/google/android/backup/BackupTransportService;)Landroid/accounts/Account;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 707
    iget-object v2, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v2}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mSchedulerOptions:Lcom/android/common/OperationScheduler$Options;
    invoke-static {v3}, Lcom/google/android/backup/BackupTransportService;->access$200(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler$Options;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v0

    .line 708
    .local v0, "next":J
    const-wide/16 v2, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, v0, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 711
    .end local v0    # "next":J
    :goto_0
    monitor-exit p0

    return-wide v2

    :cond_0
    const-wide/32 v2, 0x240c8400

    goto :goto_0

    .line 702
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized sendBackupData(I)I
    .locals 1
    .param p1, "numBytes"    # I

    .prologue
    .line 846
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupDataStream:Lcom/google/android/backup/BlockingFileDataStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1;->fullBackupDataStream:Lcom/google/android/backup/BlockingFileDataStream;

    invoke-virtual {v0, p1}, Lcom/google/android/backup/BlockingFileDataStream;->pushAvailableData(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 848
    const/4 v0, 0x0

    .line 850
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/16 v0, -0x3ea

    goto :goto_0

    .line 846
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized startRestore(J[Landroid/content/pm/PackageInfo;)I
    .locals 5
    .param p1, "token"    # J
    .param p3, "packages"    # [Landroid/content/pm/PackageInfo;

    .prologue
    const/4 v2, 0x0

    .line 585
    monitor-enter p0

    const/4 v3, 0x0

    :try_start_0
    iput-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;

    .line 586
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreApp:Ljava/lang/String;

    .line 587
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreData:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 588
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->mLastRestoreApp:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 591
    const/4 v3, 0x0

    :try_start_1
    invoke-direct {p0, v3, p3}, Lcom/google/android/backup/BackupTransportService$1;->getEligiblePackagesForFullBR(Z[Landroid/content/pm/PackageInfo;)[Ljava/lang/String;

    move-result-object v1

    .line 593
    .local v1, "eligiblePackages":[Ljava/lang/String;
    new-instance v3, Lcom/google/android/backup/RestoreRequestProcessor;

    const v4, 0x7d000

    invoke-direct {v3, p1, p2, v1, v4}, Lcom/google/android/backup/RestoreRequestProcessor;-><init>(J[Ljava/lang/String;I)V

    iput-object v3, p0, Lcom/google/android/backup/BackupTransportService$1;->mRestoreProc:Lcom/google/android/backup/RestoreRequestProcessor;

    .line 597
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService$1;->getRestoreDataLocked()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601
    .end local v1    # "eligiblePackages":[Ljava/lang/String;
    :goto_0
    monitor-exit p0

    return v2

    .line 599
    :catch_0
    move-exception v0

    .line 600
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    const-string v2, "BackupTransportService"

    const-string v3, "Error getting restore data from server: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 601
    const/16 v2, -0x3e8

    goto :goto_0

    .line 585
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public transportDirName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 363
    const-string v0, "com.google.android.backup.BackupTransportService"

    return-object v0
.end method
