.class public Lcom/google/android/backup/SetBackupAccountService;
.super Lcom/google/android/backup/BackupTransportService;
.source "SetBackupAccountService.java"


# static fields
.field private static final GMS_COMPONENT_NAME:Landroid/content/ComponentName;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.gms"

    const-string v2, "com.google.android.gms.backup.SetBackupAccountService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/backup/SetBackupAccountService;->GMS_COMPONENT_NAME:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService;-><init>()V

    return-void
.end method

.method private tryProxyToGmsSetBackupAccountService(Landroid/content/Intent;)Z
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 59
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/backup/SetBackupAccountService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    sget-object v5, Lcom/google/android/backup/SetBackupAccountService;->GMS_COMPONENT_NAME:Landroid/content/ComponentName;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;

    move-result-object v2

    .line 60
    .local v2, "info":Landroid/content/pm/ServiceInfo;
    invoke-virtual {v2}, Landroid/content/pm/ServiceInfo;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 61
    const-string v4, "SetBackupAccountService"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "SetBackupAccountService"

    const-string v5, "GMS backup not enabled"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 80
    .end local v2    # "info":Landroid/content/pm/ServiceInfo;
    :cond_0
    :goto_0
    return v3

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    const-string v4, "SetBackupAccountService"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "SetBackupAccountService"

    const-string v5, "GMS backup not found"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 78
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 79
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "SetBackupAccountService"

    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 69
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "info":Landroid/content/pm/ServiceInfo;
    :cond_1
    :try_start_2
    const-string v4, "SetBackupAccountService"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "SetBackupAccountService"

    const-string v5, "GMS backup enabled"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    :cond_2
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    sget-object v5, Lcom/google/android/backup/SetBackupAccountService;->GMS_COMPONENT_NAME:Landroid/content/ComponentName;

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "backupAccount"

    const-string v6, "backupAccount"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "backupUserHandle"

    const-string v6, "backupUserHandle"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 76
    .local v1, "gmsIntent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/google/android/backup/SetBackupAccountService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 77
    const/4 v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 28
    const-string v4, "com.google.android.backup.SetBackupAccount"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/backup/SetBackupAccountService;->tryProxyToGmsSetBackupAccountService(Landroid/content/Intent;)Z

    move-result v2

    .line 32
    .local v2, "proxied":Z
    if-nez v2, :cond_0

    .line 33
    const-string v4, "backupAccount"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 34
    .local v0, "account":Landroid/accounts/Account;
    const-string v4, "backupUserHandle"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/os/UserHandle;

    .line 35
    .local v3, "userHandle":Landroid/os/UserHandle;
    if-eqz v0, :cond_0

    invoke-virtual {v3}, Landroid/os/UserHandle;->isOwner()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 38
    invoke-static {}, Lcom/google/android/backup/BackupTransportService;->getTransportInstance()Lcom/google/android/backup/BackupTransportService;

    move-result-object v1

    .line 39
    .local v1, "bts":Lcom/google/android/backup/BackupTransportService;
    if-eqz v1, :cond_1

    .line 40
    const-string v4, "SetBackupAccountService"

    const-string v5, "Setting backup account"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-virtual {v1, v0}, Lcom/google/android/backup/BackupTransportService;->saveBackupAccount(Landroid/accounts/Account;)V

    .line 52
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "bts":Lcom/google/android/backup/BackupTransportService;
    .end local v2    # "proxied":Z
    .end local v3    # "userHandle":Landroid/os/UserHandle;
    :cond_0
    :goto_0
    invoke-virtual {p0, p3}, Lcom/google/android/backup/SetBackupAccountService;->stopSelf(I)V

    .line 53
    const/4 v4, 0x2

    return v4

    .line 46
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "bts":Lcom/google/android/backup/BackupTransportService;
    .restart local v2    # "proxied":Z
    .restart local v3    # "userHandle":Landroid/os/UserHandle;
    :cond_1
    const-string v4, "SetBackupAccountService"

    const-string v5, "No BackupTransportService; setting account directly"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-virtual {p0, v0}, Lcom/google/android/backup/SetBackupAccountService;->saveBackupAccount(Landroid/accounts/Account;)V

    goto :goto_0
.end method
