.class Lcom/google/android/backup/BackupTransportService$2;
.super Landroid/content/BroadcastReceiver;
.source "BackupTransportService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/backup/BackupTransportService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/backup/BackupTransportService;


# direct methods
.method constructor <init>(Lcom/google/android/backup/BackupTransportService;)V
    .locals 0

    .prologue
    .line 1272
    iput-object p1, p0, Lcom/google/android/backup/BackupTransportService$2;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v12, 0x0

    .line 1279
    :try_start_0
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$2;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v8}, Lcom/google/android/backup/BackupTransportService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    new-instance v9, Landroid/content/ComponentName;

    const-string v10, "com.google.android.backuptransport"

    const-string v11, "com.google.android.backup.BackupTransportService"

    invoke-direct {v9, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;

    move-result-object v2

    .line 1282
    .local v2, "info":Landroid/content/pm/ServiceInfo;
    invoke-virtual {v2}, Landroid/content/pm/ServiceInfo;->isEnabled()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 1289
    .local v3, "isEnabled":Z
    if-nez v3, :cond_1

    .line 1336
    .end local v2    # "info":Landroid/content/pm/ServiceInfo;
    .end local v3    # "isEnabled":Z
    :cond_0
    :goto_0
    return-void

    .line 1283
    :catch_0
    move-exception v1

    .line 1287
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 1296
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "info":Landroid/content/pm/ServiceInfo;
    .restart local v3    # "isEnabled":Z
    :cond_1
    const-string v8, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1298
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$2;->this$0:Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v8}, Lcom/google/android/backup/BackupTransportService;->clearMoratoriums()V

    .line 1299
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$2;->this$0:Lcom/google/android/backup/BackupTransportService;

    # invokes: Lcom/google/android/backup/BackupTransportService;->validateAndGetBackupAccount()Landroid/accounts/Account;
    invoke-static {v8}, Lcom/google/android/backup/BackupTransportService;->access$000(Lcom/google/android/backup/BackupTransportService;)Landroid/accounts/Account;

    .line 1305
    :cond_2
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$2;->this$0:Lcom/google/android/backup/BackupTransportService;

    const-string v9, "connectivity"

    invoke-virtual {v8, v9}, Lcom/google/android/backup/BackupTransportService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1306
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v8

    if-nez v8, :cond_4

    .line 1308
    :cond_3
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$2;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v8}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v8

    invoke-virtual {v8, v12}, Lcom/android/common/OperationScheduler;->setEnabledState(Z)V

    .line 1323
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1324
    .local v6, "now":J
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$2;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v8}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/backup/BackupTransportService$2;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mSchedulerOptions:Lcom/android/common/OperationScheduler$Options;
    invoke-static {v9}, Lcom/google/android/backup/BackupTransportService;->access$200(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler$Options;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v8

    cmp-long v8, v8, v6

    if-gtz v8, :cond_0

    .line 1327
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$2;->this$0:Lcom/google/android/backup/BackupTransportService;

    # invokes: Lcom/google/android/backup/BackupTransportService;->getBackupManager()Landroid/app/backup/BackupManager;
    invoke-static {v8}, Lcom/google/android/backup/BackupTransportService;->access$300(Lcom/google/android/backup/BackupTransportService;)Landroid/app/backup/BackupManager;

    move-result-object v4

    .line 1328
    .local v4, "manager":Landroid/app/backup/BackupManager;
    if-nez v4, :cond_7

    .line 1329
    const-string v8, "BackupTransportService"

    const-string v9, "No BackupManager service available"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1309
    .end local v4    # "manager":Landroid/app/backup/BackupManager;
    .end local v6    # "now":J
    :cond_4
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v8

    if-nez v8, :cond_5

    .line 1311
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$2;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v8}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v8

    invoke-virtual {v8, v12}, Lcom/android/common/OperationScheduler;->setEnabledState(Z)V

    goto :goto_1

    .line 1313
    :cond_5
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v5

    .line 1314
    .local v5, "ni":Landroid/net/NetworkInfo;
    if-eqz v5, :cond_6

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1316
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$2;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v8}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/android/common/OperationScheduler;->setEnabledState(Z)V

    goto :goto_1

    .line 1319
    :cond_6
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$2;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v8}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v8

    invoke-virtual {v8, v12}, Lcom/android/common/OperationScheduler;->setEnabledState(Z)V

    goto :goto_1

    .line 1332
    .end local v5    # "ni":Landroid/net/NetworkInfo;
    .restart local v4    # "manager":Landroid/app/backup/BackupManager;
    .restart local v6    # "now":J
    :cond_7
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService$2;->this$0:Lcom/google/android/backup/BackupTransportService;

    # getter for: Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;
    invoke-static {v8}, Lcom/google/android/backup/BackupTransportService;->access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;

    move-result-object v8

    const-wide v10, 0x7fffffffffffffffL

    invoke-virtual {v8, v10, v11}, Lcom/android/common/OperationScheduler;->setTriggerTimeMillis(J)V

    .line 1333
    invoke-virtual {v4}, Landroid/app/backup/BackupManager;->backupNow()V

    goto/16 :goto_0
.end method
