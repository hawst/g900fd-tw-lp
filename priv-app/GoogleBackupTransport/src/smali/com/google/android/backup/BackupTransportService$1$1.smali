.class Lcom/google/android/backup/BackupTransportService$1$1;
.super Ljava/lang/Object;
.source "BackupTransportService.java"

# interfaces
.implements Lcom/google/uploader/client/TransferListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/backup/BackupTransportService$1;->performFullBackup(Landroid/content/pm/PackageInfo;Landroid/os/ParcelFileDescriptor;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/backup/BackupTransportService$1;

.field final synthetic val$authToken:Ljava/lang/String;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/backup/BackupTransportService$1;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 772
    iput-object p1, p0, Lcom/google/android/backup/BackupTransportService$1$1;->this$1:Lcom/google/android/backup/BackupTransportService$1;

    iput-object p2, p0, Lcom/google/android/backup/BackupTransportService$1$1;->val$packageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/backup/BackupTransportService$1$1;->val$authToken:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onException(Lcom/google/uploader/client/Transfer;Lcom/google/uploader/client/TransferException;)V
    .locals 5
    .param p1, "transfer"    # Lcom/google/uploader/client/Transfer;
    .param p2, "exception"    # Lcom/google/uploader/client/TransferException;

    .prologue
    .line 810
    const-string v0, "BackupTransportService"

    const-string v1, "Scotty transfer exception."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    const v0, 0x32898

    const-string v1, "Scotty transfer exception for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService$1$1;->val$packageName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 814
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1$1;->this$1:Lcom/google/android/backup/BackupTransportService$1;

    # getter for: Lcom/google/android/backup/BackupTransportService$1;->fullBackupDataStream:Lcom/google/android/backup/BlockingFileDataStream;
    invoke-static {v0}, Lcom/google/android/backup/BackupTransportService$1;->access$1400(Lcom/google/android/backup/BackupTransportService$1;)Lcom/google/android/backup/BlockingFileDataStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/backup/BlockingFileDataStream;->signalTransferException()V

    .line 816
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1$1;->this$1:Lcom/google/android/backup/BackupTransportService$1;

    # getter for: Lcom/google/android/backup/BackupTransportService$1;->fullBackupCompleteLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/backup/BackupTransportService$1;->access$1300(Lcom/google/android/backup/BackupTransportService$1;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 817
    :try_start_0
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1$1;->this$1:Lcom/google/android/backup/BackupTransportService$1;

    # getter for: Lcom/google/android/backup/BackupTransportService$1;->fullBackupStatusCode:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/backup/BackupTransportService$1;->access$1100(Lcom/google/android/backup/BackupTransportService$1;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 818
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1$1;->this$1:Lcom/google/android/backup/BackupTransportService$1;

    # getter for: Lcom/google/android/backup/BackupTransportService$1;->fullBackupCompleteLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/backup/BackupTransportService$1;->access$1300(Lcom/google/android/backup/BackupTransportService$1;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 819
    monitor-exit v1

    .line 820
    return-void

    .line 819
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onResponseReceived(Lcom/google/uploader/client/Transfer;Lcom/google/uploader/client/HttpResponse;)V
    .locals 5
    .param p1, "transfer"    # Lcom/google/uploader/client/Transfer;
    .param p2, "response"    # Lcom/google/uploader/client/HttpResponse;

    .prologue
    .line 790
    const-string v0, "BackupTransportService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Http response from scotty : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/uploader/client/HttpResponse;->getResponseCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    const v0, 0x32898

    const-string v1, "Scotty response for %s : %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService$1$1;->val$packageName:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/google/uploader/client/HttpResponse;->getResponseCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 795
    invoke-virtual {p2}, Lcom/google/uploader/client/HttpResponse;->getResponseCode()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_0

    .line 797
    :try_start_0
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1$1;->this$1:Lcom/google/android/backup/BackupTransportService$1;

    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService$1$1;->val$authToken:Ljava/lang/String;

    # invokes: Lcom/google/android/backup/BackupTransportService$1;->invalidAuthToken(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/backup/BackupTransportService$1;->access$1200(Lcom/google/android/backup/BackupTransportService$1;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 802
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1$1;->this$1:Lcom/google/android/backup/BackupTransportService$1;

    # getter for: Lcom/google/android/backup/BackupTransportService$1;->fullBackupCompleteLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/backup/BackupTransportService$1;->access$1300(Lcom/google/android/backup/BackupTransportService$1;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 803
    :try_start_1
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1$1;->this$1:Lcom/google/android/backup/BackupTransportService$1;

    # getter for: Lcom/google/android/backup/BackupTransportService$1;->fullBackupStatusCode:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/backup/BackupTransportService$1;->access$1100(Lcom/google/android/backup/BackupTransportService$1;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/uploader/client/HttpResponse;->getResponseCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 804
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1$1;->this$1:Lcom/google/android/backup/BackupTransportService$1;

    # getter for: Lcom/google/android/backup/BackupTransportService$1;->fullBackupCompleteLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/backup/BackupTransportService$1;->access$1300(Lcom/google/android/backup/BackupTransportService$1;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 805
    monitor-exit v1

    .line 806
    return-void

    .line 805
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 798
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onStart(Lcom/google/uploader/client/Transfer;)V
    .locals 5
    .param p1, "transfer"    # Lcom/google/uploader/client/Transfer;

    .prologue
    const/4 v4, 0x0

    .line 780
    const-string v0, "BackupTransportService"

    const-string v1, "Start scotty uploading."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 781
    const v0, 0x32898

    const-string v1, "Start upload full backup data for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService$1$1;->val$packageName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 784
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService$1$1;->this$1:Lcom/google/android/backup/BackupTransportService$1;

    # getter for: Lcom/google/android/backup/BackupTransportService$1;->fullBackupStatusCode:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/backup/BackupTransportService$1;->access$1100(Lcom/google/android/backup/BackupTransportService$1;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 785
    return-void
.end method

.method public onUploadProgress(Lcom/google/uploader/client/Transfer;)V
    .locals 4
    .param p1, "transfer"    # Lcom/google/uploader/client/Transfer;

    .prologue
    .line 775
    const-string v0, "BackupTransportService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bytes uploaded : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/uploader/client/Transfer;->getBytesUploaded()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    return-void
.end method
