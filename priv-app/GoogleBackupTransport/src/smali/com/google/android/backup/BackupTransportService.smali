.class public Lcom/google/android/backup/BackupTransportService;
.super Landroid/app/Service;
.source "BackupTransportService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/backup/BackupTransportService$MissingAccountsException;
    }
.end annotation


# static fields
.field private static sTransportService:Lcom/google/android/backup/BackupTransportService;


# instance fields
.field private mAbortScheduler:Lcom/android/common/OperationScheduler;

.field private mActiveUid:I

.field private mBackupAccountPreferences:Landroid/content/SharedPreferences;

.field private mBackupManager:Landroid/app/backup/BackupManager;

.field private mBackupScheduler:Lcom/android/common/OperationScheduler;

.field private final mBackupTransport:Landroid/app/backup/BackupTransport;

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mConnManager:Landroid/net/ConnectivityManager;

.field private mHttpClient:Lcom/google/android/common/http/GoogleHttpClient;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mObserverHandle:Ljava/lang/Object;

.field private mRestoreScheduler:Lcom/android/common/OperationScheduler;

.field private mSchedulerOptions:Lcom/android/common/OperationScheduler$Options;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/backup/BackupTransportService;->sTransportService:Lcom/google/android/backup/BackupTransportService;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 105
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 186
    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mConnManager:Landroid/net/ConnectivityManager;

    .line 187
    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mHttpClient:Lcom/google/android/common/http/GoogleHttpClient;

    .line 188
    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mSchedulerOptions:Lcom/android/common/OperationScheduler$Options;

    .line 189
    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;

    .line 190
    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mRestoreScheduler:Lcom/android/common/OperationScheduler;

    .line 191
    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mAbortScheduler:Lcom/android/common/OperationScheduler;

    .line 192
    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mObserverHandle:Ljava/lang/Object;

    .line 193
    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBackupManager:Landroid/app/backup/BackupManager;

    .line 194
    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 195
    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBackupAccountPreferences:Landroid/content/SharedPreferences;

    .line 197
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/backup/BackupTransportService;->mActiveUid:I

    .line 287
    new-instance v0, Lcom/google/android/backup/BackupTransportService$1;

    invoke-direct {v0, p0}, Lcom/google/android/backup/BackupTransportService$1;-><init>(Lcom/google/android/backup/BackupTransportService;)V

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBackupTransport:Landroid/app/backup/BackupTransport;

    .line 1272
    new-instance v0, Lcom/google/android/backup/BackupTransportService$2;

    invoke-direct {v0, p0}, Lcom/google/android/backup/BackupTransportService$2;-><init>(Lcom/google/android/backup/BackupTransportService;)V

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/backup/BackupTransportService;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService;->validateAndGetBackupAccount()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/backup/BackupTransportService;Landroid/content/pm/PackageInfo;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService;
    .param p1, "x1"    # Landroid/content/pm/PackageInfo;
    .param p2, "x2"    # Lcom/android/common/OperationScheduler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/google/android/backup/BackupTransportService;->generateFullBackupRequest(Landroid/content/pm/PackageInfo;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/backup/BackupTransportService;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler$Options;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mSchedulerOptions:Lcom/android/common/OperationScheduler$Options;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/backup/BackupTransportService;)Landroid/app/backup/BackupManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService;->getBackupManager()Landroid/app/backup/BackupManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/backup/BackupTransportService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService;->cancelSetBackupAccountNotification()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/backup/BackupTransportService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService;

    .prologue
    .line 105
    iget v0, p0, Lcom/google/android/backup/BackupTransportService;->mActiveUid:I

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/backup/BackupTransportService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService;
    .param p1, "x1"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/google/android/backup/BackupTransportService;->mActiveUid:I

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/backup/BackupTransportService;Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService;
    .param p1, "x1"    # Lcom/google/common/io/protocol/ProtoBuf;
    .param p2, "x2"    # Lcom/android/common/OperationScheduler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/google/android/backup/BackupTransportService;->sendRequestLocked(Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mAbortScheduler:Lcom/android/common/OperationScheduler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/backup/BackupTransportService;)Lcom/android/common/OperationScheduler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mRestoreScheduler:Lcom/android/common/OperationScheduler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/backup/BackupTransportService;)Landroid/net/ConnectivityManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/backup/BackupTransportService;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mConnManager:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method private cancelSetBackupAccountNotification()V
    .locals 3

    .prologue
    .line 1259
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mNotificationManager:Landroid/app/NotificationManager;

    const-string v1, "com.google.android.backup.notification.tag"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 1263
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/backup/BackupTransportService;->setBackupAccountMissingNotified(Z)V

    .line 1264
    return-void
.end method

.method private generateFullBackupRequest(Landroid/content/pm/PackageInfo;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 24
    .param p1, "pkg"    # Landroid/content/pm/PackageInfo;
    .param p2, "scheduler"    # Lcom/android/common/OperationScheduler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1539
    new-instance v18, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v20, Lcom/google/android/backup/proto/BackupMessageTypes;->BACKUP_REQUEST:Lcom/google/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    .line 1540
    .local v18, "request":Lcom/google/common/io/protocol/ProtoBuf;
    const/16 v20, 0x13

    const/16 v21, 0x3

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    .line 1542
    const/16 v20, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->addNewProtoBuf(I)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v19

    .line 1543
    .local v19, "requestApp":Lcom/google/common/io/protocol/ProtoBuf;
    const/16 v20, 0x3

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    .line 1546
    :cond_0
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/backup/BackupTransportService;->validateAndGetBackupAccount()Landroid/accounts/Account;

    move-result-object v8

    .line 1557
    .local v8, "backupAccount":Landroid/accounts/Account;
    if-nez v8, :cond_1

    .line 1558
    :try_start_0
    const-string v20, "BackupTransportService"

    const-string v21, "Missing backup account"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1559
    new-instance v20, Lcom/google/android/backup/BackupTransportService$MissingAccountsException;

    const-string v21, "Backup account is not defined"

    invoke-direct/range {v20 .. v21}, Lcom/google/android/backup/BackupTransportService$MissingAccountsException;-><init>(Ljava/lang/String;)V

    throw v20
    :try_end_0
    .catch Lcom/google/android/backup/BackupTransportService$MissingAccountsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1580
    :catch_0
    move-exception v9

    .line 1581
    .local v9, "e":Lcom/google/android/backup/BackupTransportService$MissingAccountsException;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    const-wide/32 v22, 0x240c8400

    add-long v14, v20, v22

    .line 1582
    .local v14, "moratorium":J
    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Lcom/android/common/OperationScheduler;->setMoratoriumTimeMillis(J)V

    .line 1583
    const-string v20, "BackupTransportService"

    const-string v21, "Backup account missing, trying again later"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1584
    new-instance v12, Ljava/lang/Exception;

    const-string v20, "Can\'t get Backup account"

    move-object/from16 v0, v20

    invoke-direct {v12, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 1585
    .local v12, "err":Ljava/lang/Exception;
    invoke-virtual {v12, v9}, Ljava/lang/Exception;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1586
    throw v12

    .line 1562
    .end local v9    # "e":Lcom/google/android/backup/BackupTransportService$MissingAccountsException;
    .end local v12    # "err":Ljava/lang/Exception;
    .end local v14    # "moratorium":J
    :cond_1
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const-string v21, "android_id"

    const-wide/16 v22, 0x0

    invoke-static/range {v20 .. v23}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v4

    .line 1563
    .local v4, "aid":J
    const-wide/16 v20, 0x0

    cmp-long v20, v4, v20

    if-nez v20, :cond_2

    .line 1564
    const-string v20, "BackupTransportService"

    const-string v21, "Missing android ID"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1565
    new-instance v20, Landroid/accounts/AccountsException;

    const-string v21, "No Android ID available"

    invoke-direct/range {v20 .. v21}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v20
    :try_end_1
    .catch Lcom/google/android/backup/BackupTransportService$MissingAccountsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1587
    .end local v4    # "aid":J
    :catch_1
    move-exception v9

    .line 1588
    .local v9, "e":Landroid/accounts/AccountsException;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    const-wide/32 v22, 0x2932e00

    add-long v14, v20, v22

    .line 1589
    .restart local v14    # "moratorium":J
    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Lcom/android/common/OperationScheduler;->setMoratoriumTimeMillis(J)V

    .line 1590
    new-instance v12, Ljava/lang/Exception;

    const-string v20, "Can\'t get credentials"

    move-object/from16 v0, v20

    invoke-direct {v12, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 1591
    .restart local v12    # "err":Ljava/lang/Exception;
    invoke-virtual {v12, v9}, Ljava/lang/Exception;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1592
    throw v12

    .line 1567
    .end local v9    # "e":Landroid/accounts/AccountsException;
    .end local v12    # "err":Ljava/lang/Exception;
    .end local v14    # "moratorium":J
    .restart local v4    # "aid":J
    :cond_2
    const/16 v20, 0x1

    :try_start_2
    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    .line 1569
    invoke-static/range {p0 .. p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    .line 1571
    .local v6, "am":Landroid/accounts/AccountManager;
    const-string v20, "android"

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v8, v0, v1}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 1573
    .local v7, "authToken":Ljava/lang/String;
    if-nez v7, :cond_3

    .line 1574
    const-string v20, "BackupTransportService"

    const-string v21, "Fail to get auth token"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1575
    new-instance v20, Landroid/accounts/AccountsException;

    const-string v21, "No auth token available"

    invoke-direct/range {v20 .. v21}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v20
    :try_end_2
    .catch Lcom/google/android/backup/BackupTransportService$MissingAccountsException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1593
    .end local v4    # "aid":J
    .end local v6    # "am":Landroid/accounts/AccountManager;
    .end local v7    # "authToken":Ljava/lang/String;
    :catch_2
    move-exception v9

    .line 1594
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual/range {p2 .. p2}, Lcom/android/common/OperationScheduler;->onTransientError()V

    .line 1595
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/backup/BackupTransportService;->mSchedulerOptions:Lcom/android/common/OperationScheduler$Options;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v16

    .line 1596
    .local v16, "next":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v10, v16, v20

    .line 1597
    .local v10, "delay":J
    const-wide/16 v20, 0x7530

    cmp-long v20, v10, v20

    if-lez v20, :cond_4

    .line 1598
    throw v9

    .line 1577
    .end local v9    # "e":Ljava/io/IOException;
    .end local v10    # "delay":J
    .end local v16    # "next":J
    .restart local v4    # "aid":J
    .restart local v6    # "am":Landroid/accounts/AccountManager;
    .restart local v7    # "authToken":Ljava/lang/String;
    :cond_3
    const/16 v20, 0xe

    :try_start_3
    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1, v7}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V
    :try_end_3
    .catch Lcom/google/android/backup/BackupTransportService$MissingAccountsException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 1579
    return-object v18

    .line 1600
    .end local v4    # "aid":J
    .end local v6    # "am":Landroid/accounts/AccountManager;
    .end local v7    # "authToken":Ljava/lang/String;
    .restart local v9    # "e":Ljava/io/IOException;
    .restart local v10    # "delay":J
    .restart local v16    # "next":J
    :cond_4
    const-string v20, "BackupTransportService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Network error - waiting "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "ms to retry: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1603
    const-wide/16 v20, 0x0

    cmp-long v20, v10, v20

    if-lez v20, :cond_0

    .line 1604
    :try_start_4
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    .line 1606
    :catch_3
    move-exception v13

    .line 1607
    .local v13, "ie":Ljava/lang/InterruptedException;
    new-instance v20, Ljava/lang/IllegalStateException;

    move-object/from16 v0, v20

    invoke-direct {v0, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v20
.end method

.method private getAccounts()[Landroid/accounts/Account;
    .locals 3

    .prologue
    .line 232
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 233
    .local v1, "am":Landroid/accounts/AccountManager;
    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 235
    .local v0, "accounts":[Landroid/accounts/Account;
    return-object v0
.end method

.method private declared-synchronized getBackupManager()Landroid/app/backup/BackupManager;
    .locals 1

    .prologue
    .line 281
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBackupManager:Landroid/app/backup/BackupManager;

    if-nez v0, :cond_0

    .line 282
    new-instance v0, Landroid/app/backup/BackupManager;

    invoke-direct {v0, p0}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBackupManager:Landroid/app/backup/BackupManager;

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBackupManager:Landroid/app/backup/BackupManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 281
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static getTransportInstance()Lcom/google/android/backup/BackupTransportService;
    .locals 1

    .prologue
    .line 218
    sget-object v0, Lcom/google/android/backup/BackupTransportService;->sTransportService:Lcom/google/android/backup/BackupTransportService;

    return-object v0
.end method

.method private isBackupAccountMissingNotified()Z
    .locals 3

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBackupAccountPreferences:Landroid/content/SharedPreferences;

    const-string v1, "backupAccountMissingNotified"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private newBackupAccountNotification()Landroid/app/Notification;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1201
    iget-object v3, p0, Lcom/google/android/backup/BackupTransportService;->mBackupTransport:Landroid/app/backup/BackupTransport;

    invoke-virtual {v3}, Landroid/app/backup/BackupTransport;->configurationIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1202
    .local v0, "intent":Landroid/content/Intent;
    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {p0, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 1206
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    .line 1207
    .local v1, "notification":Landroid/app/Notification;
    iget v3, v1, Landroid/app/Notification;->flags:I

    or-int/lit16 v3, v3, 0x80

    iput v3, v1, Landroid/app/Notification;->flags:I

    .line 1208
    const v3, 0x108008a

    iput v3, v1, Landroid/app/Notification;->icon:I

    .line 1209
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v1, Landroid/app/Notification;->when:J

    .line 1210
    invoke-virtual {p0}, Lcom/google/android/backup/BackupTransportService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f040002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 1212
    invoke-virtual {p0}, Lcom/google/android/backup/BackupTransportService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f040003

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/backup/BackupTransportService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f040004

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, p0, v3, v4, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 1216
    return-object v1
.end method

.method private sendRequestLocked(Lcom/google/common/io/protocol/ProtoBuf;Lcom/android/common/OperationScheduler;)Lcom/google/common/io/protocol/ProtoBuf;
    .locals 30
    .param p1, "request"    # Lcom/google/common/io/protocol/ProtoBuf;
    .param p2, "scheduler"    # Lcom/android/common/OperationScheduler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/backup/BackupTransportService;->mBackupTransport:Landroid/app/backup/BackupTransport;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_0

    new-instance v25, Ljava/lang/IllegalStateException;

    const-string v26, "lock not held"

    invoke-direct/range {v25 .. v26}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 1416
    :cond_0
    invoke-static/range {p0 .. p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    .line 1417
    .local v6, "am":Landroid/accounts/AccountManager;
    const/4 v15, 0x0

    .line 1420
    .local v15, "hadOneAuthError":Z
    :cond_1
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/backup/BackupTransportService;->validateAndGetBackupAccount()Landroid/accounts/Account;

    move-result-object v8

    .line 1423
    .local v8, "backupAccount":Landroid/accounts/Account;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/backup/BackupTransportService;->mSchedulerOptions:Lcom/android/common/OperationScheduler$Options;

    move-object/from16 v25, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v26

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    cmp-long v25, v26, v28

    if-lez v25, :cond_2

    .line 1424
    new-instance v25, Ljava/io/IOException;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Not ready to send network request: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 1428
    :cond_2
    if-nez v8, :cond_3

    .line 1429
    :try_start_0
    new-instance v25, Lcom/google/android/backup/BackupTransportService$MissingAccountsException;

    const-string v26, "Backup account is not defined"

    invoke-direct/range {v25 .. v26}, Lcom/google/android/backup/BackupTransportService$MissingAccountsException;-><init>(Ljava/lang/String;)V

    throw v25
    :try_end_0
    .catch Lcom/google/android/backup/BackupTransportService$MissingAccountsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/backup/BackupRequestGenerator$AuthException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/backup/BackupRequestGenerator$PolicyException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5

    .line 1480
    :catch_0
    move-exception v12

    .line 1481
    .local v12, "e":Lcom/google/android/backup/BackupTransportService$MissingAccountsException;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    const-wide/32 v28, 0x240c8400

    add-long v18, v26, v28

    .line 1482
    .local v18, "moratorium":J
    move-object/from16 v0, p2

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/android/common/OperationScheduler;->setMoratoriumTimeMillis(J)V

    .line 1483
    const-string v25, "BackupTransportService"

    const-string v26, "Backup account missing, trying again later"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1484
    new-instance v14, Ljava/io/IOException;

    const-string v25, "Can\'t get Backup account"

    move-object/from16 v0, v25

    invoke-direct {v14, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 1485
    .local v14, "err":Ljava/io/IOException;
    invoke-virtual {v14, v12}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1486
    throw v14

    .line 1431
    .end local v12    # "e":Lcom/google/android/backup/BackupTransportService$MissingAccountsException;
    .end local v14    # "err":Ljava/io/IOException;
    .end local v18    # "moratorium":J
    :cond_3
    const/16 v25, 0xe

    :try_start_1
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v25

    if-nez v25, :cond_5

    .line 1433
    const-string v25, "android"

    const/16 v26, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v6, v8, v0, v1}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 1435
    .local v7, "authToken":Ljava/lang/String;
    if-nez v7, :cond_4

    new-instance v25, Landroid/accounts/AccountsException;

    const-string v26, "No auth token available"

    invoke-direct/range {v25 .. v26}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v25
    :try_end_1
    .catch Lcom/google/android/backup/BackupTransportService$MissingAccountsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/backup/BackupRequestGenerator$AuthException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/google/android/backup/BackupRequestGenerator$PolicyException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 1487
    .end local v7    # "authToken":Ljava/lang/String;
    :catch_1
    move-exception v12

    .line 1488
    .local v12, "e":Landroid/accounts/AccountsException;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    const-wide/32 v28, 0x2932e00

    add-long v18, v26, v28

    .line 1489
    .restart local v18    # "moratorium":J
    move-object/from16 v0, p2

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/android/common/OperationScheduler;->setMoratoriumTimeMillis(J)V

    .line 1490
    new-instance v14, Ljava/io/IOException;

    const-string v25, "Can\'t get credentials"

    move-object/from16 v0, v25

    invoke-direct {v14, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 1491
    .restart local v14    # "err":Ljava/io/IOException;
    invoke-virtual {v14, v12}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1492
    throw v14

    .line 1436
    .end local v12    # "e":Landroid/accounts/AccountsException;
    .end local v14    # "err":Ljava/io/IOException;
    .end local v18    # "moratorium":J
    .restart local v7    # "authToken":Ljava/lang/String;
    :cond_4
    const/16 v25, 0xe

    :try_start_2
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1, v7}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    .line 1439
    .end local v7    # "authToken":Ljava/lang/String;
    :cond_5
    const/16 v25, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v25

    if-nez v25, :cond_8

    .line 1440
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v25

    const-string v26, "android_id"

    const-wide/16 v28, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-wide/from16 v2, v28

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v4

    .line 1441
    .local v4, "aid":J
    const-wide/16 v26, 0x0

    cmp-long v25, v4, v26

    if-nez v25, :cond_7

    new-instance v25, Landroid/accounts/AccountsException;

    const-string v26, "No Android ID available"

    invoke-direct/range {v25 .. v26}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v25
    :try_end_2
    .catch Lcom/google/android/backup/BackupTransportService$MissingAccountsException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/backup/BackupRequestGenerator$AuthException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/android/backup/BackupRequestGenerator$PolicyException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 1493
    .end local v4    # "aid":J
    :catch_2
    move-exception v12

    .line 1494
    .local v12, "e":Lcom/google/android/backup/BackupRequestGenerator$AuthException;
    const/16 v25, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 1495
    const/16 v25, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1496
    .restart local v7    # "authToken":Ljava/lang/String;
    const-string v25, "com.google"

    move-object/from16 v0, v25

    invoke-virtual {v6, v0, v7}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 1497
    const/16 v25, 0xe

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/google/common/io/protocol/ProtoBuf;->remove(II)V

    .line 1500
    .end local v7    # "authToken":Ljava/lang/String;
    :cond_6
    if-eqz v15, :cond_e

    .line 1501
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    const-wide/32 v28, 0x2932e00

    add-long v18, v26, v28

    .line 1502
    .restart local v18    # "moratorium":J
    move-object/from16 v0, p2

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/android/common/OperationScheduler;->setMoratoriumTimeMillis(J)V

    .line 1503
    new-instance v14, Ljava/io/IOException;

    const-string v25, "Repeated authentication failure"

    move-object/from16 v0, v25

    invoke-direct {v14, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 1504
    .restart local v14    # "err":Ljava/io/IOException;
    invoke-virtual {v14, v12}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1505
    throw v14

    .line 1442
    .end local v12    # "e":Lcom/google/android/backup/BackupRequestGenerator$AuthException;
    .end local v14    # "err":Ljava/io/IOException;
    .end local v18    # "moratorium":J
    .restart local v4    # "aid":J
    :cond_7
    const/16 v25, 0x1

    :try_start_3
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    .line 1445
    .end local v4    # "aid":J
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v9

    .line 1449
    .local v9, "data":[B
    new-instance v22, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v25

    const-string v26, "backup_server_url"

    const-string v27, "https://android.googleapis.com/backup"

    invoke-static/range {v25 .. v27}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 1452
    .local v22, "post":Lorg/apache/http/client/methods/HttpPost;
    const-string v25, "Content-Type"

    const-string v26, "application/octet-stream"

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1453
    new-instance v25, Lorg/apache/http/entity/ByteArrayEntity;

    move-object/from16 v0, v25

    invoke-direct {v0, v9}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/backup/BackupTransportService;->mHttpClient:Lcom/google/android/common/http/GoogleHttpClient;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/common/http/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v23

    .line 1456
    .local v23, "response":Lorg/apache/http/HttpResponse;
    invoke-interface/range {v23 .. v23}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v13

    .line 1457
    .local v13, "entity":Lorg/apache/http/HttpEntity;
    invoke-interface/range {v23 .. v23}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v25

    const/16 v26, 0x1f7

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_9

    .line 1458
    const-string v25, "Retry-After"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v16

    .line 1459
    .local v16, "header":Lorg/apache/http/Header;
    if-eqz v16, :cond_9

    .line 1460
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/common/OperationScheduler;->setMoratoriumTimeHttp(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 1471
    .end local v16    # "header":Lorg/apache/http/Header;
    :cond_9
    :goto_1
    invoke-interface/range {v23 .. v23}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v25

    const/16 v26, 0xc8

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_c

    .line 1472
    if-eqz v13, :cond_a

    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 1473
    :cond_a
    new-instance v25, Ljava/io/IOException;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Server rejected backup: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-interface/range {v23 .. v23}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v25
    :try_end_3
    .catch Lcom/google/android/backup/BackupTransportService$MissingAccountsException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/backup/BackupRequestGenerator$AuthException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/google/android/backup/BackupRequestGenerator$PolicyException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 1510
    .end local v9    # "data":[B
    .end local v13    # "entity":Lorg/apache/http/HttpEntity;
    .end local v22    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v23    # "response":Lorg/apache/http/HttpResponse;
    :catch_3
    move-exception v12

    .line 1511
    .local v12, "e":Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException;
    throw v12

    .line 1463
    .end local v12    # "e":Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException;
    .restart local v9    # "data":[B
    .restart local v13    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v16    # "header":Lorg/apache/http/Header;
    .restart local v22    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v23    # "response":Lorg/apache/http/HttpResponse;
    :cond_b
    :try_start_4
    const-string v25, "BackupTransportService"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Invalid Retry-After date: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Lcom/google/android/backup/BackupTransportService$MissingAccountsException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/google/android/backup/BackupRequestGenerator$AuthException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/google/android/backup/BackupRequestGenerator$PolicyException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    goto :goto_1

    .line 1512
    .end local v9    # "data":[B
    .end local v13    # "entity":Lorg/apache/http/HttpEntity;
    .end local v16    # "header":Lorg/apache/http/Header;
    .end local v22    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v23    # "response":Lorg/apache/http/HttpResponse;
    :catch_4
    move-exception v12

    .line 1513
    .local v12, "e":Lcom/google/android/backup/BackupRequestGenerator$PolicyException;
    throw v12

    .line 1476
    .end local v12    # "e":Lcom/google/android/backup/BackupRequestGenerator$PolicyException;
    .restart local v9    # "data":[B
    .restart local v13    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v22    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v23    # "response":Lorg/apache/http/HttpResponse;
    :cond_c
    if-nez v13, :cond_d

    :try_start_5
    new-instance v25, Ljava/io/IOException;

    const-string v26, "Missing response body"

    invoke-direct/range {v25 .. v26}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v25
    :try_end_5
    .catch Lcom/google/android/backup/BackupTransportService$MissingAccountsException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/google/android/backup/BackupRequestGenerator$AuthException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/google/android/backup/BackupRequestGenerator$PolicyException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 1514
    .end local v9    # "data":[B
    .end local v13    # "entity":Lorg/apache/http/HttpEntity;
    .end local v22    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v23    # "response":Lorg/apache/http/HttpResponse;
    :catch_5
    move-exception v12

    .line 1515
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual/range {p2 .. p2}, Lcom/android/common/OperationScheduler;->onTransientError()V

    .line 1516
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/backup/BackupTransportService;->mSchedulerOptions:Lcom/android/common/OperationScheduler$Options;

    move-object/from16 v25, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/common/OperationScheduler;->getNextTimeMillis(Lcom/android/common/OperationScheduler$Options;)J

    move-result-wide v20

    .line 1517
    .local v20, "next":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    sub-long v10, v20, v26

    .line 1518
    .local v10, "delay":J
    const-wide/16 v26, 0x7530

    cmp-long v25, v10, v26

    if-lez v25, :cond_f

    throw v12

    .line 1477
    .end local v10    # "delay":J
    .end local v12    # "e":Ljava/io/IOException;
    .end local v20    # "next":J
    .restart local v9    # "data":[B
    .restart local v13    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v22    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v23    # "response":Lorg/apache/http/HttpResponse;
    :cond_d
    :try_start_6
    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/google/android/backup/BackupRequestGenerator;->parseResponse(Ljava/io/InputStream;)Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v24

    .line 1478
    .local v24, "result":Lcom/google/common/io/protocol/ProtoBuf;
    invoke-virtual/range {p2 .. p2}, Lcom/android/common/OperationScheduler;->onSuccess()V
    :try_end_6
    .catch Lcom/google/android/backup/BackupTransportService$MissingAccountsException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/google/android/backup/BackupRequestGenerator$AuthException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcom/google/android/backup/BackupRequestGenerator$UninitializedDeviceException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Lcom/google/android/backup/BackupRequestGenerator$PolicyException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 1479
    return-object v24

    .line 1508
    .end local v9    # "data":[B
    .end local v13    # "entity":Lorg/apache/http/HttpEntity;
    .end local v22    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v23    # "response":Lorg/apache/http/HttpResponse;
    .end local v24    # "result":Lcom/google/common/io/protocol/ProtoBuf;
    .local v12, "e":Lcom/google/android/backup/BackupRequestGenerator$AuthException;
    :cond_e
    const/4 v15, 0x1

    .line 1509
    const-string v25, "BackupTransportService"

    const-string v26, "Authentication error, trying again after invalidating auth token"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1519
    .restart local v10    # "delay":J
    .local v12, "e":Ljava/io/IOException;
    .restart local v20    # "next":J
    :cond_f
    const-string v25, "BackupTransportService"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Network error - waiting "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "ms to retry: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1522
    const-wide/16 v26, 0x0

    cmp-long v25, v10, v26

    if-lez v25, :cond_1

    :try_start_7
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_0

    .line 1523
    :catch_6
    move-exception v17

    .line 1524
    .local v17, "ie":Ljava/lang/InterruptedException;
    new-instance v25, Ljava/lang/IllegalStateException;

    move-object/from16 v0, v25

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v25
.end method

.method private sendSetBackupAccountNotificationIfNeeded()V
    .locals 7

    .prologue
    .line 1220
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/UserHandle;->isOwner()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1253
    :cond_0
    :goto_0
    return-void

    .line 1224
    :cond_1
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService;->getBackupManager()Landroid/app/backup/BackupManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/backup/BackupManager;->isBackupEnabled()Z

    move-result v2

    .line 1233
    .local v2, "isBackupEnabled":Z
    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService;->mBackupAccountPreferences:Landroid/content/SharedPreferences;

    const-string v5, "accountExpected"

    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1235
    .local v0, "accountExpected":Z
    if-eqz v2, :cond_2

    if-nez v0, :cond_3

    .line 1236
    :cond_2
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService;->cancelSetBackupAccountNotification()V

    goto :goto_0

    .line 1239
    :cond_3
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService;->isBackupAccountMissingNotified()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1244
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService;->newBackupAccountNotification()Landroid/app/Notification;

    move-result-object v3

    .line 1245
    .local v3, "notification":Landroid/app/Notification;
    iget-object v4, p0, Lcom/google/android/backup/BackupTransportService;->mNotificationManager:Landroid/app/NotificationManager;

    const-string v5, "com.google.android.backup.notification.tag"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1248
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/google/android/backup/BackupTransportService;->setBackupAccountMissingNotified(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1249
    .end local v3    # "notification":Landroid/app/Notification;
    :catch_0
    move-exception v1

    .line 1250
    .local v1, "e":Landroid/os/RemoteException;
    const-string v4, "BackupTransportService"

    const-string v5, "Cannot send notification for setting Backup Account due to RemoteException"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private setBackupAccountMissingNotified(Z)V
    .locals 2
    .param p1, "b"    # Z

    .prologue
    .line 222
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService;->mBackupAccountPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 223
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "backupAccountMissingNotified"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 224
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 225
    return-void
.end method

.method private declared-synchronized validateAndGetBackupAccount()Landroid/accounts/Account;
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 239
    monitor-enter p0

    :try_start_0
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService;->mBackupAccountPreferences:Landroid/content/SharedPreferences;

    const-string v9, "accountType"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 240
    .local v2, "accountType":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService;->mBackupAccountPreferences:Landroid/content/SharedPreferences;

    const-string v9, "accountName"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 241
    .local v1, "accountName":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 242
    :cond_0
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService;->sendSetBackupAccountNotificationIfNeeded()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v7

    .line 260
    :goto_0
    monitor-exit p0

    return-object v0

    .line 249
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService;->getAccounts()[Landroid/accounts/Account;

    move-result-object v3

    .local v3, "arr$":[Landroid/accounts/Account;
    array-length v6, v3

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_3

    aget-object v0, v3, v5

    .line 250
    .local v0, "account":Landroid/accounts/Account;
    iget-object v8, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 251
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService;->cancelSetBackupAccountNotification()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 239
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountName":Ljava/lang/String;
    .end local v2    # "accountType":Ljava/lang/String;
    .end local v3    # "arr$":[Landroid/accounts/Account;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 249
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accountName":Ljava/lang/String;
    .restart local v2    # "accountType":Ljava/lang/String;
    .restart local v3    # "arr$":[Landroid/accounts/Account;
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 255
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_3
    :try_start_2
    iget-object v8, p0, Lcom/google/android/backup/BackupTransportService;->mBackupAccountPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 256
    .local v4, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v8, "accountName"

    invoke-interface {v4, v8}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 257
    const-string v8, "accountType"

    invoke-interface {v4, v8}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 258
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 259
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService;->sendSetBackupAccountNotificationIfNeeded()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v7

    .line 260
    goto :goto_0
.end method


# virtual methods
.method clearMoratoriums()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1268
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;

    invoke-virtual {v0, v2, v3}, Lcom/android/common/OperationScheduler;->setMoratoriumTimeMillis(J)V

    .line 1269
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mRestoreScheduler:Lcom/android/common/OperationScheduler;

    invoke-virtual {v0, v2, v3}, Lcom/android/common/OperationScheduler;->setMoratoriumTimeMillis(J)V

    .line 1270
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1341
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBackupTransport:Landroid/app/backup/BackupTransport;

    invoke-virtual {v0}, Landroid/app/backup/BackupTransport;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1349
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/backup/BackupTransportService;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1350
    sget-object v0, Lcom/google/android/backup/BackupTransportService;->sTransportService:Lcom/google/android/backup/BackupTransportService;

    if-eqz v0, :cond_0

    .line 1351
    const-string v0, "BackupTransportService"

    const-string v1, "Service instance recreated!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1353
    :cond_0
    sput-object p0, Lcom/google/android/backup/BackupTransportService;->sTransportService:Lcom/google/android/backup/BackupTransportService;

    .line 1357
    :cond_1
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/android/backup/BackupTransportService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mConnManager:Landroid/net/ConnectivityManager;

    .line 1359
    new-instance v0, Lcom/google/android/common/http/GoogleHttpClient;

    const-string v1, "Android-Backup/1.0"

    invoke-direct {v0, p0, v1, v3}, Lcom/google/android/common/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mHttpClient:Lcom/google/android/common/http/GoogleHttpClient;

    .line 1361
    new-instance v0, Lcom/android/common/OperationScheduler$Options;

    invoke-direct {v0}, Lcom/android/common/OperationScheduler$Options;-><init>()V

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mSchedulerOptions:Lcom/android/common/OperationScheduler$Options;

    .line 1362
    new-instance v0, Lcom/android/common/OperationScheduler;

    const-string v1, "BackupTransport.backupScheduler"

    invoke-virtual {p0, v1, v3}, Lcom/google/android/backup/BackupTransportService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/common/OperationScheduler;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBackupScheduler:Lcom/android/common/OperationScheduler;

    .line 1364
    new-instance v0, Lcom/android/common/OperationScheduler;

    const-string v1, "BackupTransport.restoreScheduler"

    invoke-virtual {p0, v1, v3}, Lcom/google/android/backup/BackupTransportService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/common/OperationScheduler;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mRestoreScheduler:Lcom/android/common/OperationScheduler;

    .line 1366
    new-instance v0, Lcom/android/common/OperationScheduler;

    const-string v1, "BackupTransport.abortScheduler"

    invoke-virtual {p0, v1, v3}, Lcom/google/android/backup/BackupTransportService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/common/OperationScheduler;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mAbortScheduler:Lcom/android/common/OperationScheduler;

    .line 1369
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/backup/BackupTransportService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1371
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/backup/BackupTransportService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1373
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/backup/BackupTransportService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1375
    const/4 v0, 0x1

    new-instance v1, Lcom/google/android/backup/BackupTransportService$3;

    invoke-direct {v1, p0}, Lcom/google/android/backup/BackupTransportService$3;-><init>(Lcom/google/android/backup/BackupTransportService;)V

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mObserverHandle:Ljava/lang/Object;

    .line 1385
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/backup/BackupTransportService;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/backup/BackupTransportService$4;

    invoke-direct {v1, p0}, Lcom/google/android/backup/BackupTransportService$4;-><init>(Lcom/google/android/backup/BackupTransportService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1394
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/backup/BackupTransportService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 1396
    const-string v0, "BackupTransport.backupAccount"

    invoke-virtual {p0, v0, v3}, Lcom/google/android/backup/BackupTransportService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBackupAccountPreferences:Landroid/content/SharedPreferences;

    .line 1397
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 1401
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/backup/BackupTransportService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1402
    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mObserverHandle:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/backup/BackupTransportService;->mObserverHandle:Ljava/lang/Object;

    invoke-static {v0}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    .line 1403
    :cond_0
    return-void
.end method

.method protected declared-synchronized saveBackupAccount(Landroid/accounts/Account;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 264
    monitor-enter p0

    if-nez p1, :cond_0

    .line 265
    :try_start_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Backup account should be not null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 268
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/backup/BackupTransportService;->mBackupAccountPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 269
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "accountName"

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 270
    const-string v1, "accountType"

    iget-object v2, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 272
    const-string v1, "accountExpected"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 274
    const-string v1, "backupAccountMissingNotified"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 275
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 277
    invoke-direct {p0}, Lcom/google/android/backup/BackupTransportService;->cancelSetBackupAccountNotification()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 278
    monitor-exit p0

    return-void
.end method
