.class public Lcom/google/android/backup/BlockingFileDataStream;
.super Ljava/lang/Object;
.source "BlockingFileDataStream.java"

# interfaces
.implements Lcom/google/uploader/client/DataStream;


# instance fields
.field private availablePosition:J

.field private currentPosition:J

.field private final dataAvailable:Ljava/util/concurrent/locks/Condition;

.field private final dataProcessed:Ljava/util/concurrent/locks/Condition;

.field private hasException:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private hasMoreData:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private inputStream:Ljava/io/InputStream;

.field private final lock:Ljava/util/concurrent/locks/Lock;

.field private markPosition:J

.field private transferCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 5
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    .line 30
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->dataAvailable:Ljava/util/concurrent/locks/Condition;

    .line 31
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->dataProcessed:Ljava/util/concurrent/locks/Condition;

    .line 35
    iput-object p1, p0, Lcom/google/android/backup/BlockingFileDataStream;->inputStream:Ljava/io/InputStream;

    .line 37
    iput-wide v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->availablePosition:J

    .line 38
    iput-wide v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J

    .line 39
    iput-wide v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->markPosition:J

    .line 40
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->hasMoreData:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 41
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->hasException:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 42
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->transferCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 43
    return-void
.end method


# virtual methods
.method public finishDataStream()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 82
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 84
    :try_start_0
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->hasMoreData:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 86
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->dataAvailable:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public getMarkPosition()J
    .locals 3

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 189
    :try_start_0
    iget-wide v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    iget-object v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-wide v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public getReadAheadLimit()J
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 209
    const-wide v0, 0x7fffffffffffffffL

    .line 211
    iget-object v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-wide v0
.end method

.method public getReadPosition()J
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 199
    :try_start_0
    iget-wide v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    iget-object v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-wide v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public getSize()J
    .locals 3

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 283
    const-wide/16 v0, -0x1

    .line 285
    iget-object v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-wide v0
.end method

.method public hasMoreData()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 294
    :try_start_0
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->hasMoreData:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 296
    iget-object v1, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public mark()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 181
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 183
    return-void
.end method

.method public pushAvailableData(I)I
    .locals 6
    .param p1, "numBytes"    # I

    .prologue
    const/4 v1, -0x1

    .line 52
    iget-object v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 56
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->availablePosition:J

    int-to-long v4, p1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->availablePosition:J

    .line 59
    iget-object v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->dataAvailable:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 63
    :goto_0
    iget-wide v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J

    iget-wide v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->availablePosition:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->hasException:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_0

    .line 65
    iget-object v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->dataProcessed:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v2, "BackupTransportService"

    const-string v3, "InterruptedException when waiting for data processed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    iget-object v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_1
    return v1

    .line 68
    :cond_0
    :try_start_2
    iget-object v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->hasException:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 69
    const-string v2, "BackupTransportService"

    const-string v3, "Data stream received transfer exception."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 77
    iget-object v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    .line 72
    :cond_1
    const/4 v1, 0x0

    .line 77
    iget-object v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1
.end method

.method public read([BII)I
    .locals 8
    .param p1, "buffer"    # [B
    .param p2, "bufferOffset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 120
    array-length v4, p1

    sub-int/2addr v4, p2

    if-ge v4, p3, :cond_0

    .line 121
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Buffer length must be greater than desired number of bytes."

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 124
    :cond_0
    if-nez p3, :cond_1

    move v0, v3

    .line 170
    :goto_0
    return v0

    .line 128
    :cond_1
    iget-object v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 135
    :goto_1
    :try_start_0
    iget-wide v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J

    iget-wide v6, p0, Lcom/google/android/backup/BlockingFileDataStream;->availablePosition:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->hasMoreData:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->transferCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v4

    if-nez v4, :cond_2

    .line 137
    iget-object v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->dataAvailable:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 166
    :catch_0
    move-exception v2

    .line 167
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v3, "BackupTransportService"

    const-string v4, "InterruptedException when waiting for available data in read method."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3, v2}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 170
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v3

    .line 140
    :cond_2
    :try_start_2
    iget-object v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->transferCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 141
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Transfer cancelled by framework."

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 144
    :cond_3
    iget-object v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->hasMoreData:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    if-nez v4, :cond_4

    .line 170
    iget-object v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move v0, v3

    goto :goto_0

    .line 148
    :cond_4
    :try_start_3
    iget-wide v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->availablePosition:J

    iget-wide v6, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J

    sub-long/2addr v4, v6

    long-to-int v4, v4

    invoke-static {v4, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 149
    .local v1, "bytesToRead":I
    iget-object v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v4, p1, p2, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 152
    .local v0, "bytesRead":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_6

    .line 153
    iget-wide v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J

    .line 156
    iget-wide v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->availablePosition:J

    iget-wide v6, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_5

    .line 158
    iget-object v3, p0, Lcom/google/android/backup/BlockingFileDataStream;->dataProcessed:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 170
    :cond_5
    iget-object v3, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    :cond_6
    iget-object v4, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move v0, v3

    goto/16 :goto_0
.end method

.method public rewind()V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 222
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 224
    return-void
.end method

.method public signalTransferCancelled()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 106
    :try_start_0
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->transferCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 107
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->dataAvailable:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 111
    return-void

    .line 109
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public signalTransferException()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 96
    :try_start_0
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->hasException:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 97
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->dataProcessed:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    iget-object v0, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 101
    return-void

    .line 99
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public skip(J)J
    .locals 11
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v0, 0x0

    .line 233
    cmp-long v5, p1, v0

    if-gez v5, :cond_0

    .line 234
    new-instance v5, Ljava/io/IOException;

    const-string v6, "Can\'t skip negative bytes."

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 237
    :cond_0
    cmp-long v5, p1, v0

    if-nez v5, :cond_1

    .line 274
    .end local p1    # "n":J
    :goto_0
    return-wide p1

    .line 241
    .restart local p1    # "n":J
    :cond_1
    iget-object v5, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 248
    :goto_1
    :try_start_0
    iget-wide v6, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J

    iget-wide v8, p0, Lcom/google/android/backup/BlockingFileDataStream;->availablePosition:J

    cmp-long v5, v6, v8

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/backup/BlockingFileDataStream;->hasMoreData:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/backup/BlockingFileDataStream;->transferCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-nez v5, :cond_2

    .line 249
    iget-object v5, p0, Lcom/google/android/backup/BlockingFileDataStream;->dataAvailable:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 270
    :catch_0
    move-exception v4

    .line 271
    .local v4, "e":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v5, "BackupTransportService"

    const-string v6, "InterruptedException when waiting for available data in skip method."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    new-instance v5, Ljava/io/IOException;

    invoke-direct {v5, v4}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274
    .end local v4    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v6}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v5

    .line 252
    :cond_2
    :try_start_2
    iget-object v5, p0, Lcom/google/android/backup/BlockingFileDataStream;->transferCancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 253
    new-instance v5, Ljava/io/IOException;

    const-string v6, "Transfer cancelled by framework."

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 256
    :cond_3
    iget-object v5, p0, Lcom/google/android/backup/BlockingFileDataStream;->hasMoreData:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v5

    if-nez v5, :cond_4

    .line 274
    iget-object v5, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move-wide p1, v0

    goto :goto_0

    .line 260
    :cond_4
    :try_start_3
    iget-wide v6, p0, Lcom/google/android/backup/BlockingFileDataStream;->availablePosition:J

    iget-wide v8, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J

    sub-long/2addr v6, v8

    invoke-static {v6, v7, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 261
    .local v2, "bytesToSkip":J
    iget-object v5, p0, Lcom/google/android/backup/BlockingFileDataStream;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v5, v2, v3}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 263
    .local v0, "bytesSkipped":J
    iget-wide v6, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J

    add-long/2addr v6, v0

    iput-wide v6, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J

    .line 265
    iget-wide v6, p0, Lcom/google/android/backup/BlockingFileDataStream;->currentPosition:J

    iget-wide v8, p0, Lcom/google/android/backup/BlockingFileDataStream;->availablePosition:J

    cmp-long v5, v6, v8

    if-nez v5, :cond_5

    .line 266
    iget-object v5, p0, Lcom/google/android/backup/BlockingFileDataStream;->dataProcessed:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 274
    :cond_5
    iget-object v5, p0, Lcom/google/android/backup/BlockingFileDataStream;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move-wide p1, v0

    goto :goto_0
.end method
