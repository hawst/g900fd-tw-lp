.class Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;
.super Ljava/lang/Object;
.source "HttpUrlConnectionHttpClient.java"

# interfaces
.implements Lcom/google/uploader/client/Transfer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/uploader/client/HttpUrlConnectionHttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HttpUrlConnectionTransfer"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final connection:Ljava/net/HttpURLConnection;

.field private controlState:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

.field private estimatedBytesTransferred:J

.field private progressThreshold:I

.field private final requestBody:Lcom/google/uploader/client/DataStream;

.field private transferListener:Lcom/google/uploader/client/TransferListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/net/HttpURLConnection;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;Lcom/google/uploader/client/DataStream;)V
    .locals 10
    .param p1, "connection"    # Ljava/net/HttpURLConnection;
    .param p2, "method"    # Ljava/lang/String;
    .param p3, "headers"    # Lcom/google/api/client/http/HttpHeaders;
    .param p4, "body"    # Lcom/google/uploader/client/DataStream;

    .prologue
    const v7, 0x493e0

    const/4 v6, 0x1

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const/4 v5, -0x1

    iput v5, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->progressThreshold:I

    .line 97
    iput-object p1, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->connection:Ljava/net/HttpURLConnection;

    .line 99
    :try_start_0
    invoke-virtual {p1, p2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    invoke-virtual {p1, v7}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 104
    invoke-virtual {p1, v7}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 105
    invoke-virtual {p1, v6}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 108
    iput-object p4, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->requestBody:Lcom/google/uploader/client/DataStream;

    .line 109
    if-eqz p4, :cond_0

    .line 110
    invoke-virtual {p1, v6}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 111
    invoke-interface {p4}, Lcom/google/uploader/client/DataStream;->getSize()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-ltz v5, :cond_2

    .line 112
    invoke-interface {p4}, Lcom/google/uploader/client/DataStream;->getSize()J

    move-result-wide v6

    invoke-interface {p4}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v8

    sub-long v0, v6, v8

    .line 113
    .local v0, "contentLength":J
    const-wide/32 v6, 0x7fffffff

    cmp-long v5, v0, v6

    if-gez v5, :cond_1

    .line 116
    long-to-int v5, v0

    invoke-virtual {p1, v5}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 126
    .end local v0    # "contentLength":J
    :cond_0
    :goto_0
    invoke-virtual {p3}, Lcom/google/api/client/http/HttpHeaders;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 127
    .local v3, "headerName":Ljava/lang/String;
    invoke-virtual {p3, v3}, Lcom/google/api/client/http/HttpHeaders;->getFirstHeaderStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v3, v5}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 100
    .end local v3    # "headerName":Ljava/lang/String;
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 101
    .local v2, "e":Ljava/net/ProtocolException;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Invalid http method."

    invoke-direct {v5, v6, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 118
    .end local v2    # "e":Ljava/net/ProtocolException;
    .restart local v0    # "contentLength":J
    :cond_1
    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(J)V

    goto :goto_0

    .line 121
    .end local v0    # "contentLength":J
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    goto :goto_0

    .line 131
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_3
    sget-object v5, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;->IN_PROGRESS:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    iput-object v5, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->controlState:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    .line 132
    return-void
.end method

.method static synthetic access$000(Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;)Lcom/google/uploader/client/HttpResponse;
    .locals 1
    .param p0, "x0"    # Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->sendInternal()Lcom/google/uploader/client/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;)Lcom/google/uploader/client/TransferListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;

    return-object v0
.end method

.method private declared-synchronized checkControlState()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    .line 294
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->controlState:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    sget-object v1, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;->PAUSED:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    .line 297
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 298
    :catch_0
    move-exception v0

    goto :goto_0

    .line 303
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->controlState:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    sget-object v1, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;->CANCELED:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    if-ne v0, v1, :cond_1

    .line 305
    new-instance v0, Lcom/google/uploader/client/TransferException;

    sget-object v1, Lcom/google/uploader/client/TransferException$Type;->CANCELED:Lcom/google/uploader/client/TransferException$Type;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 294
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 308
    :cond_1
    :try_start_3
    sget-boolean v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->controlState:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    sget-object v1, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;->IN_PROGRESS:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 309
    :cond_2
    monitor-exit p0

    return-void
.end method

.method private getHttpResponseFromConnection()Lcom/google/uploader/client/HttpResponse;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    .line 252
    invoke-direct {p0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->checkControlState()V

    .line 254
    const/4 v5, -0x1

    .line 256
    .local v5, "responseCode":I
    :try_start_0
    iget-object v7, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 262
    const/4 v4, 0x0

    .line 264
    .local v4, "responseBodyInputStream":Ljava/io/InputStream;
    :try_start_1
    iget-object v7, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 269
    :goto_0
    const/4 v6, 0x0

    .line 270
    .local v6, "responseHeaders":Lcom/google/api/client/http/HttpHeaders;
    iget-object v7, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v1

    .line 271
    .local v1, "fields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    if-eqz v1, :cond_1

    .line 272
    new-instance v6, Lcom/google/api/client/http/HttpHeaders;

    .end local v6    # "responseHeaders":Lcom/google/api/client/http/HttpHeaders;
    invoke-direct {v6}, Lcom/google/api/client/http/HttpHeaders;-><init>()V

    .line 274
    .restart local v6    # "responseHeaders":Lcom/google/api/client/http/HttpHeaders;
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 275
    .local v3, "key":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 277
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v3, v7}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;

    goto :goto_1

    .line 257
    .end local v1    # "fields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "responseBodyInputStream":Ljava/io/InputStream;
    .end local v6    # "responseHeaders":Lcom/google/api/client/http/HttpHeaders;
    :catch_0
    move-exception v0

    .line 258
    .local v0, "e":Ljava/io/IOException;
    new-instance v7, Lcom/google/uploader/client/TransferException;

    sget-object v8, Lcom/google/uploader/client/TransferException$Type;->CONNECTION_ERROR:Lcom/google/uploader/client/TransferException$Type;

    const-string v9, "Error while reading response code."

    invoke-direct {v7, v8, v9, v0}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 265
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v4    # "responseBodyInputStream":Ljava/io/InputStream;
    :catch_1
    move-exception v0

    .line 266
    .restart local v0    # "e":Ljava/io/IOException;
    iget-object v7, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v4

    goto :goto_0

    .line 282
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "fields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .restart local v6    # "responseHeaders":Lcom/google/api/client/http/HttpHeaders;
    :cond_1
    new-instance v7, Lcom/google/uploader/client/HttpResponse;

    invoke-direct {v7, v5, v6, v4}, Lcom/google/uploader/client/HttpResponse;-><init>(ILcom/google/api/client/http/HttpHeaders;Ljava/io/InputStream;)V

    return-object v7
.end method

.method private requestBodyHasMoreData()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    .line 287
    :try_start_0
    iget-object v1, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->requestBody:Lcom/google/uploader/client/DataStream;

    invoke-interface {v1}, Lcom/google/uploader/client/DataStream;->hasMoreData()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 288
    :catch_0
    move-exception v0

    .line 289
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Lcom/google/uploader/client/TransferException;

    sget-object v2, Lcom/google/uploader/client/TransferException$Type;->REQUEST_BODY_READ_ERROR:Lcom/google/uploader/client/TransferException$Type;

    invoke-direct {v1, v2, v0}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private sendInternal()Lcom/google/uploader/client/HttpResponse;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/uploader/client/TransferException;
        }
    .end annotation

    .prologue
    const/high16 v12, 0x10000

    .line 172
    monitor-enter p0

    .line 173
    :try_start_0
    iget-object v8, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;

    if-eqz v8, :cond_0

    .line 174
    iget-object v8, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;

    invoke-interface {v8, p0}, Lcom/google/uploader/client/TransferListener;->onStart(Lcom/google/uploader/client/Transfer;)V

    .line 176
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    invoke-direct {p0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->checkControlState()V

    .line 181
    :try_start_1
    iget-object v8, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->connect()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 194
    iget-object v8, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->requestBody:Lcom/google/uploader/client/DataStream;

    if-nez v8, :cond_1

    .line 195
    invoke-direct {p0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->getHttpResponseFromConnection()Lcom/google/uploader/client/HttpResponse;

    move-result-object v8

    .line 244
    :goto_0
    return-object v8

    .line 176
    :catchall_0
    move-exception v8

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v8

    .line 182
    :catch_0
    move-exception v3

    .line 184
    .local v3, "e":Ljava/io/FileNotFoundException;
    new-instance v8, Lcom/google/uploader/client/TransferException;

    sget-object v9, Lcom/google/uploader/client/TransferException$Type;->BAD_URL:Lcom/google/uploader/client/TransferException$Type;

    invoke-direct {v8, v9, v3}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/Throwable;)V

    throw v8

    .line 185
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v4

    .line 187
    .local v4, "originalException":Ljava/io/IOException;
    :try_start_3
    invoke-direct {p0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->getHttpResponseFromConnection()Lcom/google/uploader/client/HttpResponse;
    :try_end_3
    .catch Lcom/google/uploader/client/TransferException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v8

    goto :goto_0

    .line 188
    :catch_2
    move-exception v7

    .line 189
    .local v7, "transferException":Lcom/google/uploader/client/TransferException;
    new-instance v8, Lcom/google/uploader/client/TransferException;

    sget-object v9, Lcom/google/uploader/client/TransferException$Type;->CONNECTION_ERROR:Lcom/google/uploader/client/TransferException$Type;

    invoke-direct {v8, v9, v4}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/Throwable;)V

    throw v8

    .line 198
    .end local v4    # "originalException":Ljava/io/IOException;
    .end local v7    # "transferException":Lcom/google/uploader/client/TransferException;
    :cond_1
    const/4 v5, 0x0

    .line 200
    .local v5, "requestBodyOutputStream":Ljava/io/OutputStream;
    :try_start_4
    iget-object v8, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v8}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v5

    .line 209
    const/4 v2, 0x0

    .line 210
    .local v2, "bytesSinceLastProgressNotification":I
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->requestBodyHasMoreData()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 211
    invoke-direct {p0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->checkControlState()V

    .line 212
    const/4 v6, 0x0

    .line 213
    .local v6, "totalBytesRead":I
    new-array v0, v12, [B

    .line 214
    .local v0, "buffer":[B
    :goto_2
    if-ge v6, v12, :cond_3

    invoke-direct {p0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->requestBodyHasMoreData()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 215
    const/4 v1, 0x0

    .line 217
    .local v1, "bytesRead":I
    :try_start_5
    iget-object v8, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->requestBody:Lcom/google/uploader/client/DataStream;

    sub-int v9, v12, v6

    invoke-interface {v8, v0, v6, v9}, Lcom/google/uploader/client/DataStream;->read([BII)I

    move-result v1

    .line 218
    iget-wide v8, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->estimatedBytesTransferred:J

    int-to-long v10, v1

    add-long/2addr v8, v10

    iput-wide v8, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->estimatedBytesTransferred:J
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 219
    add-int/2addr v6, v1

    .line 224
    sub-int v8, v6, v1

    :try_start_6
    invoke-virtual {v5, v0, v8, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_2

    .line 225
    :catch_3
    move-exception v4

    .line 227
    .restart local v4    # "originalException":Ljava/io/IOException;
    :try_start_7
    invoke-direct {p0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->getHttpResponseFromConnection()Lcom/google/uploader/client/HttpResponse;
    :try_end_7
    .catch Lcom/google/uploader/client/TransferException; {:try_start_7 .. :try_end_7} :catch_7

    move-result-object v8

    goto :goto_0

    .line 201
    .end local v0    # "buffer":[B
    .end local v1    # "bytesRead":I
    .end local v2    # "bytesSinceLastProgressNotification":I
    .end local v4    # "originalException":Ljava/io/IOException;
    .end local v6    # "totalBytesRead":I
    :catch_4
    move-exception v4

    .line 203
    .restart local v4    # "originalException":Ljava/io/IOException;
    :try_start_8
    invoke-direct {p0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->getHttpResponseFromConnection()Lcom/google/uploader/client/HttpResponse;
    :try_end_8
    .catch Lcom/google/uploader/client/TransferException; {:try_start_8 .. :try_end_8} :catch_5

    move-result-object v8

    goto :goto_0

    .line 204
    :catch_5
    move-exception v7

    .line 205
    .restart local v7    # "transferException":Lcom/google/uploader/client/TransferException;
    new-instance v8, Lcom/google/uploader/client/TransferException;

    sget-object v9, Lcom/google/uploader/client/TransferException$Type;->CONNECTION_ERROR:Lcom/google/uploader/client/TransferException$Type;

    invoke-direct {v8, v9, v4}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/Throwable;)V

    throw v8

    .line 220
    .end local v4    # "originalException":Ljava/io/IOException;
    .end local v7    # "transferException":Lcom/google/uploader/client/TransferException;
    .restart local v0    # "buffer":[B
    .restart local v1    # "bytesRead":I
    .restart local v2    # "bytesSinceLastProgressNotification":I
    .restart local v6    # "totalBytesRead":I
    :catch_6
    move-exception v3

    .line 221
    .local v3, "e":Ljava/io/IOException;
    new-instance v8, Lcom/google/uploader/client/TransferException;

    sget-object v9, Lcom/google/uploader/client/TransferException$Type;->REQUEST_BODY_READ_ERROR:Lcom/google/uploader/client/TransferException$Type;

    invoke-direct {v8, v9, v3}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/Throwable;)V

    throw v8

    .line 228
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v4    # "originalException":Ljava/io/IOException;
    :catch_7
    move-exception v7

    .line 229
    .restart local v7    # "transferException":Lcom/google/uploader/client/TransferException;
    new-instance v8, Lcom/google/uploader/client/TransferException;

    sget-object v9, Lcom/google/uploader/client/TransferException$Type;->REQUEST_BODY_READ_ERROR:Lcom/google/uploader/client/TransferException$Type;

    invoke-direct {v8, v9, v4}, Lcom/google/uploader/client/TransferException;-><init>(Lcom/google/uploader/client/TransferException$Type;Ljava/lang/Throwable;)V

    throw v8

    .line 234
    .end local v1    # "bytesRead":I
    .end local v4    # "originalException":Ljava/io/IOException;
    .end local v7    # "transferException":Lcom/google/uploader/client/TransferException;
    :cond_3
    add-int/2addr v2, v6

    .line 235
    iget v8, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->progressThreshold:I

    if-le v2, v8, :cond_2

    .line 236
    monitor-enter p0

    .line 237
    :try_start_9
    iget-object v8, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;

    if-eqz v8, :cond_4

    .line 238
    iget-object v8, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;

    invoke-interface {v8, p0}, Lcom/google/uploader/client/TransferListener;->onUploadProgress(Lcom/google/uploader/client/Transfer;)V

    .line 240
    :cond_4
    monitor-exit p0

    .line 241
    const/4 v2, 0x0

    goto :goto_1

    .line 240
    :catchall_1
    move-exception v8

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    throw v8

    .line 244
    .end local v0    # "buffer":[B
    .end local v6    # "totalBytesRead":I
    :cond_5
    invoke-direct {p0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->getHttpResponseFromConnection()Lcom/google/uploader/client/HttpResponse;

    move-result-object v8

    goto/16 :goto_0
.end method


# virtual methods
.method public declared-synchronized attachListener(Lcom/google/uploader/client/TransferListener;I)V
    .locals 1
    .param p1, "listener"    # Lcom/google/uploader/client/TransferListener;
    .param p2, "progressThreshold"    # I

    .prologue
    .line 356
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->transferListener:Lcom/google/uploader/client/TransferListener;

    .line 357
    if-lez p2, :cond_0

    .line 358
    iput p2, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->progressThreshold:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 360
    :cond_0
    monitor-exit p0

    return-void

    .line 356
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getBytesUploaded()J
    .locals 2

    .prologue
    .line 351
    iget-wide v0, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;->estimatedBytesTransferred:J

    return-wide v0
.end method

.method public send()Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/uploader/client/TransferExceptionOrHttpResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    new-instance v1, Ljava/util/concurrent/FutureTask;

    new-instance v2, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;

    invoke-direct {v2, p0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$1;-><init>(Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;)V

    invoke-direct {v1, v2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 162
    .local v1, "future":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Lcom/google/uploader/client/TransferExceptionOrHttpResponse;>;"
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 163
    .local v0, "executor":Ljava/util/concurrent/ExecutorService;
    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 164
    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 165
    return-object v1
.end method
