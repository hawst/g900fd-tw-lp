.class final enum Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;
.super Ljava/lang/Enum;
.source "HttpUrlConnectionHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ControlState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

.field public static final enum CANCELED:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

.field public static final enum IN_PROGRESS:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

.field public static final enum PAUSED:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    new-instance v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v2}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;->IN_PROGRESS:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    .line 74
    new-instance v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;->PAUSED:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    .line 75
    new-instance v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    const-string v1, "CANCELED"

    invoke-direct {v0, v1, v4}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;->CANCELED:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    .line 72
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    sget-object v1, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;->IN_PROGRESS:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;->PAUSED:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;->CANCELED:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;->$VALUES:[Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 72
    const-class v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    return-object v0
.end method

.method public static values()[Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;->$VALUES:[Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    invoke-virtual {v0}, [Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer$ControlState;

    return-object v0
.end method
