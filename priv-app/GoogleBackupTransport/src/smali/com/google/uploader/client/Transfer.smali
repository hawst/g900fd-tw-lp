.class public interface abstract Lcom/google/uploader/client/Transfer;
.super Ljava/lang/Object;
.source "Transfer.java"


# virtual methods
.method public abstract attachListener(Lcom/google/uploader/client/TransferListener;I)V
.end method

.method public abstract getBytesUploaded()J
.end method

.method public abstract send()Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/uploader/client/TransferExceptionOrHttpResponse;",
            ">;"
        }
    .end annotation
.end method
