.class public Lcom/google/uploader/client/TransferOptions;
.super Ljava/lang/Object;
.source "TransferOptions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/uploader/client/TransferOptions$1;,
        Lcom/google/uploader/client/TransferOptions$Builder;
    }
.end annotation


# instance fields
.field private final idleTimeoutSecs:J


# direct methods
.method private constructor <init>(Lcom/google/uploader/client/TransferOptions$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/google/uploader/client/TransferOptions$Builder;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    # getter for: Lcom/google/uploader/client/TransferOptions$Builder;->idleTimeoutSecs:J
    invoke-static {p1}, Lcom/google/uploader/client/TransferOptions$Builder;->access$100(Lcom/google/uploader/client/TransferOptions$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/uploader/client/TransferOptions;->idleTimeoutSecs:J

    .line 42
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/uploader/client/TransferOptions$Builder;Lcom/google/uploader/client/TransferOptions$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/uploader/client/TransferOptions$Builder;
    .param p2, "x1"    # Lcom/google/uploader/client/TransferOptions$1;

    .prologue
    .line 6
    invoke-direct {p0, p1}, Lcom/google/uploader/client/TransferOptions;-><init>(Lcom/google/uploader/client/TransferOptions$Builder;)V

    return-void
.end method

.method public static newBuilder()Lcom/google/uploader/client/TransferOptions$Builder;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/google/uploader/client/TransferOptions$Builder;

    invoke-direct {v0}, Lcom/google/uploader/client/TransferOptions$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getIdleTimeoutSecs()J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/google/uploader/client/TransferOptions;->idleTimeoutSecs:J

    return-wide v0
.end method
