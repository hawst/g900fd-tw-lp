.class Lcom/google/uploader/client/ChunkedDataStream;
.super Lcom/google/uploader/client/PartialDataStream;
.source "ChunkedDataStream.java"


# direct methods
.method public constructor <init>(Lcom/google/uploader/client/DataStream;I)V
    .locals 2
    .param p1, "stream"    # Lcom/google/uploader/client/DataStream;
    .param p2, "chunkGranularity"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    invoke-static {p1, p2}, Lcom/google/uploader/client/ChunkedDataStream;->findSize(Lcom/google/uploader/client/DataStream;I)J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/uploader/client/PartialDataStream;-><init>(Lcom/google/uploader/client/DataStream;J)V

    .line 33
    return-void
.end method

.method private static findSize(Lcom/google/uploader/client/DataStream;I)J
    .locals 10
    .param p0, "stream"    # Lcom/google/uploader/client/DataStream;
    .param p1, "chunkGranularity"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    if-lez p1, :cond_1

    const/4 v6, 0x1

    :goto_0
    const-string v7, "Chunk granularity must be greater than 0."

    invoke-static {v6, v7}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 37
    int-to-long v6, p1

    invoke-interface {p0}, Lcom/google/uploader/client/DataStream;->getReadAheadLimit()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-gez v6, :cond_2

    const/4 v6, 0x1

    :goto_1
    const-string v7, "Chunk granularity must be smaller than the read ahead limit."

    invoke-static {v6, v7}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 42
    invoke-interface {p0}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v4

    .line 43
    .local v4, "startPosition":J
    invoke-interface {p0}, Lcom/google/uploader/client/DataStream;->getSize()J

    move-result-wide v0

    .line 44
    .local v0, "endPosition":J
    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-ltz v6, :cond_3

    .line 45
    invoke-interface {p0}, Lcom/google/uploader/client/DataStream;->getMarkPosition()J

    move-result-wide v6

    invoke-interface {p0}, Lcom/google/uploader/client/DataStream;->getReadAheadLimit()J

    move-result-wide v8

    add-long v2, v6, v8

    .line 46
    .local v2, "maxReadPosition":J
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_0

    cmp-long v6, v2, v0

    if-gez v6, :cond_0

    .line 47
    move-wide v0, v2

    .line 60
    .end local v2    # "maxReadPosition":J
    :cond_0
    :goto_2
    sub-long v6, v0, v4

    int-to-long v8, p1

    div-long/2addr v6, v8

    int-to-long v8, p1

    mul-long/2addr v6, v8

    return-wide v6

    .line 36
    .end local v0    # "endPosition":J
    .end local v4    # "startPosition":J
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 37
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 51
    .restart local v0    # "endPosition":J
    .restart local v4    # "startPosition":J
    :cond_3
    :goto_3
    invoke-interface {p0}, Lcom/google/uploader/client/DataStream;->hasMoreData()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {p0}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v6

    invoke-interface {p0}, Lcom/google/uploader/client/DataStream;->getMarkPosition()J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-interface {p0}, Lcom/google/uploader/client/DataStream;->getReadAheadLimit()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-gez v6, :cond_4

    .line 53
    invoke-interface {p0}, Lcom/google/uploader/client/DataStream;->getReadAheadLimit()J

    move-result-wide v6

    invoke-interface {p0, v6, v7}, Lcom/google/uploader/client/DataStream;->skip(J)J

    goto :goto_3

    .line 55
    :cond_4
    invoke-interface {p0}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v0

    .line 56
    invoke-interface {p0}, Lcom/google/uploader/client/DataStream;->rewind()V

    .line 57
    invoke-interface {p0}, Lcom/google/uploader/client/DataStream;->getMarkPosition()J

    move-result-wide v6

    sub-long v6, v4, v6

    invoke-interface {p0, v6, v7}, Lcom/google/uploader/client/DataStream;->skip(J)J

    goto :goto_2
.end method
