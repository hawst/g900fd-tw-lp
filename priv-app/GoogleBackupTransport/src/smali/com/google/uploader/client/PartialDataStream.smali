.class Lcom/google/uploader/client/PartialDataStream;
.super Ljava/lang/Object;
.source "PartialDataStream.java"

# interfaces
.implements Lcom/google/uploader/client/DataStream;


# instance fields
.field private markPosition:J

.field private readPosition:J

.field private final size:J

.field private final startPosition:J

.field private final underlyingStream:Lcom/google/uploader/client/DataStream;


# direct methods
.method public constructor <init>(Lcom/google/uploader/client/DataStream;J)V
    .locals 6
    .param p1, "underlyingStream"    # Lcom/google/uploader/client/DataStream;
    .param p2, "size"    # J

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-interface {p1}, Lcom/google/uploader/client/DataStream;->getReadAheadLimit()J

    move-result-wide v0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 25
    invoke-interface {p1}, Lcom/google/uploader/client/DataStream;->getReadAheadLimit()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v2

    invoke-interface {p1}, Lcom/google/uploader/client/DataStream;->getMarkPosition()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    cmp-long v0, p2, v0

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 30
    :cond_0
    iput-object p1, p0, Lcom/google/uploader/client/PartialDataStream;->underlyingStream:Lcom/google/uploader/client/DataStream;

    .line 31
    invoke-interface {p1}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/uploader/client/PartialDataStream;->startPosition:J

    .line 32
    iput-wide p2, p0, Lcom/google/uploader/client/PartialDataStream;->size:J

    .line 33
    return-void

    .line 25
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getMarkPosition()J
    .locals 2

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/uploader/client/PartialDataStream;->markPosition:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getReadAheadLimit()J
    .locals 2

    .prologue
    .line 76
    monitor-enter p0

    const-wide v0, 0x7fffffffffffffffL

    monitor-exit p0

    return-wide v0
.end method

.method public declared-synchronized getReadPosition()J
    .locals 2

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSize()J
    .locals 2

    .prologue
    .line 109
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/uploader/client/PartialDataStream;->size:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasMoreData()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J

    iget-wide v2, p0, Lcom/google/uploader/client/PartialDataStream;->size:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized mark()V
    .locals 2

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J

    iput-wide v0, p0, Lcom/google/uploader/client/PartialDataStream;->markPosition:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    monitor-exit p0

    return-void

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized read([BII)I
    .locals 10
    .param p1, "buffer"    # [B
    .param p2, "bufferOffset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    monitor-enter p0

    :try_start_0
    array-length v1, p1

    sub-int/2addr v1, p2

    if-lt v1, p3, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v4, "Cannot read into a buffer smaller than given length"

    invoke-static {v1, v4}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 42
    int-to-long v4, p3

    iget-wide v6, p0, Lcom/google/uploader/client/PartialDataStream;->size:J

    iget-wide v8, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J

    sub-long/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int p3, v4

    .line 46
    iget-wide v4, p0, Lcom/google/uploader/client/PartialDataStream;->startPosition:J

    iget-wide v6, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J

    add-long/2addr v4, v6

    iget-object v1, p0, Lcom/google/uploader/client/PartialDataStream;->underlyingStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v1}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 47
    iget-object v1, p0, Lcom/google/uploader/client/PartialDataStream;->underlyingStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v1}, Lcom/google/uploader/client/DataStream;->rewind()V

    .line 48
    iget-wide v4, p0, Lcom/google/uploader/client/PartialDataStream;->startPosition:J

    iget-object v1, p0, Lcom/google/uploader/client/PartialDataStream;->underlyingStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v1}, Lcom/google/uploader/client/DataStream;->getMarkPosition()J

    move-result-wide v6

    sub-long/2addr v4, v6

    iget-wide v6, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J

    add-long v2, v4, v6

    .line 49
    .local v2, "bytesToSkip":J
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 50
    iget-object v1, p0, Lcom/google/uploader/client/PartialDataStream;->underlyingStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v1, v2, v3}, Lcom/google/uploader/client/DataStream;->skip(J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    goto :goto_1

    .line 37
    .end local v2    # "bytesToSkip":J
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 54
    :cond_1
    iget-object v1, p0, Lcom/google/uploader/client/PartialDataStream;->underlyingStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v1, p1, p2, p3}, Lcom/google/uploader/client/DataStream;->read([BII)I

    move-result v0

    .line 55
    .local v0, "bytesRead":I
    iget-wide v4, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    monitor-exit p0

    return v0

    .line 37
    .end local v0    # "bytesRead":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized rewind()V
    .locals 2

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/uploader/client/PartialDataStream;->markPosition:J

    iput-wide v0, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    monitor-exit p0

    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized skip(J)J
    .locals 9
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    monitor-enter p0

    :try_start_0
    iget-wide v4, p0, Lcom/google/uploader/client/PartialDataStream;->size:J

    iget-wide v6, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J

    sub-long/2addr v4, v6

    invoke-static {p1, p2, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    .line 94
    iget-wide v4, p0, Lcom/google/uploader/client/PartialDataStream;->startPosition:J

    iget-wide v6, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J

    add-long/2addr v4, v6

    iget-object v6, p0, Lcom/google/uploader/client/PartialDataStream;->underlyingStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v6}, Lcom/google/uploader/client/DataStream;->getReadPosition()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 95
    iget-object v4, p0, Lcom/google/uploader/client/PartialDataStream;->underlyingStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v4}, Lcom/google/uploader/client/DataStream;->rewind()V

    .line 96
    iget-wide v4, p0, Lcom/google/uploader/client/PartialDataStream;->startPosition:J

    iget-object v6, p0, Lcom/google/uploader/client/PartialDataStream;->underlyingStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v6}, Lcom/google/uploader/client/DataStream;->getMarkPosition()J

    move-result-wide v6

    sub-long/2addr v4, v6

    iget-wide v6, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J

    add-long v2, v4, v6

    .line 97
    .local v2, "bytesToSkip":J
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 98
    iget-object v4, p0, Lcom/google/uploader/client/PartialDataStream;->underlyingStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v4, v2, v3}, Lcom/google/uploader/client/DataStream;->skip(J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    goto :goto_0

    .line 102
    .end local v2    # "bytesToSkip":J
    :cond_0
    iget-object v4, p0, Lcom/google/uploader/client/PartialDataStream;->underlyingStream:Lcom/google/uploader/client/DataStream;

    invoke-interface {v4, p1, p2}, Lcom/google/uploader/client/DataStream;->skip(J)J

    move-result-wide v0

    .line 103
    .local v0, "bytesSkipped":J
    iget-wide v4, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J

    add-long/2addr v4, v0

    iput-wide v4, p0, Lcom/google/uploader/client/PartialDataStream;->readPosition:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    monitor-exit p0

    return-wide v0

    .line 90
    .end local v0    # "bytesSkipped":J
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method
