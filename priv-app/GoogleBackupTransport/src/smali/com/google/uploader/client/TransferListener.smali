.class public interface abstract Lcom/google/uploader/client/TransferListener;
.super Ljava/lang/Object;
.source "TransferListener.java"


# virtual methods
.method public abstract onException(Lcom/google/uploader/client/Transfer;Lcom/google/uploader/client/TransferException;)V
.end method

.method public abstract onResponseReceived(Lcom/google/uploader/client/Transfer;Lcom/google/uploader/client/HttpResponse;)V
.end method

.method public abstract onStart(Lcom/google/uploader/client/Transfer;)V
.end method

.method public abstract onUploadProgress(Lcom/google/uploader/client/Transfer;)V
.end method
