.class public Lcom/google/uploader/client/HttpResponse;
.super Ljava/lang/Object;
.source "HttpResponse.java"


# instance fields
.field private final responseBody:Ljava/io/InputStream;

.field private final responseCode:I

.field private final responseHeaders:Lcom/google/api/client/http/HttpHeaders;


# direct methods
.method public constructor <init>(ILcom/google/api/client/http/HttpHeaders;Ljava/io/InputStream;)V
    .locals 0
    .param p1, "responseCode"    # I
    .param p2, "responseHeaders"    # Lcom/google/api/client/http/HttpHeaders;
    .param p3, "responseBody"    # Ljava/io/InputStream;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput p1, p0, Lcom/google/uploader/client/HttpResponse;->responseCode:I

    .line 23
    iput-object p2, p0, Lcom/google/uploader/client/HttpResponse;->responseHeaders:Lcom/google/api/client/http/HttpHeaders;

    .line 24
    iput-object p3, p0, Lcom/google/uploader/client/HttpResponse;->responseBody:Ljava/io/InputStream;

    .line 25
    return-void
.end method


# virtual methods
.method public getResponseCode()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/uploader/client/HttpResponse;->responseCode:I

    return v0
.end method

.method public getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/uploader/client/HttpResponse;->responseHeaders:Lcom/google/api/client/http/HttpHeaders;

    return-object v0
.end method

.method public toDebugString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HttpResponse:\n   "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/uploader/client/HttpResponse;->responseCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/uploader/client/HttpResponse;->responseHeaders:Lcom/google/api/client/http/HttpHeaders;

    invoke-virtual {v1}, Lcom/google/api/client/http/HttpHeaders;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
