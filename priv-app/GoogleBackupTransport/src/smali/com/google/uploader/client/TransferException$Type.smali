.class public final enum Lcom/google/uploader/client/TransferException$Type;
.super Ljava/lang/Enum;
.source "TransferException.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/uploader/client/TransferException;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/uploader/client/TransferException$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/uploader/client/TransferException$Type;

.field public static final enum BAD_URL:Lcom/google/uploader/client/TransferException$Type;

.field public static final enum CANCELED:Lcom/google/uploader/client/TransferException$Type;

.field public static final enum CONNECTION_ERROR:Lcom/google/uploader/client/TransferException$Type;

.field public static final enum REQUEST_BODY_READ_ERROR:Lcom/google/uploader/client/TransferException$Type;

.field public static final enum SERVER_ERROR:Lcom/google/uploader/client/TransferException$Type;

.field public static final enum UNKNOWN:Lcom/google/uploader/client/TransferException$Type;


# instance fields
.field private final recoverable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 17
    new-instance v0, Lcom/google/uploader/client/TransferException$Type;

    const-string v1, "BAD_URL"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/uploader/client/TransferException$Type;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/uploader/client/TransferException$Type;->BAD_URL:Lcom/google/uploader/client/TransferException$Type;

    .line 19
    new-instance v0, Lcom/google/uploader/client/TransferException$Type;

    const-string v1, "CANCELED"

    invoke-direct {v0, v1, v4, v3}, Lcom/google/uploader/client/TransferException$Type;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/uploader/client/TransferException$Type;->CANCELED:Lcom/google/uploader/client/TransferException$Type;

    .line 21
    new-instance v0, Lcom/google/uploader/client/TransferException$Type;

    const-string v1, "REQUEST_BODY_READ_ERROR"

    invoke-direct {v0, v1, v5, v3}, Lcom/google/uploader/client/TransferException$Type;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/uploader/client/TransferException$Type;->REQUEST_BODY_READ_ERROR:Lcom/google/uploader/client/TransferException$Type;

    .line 26
    new-instance v0, Lcom/google/uploader/client/TransferException$Type;

    const-string v1, "CONNECTION_ERROR"

    invoke-direct {v0, v1, v6, v4}, Lcom/google/uploader/client/TransferException$Type;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/uploader/client/TransferException$Type;->CONNECTION_ERROR:Lcom/google/uploader/client/TransferException$Type;

    .line 31
    new-instance v0, Lcom/google/uploader/client/TransferException$Type;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v7, v4}, Lcom/google/uploader/client/TransferException$Type;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/uploader/client/TransferException$Type;->SERVER_ERROR:Lcom/google/uploader/client/TransferException$Type;

    .line 36
    new-instance v0, Lcom/google/uploader/client/TransferException$Type;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/uploader/client/TransferException$Type;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/uploader/client/TransferException$Type;->UNKNOWN:Lcom/google/uploader/client/TransferException$Type;

    .line 12
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/uploader/client/TransferException$Type;

    sget-object v1, Lcom/google/uploader/client/TransferException$Type;->BAD_URL:Lcom/google/uploader/client/TransferException$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/uploader/client/TransferException$Type;->CANCELED:Lcom/google/uploader/client/TransferException$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/uploader/client/TransferException$Type;->REQUEST_BODY_READ_ERROR:Lcom/google/uploader/client/TransferException$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/uploader/client/TransferException$Type;->CONNECTION_ERROR:Lcom/google/uploader/client/TransferException$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/uploader/client/TransferException$Type;->SERVER_ERROR:Lcom/google/uploader/client/TransferException$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/uploader/client/TransferException$Type;->UNKNOWN:Lcom/google/uploader/client/TransferException$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/uploader/client/TransferException$Type;->$VALUES:[Lcom/google/uploader/client/TransferException$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .param p3, "recoverable"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput-boolean p3, p0, Lcom/google/uploader/client/TransferException$Type;->recoverable:Z

    .line 42
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/uploader/client/TransferException$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/google/uploader/client/TransferException$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/uploader/client/TransferException$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/uploader/client/TransferException$Type;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/uploader/client/TransferException$Type;->$VALUES:[Lcom/google/uploader/client/TransferException$Type;

    invoke-virtual {v0}, [Lcom/google/uploader/client/TransferException$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/uploader/client/TransferException$Type;

    return-object v0
.end method


# virtual methods
.method isRecoverable()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/uploader/client/TransferException$Type;->recoverable:Z

    return v0
.end method
