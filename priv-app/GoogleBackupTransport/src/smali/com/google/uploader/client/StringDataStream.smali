.class public Lcom/google/uploader/client/StringDataStream;
.super Ljava/lang/Object;
.source "StringDataStream.java"

# interfaces
.implements Lcom/google/uploader/client/DataStream;


# instance fields
.field private final contents:[B

.field private currentPosition:I

.field private markPosition:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "contents"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    :try_start_0
    const-string v1, "UTF-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/uploader/client/StringDataStream;->contents:[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    return-void

    .line 19
    :catch_0
    move-exception v0

    .line 21
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public declared-synchronized getMarkPosition()J
    .locals 2

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/uploader/client/StringDataStream;->markPosition:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    int-to-long v0, v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getReadAheadLimit()J
    .locals 2

    .prologue
    .line 65
    monitor-enter p0

    const-wide v0, 0x7fffffffffffffffL

    monitor-exit p0

    return-wide v0
.end method

.method public declared-synchronized getReadPosition()J
    .locals 2

    .prologue
    .line 60
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/uploader/client/StringDataStream;->currentPosition:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    int-to-long v0, v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSize()J
    .locals 2

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/uploader/client/StringDataStream;->contents:[B

    array-length v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    int-to-long v0, v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasMoreData()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/uploader/client/StringDataStream;->currentPosition:I

    iget-object v1, p0, Lcom/google/uploader/client/StringDataStream;->contents:[B

    array-length v1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized mark()V
    .locals 1

    .prologue
    .line 50
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/uploader/client/StringDataStream;->currentPosition:I

    iput v0, p0, Lcom/google/uploader/client/StringDataStream;->markPosition:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    monitor-exit p0

    return-void

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized read([BII)I
    .locals 4
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 27
    monitor-enter p0

    :try_start_0
    array-length v2, p1

    sub-int/2addr v2, p2

    if-lt v2, p3, :cond_1

    const/4 v2, 0x1

    :goto_0
    const-string v3, "Buffer length too small."

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    if-nez p3, :cond_2

    .line 45
    :cond_0
    :goto_1
    monitor-exit p0

    return v1

    :cond_1
    move v2, v1

    .line 27
    goto :goto_0

    .line 35
    :cond_2
    :try_start_1
    iget v2, p0, Lcom/google/uploader/client/StringDataStream;->currentPosition:I

    iget-object v3, p0, Lcom/google/uploader/client/StringDataStream;->contents:[B

    array-length v3, v3

    if-eq v2, v3, :cond_0

    .line 39
    iget-object v1, p0, Lcom/google/uploader/client/StringDataStream;->contents:[B

    array-length v1, v1

    iget v2, p0, Lcom/google/uploader/client/StringDataStream;->currentPosition:I

    sub-int/2addr v1, v2

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result p3

    .line 40
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-ge v0, p3, :cond_3

    .line 41
    add-int v1, p2, v0

    iget-object v2, p0, Lcom/google/uploader/client/StringDataStream;->contents:[B

    iget v3, p0, Lcom/google/uploader/client/StringDataStream;->currentPosition:I

    aget-byte v2, v2, v3

    aput-byte v2, p1, v1

    .line 42
    iget v1, p0, Lcom/google/uploader/client/StringDataStream;->currentPosition:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/uploader/client/StringDataStream;->currentPosition:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v1, p3

    .line 45
    goto :goto_1

    .line 27
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized rewind()V
    .locals 1

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/uploader/client/StringDataStream;->markPosition:I

    iput v0, p0, Lcom/google/uploader/client/StringDataStream;->currentPosition:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    monitor-exit p0

    return-void

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized skip(J)J
    .locals 5
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/uploader/client/StringDataStream;->contents:[B

    array-length v2, v2

    iget v3, p0, Lcom/google/uploader/client/StringDataStream;->currentPosition:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    invoke-static {p1, p2, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 76
    .local v0, "skippedBytes":J
    iget v2, p0, Lcom/google/uploader/client/StringDataStream;->currentPosition:I

    int-to-long v2, v2

    add-long/2addr v2, v0

    long-to-int v2, v2

    iput v2, p0, Lcom/google/uploader/client/StringDataStream;->currentPosition:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit p0

    return-wide v0

    .line 75
    .end local v0    # "skippedBytes":J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
