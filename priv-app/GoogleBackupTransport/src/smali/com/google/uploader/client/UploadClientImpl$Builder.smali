.class public Lcom/google/uploader/client/UploadClientImpl$Builder;
.super Ljava/lang/Object;
.source "UploadClientImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/uploader/client/UploadClientImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private httpClient:Lcom/google/uploader/client/HttpClient;


# direct methods
.method private constructor <init>(Lcom/google/uploader/client/HttpClient;)V
    .locals 0
    .param p1, "httpClient"    # Lcom/google/uploader/client/HttpClient;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/uploader/client/UploadClientImpl$Builder;->httpClient:Lcom/google/uploader/client/HttpClient;

    .line 29
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/uploader/client/HttpClient;Lcom/google/uploader/client/UploadClientImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/uploader/client/HttpClient;
    .param p2, "x1"    # Lcom/google/uploader/client/UploadClientImpl$1;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/uploader/client/UploadClientImpl$Builder;-><init>(Lcom/google/uploader/client/HttpClient;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/uploader/client/UploadClient;
    .locals 3

    .prologue
    .line 32
    new-instance v0, Lcom/google/uploader/client/UploadClientImpl;

    iget-object v1, p0, Lcom/google/uploader/client/UploadClientImpl$Builder;->httpClient:Lcom/google/uploader/client/HttpClient;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/uploader/client/UploadClientImpl;-><init>(Lcom/google/uploader/client/HttpClient;Lcom/google/uploader/client/UploadClientImpl$1;)V

    return-object v0
.end method
