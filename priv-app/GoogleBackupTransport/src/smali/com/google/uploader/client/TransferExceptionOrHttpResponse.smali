.class public Lcom/google/uploader/client/TransferExceptionOrHttpResponse;
.super Ljava/lang/Object;
.source "TransferExceptionOrHttpResponse.java"


# instance fields
.field private final response:Lcom/google/uploader/client/HttpResponse;

.field private final transferException:Lcom/google/uploader/client/TransferException;


# direct methods
.method public constructor <init>(Lcom/google/uploader/client/HttpResponse;)V
    .locals 1
    .param p1, "response"    # Lcom/google/uploader/client/HttpResponse;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->transferException:Lcom/google/uploader/client/TransferException;

    .line 23
    iput-object p1, p0, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->response:Lcom/google/uploader/client/HttpResponse;

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/google/uploader/client/TransferException;)V
    .locals 1
    .param p1, "transferException"    # Lcom/google/uploader/client/TransferException;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->transferException:Lcom/google/uploader/client/TransferException;

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->response:Lcom/google/uploader/client/HttpResponse;

    .line 16
    return-void
.end method


# virtual methods
.method public getHttpResponse()Lcom/google/uploader/client/HttpResponse;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->response:Lcom/google/uploader/client/HttpResponse;

    return-object v0
.end method

.method public getTransferException()Lcom/google/uploader/client/TransferException;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->transferException:Lcom/google/uploader/client/TransferException;

    return-object v0
.end method

.method public hasHttpResponse()Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->response:Lcom/google/uploader/client/HttpResponse;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTransferException()Z
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/uploader/client/TransferExceptionOrHttpResponse;->transferException:Lcom/google/uploader/client/TransferException;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
