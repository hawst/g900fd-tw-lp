.class public Lcom/google/uploader/client/TransferOptions$Builder;
.super Ljava/lang/Object;
.source "TransferOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/uploader/client/TransferOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private idleTimeoutSecs:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-wide/16 v0, 0x3c

    iput-wide v0, p0, Lcom/google/uploader/client/TransferOptions$Builder;->idleTimeoutSecs:J

    return-void
.end method

.method static synthetic access$100(Lcom/google/uploader/client/TransferOptions$Builder;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/uploader/client/TransferOptions$Builder;

    .prologue
    .line 13
    iget-wide v0, p0, Lcom/google/uploader/client/TransferOptions$Builder;->idleTimeoutSecs:J

    return-wide v0
.end method


# virtual methods
.method public build()Lcom/google/uploader/client/TransferOptions;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/google/uploader/client/TransferOptions;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/uploader/client/TransferOptions;-><init>(Lcom/google/uploader/client/TransferOptions$Builder;Lcom/google/uploader/client/TransferOptions$1;)V

    return-object v0
.end method
