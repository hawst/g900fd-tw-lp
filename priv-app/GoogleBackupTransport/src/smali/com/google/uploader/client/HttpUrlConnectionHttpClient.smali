.class public Lcom/google/uploader/client/HttpUrlConnectionHttpClient;
.super Ljava/lang/Object;
.source "HttpUrlConnectionHttpClient.java"

# interfaces
.implements Lcom/google/uploader/client/HttpClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;,
        Lcom/google/uploader/client/HttpUrlConnectionHttpClient$ConnectionFactory;
    }
.end annotation


# instance fields
.field private connectionFactory:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$ConnectionFactory;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369
    new-instance v0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$ConnectionFactory;

    invoke-direct {v0}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$ConnectionFactory;-><init>()V

    iput-object v0, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient;->connectionFactory:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$ConnectionFactory;

    .line 370
    return-void
.end method


# virtual methods
.method public createTransfer(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;Lcom/google/uploader/client/DataStream;)Lcom/google/uploader/client/Transfer;
    .locals 4
    .param p1, "urlString"    # Ljava/lang/String;
    .param p2, "method"    # Ljava/lang/String;
    .param p3, "headers"    # Lcom/google/api/client/http/HttpHeaders;
    .param p4, "stream"    # Lcom/google/uploader/client/DataStream;

    .prologue
    .line 382
    const/4 v0, 0x0

    .line 384
    .local v0, "conn":Ljava/net/HttpURLConnection;
    :try_start_0
    iget-object v2, p0, Lcom/google/uploader/client/HttpUrlConnectionHttpClient;->connectionFactory:Lcom/google/uploader/client/HttpUrlConnectionHttpClient$ConnectionFactory;

    invoke-virtual {v2, p1}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$ConnectionFactory;->createConnection(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 391
    new-instance v2, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;

    invoke-direct {v2, v0, p2, p3, p4}, Lcom/google/uploader/client/HttpUrlConnectionHttpClient$HttpUrlConnectionTransfer;-><init>(Ljava/net/HttpURLConnection;Ljava/lang/String;Lcom/google/api/client/http/HttpHeaders;Lcom/google/uploader/client/DataStream;)V

    return-object v2

    .line 385
    :catch_0
    move-exception v1

    .line 386
    .local v1, "e":Ljava/net/MalformedURLException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Url is malformed."

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 387
    .end local v1    # "e":Ljava/net/MalformedURLException;
    :catch_1
    move-exception v1

    .line 388
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Http connection could not be created."

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method
