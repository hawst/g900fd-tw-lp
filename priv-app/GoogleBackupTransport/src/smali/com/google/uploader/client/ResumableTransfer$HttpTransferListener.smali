.class Lcom/google/uploader/client/ResumableTransfer$HttpTransferListener;
.super Ljava/lang/Object;
.source "ResumableTransfer.java"

# interfaces
.implements Lcom/google/uploader/client/TransferListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/uploader/client/ResumableTransfer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HttpTransferListener"
.end annotation


# instance fields
.field private outerListener:Lcom/google/uploader/client/TransferListener;

.field private outerTransfer:Lcom/google/uploader/client/Transfer;

.field final synthetic this$0:Lcom/google/uploader/client/ResumableTransfer;


# direct methods
.method public constructor <init>(Lcom/google/uploader/client/ResumableTransfer;Lcom/google/uploader/client/Transfer;Lcom/google/uploader/client/TransferListener;)V
    .locals 0
    .param p2, "transfer"    # Lcom/google/uploader/client/Transfer;
    .param p3, "listener"    # Lcom/google/uploader/client/TransferListener;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/uploader/client/ResumableTransfer$HttpTransferListener;->this$0:Lcom/google/uploader/client/ResumableTransfer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-object p3, p0, Lcom/google/uploader/client/ResumableTransfer$HttpTransferListener;->outerListener:Lcom/google/uploader/client/TransferListener;

    .line 106
    iput-object p2, p0, Lcom/google/uploader/client/ResumableTransfer$HttpTransferListener;->outerTransfer:Lcom/google/uploader/client/Transfer;

    .line 107
    return-void
.end method


# virtual methods
.method public onException(Lcom/google/uploader/client/Transfer;Lcom/google/uploader/client/TransferException;)V
    .locals 0
    .param p1, "transfer"    # Lcom/google/uploader/client/Transfer;
    .param p2, "status"    # Lcom/google/uploader/client/TransferException;

    .prologue
    .line 123
    return-void
.end method

.method public onResponseReceived(Lcom/google/uploader/client/Transfer;Lcom/google/uploader/client/HttpResponse;)V
    .locals 0
    .param p1, "transfer"    # Lcom/google/uploader/client/Transfer;
    .param p2, "response"    # Lcom/google/uploader/client/HttpResponse;

    .prologue
    .line 119
    return-void
.end method

.method public onStart(Lcom/google/uploader/client/Transfer;)V
    .locals 0
    .param p1, "transfer"    # Lcom/google/uploader/client/Transfer;

    .prologue
    .line 111
    return-void
.end method

.method public onUploadProgress(Lcom/google/uploader/client/Transfer;)V
    .locals 2
    .param p1, "transfer"    # Lcom/google/uploader/client/Transfer;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/uploader/client/ResumableTransfer$HttpTransferListener;->outerListener:Lcom/google/uploader/client/TransferListener;

    iget-object v1, p0, Lcom/google/uploader/client/ResumableTransfer$HttpTransferListener;->outerTransfer:Lcom/google/uploader/client/Transfer;

    invoke-interface {v0, v1}, Lcom/google/uploader/client/TransferListener;->onUploadProgress(Lcom/google/uploader/client/Transfer;)V

    .line 115
    return-void
.end method
